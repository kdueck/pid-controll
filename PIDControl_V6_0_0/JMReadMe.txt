================================
V1_0 (angelegt 26.11.09)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V1_0'
)

- basierend auf: 'Neues IMS', PC-SW, Ver. GSMControlJM_13 (1.13)

JM,
26.11.09


- Forts.: Anlegen: 
	- basierend auf: 'Neues IMS', PC-SW, Ver. GSMControlJM_14 (1.14)

JM,
05.-13.01.10


- Item: "Printing: Improve scan diagram drwaing"
	- vw.cs:
		- modif.: '_OnPrintData()'

JM,
14.01.10
  
- Item: "Ordinaten-Beschriftung: auch in mV"
	- modif.: AppData.cs, PropertiesForm.cs, Vw.cs: y unit 'mV' eingearb.
	- modif.: 'SCANPARAMS' cmd: 
		- bisher: 1 output Par.: meas. begin
		- neu:	  2 output Par.: meas. begin, amplifier scale
	- Doc.cs:
		- neu: member 'dAmplifierScale'
	- Comm.cs:
		- modif. 'OnRxCompleted()'/case "SCANPARAMS": 2. Par. (amplifier scale) eingearb.


JM,
18.01.10


================================
V2_0 (angelegt 21.01.10)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V2_0'
)

- basierend auf: V1_0

JM,
21.01.10

- Item: "Konz-berechnung soll auf Peakfl�chen anstelle von Peakh�hen basieren
	(-> PeakInfo-Anzeige auf separatem Form)"
 

	- ScriptData.cs:
		- modif.: class ScriptWindow
			- neu: Check(), LoadFull(), ToStringFull()
			- angepa�t: alles andere
		- modif.: class ScriptCollection:
			- window check eingearb.
		
	- ScriptEditorForm.cs:
		- modif.: class ScriptEditorForm
			- alle mit dem Windows-ListView verbundenen Routinen, im wesentlichen:
				- _btnWindow_Add_Click()
				- _btnWindow_edit_Click()
				- _ContentsToScript()
				- _ScriptToContents()

	- ScriptEditorWindowForm.cs:
		- modif.: class ScriptEditorWindowForm
			- UI related to the calib. points
			- validity check related to the calib. points ('_btnOK_Click()')

	- Common.cs:
		- modif.: class Check
			- neu: 2. Execute() methode, based on the CheckRelationItemList class

	- Exception.cs:
		- neu: classes ScrMsgStringlengthException, ScrWndInconsistencyException

	- CommMessage.cs:
		- 'raus: class 'CommMessageList' (generally not needed)
		- neu: 'ToString()'
		- umbenannt: Message, _sMessage -> Command, sCommand

	- CommChannel.cs:
		- modif.: WriteMessage ()



	- umbenannt:
		- bisher: 'mi_DataTrasfer_ ...' menuitems und ressource strings
		- neu:	  'mi_Tools_ ...' menuitems und ressource strings



	- MainForm.cs:
		- neu: menuitem Tools/PeakInfo ...

	- CmdUI.cs:
		- neu: 'OnPeakInfo()'

	- neu: PeakInfoForm.cs

	- neu: PeakInfo.cs (classes PeakInfo, PeakInfoItem, PeakInfoItemList)

	- Doc.cs:
		- neu: member PeakInfo

	- App.cs:
		- neu: '_frmPeakInfo' + related stuff

	- Comm.cs:
		- modif.: OnRxCompleted()/SCANALLDATA section:
			- neu: 5. Sektion: PeakInfo data
			- neu: section 'Update the Peak information form, if present'



JM,
25.-27.01.10


================================
V2_1 (angelegt 27.01.10)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V2_0'
)

- basierend auf: V2_0

JM,
27.01.10


- Forts. Item: "Konz-berechnung soll auf Peakfl�chen anstelle von Peakh�hen basieren
		(-> PeakInfo-Anzeige auf MainForm)"

	- raus: PeakInfoForm.cs

	- App.cs:
		- raus: '_frmPeakInfo' + related stuff

	- CmdUI.cs:
		- raus: 'OnPeakInfo()'

	- Comm.cs:
		- modif.: OnRxCompleted()/SCANALLDATA section:
			- raus: section 'Update the Peak information form, if present'

	- MainForm.cs:
		- modif.: MainForm_Closed()
			raus: section 'Close the Peak information form, if present'
		- modif.: UpdateCulture()
			raus: section 'Update the language of the Peak information form, if present'
	
		- raus: menuitem Tools/PeakInfo ... + related stuff





	- MainForm.cs:
		- neu: ListView 'lvPeakInfo' innerhalb GraphPanel
		- damit verbundene Ressorcen

	- Vw.cs:
		- neu: UpdatePeakInfo() + dessen Einbindung


JM,
27.01.10


================================
V2_2 (angelegt 28.01.10)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V2_2'
)

- basierend auf: V2_1

JM,
28.01.10


- Item: "MainForm-UI bereinigen und Inhalte besser arrangieren"

	- MainForm.cs:
		- raus: Groupbox 'Spektren-Darstellung' nebst Inhalt (Radiobuttons M1-8, Panels P1-8) 
		- raus in diesem Zus.: members 'arrbScan', 'arpScan'
		
		- raus: Spektren-CB 'cbSpectrum'
		- raus: 'chkStoredScans' (Spectrum recording checkbox)
		- raus: label 'lblSpectrumRecording'

		- neu: label '# of Points' (2 x, im GraphPanel): Ausgabe der aktuell akquirierten Scan-Punkte

		- neu: Groupboxes 'Device info', 'Results', 'Peak info', 'Spectrum recording'
		- neu: Temp.-und Druck-DeviceInfo-Anzeige innerhalb der Groupbox 'Device info'

		- modif.: menuitem 'Statical print preview' in Main-Load ausgeblendet:
		  PrintPreview wird IMMER statisch angezeigt. 

		- modif.: '_TimerTest_Tick()': Test class usage eingearb
		
	- Doc.cs:
		- raus: members 'nRecordScan', 'nLastScanNumber', 'nViewScan'
		- modif.: MAX_SCANBUFFERS: bisher; 8 -> neu: 1

		- raus: - method'RecordMemory()'
			- members 'bRecordMemory', 'sRecordMemoryFilename'
		- modif.: die anderen 3 recording Routinen:
			- zus�tzl. Abtesten auf: "if (doc.nPoints == Doc.NSCANDATA)"

		- neu: member 'nPoints'

	- Vw.cs:
		- raus: member 'm_nMaxScan'
		- raus: 'OnScanViewChanged()'
		- modif.: 'UpdatePeakInfo()': units gem�� UI Wahl
		- neu: code zur 'nPoints'-Anzeige

	- neu: file 'Test.cs'
		- class Test: Testing stuff

	- modif.: file 'SpecEval.cs' 
		- Class SpecEvalParameter: Peak detection parameter 
		- Class SpecEval: Spectrum evaluation

	- CmdUI.cs:
		- raus: 'OnRecordStoredScans()'
		- raus: section 'testing stuff' (�bergegangen in Test class)

	- Comm.cs:
		- modif.: OnRXCompleted / SCANALLDATA: 'WScanNo' raus

JM,
29.01.10


================================
V2_3 (angelegt 08.03.10)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V2_3'
)

- basierend auf: V2_2

JM,
08.03.10


- ZIEL: "Das PC-Programm auf den neuesten Stand (d.h. analog GSMControl, Ver. 1.17.2) bringen"
-----------------------------------------------------------------------------------------------

- Item: "Komplette �berarbeitung der RS232 communication"
	- Ordner CommRS232: komplett �berarb.
	- modif. in diesem Zus.:
		- App.cs:	
			- Klass 'AppComm' anstelle der bisherigen class 'Comm'
			- raus: members '_frmCurr', 'commChannel' & related stuff
	
		- AppComm.cs: 	
			- ehemalige class 'Comm', neu gestaltet:
			- raus: member '_timer' & related stuff
			- raus: answer- & error event handling
		
		- CmdUI.cs:
			- modif.: 'OnReadEepromStore()', 'OnReadSDcardStore()', 'OnTransferService()':
				CurrentForm-related stuff 'raus
		
		- alle Module, in denen communication enthalten ist (Dialoge, Measure.cs, CmdUi.cs):
			- �berall, wo CommMessage objecte genutzt werden: entspr. member-Aktual. eingepflegt
			- neu: comm. responses laufen jetzt in den Form's ein, in welchen die comm.-Aufrufe
				auch emittiert wurden, und NICHT mehr zentral im Comm-objekt der App.
				Das gleiche gilt f�r error responses.
				(Betroffen sind alle Form's, in welchen ser. comm. verwendet wird.)
			- neu: 'CommErrorForm' eingebunden
			- ConnectForm.cs:
				- modif.: '_Timer_Tick()'


- Item: "In der comment-TB des service dlgs. erscheint anstelle eines Kommas eine 'Eule&Meerkatze'. -> BUG"
	- ServiceForm.cs
		- region 'constants & enums':
			- section 'Service comment: Substitute char's' ge�ndert

- Item: " Beim script transfer wird nur M�ll �berspielt ... -> BUG"
	- ScriptTransferForm.cs:
		- modif.: '_SendScript()'
			- sauber gefa�t: �berall 'msg = new CommMessage (...)" verwendet, so wie es sein mu�

- Item: "Messageboxes �ber dem owner window anzeigen, und nicht nur in der task leiste.
	 (Erl�ut.: Manchmal erscheinen die message boxes nur in der task bar, sind aber selbst NICHT
	  im Vordergrund sichtbar -> kein BUG, aber unsch�n)"

	- SDcardReaderForm.cs, GSMReaderForm.cs, ScriptTransferForm.cs:
		- konsequent 'MessageBox.Show (this, ...)' verwendet

- Item: "Autom. Sprachsynchronisation einarb."
	- Comm.cs:
		- neu: message "LANGSYN"
	
	- AppData.cs/class languageData:
		- neu: member 'bLangSyn' (Read/Write routines entspr. modif.)
	
	- PropertiesForm.cs/Reiter 'Language':
		- neu: checkbox 'Autom. Sprachsynchronisation' + rel. stuff

	- ConnectForm.cs / 'ConnectForm_Load()':
	- CmdUI.cs / 'OnProperties()':
		- modif.: 'Autom. Sprachsynchronisation' eingearb.



JM,
08.03.10



- Item: "GSMReaderForm: Namen von zu speichernden Data- und Scriptfile in Registry merken"
		- AppData.cs / class CommonData:
			- neu: 
				- member 'File where data storage takes place: Last Eeprom data file': sEepromDataFile
				- member 'File where data storage takes place: Last Eeprom script file': sEepromScriptFile

		- GSMReaderForm.cs:
			- modif.: '_btnFile_Click()'

- Item: "Ein button f�r das ext. sampling w�re sch�n (SB)"
	- MainForm.cs:
		- neuer TBB: '_tbbTriggerKeyCmd' + related stuff


- Item: "Gespeicherte Spektren: Anzeige des Druckes in mbar <-> im display: kPa (SB) -> BUG"
	- Doc.cs:
		- modif.: 'RecordScanDataWriteCommonHeader()'
			- Abspeicherung des Druckes in mbar

- Item: "Sichern, da� die initiale Baudrate NICHT ge�ndert wird (durch Registry-Auslesen o.�.)"
	- AppData.cs:
		- class CommunicationData:
			- neu: member 'bBaudRateFixed'

		- modif.: '_ReadRegistry()', '_ReadResourceFile()'
			- Baudrate-sections:  'bBaudRateFixed' eingearb.



JM,
09.03.10


- Item: "Status-Anzeige im Main display: anstelle von 'Scans' -> 'Transfers'"
	- MainForm.cs:
		- modif.: '_Timer_Tick()'
			- Resource 'Main_Status_P2' entspr. ge�nd.

- Item: "PC-gestarteten script stoppen via device klappt nicht -> BUG"
	- MainForm.cs:
		- modif.: 'OnRXCompleted()': section 'SCANALLDATA':
			- neu: anf�ngliche  CD: NODATA recognition or data received?
			- neu: section '// NODATA recognition'


JM,
12.03.10


- Item: "Spektrenaufzeichnung: Lokalisierte und englische Fassung f�r alle 3 Varianten"
	- Doc.cs:
		- modif.: 'RecordResult()', 'RecordScan()', 'RecordSingleScan()'
			- 'LOCALIZED_COMMON_HEADER' sections eingearb.

- Item: "BUG in der Spektrenaufzeichnung: bei der kont. Aufzeichnung von scans & results
	werden jeweils Scan- bzw. Resultatswolken gespeichert"
	- Doc.cs:
		- neu: member 'm_bScansRecorded', 'm_bResultsRecorded'
		- modif.: 'RecordData()', 'RecordingReset()'
			- member eingearb.



JM,
15.03.10


******************************************************************************************
Backup:
- Ordner:		D:\Backups\MSVS2003Ent\WindowsApps\PID_IUT\PIDControl_Ver2_3_Park0
- korr. device SW: 	D:\Backups\IAR_EW\IUT\PID\PID_V2_3_Park0
******************************************************************************************


JM,
15.3.10


================================
V2_4 (angelegt 16.03.10)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V2_4'
)

- basierend auf: V2_3

JM,
16.03.10


- Item: "2 neue Script-Parameter einbeziehen: Signal processing interval, PC communication interval"
	- GSMReaderForm.cs:
		- modif.: 'OnRXCompleted()'/section STRESPARAM:
			- 3 Parameter einbezogen


	- ScriptData.cs:
		- class ScriptCollection:
			- modif.: 'Write()': 3 Parameter einbezogen
		- class ScriptParameter:
			- neu: member 'dwSigProcInt', 'dwPCCommInt'

	- ScriptEditorForm.cs:
		- modif.: GUI: 2 CB'n f�r zus�tzliche Parameter
		- modif.: '_ScriptToContents()', '_ContentsToScript()', '_InitControls()',
			'_ctl_HelpRequested()', 


	- MainForm.cs:
		- modif.: 'OnRXCompleted()'/section SCANPARAMS:
			- section '// Update the documents ScanParams member'

	- neu: ScanParams.cs: class ScanParams

	- Vc.cs:
		- modif.: '_ConvertAccordingToX/YUnit()':
			- angepa�t an ScanParams object

	- Doc.cs:
		- raus: member 'wMeascycle', 'wAccuRate', 'dAmplifierScale'
		- daf�r neu: member 'ScanParams'


JM,
16.03.10



================================
V2_5 (angelegt 18.03.10)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V2_5'
)

- basierend auf: V2_4

JM,
18.03.10


- Item: "PID-Offset neben T, p, ... auf GUI ausgeben"
	- MainForm.cs:
		- modif: GUI
			- section 'Device information': 2 Offset-Labels neu angelegt

	- DeviceInfo.cs:
		- neu: member 'wOffset' + Einarb.

	- Diagnosis.cs:
		- modif.: 'Append()'
			- 'wOffset' einbezogen

	- Doc.cs:
		- modif.: 'RecordScanDataWriteCommonHeader()'
			- 'wOffset' einbezogen

	- Vw.cs:
		- modif.: 'OnInitialUpdate()', 'UpdateResultData()'
			- 'wOffset' einbezogen
		- modif.: ''_OnPrintParams()'
			- 'wOffset' einbezogen


JM,
18.03.10



================================
V2_6 (angelegt 19.03.10)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V2_6'
)

- basierend auf: V2_5

JM,
19.03.10


- Item: "Scan offset MinMax via service screen an device �bergeben (um dort die Korrektheit des
	gemessenen scan offsets abzupr�fen)"
	- ServiceForm.cs:
		- GUI / section 'spectrum analysis data':
			- neu: 2 TB'n f�r Scanoffset Min/Max + zugeh�rige Labels

		- modif.: '_CheckControls()', '_UpdateServiceData()'
			- Scanoffset Min/Max einbezogen


JM,
19.03.10


- Item: "Beim �ffnen einer scriptdatei kommt anstelle des FileOpen-Dialogs eine Fehlerausschrift,
	wenn der Ordner nicht mehr existiert -> BUG!"
	- Doc.cs:
		- modif.: 'GetFileName()' / section '// Initial dir.'
			- Directory.Exists-Pr�fung hinzugef�gt


JM,
23.03.10



- Item: "Neben scan offset auch STD an PC �bergeben und bei Data logging in scan file hinterlegen"
	- DeviceInfo.cs:
		- neues member: 'dSTD' (STD) + eingearb.

	- Doc.cs:
		- modif.: 'RecordScanDataWriteCommonHeader()'
			- Offset- und STD-Logging einbezogen


- Item: "Test funcionality auf reale scans ausdehnen"
	- Test.cs: erweitert um 'real scan' funcionality
		- neue constants: enum TestMode
		- neue member: eTestMode, fAve, fSTD, fTemp, fPres, sScanFilePath, sPeakInfo
		- neue routines: 'GetRowValue()', 'FillTestArray_Real()'
		- (Umbenennung weiterer, bereits ex. routines)

	- CmdUI.cs:
		- modif.: 'OnTest()' / section 'UI-/PrintPreview test via RS232 emulation'
			- erweitert um 'real scan' funcionality



- Item: "Im test mode werden bei disablter, aber gecheckter 'scan cont.' checkbox scans wiederholt
	gespeichert -> BUG!"
	- Measure.cs:
		- modif.: 'StopMeasure()'
			- section '// Update documents 'bRecord ...' members: Disable recording' eingearb.


JM,
23./24.03.10


================================
V2_7 (angelegt 25.03.10)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V2_7'
)

- basierend auf: V2_6

JM,
25.03.10



- Item: "Peak area calculation verbessern"

	- SpecEval.cs:
		- neu: members 'g_nMins', 'g_ariMinPos[MAX_PEAKS]'; 	
		- neu: 'DetectPeaksEx()'
		- neu: routines '_FindPeaksEx_I()', 'RejectNegativePeaks()'
		- neu: routines 'PeakAreaDetectEx_Sigma_Calc()', 'PeakAreaDetectEx_NoiseLevel()'

	- Test.cs:
		- modif.: 'TestArray_SCANALLDATA_Real', 'TestArray_SCANALLDATA_Sim'
			- ausgewechselt: bisher: 'DetectPeaks()' -> neu: 'DetectPeaksEx()'


JM,
25./26.3.10


- Item: "PID-Ikone k�nnte besser sein ..."
	- neu: Images/App/PIDControl.ico


- Item: "GSMReaderForm liest/speichert Ordnernamen NICHT in Registry, wie eigentlich gew�nscht -> BUG"
	- GSMReaderForm.cs:
		- modif.: 'GSMReaderForm_Load()', 'GSMReaderForm_Closed()'
			- 'Read/Write app data: EEprom data & scriptinfo filepaths' eingearb.


- Item: "Fehlerhaftes Verhalten im GSMReaderForm beim �ffnen einer Datei, falls diese nicht mehr
	existiert. -> BUG!"
	- GSMReaderForm.cs:
		- modif.: '_btnFile_Click()' / section 'Prepare 'Save file' dialog / Initial dir.':
			- Check auf dir. existence eingearb.


JM,
29.3.10


================================
V2_8 (angelegt 29.03.10)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V2_8'
)

- basierend auf: V2_7

JM,
29.03.10


- Item: "Verschiedene 'n' sigma Faktoren f�r a) peak search und b) peak area determination
	via service dialog editieren + an device �bergeben"
	- ServiceForm.cs:
		- neu: GUI: label + TB : peak area detection
		- entspr. modif.: 'Form_Load()', '_CheckControls()', '_UpdateServiceData()'
		- neu: '_txtPeakSearchNsig_TextChanged()'



- Item: "Z.Z. unklar, inwieweit die 'AMPL_SCALE_EEPROM_LOCATION' define der device-SW eingebunden werden
	soll bzw. mu�, deshalb sollte die 'Amplifier scale ' textBox des service dialogs NICHT
	editiert werden k�nnen."
	- ServiceForm.cs:
		- GUI: '_txtAmplScale' control readonly gemacht


JM,
29.03.10


================================
V2_9 (angelegt 8.4.10)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V2_9'
)

- basierend auf: V2_8

JM,
8.4.10


- Item: "PID lamptime Verwaltung einarbeiten"
	- ServiceForm.cs:
		- modif.: GUI / section "Device data"
			- eingearb.: PID lamp


		- neu: '_chkResetPIDlamp_CheckedChanged()'

		- modif.: '_UpdateServiceData()' / section "Device" (2 x)
			- eingearb.: PID lamp

		- modif.: '_ctl_HelpRequested()'
			- eingearb.: PID lamp


JM,
16.4.10


- Item: "In den scan files sollte bei speicherung auch die positionen & area der peaks mit ausgegeben werden
	 -> MF, 16.4.10"
	- Doc.cs:
		- modif.: 'RecordScanDataWriteCommonHeader()'
			- neu: section "// Rows: Peak information (subheader)"


- Item: "die aktuelle grafik (scan), die nach stoppen der �bertragung auf dem GUI sichtbar ist, sollte
	 gespeichert werden k�nnen -> MF, 16.4.10"
	- MainForm.cs:
		- neuer menu item: '_miDataRecording_Grafic'
		- entspr. modif.: 'UpdateCulture()', '_mi_Popup()', '_mi_Click()'

	- CmdUI.cs
		- neu in diesem Zus.: 'OnRecGrafic()'

	- Doc.cs:
		- neu in diesem Zus.: 'RecordSingleScanWithMessage()'




JM,
19.4.10
 

- Item: "Bei Benzol-messung zeigt das Ger�t anstelle EINes Peakes einen 'zerfaserten' Mehrfach-Peak an
	 -> BUG! (s. auch entsprechende Dateien von MF)"

	-> Grund: der nsig-Faktor f�r die peakerkennung ist zu klein (bisherige OG 10 reicht nicht aus)
		  u. mu� gr��er eingestellt werden k�nnen 

	- ServiceForm.cs:
		- GUI: "Peak search: 'n' sigma factor (in [6, (N)], def. 6):"
			- bisher: N=10 -> neu: N=100
			  (+ ressourcen f�r label entspr. aktual., max. Inputchars f�r TB entspr. aktual.)
		- modif.: '_CheckControls()':
			- validity der '_txtPeakSearchNsig' control: 
				- bisher: [6,10] -> neu: [6,100]


JM,
21.4.10


- Item: "BUG-Beseitigung im Test-Modus"
	- SpecEval.cs:
		- neu: 'DetectPeaksEx()' mit n sigma factoren for peak search & peak area det.

	- Test.cs:
		- modif.: 'FillTestArray_Real()':
			- section 'Header/ End of header' sauber gefa�t
		- modif.: 'TestArray_SCANALLDATA_Real()'
			- o.a. DetectPeaksEx()-routine verwendet


JM,
26.4.10


================================
V2_9_1  (angelegt 10.5.10)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V2_9'
)


JM,
10.5.10

==> Aufgrund einer Bug-Meldung in IMSControlJM mu� die PIDControl-SW ebenfalls ge�ndert werden, da dort
	analoge Vorgehensweise:

- Item: "SB meldet bzgl. IMSControlJM : BUG unter Vista: RSA-De/Encrypt macht �rger, wenn man das 		Benutzerkonto/Password wechselt: Das PN l�uft dann nicht mehr an bzw. bringt eine error message bei der 	PW-Eingabe."

	- Crypt.cs:
		- neu: routines 'JMEn-/-Decrypt ()'
	- gesamte App: 
		- alle Vorkommen von 'RSAEn-/-Decrypt ()' wurden durch 'JMEn-/-Decrypt ()' ersetzt.


- Item: "Program-Version f�r registry-Speicherung: 
		bisher: Major.Minor
		neu: Major.Minor.Build"

	- Registry.cs:
		- modif.: 'AppRegistry()'
			- section '// Products registry key' entspr. erweitert


JM,
10.5.10

- (gleicher Item): "Es d�rfen nur ASCII chars und BS als key input zugelassen werden."

	- PWForm.cs:
		- TB control: neu: '_txtPW_KeyPress()'
	- PWNewForm.cs:
		- 2 x TB controls: neu: '_txt_KeyPress()'


JM,
11.5.10


================================
V2_9_2  (angelegt 18.6.10)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_0'
)

- basierend auf: V2_9_1


JM,
18.6.10

- Doc.cs:
	- Device program version angepa�t

JM,
18.6.10


- Item: "�nderungen f�r CombiNG embedding einarbeiten"

	- 1. Device ready for meas.: Bit 15 of the HW control word is used therefore.
		- ScanInfo.cs: 
			- entspr. erg�nzt: Remarks zu class ScanInfo

	- 2. Slot-Initialisierung: neues "PC" cmd: DEVTYPE
		- AppComm.cs:
			- neu: section "CombiNG embedding Messages" within the members region

	- 3. Error-Dword via SCANALLDATA zum Kombi transferieren
		- ScanInfo.cs:
			- entspr. erg�nzt: Remarks zu class ScanInfo
			- neu: member 'dwError' + eingearb.
			- modif.: 'ScanInfo ( string sPar )'
				- Remarks aktualisiert
				- const int _NO_SCANINFO_MEMBERS: 4 (bisher) -> 5 (neu) 
		- Test.cs:
			- modif.: properties 'TestArray_SCANALLDATA_Sim', 'TestArray_SCANALLDATA_Real'
				- section "// 2. part: Scan info" entspr. aktual.



JM,
22.6.10


- Item: "Gl�ttung des PID-Spektrums einarb. / ausprobieren"
	
	- SpecEval.cs:
		- neu: 'SmoothArray()', 'sgsmth()'

	- neu: 'TestForm.cs'

	- Test.cs
		- neu: globals 'm_bSmoothing', 'm_nFilterWidth', 'm_nNSigPD', 'm_nMethod'
		- neu: 'SetParams()'
		- modif.: 'TestArray_SCANALLDATA_Real()'
			- neue Param's & routines eingearb.

	- CmdUI.cs:
		- modif.: 'OnTest()'/section "UI-/PrintPreview test via RS232 emulation"
			- Aufruf v. 'SetParams ()' eingearb.


- Item: "Gegl�ttetes spektrum als 2. scan grafisch darstellen " 

	- View.cs:
		- modif.: 'UpdateGraph()' / section "// Allocate & Fill the data array, based on the scan arrays to be drawn"
			- bisher: 1 data array (raw scan) -> neu: 2 data arrays (raw & smoothed scans)

	- Doc.cs:
		- modif.: const 'MAX_SCANBUFFERS': bisher: 1 -> neu: 2
		- neu: 'BuildSmoothedScanArray()'

	- MainForm.cs:
		- modif.: 'OnRXCompleted()' / section SCANALLDATA:
			- neu: sections "// Select the scan receive buffer for the smoothed scan" and
                	       "// Build the smoothed scan"

	- ScanData.cs:
		- Methoden-Name ge�nd.: bisher: 'ToArray()' -> neu: 'ToFloatArray'
		


JM,
24.6.10

- Item: "Auswahl der darzustellenden scans via CB"

	- MainForm.cs:
		- neu: CB 'cbScansToDraw' + event handler 'cbScansToDraw_SelectedIndexChanged'
		- modif.: 'UpdateCulture()'
			- section "// CB 'cbScansToDraw'" eingearb.

	- Doc.cs:
		- neu: member 'arScanToDraw'

	- GraphData.cs:
		- neu: member '_arDataToDraw'
		- modif.: 'Attach()'
			- section "// Show all data graphs" eingearb.
		- modif.: 'DrawData()'
			- section "// Check: Should the current data graph be drawn?" eingearb.
		- neu: 'SetDataToDraw()'

	- View.cs:
		- modif.: 'UpdateGraph()'
			- section "// Select the data arrays to be drawn" eingearb.
 


JM,
25.6.10


- Item: "Scriptzeile "100=lamp,10" wird als fehlerhaft moniert"

	- ScriptCommands.cs:
		- modif.: 'ScriptCommand[]':
			- bisher: ...
				new ScriptCommand ("LAMP", true, 1, 0, -1, 0, 1),                             // 0,1
				  ...
			- neu: ...
				new ScriptCommand ("LAMP", true, 1, 0, 100, 0, -1),                             // 0-100
				  ...
		- Ressource-string 'ScriptCommands_HlpOnCmd_LAMP' entspr. ge�ndert




JM,
28.6.10

-Item: "Anzeige der 'Height'-daten in der peakinfo sauber fassen: w�hrend der DAQ - raw scan, nach der DAQ - complete scan"
	
	- View.cs:
		- modif.: 'UpdatePeakInfo()'
			- section "// The current scan" ge�ndert.


JM,
29.6.10


================================
V3_0_0  (angelegt 6.7.10)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_1'
)

- basierend auf: V2_9_1


JM,
6.7.10

- Doc.cs:
	- Device program version angepa�t

JM,
6.7.10


- Item: "Die device DateTime mu� via PC oder �bergeord. Ger�t gesetzt werden k�nnen"
	- AppComm.cs:
		- neu: "DATI" message 
	- ConnectForm.cs:
		- modif.: 'ConnectForm_Load()'
			- neu: section "// TX: DateTime" 


JM,
6.7.10


================================
V3_1  (angelegt 30.7.10)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_2'
)

- basierend auf: V3_0_0


JM,
30.7.10

- Doc.cs:
	- Device program version angepa�t

JM,
30.7.10


- Item: "Messbereichs�berschreitung (OverFlow) einarbeiten"

	1. subitem "Peak invalid (overflow)"

	- PeakInfo.cs:
		- class PeakInfoItem:
			- neu: member 'Invalid'
		- class PeakInfo:
			- con# entspr. modif.
	- View.cs:
		- modif.: 'UpdatePeakInfo()'
			- Abschnitt "//    3. subitem: Area (unit: [adc*pts])" entspr. modif.
	- Doc.cs:
		- modif.: 'RecordScanDataWriteCommonHeader()'
			- Abschnitt "// Rows: Peak information (subheader)" entspr. modif.

	2. subitem "Conc. incredible"

	- ScanResult.cs:
		- neu: member 'byConcIncredible' + eingearb.
	- MainForm.cs:
		- modif.: 'OnRXCompleted()' / section '"SCANALLDATA"': 
			- entspr. erweitert: subsection "// SCANRESULT data block"
	- View.cs:
		- modif.: 'UpdateResultData()'
			- Abschnitt "// Display the substance result" entspr. erweitert
	- Doc.cs:
		- modif.: 'RecordScanDataWriteCommonHeader()'
			- Abschnitt "// Rows: Windows (Name, Begin, Width, Concentration unit)" entspr. modif.

	3. Test stuff entspr. anpassen

	- SpecEval.cs:
		- neu: member 'g_arbyPeakInvalid', property 'PeakInvalid'
		- modif: 'DetectPeaksEx()'
			- eingearb.: Abschnitt "// Peak validity check (Overflow)"

	- Test.cs:
		- entspr. Anpassungenin 'TestArray_SCANALLDATA_Sim' und 'TestArray_SCANALLDATA_Real'


- Item: "Gain vom device zum PC �bertragen (als 2. Wert in der SD section)"
	-> Goal: Die Kombi-NG-SW kann beim direkten RX'n & Parsen der SCANALLDATA data den compressed PID-scan
		sauber bilden (f�r die Skalierung ist das data-Max. erford., und das h�ngt vom Gain ab) 
	- MainForm.cs:
		- modif.: 'OnRXCompleted()' / section '"SCANALLDATA"': 
			- neu: segment "//      5.2. Gain"
	- Doc.cs:
		- neu: member 'Gain'


- Item: "error-ausgabe einarb., wenn RX SCANALLDATA fehlschl�gt aufgrund fehelerhafter device datatime"
	- MainForm.cs:
		- modif.: 'OnRXCompleted()' / section '"SCANALLDATA"': 
			- entspr. erweitert: subsection "// INFO data block"
	

- Item: "die text-farben des result panels (darggray f�r non-alarm-, darkred f�r alarm case) sind nicht OK ->
	 bessere w�hlen"
	- View.cs:
		- modif.: 'UpdateResultData()'
			- Abschnitt "// Choose the colors of the result TB, depending on the alarm case" entspr. ge�nd.


- Item: "class scanresult aufpeppen: ToString method einf�hren"
	- ScanResult.cs:
		- neu: 'ToString()'


- Item: "Test class mit realen data: scan result NICHT fiktiv bilden, sondern aus Scan-Datei �bernehmen
	 "Abschnitt '// Rows: Windows in Running Script (subheader)')"

	- Doc.cs:
		- modif.: 'RecordScanDataWriteCommonHeader()'
			- Abschnitt "// Rows: Windows (Name, Begin, Width, Concentration unit)" so ge�ndert,
			  da� auch das 'Conc. incredible' member ber�cksichtigt wird

	- Test.cs:
		- modif.: 'FillTestArray_Real()'
			- neu: Parsing + Speicherung von "// Windows in Running Script"
		- in diesem Zus.: neue global: 'arScanResult'
		- modif.: 'TestArray_SCANALLDATA_Real'
			- ge�nd.: section "// 3. part: Scan result"



JM,
30.7.-2.8.10



================================
V3_2  (angelegt 5.8.10)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_3' 
)

- basierend auf: V3_1


JM,
5.8.10

- Doc.cs:
	- Device program version angepa�t

JM,
5.8.10


- Item: "TODO-Liste MF/HG vom 5.8.10 umsetzen"
	
	Punkt 15:
	- ServiceForm.cs:
		- MAXDEVDATALENGTH = 15 gesetzt
		- modif.: '_CheckControls()': section "// Show detailed message" eingef�gt 

	Punkt 20a:
	- MainForm.cs + Ressourcen-dateien:
		- entspr. Strings ge�ndert

	Punkt 17:
	- MainForm.cs:
		- 'X' / 'Y' labels vor Cursor-Pos.-labels eingearb.

	Punkt 13:
	- TitelForm.cs:
		- Timer von 5 sec auf 2,5 sec heruntergesetzt

	Punkt 9:
	- MainForm.cs: 
		- neu: member 'btnDisplayedGrafic' + Einbindung (Bitmap, Ressourcen, ...)
		- modif.: cmdUI.OnRecGrafic() (der neue Button + der zugeornete menuitem machen das gleiche)
	Punkt 5:
	- View.cs:
		- modif.: '_OnPrintParams()'
			- section "// Substance " erweitert um alarm indication
	Punkt 7:
	- View.cs:
		- modif.: '_OnPrintHeader()'
			- Comment eingearb., dabei Layout der einzelnen sections ge�nd.

		- modif.: 'OnPrint()'
			- section "// Get the positions for header, data and params drawing area:" ge�nd.:
			  Verh�ltnis ge�nd.

	Punkt 10:
	- Doc.cs:
		- neu: section "// Loading scan from HD "
		- neu: member 'sScanFilePath'
	- CmdUI.cs:
		- neu: 'OnLoadScanFromHD()'
	- MainForm.cs:
		- neu: menuitem '_miTools_LoadScanFromHD' + Einbindung (Ressourcen, ...)

JM,
6.8.10

- Item: Forts.: "TODO-Liste MF/HG vom 5.8.10 umsetzen"


	Punkt 8:
	- PeakInfo.cs:
		- class PeakInfoItem:
			neu: member 'Heigth'
		- class PeakInfo:
			con#: Heigth eingearb.
	- View.cs:
		- modif.: 'UpdatePeakInfo()'
			- raus: section "// The current scan"
			- modif.: section "//    2. subitem: Height"

	- Doc.cs:
		- modif.: 'RecordScanDataWriteCommonHeader()'
			- section "// Rows: Peak information (subheader)" ge�nd.

		- modif.: 'FillDataStructures()'
			- section "// Parse the current PI data row" ge�nd. 

	- Test.cs:
		- modif.: 'TestArray_SCANALLDATA_Sim', 'TestArray_SCANALLDATA_Real'
			- section "// 4. part: Peak info" ge�nd.: Height eingef.



JM,
9.8.10


- Item: Forts.: "TODO-Liste MF/HG vom 5.8.10 umsetzen"


	Punkt 4:
	- GraphData.cs:
		- neu: const 'MAX_MARKER_WINDOWS'
		- modif.: '_fXMarkBegin', '_fXMarkWidth': float (bisher) -> float[] (neu),
			  entspr. Properties ge�nd.
		- raus: 'bMarkWindow' + property -> no longer needed
		- modif.: 'DrawData()'
			- section "// Draw the marker windows" ge�nd.

	- View.cs:
		- modif.: 'UpdateGraph()'
			- section "// Indicate the windows to be marked" ge�ndert
		- raus: 'OnResultCheck()': no longer needed

	- Doc.cs:
		- modif.: member 'nMarkedWindow': int (bisher) -> bool[] (neu) 

	- MainForm.cs:
		- modif.: '_chk_CheckedChanged()'



	Punkt 3:
	- MainForm.cs:
		- modif.: '_mi_Popup()'
			- section "// Acknowledge alarm": 
				- bisher: bEnable = true;
        			- neu: bEnable = doc.bResultAlarm;
			- section "// Trigger Key Cmd":
				- neu: zeile "bEnable &= !doc.bResultAlarm;"
		- modif.: '_UpdateToolbarButtons()'
			- section "// Trigger Key cmd":
				- neu: zeile "bEnable &= !doc.bResultAlarm;"


	- Measure.cs:
		- modif.: "StartMeasure()"
			- section "// Indicate: No alarm present" hinzugef.

	- cmdUI.cs:
		- modif.: 'OnAlarmAcknowledge()'


	Punkt 21:
	- ServiceForm.cs:
		- neu: ChB-control '_chkPresOff' 
		- diesbzgl. modif.: '_ctl_HelpRequested()'
		- diesbzgl. neu: '_chkPresOff_CheckedChanged()'
		- diesbzgl. modif.: '_CheckControls()'
		- diesbzgl. modif.: '_UpdateServiceData()'



JM,
10.8.10


- Item: Forts.: "TODO-Liste MF/HG vom 5.8.10 umsetzen"

	Punkt 20b:
	- MainForm.cs:
		- modif.: '_mi_Popup()'
			- section "// Trigger Key Cmd":
				- Auswertung von 'bDevReadyForMeas' eingearb.
		- modif.: '_UpdateToolbarButtons()'
			- section "// Trigger Key cmd":
				- Auswertung von 'bDevReadyForMeas' eingearb.

	Punkt 14:
	- AppComm.cs:
		- neu: message "msgScriptFilename"
	- ScriptTransferForm.cs:
		- modif.: 'OnRXCompleted()'
			- section "// ScriptFilename" hinzugef.
		- modif.: '_SendScript()'
			- section "// TX: Script Filename" eingef�gt


	Punkt 11: ( == Dimming ist via Script cmd zu real. == )
	- ScriptCommands.cs:
		- modif.: member 'arsc'
			- scriptcmd "DIMMING" hinzugef.
		- modif.: 'HelpOnCmd()'
			- section "case "DIMMING":" hinzugef.



- Item: "Einige Engl. Ausdr�cke sauber fassen"
	- 'ScriptSelect_txtIntro': ...measuring -> ...measurement
	- 'Main_Status_P1_Running': dito




JM,
11.8.10


- Item: Forts.: "TODO-Liste MF/HG vom 5.8.10 umsetzen"

	Punkt 28:
		- ScriptData.cs
			- modif.: Class ScriptWindow
				- neu: member 'nCalType' + Einarbeitung 

		- ScriptEditorWindowForm.cs:
			- neu: UI-member '_CBCalType', member '_CalType' + Einarbeitung

		
		- ScriptEditorForm.cs:
			- neu: UI-member 'colCalType'
			- modif.: '_btnWindow_Add_Click()', '_btnWindow_Edit_Click()'
				- ScriptEditorWindowForm-member 'CalType' eingearb.



JM,
12.8.10


- Item: "Bei start eines scripts m�ssen alle result panel checkboxen uncheckt sein"
	- Vw.cs:
		- neu: 'ClearResultPanel()'
	- Measure.cs:
		- modif.: 'StartMeasure()'
			- section "// Clear the result panel" einfeg�gt




- Item: "Falls Druckanzeige auf dem device deakt. ist, so mu� das auch am PC so sichtbar sein"

	- DeviceInfo.cs:
		- neu: member "nPresDispOnOff" + Einarb.
	- Diagnosis.cs:
		- modif.: 'Append()'
			- 'Pressure display on/off' eingearb.
	- Doc.cs:
		- modif.: 'RecordScanDataWriteCommonHeader()', 'FillDataStructures()'
			- 'Pressure display on/off' eingearb.
	- Vw.cs:
		- modif.: 'UpdateResultData()', '_OnPrintParams()'
			- 'Pressure display on/off' eingearb.




JM,
13.8.10


- Item: "Wenn ein gespeichertes spektrum zur ansicht zur�ckgeladen wurde -> entsprechende OFFLINE
	 Info anzeigen"
	- MainForm.cs:
		- neu: UI-member 'lblOffline'
	- Vw.cs:
		- modif.: 'OnInitialUpdate()'
			- 'lblOffline' handling eingearb.
	- cmdUI.cs:
		- modif.: 'OnLoadScanFromHD()', 'OnExecuteScript()'
			- 'lblOffline' handling eingearb.


- Item: "bei deviceinfo-Fehler (SCANALLDATA-answer) funktioniert die Script-beendigung und 
	 Fehlerausachrift nicht wie gew�nscht -> BUG"
	- MainForm.cs:
		- modif.: 'OnRXCompleted()'
			- 1. section "// A Timeout occurred:", subsection "React":
				�nderung eingearb.
			  (dies steht NICHT in direktem Zusammenhang mit dem Bug, ist mir aber bei
			   der Durchsicht aufgefallen)
			- 2. section "// Update the documents 'DeviceInfo' member" / normal mode:
				�nderung eingearb.



- Item: "Der 'Trigger Key Cmd' TBB sollte im alarmfall einen entspr. Tooltip-Text zeigen (FF)"
	- MainForm.cs:
		- modif.: '_Timer_Tick()'
			- neu: section "// ToolTip text for the 'Trigger Key cmd' TBB, depending on alarm state"

		- modif.: 'UpdateCulture()'
			- Passagen, die den 'Trigger Key Cmd' TBB betreffen, ausgesternt


- Item: "Eine Alarm-Auswertung sollte nur nach (Neu)berechnung der Me�resultate stattfinden -> BUG (FF)"
	- Vw.cs:
		- modif.: 'UpdateResultData()'
			- section "// Set the alarm switch in case of substance alarm" entspr. ge�ndert


	
JM,
16.8.10



================================
V3_3  (angelegt 8.9.10)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_4' 
)

- basierend auf: V3_2


JM,
8.9.10

- Doc.cs:
	- Device program version angepa�t

JM,
8.9.10


- Item: "BUG: im test-modus geht das PN nach Auswahl der datendatei nicht in die simulation, sondern
	stoppt die Sim. und zeigt eine error message"
	Ursache: falsche # of DI members: 6 are required, 5 are present ('nPresOnOff' nicht ber�cks.!)

	- Test.cs:
		- modif.: 'FillTestArray_Real()'
			- 'nPresOnOff' eingearb.
		- modif.: 'TestArray_SCANALLDATA_Real', 'TestArray_SCANALLDATA_Sim'
			- 'nPresOnOff' eingearb.

	
- Item: "Stromschleifen-Ausgabe einarb. (FF)"
	- ScriptData.cs:
		- modif.: class ScriptWindow
			- neu: member 'dUserVal' + eingearb.

JM,
8.9.10


- Item: "Stromschleifen-Ausgabe einarb. (FF) - Forts."

		- ScriptEditorWindowForm.cs:
			- neu: UI-member '_txtUserVal', member '_UserVal' + Einarbeitung

		
		- ScriptEditorForm.cs:
			- neu: UI-member 'colUserVal'
			- modif.: '_btnWindow_Add_Click()', '_btnWindow_Edit_Click()'
				- ScriptEditorWindowForm-member 'UserVal' eingearb.



JM,
9.9.10


- Item: "Script-commandos AOUTx (Current loop adjustment) einarbeiten."
	
	- ScriptCommand.cs:
		- class ScriptCommands:
			- global 'arsc[]' modif.:
				- neu: 'AOUTx' commands
			- modif.: 'HelpOnCmd()', 'HelpOnPar()'
				- 'AOUTx' commands eingearb.


- Item: "Die scriptEvent - Parameter, die via TB eingegeben werden, wurden bisher NICHT SAUBER auf G�ltigkeit
	�berpr�ft -> BUG!"

	- ScriptData.cs:
		- class ScriptEvent:
			- neu: 'CheckParameters()'
			- modif.: 'Check()'
				- section "// Check ... / TB commands:" eingearb.

	- ScriptEditorCmdForm.cs:
		- modif.: '_btnOK_Click()'
			- section "// Parameter / Via TB" ge�ndert (straffer gefa�t):
				- Routine 'CheckParameters()' eingearb.
	
	- ScriptCommand.cs:
		- class ScriptCommands:
			- global 'arsc[]' modif.:
				- modif.: GETOFFSET, SCANREADY, GOTO, KEY, -> f�r neue check-routine 'CheckParameters()' aufbereitet
				- Comment f. cmd's 'TEMPQUITALL' u. 'TEMPALL' angepasst




JM,
10.9.10


================================
V3_3_1  (angelegt 7.10.10)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_4' 
)

- basierend auf: V3_3

JM,
7.10.10

Wunsch von HW, 7.10.10: Die Untergrenzen f. d. N-sigma-Werte f�r die Spekrenanalyse (3 resp.6)
sollen 'rausgenommen werden, eintragbare Werte sollen >= 0 sein (Taiwan-Ger�te, Hauptrechner-Version: 3.4)
-> deshalb diese Unter-Version

	- ServiceForm.cs:
		- raus: '_txtPeakSearchNsig_TextChanged()' -> no longer needed
		- modif.: '_CheckControls()'
			- sections "// Peak search: 'n' sigma factor" und "// Peak area determination: 'n' sigma factor":
			  jeweils check auf >= 0 anstelle [6,100] resp [3,<x>] eingearb.
		- UI: Beschriftung der labels '_lblPeakSearchNsig' und '_lblPeakAreaDetNsig' ge�ndert:
		  	- jeweils '>= 0' eingearb.
		- String-Ressourcen entspr. modif.
			

JM,
7.10.10



================================
V3_6  (angelegt 9.3.11)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_6' 
)

- basierend auf: V3_3_1

JM,
9.3.11



- Item: "beim RX/TX der PID lamptime vom/zum device mu� ein scaling durchgef�hrt werdeN:
	 Grund: device - (10min) unit, PC - h unit"

	- ServiceForm.cs:
		- modif.: '_UpdateServiceData()'
			- Write/Read: sec. "PID Lamptime" eingearb.



- Item: "Die Untergrenzen f. d. N-sigma-Werte f�r die Spekrenanalyse (3 resp.6) wurden zwar
	 rausgenommen (Ver. 3_3_1), ABER das TestForm l��t nach wie vor nur die UG 6 f�r die peak det. zu
	 -> BUG!"

	- TestForm.cs:
		- modif.: 'Init()'
			- CB 'N-sigma peak detection' wird jetzt mit Werten von [0,20] gef�llt


- Item: "Service data: variable Breite des Gl�ttungsfilters einarb."

	- ServiceForm.cs:
		- modif.: UI / sec. "Section: Spectrum analysis"
			- Label & TB f�r Smoothing filter width eingearb.
			- entspr. String-Ressourcen eingearb.

		- modif.: '_UpdateServiceData()'
			- Read/Write: sec. "Section: Spectrum analysis" erweitert:
				- neu: Smoothing filter width eingearb.



JM,
9.3.11


- Item: "Forts.: smoothing: variable filter width einbauen"

	- ScanParams.cs:
		- neu: member 'bySmoothingFilterWidth'
	
	- Doc.cs:
		- modif.: 'BuildSmoothedScanArray()'
			- ge�ndert: sec "// Smoothing (based on adc units)"
				- neue smoothing method (m. var. breite) eingef�hrt

		- modif.: 'RecordScanDataWriteCommonHeader()'
			- neu: sec. "// Smooting filter width" eingearb.

		- modif.: 'FillDataStructures()'
			- neu: elseif zu "// Smoothing filter width" eingearb.

	- TestForm.cs:
		- neu: 4. Methode "Common case (Wrapping)" eingearb.

	- Test.cs:
		- neu: 'GetSmoothFilterWidth_Real()'
		- modif.: 'TestArray_SCANPARAMS'
			- sec. "// Get the Smoothing filter width from the test file, if any" eingearb.
			- zus�tzl. Par. 'Smoothing filter width' eingearb.

	- SpecEval.cs:
		- neu: 'sg_smooth()'
		- neu: 'sg_smooth_CalcSGCoeff()'
		- modif.: 'SmoothArray()'
			- method 4 eingearb.


JM,
9./10.3.11



- Item: "Scan offset determination: die (bisherige) Int16U-Def. f�r den Offset bereitet Probleme bei
	 negativem offset (theor., praktisch sollte es nicht vorkommen) -> deshalb: umdefinieren zu int"
	
	- DeviceInfo.cs:
		- umbenennung + typ modif.: member'wOffset' (Int16U, bisher) -> 'nOffset' (int, neu)
		  (entspr. Einarb. in code)

	- Diagnosis.cs:
		- modif.: 'Append()'
			- 'nOffset' eingearb.

	- Doc.cs:
		- modif.: 'RecordScanDataWriteCommonHeader()', 'FillDataStructures()'
			- 'nOffset' eingearb.


	- View.cs:
		- modif.: 'UpdateResultData()', '_OnPrintParams()'
			- 'nOffset' eingearb.



JM,
21.3.11


- Item: " mehrere Konz.-einheiten einarbeiten"

	- ScriptData.cs:
		- neu: class 'ConcUnits'


	- diese Klasse �berall wo erforderlich eingearb. (anstelle von "ppb" und "ppm"):
		- class ScriptWindow:
			- 'Default ()'
		- class ScriptEditorWindowForm:
			- Init. der ConcUnit-global  
			- 'Init()'
		- class Vw.cs:
			- 'OnInitialUpdate()', 'UpdateResultData()', '_OnPrintParams()'
		- class Test:
			- 'TestArray_SCANALLDATA_sim'

	- MainForm.cs:
		- UI angepa�t:
			- '_lblResUnit_x' controls verbreitert, damit der breiteste der ConcUnit-Strings
			  ('mg/cbm') reinpa�t


JM,
29.4.11


- Item: "Alarm handling"

	- ScanResult.cs:
		- umbennen: member 'dAlarm' (bisher) -> 'dA2' (neu)
	- (entspr. Umbenennung im gesamten app code)

	- ScanResult.cs:
		- neu: member 'dA1' 
	- (entspr. Einarbeitung im gesamten app code, dort wo auch A2)


	- ScriptCommands.cs:
		- modif.: ress. string 'ScriptCommands_HlpOnCmd_DISPMODE0/1': HI eingef�gt

	- Vw.cs:
		- modif.: 'UpdateResultData()'
			- sec. "// Choose the colors of the result TB, depending on the alarm case" ge�nd. 
			  (Layout und A1 eingef�gt)

	- Test.cs:
		- modif.: prop. 'TestArray_SCANALLDATA_sim'
			- sec. "// 3. part: Scan result:" entspr. erweitert

	- Scriptdata.cs:
		- class ScriptWindow:
			- umbenannt: 'dAlertLimit' -> 'dA2Limit'
			- neues member: 'dA1Limit'


- Item: "Forts.: Alarm handling"

	- ScriptEditorWindowForm.cs:
		- umbenannt: 'lblAlert' ->'lblA2', 'txtAlert' ->'txtA2', entspr. ressources ge�ndert
		- neu: 'lblA1', 'txtA1' + eingearb.

	- ScriptEditorForm.cs:
		- modif.: UI: 'lvWindows'
			- Spalte modif.: 'Alert' -> 'A2'
			- neue Spalte: 'A1'
		- modif.: '_btnWindow_Add/Edit_Click()'
			- umbenannt: 'f.Alert' -> 'f.A2'
			- 'f.A1' eingearbeitet


	- Scriptdata.cs:
		- class ScriptWindow:
			- umbenannt: 'dScaling' -> 'dSpanFac'


- Item: "Forts.: Alarm handling"
	(alarm data storage)

	- AppComm.cs:
		- neu: messages 'RDALFO', 'RDALFOS', 'RDALFI'

	- SDcardReaderForm.cs:
		- modif.: UI
			- neu: groupBox 'Alarm files on device' 
			- neu: RB 'Clear  device storage - Alarm'


		- neu: globals
			- '_sAlarmFolderPath', '_sAlarmSubFolderName'

		- modif.: enum 'DeviceFolderType'
			- neu: member 'Alarm'

		- neu: 'ResetAlarm()'

		- neu: ressourcen
			- 'SDcardReader_gpAlarm', 'SDcardReader_Err_NoAlarmSubFolder', 'SDcardReader_Que_ClearAlarm',
				'SDcardReader_Info_AlarmEmptied'
	
		- modif.: alle routines, in denen Alarm-related controls angesprochen werden




JM,
2.5.11


================================
V3_6_1  (angelegt 10.8.11)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_6_1' 
)

- basierend auf: V3_6

- Doc.cs:
	- Device program version angepa�t


JM,
10.8.11



================================
V3_6_2  (angelegt 25.8.11)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_6_2' 
)

- basierend auf: V3_6_1

- Doc.cs:
	- Device program version angepa�t


- Item: "Komm.-probleme im CombiNG:
	 Da wir bisher Komm-probleme mit BD115200 im Kombi-Verbund haben, wollen wir sehen, 
	 ob mit verlangsamter Komm. als embedded device (BD57600) die chose besser l�uft
	 (Schritt 1 der Korrekturen)"  

	- PropertiesForm.cs:
		- mod.: UI:
			- neu: Baudrate-CB

		- mod.: 'PropertiesForm_Load()', 'PropertiesForm_Closed()'
			- RS232 / Baudrate eingearb.

		- neu: 'Init()'


- Item: "Neue Produktinfo einf�hren: IUT (bisher) -> ENIT (neu)"

	- AboutForm.cs:
		- mod.: UI
			- Texte der entspr. label entspr. ge�ndert



JM,
25.8.11


================================
V3_6_3 (angelegt 30.8.11)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_6_3' 
)


- Doc.cs:
	- Device program version angepa�t


- Item: "Komm.-probleme im CombiNG:
	 Einarbeitung einer CRC16-Pr�fung in alle beteiligten codes.
	 (Schritt 2 der Korrekturen)"  


	- neu: System / CRC16.cs

	- neu: CommRS232 / Exceptions.cs

	- CommRS232.cs / class CommRS232:

		- neu: const's 'SZCOMMERROR_COMMAND', 'SZCOMMERROR_CMDPARSTR', 'SZCOMMERROR_CRC', 'SZCOMMERROR_BUF',
			'MAXCOMMERROR'

		- neu: global 'm_nErr', property 'ErrorCount'

		- modif.: 'ThreadProc()'
			- neu: sec. "// Before TX: Build the CRC16 from the message string, ..."
			- neu: sec. "// After RX: Calculate the CRC16 of the response byte array, ..."
			- neu: sec. "// Check, whether an error occurred on device"
			- neu: sec. "// Reset comm. error counter"

			- modif.: catch-Klausel:
				- neu: sec. "// CD: Exception type"

		- neu: 'BuildCRC16()', 'CRC16_ToByteArray()', 'CompareCRC16()', 'CRC16_FromByteArray()'

		- modif.: 'CloseChannel()'
			- try-catch-Block anders gestaltet: try umfa�t das gesamte 'Communication wait' dialog handling

		- modif.: 'OpenChannel()'
			- neu: sec. "        // Reset the comm. error counter"

		- mod.: 'WriteMessage()'
			- Limitation auf 'TXBUFSIZE-6' eingebaut (bisher: 'TXBUFSIZE-2')

		- mod.: 'WriteString()'
			- Parameter ge�ncert: 'string sWrite' (bisher) -> 'byte[] arbyWithCRC' (neu)
			- neu in diesem Zus.: sec. "// Provide the byte array to be transmitted with the comm. end char"
			- ersetzt: Debug.Assert() -> Debug.WriteLine() (3x)
		
		- mod.: 'ReadBytes_Multi()', 'ReadBytes_Single()'
			- 'TimeoutException' anst. von 'AppException' eingearb.
			- in allen Vorkommen von 'Debug.WriteLine()': trailing \n gel�scht


	- ScriptData.cs / class ScriptWindow:
		- mod.: 'Check()' / sec. "// Script window message string length check:"
			- Limitation auf 'TXBUFSIZE-6' eingebaut (bisher: 'TXBUFSIZE-2')


	- alle Forms, in denen ein 'OnRXCompleted()' Handler installiert ist:
		- mod.: 'OnRXCompleted()'
			- mod.: sec. "// A Timeout occurred:":
				- neu: sec. "// Close comm."
				- neu: Zeile 'dlg. ShowDialog()' (wurde fr�her vergessen)


	- MainForm.cs:
		- mod.: '_Timer_Tick()'
			- erweitert: sec. "// Statusbar: / Define panel texts / Panel1"
				- stuff bzgl. comm. errors hinzugef�gt
				- in diesem Zus.: neue Ressources: 'Main_Status_P1_CommError'

		- mod: UI / StatusBar control
			- neu: Eventhandler 'StatusBar_DrawItem ()'
			- mod.: prop. 'Panels' / 'StatusBarPanel2'
				- mod.: 'Style': Text (bisher) -> OwnerDraw (neu) 

		- neu: 'StatusBar_DrawItem()'
			

	- CommErrorForm.cs:
		- mod: Properties:
			- 'TopMost': false (bisher) -> true (neu)



JM,
30./31.8.11


- Item: "Testen - (Schritt 2 der Korrekturen) & entspr. Korrekturen"

	- CommRS232.cs / class CommRS232:

		- 'raus: const's 'SZCOMMERROR_COMMAND', 'SZCOMMERROR_CMDPARSTR', 'SZCOMMERROR_CRC', 'SZCOMMERROR_BUF',

		- mod.: 'MAXCOMMERROR': 3 (bisher)  -> 5 (neu)

		- modif.: 'ThreadProc()'
			- neu gefa�t: sec "// Check, whether an comm. error occurred on device"

		- modif.: 'CloseChannel()'
			- sec. "// Reset the comm. error counter" eingearb.


	- CommMessage.cs:

		- neu: const's 'SZCOMMERROR_COMMAND', 'SZCOMMERROR_CMDPARSTR', 'SZCOMMERROR_CRC', 'SZCOMMERROR_BUF',

		- neu: 'CheckForCommError()'

		- neu: member '_bPermanentyCalled' + eingearb.


	- Measure.cs:

		- mod.: 'OnCommEmpty()'
			- 'PermanentyCalled' property added


	- MainForm.cs:
	- ConnectForm.cs:
	- ScriptTransferForm.cs:

		- mod.: 'OnRXCompleted()' / sec. "// A Timeout occurred:"
			- ge�ndert: sec. "// React:"


	- GSMReaderForm.cs:
	- SDcardReaderForm.cs:
	- ServiceForm.cs:

		- neu: member '_nTimerCommError', '_bTimerCommError'

		- neu: member '_TimerCommError', '_TimerCommError_Tick()'

		- mod.: 'Load()'
			- neu: sec. "// Enable comm. error timer"

		- mod.: 'Closed()'
			- neu: sec. "stop comm. error timer"

		- mod.: 'OnRXCompleted()' / sec. "// A Timeout occurred:"
			- ge�ndert: sec. "// React:"


	- Doc.cs:
		- neu: member 'bReactOnCommError'

		- neu: 'OnCommError()', 'ReactOnCommError()'


	- MainForm.cs:
		- mod.: '_Timer_Tick()'
			- neu: sec. "// React on comm. errors, if any" eingearb.


	- ScriptTransferForm.cs:
		- mod.: 'OnRXCompleted()'
			- sec. "// Check: Appropriate form addressed?" in try-catch-Block eingeschlossen


	== B - �nderungen von 3.6.2 tw. r�ckg�ngig gemacht ==


	- PropertiesForm.cs:
		- mod.: 'Init()'
			- Visibility der baudrate-CB: false

	- AppData.cs:
		- mod.: 'AppData()'
			- sec. "// Secure, that the comm. Baudrate was set to 115 KB" eingearb.


	== E - �nderungen von 3.6.2 tw. r�ckg�ngig gemacht ==



- Item: "BUG: Wird eine Scan-datei von HD geladen und offline dargestellt, so stimmen die Peak-Y-Positionen
	 vom Graphpanel (Cursor-Y) und die angezeigten H�hen in der PeakData-LV NICHT �berein."

	 -> Grund: Speichern der PeakInfo-Information erfolg im Format F0 -> damit geht die Skal.-genauigkeit
		   verloren.


	- Doc.cs:
		- mod.: 'RecordScanDataWriteCommonHeader()'
			- Speicherung der H�he (%) in sec. "//    Peak info data": 
				0 Dezi-stellen (bisher) -> 2 Dezi-stellen (neu)

	- Vw.cs:
		- mod.: 'UpdatePeakInfo()'
			- Ausgabe der H�he in sec. "//    2. subitem: Height":
				0 Dezi-stellen (bisher) -> 2 Dezi-stellen (neu)



JM,
19.9.11


- Item: "(beinahe) BUG: Wird ein comm. error ausgel�st, so kann es vorkommen, da� der CommError-Dialog
			mehrfach ge�ffnet wird"

	-> Grund: Die Transmit-Liste kann mehrere cmd's enthalten, und solange die comm. nicht 
	 	  geschlossen wird, werden diese abgearbeitet, wobei jedes dieser cmd's einen error
		  generiert und damit der Dialog ge�ffnet wird.
		  Die L�sung besteht im Sperren (Schreibsperre) der Transmit list, nachdem ein comm. error 
		  aufgetreten ist, und anschl. clearen. 


	- CommRS232.cs / class CommRS232:

		- neu: member 'm_bTransmitListLocked'

		- mod.: 'ThreadProc()'
			- sec. "// Lock the message list for Write access & clear it" eingearb.

		- mod.: 'WriteMessage()', 'RepeatLastMessage()'
			- 'm_bTransmitListLocked' eingearb.

		- mod.: 'OpenChannel()'
			- sec. "// Indicate, that the message list is not locked for Write access" eingearb.


JM,
20.9.11


================================
V3_6_4 (angelegt 11.10.11)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_6_4' 
)


- Doc.cs:
	- Device program version angepa�t



JM,
11.10.11

- Item: "message 'Remote error confirmation' einarbeiten"

	- AppComm.cs:
		- neu: CommMessage msgErrConf 


JM,
17.10.11


================================
V3_7_0 (angelegt 25.1.12)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_7_0' 
)


- Doc.cs:
	- Device program version angepa�t



JM,
25.1.12


- Item: "1. Accu-Anzeige auf device ein-/ausblendbar gestalten,
	 2. Logo beim Ger�testart w�hlbar gestalten"


	- ServiceForm.cs:
		- neu: UI entspr. erweitert (section 'Device') 

		- neu: enum 'Logo'
		- modif.: '_UpdateServiceData()', '_ctl_HelpRequested()', '_CheckControls()', 'ServiceForm_Load()'


- Item: "MainForm / Device section: Error-ID's anzeigen"

	- MainForm.cs:
		- mod.: UI / sec. Device info
			- neu: controls zu 'Ger�tefehler' stuff

	- ScanInfo.cs:
		- neu: 'ErrorAsString()'

	- Vw.cs:
		- mod.: 'UpdateResultData()'
			- neu: sec. "// Ger�tefehler"

		- mod.: 'OnInitialUpdate()'
			- neu: sec. "// Ger�tefehler"


JM,
25.1.12


- Item: "Ger�te-Nr. im Hauptscreen mit anzeigen (sec. Device)"

	- ConnectForm.cs:
		- mod.: 'ConnectForm_Load()'
			- neu: sec. "// TX: Read service data (Device section)"

		- mod.: 'OnRXCompleted()'
			- neu: sec. "case "SVCREADDATA":"

			- in diesem Zus. einheitlich gefa�t hins. try-catch-Bl�cken:
				- neu: sec. "case "DATI":" 
				- mod.: sec. "case "LANGSYN":"


	- MainForm.cs:
		- mod.: UI / Device section
			- neu: controls 'lblDeviceNo' + Ressource 'Main_lblDeviceNo', 'lblDeviceNo_Val'


	- Vw.cs:
		- mod.: 'OnInitialUpdate()'
			- neu: sec.: "// Ger�te-Nr."
 


JM,
26.1.12


- Item: "Multiplexer einarb."

	- ScanInfo.cs:

		- neu: member 'byChan' + eingearb. in class context


	- Doc.cs:

		- mod.: 'RecordScanDataWriteCommonHeader()'
			- neu: sec. "// Row: Monitoring station" eingearb.


	- MainForm.cs:

		- mod.: '_Timer_Tick()'
			- mod.: sec. "Statusbar"
				- neu: sec. "//    Panel 3: Meas. channel" (Text def.) &
					sec. "//    Panel 3" (set text)

		- mod: UI / StatusBar control
			- mod.: prop. 'Panels'
				- neu: Panel '_StatusBarPanel4'


	- Test.cs:

		- mod.: 'TestArray_SCANALLDATA'
			- mod.: sec. "// 2. part: Scan info": meas. channel eingearb.


- Item: "Forts.: Multiplexer einarb.: Setzen/Lesen der Flows, Script-Kommando zum V�Z-Setzen f�r einen einzelnen
	 Kanal"

	
	- neu: ServiceMPForm.cs

	- TransferTask.cs:

		- mod.: enum TransferTask:
			- neu: member 'MP_Read', 'MP_Write'

	- MainForm.cs:

		- neu: menu item '_miTools_MP' + eingearb. (incl. Ressourcen)

	- CmdUI.cs:

		- neu: 'OnTransferMP()'

	- Doc.cs:

		- neu: member 'nMPChan' + eingearb. in code

	- ConnectForm.cs:

		- mod.: 'OnRXCompleted()' / sec. case "VER"
			- neu: sec. "// The # of MP channels (0, if MP not connected)"


	- ScriptCommands.cs / class ScriptCommands:

		- mod.: member 'arsc[]'
			- neu: 'MPSETCHANx' items

		- mod.: 'HelpOnCmd()'
			- neu: sec's "case MPSETCHANx"



- Item: "Der Peak search 'n' sigma factor und der Peak area determination 'n' sigma factor sollten 
	 bei PC storage mit im common header hinterlegt werden"


	- ScanParams.cs:
		- neu: member 'byNSigPeakSearch', 'byNSigAreaDetection' + eingearb. in class context

	- Doc.cs:
		- mod.: 'RecordScanDataWriteCommonHeader()'
			- neu: sec "// Peak search 'n' sigma factor",
				sec. "// Peak area determination 'n' sigma factor"
 

JM,
27.1.12


- Item: "Neue features einarb.: Script ID (Parameter)"

	- Scriptdata.cs:

		- modif.: class ScriptParameter
			- neu: member 'byScriptID' + eingearb.

		- modif.: class ScriptCollection:
			- neu: const 'ScriptID'
			- neu: 'Check()'
			- modif.: 'Load()'
				- sec. "// Checks at ScriptCollection level" eingearb.
			- modif.: 'Write()'
				- sec. "// Parameter" erweitert um ScriptID


	- Exceptions.cs:
		- neu: class ScrCollectionAmbiguousIDException


	- GSMReaderForm.cs:
		- modif.: 'OnRXCompleted()' / sec. "case "STRESPARAM"":
			- erweitert um ScriptID


	- ScriptEditorForm.cs:
		- erweitert: UI / Script parameter: ScriptID

		- modif.: 'ScriptEditorForm_Load():'
				- neue Ressourcen eingearb.


		- modif.: '_ContentsToScript', '_ScriptToContents()', '_ctl_HelpRequested()',
			  '_InitControls()'
			- Script parameter ScriptID eingearb.


	Common.cs:
		- modif.: 'Check()', enum 'CheckType'
			- type UInt32, UInt316 eingearb.




JM,
30.1.12



- Item: "ScanResults: Spanfaktor einbeziehen & an geigneter Stelle mit ausgeben (Spektrenaufzeichnung, 
	 im spez. Result-Dateien, Druckerausgabe)"


	- ScanResult.cs:
		- neu: member 'dSpanFac' + eingearb.

	- MainForm.cs:
		- modif.: 'OnRXCompleted()' / sec. 'SCANALLDATA':
			- sec. "// SCANRESULT data block" erweitert um member Span factor

	- Doc.cs:
		- modif.: 'RecordScanDataWriteCommonHeader()'
			- sec. "// Rows: Windows in running script": Span factor eingearb.

		- modif.: 'RecordResult()'
			- neu: Headerzeile 'SpanFac\t ...'
			- mod.: part Data / Substance results
				- bisher: result only
				- neu: result, including conc. incredible

	- Scriptdata.cs:
		- class ScriptWindow:
			-  neu: member 'dSpanFac' + eingearb. in class context

	- Vw.cs:
		- modif.: '_OnPrintParams()' / sec. 'Results'
			- Spalte Span factor eingearb.


	- Test.cs:
		- mod.: 'FillTestArray_Real()'
			- neu: sec. SR / span factor

		- mod.: 'TestArray_SCANALLDATA_Sim'
			- mod.: // 3. part: Scan result
				- span factor eingearb.


- Item: "Neue features einarb.: Span conc's (Parameter)"

	- Scriptdata.cs:
		- modif.: class ScriptParameter
			- neu: member 'arfSpanConc[]' + eingearb. in class context

		- modif.: class Script:
			- modif.: 'Check()'
				- sec. "// Check, whether the Span conc's (Parameter section) and the 
					substance windows (Windows section) are consistent or not" eingearb.

		- modif.: class ScriptCollection:
			- neu: const 'SpanConc'
			- modif.: 'Load()'
				- catch-branch:
					- 'ScrSpanConcException' check eingearb.
			- modif.: 'Write()'
				- sec. "// Parameter" erweitert um SpanConc


	- Exceptions.cs:
		- neu: class ScrSpanConcException


	- GSMReaderForm.cs:
		- modif.: 'OnRXCompleted()' / sec. "case "STRESPARAM"":
			- erweitert um SpanConc


	- neu: ScriptEditorSpanConcForm.cs
		- neu: class ScriptEditorSpanConcForm


	- ScriptEditorForm.cs:
		- erweitert: UI / Script parameter: Span conc's eingearb.

		- modif.: 'ScriptEditorForm_Load():'
				- neue Ressourcen eingearb.

		- neu: '_btnSpanConc_Add/Edit_Click()'

		- raus: '_btnCmd_Remove_Click()', '_btnWindow_Remove_Click()' nebst Ressourcen
		- daf�r neu: '_btnRemove_Click()' nebst Ressourcen

		- raus: '_lvCmd_DoubleClick()', '_lvWindow_DoubleClick()'
		- daf�r neu: '_lv_DoubleClick()'

		- modif.: '_ContentsToScript', '_ScriptToContents()', '_ctl_HelpRequested()',
			  '_Timer_Tick()', '_InitControls()'
			- Script parameter "Span conc's" eingearb.



- Item: "script editor: comment-TB f�r Parameter einarb."

	- ScriptEditorForm.cs:

		- mod.: UI / Comments:
			- neu: controls 'Parameter section '

		- modif.: '_ContentsToScript', '_ScriptToContents()'
			- Parameter comments eingearb.



- Item: "Zero-calibration (stat. scan offset) einarb."

	- class ScriptParameter:

		- neu: member 'byOffsetCorr' + eingearb. in class context

		- neu in diesem Zus.: 'IsValidOffsetCorr()'


	- class ScriptCollection:
		- mod.: 'Write()' / Section: Parameter
			- Store par. 'byOffsetCorr' (Static scan offset correction) eingearb.
		- neu: const 'OffsetCorr'


	- GSMReaderForm.cs:

		- mod.: 'OnRXCompleted()' / case "STRESPARAM"
			- par. 'byOffsetCorr' eingearb.


	- ScriptEditorForm.cs:

		- mod.: UI
			- neu: controls '_lblOffsetCorr', '_chkOffsetCorr' 

		- mod.: '_InitControls()', 'Load()', '_ctl_HelpRequested()'
			- controls eingearb. 
		- mod.: '_ContentsToScript()', '_ScriptToContents()'
			- controls eingearb. / par. 'byOffsetCorr' eingearb.

		- mod.: '_btnScript_Add_Click()'
			- sec. "// Check the script for validity" eingearb.


	- ServiceForm.cs:

		- mod.: UI
			- neu: controls '_lblStatScanOffset', '_txtStatScanOffset'

		- mod.: 'Load()', '_UpdateServiceData()' / sec. Read
			- controls eingearb. 


	- ScriptCommands.cs / class ScriptCommands:

		- neu: cmd 'GETSTATOFFSET'

		- entspr. mod.: 'HelpOnCmd()', 'HelpOnPar()'


- Item: "DOUT handling: script cmd 'DOUT' einarb."

	- ScriptCommands.cs / class ScriptCommands:

		- neu: cmd 'DOUT<x>'

		- entspr. mod.: 'HelpOnCmd()'





JM,
9.2.12


- Item: "Testen der SW -> Ausmerzen von BUGs"

	1) 
	- ScriptEditorForm.cs:
		- mod.: ressource 'ScriptEditor_Hlp_txtMeasCycle': ... s (bisher) -> ... 1/10 s (neu) 


	2)
	- ScriptEditorWindowForm.cs:
		- mod.: UI
			- neu: SpanFac - controls 
		- entspr. stuff eingearb. in class context (einschl. ressourcen)


	- ScriptEditorForm.cs:
		- mod.: '_btnWindow_Add/Edit_Click()'
			- SpanFac stuff eingearb.
		- mod.: _lvWindows / Columns collection
			- neu: Spanfac column


	3)
	- ScriptEditorWindowForm.cs:
		- mod.: 'Load()'
			- ressource f�r UserVal eingearb. 
		- mod.: ressource  'ScriptEditorWindow_Hlp_txtPx': ... fl�che (bisher) -> ... h�he/fl�che (neu)


	4)
	- ScanInfo.cs:
		- mod.: '_NO_SCANINFO_MEMBERS': 5 (bisher) -> 6 (neu)



JM,
15.2.12



================================
V3_7_1 (angelegt 13.2.12)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_7_1' 
)


- Doc.cs:
	- Device program version angepa�t



JM,
13.2.12


- Item: "Analytische Methoden verbessern: Peak search, Peakfl�chenbestimmung"


	- neu: Dateien Nru_ad.cs, SpevRout_ad.cs, FakeSpectrum.cs,
	       Dateien Nru.cs, SpevRout.cs


	- mod.: SpecEval.cs

		- mod.: 'DetectPeaksEx()'
			- neu: sec. "// New version: Begining from version 3.7.1"

		(==> peak search:)

		- umbenannt: '_FindPeaksEx_I()' -> '_FindPeaksEx_I_Unidirectional()'
		- neu: '_FindPeaksEx_I_Bidirectional()'

		(==> peak area det.:)

		- neu: 'PeakAreaDetectEx_Combined()', 'ml_nru()', 'ml_nru_ad()', 'ml_sr()', 'ml_sr_ad()', 
			'svd_direct()', 'svd_fit()'



- Item: "Forts.: Analytische Methoden verbessern: Smoothing"
	(hier nur angearbeitet, NICHT implementiert!)

	- mod.: SpecEval.cs

		- neu: 'ma_smooth()'



JM,
13.2.-2.3.12




================================
V3_7_2 (angelegt 6.3.12)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_7_2' 
)


- Doc.cs:
	- Device program version angepa�t



JM,
6.3.12


- Item: "Forts.: Analytische Methoden verbessern: Smoothing"

	- ScanParams.cs:
		- neu: global 'bySmoothingType' + eingearb. in class context


	- Doc.cs:
		- mod.: 'RecordScanDataWriteCommonHeader()'
			- mod.: sec. "// Smoothing"

		- mod.: 'BuildSmoothedScanArray()'
			- mod.: sec. "// Smoothing"

		- mod.: 'FillDataStructures()'
			- mod.: sec. "// Smoothing"
			- neu: sec. "// nSig - peak search"
			- neu: sec. "// nSig - area det."

			- mod.: sec. "// Check: If the 'Smoothing' entry was NOT found, ..."
			- neu: sec. "// Check: If the 'NSigma (Peaksearch)' entry was NOT found, ..."
			- neu: sec. "// Check: If the 'NSigma (Areadetection)' entry was NOT found, ..."


	- ServiceForm.cs:
		- mod.: UI
			- mod.: sec. spectrum analysis data
				- neu: groupbox 'Smoothing' + included controls

		- mod.: 'Load()'
			- neu: sec. 'Smoothing' + zugeh. ressources

		- mod.: '_CheckControls()'
			- neu: sec's "// Smoothing: MA/SG-Filter width" + resources

		- mod.: '_UpdateServiceData()'
			- Smoothing data in R/W eingearb.




	- Test.cs:
		- neu: member 'Enabled'

		- mod.: 'GetSmoothFilterPar_Real()'
			- Parameterliste erweitert
			- mod.: sec. "// Smoothing"
			- neu: sec. "// nSig - peak search"
			- neu: sec. "// nSig - area det."

			- mod.: sec. "// Check: If the 'Smoothing' entry was NOT found, ..."
			- neu: sec. "// Check: If the 'NSigma (Peaksearch)' entry was NOT found, ..."
			- neu: sec. "// Check: If the 'NSigma (Areadetection)' entry was NOT found, ..."

		- mod.: 'TestArray_SCANPARAMS'
			- Smoothing-/Peak-/PeakArea-Params eingearb.


	- TestForm.cs:
		- mod.: UI
			- neu: MA controls

		- MA controls in class context eingearb.


	- neu: Test1.cs, Test1Form.cs


	- MainForm.cs:
		- mod.: '_TimerTest_Tick()'
			- Test-enabled eingearb.


	- CmdUI.cs:
		- mod.: 'OnTest()'
			- 'Test1' stuff eingearb.

	- SpecEval.cs:
		- neu: 'SmoothArray (int[] array, ref int[] array_sm, int nPoints, int nFilterWidth, int nMethod)'
		- mod.: 'SmoothArray(ref int[] array, int nFilterWidth, int nMethod)'
			- 'MA' eingearb.



JM,
6.-7.3.12


- Item: Span calibration besser einarb."
	-> neu: es wird ein neuer script befehl eingearb.: "CNT"


	- ScriptCommands.cs / class ScriptCommands:

		- mod.: global 'arsc'
			- neu: member "CNT" + eingearb. in class context

		- neu: 'CheckParameters()'


	- ScriptData.cs / class ScriptEvent:

		- mod.: 'CheckParameters()'
			- neu: sec. "// Perform a special parameter check"



- Item: =BUG= "script editor: Sichern, da� im Falle eines script cmd's mit mehreren Parametern (TEMPALL, CNT, ...) 
	 das Schreiben/Lesen der cmd-ListView control sauber gehandelt wird"

	- ScriptEditorForm.cs:

		- neu: '_LimitCmdItemNo()'

		- mod.: '_ScriptToContents()' / Section: Commands
			- neu: sec. "// Secure, that the LV item to be build contained exactly 3 subitems"




JM,
20.3.12	



- Item: "CTS-Handling einarbeiten"

	- neu: CTSForm.cs, CTSElementForm.cs nebst Ressourcen

	- neu: Ordner Images/Dialogs/CTS
		- neu: Up.bmp, Down.bmp

	
	- Doc.cs:
		- neu: const 'MAX_CLOCKTIMEDSCRIPTS'


	- TransferTask.cs:
		- mod.: enum 'TransferTask'
			- neu: members 'CTS_Read', 'CTS_Write'

	- AppComm.cs:
		- neu: members 'msgCTSRead', 'msgCTSWrite'


	- MainForm.cs:
		- mod.: UI / Manu
			- neu: menuitem '_miTools_CTS' + eingearb. in class context

		- mod.: '_mi_Click()'
			- neu: sec. '// Transfer clock-timed script data ...'

		- mod.: '_mi_Popup()'
			- neu: sec. '// Transfer clock-timed script data ...'

		- mod.: 'UpdateCulture()'
			- mod.: sec. '// MainMenu: Items'
				- Transfer clock-timed script data eingearb.

			- mod.: sec. '// Assign status messages to menu items:'
				- Transfer clock-timed script data eingearb.



	- CmdUI.cs:
		- neu: 'OnTransferCTS()'




JM,
4.4.12	


- Item: =BUG= "Bei Testl�ufen mit dem SpanCalib-script schnippte ab und an das Comm-error-fenster auf (ohne
	       "Voranmeldung" in der Statuszeile) -> Grund?"
		Grund: ArgumentNullExc. wurde generiert, nachdem 'ERRBUF' vom device empfangen wurde
		       (dies passierte bei Ausf�hrung der GetString-Anweisung in OnRx...() nach Erhalt
			von arbyPar=NULL)


	- CommMessage.cs:

		- mod.: 'ProcessAnswer()'
			- ge�ndert: bisher: _arParAnswer = null
				    neu:    _arParAnswer =  new byte[0]; 		




JM,
10.4.12



================================
V3_7_3 (angelegt 10.4.12)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_7_3' 
)


- Doc.cs:
	- Device program version angepa�t



JM,
10.4.12



================================
V3_8_0 (angelegt 11.4.12)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_8_0' 
)


- Doc.cs:
	- Device program version angepa�t



JM,
11.4.12


- Item: "wenn CTS handling auf dem Ger�t l�uft, so darf kein PC-script gestartet werden"

	- Doc.cs:

		- neu: member 'bCTSRunning'


	- ConnectForm.cs:

		- mod.: 'ConnectForm_Load()'
			- neu: sec. "// TX: CTS Read"

		- mod.: 'OnRXCompleted()'
			- neu: sec. "case "CTSREAD""

	- CTSForm.cs:

		- mod.: 'OnRXCompleted()'
			- mod.: sec. "case "CTSWRITE""
				- neu: sec. "// Update the 'CTS handling enabled?' indication"


	- MainForm.cs:

		- mod.: '_UpdateToolbarButtons()'
			- mod.: sec. "// Execute script"
				- neu: sec. "// Disabled, if CTS handling on the device is enabled"

		- mod.: '_mi_Popup()'
			- mod.: sec. "// Execute script"
				- neu: sec. "// Disabled, if CTS handling on the device is enabled"



JM,
16.4.12

	
- Item: =BUG= "Data storage: Wenn bei 'Scans cont.' und 'Results cont' die H�kchen rausgenommen werden,
		so erscheint KEINE Messagebox"
		- Grund: da nach der 2048-ScanAllData-�bertragung nur noch NODATA �bertragen wird,
			 wird 'RecordData()' nicht mehr angesprungen; somit kann auch keine Messagebox
			 gezeigt werden (die wird ja dort generiert)
		- L�sung: 'RecordData()' auch im NODATA-Zweig des ScanAllData-RX verankern 


	- MainForm.cs:

		- mod.: 'OnRXCompleted()' 
			- mod.: case SCANALLDATA / NODATA branch
				- neu: sec. "// Ensure, that at this point the Data recording routine was included"


	- Doc.cs:

		- mod.: 'RecordData()'
			- neu: if-Schleife zu "// Check: Did a storage take place?" (2 mal)
			


JM,
17.4.12


- Item: "Sauberes Handling der Form-methoden, die im Context des comm. threads ausgef�hrt werden,
	 erforderlich (Stichwort: Invoke)"


	- CTSForm.cs:

		- neu: 'UpdateCTSDataDelegate'
		- mod.: '_UpdateCTSData()'
			- neu: sec. "// Invocation required?"

		- mod.: '_EndTransfer()'
			- neu: sec. "// Invocation required?"


	- GSMReaderForm.cs:

		- mod.: 'UpdateData()'
			- neu: sec. "// Invocation required?"

		- mod.: '_EndTransfer()'
			- neu: sec. "// Invocation required?"


	- SDcardReaderForm.cs:

		- neu: 'UpdateDataDelegate'
		- mod.: 'UpdateData()'
			- neu: sec. "// Invocation required?"

		- mod.: '_EndTransfer()'
			- neu: sec. "// Invocation required?"


	- ServiceForm.cs:
	- ServiceMPForm.cs:

		- mod.: 'UpdateData()'
			- neu: sec. "// Invocation required?"

		- neu: 'UpdateServiceDataDelegate'
		- mod.: '_UpdateServiceData()'
			- neu: sec. "// Invocation required?"

		- mod.: '_EndTransfer()'
			- neu: sec. "// Invocation required?"



JM,
18.4.12


- Item: "Nach dem Clearen eines root folders m�ssen auch entsprechende Eintr�ge aus der 
	 'Device files to be transferred' ListBox gel�scht werden" 


	- SDcardReaderForm.cs:

		- neu: 'DeleteAddedEntries()' 

		- mod.: 'ResetData/Alarm/Scan/Script()'
			- neu: sec. "// Delete all entries that refer to the ... root folder from the 
				     'lbAddedFiles' ListBox"



JM,
19.4.12



- Item: "SDcard Reader Form: Der Clear progress bar wird NACH L�schen von Ordnern nicht resettet."

	- SDcardReaderForm.cs:

		- mod.: 'OnRXCompleted()'
			- mod.: case "REMFO"
				- neu: sec. "// Reset Clear progress bar"
	


- Item: =BUG= "SDcard Reader Form: Logisch fehlerfahfte Init. des Clear progress bar"

	- SDcardReaderForm.cs:

		- mod.: 'ResetAll()'
			- mod.: sec. "// Section 'Clear'"
				- bisher: this.pgbClear.Value = this.pgbTransfer.Minimum;
				- neu:    this.pgbClear.Value = this.pgbClear.Minimum;

				

JM,
20.4.12


================================
3.8.1 (angelegt 26.4.12)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_8_0' 
)


based on: 3.8.0

JM,
26.4.12


- Item: "Sauber fassen: React on comm. errors"
	- Grund: ... Probleme in der bisherigen Fassung ...


	- Doc.cs:

		- neu: member 'ActiveForm'
		- neu gefa�t: 'ReactOnCommError()'
		- raus: 'OnCommError()'


	- MainForm.cs:
	- ConnectForm.cs:
	- ScriptTransferForm.cs:

		- mod.: 'OnRXCompleted()'
			- neu gefa�t: sec. "// A Timeout occurred"


	- CTSForm.cs:
	- GSMReaderForm.cs:
	- SDCardReaderForm.cs:
	- ServiceForm.cs:
	- ServiceMPForm.cs:

		- raus: globals '_nTimerCommError', '_bTimerCommError'
		- raus: component '_TimerCommError' einschl. routine '_TimerCommError_Tick()'

		- mod.: 'OnRXCompleted()'
			- neu gefa�t: sec. "// A Timeout occurred"


	- CommErrorForm.cs:

		- neu: #tor 'public CommErrorForm (Form fOwner)'



JM,
26.4.12


================================
3.9.0 (angelegt 16.5.12)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_9_0' 
)


based on: 3.8.1

JM,
16.5.12


- Item: "In allen von der PC-SW generierten files soll die Ger�tenummer hinterlegt werden"


	- Doc.cs:

		- neu: member 'sDevNo'
		- mod.: 'RecordScanDataWriteCommonHeader()'
			- neu: sec. "// Row: Device number"


	- ConnectForm.cs:

		- mod.: 'ConnectForm_Load()'
			- mod.: sec. "// TX: Read service data (Device section)"
				- neu: Zeile 'app.Doc.sDevNo = "";'

		- mod.: #OnRXCompleted()'
			- mod.: case "SVCREADDATA"
				- 'sDevNo' eingearb.



JM,
16.5.12



- Item: "1. Optionale Zeile im script file (ganz oben): Ger�te-Nr.
	 2. Abgleich Ger�te-Nr. Scriptdatei <-> device vor �berspielen"

	- CmdUI.cs:

		- mod.: 'OnTransmitConfigData()'
			- neu: sec. "// Check: Is there a 'Device No.' Top line present in the script configuration file?"



	- ScriptData.cs / c. ScriptCollection:

		- neu: member 'sDevNo'

		- neu: const. 'DevNo', 'chTopBegin/End', 
		- mod.: enum typeModi
			- neu: member 'typeTop'

		- mod.: 'Reset()'
			- 'sDevNo' eingearb.

		- mod.: 'Load()'
			- mod.: sec. "// CD: Line mode"
				- neu: case (int) typeModi.typeTop

		- mod.: 'Write()'
			- neu: sec. "// Top"

		- mod.: 'IdentifyString()', 'ExtractString()', 'FormatString()'
			- case Top eingearb.



	- ScriptEditorForm.cs:
	
		- mod.: UI
			- neu: 'Script file binding to device no.' GroupBox + included controls


		- mod.: 'Load()'
			- mod.: sec. "Resources"
				- neu: Ress. f�r 'Script file binding to device no.' GroupBox


		- mod.: '_ctl_HelpRequested()'
			- neu: sec. "// Script file binding to device no." + Ressources


		- mod.: '_ScriptsToContents()'
			- neu: sec. "// Update the 'Script file binding to device no.' TB "


		- mod.: '_btnSave_Click()'
			- neu: sec. "// Update the 'Device no.' Top line of the script collection"



JM,
16./18.5.12



- Item: "Nachtrag: Init. in Script editor unvollst�ndig (Nicht sch�dlich!)"

	- ScriptEditorForm.cs:

		- mod.: '_InitControls()'
			- mod.: sec. "// Comments"
				- neu: Zeile 'this._txtCmt_Par.Text = "";'



- Item: "Service data auch als Datei auf der SD-Karte speichern"

	- AppComm.cs:

		- neu: members 'RDSVCFO', 'RDSVCFI', 'EYSVCFO'
		- neu: Notes zu SDcardReader messages


	- SDcardReaderForm.cs:

		- mod.: UI:
			- neu: GroupBox 'Service files on device' + included controls
			- mod.: GroupBox 'Clear device storage'
				- neu: RB 'Service' 

		- mod.: enum 'DeviceFolderType'
			- neu: member 'Service'

		- neu: member '_sServiceFolderPath'

		- neu: 'ResetService()'

		- mod.: 'ResetAll()'
			- neu: sec. "// Section 'Service'"

		- mod.: 'Load()'
			- neu: sec. "Service" GroupBox ((einschl. Ressource 'SDcardReader_gpService')


		- mod.: 'UpdateData()'
			- Service controls eingearb.

		- mod.: 'SetClearEnable()'
			- Service eingearb.


		- mod.: 'RequestDeviceRootFolders()'
			- neu: sec. "// Read Service root folder path: "RDSERFO\r""


		- mod.: 'btnRead_Click()'
			- neu: sec. "// The 'Service files - Read' Button was pressed:"


		- mod.: 'btnSelect_Click()'
			- mod.: sec. "// Get the ListBox corr'ing to the Select Button control"
				- Service eingearb.


		- mod.: 'lbFiles_SelectedIndexChanged()'
			- mod.: sec. "// Get the Select Button corr'ing to the ListBox control"
				- Service eingearb.


		- mod.: 'btnAdd_Click()'
			- neu: sec. "// Add selected Service files"



		- mod.: 'rbClear_CheckedChanged()'
			- mod.: sec. "// Set the device folder type"
				- Service eingearb.


		- mod.: 'btnClear_Click()'
			- mod.: sec. "// Safety request: Should the root folder really be emptied?"
				- Service eingearb. (einschl. Ressource 'SDcardReader_Que_ClearService')

 			- mod.: sec. "// Clear the subfolders on device"
				- Service eingearb.


		- mod.: 'OnRXCompleted()'

			- neu: case "RDSVCFO"
			- neu: case "RDSVCFI"
			- neu: case "EYSVCFO" (einschl. Ressource 'SDcardReader_Info_ServiceEmptied')

			- 'Remarks' aktualisiert
			


JM,
18.5.12


- Item: "Forts.: Service data auch als Datei auf der SD-Karte speichern:
		In der LB 'Service files' des SDcardReader-Dialogs sollten die Dateien sortiert
		(zeitlich aufsteigend bzgl. ihrer creation) angezeigt werden."


	- SDcardReaderForm.cs:

		mod.: 'Init()'
			- neu: sec. "      // Assign the 'Sorted' style to the Service files LB:"



JM,
25.5.12



- Item: "Beim Laden (u. nachfolg. Darstellen) eines scan files von HD werden die ScanResult-label
	 willk�rlich bzgl. eines Alarmzustandes coloriert -> BUG"
	 Grund: die scan files enthalten keine Alarm info. Der Abgleich auf Alarmzustand erfolgt mit
		den default alarm values, und das ist nat�rlich buggy.
	 L�sung: fiktive Alarmwerte beim laden zuweisen (am besten so ,da� kein Alarmzustand gemeldet wird).


	- Doc.cs:

		- mod.: 'FillDataStructures()'
			- mod.: SR (scan result) data rows  
				- neu: sec. " // Depose fictive alarm values"


- Item: "Gleiches gilt f�r den kompletten offline-Test vermittels Test.cs und Test1.cs -> BUG"

	- Test.cs
	  Test1.cs:

		- mod.: 'FillTestArray_Real()'
			- mod.: SR (scan result) data rows  
				- neu: sec. " // Depose fictive alarm values"
		



JM,
29.5.12



================================
3.10.0 (angelegt 31.7.12)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V3_10_0' 
)


based on: 3.9.0

JM,
31.7.12


- Item: "Flow sensor/check einarb."

	- ServiceForm.cs:

		- mod: UI
			- raus: Buttons '_btnF1_Fix/Zero', '_btnF2_Fix/Zero'

			- mod.: TB's '_txtF1/2_Soll': readonly weggenommen
			- neu: TB's '_txtF1/2_Soll_TolAbs'


		- raus: global '_bFlowAdjustInProgress'

		- raus: '_btnFlow_Click()'

		- mod.: 'ServiceForm_Closing()'
			- raus: sec. " // Check: Is a 'Flow adjust' Transfer process still in progress?"
				nebst zugeh. ressources

		- mod.: 'OnRXCompleted()
			- raus: sec. "case "SVCADJUSTFLOW":" nebst zugeh. ressources

		- mod.: '_UpdateServiceData()'
			- mod.: sec. Read / Sensor
				- neu: sec. "//      Flow"
			- mod.: sec. Write / Sensor
				- mod.: '_arsServiceData[2]': F1/2 eingearb.

		- mod.: '_CheckControls()'
			- neu: sec's "// F1", "// F2"

		- mod.: 'Load()'
			- raus: ressources f�r die 4 Flow-Buttons

		- mod.: '_ctl_HelpRequested()'
			- raus: sec. "// 4 Flow Buttons" nebst ress. 'Service_Help_btnFlow'

		- mod.: 'UpdateData()'
			- raus: sec. "// The 4 Flow Button controls"

		- raus: enum FlowAdjustTask

	- AppComm.cs:

		- raus: global 'msgServiceAdjustFlow'


- Item: "Falls eine Scriptdatei mehr Scripte enth�lt als zul�ssig, 
	ODER falls ein Script dieser Datei mehr Fenster enth�lt als zul�ssig,
	ODER falls ein Script dieser Datei mehr Befehle enth�lt als zul�ssig,
	so sollte beim Laden dieser Datei eine entspr. Info an den User ausgegeben werden."


	- ScriptData.cs / class Script:

		- neu: member 'MaxWindowNoExceeded', 'MaxCmdNoExceeded'

		- mod.: 'Reset()'

			- neu: sec. "// Indicators: Max. item number exceeded"

		- mod.: 'CopyTo()'
			- neu: sec. "// Indicators: Max. item number exceeded"



	- ScriptData.cs / class ScriptCollection:

		- neu: member 'MaxScriptNoExceeded'

		- mod.: 'Reset()'

			- neu: 'MaxScriptNoExceeded' eingearb.

		- mod.: 'Load()'

			- mod.: ret-Typ: bool (bisher) -> void (neu)

			- neu: Parameter 'sInfo'

			- raus: local 'bLimitReached'

			- mod.: sec. "// Group"
				- neu: sec. "// Indicate, that the number of scripts exceeds the permitted max. number"

			- mod.: sec. "// Windows item"
				- neu: sec. "// Indicate, that the number of script windows exceeds the permitted max. number"

			- mod.: sec. "// Command item"
				- neu: sec. "// Indicate, that the number of script commands exceeds the permitted max. number"

			- neu: sec. "// Part 2: Info string building" nebst ressources



	- Doc.cs:

		- mod.: 'LoadScript()'

			- entspr. mod.: sec. "// Load the script file" nebst ress.


	- ScriptEditorForm.cs:

		- mod.: '_btnLoad_Click()'

			- entspr. mod.: sec. "// Load a script collection from file"



- Item: "RS232 comm. �berarb.: 
	 - bisher: RX DEzentralisIert (in den betroffenen Dialogen),
	 - neu: RX zentralisert (in Main) + Invoke-umh�llt, 

	 -> goal: handeln der messages im Main thread, NICHT mehr im comm thread (mit den bekannten
		Debug-Problemen ...)
	"

	- Doc.cs:

		- raus: globals 'bReactOnCommError', 'ActiveForm'
		- raus: 'ReactOnCommError()'

	- MainForm.cs:

		- mod.: 'OnRXCompleted()'
		- neu: 'HandleRX()', delegate 'HandleRXDelegate'
		- neu: 'AfterRXCompleted()'

		- mod.: '_Timer_Tick()'
			- raus: sec. "// React on comm. errors, if any"


	- ServiceForm.cs:
	- ServiceMPForm.cs:

		- raus: global '_crh'
		- entspr. mod.: 'Load()', 'Closed()', '#tor' resp. 'Init()'
			- '_crh'-Stuff raus

		- mod.: 'UpdateData()', 'EndTransfer()'
			- raus: sec. "Invocation required?"


		- raus: 'UpdateServiceDataDelegate()'
		- mod.: 'UpdateServiceData()'
			- raus: sec. "Invocation required?"

		- raus: 'OnRXCompleted()'
		- daf�r neu: 'AfterRXCompleted()'
			- based on: 'OnRXCompleted()'


	- SDcardReaderForm.cs:

		- raus: global '_crh'
		- entspr. mod.: 'Load()', 'Closed()', 'Init()'
			- '_crh'-Stuff raus

		- raus: 'UpdateDataDelegate()'
		- mod.: 'UpdateData()', 'EndTransfer()'
			- raus: sec. "Invocation required?"

		- raus: 'OnRXCompleted()'
		- daf�r neu: 'AfterRXCompleted()'
			- based on: 'OnRXCompleted()'


	- GSMReaderForm.cs:

		- raus: global '_crh'
		- entspr. mod.: 'Load()', 'Closed()', #-tor
			- '_crh'-Stuff raus

		- mod.: 'UpdateData()', 'EndTransfer()'
			- raus: sec. "Invocation required?"

		- raus: 'OnRXCompleted()'
		- daf�r neu: 'AfterRXCompleted()'
			- based on: 'OnRXCompleted()'


	- CTSForm.cs:

		- raus: global '_crh'
		- entspr. mod.: 'Load()', 'Closed()',  #-tor
			- '_crh'-Stuff raus

		- raus: 'UpdateCTSDataDelegate()'
		- mod.: 'UpdateCTSData()', 'EndTransfer()'
			- raus: sec. "Invocation required?"

		- raus: 'OnRXCompleted()'
		- daf�r neu: 'AfterRXCompleted()'
			- based on: 'OnRXCompleted()'


	- ConnectForm.cs:
	- ScriptTransferForm.cs:

		- raus: global '_crh'
		- entspr. mod.: 'Load()', 'Closed()',  #-tor
			- '_crh'-Stuff raus

		- raus: 'OnRXCompleted()'
		- daf�r neu: 'AfterRXCompleted()'
			- based on: 'OnRXCompleted()'


	- ServiceForm.cs:
	- ServiceMPForm.cs:
	- SDcardReaderForm.cs:
	- GSMReaderForm.cs:
	- CTSForm.cs:

		- mod.: 'UpdateData()'
			- public (bisher) -> private (neu)


	- CommRS232.cs:

		- mod.: 'm_eReadMode'
			- raus aus const's, rein in members	

		- raus: 'ClearMessageList()' -> not needed



- Item: "Vereinheitlichen des SCRLIST-Empfangs, dabei Korrektur eines kleinen BUG's (wenig sch�dlich):
	 in die Script list d�rfen nur nicht-leere Scriptnamen aufgenommen werden."

	- Doc.cs:
		- neu: 'UpdateScriptList()'
	

	- MainForm.cs:
	- ConnectForm.cs:
	- ScriptTransferForm.cs:

		- mod.: 'AfterRXCompleted()'
			- mod.: case SCRLIST
				- 'UpdateScriptList()' eingearb.



- Item: "Summensignal einarb."

	- ScriptCommands.cs:
		- modif.: global 'arsc': neues script cmd 'SUMSIGNAL' eingef�gt
		- modif.: 'HelpOnCmd()', 'HelpOnPar()'
			- case 'SUMSIGNAL' eingearb.


	- ScriptData.cs/class Script:
		- modif.: 'Check()'
			- sec. "Check, whether a script with summary signal detection contains max. 'MAX_SCRIPT_WINDOWS' 
      				substance windows or not" eingearb.

		- neu: 'ConsiderSumSignal_NumWindows()'

	- Exceptions.cs:
		- neu: exc. 'ScrSumSignalNumWindowsException'



- Item: "Der Bezeichner f�r das SumSignal soll user-settable gemacht werden (bisher: fixed
	 via '_sDRB_SumSignal[]') (SB, 25.6.12)"


	- ScriptData.cs / class ScriptParameter:
		
		- neu: const 'MAXLENGTH_TOTAL'
		- neu: member 'sTotal' + eingearb. in class context
		- neu in diesem Zus.: 'IsValidTotal()'


	- ScriptData.cs / class ScriptCollection

		- neu: constant 'Total'
		- mod.: 'Write()'
			- mod.: sec. "// Section: Parameter"
				- 'sTotal' eingearb.


	- GSMReaderForm.cs:

		- mod.: 'OnRXCompleted()' / case "STRESPARAM"
			- Par. 'sTotal' eingearb.


	- ScriptEditorForm.cs:

		- mod.: UI
			- neu: members '_lblTotal', '_txtTotal'

		- mod.: 'Load()'
			- mod.: sec. "// Resources"
				- neu: '_lblTotal'-Ressource

		- mod.: '_btnScript_Add_Click()'
			- neu: Total-Parameter check

		- mod.: '_ctl_HelpRequested()' / sec. "// Script parameter"
			- neu: Ressource f�r Total-Helptxt

		- mod.: '_ContentsToScript()', '_ScriptToContents()', '_InitControls()'
			- mod.: sec. "// Section: Parameter"
				- neu: Total-Einbindung 



- Item: "Sichern, da� Scripte mit Summensignal-Ermittlung einen 'Total'-Parameter beinhalten"

	- Exceptions.cs:

		- neu: class 'ScrSumSignalTotalIdentifierException'


	- ScriptData.cs / class ScriptCollection:

		- mod.: 'Load()' / catch-branch
			- 'ScrSumSignalTotalIdentifierException' eingearb.


	- ScriptData.cs / class Script:

		- neu: 'ConsiderSumSignal_TotalIdentifier()'

		- mod.: 'Check()'
			- neu: sec. "// Check, whether a script with summary signal detection doesn't contain 					a summary signal identifier paramter row or not"



- Item: "Das script cmd VALVE2 wird in der device SW explizit nicht mehr verwendet, und mu� folglich
	 aus der cmd-Liste entfernt werden"


	- ScriptCommands.cs:

		- mod.: global 'arsc[]'
			- raus: item "VALVE2"



- Item: "Menu item '&Load and display scan from HD ...': Lastly loaded & displayed scan file in registry
	 speichern"


	- AppData.cs:

		- neu: const 'sDisplayScanFile'

		- mod.: class Common
			- neu: 'sDisplayScanFile'

		- mod.: '_ReadRegistry()', '_ReadResourceFile()', '_WriteRegistry()', '_WriteResourceFile()'
			- 'sDisplayScanFile' eingearb.


	- MainForm.cs:

		- mod.: 'Load()', 'Closed()'
			- neu: sec. "// lastly loaded & displayed scan file"



- Item: "'Connect'-Funktionalit�t klarer fassen"


	- CmdUI.cs:

		- mod.: 'OnConnectDevice()'
			- Funktionsk�rper klarer gefa�t (bei Beibehaltung der Funktionalit�t)


	- ConnectForm.cs:

		- mod.: 'ConnectForm_Load()'
			- Funktionsk�rper klarer gefa�t (bei Beibehaltung der Funktionalit�t)
			- neu: ress. 'Connect_CommNotAccessible' eingearb. 

		- mod.: '_Timer_Tick'
			- Funktionsk�rper klarer gefa�t (bei Beibehaltung der Funktionalit�t)
			- in diesem Zus.:
				- raus: const '_max_nCheckCount'
				- daf�r neu: const '_2nd_nCheckCount'



- Item: "Load script: error msg. detaillierter fassen"

	- ScriptData.cs / class Script:

		- mod.: 'Check()'
			- mod.: sec. "// Checks, whether a substance name is unique or not"
				- error msg: Substanznamen hinzugef�gt



- Item: "Load script: error-Ausschrift �bersichtlicher fassen"


	- ScriptData.cs / class ScriptCollection:

		- mod.: 'Load()'
			- mod.: catch-branch
				- empty lines zwischen den error-Bl�cken eingef�gt 
				(�bersichtlichere Gliederung)

		- mod.: ress. 'cScriptCollection_Load_Err_Basic'
			- 'Datei' weggelassen (doppelt vorh.)




JM,
31.7.-1.8.12


- Item: "Die peak search & peak area determination modi sollten NICHT festverdrahtet, sondern per
	 service menu einstellbar sein"

	- ServiceForm.cs:

		- mod.: UI
			- neu: GB Peak search, Label + CB Peak search mode
			- neu: GB Peak area det., Label + CB Peak area det. mode

		- mod.: 'Load()'
			- neu: Ress. f�r diese controls

		- mod.: '_ctl_HelpRequested()'
			- neu: sec. "//  Peak search  mode CB",
				sec. "//  Peak area determination mode CB"

		- neu: const's '_Unidirectional', '_Bidirectional', '_Simple','_Extended_1', '_Extended_2'

		- neu: '_Init()'
			- hinzugef�gt: sec. "//  Peak search",
					sec. "//  Peak area determination"


		- mod.: '_CheckControls()'
			- neu: sec. "// Peak search: mode",
				sec. "// Peak area determination: mode"


		- mod.: '_UpdateServiceData()'
			- neu: Peak search - und Peak area det. mode eingearb.


JM,
1.8.12



- Item: "Forts.: Die peak search & peak area determination modi sollten NICHT festverdrahtet, sondern per
	 service menu einstellbar sein"

	- ScanParams.cs:

		- neu: members 'byModePeakSearch', 'byModeAreaDetection' + eingearb. in class context


	- Doc.cs:

		- mod.: 'RecordScanDataWriteCommonHeader()'
			- neu: sec. "// Peak search mode"
			- neu: sec. "// Peak area determination mode"

		- mod.: 'FillDataStructures()'
			- neu: elseif zu "// mode - peak search"
			- neu: elseif zu "// mode - peak area det."
			- neu: sec. "// Check: If the 'Mode (Peaksearch)' entry was NOT found, then assign: 0"
			- neu: sec. "// Check: If the 'Mode (Areadetection)' entry was NOT found, then assign: 0"


			
	- Test.cs:

		- mod.: 'GetSmoothFilterPar_Real()'
			- 2 neue Par.: 'nModePeakSearch', 'nModeAreaDetection' + eingearb. in routine context


		- mod.: 'TestArray_SCANPARAMS'
			- PS- und PA-modi eingebaut

		- mod.: 'TestArray_SCANALLDATA_Real'
			- mod.: sec. "//    Peak detection"
				- 'DetectPeaksEx()'-Aufruf angepa�t (Parameterzahl)



	- Test1.cs:

		- neu: globals 'm_nModePD', 'm_nModePA'

		- mod.: 'GetSmoothFilterPar_Real()'
			- 2 neue Par.: 'nModePeakSearch', 'nModeAreaDetection' + eingearb. in routine context

 		- mod.: 'SetParams()'
			- globals 'm_nModePD', 'm_nModePA' eigearb.
		
		- mod.: 'TestArray_SCANPARAMS'
			- PS- und PA-modi eingebaut

		- mod.: 'TestArray_SCANALLDATA_Real'
			- mod.: sec. "// 4. part: Peak info"
				- 'PS- und PA-modi eingearb.
				- 'DetectPeaksEx()'-Aufruf angepa�t (Parameterzahl)


	- Test1Form.cs:

		- mod.: UI
			- neue controls f�r PS- und PA-modi

		- diese controls in form context eingearb.


	- SpecEval.cs:

		- mod.: 'DetectPeaksEx()'
			- 'DetectPeaksEx()'-Aufruf angepa�t (Parameterzahl)

		- mod.: 'DetectPeaksEx()'
			- 2 neue Par.: 'nModePD', 'nModePA' + eingearb. in routine context

		- mod.: 'PeakAreaDetectEx_Combined()'
			- neuer Par.: 'nModePA' + eingearb. in routine context




JM,
2.8.12



- Item: "Peak search / peak area det.: Modi-strings im gesamten PN code einheitlich fassen"


	- neu: PeakAreaDetModi.cs, PeakSearchModi.cs

	- Test.cs:
	- Test1.cs:
	- ServiceForm.cs:
	- Test1Form.cs:
	- Doc.cs:
		- Modi-Strings eingearb.



JM,
9.8.12



- Item: "Laden einer Scriptdatei: Pr�fen, da� die Scriptnamen voneinander verschieden sind"


	- Exceptions.cs:

		- neu: class 'ScrCollectionEqualScrNamesException' einschl. Ress.


	- Scriptdata.cs / class ScriptCollection:

		- mod.: 'Check()'
			- neu: sec. "// Check, whether the script names are different from each other"

		- mod.: 'Load()'
			- mod.: catch branch
				- neu: test auf 'ScrCollectionEqualScrNamesException' eingearb.

		- mod.: 'FindScript()'
			- beim scriptname-Vgl.: ToUpper() hinzugef�gt



	- ScriptEditorForm.cs:

		- mod.: '_btnScript_Add_Click()'
			- neu gefa�t: sec. "// The script already exists:"
			  (enabled + neue Ressource)



- Item: "Spektrenspeicherung: #def. 'LOCALIZED_COMMON_HEADER' rausnehmen -> not needed"

	- entspr. mod.: 'RecordScanDataWriteCommonHeader()', 'RecordSingleScan()', 'RecordScan()', 
			'RecordResult()' einschl. Ressourcen 'rausgenommen




JM,
13.8.12



- Item: "Flows1 und 2 auf PC ausgeben & darstellen"

	- MainForm.cs:

		- mod.: UI
			- neu: labels 'lblFlow', 'lblFlow_Val'

		- mod.: 'Load'
			- neu: Ress. f�r 'lblFlow'


	- DeviceInfo.cs:

		- neu: member 'nFlow1', 'nFlow2' + eingearb. in class context


	- Test.cs:
	- Test1.cs:

		- neu: member 'nFlow1', 'nFlow2'

		- mod.: 'FillTestArray_Real()'
			- F1/2 eingearb.

		- mod.: 'TestArray_SCANALLDATA_Sim' 
			- mod.: sec. '// 1. part: Device info'
				- F1/2 eingearb.

		- mod.: 'TestArray_SCANALLDATA_Real' 
			- mod.: sec. '// 1. part: Device info'
				- F1/2 eingearb.


	- Diagnosis.cs:

		- mod.: 'Append()'
			- F1/2 eingearb.


	- Doc.cs:

		- mod.: 'RecordScanDataWriteCommonHeader()'
			- neu: sec. '// Row: Flows'

		- mod.: 'FillDataStructures()'
			- F1/2 eingearb.


	- Vw.cs:

		- mod.: 'OnInitialUpdate()'
			- mod.: sec. "// Device info"
				- Flow eingearb.

		- mod.: 'UpdateResultData()'
			- mod.: sec. '// Device info'
				- Flow eingearb.

		- mod.: '_OnPrintParams()'
			- F1/2 eingearb.


JM,
14.8.12



- Item: "BUG: script editor: TB 'script ID' hat nicht die '_txt_TextChanged' zugewiesen bekommen -> nachholen"

	- ServiceForm.cs:

		- mod.: control '_txtScriptID'
			- Callback hinzugef�gt: '_txt_TextChanged()'


- Item: "Weitere HelpRequested topics aufnehmen"

	- GSMReaderForm:

		- neu zugewiesen f. control: '_btnFile_EE_ScriptInfo'


	- ServiceForm.cs:

		- mod.: '_ctl_HelpRequested()'
			- neu: sec. "// RB's Smoothing method" nebst ress.

		- ALLEN controls die '_ctl_HelpRequested()' routine zugewiesen (damit die 'kein Kommentar
		  verf�gbar' message auch angezeigt wird)


	- ServiceMPForm.cs:

		- mod.: '_ctl_HelpRequested()'
			- neu: sec. "// Flow - Soll values",
				sec. "// Flow - Ist values"

		- raus: sec. "... El resto del mundo ..."


	- CTSForm.cs:
		- raus: sec. "... El resto del mundo ..."



JM,
27.8.12



--------------------------------------------------------------------

Hier wird 3.10.0 abgeschlossen und in 3.10.0_old umbenannt.
Weiterf�hrung in 3.10.0.

--------------------------------------------------------------------


JM,
5.9.12



- Item: "Ressource files ges�ubert"

	- raus:

		CommChannelWait_Title
		CommChannelWait_txtMessage
		CommChannelWait_btnCancel

		CommError_Command
		CommError_Parameter

		cDoc_Record_Scans_DataHeader
		cDoc_Record_SingleScan_DataHeader

		CommChannel_Err_Timeout
		CommChannel_Err_Gen



- Item: "ScanInfo: routine 'GetReadyForMeas()' einf�hren (analog zu IMS)"

	- ScanInfo.cs

		- neu: 'GetReadyForMeas()'


	- MainForm.cs:

		- mod.: '_mi_Popup()', '_UpdateToolbarButtons()'
			- 'GetReadyForMeas()' eingearb.



- Item: "gesamten code auf Vorkommen von 'goto case' durchgeforsten: rausnehmen, wo man das einfacher
	 fassen kann"


	- ScriptEditorCmdForm.cs:

		- mod.: '_btnOK_Click()'
			- entspr. mod.: sec. "// Perform a parameter validity check"



- Item: "BUG (SM, 3.9.12): GSMControl: Wenn man bei laufender Messung zoomt, und die Verbindung bricht ab,
	 kommt es vor, da� der Cursor geclippt bleibt."


	- MainForm.cs:

		- mod.: 'AfterRXCompleted()' / Timeout section

			- neu: sec. "// Ensure, that the mouse is not captured & the cursor is not clipped"

 

- Item: "alle 'GetString()'-Aufrufe durchforstet: Ressource vorhanden? wenn nicht, nachtragen."

	- Doc.cs/GetScanFile(): 
		- raus: 'GSMReader_OFN_Open'
		- daf�r rein: 'cDoc_LoadScan_OFN_Open'

	- Test.cs/GetScanFile(): 
	- Test1.cs/GetScanFile(): 
		- raus: "Open Scan File"
		- daf�r rein: cDoc_LoadScan_OFN_Open



- Item: "alle files durchforsten: members in der richtigen (///) Notation? Falls nicht, anpassen."

	- entspr. angepa�t:

		App.cs (members)
		AppData (members)
		CmdUI (members)

		ConnectForm (members)
		CTSForm (members)
		GSMReaderForm (members)
		PropertiesForm (members)
		ScriptSelectForm (members)
		ScriptTransferForm (members)
		SDcardReaderForm (enum, members)
		ServiceForm (enum, members)
		ServiceMPForm (members)
		TitleForm (members)

		ScriptCommands (members)

		FakeSpectrum (members)
		MessageBoxCent.cs (delegate,members
		ToolbarFunction (members)

		Vw (members)



JM,
5.9.12



- Item: "In die DeviceInfo auch den battery level einbeziehen, diesen im common file header ausgeben
	 und in Printerausgabe einbeziehen (analog zu IMS)"


	- DeviceInfo.cs:

		- neu: member 'dBattery' + eingearb. in class context


	- Doc.cs:

		- mod.: 'RecordScanDataWriteCommonHeader()'
			- neu: sec. "// Row: Battery"


	- Vw.cs:

		- mod.: '_OnPrintParams()' / // Part: DeviceInfo
			- mod.: row 'Flow ...'
				  - Battery level hinzugef�gt


	- Test.cs:
	- Test1.cs:

		- mod.: 'TestArray_SCANALLDATA_Sim', 'TestArray_SCANALLDATA_Real'
			- mod.: sec. "// 1. part: Device info"
				- Battery level eingearb.




JM,
6.9.12



- Item: "DeviceInfo: Battery state-Aussage (Charging bzw. %-Zahl) in Klasse kapseln"

	- DeviceInfo.cs

		- neu: prop. 'BatteryState'


	- Doc.cs:

		- mod.: 'RecordScanDataWriteCommonHeader()'
			- mod.: sec. "// Row: Battery"
				- 'BatteryState' eingearb.

	- Vw.cs:

		- mod.: '_OnPrintParams()' / // Part: DeviceInfo
			- mod.: sec. "// 4. line: Flow & Battery"
				- 'BatteryState' eingearb.



- Item: "File storage: Ger�tefehler mit im Header ausgeben"

	- Doc.cs:

		- mod.: 'RecordScanDataWriteCommonHeader()'
			- neu: sec. "// Row: Error Dword"



JM,
7.9.12



- Item: "Flow regulation einarbeiten, Flow 3 einarbeiten (FF, 10.9.12)"


	- DeviceInfo.cs:

		- neu: member 'nFlow3' + eingearb. in class context
		- in diesem Zus.: mod.: '_NO_DEVICEINFO_MEMBERS' (bisher 9 -> neu 10)


	- Diagnosis.cs

		- mod.: 'Append'
			- F3 eingearb.


	- Doc.cs:

		- mod.: 'RecordScanDataWriteCommonHeader()'
			- mod.: sec. "// Row: Flows"
				- F3 eingearb.

		- mod.: 'FillDataStructures()'
			- mod.: sec. "// Flow1..."
				- F3 eingearb.


	- Vw.cs:

		- mod.: 'UpdateResultData()'
			- mod.: sec. "// Section 'Device information'"
				- F3 einbezogen


		- mod.: '_OnPrintParams()'
			- mod.: sec. "// Part: DeviceInfo"
				- F3 einbezogen


	- Test.cs:
	- Test1.cs:

		- mod.: 'TestArray_SCANALLDATA_Sim'
			- mod.: sec. "// 1. part: Device info"
				- F3 einbezogen

		- mod.: 'TestArray_SCANALLDATA_Real'
			- mod.: sec. "// 1. part: Device info"
				- F3 einbezogen

		- mod.: 'FillTestArray_Real()'
			- neuer Par.: F3 + eingearb. in routine



	- ServiceForm.cs:

		- mod.: UI
			- neu: GB 'Pump-Flow handling'
				- neue controls: '_txtF1' ... '_txtF3', '_chkPF1' ... '_chkPF3',
						'_txtKP1' ... '_txtKP4' 
			- neu: F3 controls
		- neu in diesem Zus.: '_chkPF_CheckedChanged()'
		- mod. in diesem Zus.: '_ctl_HelpRequested()'
		- mod. in diesem Zus.: 'Load()'
			- entspr. Ressources eingearb.

		- mod.: UI
			- LOGO-RB's: Tabstop-property auf True gesetzt

		- mod.: enum 'ReadDataError'
			- neu: members 'F3_Debit', 'KP'

		- mod.: '_UpdateServiceData()' / WRITE, READ
			- mod.: sec. "// Section: Sensor"
			- entspr. mod.: Notes 

		- mod.: '_CheckControls()'
			- entspr. �berarb.: sec. "// Section: Sensor"

		- mod.: '_Init()'
			- neu: sec. "// Dis-/Enabled state of Pump - Flow handling controls"




- Item: "SDCardReaderForm: TabStops f. RB's aktivieren (wurde vorher leider vergessen)"


	- SDCardReaderForm.cs:
			- 'Clear  device storage'-RB's: Tabstop-property auf True gesetzt



- Item: "Flow: konsequent in 'ml/min' anstelle von 'adc' angeben"


	- MainForm.cs:

		- mod.: UI
			- entspr. ge�nd.: label 'lblFlow' einschl. ress. 'Main_lblFlow'



- Item: "Load and display scan from HD ...: Auch die 'errors on device' auslesen und darstellen"

	- Doc.cs:

		- mod.: 'FillDataStructures()'
			- neu: sec. "// Errors on device"


	- ScanInfo.cs:
		- neu: 'ErrorFromString()'





JM,
17.-18.9.12



- Item: "WarmUp: PC comm. zulassen: falls etwas RX'd wird, so sollte 'WARMUP' zur�ckgegeben
	 werden (FF, 25.9.12)"


	- Doc.cs:

		- neu: global 'bWarmUpRunning'


	- MainForm.cs:

		- mod.: 'StatusBar_DrawItem()'
			- 'bWarmUpRunning' eingebaut, dabei insg. die if-else Struktur �berarb.
			- neu in diesem Zus.: ress. 'Main_Status_P1_WarmUp'


		- mod.: '_Timer_Tick()'
			- mod.: sec. "Statusbar / Panel 1"
				- 'bWarmUpRunning' eingebaut, dabei insg. die if-else Struktur �berarb.


		- mod.: 'HandleRX()'

			- ge�nd.: Null-Pr�fung von 'msgRX' an den Anfang der Routine gezogen
			- gesamte routine in try-catch eingepackt
			- neu: sec. "// Check: WarmUp running?"
			  (mit �bernahme der ursp�ngl. '// Dispatch the message acc'ing to the owner form'-
			   Sektion in den else-Zweig der sec.)


	- CommMessage.cs:

		- neu: const 'SZWARMUP'

		- mod.: 'Empty()'
			- bisher: 	_arParAnswer = null;
			- neu: 		_arParAnswer = new byte[0];

			- bisher: 	_wi = null;
			- neu:		_wi = new WndInfo ();


	- ConnectForm.cs:

		- mod.: '_Timer_Tick()'

			- neu: sec. "// CD: WarmUp running?"
			  (der bisherige content ist im else-Zweig hinterlegt)
			- neu in diesem Zus.: ress. 'Connect_WarmUp'



- Item: "Die min. Dauer des Selbsttest-OK-Status sollte per Service menu vorgebbar sein"


 	- ServiceForm.cs:

		- mod.: UI
			- neu: GB 'Cont.: Device Data' nebst controls
			

		- mod.: 'Load()'
			- neu: Ress. zu ContDevice


		- neu: const's '_MIN_SELFCHECK_IN_TUBE_SEC', '_MAX_SELFCHECK_IN_TUBE_SEC'


		- mod.: '_ctl_HelpRequested()'
			- neu: sec. "// ContDevice section" nebst Ress.


		- mod.: '_CheckControls()'
			- neu: sec. "// Section: ContDevice"


		- mod.: '_UpdateServiceData()' / READ, WRITE
			- eingearb.: self check



JM,
28.9.12



- Item: "Produktinfo: neben PC version auch kompatible device version(s) anzeigen"

	- AboutForm.cs

		- mod.: UI:

			- neu: 'lblVersionPC', 'txtVersionPC', 'lblVersionDev', 'txtVersionDev'

		- mod.: 'Load()' 

			- bisher: Ausgabe PC ver.
			- neu:	  Ausgabe PC- und kompatible device ver.

			- in diesem Zus.: neue Ress: 'About_lblVersionPCKey', 'About_lblVersionDevKey'


	- Doc.cs:

		- mod.: member 'VERSIONSTRING'
			- bisgher: private
			- neu: internal



- Item: "Service data: Automatisches Updaten der ServiceData durch Auslesen einer auf dem device
	 gespeicherten Service-Datei einarb."


	- ServiceForm.cs:

		- mod.: UI

			- neu: GroupBox 'Load service file' + untergeordnete controls
			- in diesem Zus.: gesamtes Layout angepa�t & verdichtet

		- mod.: 'Load()'

			- neu: Ress. f�r GroupBox 'Load service file' + untergeordnete controls

		- neu: '_btnLoad_Click()'

		- mod.: '_ctl_HelpRequested()'

			- neu: sec. "// Load service file section" nebst zugeh. ress.

		- neu: member '_sServiceFolderPath', '_sCurServiceFilename'

		- mod.: 'UpdateData()'

			- neu: sec. "// Button 'Load'"

		- mod.: '_UpdateServiceData (Boolean bDDX)'

			- mod.: sec. "// Error handling"
				- '_WriteErrors()' eingearb.

		- neu: '_UpdateServiceData (string sSvcFile)'

		- neu: '_WriteErrors()'

		- neu: '_GetValue()'

		- mod.: 'AfterRXCompleted()'

			- mod.: case "SVCREADDATA"
				- bisher: nach RX aller service data strings:
					sec. "Finish the transfer"

				- neu: nach RX aller service data strings:
					sec. "// Read Service root folder path: "RDSVCFO\r""
					(die sec. "Finish the transfer" wurde rausgenommen und kommt sp�ter
					 in der 'RDSVCFI' response)

			- mod.: case "SVCWRITEDATA"
				- bisher: nach TX aller service data strings:
					sec. "Finish the transfer"

				- neu: nach TX aller service data strings:
					sec. "// Read Service files: "RDSVCFI\r""
					(die sec. "Finish the transfer" wurde rausgenommen und kommt sp�ter
					 in der 'RDSVCFI' response)

 			- neu: sec. "SDcard storage messages", im spez.
				- case "RDSVCFO"
				- case "RDSVCFI"
				- case "RDTFI"


- Item: "L�nge des Parameter-Teils eines PC-�bertragungs-strings sauberer def."


	- mod.: Notes zu
		- CTSForm / _UpdateCTSData() / WRITE
		- ServiceMPForm / _UpdateServiceData() / WRITE
		- ServiceForm / _UpdateServiceData() / WRITE
		- ServiceForm / _UpdateServiceCmt / WRITE


	- CommRS232.cs:
		- neu: const 'MAX_PAR_LEN' 
	

	- ScriptData.cs:
		- class ScriptParameter:
			- neu: 'Check()'

		- class ScriptCollection:
			- mod.: 'Load()'
				- neu: sec. "// Check, whether the script parameters are valid"


- Item: "BUG: Scripteditor/Window: Der Teststring bzgl. zul�ssiger wnd-msg-L�nge ist viel zu lang
	 -> Ursache finden + beseitigen!"


	- SriptEditorWindowForm.cs

		- mod.: '_btnOK_Click()'
			- mod.: sec. "// Build the script window, that corresponds to the data"
				(see corr'ing Notes)


- Item: "SDCardReader: Die Dateien in den Listboxes sollten in der Reihenfolge ihres Anlegens
	aufgelistet sein. Das schien bisher autom. zu stimmen (mit ausgeschalteter Sort property),
	ist jedoch nicht immer so (i.e. bei LB's mit vielen Eintr�gen konnte man sehen, da� die
	zeitliche Aufeinanderfolge gest�rt war) -> Abstellen!


	SDCardReaderForm.cs:

		- mod.: UI
			- mod.: 'lbData/Alarm/Scan/ScriptFolders', 'lbData/Alarm/Scan/Script/ServiceFiles'
				- Sorted prop.: false (bisher) -> true (neu)



JM,
5.-6.11.12



- Item: "Script window: Die min. Anzahl von zu belegenden Kalib.-punkten sollte als const vorgegeben werden.
	 Der ScriptWindow-Check sollte einen diesbzgl. check beinhalten.
	 Diese const sollte auch im ScriptEditorWindow-Form eingearb. werden.
	"


	- ScriptData.cs:

		- class ScriptWindow

			- neu: const 'MIN_CALIB_POINTS'

			- mod.: 'Check()'
				- neu: sec. "// Check: Min. number of allocated substance calibration points reached?"

		- class ScriptCollection:

			- mod.: 'Load()'
				- mod.: catch-branch
					- Test auf 'ScrWndMinNoCalibPointsException' eingearb.



	- Exceptions.cs:

		- neu: c. 'ScrWndMinNoCalibPointsException' nebst ress.



	- ScriptEditorWindowForm.cs:

		- mod.: '_btnOK_Click()'
			- mod.: sec. "// Calibration arrays"
				- 'MIN_CALIB_POINTS' eingearb.



- Item: "Abgriff der Flow log files (und sp�ter auch der Error log files) von der SD card f�r
	 den internen Gebrauch erm�glichen. Daf�r ein PW-gesch�tztes 'Erweiterter Service'-Form
	 verwenden.
	"


	- Crypt.cs:

		- neu: region 'RC2'


	- AppData.cs:

		- neu: const 'sPWSvcExt'
		
		- mod.: c. 'CommonData'
			- neu: region 'Service-Extended password'


		- mod.: '_ReadRegistry()', '_ReadResourceFile()'
			'_WriteRegistry()', '_WriteResourceFile()'
			- 'Service-Extended password' stuff eingearb.


	- ServiceForm.cs:

		- mod.: UI
			- neu: controls '_txtPW', '_btnExtended' + callbacks

		- mod.: 'Load()'
			- Ress. f�r '_btnExtended'

		- mod.: 'UpdateData()'
			- neu: sec. "// Button 'Extended ...'"

		- mod.: '_ctl_HelpRequested()'
			- neu: sec's f�r 'Extended' Funktionalit�t


	- AppComm.cs:

		- neu: messages 'RDFLWFO', 'RDFLWFI', 'EYFLWFO'
				'RDERRFO', 'RDERRFI', 'EYERRFO'


	- neu: SDcardReaderExtForm.cs
		+ zugeh�rige ress.




JM,
27.11.12

	

- Item: "Wenn die L�nge eines TX-�bertragungsstrings das zul�ssige Max. �berschritten haben sollte, so hat
	 bisher die 'Writemessage()-routine False returnt; allerdings wurde im code keine diesbez�gliche
	 Auswertung durchgef�hrt -> BUGGY!
	 Deshalb: Mechanismus einarbeiten, der dieses Manko abstellt.
 	"


	- CommRS232.cs:

		- neu: const 'CMID_STRLENERR'
		- mod.: 'WriteMessage()'
			- ge�nd.: ret. Typ: bool -> void
			- neu: sec. "// String length error: Send the message with the 'String length error' notification  "


	- MainForm.cs:

		- mod.: 'HandleRX()'
			- neu: check auf CMID_STRLENERR eingearb.
			- mod.: Aufruf des CommErrorForm - #tors:
				- bisher: #tor (f)
				- neu:	  #tor (f, msgRX)


	- CommErrorForm.cs:

		- mod.: 'Load()'
			- neu gefa�t: sec. "// Message text"
				- umbenannt: 'CommError_Err_Body' -> 'CommError_Err_Def'
				- neu: 'CommError_Err_StrLen' nebst ress.

		- raus: 	member 'CommRs232ResponseEventArgs _e'
		- daf�r neu: 	member: 'CommMessage _msg'

		- raus: 	#tor 'public CommErrorForm (Form fOwner, CommRs232ResponseEventArgs  e)'
		- daf�r neu: 	#tor 'public CommErrorForm (Form fOwner, CommMessage msg)'



	- MainForm.cs:
	  ConnectForm.cs:
	  CTSForm.cs:
	  GSMReaderForm.cs:
	  ScriptTransferForm.cs:
	  SDcardReaderExtForm.cs:
	  SDcardReaderForm.cs:
	  ServiceForm.cs:
	  ServiceMPForm.cs:

		- mod.: 'AfterRXCompleted()'
			- neu: check auf CMID_STRLENERR eingearb.




- Item: "SDcardReader / SDcardReaderExt: Die Form's reagieren nicht auf die ESC-Taste,
	 um sie via Cancel zu schliessen -> �ndern!"


	- SDcardReaderForm.cs:
	- SDcardReaderExtForm.cs:

		- neu: 'SDcardReaderForm_KeyPress()'
		- mod.: UI
			- member 'KeyPreview': F (bisher) -> T (neu)




JM,
5.12.12


- Item: "BUG: Die PC-Verbindung bricht undefiniert ab (mit oder ohne Error-screen, meistens mit) -> Grund?"

	- MainForm.cs:

		- neu: members 'nDbgWnd', 'bDbgWnd'

		- mod.: '_Timer_Tick()'
		- mod.: 'AfterRXCompleted()' / SCANALLDATA
			- neu: sec. "// ==> B/E: SCANALLDATA cmd receive handling"



JM,
7.12.12


--------------------------------------------------------------------

Hier wird 3.10.0 abgeschlossen und in 3.10.0_old_1 umbenannt.
Weiterf�hrung in 3.10.0.

--------------------------------------------------------------------


JM,
11.12.12


- Item: "Forts.: BUG: Die PC-Verbindung bricht undefiniert ab (mit oder ohne Error-screen, meistens mit) -> Grund?"


	- MainForm.cs:

		- mod.: '_Timer_Tick()'
		- mod.: 'AfterRXCompleted()' / SCANALLDATA
			- mod.: sec. "// ==> B/E: SCANALLDATA cmd receive handling"
				- sec. "// Optical announcement" ebenfalls in den '#if Debug ... #endif'-Block
				  eingeschlossen


		- mod.: '_Timer_Tick()'
			- mod.: sec. "// Statusbar / Panel2"
				- 'dwNoDataCount' eingearb.
				- ress. 'Main_Status_P2' entspr. ge�nd.

			
		- mod.: 'AfterRXCompleted()' / SCANALLDATA
			- neu: sec. "// Increment the # of incoming NODATA responses"

			

	- Doc.cs:

		- neu: member 'dwNoDataCount' + eingearb. in class context




- Item: "Device self check: Bei Vorgabe von 'min duration of OK state' = 25 sec l�uft der Dialog in einer Schleife, 
	 i.e. kommt nicht von selbst (oder erst nach mehreren Durchl�ufen) in den OK state -> BUG!"


	Grund: Bei der Bestimmung des max. Werts dieser Konstanten wurde vergessen, das Screen update interval (in sec)
	       in die Berechnung einzubeziehen - daraus resultiert dann ein niedrigerer Wert (siehe device SW).

	
	- ServiceForm.cs:

		- mod.: const '_MAX_SELFCHECK_IN_TUBE_SEC'
			- bisher: 25
			- neu: 24



JM,
11.12.12
		

- Item: "Das CommError-Form sollte, sofern aktiv, in der Taskbar angezeigt werden."


	- CommErrorForm.cs:
		- mod.: GUI
			- mod.: member 'ShowIntaskBar': F (bisher) -> T (neu)



JM,
13.12.12



--------------------------------------------------------------------

Hier wird 3.10.0 abgeschlossen und in 3.10.0_old_2 umbenannt.
Weiterf�hrung in 3.10.0.

--------------------------------------------------------------------


JM,
19.12.12



- Item: "Der name der bisherigen exe-Datei ist irref�hrend ('GSMControl' ist die IMS-Anwendung),
	 deshalb umbenennen in 'PIDControl'"


	- Solution explorer: App -> Properties/General:

		- mod.: key 'assembly name'
			- bisher: GSMControlJM
			- neu: PIDControlJM
		  (damit �ndert sich der name des .exe files in 'PIDControlJM.exe')



- Item: "PC comm. mitschneiden ("PC comm. listener")"


	- CommMessage.cs:

		- neu: member '_sExcMsg' + eingearb. in class context (prop. 'ExcMsg', 'Empty()', ...)


	- neu: ExcMsgFile.cs


	- CommRS232.cs:

		- neu: member 'm_emf'
		- neu: prop. 'ExcMsgFile'

		- mod.: 'ThreadProc()'
			- neu: sec's "// Exception message file: Append data"
			- neu: sec. "// Fill the messages 'ExcMsg' member"


	- CmdUI.cs:

		- mod.: 'OnConnectDevice()'
			- neu: sec. "// Init. Exception message file handling"

		- neu: 'OnPCcommListener()'

		- mod.: 'OnPW()'
			- mod.: sec. "// An Expert or TC logged in (usertype = Expert or TrainedCustomer):"
				- Zus�tzl. Abgleich auf 'PC communication logging' eingearb.
				  (nebst ress. 'cCmdUI_PW_Info1')



	- MainForm.cs:

		- mod.: UI
			- neu: menuitem '_miTools_PCcommListener' + eingearb. in context:
				- '_mi_Click()'
				- '_mi_Popup()'
				- 'UpdateCulture()' nebst ress.
				- 'UpdateAccordingUserMode()'



JM,
17.-19.12.12


- Item: "Forts.: PC comm. mitschneiden ("PC comm. listener")"


	- ExcMsgFile.cs
		
		- mod.: enum 'AppendSpec'
			- neu: members: 
    				OnThreadProc_CMID_ANSWER,
    				OnThreadProc_CMID_EMPTY,
    				OnThreadProc_Catch,

    				OnHandleRX_Entry,
    				OnHandleRX_AfterInv,
    				OnHandleRX_CommClose,
    				OnHandleRX_WarmUp,
    				OnHandleRX_NotWarmUp,
    
    				OnAfterRXCompleted_CMID_EMPTY,
    				OnAfterRXCompleted_CMID_TIMEOUT,
    				OnAfterRXCompleted_CMID_ANSWER,
    				OnAfterRXCompleted_SAD_NODATA

		- mod.: 'AppendExcMsgFile'
			- neue 'AppendSpec' members eingearb.

		- neu: 'AppendSpecial()', 'AppendExcMsgFileSpecial()'



	- CommRS232.cs:

		- mod.: 'ThreadProc()'
			- neu: sec's "// Exception message file: Append helper stuff"
			- neu: sec. "// Exception message file: Append special data"


	- MainForm.cs:

		- mod.: 'HandleRX()'
			- neu: sec's "// Exception message file: Append helper stuff"

		- mod.: 'AfterRXCompleted()'
			- neu: sec's "// Exception message file: Append helper stuff"




JM,
21.12.12



- Item: "2. Forts.: PC comm. mitschneiden ("PC comm. listener")"

	Grund: Das Logging soll (initial) enabled sein, nur der Expert sollte es ausschalten k�nnen.



	- ExcMsgFile.cs
		
		- mod.: member 'm_bEnabled'
			- initial: FALSE (bisher) -> TRUE (neu) 

		- mod.: const '_MAXFILES'
			- 5 (bisher) -> 2 (neu)

		

	- CmdUI.cs:

		- mod.: 'OnPW()'
			- mod.: sec. "// An Expert or TC logged in (usertype = Expert or TrainedCustomer):"
				- mod.: Zus�tzl. Abgleich auf 'PC communication logging'
				  	(nebst ress. 'cCmdUI_PW_Info1')
					- bisher: falls Listening aktiv, so deaktiv.
					- neu: falls Listening inaktiv, so aktiv.



JM,
3.1.13


- Item: "Diagnosis: 'IUT' mu� durch ENIT ersetzt werden; konsequent language-lokalisiert arbeiten (betr. Email
	 subject and body)"


	- Diagnosis.cs:

		- mod.: 'Terminate()'
			- neu: ress's 'cDiagnosis_Info_PerformedAndSend', 'cDiagnosis_Term_Subject', 'cDiagnosis_Term_Body'

			- mod.: local 'sRecipient'
				- bisher: 'info@iut-berlin.com'
				- neu: 'info@environics-iut.com'

			- mod.: local 'sSubject' 
				- ress. eigearb.

			- mod.: local 'sFmt' (Body)
				- ress. eigearb.



JM,
18.1.13


- Item: "Alarm acknowledge handling etwas sauberer fassen:
	 - Alarm button darf nur sichtbar sein, wenn ein alarm anliegt UND das Ger�t nicht im KEY steht
	 - diverse Abtestungen m�ssen auf 'btnAlarm.Visible' erfolgen anstatt auf 'doc.bResultAlarm'


	- CmdUI.cs:

		- mod.: 'OnAlarmAcknowledge()'
			- insg. neu gefa�t


	- MainForm.cs:

		- mod.: Alarm button control 
			- bisher: private '_btnAlarm' -> neu: internal 'btnAlarm'


		- mod.: '_Timer_Tick()'
			- mod.: sec. "// Check alarm state: Show/hide the Alarm button corr'ly"
				- bisher: btnAlarm.Visible = doc.bResultAlarm
				- neu: 'btnAlarm.Visible' komplexer ausgewertet
			- mod.: sec. "// ToolTip text for the 'Trigger Key cmd' TBB, depending on alarm state"
				- ersetzt: 'doc.bResultAlarm' durch 'btnAlarm.Visible'


		- mod.: '_mi_Popup()'
			- mod.: sec. "// Acknowledge alarm"
				- bisher: bEnable = doc.bResultAlarm
				- neu: bEnable = btnAlarm.Visible;

			- mod.: sec. "// Trigger Key Cmd"
				- bisher: bEnable = !doc.bResultAlarm;
				- neu: bEnable &= !btnAlarm.Visible; 

		- mod.: '_UpdateToolbarButtons()'
			- mod.: sec. "// Trigger Key Cmd"
				- bisher: bEnable = !doc.bResultAlarm;
				- neu: bEnable &= !btnAlarm.Visible; 




- Item: "ScriptTransferForm: Debug-Ausgaben bei RX einarbeiten (nur zu Testzwecken)"


	- ScriptTransferForm.cs:

		- mod.: 'AfterRXCompleted()'
			- mod.: alle cases:
				- neu: sec's "// Testing only"



- Item: "Menuitem 'MP data transfer': nur f�r Expert sichtbar"


	- MainForm.cs:

		- mod.: 'UpdateAccordingUserMode()'
			- mod.: sec. "// 2. group: Access for Expert only"
				- neu: sec. "// Transfer MP data ..."	



JM,
21.1.13


- Item: "Connect device: falls eine PCcomm-logging-Datei ge�ffnet ist, wenn ein device konnektiert werden soll,
	 so generiert das System einen IO-Fehler -> BUG"


	-> Grund: Die 'Directory.Delete()' routine kann vom System nicht ausgef�hrt werden, solange Dateien
		  des betreffenden Verz. ge�ffnet sind.


	- CmdUI.cs:

		- mod.: 'OnConnectDevice()'

			- mod.: sec. "// Init. Exception message file handling"
				- try-catch eingearb. (nebst ress.)



- Item: "Unsaubere Darstellung der 'Amplifier scale' Beschriftung im Service dlg."

	- ServiceForm.cs:

		- mod.: UI
			- mod.: control '_lblAmplScale'
				- verbreitert, so da� der deutsche Text sauber hineinpa�t




JM,
23.1.13


- Item "Release-Mode der SW : Comm-Error-Messages sollten NICHT in der StatusBar ausgegeben werden
	(HG: das verwirrt)"


	- MainForm.cs:

		- mod.: '_Timer_Tick()',
			'StatusBar_DrawItem()'

			- mod.: sec. "//    Check: Comm. errors present?"
				- local 'errCount' eingef�hrt
				- neu: sec. "// Release: Do NOT visualize comm. errors"



JM,
24.1.13



- Item: "PS/PA: Die neuen Methoden (Bidirectional bzw. Extended1/2) erscheinen zu unsicher, deshalb
	 disablen (meine Meinung)"


	- ServiceForm.cs:

		- mod.: UI:
			- mod.: CB '_cbPSMode', CB '_cbPAMode'
				- enabled (bisher) -> disabled (neu)


JM,
10.6.13



******************************************************************************************************
Die Enwicklung dieser PID-Version wurde bis 01/13 gef�hrt (abgesehen von der 6/13 gemachten Korrektur).
Dabei wurden SW-Anregungen der IMS-Strecke bis einschl. IMS-Ver. 6.8.0 (device, PC 2.8.0) ber�cksichtigt. 
D.h., alle IMS items ab Ver. 6.9.0 (device, PC 2.9.0) m�ssen (sofern relevant) in den PID code �bernommen
werden. Zu diesem Zweck wurde PID 4.0.0 angelegt.
******************************************************************************************************

JM,
5.9.13



================================
4.0.0 (angelegt 5.9.13)
================================

(
Korr. Device-SW-Version: Ordner 'D:\Eigene Dateien\IAR_EW\IUT\PID\PID_V4_0_0' 
)


based on: 3.10.0

JM,
5.9.13



- Item: "Multiplexer: neues Scrpt cmd 'MPCYCCNT' einarb."

	- ScriptCommands.cs / class ScriptCommands:

		- neu: cmd 'MPCYCCNT'
		- mod.: 'HelpOnCmd()', 'HelpOnPar()'
			- neu: case "MPCYCCNT" nebst Ress.

	- Ress.: ge�ndert in diesem Zus.: ress. 'ScriptCommands_HlpOnCmd_MPSETCHANx'



- Item: "BUG: Wenn ein PC-gestarteter script mit gecheckten result-Checkboxes l�uft, und nach Stop desselben
	 via PC ein script mit GERINGERER result- (und damit aktivierter Checkbox-) Anzahl gestartet wird, h�ngt sich
	 GSMControl auf mit der message 'Index au�erhalb des g�ltigen Bereiches ...'"

	Grund: Das 'nMarkedWindow'-array hat bei Start des neuen scripts noch seinen bisherigen 'checked'-Zustand.
		Wenn der neue script weniger Fenster als der bisherige besitzt, wird aufgrund noch gesetzter
		'nMarkedWindow'-arraywerte auf Scanresult-arrayelemente zugegriffen, die im neuen script gar nicht
		definiert sind (UP 'UpdateGraph' / sec. "// Indicate the windows to be marked"). 
		Dies l�st den o.a. Fehler aus.
  

	- Doc.cs:

		- mod.: 'ResetScanData()'
			- neu: sec.'s "// Reset marked windows",
					"// Synchronously reset the Result Checkboxes"



- Item: "CTS: Es ist erforderlich, da� Scripte in der Scriptliste mehrmals auftreten k�nnen
	 (Bisher: ein Script durfte nur einmal in der Liste auftreten)."


	- CTSForm.cs:

		- neu: #define 'CTS_UNIQUE_ENTRY'

		- mod.: '_btnAdd_Click()', '_btnEdit_Click()'
			- umbennant: sec. "// Check: Unique entry?" (bisher) -> sec. "// Check for entry correctness:"
 			- raus: local 'bEntryExists'
			- daf�r neu: local 'bCorrectEntry'
			- eingearb.: #def. 'CTS_UNIQUE_ENTRY'


	- AppNotes.txt:

		- #define 'CTS_UNIQUE_ENTRY' einbezogen



- Item: "CTS: die max. Anzahl von CTS-scripts ist
		a) = 6 und
		b) mu� in PC- und device SW �bereinstimmen
	"

	- Doc.cs:

		- mod.: const 'MAX_CLOCKTIMEDSCRIPTS': 10 (bisher) -> 6 (neu)

	- CTSForm.cs:

		- mod.: '_btnAdd_Click()'
			- mod.: sec. "// Check: At most MAX_CLOCKTIMEDSCRIPTS rows should be edited!"
				- hier war in string 'sMsg' bisher f�lschlicherweise 'Script.MAXSCRIPTWINDOWS'
				  aufgef�hrt; richtig mu� es jedoch heisen 'Doc.MAX_CLOCKTIMEDSCRIPTS'.
			==> da diese code sequence NICHT zur Ausf�hrung gelangt, ist dieser Bug bisher verborgen
			    geblieben.



- Item: "Einarbeiten: Automatisiertes Spanmonitoring"


	- Scriptdata.cs:

		- mod.: class ScriptParameter
			- neu: class 'AutoSpanMon'
			- neu: member 'arAutoSpanMon[]' + eingearb. in class context

		- mod.: class ScriptCollection:
			- neu: const 'AutoSpanMon'
			- mod.: 'Load()'
				- mod.: catch-branch:
					- Test auf 'ScrAutoSpanMonException' eingearb.
			- mod.: 'Write()'
				- mod.: sec. "// Parameter"
					- neu: sec. "//    Automated span monitoring" 

		- mod.: class Script:
			- modif.: 'Check()'
				- mod.: Parameter sec.
					- neu: sec. "// Check, whether the 'Automated span monitoring' members 	
							(Parameter section) and the substance windows
      							(Windows section) are consistent or not"


	- Exceptions.cs:

		- neu: class ScrAutoSpanMonException


	- GSMReaderForm.cs:

		- mod.: 'AfterRXCompleted()'
			- mod.: case "STRESPARAM":
				- neu: sec. "// Automated span monitoring"


	- neu: ScriptEditorAutoSpanMonForm.cs

		- neu: class ScriptEditorAutoSpanMonForm nebst ress.


	- ScriptEditorForm.cs:

		- mod.: UI 
			- mod.: sec. Script parameter
				- neu: controls zu 'Automated span monitoring'

		- modif.: 'ScriptEditorForm_Load()'
				- neue Ressourcen eingearb.

		- neu: '_btnAutoSpanMon_Add_Click()' nebst Ress.,
			'_btnAutoSpanMon_Edit_Click()' 

		- mod.: '_btnRemove_Click()', 
			'_lv_DoubleClick()',
			'_ctl_HelpRequested()' nebst Ress.,
			'_Timer_Tick()',
			'_ContentsToScript ()', 
			'_ScriptToContents()', 
			'_InitControls()'

			- Script parameter 'Automated span monitoring' eingearb.

	- ScriptTransferForm.cs:

		- mod.: '_SendScript()'
			- mod.: sec. "// TX: Script Parameter ( Write the script parameter set )"
				- mod.: # of script parameter (string) parts
					- bisher: 1
					- neu: 2 + Script.MAX_SCRIPT_WINDOWS



- Item: "Der user mu� Zugriff auf die Errorlog-Dateien haben. Deshalb ist deren Handling in den
	  'SDcardReader' - Dialog zu implementieren."


	- SDcardReaderForm.cs:
	- SDcardReaderExtForm.cs:

		- alle Passagen, die die Errorlog-Verwaltung auf SDcard betreffen, wurden aus dem
		  'SDcardReaderExt' Form herausgenommen und in das 'SDcardReader' Form eingearb.
		  (betrifft sowohl UI als auch code)



- Item: "BUG: CTS: Es mu� gesichert werden, daߴdie �bermittlung fehlerhafter CTS-Daten vom device NICHT
		zur Nicht-Funktion der PC-SW f�hrt"

	Grund: Dieses Scenario kann eintreten nach einer Eeprom-Neupartitionierung bei der device SW-Entwickulng
		und nachfolgendem Sart der device Sw, OHNE da� die device SW-Versions-Nr. zuvor ge�ndert wurde
		-> dann werden aus einem Eeprom-Segment, in dem unter der bisherigen Eeprom-Partitionierung
		g�ltige Daten gehalten wurden, nunmehr irgendwelche (unsinnigen) Daten gelesen 
		-> Folge: s.o.
	

	==> Die Moral aus der Geschicht':
	Bei device SW-�nderungen, die Eeprom-Neupartitionierungen beinhalten, mu� stets eine neue 
	device Ver.-Nr. vergeben werden !!!


	Die u.a. Korrekturen sind als Sicherungs-Mechnismus "f�r alle F�lle ..." vorgenommen worden; bei
	vorheriger device-Neuversionierung w�rden sie nicht ben�tigt werden.


	- CTSForm.cs:

		- mod.: '_UpdateCTSData()' / Read
			- mod.: sec. "// Part 'Script sequence'"
				- neu: row "// (The script idx ...) ... in a safe manner"




- Item: "Immer 2 mA ausgeben au�er Ger�t befindet sich im Me�modus
 	 (FF-Liste, 7.7.13)"


	- class ScriptParameter:

		- neu: member 'byMeasScript' + eingearb.

		- neu in diesem Zus.: 'IsValidMeasScript()'


	- Exceptions.cs:

		- neu: c. 'ScrMeasScriptASMScriptException'


	- class Script:

		- mod.: 'Check()'
			- neu: sec. "// Check, whether the script is not simultaneously both a meas. script and ..."


	- class ScriptCollection:

		- mod.: 'Write()' / Section: Parameter
			- Store par. 'byMeasScript' (Meas. script ident.) eingearb.

		- mod.: 'Load ()'
			- mod.: catch-branch:
				- Test auf 'ScrMeasScriptASMScriptException' eingearb.


	- GSMReaderForm.cs:

		- mod.: 'AfterRXCompleted()' / case "STRESPARAM"
			- par. 'byMeasScript' eingearb.


	- ScriptEditorForm.cs:

		- mod.: UI
			- neu: controls '_lblMeasScript', '_chkMeasScript' 

		- mod.: '_InitControls()', 'Load()', '_ctl_HelpRequested()'
			- controls eingearb. 

		- mod.: '_ContentsToScript()', '_ScriptToContents()'
			- controls eingearb. / par. 'byMeasScript' eingearb.



- Item: "ErrorLog: Auf dem Display werden momentan alle Fehler angezeigt ...
	(FF-Liste, 7.7.13)"

	Real.: 	a) im Statusbar DE nicht anzeigen, aber Funktionalit�t bleibt erhalten
		   (Vorschlag: via service menu ein/ausblendbar gestalten)


	a)


	- ServiceForm.cs:

		- mod.: UI / section 'Forts.: Device'
			- neu: control '_chkErrConf'


		- mod.: 'ServiceForm_Load()', '_ctl_HelpRequested()', '_UpdateServiceData(bool)', 
			'_UpdateServiceData(string)'
			- error conf. stuff eingearb. einschl. ress.


		- mod.: '_CheckControls()'
			- comment bzgl. error conf. eingearb.



- Item: "Auch w�hrend des CTS-Betriebes mu� eine PC-Komm m�glich sein
	(FF-Liste, 7.7.13)"


	- ScanInfo.cs:

		- neu: member 'byScriptIdx' + eingearb. in class context

	- Measure.cs:

		- neu: enum 'StartMode'

		- neu: members 'ScriptIdxAfterChange', 'MeasureChanged'

		- mod.: 'StartMeasure()'
			- neu: parameter 'startMode'
			- neu: if-else bzgl. 'startMode' sowie Inhalt des else-Zweiges


	- MainForm.cs:

		- mod.: 'AfterRXCompleted()' / case "SCANALLDATA"
			- neu: sec. "// Check, whether a script change has taken place on device"

		- mod.: '_mi_Popup()', '_UpdateToolbarButtons()'
			- mod.: sec. "// Execute script"
				- raus: sec. "// Disabled, if CTS handling on the device is enabled"

		- mod.: '_Timer_Tick()'
			- neu: sec. "// Check, whether a script change has taken place on device "


	- Doc.cs:

		- neu: 'ScriptNameFromIdx()'


	- CmdUI.cs:

		- mod.: 'OnExecuteScript()'
			- 'StartMeasure()'-Aufruf parametriert



- Item: "Scripteditor: Cmds, die auf der gleichen Zeit sitzen, werden nach Editierung reihenfolgem��ig
	 ggf. anders eingeordnet. Um sie in die gew�nschte Ablaufreihenfolge zu bringen, sollten daher
	 Up/Down-buttons angebracht werden"


	- ScriptEditorForm.cs:

		- mod.: UI
			- mod.: sec. "Script commands"
				- neu: controls '_btnUp/Down'

		- neu: '_btnUpDown_Click()'

		- mod.: '_ctl_HelpRequested()'
			- '_btnUp/Down' eingearb.

		- mod.: '_Timer_Tick()'
			- '_btnUp/Down' eingearb. 



- Item: "BUG: CTS: wenn die scriptliste leer ist, und die Up/Down.buttons bet�tigt werden, 
	 K�NNTE eine Exc. ausgel�st werden, sofern nicht anderweitig abgefangen"

	-> es ist hier bereits anders abgefangen, aber f.a.F. nachfolg. Korrektur...

	- CTSForm.cs:

		- mod.: '_btnUpDown_Click()'
			- neu: try-catch-Block hinzugef�gt
		


- Item: "BUGGY: nach �ndern der device No im service dlg. sollte die DevNo-Anzeige im Main screen entspr. angepasst
		werden"


	- CmdUI.cs:

		- mod.: 'OnTransferService()'
			- neu: sec. "// Trigger device no. update in  main screen"

	- MainForm.cs:

		- mod.: 'AfterRXCompleted()'
			- neu: sec. "// Service-Read"



- Item: "Einarb.: Error-Handling BR"


	- Vw.cs:

		- mod.: 'InitResultPanel()'
			- mod.: sec. "Section 'Device information'"
				- mod.: sec. "Errors"
				- neu: sec. "Warnings"

		- mod.: 'UpdateResultData()
			- mod.: sec. "Section 'Device information'"
				- mod.: sec. "Errors"
				- neu: sec. "Warnings"
			- neu in diesem Zus. ress. 'cView_DeviceInfo_ErrWar_None'


	- MainForm.cs:

		- mod.: UI ( sec. 'Device information')
			- neu: Warning controls,
			       ACK button f�r errors	
		- neu in diesem Zus.: '_btnErrAck_Click()'		

		- mod.: 'AfterRXCompleted()'
			- mod.: case "SCANALLDATA"
				- neu: // Error / Warning Tooltip hints
			- neu: case "ERRCONF"

		- mod.: '_Timer_Tick()'
			- neu: sec. "// Check error state: Show/hide the Error acknowledge ('ACK') button corr'ly"


	- AppComm.cs:

		- neu: members 'msgEHRead', 'msgEHWrite'


	- ScanInfo.cs:

		- neu: member 'wWarning' + eingearb. in clas context
		- neu: members 'sWarningMsg[]', 'sErrorMsg[]'

		- neu: 'ErrorAsHint()',
			'WarningAsString()', 'WarningFromString()', 'WarningAsHint()'


	- Doc.cs:

		- mod.: 'FillDataStructures()'
			- neu: sec. "// Warnings on device"

		- mod.: 'RecordScanDataWriteCommonHeader()'
			- neu: sec. "// Row: Warning Word"


	- umbenannt: 'SDcardReaderExtForm' -> 'ServiceExtForm' (incl. title & ress.)


	- ServiceExtForm.cs:

		- mod.: UI
			- neu: GB 'Error handling' 
			- mod.: SDcard related controls -> eingebettet in GB 'SD card'

		- neu: member '_sEHdata'

		- mod.: 'ServiceExtForm_Load()'
			- neu: Error handling related stuff, nebst ress.
			- neu: sec. "// Read in initially the  error handling data"

		- neu: '_lvPercent_DoubleClick()',
			'_btnEHRead_Click()',
			'_btnEHWrite_Click()',

			'_UpdateEHData(),
			'_EHStringFromBytearray()',

		- mod.: 'ResetAll()',
			'UpdateData()'
			- neu: sec. "Error handling section"

		- mod.: 'AfterRXCompleted()'
			- neu: sec. "// Error handling messages" (EHREAD, EHWRITE)


	- neu: ServiceExtEHForm.cs



- Item: "Forts.: Einarb.: Error-Handling BR:
	 Error screen: im Statusbar DE anzeigen, aber Funktionalit�t des Anzeigens/Nicht-Anzeigens
	 potentiell im code erhalten (via service menu DE ein/ausblendbar gestalten, Funktionalit�t selbst
	 disablen)
	"


	- ServiceForm.cs:
		
		- neu: #def. 'SERVICE_ERRCONF_DISABLED'

		- mod.: 'Init()'
			- neu: sec. "// 'Error confirmation on / off' ChB: disabled"

		- mod.: '_UpdateServiceData (string sSvcFile)'
			- mod.: sec. "// Error confirmation: on/off "



JM,
5.-6.9.13



- Item: "Anpassung comm. handling gem�� IMS 7.0.0:
	 Die comm. buffer L�nge (TX) der PC-SW mu� der comm. buffer L�nge (RX) der device SW entsprechen"


	- CommRS232.cs:

		- mod.: c. 'CommRS232'

			- mod.: const 'TXBUFSIZE': 256 (bisher) -> 160 (neu)



- Item: "Anpassung comm. handling gem�� IMS 7.0.0:
	 ScriptWindow aktualisieren gem�� comm. handling in device SW 
	 (i.e.: 2 ScriptWindow-parts werden anstelle von bisher 1 part �bertragen)
	"


	- ScriptData.cs / c. ScriptWindow:

		- neu: 'ToSubString()'


	- ScriptTransferForm.cs:

		- mod.: '_SendScript()'
			- mod.: sec. "// TX: Script Window ( Write the script windows settings )" 




JM,
6.-9.9.13



- Item: "Anpassung comm. handling gem�� IMS 7.0.0:
	 Checks auf g�ltige ScriptParameter-/ScriptWindow-Stringl�ngen aktualisieren"


	- ScriptData.cs / c. ScriptParameter:
		
		- mod.: 'Check()'
			- script parameter parts eingearb.


	- ScriptData.cs / c. ScriptWindow:
		
		- mod.: 'Check()'
			- mod.: sec. "// Script window message string length check"
				- script window parts eingearb.




JM,
9.9.13


- Item: "Testing: SCANALLDATA nur alle sec. aufrufen, nicht hintereinanderweg wie bisher"


	- Measure.cs:

		- neu: const '_WAIT_ONCOMMEMPTY_PID'
		- neu: globals 'bOnCommEmpty', 'nOnCommEmpty'

		- mod.: 'StartMeasure()'
			- 'bOnCommEmpty=T; nOnCommEmpty=0;' anstelle von direktem 'OnCommEmpty' call
		- neu: 'PushOnCommEmpty()'


	- MainForm.cs:

		- neu: global '_nTimerCounter'

		- mod.: 'AfterRXCompleted()'
			- mod.: sec: "// The message list is empty: Push the commands, that should be permanenty executed"
				- 'bOnCommEmpty=T' anstelle von direktem 'OnCommEmpty' call

		- mod.: '_Timer_Tick()'
			- Intervall ge�ndert: 1 sec (bisher) -> 100 msec (neu)
			- Struktur ge�ndert (stuff: 100 msec - 500 msec - 1 sec)		
			- 'PushOnCommEmpty()' call eingearb.




- Item: "Testing: CTS and PC comm -> Einige Sachen funktionieren nicht wie gew�nscht"


	- Vw.cs:

		- mod.: 'UpdateGraph()'
			- sec. "// The window should be marked:" in try-catch gesetzt

		- neu: 'UpdateDI_ErrWar()'
		- mod.: 'UpdateResultData()'
			- 'UpdateDI_ErrWar()' eingearb.
	

	- Measure.cs:

		- raus: global 'ScriptIdxAfterChange'

		- mod.: 'StartMeasure()'
			- raus: sec. "// Remember the name of the changed script"
			- neu: sec. "// Condider the fact, that after script change on device already one DATA transfer has been performed"


	- MainForm.cs:

		- mod.: 'AfterRXCompleted()' / SCANALLDATA
			- mod.: NODATA sec.
				- neu: sec. "// Update partial view"
			- mod.: DATA sec.
				- mod.: sec. "                // Check, whether a script change has taken place on device" (1. occurence)
				- neu: sec. "// Check, whether a script change has taken place on device" (2. occurence)




JM,
12.-13.9.13



- Item: "In Analogie zum IMS: Scan screen zu- und abschaltbar gestalten via script (DISPHIDES cmd)"

	--> Note: Hier wird die alte DISPHIDES version verwendet: Par. in [0,1] = show/hide scan screen, def. 0


	- ScriptCommands.cs / c. ScriptCommands:

		- mod.: global 'arsc[]'
			- neu: item 'DISPHIDES'

		- mod.: 'HelpOnCmd()'
			- neu: case "DISPHIDES" nebst ress.



JM,
17.9.13


- Item: "Testing: BUG: Nach �bertragen eines script files (lt. PCCommListener asynchrones TX-RX nach comm. error) 
	 erschien eine zus�tzliche Window row im Result screen -> Grund?"

	 --> wahrsch. Grund:
		ein PC-CRC16 error ist nach SCRWINDOW 1,... transfer aufgetreten. Das cmd wurde erneut gesendet, 
		und legte auf dem device ein neues (�berfl�ssiges) window an.
	 --> L�sung:
		PC-CRC16 error NUR dann behandeln, wenn das cmd KEIn 'Set' cmd ist
		(Ein 'Set' cmd. returnt keine Info vom Ger�t, sondern lediglich ein 'OK'.)	

	 Die o.g. Asynchronit�t ist beim script transfer nicht weiter schlimm, da alle cmd's 'SET' cmds sind bis auf
	 das lezt gesendete (SCRLIST): daher wird dieses zur Sicherheit nochmals abgefragt.



	- CmdUI.cs:

		- mod.: 'OnTransmitConfigData()'
			- neu: sec. "// Security call: Update Script List"


	- AppComm.cs:

		- neu: global 'SetCmds[]'


	- CommMessage.cs:

		- neu: 'IsSetCmd()'


	- CommRS232.cs:

		- mod.: 'ThreadProc()'
			- mod.: sec. "// After RX: Calculate the CRC16 of the response byte array, and compare it with the transmitted one"
				- neu: 'if' zu "// Check, whether the message is a 'Set' message or not"



- Item: "Scripteditor: Der Zugriff via ALT-Taste ist nicht eindeutig"

	- ScriptEditor.cs:

		- mod.: UI
			- & sauber gesetzt




JM,
18.9.13



- Item: "BUG: Es wurde vergessen, die Test-Module an die ge�nderte Syntax der SCANALLDATA-Params anzupassen"


	- Test.cs:

		- mod.: 'TestArray_SCANALLDATA_Sim'
			- mod.: sec. "// 2. part: Scan info"

		- mod.: 'TestArray_SCANALLDATA_Real'
			- mod.: sec. "// 2. part: Scan info"


	- Test1.cs:

		- mod.: 'TestArray_SCANALLDATA_Sim'
			- mod.: sec. "// 2. part: Scan info"

		- mod.: 'TestArray_SCANALLDATA_Real'
			- mod.: sec. "// 2. part: Scan info"

JM,
25.9.13


******************************************************************************************
Backup:
- Ordner:		D:\Backups\MSVS2003Ent\WindowsApps\PID_IUT\PIDControl_Ver4_0_0_Park0
- korr. device SW: 	D:\Backups\IAR_EW\IUT\PID\PID_V4_0_0_Park0
******************************************************************************************

JM,
10.10.13



- Item: "Script transfer: �bertragung indizierter Items sicherer machen"


	- ScriptTransferForm.cs:

		- mod.: '_SendScript()'
			- mod.: sec. "// TX: Script Event ( Write the script event settings )"
				- Idx eingearb.

			- mod.: sec. "// TX: Script Window ( Write the script windows settings )"
				- Idx eingearb.



- Item: "Scripting:
	 1. sichern, dass innerhalb eines Scripts keine identischen cmd's vorhanden sind
	 2. sichern, da� der Name eines script windows nicht leer ist"
 

	- Exceptions.cs:
		
		- mod.: c. 'ScrWndNameInvalidException'
			-> auch auf leere window names ausgedehnt
			- mod.: ress. 'ScrWndNameInvalidException_Msg'

		- neu: c. 'ScrCmdEqualityException' nebst ress.


	- ScriptData.cs:

		- mod.: c. ScriptWindow
			- mod.: 'Load()', 'LoadFull()'
				- bisher: if (szSubstance.Length > MAX_SCRIPT_WINDOWSUBSTANCENAME) ...
				- neu:    if ((szSubstance.Length == 0) || (szSubstance.Length > MAX_SCRIPT_WINDOWSUBSTANCENAME)) ...

		- mod.: c. Script
			- mod.: 'Check()'
				- neu: sec. "// Checks, whether two script commands are identical (not allowed!) or not"




JM,
10.10.13



- Item: "Script transfer: NACH erfolgtem transfer communication schliessen & erneut starten
	 -> Goal: Komm.-buffer leeren"


	- CmdUI.cs:
		- neu: member '_nStartReConnect', '_nStepReConnect', '_bStartReConnect'
		- neu: const '_WAIT_DEV_RECONNECTION'

		- mod.: 'OnTransmitConfigData()'
			- neu: Notes bzgl device reconnection
			- neu: sec. '// Reconnect device'
			- raus: Notes zu sec. "// Security call: Update Script List" (diese wurden in die
				obigen Notes eingearb.)

		- neu: 'PushStartReConnect()'


	- MainForm.cs:
		- mod.: '_Timer_Tick()'
			- neu: sec. "// Where necessary: Reconnect the device"




JM,
11.10.13



- Item: "script transfer: damit das letzte cmd (SCRLIST) auch mit Sicherheit bedient wird, wird f�r diese message
	 als Zielfenster das main window angegeben (und nicht, wie bisher, das scripttransfer window, das 	
	 schon geschlossen sein kann und damit die Bedienung nicht mehr vollziehen kann) 


	- MainForm.cs:

		- mod.: 'AfterRXCompleted()' 
			- mod.: case "SCRLIST"
				- neu: sec. "// Testing only"


	- ScriptTransferForm.cs:

		- mod.: '_SendScript()'
			- mod.: sec. "// TX: Script list"
				- mod.: owner window (bisher: scripttransfer window, neu: main)
				- neu: Notes



- Item: "Ein PC-seitiger CRC16-Fehler soll in exception-message sauber vom device-seitigen CRC16-Fehler
	 unterschieden werden k�nnen"


	- CommRS232.cs:

		- mod.: 'ThreadProc()'
			- mod.: sec. "// Check, whether the message is a 'Set' message or not"
				- bisher: throw new CRC16Exception ();
				- neu: throw new CRC16Exception ("CRC failure - PC");



- Item: "Aus Sicherheitsgr�nden: RC2-encryption durch JM-encryption ersetzen
	 (Grund: Probleme mit finn. Kollegen im Aug. 2013)"


	- AppData.cs:

		- mod.: c. CommonData
			- mod.: Service-Extended password (encrypted)
				- bisher: RC2
				- neu: JM


	- ServiceForm.cs:

		- mod.: '_txtPW_TextChanged()'
			- mod.: sec. "// Get the stored password (decrypted)"
				- bisher: RC2
				- neu: JM



- Item: "BUG: 'Load scan from HD' funktioniert nicht sauber"


	- Doc.cs:

		- mod.: 'FillDataStructures()'
			- mod.: sec. "// Errors on device"
			- mod.: sec. "// Warnings on device"
				- neu: row "if (s == "None") s="";




JM,
14.10.13



- Item: "Konz-unit 'ppt' einarb."


	- ScriptData.cs:

		- mod.: c. 'ConcUnits'
			- neu: const 'ppt'
			- mod.: 'GetDecimals()'
				- ppt einbezogen


	- ScriptEditorWindowForm.cs:

		- mod.: 'Init()'
			- mod.: Items der ConcUnit CB
				- neu: item 'ppt'




JM,
15.10.13



- Item: "Script transfer: 
	 1. Da der Aufruf von SCRLIST nach erfolgtem transfer nunmehr nach Reconnection der
	 Schnittstelle erfolgt, ist ein Aufruf als letzter transferbefehl �berfl�ssig.
	 2. Nach erfolgtem trasfer sollte noch ein bi�chen bis zum Schliessen des Dialogs gewartet
	 werden, so da� die Komm.-prozesse (RX evaluation) definitiv zum Abschlu� gelangen k�nnen.
	"


	- ScriptTransferForm.cs:

		- mod.: '_SendScript()'
			- raus: sec. "// TX: Script list"

		- mod.: 'AfterRXCompleted()'
			- raus: sec. case "SCRLIST"

		- mod.: '_Timer_Tick()'
			- raus: sec. "// Check: Communication idle?"
			- daf�r neu: sec. "// Wait until communication is idle,  ..."



- Item: "Vereinheitlichung der Komm-Exception-Aufrufe f�r IMS/PID/Kombi"

	- CommMessage.cs:

		- mod.: 'CheckForCommError()'

			- bisher: Aufruf: exc = new CmdException ();
			- neu: Aufruf: exc = new CmdException ("Cmd failure - device");
			(analog f�r: CmdPar-, CRC16-, InputBufferExc.)



JM,
16.10.13



******************************************************************************************
Backup:
- Ordner:		D:\Backups\MSVS2003Ent\WindowsApps\PID_IUT\PIDControl_Ver4_0_0_Park1
- korr. device SW: 	D:\Backups\IAR_EW\IUT\PID\PID_V4_0_0_Park1
******************************************************************************************


JM,
4.11.13



- Item: "Device reconnection after script transfer: optional gestalten"


	- CmdUI.cs:

		- neu: #def. 'PUSH_DEVICE_RECONNECTION'

		- mod.: 'OnTransmitConfigData()'
			- 'PUSH_DEVICE_RECONNECTION' eingearb.



- Item: "PCCommListener files: Im Falle einer Exception sollte der Name der exc. mit ausgegeben
	 werden"

  
	- ExcMsgFile.cs:

		- neu: member 'm_ExcName'
		- neu: prop. 'ExcName'

		- mod.: 'AppendExcMsgFile()'
			- mod.: sec. "case AppendSpec.OnThreadProc_Catch:"
				- 'm_ExcName' eingearb.


	- CommRS232.cs:

		- mod.: 'ThreadProc()'
			- mod.: catch branch
				- mod.: sec. "// Exception message file: Append helper stuff"
					- 'ExcName' �bergeben




- Item: "Beim Debuggen: SCRLIST-row im output-window mit dem Empfangs-window versehen"


	- ConnectForm.cs:

		- mod.: 'AfterRXCompleted()'
			- mod.: sec. "case "SCRLIST":"
				- neu: Debug-Ausschrift eingearb.


	- MainForm.cs:

		- mod.: 'AfterRXCompleted()'
			- mod.: sec. "case "SCRLIST":"
				- mod.: Debug-Ausschrift: 'Main' eingearb.




JM,
5.11.13



- Item: "Pumpen: Pump On times Verwaltung einarbeiten"


	- ServiceForm.cs:

		- mod.: UI
			- mod.: sec. "Device Data"
				- neu: GB '_gbPumpOnTimes' + zugeh�rige controls


		- renamed: bisher: '_chkResetPIDlamp_CheckedChanged()'
			   neu: '_chkResetOnTimes_CheckedChanged()'	
		- mod.: '_chkResetOnTimes_CheckedChanged()'
			- stuff f�r P1/2/3 einbezogen, nebst ress.


		- mod.: 'ServiceForm_Load()'
			- Text & ress. f�r neue controls eingearb.
			- in diesem Zus.: 
				- raus: ress. 'Service_chkResetPIDlamp'
				- daf�r: ress. 'Service_chkReset'


		- mod.: '_UpdateServiceData (bool)' / Read, Write
			- mod.: sec. "//  Section: Device"
				- neu: sec. "//    P1/2/3On times"


		- mod.: '_UpdateServiceData (string)'
			- mod.: sec. "//  Section: Device"
				- neu: sec. "//    P1/2/3On times"



JM,
7.11.13



- Item: "BUG: Beim Testen der 'Load svc file' Funktionalit�t werden Fehler im error display angezeigt -> Grund?"


	-> Ursache: Unsaubere '_GetValue()' routine


	- ServiceForm.cs:

		- mod.: '_GetValue()'
			- sauber gefa�t



- Item: "BUG: Service dialog/Pump On times Verwaltung: Beim Updaten der svc data via (fr�herer) svc files sollten
	 die Pump-On times NICHT ver�ndert werden"


	- ServiceForm.cs:

		- mod.: '_UpdateServiceData(string)'
			- mod.: sec. "// Pump On times:"
				- neu: do not update these contents




- Item: "BUG: Service dialog/PID lamptime: Beim Updaten der svc data via (fr�herer) svc files sollte
	 die PID lamptime NICHT ver�ndert werden"


	- ServiceForm.cs:

		- mod.: '_UpdateServiceData(string)'
			- mod.: sec. "// PID Lamptime"
				- neu: do not update this content



JM,
12.11.13



- Item: "BUG: PC-seitiger CRC16-error:
         If a (PC-sided) comm. error is reported, we do NOT know, whether the message
         has been executed on device or nor - that means, the message should be repeated in any case 
	 true to the motto "In case of doubt once again ...". Hence the following 'Set' message check
         is incorrect and must not be performed. 
	"


	- CommRS232.cs:

		- mod.: 'ThreadProc()'
			- mod.: sec. "// After RX: Calculate the CRC16 of the response byte array, and compare it ..."
				- disabled: 'if' zu "// Check, whether the message is a 'Set' message or not"


	- CommMessage.cs:

		- mod.: 'IsSetCmd()'
			- neu: remarks: currently not needed

	- AppComm.cs:

		- mod: member 'SetCmds[]'
			- neu: remarks: currently not needed


JM,
3.12.13



******************************************************************************************
Backup:
- Ordner:		D:\Backups\MSVS2003Ent\WindowsApps\PID_IUT\PIDControl_Ver4_0_0_Park2
- korr. device SW: 	D:\Backups\IAR_EW\IUT\PID\PID_V4_0_0_Park2
******************************************************************************************

JM,
30.1.14



- Item: "Script editor: sauberer fassen: Aufteilung Script collection - <-> script edition auf 2 Forms"

	Grund: bisheriges script Add/Update (Problem: '_txtName' <-> _txtChanged J/N ?) sollte besser gefa�t werden

	(�bernahme von IMS 7.1.0)


	- ScriptData.cs:

		- c. ScriptParameter:

			- neu: const's 'MeasCycle_Min/Max/Fac/Def' (analog SigProcInt, PCCommInt)

			- mod.: 'Default()'
				- const's eingearb.

			- mod.: 'IsValidCycleTime()', 'IsValidSigProcInt()', 'IsValidPCCommInt()'
				- const's eingearb.



	- ScriptEditorForm.cs: 

		- mod.: UI
			- Script collection part 'rausgenommen und in neues Form 'ScriptCollEditorForm.cs' gesteckt
			- controls neu angeordnet
			- mod.: Titel
				- bisher : "Scripteditor"
				- neu: "Scripteditor - Script"

		
		- mod.: #tor
			- neu: sec. 'Init. ..."

		- mod.: 'Load()'
			- raus: Ressourcen f�r script coll. part 
			- neu: Ress. f�r '_btnOK'
			- mod.: sec. "// Control  initialisation"
				- neu: 'if' zu '// Editing is requested:'

		- raus: '_btnScript_Add_Click()',
			'_btnScript_Remove_Click()',
			'_lvScript_SelectedIndexChanged()',
			'_btnLoad_Click()',
			'_btnSave_Click()',
			'_txt_TextChanged()',
			'_cb_SelectedIndexChanged()',
			'_chk_CheckedChanged()'
			- no longer needed (z.T. in 'ScriptCollEditorForm.cs' �bergegangen)

		- neu: '_btnOK_Click()'
		
		- mod.: '_ctl_HelpRequested()',
			'_Timer_Tick()'
			- raus: script coll. part (z.T. in 'ScriptCollEditorForm.cs' �bergegangen) 


		- raus: member '_bChanged'
		- neu: members '_idx', '_scr', '_sScrNameOrig'

		
		- neu: '_Init()'

		- mod.: '_InitControls()',
			- raus: script coll. part
    			- neue ScriptParameterPID-const's eingearb.

		- raus: '_ScriptsToContents()'


		- neu: properties 'ScrColl', 'Index', 'Script'


		- mod. (raus bzw. Namen ge�ndert: alle die Ressourcen, die in 'ScriptCollEditorForm.cs' �bernommen wurden



	- neu: 'ScriptCollEditorForm.cs' nebst ress.

		- mod.: '_btnScript_Remove_Click()'
			- raus: Control  initialisation im Falle n=0
		
		- mod.: '_Timer_Tick()'
			- neu: sec. "// Check, whether a script file is in processing (True) or not (False)"


	- CmdUI.cs:

		- mod.: 'OnScriptEditor()'
			- 'ScriptCollEditorForm'-Aufruf ersetzt 'ScriptEditorForm'-Aufruf


	- ScriptEditorWindowForm.cs: 

		- mod.: Titel
			- bisher : "Script window"
			- neu: "Scripteditor - Window"


	- ScriptEditorCmdForm.cs: 

		- mod.: Titel
			- bisher : "Script command"
			- neu: "Scripteditor - Command"



- Item: "Buggy: script window: member 'SpanFac' wird mit 0 init., m��te aber mit '1' init. werden -> �ndern!"

	- ScriptEditorWindowForm.cs:

		- mod.: Init. von member '_SpanFac'
			- bisher: 0
			- neu: 1.0




JM,
30.1.14



- Item: "SDcardReader dialog: Es sollte m�glich sein, die gesamte SD-Karte auf einen Schub zum PC zu
	 transferieren"


	- SDcardReaderForm.cs:

		- mod.: UI
			- neu: Timer control '_TimerPrepSDTrans',
				Button control 'btnTransferSDcard'

		- neu: members '_bWait', '_nWait'

		- neu: '_TimerPrepSDTrans_Tick()',
			'btnTransferSDcard_Click (),
			'_WaitUntilCommIdle()',

		- mod.: 'SDcardReaderForm_Load()'
			- neu: Ress. f�r 'btnTransferSDcard' eingearb.

		- mod.: 'SDcardReaderForm_Closed()'
			- mod.: sec. "// Stop timer"
				- SDcard transfer preparation timer einbezogen



JM,
7.2.14



- Item: "Help system f�r einige Form's einarb."


	- ServiceExtForm.cs:
	- SDcardReaderForm.cs:

		- mod.: UI:
			- neu: 'HelpButton' prop. enabled
			- neu: member '_ToolTip'
			- neu: 'HelpRequested' prop. diverser controls aktiviert 

		- neu: '_ctl_HelpRequested()' nebst Ress.



JM,
10.2.14



- Item: "Flow-Anzeige rausnehmen, wie auch bereits in device SW gehandhabt
	 (Fertigungsrunde, 26.2.14)"


	- AppData.cs:

		- mod.: c. ProgramData

			- neu: member 'bDisplayFlow'


	- MainForm.cs:

		- mod.: '_Init()'
			- neu: sec. '// Flow display'



	- Vw.cs:

		- mod.: '_OnPrintParams()'
			- mod.: sec. "// 4. line: Flow & Battery"
				- Flow output depending on settings eingearb.



	- Doc.cs:

		- mod.: 'RecordScanDataWriteCommonHeader()'
			- mod.: sec. "// Row: Flows"
				- Flow output depending on settings eingearb.



JM,
27.2.14



- Item:  "Redesign SDcard reader form: Explorer-�hnliches design erstellen:
	  Anstelle der vielen Buttons und Listboxes mit einer folder TreeView und einer files ListView arbeiten" 
	 (�bernahme von IMS 3.1.0)


	- neu: 'SDcardReaderNewForm.cs' nebst Ress.

	- MainForm.cs:

		- mod.: 'HandleRX()'
			- mod.: sec. "// Dispatch the message acc'ing to the owner form"
				- 'SDcardReaderNewForm' einbezogen


	- CmdUI.cs:

		- mod.: 'OnReadSDcardStore()'
			- 'SDcardReaderNewForm' einbezogen


	- PropertiesForm.cs:

		- mod.: UI / sec. Program
			- neu: control '_chkSDcardReader'

		- mod.: 'PropertiesForm_Load()', 'PropertiesForm_Closed()'		
			- mod.: sec. 'Tab Program'
				- neu: sec. "// New SDcard reader style?"

		- mod.: 'Init()'
			- neu: sec. '// Visibility: ChB 'New SDcard reader style?''



	- AppData.cs:

		- neu: const 'sSDcardReaderNew'

		- mod.: c. ProgramData
			- neu: member 'bSDcardReaderNew'

		- mod.: '_ReadRegistry()', '_ReadResourceFile()',
			'_WriteRegistry()', '_WriteResourceFile()'
			- 'bSDcardReaderNew' eingearb.



JM,
27.2.14



- Item: "DateTime-Synchro exakter fassen: von min-Basis auf sec-Basis �bergehen"


	- ConnectForm.cs:

		- mod.: 'ConnectForm_Load()'
			- mod.: sec. "// TX: DateTime"
				- bisher: format "ddMMyyHHmm"
				- neu: format "ddMMyyHHmmss"



JM,
7.5.14





****************************************************************************
****************************************************************************

- Ver. 4.0.0 wurde von MSVS2003 nach MSVS2010 portiert und dort als Ver. 5.0.0 hinterlegt.

****************************************************************************
****************************************************************************


JM,
8.7.14



================================
5.0.0 (angelegt 8.7.14)
================================

(
Korr. Device-SW-Version: Ordner '..\IAR_EW\IUT\PID\PID_V4_0_0'
)


- based on: 4.0.0 (MSVS2003)


JM,
8.7.14



- Item: "Der specifier f�r bedingte Kompilierung 'NET_ ...', der f�r das (bisher genutzte) NET Framework 1.1
	 benutzt wurde, ist nunmehr �berfl�ssig."


	- App / Properties / Erstellen / Symbole f. bedingte Komp.:
		- raus: 'NET_1_1' (f. Debug u. Release)



- Item: "Bei der Portierung wurden Compilerfehler / - warnungen generiert, die beseitigt werden m�ssen / sollten"


	= Warnungen =


	- MessageBoxCent.cs:

		- mod.: 'Initialize()'
			- mod.: Par.-liste von 'SetWindowsHookEx()'
				- ersetzt: AppDomain.GetCurrentThreadId() (bisher) -> System.Threading.Thread.CurrentThread.ManagedThreadId (neu)


	= Errors =


	- CommRS232.cs:

		- neu: delegate 'SetCursorDelegate'
		- neu: 'SetCursor()'

		- mod.: 'ThreadProc()'
			- mod.: sec's "// Indicate: A transfer is in progres (Cursor: Wait, if req.)",
					"// Indicate: A transfer is no longer in progres (Cursor: Default, if req.)",
					"// Indicate: A transfer is no longer in progres (Cursor: Default, if req.)"
				- bisher: 	row: fOwner.Cursor = Cursors. ...
				- daf�r neu: 	row: SetCursor(fOwner, Cursors. ...) 


JM,
8.7.14




================================
5.1.0 (angelegt 8.7.14)
================================

(
Korr. Device-SW-Version: Ordner '..\IAR_EW\IUT\PID\PID_V4_1_0'
)


- based on: 5.0.0

- Doc.cs:
	- Device program version angepa�t


JM,
8.7.14



- Item: "EnkyLC einarb."
	(gem��: AES-Verschl�sselung lt. BAFA, FF, 9.5.14)
	
	(vorerst auf Basis des zur Verf�gung stehenden Test-Dongels)

	==> �bernahme von GSM 4.1.0


	- Images:

		- neu: 2 neue TB-Bitmaps
			- OpenServiceDataFile.bmp,
			  TransmitServiceData.bmp


	- neu: EnkyLC.cs


	- neu: ServiceDataFile.cs
		- neu in diesem Zus.: enum Logo (umgesetzt von ServiceForm.cs)


	- Crypt.cs:

		- neu: 'AESEncrypt()', 'AESDEcrypt()'


	- Doc.cs:

		- mod.: 'FxGetTempFilename()'
			- private -> internal

		- neu: enum 'CryptMode'

		- mod.: 'LoadScript()',
			'SaveScript()',
			'GetFileName()'
			- neu: Par. 'cm'
			- neu: CE nach crypt. mode (LoadScript() only)

		- mod.: 'GetFileName()'
			- bisher: Filename-member (ofd, sfd) stets init. 
			- neu:   Filename is displayed only if crypt. mode coincides!

		- neu: 'LoadServiceData()',
			'GetServiceFileName()'


	- AppData.cs:
		
		- raus: const's 'sPWExpert', 'sPWTC', 'sPWSvcExt'
		- neu: const 'sServiceFilename'

		- CommonData.cs:

			- neu: 'AES_KEY', 'AES_IV', 'USERPW'
			- neu: member 'sServiceFileName'

			- raus: 'INITIALPWE', 'arbyPWExpert'
				'INITIALPWT', 'arbyPWTC'
				'PW_SVCEXT', 'arbyPWSvcExt'

			- in diesem Zus. mod.:
				- _ReadRegistry()
				- _ReadResourceFile()
				- _WriteRegistry()
				- _WriteResourceFile()


	- AppComm.cs:

		- neu: 'msgScriptUserID',
			'msgGetUserIDs'


	- CmdUI.cs:

		- raus: OnPW(), OnPWNew ()
		- umben: OnOpenConfigFile() -> OnOpenConfigFile_Txt() 
		- neu: OnOpenConfigFile ()

		- neu: 'OnServiceDataEditor()',
			'OnOpenServiceDataFile()',
			'OnTransmitServiceData()'


	- ScriptData.cs:

		- mod.: c. ScriptCollection:
	
			- neu: const. 'UserID'
			- neu: member 'sUserID'

			- mod.: 'Load()'
				- mod.: sec. "// CD: Line mode / Top"
					- neu: sec. "// User ID"

			- mod.: 'Write()'
				- mod.: sec. "// Top"
					- UserID eingearb.



	- raus: PWForm.cs + ress.,
		PWNewForm.cs + ress.,
		PWQuitForm.cs + ress.


	- neu: ServiceDataTransferForm.cs


	- ScriptTransferForm.cs:

		- mod.: '_SendScript()'
			- neu: sec. "// TX: Script UserID"

		- mod.: 'AfterRXCompleted()'
			- neu: sec. "case "SCRUSERID":"

		- mod.: '_Timer_Tick()'
			- mod.: sec. "// Wait until communication is idle, ..."
				- bisher: 500 ms gewartet (_nIdle == 10)
				- neu: 1 s warten (_nIdle == 20)
			(Grund: Vereinheitlichung mit ServiceDataTransferForm)


	- ScriptCollEditorForm.cs:

		- mod.: UI
			- umben.: '_btnLoad' -> '_btnLoadPlainText', 
				'_btnSave' -> '_btnSavePlainText',
				'_btnLoad_Click()' -> '_btnLoadPlainText_Click()',				
				'_btnSave_Click()' -> '_btnSavePlainText_Click()',				

			- neu: GB's 'Plain text data' , 'Encrypted data'
			- neu: '_btnLoadEnc', '_btnSaveEnc' nebst Click-EH

		- mod: '_btnSavePlainText_Click()'
			- neu: sec. "// Update the 'UserID' Top line of the script collection"

		- mod.: 'Load()'
			- Ress. f. GB 'Plain text data', '_btnLoadPlainText', '_btnSavePlainText', 
				GB ''Encrypted  data', '_btnLoadEnc', '_btnSaveEnc'

		- mod.: '_ctl_HelpRequested()'
			- neu: passagen bzgl. '_btnLoadPlainText', '_btnSavePlainText', 
				'_btnLoadEnc', '_btnSaveEnc'

		- mod.: '_Timer_Tick()'
			- entspr. �berarb.: sec. "Section 'Generally'"


	- ServiceForm.cs:

		- raus: TB 'txtPW' + abh�ngige Rout. (_txtPW_KeyPress, _txtPW_TextChanged) + ress. (Service_Help_txtPW)

		- mod.: #tor
			- bisher: ohne Par.
			- neu: Par. 'bEditMode'

		- mod.: UI
			- mod.: sec. 'Pump On times'
				- neu: DontChange ChB's + event handler
			- mod.: sec. PID lamp'
				- neu: DontChange ChB + event handler

		- neu: member '_bEditMode'

		- mod.: 
			- bisher: const '_NROFSVCDATASTRINGS', private
			- neu: const 'NROFSVCDATASTRINGS', internal

		- mod.: 'ServiceForm_Load'
			- '_bEditMode' nebst Ress. eingearb.
				- neu: 'Service_Title_Editor', 'Service_Title_Online'
				- daf�r raus: 'Service_Title'
			- neu: Ress. f�r DontChange ChB's
			- raus: sec. "// Update the form"

		- mod.: '_chkPeriodicUpdate_CheckedChanged()'
			- raus: sec. "// Update the form"

		- mod.: '_btnRead_Click()',
			'_btnWrite_Click()'
			- neu: if-else zu "// CD: Editor mode?", Inhalt des if-branches
					
		- mod.: '_ctl_HelpRequested()'
			- '_bEditMode' nebst Ress. eingearb.
				- neu: 'Service_Help_btnLoadFromFile', 'Service_Help_btnSaveToFile'
			- neu: Help f�r DontChange ChB's

		- mod.: '_TimerPeriodicUpdate_Tick()'
			- neu: if zu "// CD: Editor mode?"

		- mod.: '_Timer_Tick()'
			- neu: sec. "// Update the form"


		- mod.: 'Init()'
			- bisher: ohne Par.
			- neu: Par. 'bEditMode'

			- neu; sec. "// Show/Hide controls depending on the edit mode"
			- neu: sec. "// Initiate Logo"
			- neu: sec. "// Overgive"

		- mod.: '_CheckControls()'
			- neu: sec. "// Pump On Times -> need for check only in editor mode"

		- mod.: 'UpdateData()'
			- neu: if-else zu "// CD: Editor mode?", Inhalt des if-branches
			- raus: PW-related stuff

		- mod.: '_UpdateServiceData()'
			- mod.: sec. '// Pump On times:'
				- '_bEditMode' eingearb.

		- mod.: 'AfterRXCompleted()'
			- mod.: sec. "// A comm. error (Timeout, ...) occurred:"
				- raus: sec. "// Update the form" 
			- mod.: sec. "// The message response is present:"
				- raus: sec. "// Update the form" 

		- raus: enum Logo (umgesetzt nach ServiceDataFile.cs)

		- raus: '_GetValue()' (wird umgesetzt nach 'ServiceDataFile.cs')

		- raus: #def 'SERVICE_ERRCONF_DISABLED'
		- mod. in diesem Zus.: '_Init()', '_UpdateServiceData (string)'
			- raus: 'SERVICE_ERRCONF_DISABLED'-Passagen



	- ServiceExtForm.cs:

		- mod.: UI:
			- neu: User ID ListView

		- neu: 'ReadUserIDs()'

		- mod.: 'Load ()'
			- 'ReadUserIDs()' impl.

		- mod.: 'AfterRXCompleted()'
			- neu: sec. "case "GETUSERIDS":"


	- MainForm.cs:

		- mod. '_Timer_Tick()'


		- raus: '_miFile_PW', '_miFile_PWNew'
			- damit verbunden mod.:
				- _mi_Click()
				- _mi_Popup()
				- UpdateCulture()
				- UpdateAccordingUserMode()
			- damit verbunden raus ress.:
				- Main_miFile_PW, Main_miFile_PWQuit, Main_miFile_PWNew,
				  Main_miFile_PW_Select, Main_miFile_PWQuit_Select, Main_miFile_PWNew_Select

		- umben.: '_miFile_OpenConfigFile' -> '_miFile_OpenConfigFile_Txt'
			- damit verbunden mod.:
				- _mi_Click()
				- _mi_Popup()
				- UpdateCulture()
			- damit verbunden umben. ress.:
				- Main_miFile_OpenConfigFile -> Main_miFile_OpenConfigFile_Txt,
				- Main_miFile_OpenConfigFile_Select -> Main_miFile_OpenConfigFile_Txt_Select

		- neu: '_miFile_OpenConfigFile'
			- damit verbunden mod.:
				- _mi_Click()
				- _mi_Popup()
				- UpdateCulture()
			- damit verbunden neu ress.:
				- Main_miFile_OpenConfigFile, Main_miFile_OpenConfigFile_Select

		- mod.: UpdateAccordingUserMode()


		- neu: MI '_miFile_ServiceDataEditor', '_miFile_OpenServiceDataFile',
			  '_miControl_TransmitServiceData'
			- damit verbunden mod.:
				- _mi_Click()
				- _mi_Popup()
				- _Init()
				- UpdateCulture()
			- damit verbunden neu Ress.:
				- Main_miFile_OpenServiceDataFile, Main_miFile_ServiceDataEditor,
				  Main_miControl_TransmitServiceData
				  Main_miFile_OpenServiceDataFile_Select, Main_miFile_ServiceDataEditor_Select,
				  Main_miControl_TransmitServiceData_Select

		- mod.: TB
			- 2 neue TBB's:
				- _tbbOpenServiceDataFile, _tbbTransmitServiceData
				- damit verbunden mod.:
					- Init()
					- _UpdateToolbarButtons()
					- UpdateCulture ()
				- damit verbunden neu Ress.:
					Main_tbbOpenServiceDataFile, Main_tbbTransmitServiceData

		- mod.: member 'ImageList'
			- 2 neue bmp's included:
				- OpenServiceDataFile.bmp,
				  TransmitServiceData.bmp

		- mod.: 'HandleRX()'
			- mod.: sec. '// Dispatch the message acc'ing to the owner form()'
				- neu: ServiceDataTransferForm einbezogen



	- AppNotes.txt:

		- raus: comment zu 'SERVICE_ERRCONF_DISABLED'




JM,
9.-10.7.14
14.-18.7.14



- Item: "Real-life dongles eingearb."

	- EnkyLC.cs:

		- raus: 'DeveloperID'
		- daf�r neu: 'DeveloperID_Test', 'DeveloperID_RealLife'

		- neu: 'OpenDongle()'

		- mod.: 'Check()'
			- mod.: sec. "// Check: Enky LC present?"
				- ersetzt:
					- bisher: "int res = LC_open(DeveloperID, 0, ref handle);"
					- neu: "int res = OpenDongle(ref handle);"


JM,
19.8.14




- Item: "BUG: SDcards mit Blocksize > 512B werden nicht akzeptiert"
	(�bernahme von PID Ver. 4.0.1)


	- AppComm.cs:

		- neu: messages "SDGETRIN", "SDRESETRIN"

	- ServiceExtForm.cs:

		- mod.: UI:
			- neu: sec. "SDcard reinitialisations"

		- in diesem Zus.

			- mod.: 'ServiceExtForm_Load()'
				- neu: Ress. zu sec. "SDcard reinitialisations"
				- neu: sec. "// Read in initially the SDcard reinitialisations data"

			- neu: 'btnSDreinitRead_Click()', 'btnSDreinitReset_Click()'

			- mod.: '_ctl_HelpRequested()'
				- mod.: "// SDcard section / Sdcard reinitialisations"
					- neu: Btn's 'Read', 'Reset' nebsr Ress. eingearb.

			- mod.: 'UpdateData()'
				- neu: sec. "// SDcard reinitialisations section"

			- mod.: 'AfterRXCompleted()'
				- neu: sec's "case "SDGETRIN":", "case "SDRESETRIN":"




- Item: "Service dialog: Datetime und Service date sollten try-catch-m��ig getrennt behandelt werden"
	(�bernahme von PID Ver. 4.0.1)


	- ServiceForm.cs:

		- mod.: '_UpdateServiceData(boolean)' / READ
			- mod.: sec. "// Section: DateTime"
				- bisher: try-catch en bloque
				- neu: try-catch detailed ('DateTime' und 'ServiceDate' error spec. entspr.)

		- mod.: '_UpdateServiceData(string)'
			- mod.: sec. "// Section: DateTime"
				- bisher: try-catch en bloque
				- neu: try-catch detailed ('DateTime' und 'ServiceDate' error spec. entspr.)

		- mod.: '_WriteErrors()'
			- 'ServiceDate'-Zeile eingef�gt

		- mod.: enum 'ReadDataError'
			- neu: member 'ServiceDate'




JM,
31.10.14



- Item: "Die Firma mu� aktualisiert werden"

	- AboutForm.cs:

		- ENIT gegen IUTmedical ausgetauscht


JM,
4.11.14
 



- Item: "SerialPort einarb."

	- CommRS232.cs:

		- raus: enum 'enum ReadMode'

		- raus: globals 'm_hCom', 'm_eReadMode'
		- neu: global 'm_SerialPort'

		- raus: 'Exists()'

		- mod.: 'IsOpen()',
			'IsClosed()',
			'IsIdle()'
			- 'm_SerialPort' eingearb.

		- raus: 'DestroyThread()'

		- mod.: 'ThreadProc()'
			- 'm_SerialPort' eingearb.

		- mod.: 'WriteMessage()'
			- 'm_SerialPort' eingearb.

		- mod.: 'RepeatLastMessage()'
			- 'm_SerialPort' eingearb.
			- mod.: ret.-Typ: bool -> void

		- mod.: 'OpenChannel()'
			- 'm_SerialPort' eingearb.
			- daf�r raus: WinAPI calls

		- mod.: 'CloseChannel()'
			- raus: 'SetCommMask()'

		- raus: 'ReadBytes()',
			'ReadBytes_Single()'

		- renamed: 'ReadBytes_Multi()' -> 'ReadBytes()'

		- mod.: 'ReadBytes()'
			- 'm_SerialPort' eingearb.
			- daf�r raus: WinAPI calls

		- mod.: 'WriteString()'
			- 'm_SerialPort' eingearb.
			- daf�r raus: WinAPI calls
			- raus: 'protected virtual' ret. Spez.

		- mod.: 'GetAvailableComPorts()'
			- 'SerialPort' eingearb.
			- daf�r raus: 'EnumCommPorts()'-call

		- raus: 'EnumCommPorts()'

		- raus: '#region Win API'



JM,
18.11.14


- Item: "BUG: gespeicherte Dateien: der Hundertstel-sec. part des Datums ist gelegentlich negativ"
	(Hinweis DR, 6.1.15)


	- CmdUI.cs:

		- mod.: 'OnTest()'
			- neu: sec. "//  DateTime - msec"


	- Doc.cs:

		- mod.: 'RecordSingleScan()',
			'RecordScan()',
			'RecordResult()'
			- ersetzt: '(Environment.TickCount % 1000)' -> 'tim.Millisecond'
 


- Item: "BUG: Speichern von Daten: Es wird bisher nicht fehlerm��ig abgefangen, ob das per settings eingestellte Datenspeicherungs-Verz. ex. oder nicht"


	- Doc.cs:

		- mod.: 'RecordSingleScan()'
			- neu: sec. "// Preparation" wird in try-catch einbezogen
			- neu: L�schen des temp. files in den catch-Bl�cken
			- neu: MsgBox-Aufruf in catch-Bl�cken mit caption & Ikone

			- mod.: Ress. 'cDoc_Record_SingleScan_FileCreate_Error'
				- bisher: "Aufzeichnung des aktuellen Spektrums: Fehler beim Initialisieren der Datei."
				- neu: "Aufzeichnung des aktuellen Spektrums: Fehler beim �ffnen der Datei."

		- mod.: 'RecordScan()'
			- neu: sec. "// Preparation" wird in try-catch einbezogen
			- neu: L�schen des temp. files in den catch-Bl�cken
			- neu: MsgBox-Aufruf in catch-Bl�cken mit caption & Ikone

		- mod.: 'RecordResult()'
			- neu: MsgBox-Aufruf in catch-Bl�cken mit caption & Ikone



- Item: "Speichern von Daten: Die Messages nach erfolgter Datenspeicherung sollten etwas bunter sein."


	- Doc.cs:

		- mod.: 'RecordData()',
			'RecordSingleScanWithMessage()'
			- neu: MsgBox-Aufrufe mit caption & Ikone



JM,
7.1.15
(Nachtr�glich eingearb.)



- Item: "BUG: Im Diagnosis code geistern noch 'ENIT GmbH' und deren email-Adresse 'rum -> durch IUT Medical ersetzen!"


	- Diagnosis.cs:

		- mod.: sec. "// Message: Diagnosis performed (Result: zip file). Send this file?"
			- mod.: Ress. 'cDiagnosis_Info_PerformedAndSend'
		- mod.: sec. "// Build the mailto string"
			- mod.: "info@environics-iut.com" -> "info@iut-medical.com"
			- mod.: Ress. 'cDiagnosis_Term_Body'


JM,
2.2.15
(Nachtrag von Version 5.0.2)


- Item: "Konzentrationsangaben f�r unit 'ppb' sollten mit 3 Dezimaldigits erfolgen (SB, 30.3.15)"

	- ScriptData.cs:

		- mod.: c. ConcUnits

			- mod.: sec. 'constants'
				- neu: sec. ' No of decimal digits'


			- mod.: 'GetDecimals()'
				- o.a. constants eingearb.



JM,
31.3.15


- Item: "Flow stuff (UI-Anzeige, printing, file storage) soll aktiviert werden, wenn Expert zugange ist (SM, 1.6.15)"


	- AppData.cs:

		- mod.: c. ProgramData

			- mod.: remarks zu global 'bDisplayFlow'


	- MainForm.cs:

		- mod.: '_Init()'
			- mod.: sec. "// Flow display"
				- Notes gel�scht

		- mod.: '_Timer_Tick()'
			- neu: sec. "// Flow display"


	- Doc.cs:

		- mod.: 'RecordScanDataWriteCommonHeader()'
			- mod.: sec. "// Row: Flows"
				- Notes gel�scht


	- Vw.cs:

		- mod.: '_OnPrintParams()'
			- mod.: sec. "// 4. line: Flow & Battery"
				- Notes gel�scht


JM,
3.6.15
(Nachtr�glich eingearb.)



================================
5.2.0 (angelegt 12.12.14)
================================

(
Korr. Device-SW-Version: Ordner '..\IAR_EW\IUT\PID\PID_V4_2_0'
)


- based on: 5.1.0

- Doc.cs:
	- Device program version angepa�t


JM,
12.12.14




- Item: "MFC f�r Flow2 einarbeiten"
	(�bernahme von IMS Ver. 7.1.1(dev)/4.0.1(PC))


	- DeviceInfo.cs:

		- mod.: member int 'nFlow2' -> double 'dFlow2'
		  	+ entspr. �nderungen im class context	

	- Diagnosis.cs:

		- mod.: 'Append'
			- sec. " // F1-3" entspr. angepa�t


	- Doc.cs:

		- mod.: 'RecordScanDataWriteCommonHeader()'
			- sec. "// Row: Flows" entspr. angepa�t

		- mod.: 'FillDataStructures()'
			- sec. "// Flow1-3" entspr. angepa�t
			- sec. "// Check: If the 'Flow1-3' entry was NOT found, then assign: 0" entspr. angepa�t


	- Vw.cs:

		- mod.: 'UpdateResultData()'
			- sec. "// Section 'Device information'" entspr. angepa�t

		- mod.: '_OnPrintParams()'
			- sec. "// 4. line: Flow & Battery" entspr. angepa�t



	- ScriptCommands.cs:

		- mod.: global 'arsc'
			- neu: member 'MFC'

		- mod.: 'HelpOnCmd()'
			- neu: sec. "case "MFC":" nebst ress.



	- ServiceForm.cs:

		- mod.: enum 'ReadDataError'
			- neu: member 'MFC'

		- mod.: UI:
			- neu: label 'MFC capacity' und zugeordnete TB

		- mod.: 'ServiceForm_Load()'
			- neu: Ress. f�r label 'MFC capacity' nebst ress.

		- mod.: '_ctl_HelpRequested()'
			- neu: sec. "// MFC cap." nebst ress.


		- mod.: '_CheckControls()'
			- neu: sec. "//  MFC cat."
			- mod.: sec. "// F2"
				- mod.: sec. "//  Soll"
					- int -> float
					- neu: error section nebst ress.
				- mod.: sec. "//  Soll - abs. tolerance"
					- int -> float
					- mod.: error section nebst ress.

		- mod.: '_UpdateServiceData()'
			- mod.: sec. READ
				- mod.: Notes (bzgl. 'MFC debit', 'MFC capacity')
				- mod.: sec. "// Section: Sensor"
					- neu: sec. "// MFC debit"
					- neu: sec. "// MFC capacity"
				- mod.: sec. " // Flow" / F2
					- int -> float

		- mod.: '_UpdateServiceData(string)'
			- mod.: sec. "// Section: Sensor"
				- mod.: sec. "// 2.2. F2"
					- int -> float

		- mod.: 'Init()'
			- neu: sec. "// Init. MFC capacity TB (required in principle only in Editor mode, but it is not harmful to do it in general)"





- Item: "ServiceForm: Amplifier scale l��t nur Eingabe von 2 chars zu -> zu wenig"


	- ServiceForm.cs:
		- mod.: UI
			- mod.: TB '_txtAmplScale': prop. 'MaxLength': 2 -> 5



JM,
12.12.14



-----------------------------------------------------------------------------
Hier wird old-Version PIDControlJM_5_2_0_old angelegt.
-----------------------------------------------------------------------------



( Die folgenden Items werden von GSM-Version 4.2.0 �bernommen.)


- Item: "Passwort-Schutz anders fassen (SM/FF, August 2015)"

	- ServiceForm.cs:

		- mod.: UI
			- neu: ChB '_chkPWOnOff', TB '_txtPW'

		- mod.: '_CheckControls()'
			- neu: sec. "// PW on/off -> no need to check"
			- neu: sec. "// PW"

		- mod.: '_ctl_HelpRequested()'
			- neu: context help f�r '_chkPWOnOff', '_txtPW'

		- mod.: 'ServiceForm_Load()'
			- neu: Ress. f�r '_chkPWOnOff', '_lblPW'

		- mod.: '_UpdateServiceData(bool)'
			- mod.: case WRITE
				- mod.: '_arsServiceData[0]': '_chkPWOnOff'-, '_txtPW'-values eingearb.
			- mod.: case READ
				- '_chkPWOnOff'-, '_txtPW'-control-Aktualisierung eingearb.

		- mod.: '_UpdateServiceData(string)'
			- neu: sec. "// PW check: on/off"
			- neu: sec. "// PW"


	- ServiceDataFile.cs:

		- mod.: 'ServiceDataAsFileString()'
			- neu: sec. "// PW check: on/off"
			- neu: sec. "// PW"

		- mod.: 'ServiceDataAsStringArray()'
			- neu: sec. "// PW check: on/off"
			- neu: sec. "// PW"




- Item: "Test: ServiceData �berspielen"

	-> Festgestellt: Der device-script mu� unterbrochen werden beim �berspielen.
			 Bisher werden die Servicedaten dagegen ohne Unterbrechung des lfd. scriptes �bertragen.
	   Grund: Im Resultat des �berspielens neuer Service-daten werden script-Daten ge�ndert (z.B. Pumpen-Einstellungen),
		  was ein �berspielen ohne Unterbrechung des lfd. scriptes verbietet.


	- ServiceDataTransferForm.cs:

		- neu: globals '_bTransferInProgress', '_sLastScriptName', '_arsServiceData', '_currDataType'

		- mod.: 'ServiceDataTransferForm_Load()'
			- raus: row 'enable timer'

		- mod.: 'ServiceDataTransferForm_FormClosed()'
			- neu: sec. "// Check: Is a Transfer process still in progress?"


		- mod.: '_Timer_Tick()'
			- neu: sec. "// Check: Transfer in progress? "

		- mod.: '_SendServiceData()'
			- neu: sec. "// Indicate that the Transfer process has begun."
		        - neu: sec. "// Enable timer"
			- neu: sec. "// TX: Script Current"
			- neu: sec. "// TX: Transfer Start"
			- neu: abschlie�ende 'Notes' section

		- neu: '_EndTransfer()'

		- mod.: 'AfterRXCompleted()'
			- neu: sec. "//  Common Transfer messages"
			- mod.: sec. "case "SVCWRITEDATA":"
				- neu: sec. "// After receipt of the answer of the last WriteData cmd SVCWRITEDATA, the transfer should be finished"



- Item: "Test: Die Textfelder in den script- und servicedaten-transfer-dialogen sind zu gering dimensioniert
	 (ein langer dateiname passt nicht rein) -> vergr��ern!"

	- ScriptTransferForm.cs:
	- ServiceDataTransferForm.cs:
		- in der H�he vergr��ert 



- Item: "BUG: ServiceDataTransferForm: wenn nach Bet�tigen des "servicedata �bertragen" TBB's die message 'Kein Servicefile geladen'
	      erscheint, wird nach dessen Best�tigung trotzdem (f�lschlicherweise) der Transferdialog ge�ffnet"	


	- mod.: '_SendServiceData()'
		- mod.: sec. "// Check: Valid service data file string?"
			- bisher: return im errorfall
			- neu: throw im errorfall
		- mod.: sec. "// Update the DateTime item in the Service data file string"
			- bisher: return im errorfall
			- neu: throw im errorfall



- Item: "BUG: Bei Anzeige von Warnung-ID2 ist die Tooltip-Box unvollst�ndig (Warnungstext fehlt)"

	- ScanInfo.cs:

		- mod.: global 'sWarningMsg'
			- Text f�r ID2 eingearb. 



- Item: "BUG: ServiceForm: passwort wird plain text angezeigt -> das mu� in Abh. von den Zugriffsberechtigungen ggf. maskiert werden!"

	- ServiceForm.cs:

		- mod.: 'UpdateData()'
			- neu: sec. "// Common for both modi: // Password: "



- Item: "SDcardReader: Service files d�rfen nur mit Zugriffsberechtigung (Enky) angeschaut/transferiert werden"

	- SDCardReaderForm.cs:

		- neu: '_Timer_Svc' control

		- mod.: 'SDcardReaderForm_Load()'
			- neu: sec. "// Enable Service file timer"

		- mod.: 'SDcardReaderForm_Closed'
			- neu: row '(stop) Service file timer'

		- neu: '_Timer_Svc_Tick()'

		- mod.: 'UpdateData()'
			- mod.: sec. "// Special case: Dis-/Enable the Select buttons"
				- mod.: row "// Enabled state of the Select Service file button depends on access permissions!"

		- neu: globals '_bSvcEnableOnce', '_bSvcDisableOnce'



- Item: "Fall 'MFC not present' in code einarbeiten (wurde vergessen)"


	- ServiceForm.cs:

		- mod.: '_CheckControls()'
			- mod.: sec. "//  Check Soll vs. MFC capacity -> need for check only, if not in editor mode"
				- neu: if zu "//  Check: MFC present?

		- mod.: 'AfterRXCompleted()'
			- mod.: sec. "case "SVCREADDATA":"
				- neu: sec. "// Adjust Enabled state: TB's F2 (MFC controlled) Soll, F2 (MFC controlled) Soll - abs. tolerance"



- Item: "BUG: ServiceForm: Enabled-state von Btn Extended nicht sauber gefa�t"


	- ServiceForm.cs:

		- mod.: 'UpdateData()'
			- mod.: sec. "// Button 'Extended"
				- 'bEn' eingearb.



- Item: "BUG: Die PC-SW l�uft unter Win7, aber nicht unter WinXP (Error: badFormatException)"

	Grund: EnkyLC: Unter Win7 wird die 64bit DLL ben�tigt, unter XP die 32bit DLL.
		Bisher war nur die 64 bit DLL beigef�gt.



	neu: c. EnkyLC32, EnkyLC64 (basierend auf der bisherigen c. EnkyLC)
	mod.: c. EnkyLC
		- wird als wrapprer class f�r EnkyLC32 resp. EnkyLC64 gestaltet





JM,
31.8.15



- Item: "ServiceForm: PW-Eingabe: 
	 - wenn erste Zahl keine 0, werden nur 3 Zahlen/ Sternchen angezeigt
	 - wenn 4 mal die 0, wird nur 1 Sternchen angezeigt
	 -> kein Fehler, aber etwas verwirrend"


	- ServiceForm.cs:

		- mod.: '_UpdateServiceData(bool)' / sec. READ
			- mod.: sec. "// PW"
				- bisher: PW wird 'as it is' angezeigt
				- neu: PW wird mit exakt 4 Stellen angezeigt (PadLeft usage)

		- mod.: '_UpdateServiceData(string)'
			- mod.: sec. "// PW"
				- bisher: PW wird 'as it is' angezeigt
				- neu: PW wird mit exakt 4 Stellen angezeigt (PadLeft usage)


		- mod.: '_CheckControls()'
			- mod.: sec. "// PW"
				- MsgBox mit Info eingearb.
				- neu in diesem Zus.: ress. 'Service_Check_Device_PW_Err'


	- Ressources:

		- mod.: 'Service_Help_txtPW'
			- exakter gefa�t



JM,
11.9.15



================================
5.3.0 (angelegt 14.1.16)
================================

(
Korr. Device-SW-Version: Ordner '..\IAR_EW\IUT\PID\PID_V4_3_0'
)


- based on: 5.2.0

- Doc.cs:
	- Device program version angepa�t


JM,
14.1.16




- Item: "TotalVOC einarb. (FF, 4.12.15)"

	- Exceptions.cs:

		- neu: 'ScrWndTotalVOCException' nebst Ress.
		- neu: 'ScrTotalVOCOffsetCorrException' nebst Ress.


	- ScriptsData.cs:

		- c. ScriptWindow:

			- mod.: 'Check()'
				- neu: sec. "// Check: TotalVOC windows are allowed only with calibration type 'area'"


		- c. ScriptCollection:

			- mod.: 'Load()'
				- mod.: catch-branch
					- 'ScrWndTotalVOCException' eingearb.
					- 'ScrTotalVOCOffsetCorrException' eingearb.


		- c. Script:

			- mod.: 'Check()'
				- neu: sec. "// Check, whether a script with a TotalVOC window has the static scan offset correction enabled (this is not recommended) or not"




JM,
15.1.16



-----------------------------------------------------------------------------
Hier wird old-Version PIDControlJM_5_3_0_old angelegt.
-----------------------------------------------------------------------------



- Item: "Einarb.: Redesign ServiceForm (TabControl-Nutzung)"


	Grund: Das bisherige ServiceForm ist sehr hoch, so da� es bei kleineren Laptops (JL) nur z.T. dargestellt
	       wird -> die Read/Write ... Buttons werden so NICHT mehr dargestellt, k�nnen auch nicht ins Bild geholt werden,
	       womit das Form nicht mehr bedienbar ist!


	- neu: ServieNewForm.cs nebst Ress.

	- ServiceForm.cs,
	  ServieNewForm.cs:

		- raus: const 'NROFSVCDATASTRINGS'

		- mod.: globals '_arsServiceData', '_anzDataStrings'
			- mod.: Konstanten-Zuweisung

	- ServiceDataFile.cs

		- neu: const 'const int NROFSVCDATASTRINGS'

		- mod.: 'ServiceDataAsStringArray()'
			- mod.: local 'arsServiceData'
				- mod.: Konstanten-Zuweisung


	- MainForm.cs:

		- mod.: 'HandleRX()'
			- mod.: sec. "// Dispatch the message acc'ing to the owner form"
				- ServieNewFormeinbezogen


	- CmdUI.cs:

		- mod.: 'OnTransferService()',
			'OnServiceDataEditor()'
			- 'ServieNewForm' einbezogen


	- PropertiesForm.cs:

		- mod.: UI / sec. Program
			- neu: control '_chkService'

		- mod.: 'PropertiesForm_Load()', 'PropertiesForm_Closed()'		
			- mod.: sec. 'Tab Program'
				- neu: sec. "// New Service dialog style?"

		- mod.: 'Init()'
			- neu: sec. '// Visibility: ChB 'New Service dialog style?'



	- AppData.cs:

		- neu: const 'sServiceNew'

		- mod.: c. ProgramData
			- neu: member 'bServiceNew'

		- mod.: '_ReadRegistry()', '_ReadResourceFile()',
			'_WriteRegistry()', '_WriteResourceFile()'
			- 'bServiceNew' eingearb.


	- Common.cs:

		- mod.: 'Execute()' (2 x)
			- mod.: catch-branch
				- neu: sec. "// Find the TabPage (if any), that contains the erroneous control, ..."



- Item: "Die Help comments f�r das ServieForm und das ServiceNewForm sind unvollst�ndig -> vervollst�ndigen!"

	- ServiceForm.cs,
	  ServieNewForm.cs:
		
		- mod.: '_ctl_HelpRequested()'

			- Control-Help vervollst�ndigt, incl. Ress.




----------------------------------------------------------
Ab hier wird die neue 64-bit DLL f�r EnkyLC verwendet.
----------------------------------------------------------



JM,
6.4.16




================================
5.3.1 (angelegt 7.7.16)
================================

(
Korr. Device-SW-Version: Ordner '..\IAR_EW\IUT\PID\PID_V4_3_0'
)


- based on: 5.3.0

- Doc.cs:
	- Device program version angepa�t


JM,
7.7.16


- Item: "Das Erscheinungsbild von Service-Dialog und SDcardReader-Dialog soll auch vom normalen user eingestellt werden
	 k�nnen, nicht nur wie bisher von E/T (SM, 4.7.16)"


	- PropertiesForm.cs:

		- mod.: 'Init()'
			- raus: sec. "// Visibility: ChB 'New Service dialog style?'"
			- raus: sec. "// Visibility: ChB 'New SDcard reader style?'"


	- CmdUi.cs:

		- mod.: 'OnServiceDataEditor()'
			- mod.: sec. "// CD: Service dialog style"
				- bisher: bool bUseNewServiceStyle = (app.Data.MainForm.User != UserType.Customer) && app.Data.Program.bServiceNew;
				- neu: bool bUseNewServiceStyle = app.Data.Program.bServiceNew;

		- mod.: 'OnTransferService()'
			- mod.: sec. "// CD: Service dialog style"
				- bisher: bool bUseNewServiceStyle = (app.Data.MainForm.User != UserType.Customer) && app.Data.Program.bServiceNew;
				- neu: bool bUseNewServiceStyle = app.Data.Program.bServiceNew;

		- mod.: 'OnReadSDcardStore()'
			- mod.: sec. "// CD: SDcardReader dialog style"
				- bisher: bool bUseNewSDcardReaderStyle = (app.Data.MainForm.User != UserType.Customer) && app.Data.Program.bSDcardReaderNew;
				- neu: bool bUseNewSDcardReaderStyle = app.Data.Program.bSDcardReaderNew;


JM,
7.7.16



================================
6.0.0 (angelegt 20.4.17)
================================

(
Korr. Device-SW-Version: Ordner '..\IAR_EW\IUT\PID\PID_V5_0_0'
)


- based on: 5.3.1

- Doc.cs:
	- Device program version angepa�t


JM,
20.4.17


- Item: "Dyn. Verst�rkerumschaltung: Einarbeitung Konzept AS"


	- ServiceForm.cs:

		- mod.: UI
			- raus: '_lblAmplScale', '_txtAmplScale'
			- daf�r neu: '_lblGainFactor', '_cbGainFactor'

		- mod.: 'Load()
			- raus: row "this._lblAmplScale.Text = ..."
			- daf�r neu: row "this._lblGainFactor.Text = ..."
			- raus: ress 'Service_lblAmplScale'
			- daf�r neu: ress 'Service_lblGainFactor'

		- mod.: '_ctl_HelpRequested()'
			- raus: sec. "// Ampl. scale"
			- daf�r neu: sec. "Gain factor"
			- raus: ress. 'Service_Help_txtAmplScale'
			- daf�r neu: ress. 'Service_Help_cbGainFactor'
	
		- mod.: '_Init()'
			- raus: sec. "// Handle the 'Amplifier scale' control depending on the edit mode"
			- daf�r neu: sec. "// Gain factor "

		- mod.: '_CheckControls()'
			- raus: sec. "// Amplifier scale"
			- daf�r neu: sec. "Gain factor"

		- mod.: '_UpdateServiceData(b)'
			- mod.: sec. TX
				- angepasst: Notes: AmplScale raus, GainFactor rein
				- raus: sec. "//    Amplifier scale: Format %.1f"
				- daf�r neu: sec. "//    Gain factor: Format %.1f"

			- mod.: sec. RX
				- angepasst: Notes: AmplScale raus, GainFactor rein
				- raus: sec. "// Amplifier scale"
				- daf�r neu: sec. "// Gain factor"

		- mod.: '_UpdateServiceData(s)'
			- raus: sec. "// Amplifier scale"
			- daf�r neu: sec. "// Gain factor"


	- ServieNewForm.cs:

		- mod.: UI
			- raus: '_lblAmplScale', '_txtAmplScale'
			- daf�r neu: '_lblGainFactor', '_cbGainFactor'

		- mod.: 'Load()
			- raus: row "this._lblAmplScale.Text = ..."
			- daf�r neu: row "this._lblGainFactor.Text = ..."

		- mod.: '_ctl_HelpRequested()'
			- raus: sec. "// Ampl. scale"
			- daf�r neu: sec. "Gain factor"
	
		- mod.: '_Init()'
			- raus: sec. "// Handle the 'Amplifier scale' control depending on the edit mode"
			- daf�r neu: sec. "// Gain factor "

		- mod.: '_CheckControls()'
			- raus: sec. "// Amplifier scale"
			- daf�r neu: sec. "Gain factor"

		- mod.: '_UpdateServiceData(b)'
			- mod.: sec. TX
				- angepasst: Notes: AmplScale raus, GainFactor rein
				- raus: sec. "//    Amplifier scale: Format %.1f"
				- daf�r neu: sec. "//    Gain factor: Format %.1f"

			- mod.: sec. RX
				- angepasst: Notes: AmplScale raus, GainFactor rein
				- raus: sec. "// Amplifier scale"
				- daf�r neu: sec. "// Gain factor"

		- mod.: '_UpdateServiceData(s)'
			- raus: sec. "// Amplifier scale"
			- daf�r neu: sec. "// Gain factor"


	- ScanParams.cs:

		- raus: member 'dAmplifierScale' + code bereinigt
		- daf�r neu: member 'dGainFactor' + in code eingearb.


	- Vw.cs:

		- mod.: '_ConvertAccordingToYUnit()'
			- mod.: sec. "case AppData.ProgramData.YUnits.mV:         // mV units"
				- bisheriger Inhalt raus, neu TODO

	
	- PropertiesForm.cs:

		- mod.: 'Init()'
			- neu: sec. "// Ordinate scaling 'mV': ..."


	- ServiceDataFile.cs:

		- mod.: 'ServiceDataAsFileString()'
			- raus: sec. "// Amplifier scale"
			- daf�r neu: sec. "// Gain factor"
		
		- mod.: 'ServiceDataAsStringArray()'
			- raus: sec. "// Amplifier scale"
			- daf�r neu: sec. "// Gain factor"




JM,
20.4.17



























