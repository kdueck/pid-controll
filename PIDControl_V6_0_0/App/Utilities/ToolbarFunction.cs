using System;
using System.Collections;
using System.Windows.Forms;

namespace Utilities
{
	/// <summary>
  /// Class ToolbarFunction:
  /// Manages the operation of a ToolBarButton like the corr'ing MenuItem was selected.
  /// </summary>
	public class ToolbarFunction
	{
    #region members

    /// <summary>
    /// Object (ToolBarButton) - MenuItem storage
    /// </summary>
    Hashtable m_Dictionary = new Hashtable( );

    /// <summary>
    /// The tool bar
    /// </summary>
    ToolBar  m_ToolBar = null;

    #endregion members

    #region methods

    ///<summary>
    /// Sets each ToolBarButton to a specific MenuItem.
    ///</summary>
    ///<param name="pObject">The object (ToolBarButton)</param>
    ///<param name="pMenuItem">The corr'ing MenuItem</param>
    public void SetToolbarFunction (object pObject, MenuItem pMenuItem)
    {
      // Exclude non-permitted objects
      if (!CanExtend (pObject))
        return;
      // Check: Does the ToolBarButton-MenuItem entry already exist?
      if (!m_Dictionary.Contains (pObject))
      {
        // No:
        // Add entry
        m_Dictionary.Add (pObject, pMenuItem);
        // Check: Was the tool bar already assigned?
        if (m_ToolBar == null)
        {
          // No:
          // Get the ToolBarButton
          ToolBarButton pToolBarButton = pObject as ToolBarButton;
          if (pToolBarButton != null)
          {
            // Assign the parent tool bar & Add its 'ButtonClick' event handler
            m_ToolBar = pToolBarButton.Parent as ToolBar;
            if (m_ToolBar != null)
              m_ToolBar.ButtonClick += new ToolBarButtonClickEventHandler(Handle_ToolbarButtonClick);
          }
        }
      }
      else 
      {
        // Yes:
        // Update entry
        m_Dictionary[pObject] = pMenuItem;
      }
    }

    ///<summary>
    /// Returns the MenuItem that is assigned to each ToolBarButton.
    ///</summary>
    ///<param name="pObject">The object (ToolBarButton)</param>
    ///<returns>The MenuItem (Null, if not assigned)</returns>
    public MenuItem GetToolbarFunction (object pObject)
    {
      // If available, get & return the MenuItem
      if (m_Dictionary.Contains(pObject))
        return (MenuItem) m_Dictionary[pObject];
      // If not, return null
      return null;
    }

    ///<summary>
    /// Defines the objects for which the opeartion management is permitted.
    ///</summary>
    public bool CanExtend (object pObject)
    {
      return (pObject is ToolBarButton);
    }

    #endregion methods

    #region event handling
    
    /// <summary>
    /// Handles the 'ButtonClick' event of a tool bar 
    /// </summary>
    void Handle_ToolbarButtonClick (object sender, ToolBarButtonClickEventArgs e)
    {
      // Check: Is the ToolbarButton connected to a MenuItem?
      if (m_Dictionary.Contains (e.Button))
      {
        // Yes:
        // Get its corr'ing MenuItem & Perform the items 'Click' event
        MenuItem pMenuItem = (MenuItem) m_Dictionary[e.Button];
        if (pMenuItem != null)
          pMenuItem.PerformClick();
      }
    }

    #endregion event handling
    
	}
}
