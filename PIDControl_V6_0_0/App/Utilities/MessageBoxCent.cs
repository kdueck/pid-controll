using System; 
using System.Windows.Forms; 
using System.Text; 
using System.Drawing; 
using System.Runtime.InteropServices; 

namespace Utilities 
{ 
  /// <summary>
  /// Class MessageBoxCent:
  /// MessageBox, centered above the owner window.
  /// </summary>
  /// <remarks>
  /// 1.
  /// Uses the 'CBTProc' hook procedure.
  /// Preferred variant: 
  ///   Even though both variants (the 'CallWndRetProc' and the 'CBTProc' variant) work fine,
  ///   the 'CBTProc' variant is a bit simplier in code compared with the 'CallWndRetProc' variant. 
  ///   Thats why the preferred variant is the 'CBTProc' variant.
  /// 2.
  /// TODO: Not yet included.  
  /// </remarks>
  public class MessageBoxCent 
  { 
    #region constructors

    /// <summary>
    /// Static constructor
    /// </summary>
    static MessageBoxCent() 
    { 
      // Init.
      _hookProc = new HookProc(MessageBoxHookProc); // The 'CBTProc' hook procedure 
      _hHook = IntPtr.Zero;                         // Its handle
    } 

    #endregion constructors

    #region delegates

    /// <summary>
    /// The 'CBTProc' callback (hook) function delegate 
    /// </summary>
    delegate IntPtr HookProc(int nCode, IntPtr wParam, IntPtr lParam); 

    #endregion delegates

    #region members

    /// <summary>
    /// The owner window
    /// </summary>
    static IWin32Window _owner; 

    /// <summary>
    /// The 'CBTProc' hook procedure 
    /// </summary>
    static HookProc _hookProc; 

    /// <summary>
    /// The handle of the 'CBTProc' hook procedure
    /// </summary>
    static IntPtr _hHook; 
    
    #endregion members

    #region public methods

    /// <summary>
    /// Displays a message box with specified text.
    /// </summary>
    /// <param name="text">The text to display in the message box.</param>
    /// <returns>One of the DialogResult values.</returns>
    public static DialogResult Show(string text) 
    { 
      _owner = null;
      Initialize(); 
      return MessageBox.Show(text); 
    } 

    /// <summary>
    /// Displays a message box with specified text and caption.
    /// </summary>
    /// <param name="text">The text to display in the message box.</param>
    /// <param name="caption">The text to display in the title bar of the message box.</param>
    /// <returns>One of the DialogResult values.</returns>
    public static DialogResult Show(string text, string caption) 
    { 
      _owner = null;
      Initialize(); 
      return MessageBox.Show(text, caption); 
    } 

    /// <summary>
    /// Displays a message box with specified text, caption, and buttons.
    /// </summary>
    /// <param name="text">The text to display in the message box.</param>
    /// <param name="caption">The text to display in the title bar of the message box.</param>
    /// <param name="buttons">One of the MessageBoxButtons values that specifies which buttons to display in the message box.</param>
    /// <returns>One of the DialogResult values.</returns>
    public static DialogResult Show(string text, string caption, MessageBoxButtons buttons) 
    { 
      _owner = null;
      Initialize(); 
      return MessageBox.Show(text, caption, buttons); 
    } 

    /// <summary>
    /// Displays a message box with specified text, caption, buttons, and icon.
    /// </summary>
    /// <param name="text">The text to display in the message box.</param>
    /// <param name="caption">The text to display in the title bar of the message box.</param>
    /// <param name="buttons">One of the MessageBoxButtons values that specifies which buttons to display in the message box.</param>
    /// <param name="icon">One of the MessageBoxIcon values that specifies which icon to display in the message box.</param>
    /// <returns>One of the DialogResult values.</returns>
    public static DialogResult Show(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon) 
    { 
      _owner = null;
      Initialize(); 
      return MessageBox.Show(text, caption, buttons, icon); 
    } 

    /// <summary>
    /// Displays a message box with the specified text, caption, buttons, icon, and default button.
    /// </summary>
    /// <param name="text">The text to display in the message box.</param>
    /// <param name="caption">The text to display in the title bar of the message box.</param>
    /// <param name="buttons">One of the MessageBoxButtons values that specifies which buttons to display in the message box.</param>
    /// <param name="icon">One of the MessageBoxIcon values that specifies which icon to display in the message box.</param>
    /// <param name="defButton">One of the MessageBoxDefaultButton values that specifies the default button for the message box.</param>
    /// <returns>One of the DialogResult values.</returns>
    public static DialogResult Show(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, 
      MessageBoxDefaultButton defButton) 
    { 
      _owner = null;
      Initialize(); 
      return MessageBox.Show(text, caption, buttons, icon, defButton); 
    } 

    /// <summary>
    /// Displays a message box with the specified text, caption, buttons, icon, default button, and options.
    /// </summary>
    /// <param name="text">The text to display in the message box.</param>
    /// <param name="caption">The text to display in the title bar of the message box.</param>
    /// <param name="buttons">One of the MessageBoxButtons values that specifies which buttons to display in the message box.</param>
    /// <param name="icon">One of the MessageBoxIcon values that specifies which icon to display in the message box.</param>
    /// <param name="defButton">One of the MessageBoxDefaultButton values that specifies the default button for the message box.</param>
    /// <param name="options">One of the MessageBoxOptions values that specifies which display and association options will be used for the message box.</param>
    /// <returns>One of the DialogResult values.</returns>
    public static DialogResult Show(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, 
      MessageBoxDefaultButton defButton, MessageBoxOptions options) 
    { 
      _owner = null;
      Initialize(); 
      return MessageBox.Show(text, caption, buttons, icon, defButton, options); 
    } 

    /// <summary>
    /// Displays a message box in front of the specified object and with the specified text.
    /// </summary>
    /// <param name="owner">The IWin32Window the message box will display in front of.</param>
    /// <param name="text">The text to display in the message box.</param>
    /// <returns>One of the DialogResult values.</returns>
    public static DialogResult Show(IWin32Window owner, string text) 
    { 
      _owner = owner; 
      Initialize(); 
      return MessageBox.Show(owner, text); 
    } 

    /// <summary>
    /// Displays a message box in front of the specified object and with the specified text and caption.
    /// </summary>
    /// <param name="owner">The IWin32Window the message box will display in front of.</param>
    /// <param name="text">The text to display in the message box.</param>
    /// <param name="caption">The text to display in the title bar of the message box.</param>
    /// <returns>One of the DialogResult values.</returns>
    public static DialogResult Show(IWin32Window owner, string text, string caption) 
    { 
      _owner = owner; 
      Initialize(); 
      return MessageBox.Show(owner, text, caption); 
    } 

    /// <summary>
    /// Displays a message box in front of the specified object and with the specified text, caption, and buttons.
    /// </summary>
    /// <param name="owner">The IWin32Window the message box will display in front of.</param>
    /// <param name="text">The text to display in the message box.</param>
    /// <param name="caption">The text to display in the title bar of the message box.</param>
    /// <param name="buttons">One of the MessageBoxButtons values that specifies which buttons to display in the message box.</param>
    /// <returns>One of the DialogResult values.</returns>
    public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons) 
    { 
      _owner = owner; 
      Initialize(); 
      return MessageBox.Show(owner, text, caption, buttons); 
    } 

    /// <summary>
    /// Displays a message box in front of the specified object and with the specified text, caption, buttons, 
    /// and icon.
    /// </summary>
    /// <param name="owner">The IWin32Window the message box will display in front of.</param>
    /// <param name="text">The text to display in the message box.</param>
    /// <param name="caption">The text to display in the title bar of the message box.</param>
    /// <param name="buttons">One of the MessageBoxButtons values that specifies which buttons to display in the message box.</param>
    /// <param name="icon">One of the MessageBoxIcon values that specifies which icon to display in the message box.</param>
    /// <returns>One of the DialogResult values.</returns>
    public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, 
      MessageBoxIcon icon) 
    { 
      _owner = owner; 
      Initialize(); 
      return MessageBox.Show(owner, text, caption, buttons, icon); 
    } 

    /// <summary>
    /// Displays a message box in front of the specified object and with the specified text, caption, buttons, icon, 
    /// and default button.
    /// </summary>
    /// <param name="owner">The IWin32Window the message box will display in front of.</param>
    /// <param name="text">The text to display in the message box.</param>
    /// <param name="caption">The text to display in the title bar of the message box.</param>
    /// <param name="buttons">One of the MessageBoxButtons values that specifies which buttons to display in the message box.</param>
    /// <param name="icon">One of the MessageBoxIcon values that specifies which icon to display in the message box.</param>
    /// <param name="defButton">One of the MessageBoxDefaultButton values that specifies the default button for the message box.</param>
    /// <returns>One of the DialogResult values.</returns>
    public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, 
      MessageBoxIcon icon, MessageBoxDefaultButton defButton) 
    { 
      _owner = owner; 
      Initialize(); 
      return MessageBox.Show(owner, text, caption, buttons, icon, defButton); 
    } 

    /// <summary>
    /// Displays a message box in front of the specified object and with the specified text, caption, buttons, icon, 
    /// default button, and options.
    /// </summary>
    /// <param name="owner">The IWin32Window the message box will display in front of.</param>
    /// <param name="text">The text to display in the message box.</param>
    /// <param name="caption">The text to display in the title bar of the message box.</param>
    /// <param name="buttons">One of the MessageBoxButtons values that specifies which buttons to display in the message box.</param>
    /// <param name="icon">One of the MessageBoxIcon values that specifies which icon to display in the message box.</param>
    /// <param name="defButton">One of the MessageBoxDefaultButton values that specifies the default button for the message box.</param>
    /// <param name="options">One of the MessageBoxOptions values that specifies which display and association options will be used for the message box.</param>
    /// <returns>One of the DialogResult values.</returns>
    public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, 
      MessageBoxIcon icon, MessageBoxDefaultButton defButton, MessageBoxOptions options) 
    { 
      _owner = owner; 
      Initialize(); 
      return MessageBox.Show(owner, text, caption, buttons, icon, defButton, options); 
    } 

    #endregion public methods

    #region private methods

    /// <summary>
    /// Installs a 'CBTProc' hook procedure.
    /// </summary>
    static void Initialize()
    {
      // Install the 'CBTProc' hook procedure, if the procedure is not yet installed and if an owner window is present
      if ((_hHook == IntPtr.Zero) && (_owner != null))
      {
        _hHook = SetWindowsHookEx(WH_CBT, _hookProc, IntPtr.Zero, System.Threading.Thread.CurrentThread.ManagedThreadId);
      }
    } 

    /// <summary>
    /// The 'CBTProc' hook procedure.
    /// </summary>
    /// <param name="nCode">The hook code passed to the hook procedure</param>
    /// <param name="wParam">The wParam value passed to the hook procedure</param>
    /// <param name="lParam">The lParam value passed to the hook procedure</param>
    /// <returns></returns>
    static IntPtr MessageBoxHookProc(int nCode, IntPtr wParam, IntPtr lParam) 
    { 
      // If 'nCode' is less than zero, the hook procedure must return the value returned by 'CallNextHookEx' 
      if (nCode < 0) 
      { 
        return CallNextHookEx(_hHook, nCode, wParam, lParam); 
      } 
      // Remember the Handle of the 'CBTProc' hook procedure
      IntPtr hook = _hHook; 
      // Check: Is the system about to activate a window?
      if (nCode == (int) CbtHookAction.HCBT_ACTIVATE)
      {
        // Yes:
        try 
        { 
          // Center that window (our child window) above the owner window
          CenterWindow(wParam); 
        } 
        finally 
        { 
          // Remove the hook procedure from the hook chain
          UnhookWindowsHookEx(_hHook); 
          _hHook = IntPtr.Zero; 
        } 
      }
      // Call & return the value returned by 'CallNextHookEx', so that other applications that have 
      // installed WH_CBT hooks could receive hook notifications
      return CallNextHookEx(hook, nCode, wParam, lParam); 
    } 

    /// <summary>
    /// Centers a child window above its owner window.
    /// </summary>
    /// <param name="hChildWnd">The child window handle</param>
    static void CenterWindow(IntPtr hChildWnd) 
    { 
      // Get the child windows bounding rect
      Rectangle recChild = new Rectangle(0, 0, 0, 0); 
      bool success = GetWindowRect(hChildWnd, ref recChild); 
      // The child windows width & heigt
      int width = recChild.Width - recChild.X; 
      int height = recChild.Height - recChild.Y; 
      // Get the owner windows bounding rect
      Rectangle recParent = new Rectangle(0, 0, 0, 0); 
      success = GetWindowRect(_owner.Handle, ref recParent); 
      // Calc. the owner windows center point 
      Point ptCenter = new Point(0, 0); 
      ptCenter.X = recParent.X + ((recParent.Width - recParent.X)/2); 
      ptCenter.Y = recParent.Y + ((recParent.Height - recParent.Y)/2); 
      // Calc. the TopLeft corner of the child windows (new) window position
      Point ptStart = new Point(0, 0); 
      ptStart.X = (ptCenter.X - (width/2)); 
      ptStart.Y = (ptCenter.Y - (height/2)); 
      ptStart.X = (ptStart.X < 0) ? 0 : ptStart.X; 
      ptStart.Y = (ptStart.Y < 0) ? 0 : ptStart.Y; 
      // Position the child window corr'ly
      int result = MoveWindow(hChildWnd, ptStart.X, ptStart.Y, width, height, false); 
    } 

    #endregion private methods

    #region WinAPI

    /// <summary>
    /// A type of hook procedure to be installed:
    /// Installs a hook procedure that monitors messages before activating, creating, destroying, 
    /// minimizing, maximizing, moving, or sizing a window and so on (a 'CBTProc' hook procedure). 
    /// </summary>
    const int WH_CBT = 5; 

    /// <summary>
    /// Specifies a code that a CBTProc (computer-based training) hook procedure uses to determine 
    /// how to process the message.
    /// </summary>
    enum CbtHookAction : int 
    { 
      HCBT_MOVESIZE     = 0, // A window is about to be moved or sized.
      HCBT_MINMAX       = 1, // A window is about to be minimized or maximized.
      HCBT_QS           = 2, // The system has retrieved a WM_QUEUESYNC message from the system message queue.
      HCBT_CREATEWND    = 3, // A window is about to be created.
      HCBT_DESTROYWND   = 4, // A window is about to be destroyed.
      HCBT_ACTIVATE     = 5, // The system is about to activate a window.
      HCBT_CLICKSKIPPED = 6, // The system has removed a mouse message from the system message queue.
      HCBT_KEYSKIPPED   = 7, // The system has removed a keyboard message from the system message queue.
      HCBT_SYSCOMMAND   = 8, // A system command is about to be carried out.
      HCBT_SETFOCUS     = 9  // A window is about to receive the keyboard focus. 
    } 

    /// <summary>
    /// The GetWindowRect function retrieves the dimensions of the bounding rectangle of the specified window.
    /// </summary>
    /// <param name="hWnd">Handle to the window.</param>
    /// <param name="lpRect">Pointer to a structure that receives the screen coordinates of the upper-left
    /// and lower-right corners of the window (out).</param>
    /// <returns>If the function succeeds, the return value is nonzero.
    /// If the function fails, the return value is zero.</returns>
    [DllImport("user32.dll")] 
    static extern bool GetWindowRect(IntPtr hWnd, ref Rectangle lpRect); 

    /// <summary>
    /// The MoveWindow function changes the position and dimensions of the specified window.
    /// </summary>
    /// <param name="hWnd">Handle to the window.</param>
    /// <param name="X">Specifies the new position of the left side of the window.</param>
    /// <param name="Y">Specifies the new position of the top of the window.</param>
    /// <param name="nWidth">Specifies the new width of the window.</param>
    /// <param name="nHeight">Specifies the new height of the window.</param>
    /// <param name="bRepaint">Specifies whether the window is to be repainted.</param>
    /// <returns>If the function succeeds, the return value is nonzero.
    /// If the function fails, the return value is zero.</returns>
    [DllImport("user32.dll")] 
    static extern int MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint); 

    /// <summary>
    /// The SetWindowsHookEx function installs an application-defined hook procedure into a hook chain. 
    /// </summary>
    /// <param name="idHook">Specifies the type of hook procedure to be installed.</param>
    /// <param name="lpfn">Pointer to the hook procedure.</param>
    /// <param name="hInstance">Handle to the DLL containing the hook procedure pointed to by the lpfn parameter. 
    /// The hMod parameter must be set to NULL if the dwThreadId parameter specifies a thread created by 
    /// the current process and if the hook procedure is within the code associated with the current process.</param>
    /// <param name="threadId">Specifies the identifier of the thread with which the hook procedure is to be associated.</param>
    /// <returns>If the function succeeds, the return value is the handle to the hook procedure. 
    /// If the function fails, the return value is NULL.</returns>
    [DllImport("user32.dll")] 
    static extern IntPtr SetWindowsHookEx(int idHook, HookProc lpfn, IntPtr hInstance, int threadId); 

    /// <summary>
    /// The UnhookWindowsHookEx function removes a hook procedure installed in a hook chain.
    /// </summary>
    /// <param name="idHook">Handle to the hook to be removed.</param>
    /// <returns>If the function succeeds, the return value is nonzero.
    /// If the function fails, the return value is zero.</returns>
    [DllImport("user32.dll")] 
    static extern int UnhookWindowsHookEx(IntPtr idHook); 

    /// <summary>
    /// The CallNextHookEx function passes the hook information to the next hook procedure in the current 
    /// hook chain.
    /// </summary>
    /// <param name="idHook">Handle to the current hook.</param>
    /// <param name="nCode">Specifies the hook code passed to the current hook procedure.</param>
    /// <param name="wParam">Specifies the wParam value passed to the current hook procedure.</param>
    /// <param name="lParam">Specifies the lParam value passed to the current hook procedure. </param>
    /// <returns>This value is returned by the next hook procedure in the chain. 
    /// The current hook procedure must also return this value.</returns>
    [DllImport("user32.dll")] 
    static extern IntPtr CallNextHookEx(IntPtr idHook, int nCode, IntPtr wParam, IntPtr lParam); 

    #endregion WinAPI
    
  } 
}
