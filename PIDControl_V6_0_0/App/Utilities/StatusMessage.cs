using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;

namespace Utilities
{
	/// <summary>
  /// Class StatusMessage:
  /// Manages the display of status messages from menu items and/or tool bar buttons 
  /// in a status bar panel.
  /// </summary>
	public class StatusMessage
	{
    #region members

    /// <summary>
    /// Object (MenuItem or ToolBarButton) - message storage
    /// </summary>
    Hashtable m_Dictionary = new Hashtable();

    /// <summary>
    /// The status bar panel, the message should appear within
    /// </summary>
    StatusBarPanel m_StatusBar = null;
    
    /// <summary>
    /// The tool bar
    /// </summary>
    ToolBar m_ToolBar = null;

    #endregion members

    #region methods
    
    ///<summary>
    /// Assigns the message that will appear in the status bar panel, when a MenuItem is selected
    /// or a ToolBarButton is crossed by the mouse.
    ///</summary>
    ///<param name="pObject">The object (MenuItem or ToolBarButton)</param>
    ///<param name="strMessage">The message</param>
    public void SetStatusMessage (object pObject, string strMessage)
    {
      // Exclude non-permitted objects
      if (!CanExtend (pObject))
        return;
      // Check: Does the object-message entry already exist?
      if (! m_Dictionary.Contains (pObject))
      {
        // No:
        // Add entry
        m_Dictionary.Add (pObject, strMessage);
        // Check, whether the object is a MenuItem or a ToolBarButton
        if (pObject is MenuItem)
        {
          // MenuItem:
          // Add a 'Select' event handler to the corr'ing object (MenuItem)
          MenuItem pMenuItem = pObject as MenuItem;
          if (pMenuItem != null)
            pMenuItem.Select += new EventHandler(Handle_MenuSelect);
        }
        else if (pObject is ToolBarButton)
        {
          // ToolBarButton:
          // Add a 'MouseMove' event handler to the parent tool bar (only once)
          ToolBarButton pToolBarButton = pObject as ToolBarButton;
          if (pToolBarButton != null)
          {
            if (m_ToolBar == null)
            {
              m_ToolBar = pToolBarButton.Parent;
              if (null != m_ToolBar)
              {
                m_ToolBar.MouseMove += new MouseEventHandler(Handle_ToolBarMouseMove);
              }
            }
          }
        }
      }
      else 
      {
        // Yes:
        // Update entry
        m_Dictionary[pObject] = strMessage;
      }
    }

    ///<summary>
    /// Retrieves the message that will appear in the status bar panel when a MenuItem 
    /// or a ToolBarButton is selected.
    ///</summary>
    ///<param name="pObject">The object (MenuItem or ToolBarButton)</param>
    ///<returns>The message (Null, if not available)</returns>
    public string GetStatusMessage (object pObject)
    {
      // If available, get & return the message
      if (m_Dictionary.Contains (pObject))
        return (string) m_Dictionary[pObject];
      // If not, return null
      return null;
    }

    ///<summary>
    /// Defines the objects for which the status message display is permitted.
    ///</summary>
    public bool CanExtend (object pObject)
    {
      return (pObject is MenuItem) || (pObject is ToolBarButton);
    }

    #endregion methods

    #region properties

    ///<summary>
    /// The status bar panel, the message should appear within
    ///</summary>
    public StatusBarPanel StatusBar
    {
      get { return m_StatusBar;  }
      set { m_StatusBar = value; }
    }

    #endregion properties

    #region event handling

    /// <summary>
    /// Handles the 'Select' event of a MenuItem 
    /// </summary>
    void Handle_MenuSelect (object sender, System.EventArgs e)
    {
      // Do nothing, if no status bar panel is assigned
      if (StatusBar == null)
        return;
      // Display the message, corr'ing to the given MenuItem, in the status bar panel
      if (m_Dictionary.Contains (sender))
        StatusBar.Text = (string) m_Dictionary[sender];
    }

    /// <summary>
    /// Handles the 'MouseMove' event of a ToolBar 
    /// </summary>
    void Handle_ToolBarMouseMove (object sender, System.Windows.Forms.MouseEventArgs e)
    {
      // Get the coordinates of the mouse      
      Point p = new Point(e.X, e.Y);
      // Empty the status bar panel
      StatusBar.Text = "";
      // Loop through the toolbar buttons:
      // Display the message, corr'ing to the given ToolBarButton, in the status bar panel
      foreach (ToolBarButton tb in m_ToolBar.Buttons)
      {
        if (tb.Rectangle.Contains (p))
        {
          if (m_Dictionary.Contains (tb))
            StatusBar.Text = (string) m_Dictionary[tb];
        }
      }
    }    
    
    #endregion event handling
  
  }
}
