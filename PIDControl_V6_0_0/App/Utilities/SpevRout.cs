using System;

namespace Utilities
{
   /// <summary>
	/// Class SpevRout:
  /// Spectrum evaluation routines
  ///
  /// Original spectrum evaluation routines
  /// Source: "", Kap. 4: "Spectrum evaluation", v. Espen, Janssens
	/// </summary>
	public class cSpevRout
	{
		
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public cSpevRout()
		{
		}

    #endregion constructors

    #region E. Fitting using analytical functions

    /// <summary>
    /// Fitting function: polynomial background with 'nb' terms and 'np' gaussians with position,
    ///					 width, and area as parameters
    /// Input: - array of independent variables 'x'[1, 'npts']
    ///		  - number of data points 'npts'
    ///		  - array of initial values of the parameters 'a'[1, nterms]
    ///		  - number of polynomial background terms 'nb'
    ///		  - number of Gaussians 'np'
    /// Output: - array of fitted datapoints 'yfit'[1, 'npts']
    /// BEMERKUNG: Gesamtzahl an Param's: nterms = 'nb'+3*'np'
    /// </summary>
    void fitfunc(float[] x, float[] yfit, int npts, float[] a, int nb, int np)
    {
      int i, j, k;
      float sqr2pi, area, pos, swid, z, g;

      // Param's
      sqr2pi=2.50663f;
      // loop over all channels
      for (i=1; i <= npts ;i++) 
      {
        yfit[i]=0;
        // background
        if (nb > 0)
        {
          yfit[i] += a[1];
          for (j=2 ; j <= nb ;j++)
            yfit[i] += (float)(a[j]*Math.Pow(x[i],j-1));
        }
        // peaks
        for (k=1; k <= np ;k++) 
        {
          area=a[nb+k];
          pos=a[nb+np+k];
          swid=a[nb+2*np+k];
          z=((pos-x[i])/swid)*((pos-x[i])/swid);
          if (z < 50) 
          {
            g=(float)(Math.Exp(-z/2)/swid/sqr2pi);
            yfit[i] += area*g;
          }
        }
      }
      // ready
    }

    /// <summary>
    /// Derivatives of fitting function: polynomial background with 'nb' terms and 'np' gaussians 
    ///									with position, width, and area as parameters
    /// Input: - array of independent variables 'x'[1, 'npts']
    ///		  - number of data points 'npts'
    ///		  - array of initial values of the parameters 'a'[1, nterms]
    ///		  - number of polynomial background terms 'nb'
    ///		  - number of Gaussians 'np'
    ///		  - point-index 'i'
    /// Output: - Derivatives of fitting function at point 'i': 'deriv'[1, nterms]
    /// BEMERKUNG: Gesamtzahl an Param's: nterms = 'nb'+3*'np'
    /// </summary>
    void derfunc(float[] x, int npts, float[] a, int nb, int np, float[] deriv, int i)
    {
      int  j, k;
      float sqr2pi, area, pos, swid, z, g;

      // Param's
      sqr2pi=2.50663f;
      // Derivatives of function with respect to the background parameters
      if (nb > 0)
      {
        deriv[1]=1.0f;
        for (j=2 ; j <= nb ;j++)
          deriv[j]=(float)Math.Pow(x[i],j-1);
      }
      // Derivatives of function with respect to the peak parameters
      for (k=1; k <= np ;k++) 
      {
        area=a[nb+k];
        pos=a[nb+np+k];
        swid=a[nb+2*np+k];
        z=((pos-x[i])/swid)*((pos-x[i])/swid);
        if (z < 50) 
        {
          g=(float)(Math.Exp(-z/2)/swid/sqr2pi);
          // peak area
          deriv[nb+k]=g;
          // peak position
          deriv[nb+np+k]=-area*g*(pos-x[i])/swid/swid;
          // peak width
          deriv[nb+2*np+k]=area*g*(z-1)/swid;
        }
        else 
        {
          deriv[nb+k]=0.0f;
          deriv[nb+np+k]=0.0f;
          deriv[nb+2*np+k]=0.0f;
        }
      }
      // ready
    }

    #endregion E. Fitting using analytical functions

    #region G. Least Squares procedures

    /// <summary>
    /// Marquardt algorithm for ninlinear least-squares fitting
    /// Input: - maximum namber of iterations 'maxiter'
    ///		  - array of independent variables 'x'[1, 'npts']
    ///		  - array of dependent variables 'y'[1, 'npts']
    ///		  - array of weights 'w'[1, 'npts']
    ///		  - number of data points 'npts'
    ///		  - number of polynomial background terms 'nb'
    ///		  - number of Gaussians 'np'
    ///		  - array of initial values of the parameters 'a'[1, nterms]
    ///		  - minimum percentage difference in two chi-square values to stop the iteration
    /// Output: - error status 'ierr': -1 indicates failure of fit, 0 - OK
    ///		   - reduced chi-square value 'chisqr'
    ///		   - Marquardt control parameter 'flamda'
    ///		   - array of fitted datapoints 'yfit'[1, 'npts']
    ///		   - least-squares estimate of the fitting parameters 'a'[1, nterms]
    ///		   - standard deviation of 'a': 'sa'[1, nterms]
    /// Workspace: 'b'[1, nterms], 'beta'[1, nterms], 'deriv'[1, nterms]
    ///			  'alfa'[1, nterms*(nterms+1)/2], arr[1, nterms*(nterms+1)/2]
    /// BEMERKUNG: Gesamtzahl an Param's: nterms = 'nb'+3*'np'
    /// </summary>
    public void marqfit(out int ierr, out float chisqr, out float flamda, float cridif, int maxiter,
                        float[] x, float[] y, float[] w, float[] yfit, int npts, 
                        float[] a, float[] sa, int nb, int np, float[] b, float[] beta, float[] deriv,
                        float[] alfa, double[] arr)
    {
      int niter, j, i, jj, k, jk, jjj, nterms;
      float flammax, flammin, chisav, d, perdif, sdev;

      // number of parameters 'nterms'
      nterms=nb+3*np;
      // Param's
      flammax=(float)1e4;
      flammin=(float)1e-6;
      // evaluate the fitting function 'yfit' for the current parameters and
      // save the chi-square value
      niter=0;
      fitfunc(x, yfit, npts, a, nb, np);
      chisqr=chifit(y, yfit, w, npts, nterms);
      flamda=0.0f;
      // set 'alfa' and 'beta' to zero, save the current value of the parameters 'a'
      do 
      {
        niter++;
        chisav=chisqr;
        for (j=1; j <= nterms ;j++) 
        {
          b[j]=a[j];
          beta[j]=0.0f;
        }
        for (j=1; j <= nterms*(nterms+1)/2 ;j++)
          alfa[j]=0.0f;
        // accumulate 'alfa'- and 'beta'-matrices
        for (i=1; i <= npts ;i++) 
        {
          d=y[i]-yfit[i];
          // calculate derivatives at point i
          derfunc(x, npts, a, nb, np, deriv, i);
          for (j=1; j <= nterms ;j++) 
          {
            beta[j] += w[i]*d*deriv[j];
            jj=j*(j-1)/2;
            for (k=1; k <= j ;k++) 
            {
              jk=jj+k;
              alfa[jk] += w[i]*deriv[j]*deriv[k];
            }
          }
        }
        // test and scale 'alfa'-matrix
        for (j=1; j <= nterms ;j++) 
        {
          jj=j*(j-1)/2;
          jjj=jj+j;
          if (alfa[jjj] < 1e-20) 
          {
            for (k=1; k <= j ;k++) 
            {
              jk=jj+k;
              alfa[jk]=0.0f;
            }
            alfa[jjj]=1.0f;
            beta[j]=0.0f;
          }
          sa[j]=(float)Math.Sqrt(alfa[jjj]);
        }
        for (j=1; j <= nterms ;j++) 
        {
          jj=j*(j-1)/2;
          for (k=1; k <= j ;k++) 
          {
            jk=jj+k;
            alfa[jk] /= (sa[j]*sa[k]);
          }
        }
        // store 'alfa' in array 'arr', modify the diagonal elements with 'flamda'
        while (true) 
        {
          for (j=1; j <= nterms ;j++) 
          {
            jj=j*(j-1)/2;
            for (k=1; k <= j ;k++) 
            {
              jk=jj+k;
              arr[jk]=alfa[jk];
            }
            jjj=jj+j;
            arr[jjj]=1+flamda;
          }
          // invert matrix 'arr'
          lminv(arr, nterms, out ierr);
          if (ierr != 0) 
          {
            return;
          }
          // calculate new values of parameters 'a'
          for (j=1; j <= nterms ;j++) 
          {
            for (k=1; k <= nterms ;k++) 
            {
              if (k > j)
                jk=j+k*(k-1)/2;
              else
                jk=k+j*(j-1)/2;
              a[j] += (float)((arr[jk]/sa[j])*(beta[k]/sa[k]));
            }
          }
          // evaluate the fitting function 'yfit' for the new parameters and chi-square
          fitfunc(x, yfit, npts, a, nb, np);
          chisqr=chifit(y, yfit, w, npts, nterms);
          if (niter == 1) flamda=0.001f;
          // test new parameter set
          if ( chisqr > chisav) 
          {
            // iteration NOT successful, increase 'flamda' and try again
            flamda=Math.Min(flamda*10, flammax);
            for (j=1; j <= nterms ;j++)
              a[j]=b[j];
          }
          else 
          {
            // iteration successful 
            break;
          }
        }
        // iteration successful, decrease 'flamda' 
        flamda=Math.Max(flamda/10, flammin);
        // get next better estimate if required
        perdif=100*(chisav-chisqr)/chisqr;
      } while (niter < maxiter && perdif > cridif);
      // calculate standard deviations and return
      for (j=1; j <= nterms ;j++) 
      {
        jj=j*(j+1)/2;
        sdev=(float)(Math.Sqrt(arr[jj])/sa[j]);
        sa[j]=sdev;
      }
      // ready
    }

    /// <summary>
    /// Evaluate chi-square
    /// Input: - array of datapoints 'y'[1, 'npts']
    ///		  - array of fitted datapoints 'yfit'[1, 'npts']
    ///		  - array of weights 'w'[1, 'npts']
    ///		  - number of data points 'npts'
    ///		  - number of parameters 'nterms'
    /// Output: - chi-square value (ret.)
    /// </summary>
    float chifit(float[] y, float[] yfit, float[] w, int npts, int nterms)
    {
      int i;
      float chi;

      chi=0.0f;
      for (i=1; i <= npts ;i++) 
        chi += w[i]*(y[i]-yfit[i])*(y[i]-yfit[i]);
      chi /= (npts-nterms);
      // ready
      return chi;
    }
	
    /// <summary>
    /// Matrix inversion
    /// (General purpose routine to invert a symmetrical matrix)
    /// Input: - 'arr': upper triangle and diagonal of real symmetrical matrix
    ///				          stored in linear array, size 'n'*('n'+1)/2
    ///		     - 'n': order of the matrix (number of columns)
    /// Output: - 'ierr': error status:  0: inverse obtained, -1: singular matrix
    ///		      - 'arr': upper triangle and diagonal of inverted matrix
    /// </summary>
    void lminv(double[] arr, int n, out int ierr)
    {
      int kpiv, k, ind, lend, i, l, ipiv, min, kend, lanf, j, lhor, lver;
      double dsum, dpiv=0, din, work;

      //
      kpiv=0;
      for (k=1; k <= n ;k++) 
      {
        kpiv += k;
        ind=kpiv;
        lend=k-1;
        for (i=k; i <= n ;i++) 
        {
          dsum=0;
          if (lend > 0) 
          {
            for (l=1; l <= lend ;l++)
              dsum += arr[kpiv-l]*arr[ind-l];
          }
          dsum=arr[ind]-dsum;
          if (i == k) 
          {
            if (dsum <= 0) 
            {
              ierr= -1;
              return;
            }
            dpiv=Math.Sqrt(dsum);
            arr[kpiv]=dpiv;
            dpiv=1/dpiv;
          }
          else 
          {
            arr[ind]=dsum*dpiv;
          }
          ind += i;
        }
      }
      //
      ierr=0;
      ipiv=(n*(n+1))/2;
      ind=ipiv;
      for (i=1; i <= n ;i++) 
      {
        din=1/arr[ipiv];
        arr[ipiv]=din;
        min=n;
        kend=i-1;
        lanf=n-kend;
        if (kend > 0) 
        {
          j=ind;
          for (k=1; k <= kend ;k++) 
          {
            work=0;
            min--;
            lhor=ipiv;
            lver=j;
            for (l=lanf; l <= min ;l++) 
            {
              lver++;
              lhor += l;
              work += arr[lver]*arr[lhor];
            }
            arr[j]= -work*din;
            j -= min;
          }
        }
        ipiv -= min;
        ind--;
      }
      //
      for (i=1; i <= n ;i++) 
      {
        ipiv += i;
        j=ipiv;
        for (k=i; k <= n ;k++) 
        {
          work=0;
          lhor=j;
          for (l=k; l <= n ;l++) 
          {
            lver=lhor+k-i;
            work += arr[lver]*arr[lhor];
            lhor += l;
          }
          arr[j]=work;
          j += k;
        }
      }
      // ready
      ierr=0;
    }

    #endregion G. Least Squares procedures

  }
}
