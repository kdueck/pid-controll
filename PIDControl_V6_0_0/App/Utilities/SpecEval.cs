using System;

namespace Utilities
{
  /// <summary>
  /// Class SpecEvalParameter:
  /// Peak detection parameter 
  /// </summary>
  public class SpecEvalParameter
  {
    /// <summary>
    /// Signals noisy part (def.: 128 for IMS, 0 for PID)
    /// </summary>
    public int nNoisy;            
    /// <summary>
    /// Noise level (def.: 6.0 * STD of noisy part)
    /// </summary>
    public float fNoiseLevel;      
  }
  
  /// <summary>
	/// Class SpecEval:
	/// Spectrum evaluation
	/// </summary>
	public class SpecEval
	{
		
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public SpecEval()
		{
		}

    #endregion constructors
  
    #region constants & enums
  
    /// <summary>
    /// Max. permissible peak resp. Minimum number
    /// </summary>
    const int MAX_PEAKS = 32;

    /// <summary>
    /// Gain factor
    /// </summary>
    const float _gain = 10.4f;       

    #endregion constants & enums
 
    #region members
  
    /// <summary>
    /// Peak area detection:
    /// SG (Savitzky/Golay) coeff's: 2. derivative, m=8 (filter half width)
    /// </summary>
    double[] g_pDer2_8 = new double[]
    {
      -.021156,
      .014319,
      .026703,
      .023865,
      .012463,
      -.002054,
      -.015450,
      -.024698,
      -.027983,
      -.024698,
      -.015450,
      -.002054,
      .012463,
      .023865,
      .026703,
      .014319,
      -.021156
    };

    /// <summary>
    /// Smoothing:
    /// SG (Savitzky/Golay) coeff's: smoothing, filter width = 41 (half width: 41/2 = 20)
    /// </summary>
    double[] g_sgsm_41 = new double []
      {
        -0.0323312535450936,
        -0.0238230289279637,
        -0.0157511235219687,
        -0.00811553732710851,
        -0.000916270343383219,
        0.00584667742920721,
        0.0121733059906628,
        0.0180636153409835,
        0.0235176054801693,
        0.0285352764082203,
        0.0331166281251363,
        0.0372616606309176,
        0.0409703739255639,
        0.0442427680090754,
        0.0470788428814521,
        0.0494785985426938,
        0.0514420349928007,
        0.0529691522317728,
        0.0540599502596099,
        0.0547144290763122,
        0.0549325886818797,
        0.0547144290763122,
        0.0540599502596099,
        0.0529691522317728,
        0.0514420349928007,
        0.0494785985426938,
        0.0470788428814521,
        0.0442427680090754,
        0.0409703739255639,
        0.0372616606309176,
        0.0331166281251363,
        0.0285352764082203,
        0.0235176054801693,
        0.0180636153409835,
        0.0121733059906628,
        0.00584667742920721,
        -0.000916270343383219,
        -0.00811553732710851,
        -0.0157511235219687,
        -0.0238230289279637,
        -0.0323312535450936
      };
    
    /// <summary>
    /// Smoothing:
    /// SG (Savitzky/Golay) coeff's: smoothing, variable filter width in [5, 41]
    /// </summary>
    double[] g_sgsm = new double [41];

    /// <summary>
    /// Number of peaks found
    /// </summary>
    int g_nPeaks; 
  
    /// <summary>
    /// Peak position (index) array
    /// </summary>
    int[] g_ariPeakPos = new int[MAX_PEAKS];

    /// <summary>
    /// Peak invalid array
    /// </summary>
    byte[] g_arbyPeakInvalid = new byte[MAX_PEAKS];

    /// <summary>
    /// Peak area array
    /// </summary>
    float[] g_arfPeakArea = new float[MAX_PEAKS];
    
    /// <summary>
    /// Number of Min's found
    /// </summary>
    int g_nMins;
    
    /// <summary>
    /// Min position (index) array
    /// </summary>
    int[] g_ariMinPos = new int[MAX_PEAKS];

    #endregion members

    #region public methods

    /// <summary>
    /// Performs the peak detection.
    /// </summary>
    /// <param name="array">The array, the peaks should be detected within</param>
    /// <param name="sep">The Peak detection parameter</param>
    /// <param name="nPoints">The # of points (the array part), the peaks should be detected within</param>
    public void DetectPeaks (int[] array, SpecEvalParameter sep, int nPoints)
    {
      // Peak search
      FindPeaks_I(array, nPoints, sep, ref g_ariPeakPos, ref g_nPeaks);
      // Peak area detection
      PeakAreaDetect_Sigma_Calc (array, nPoints); 
    }

    /// <summary>
    /// Performs the peak detection - Extended version.
    /// </summary>
    /// <param name="array">The array, the peaks should be detected within</param>
    /// <param name="sep">The Peak detection parameter</param>
    /// <param name="nPoints">The # of points (the array part), the peaks should be detected within</param>
    public void DetectPeaksEx (int[] array, SpecEvalParameter sep, int nPoints)
    {
      // n sigma factor for peak search: 6
      // peak search mode: 0 - Unidirectional
      // n sigma factor for peak area det.: 3
      // peak area det. mode: 0 - Simple
      DetectPeaksEx (array, sep, nPoints, 6, 0, 3, 0);
    }

    /// <summary>
    /// Performs the peak detection - Extended version.
    /// </summary>
    /// <param name="array">The array, the peaks should be detected within</param>
    /// <param name="sep">The Peak detection parameter</param>
    /// <param name="nPoints">The # of points (the array part), the peaks should be detected within</param>
    /// <param name="nSigPS">n sigma factor for peak search</param>
    /// <param name="nModePD">Peak search mode</param>
    /// <param name="nSigPA">n sigma factor for peak area det.</param>
    /// <param name="nModePA">Peak area det. mode</param>
    public void DetectPeaksEx (int[] array, SpecEvalParameter sep, int nPoints, 
                               int nSigPS, int nModePD, int nSigPA, int nModePA)
    {
      // Up to version 3.7.0:         Peak search: Unidirectional, Peak area detection: Noise level
      // Begining from version 3.7.1: Peak search: Bidirectional, Peak area detection: Combined
      // Begining from version 3.10.0:Peak search: Uni-/Bidirectional, Peak area detection: Simple/Ext1/Ext2
      float fSTD = sep.fNoiseLevel;
      
      // Peak search
      sep.fNoiseLevel = nSigPS * fSTD;   // n sigma factor for peak search
      switch (nModePD)
      {
        case 0: 
          FindPeaksEx_I_Unidirectional(array, nPoints, sep, ref g_ariPeakPos, ref g_nPeaks, ref g_ariMinPos, ref g_nMins);
          break;
        case 1: 
          FindPeaksEx_I_Bidirectional(array, nPoints, sep, ref g_ariPeakPos, ref g_nPeaks, ref g_ariMinPos, ref g_nMins);
          break;
      }

      // Reject negative peaks
      RejectNegativePeaks (array, nPoints); 
      
      // Peak validity check (Overflow)
      float fOFlimitation = 32768 * _gain * 0.9f; // OF limitation ("Kappungsgrenze"): 90% from the max. adc value (2^15) multiplied by the Gain factor
      for (int i=0; i < g_nPeaks; i++)
      {
        if (array[g_ariPeakPos[i]] > fOFlimitation)   g_arbyPeakInvalid[i] = 1;
        else                                          g_arbyPeakInvalid[i] = 0;
      }
      
      // Peak area detection
      sep.fNoiseLevel = nSigPA * fSTD;   // n sigma factor for peak area det.
      PeakAreaDetectEx_Combined (array, nPoints, sep, nModePA);
    }

    /// <summary>
    /// Performs an array smoothing (the whole array is smoothed).
    /// </summary>
    /// <param name="array">The array (smoothed on output</param>
    /// <param name="nFilterWidth">The width of the smoothing filter</param>
    /// <param name="nMethod">The smoothing method</param>
    public void SmoothArray (ref int[] array, int nFilterWidth, int nMethod)
    {
      // Init.
      int[] y = new int[array.Length];  // source array
      array.CopyTo (y, 0);  
      int[] s = new int[array.Length];  // destination (smoothed) array
      int nchan = array.Length;         // Array length
      int ich1 = 0;                     // Begin channel of the smoothing  region
      int ich2 = array.Length-1;        // End channel of the smoothing  region
      int iwid = nFilterWidth;          // Width of the smoothing filter
      // Smooth
      // CD: Smoothing method
      switch (nMethod)
      {
          // SG
        case 1: sgsmth(y, ref s, nchan, ich1, ich2, iwid); break;       // case 1: Common case, no wrapping
        case 2: sgsmth(y, ref s, nchan, ich1, ich2); break;             // case 2: Fixed width: 41, no wrapping
        case 3: sgsmth(y, ref s, nchan, array.Length); break;           // case 3: Fixed width: 41, wrapping
        case 4:                                                         // case 4: Common case, Wrapping
          this.sg_smooth_CalcSGCoeff (iwid);
          sg_smooth(y, ref s, nchan, array.Length, iwid); 
          break;  
          // MA
        case 5: ma_smooth(y, ref s, nchan, array.Length, iwid); break;
      }
      // Overgive
      s.CopyTo (array, 0);
    }
    
    /// <summary>
    /// Performs an array smoothing.
    /// </summary>
    /// <param name="array">The array</param>
    /// <param name="array_sm">The smoothed array</param>
    /// <param name="nPoints">The # of points to be smoothed</param>
    /// <param name="nFilterWidth">The width of the smoothing filter</param>
    /// <param name="nMethod">The smoothing method</param>
    public void SmoothArray (int[] array, ref int[] array_sm, int nPoints, int nFilterWidth, int nMethod)
    {
      // Init.
      int[] y = new int[array.Length];  // source array
      array.CopyTo (y, 0);  
      int[] s = new int[array.Length];  // destination (smoothed) array
      int nchan = array.Length;         // Array length
      int ich1 = 0;                     // Begin channel of the smoothing  region
      int ich2 = nPoints-1;             // End channel of the smoothing  region
      int iwid = nFilterWidth;          // Width of the smoothing filter
      // Smooth
      // CD: Smoothing method
      switch (nMethod)
      {
          // SG
        case 1: sgsmth(y, ref s, nchan, ich1, ich2, iwid); break;       // case 1: Common case, no wrapping
        case 2: sgsmth(y, ref s, nchan, ich1, ich2); break;             // case 2: Fixed width: 41, no wrapping
        case 3: sgsmth(y, ref s, nchan, nPoints); break;                // case 3: Fixed width: 41, wrapping
        case 4:                                                         // case 4: Common case, Wrapping
          this.sg_smooth_CalcSGCoeff (iwid);
          sg_smooth(y, ref s, nchan, nPoints, iwid); 
          break;  
          // MA
        case 5: ma_smooth(y, ref s, nchan, nPoints, iwid); break;
      }
      // Overgive
      s.CopyTo (array_sm, 0);
    }

    #endregion public methods

    #region peak search methods

    /// <summary>
    /// Gets the average of an entire array or part of it.
    /// </summary>
    /// <param name="array">The array</param>
    /// <param name="nData">The array part, over which the average should be calculated</param>
    /// <returns></returns>
    public float GetAve (int[] array, int nData)
    {
      float ave;
      int i;
  
      if (nData > array.Length) nData = array.Length;
      if (nData < 0)            nData = 0;
      for (ave=0, i=0; i < nData; i++)  ave += array[i]; 
      if (nData > 0) ave /= nData;
      return ave;
    }

    /// <summary>
    /// Gets the standard deviation of an entire array or part of it.
    /// </summary>
    /// <param name="array">The array</param>
    /// <param name="nData">The array part, over which the standard deviation should be calculated</param>
    /// <returns></returns>
    public float GetStdDev (int[] array, int nData)
    {
      float stddev;
      int i;
  
      if (nData > array.Length) nData = array.Length;
      if (nData < 0)            nData = 0;
      float ave = GetAve (array, nData);
      for (stddev=0, i=0; i < nData; i++) stddev += (ave-array[i])*(ave-array[i]); 
      if (nData > 1)
      {
        stddev /= (nData-1);
        stddev = (float) Math.Sqrt(stddev);
      }
      return stddev;
    }

    /// <summary>
    /// Performs a Baseline correction.
    /// </summary>
    /// <param name="array">The array (signal)</param>
    /// <param name="nNoisy">The array's noisy part (No of noisy leading bins)</param>
    public void CorrectBaseLine(ref int[] array, int nNoisy)
    { 
      // Get the baseline
      float ave = GetAve (array, nNoisy);
      // Subtract the baseline
      for (int i=0; i < array.Length ;i++) array[i] -= (int) ave;
    }

    /// <summary>
    /// Detects the peaks in the non-noisy part of a signal.
    /// </summary>
    /// <param name="array">The array (signal)</param>
    /// <param name="nData">The array part, the peaks should be detected within</param>
    /// <param name="parameter">The parameter set</param>
    /// <param name="peakPos">The peak position (index) array containing the bin numbers of the peaks found (output)</param>
    /// <param name="nPeaks">The number of peaks found (output)</param>
    void FindPeaks_I(int[] array, int nData, SpecEvalParameter parameter, ref int[] peakPos, ref int nPeaks) 
    {
      UInt16 i, j, maxi, mini, counter;
      int maxy, miny;

      // Init's
      if (nData > array.Length) nData = array.Length;
      if (nData < 0)            nData = 0;
      
      mini=0;                       //initial minimum idx
      maxi=mini;                    //initial maximum idx
  
      maxy = array[0];		//initial maximum value
      miny = array[0];		// ...    minimum ...
      for (i=1; i < nData ;i++) 
      {
        if (array[i] > miny) miny = array[i];
        if (array[i] < maxy) maxy = array[i];
      }
      miny += 1;                    //the (beginning) min. value is greater than any array element    
      maxy -= 1;                    //the (beginning) max. value is smaller than any array element 
  
      // Search algorithm: locale MinMax
      // (Important: 
      //  It doesn't matter if the 'j'-beginning is even (0) or odd (1).
      //  Furthermore it has to be taken into consideration, that peaks detected in the
      //  signal's noisy part must not be counted.)
      for (counter=0,j=1,i = 0; i < nData ;i++) 
      {
        if ((j % 2) > 0)		
        {	
          // Maxima
          if (array[i] > maxy) 
          {
            maxy = array[i];
            maxi = i;
          }
          if (array[i] < (maxy - parameter.fNoiseLevel)) 
          {
            // Consider only peaks in the region permitted (i.e. the signals non-noisy part)
            if (maxi >= parameter.nNoisy) 
            {
              // Peak found:
              // Check: Does the number of peaks found exceed the max. number of peaks permitted?
              if (counter < MAX_PEAKS)
              {
                // No: register the peak
                peakPos[counter++]=maxi; 
                // Quit the search, if we already got the max. number of peaks permitted
                if (counter == MAX_PEAKS)
                  break;
              }
            }
            j++;
            miny=array[i];
            mini=i;
          }
        } 
        else					
        {
          // Minima
          if (array[i] < miny) 
          {
            miny=array[i];
            mini=i;
          }
          if (array[i] > (miny + parameter.fNoiseLevel)) 
          {
            j++;
            maxy=array[i];
            maxi=i;
          }
        }
      }
  
      // Overgive the number of peaks found
      nPeaks=counter;
    }


    /// <summary>
    /// Detects the peaks in the non-noisy part of a signal - Extended version - Unidirectional search.
    /// </summary>
    /// <param name="array">The array (signal)</param>
    /// <param name="nData">The array part, the peaks should be detected within</param>
    /// <param name="parameter">The parameter set</param>
    /// <param name="peakPos">The peak position (index) array containing the bin numbers of the peaks found (output)</param>
    /// <param name="nPeaks">The number of peaks found (output)</param>
    /// <param name="minPos">The Min. position (index) array containing the bin numbers of the Min's found (output)</param>
    /// <param name="nMins">The number of Min's found (output)</param>
    void FindPeaksEx_I_Unidirectional(int[] array, int nData, SpecEvalParameter parameter, 
      ref int[] peakPos, ref int nPeaks, ref int[] minPos, ref int nMins) 
    {
      UInt16 i, j, maxi, mini, counter, counter_min;
      int maxy, miny;

      // Init's
      if (nData > array.Length) nData = array.Length;
      if (nData < 0)            nData = 0;
      
      mini=0;                       //initial minimum idx
      maxi=mini;                    //initial maximum idx
  
      maxy = array[0];		//initial maximum value
      miny = array[0];		// ...    minimum ...
      for (i=1; i < nData ;i++) 
      {
        if (array[i] > miny) miny = array[i];
        if (array[i] < maxy) maxy = array[i];
      }
      miny += 1;                    //the (beginning) min. value is greater than any array element    
      maxy -= 1;                    //the (beginning) max. value is smaller than any array element 
  
      // Search algorithm: locale MinMax
      // (Important: 
      //  It doesn't matter if the 'j'-beginning is even (0) or odd (1).
      //  Furthermore it has to be taken into consideration, that peaks detected in the
      //  signal's noisy part must not be counted.)
      for (counter=0,counter_min=0,j=1,i = 0; i < nData ;i++) 
      {
        if ((j % 2) > 0)		
        {	
          // Maxima
          if (array[i] > maxy) 
          {
            maxy = array[i];
            maxi = i;
          }
          if (array[i] < (maxy - parameter.fNoiseLevel)) 
          {
            // Consider only peaks in the region permitted (i.e. the signals non-noisy part)
            if (maxi >= parameter.nNoisy) 
            {
              // Peak found:
              // Check: Does the number of peaks found exceed the max. number of peaks permitted?
              if (counter < MAX_PEAKS)
              {
                // No: register the peak
                peakPos[counter++]=maxi; 
                // Quit the search, if we already got the max. number of peaks permitted
                if (counter == MAX_PEAKS)
                  break;
              }
            }
            j++;
            miny=array[i];
            mini=i;
          }
        } 
        else					
        {
          // Minima
          if (array[i] < miny) 
          {
            miny=array[i];
            mini=i;
          }
          if (array[i] > (miny + parameter.fNoiseLevel)) 
          {
            // Consider only Min's in the region permitted (i.e. the signals non-noisy part)
            if (mini >= parameter.nNoisy) 
            {
              // Min. found:
              // Check: Does the number of Min's found exceed the max. number of Min's permitted?
              if (counter_min < MAX_PEAKS)
              {
                // No: register the Min.
                minPos[counter_min++]=mini; 
                // Quit the search, if we already got the max. number of Min's permitted
                if (counter_min == MAX_PEAKS)
                  break;
              }
            }
            j++;
            maxy=array[i];
            maxi=i;
          }
        }
      }
  
      // Overgive the number of peaks / Min's found
      nPeaks=counter;
      nMins=counter_min;
    }

    /// <summary>
    /// Detects the peaks in the non-noisy part of a signal - Extended version - Bidirectional search.
    /// </summary>
    /// <param name="array">The array (signal)</param>
    /// <param name="nData">The array part, the peaks should be detected within</param>
    /// <param name="parameter">The parameter set</param>
    /// <param name="peakPos">The peak position (index) array containing the bin numbers of the peaks found (output)</param>
    /// <param name="nPeaks">The number of peaks found (output)</param>
    /// <param name="minPos">The Min. position (index) array containing the bin numbers of the Min's found (output)</param>
    /// <param name="nMins">The number of Min's found (output)</param>
    void FindPeaksEx_I_Bidirectional(int[] array, int nData, SpecEvalParameter parameter, 
      ref int[] peakPos, ref int nPeaks, ref int[] minPos, ref int nMins) 
    {
      UInt16 i, ii, j, k, maxi, mini, counter_maxH, counter_minH, counter_maxR, counter_minR, counter;
      int maxy, miny, minyDup, maxyDup, iDeltaMin, iDelta, ug, og;

      // ---------------------------
      // Init's

      if (nData > array.Length) nData = array.Length;
      if (nData < 0)            nData = 0;
      
      mini=0;                       // initial minimum idx
      maxi=0;                       // initial maximum idx
  
      maxy = array[0];		          // initial maximum value
      miny = array[0];		          //  ...    minimum ...
      for (i=1; i < nData ;i++) 
      {
        if (array[i] > miny) miny = array[i];
        if (array[i] < maxy) maxy = array[i];
      }
      miny += 1;                    // the (beginning) min. value is greater than any array element    
      maxy -= 1;                    // the (beginning) max. value is smaller than any array element 
  
      minyDup = miny;               // the duplicated min. value
      maxyDup = maxy;               // the duplicated max. value

      counter_maxH = 0;             // counter: Maxima (Hin)
      counter_minH = 0;             // counter: Minima (Hin)
      counter_maxR = 0;             // counter: Maxima (R�ck)
      counter_minR = 0;             // counter: Minima (R�ck)
      
      iDelta = 0;                   // Delta-x between Hin- and R�ck-Peaks

      int[] arPeakPosH = new int[MAX_PEAKS];    // Local: Peak position array (Hin)
      int[] arMinPosH = new int[MAX_PEAKS];     // Local: Min position array (Hin)
      int[] arPeakPosR = new int[MAX_PEAKS];    // Local: Peak position array (R�ck)
      int[] arMinPosR = new int[MAX_PEAKS];     // Local: Min position array (R�ck)
      
      // ---------------------------
      // 1. step

      // Search algorithm: Locale MinMax
      // Notes:
      //  It has to be taken into consideration, that peaks detected in the
      //  signal's noisy part must not be considered.
      
      // Hin(1)- / R�ck(2)-Schleife
      for (k=1; k <= 2; k++)
      {
        // CD: Hin/R�ck
        if (k==1) { counter_maxH=0; counter_minH=0; miny=minyDup; maxy=maxyDup; } 
        else      { counter_maxR=0; counter_minR=0; miny=minyDup; maxy=maxyDup; }
        // Loop over the data array
        for (j=1, ii = 0; ii < nData ;ii++) 
        {
          // CD: Hin/R�ck
          if (k==1) i=ii;
          else      i=(UInt16)(nData-1-ii);
          // MaxMin detection
          if ((j % 2) > 0)		
          {	
            // Maxima
            if (array[i] > maxy) 
            {
              maxy = array[i];
              maxi = i;
            }
            if (array[i] < (maxy - parameter.fNoiseLevel)) 
            {
              // Consider only peaks in the region permitted (i.e. the signals non-noisy part)
              if (maxi >= parameter.nNoisy) 
              {
                // Peak found:
                // CD: Hin/R�ck
                if (k==1) counter = counter_maxH;
                else      counter = counter_maxR;
                // Check: Does the number of peaks found exceed the max. number of peaks permitted?
                if (counter < MAX_PEAKS)
                {
                  // No: register the peak
                  if (k==1) { arPeakPosH[counter++]=maxi; counter_maxH=counter; }
                  else      { arPeakPosR[counter++]=maxi; counter_maxR=counter; }
                  // Quit the search, if we already got the max. number of peaks permitted
                  if (counter == MAX_PEAKS)
                    break;
                }
              }
              j++;
              miny=array[i];
              mini=i;
            }
          } 
          else					
          {
            // Minima
            if (array[i] < miny) 
            {
              miny=array[i];
              mini=i;
            }
            if (array[i] > (miny + parameter.fNoiseLevel)) 
            {
              // Consider only Min's in the region permitted (i.e. the signals non-noisy part)
              if (mini >= parameter.nNoisy) 
              {
                // Min. found:
                // CD: Hin/R�ck
                if (k==1) counter=counter_minH;
                else      counter=counter_minR;
                // Check: Does the number of Min's found exceed the max. number of Min's permitted?
                if (counter < MAX_PEAKS)
                {
                  // No: register the Min.
                  if (k==1) { arMinPosH[counter++]=mini; counter_minH=counter; }
                  else      { arMinPosR[counter++]=mini; counter_minR=counter; }
                  // Quit the search, if we already got the max. number of Min's permitted
                  if (counter == MAX_PEAKS)
                    break;
                }
              }
              j++;
              maxy=array[i];
              maxi=i;
            }
          }
        }// E - for (ii)
      }//E - for (k)

      // ---------------------------
      // 2. step

      // Hin/R�ck-Peak evaluation:
      // Loop over the Hin-Peaks
      for (counter=0, i=0; i < counter_maxH; i++)
      {
        // Loop over the R�ck-Peaks
        for (iDeltaMin=int.MaxValue, j=0; j < counter_maxR; j++)
        {
          // Determine smallest Delta (Delta must be positive for Hin-Peak base)
          iDelta=arPeakPosR[j] - arPeakPosH[i];
          if (iDelta < iDeltaMin)
          {
            if (iDelta >= 0)
            {
              iDeltaMin=iDelta;   // currently smallest delta
              k=j;                // R�ck-Peak refered to
            }
          }
        }
        // Check: Delta = 0?
        if (0 == iDeltaMin)
        {
          // Yes:
          // the current Hin-Peak is a true peak -> register it
          peakPos[counter++]=arPeakPosH[i];  
        }
        else
        {
          // No:
          // Check: Is there at least one Min. between the current Hin- & R�ck-Peak?
          //        If yes, then the current Hin-Peak is NOT a true peak -> skip it
          //        If no, then the arithmetic mean of both peak positions is the true peak 
          // 1. Subcheck: Hin-Min's
          for (j=0; j < counter_minH; j++)
          {
            if ((arMinPosH[j] >= arPeakPosH[i]) && (arMinPosH[j] <= arPeakPosR[k]))
            {
              break;
            }
          }
          if (j == counter_minH)
          {
            // No Hin-Min between the current Hin- & R�ck-Peak:
            // 2. Subcheck: R�ck-Min's
            for (j=0; j < counter_minR; j++)
            {
              if ((arMinPosR[j] >= arPeakPosH[i]) && (arMinPosR[j] <= arPeakPosR[k]))
              {
                break;
              }
            }
            if (j == counter_minR)
            {
              // No R�ck-Min between the current Hin- & R�ck-Peak:
              // the arithmetic mean of both peak positions is the true peak
              peakPos[counter++]=(arPeakPosH[i]+arPeakPosR[k])/2;  
            }
          }
        }
      }
      
      // Overgive the number of peaks found
      nPeaks=counter;

      // ---------------------------
      // 3. step

      // Hin/R�ck-Min evaluation:
      // Loop over the peaks
      for (counter=0, i=0; i <= nPeaks; i++)
      {
        // Interval assignment: [ug, og]
        // If in [ug, og] a Hin-Min AND a R�ck-Min are encountered, then the true min is the arithmetic mean 
        // of both min positions.
        // In the opposite case (only one or no min is encountered in [ug, og]) we have no true min.  
        if      (i == 0)      { ug = 0;             og = peakPos[i];  }
        else if (i == nPeaks) { ug = peakPos[i-1];  og = nData-1;     }
        else                  { ug = peakPos[i-1];  og = peakPos[i];  }
        // Check: Is there a Hin-Min in the interval?
        for (j=0; j < counter_minH; j++)
        {
          if ((arMinPosH[j] >= ug) && (arMinPosH[j] <= og))
          {
            break;    // Yes
          }
        }
        // Check: Is there a R�ck-Min in the interval?
        for (k=0; k < counter_minR; k++)
        {
          if ((arMinPosR[k] >= ug) && (arMinPosR[k] <= og))
          {
            break;    // Yes
          }
        }
        // Check: Hin-Min AND a R�ck-Min in the interval?
        if ((j < counter_minH) && (k < counter_minR))
        {
          // Yes:
          // the arithmetic mean of both min positions is the true min
          minPos[counter++]=(arMinPosH[j]+arMinPosR[k])/2;  
        }
      }
      
      // Overgive the number of Min's found
      nMins=counter;
    }
    
    /// <summary>
    /// Rejects negative peaks, i.e. peaks localized in the negative Y half area.
    /// </summary>
    void RejectNegativePeaks (int[] array, int nData)
    {
      _rnp_lbl0:
        // Loop over all peaks    
        for (int i=0; i < g_nPeaks; i++)
        {
          // Position of the current peak  
          int pos = g_ariPeakPos[i];
          // Check: Peak localized in the negative Y half area?
          if ( array[pos] <= 0)
          {
            // Yes:
            // Reject it
            for (int j=i; j < g_nPeaks-1; j++)
            {
              g_ariPeakPos[j] =  g_ariPeakPos[j+1];
            }
            g_nPeaks--;  
            // Next search ...
            goto _rnp_lbl0;
          }
        }
    }

    #endregion peak search methods

    #region peak area detection methods

    /// <summary>
    /// Digital cyclic FIR filter (with (P)lus - sign):
    /// y = y(N) = Sum(j=-m,m) h(j)*x(N+j)
    /// </summary>
    /// <param name="x">The signal</param>
    /// <param name="n">Its size (dimension)</param>
    /// <param name="h">The filter</param>
    /// <param name="m">Its halfwidth</param>
    /// <param name="N">Array position, where the filtered signal should be calc'd (N in [0, 'n'-1])</param>
    /// <returns>The filtered signal at given array position</returns>
    /// <remarks>
    /// Literature:
    /// NR, ch. 14.8, (14.8.1)
    /// </remarks>
    double sg_FIR_zyk_p_position_2_I (int[] x, int n, double[] h, byte m, int N)
    {	
      int j, k, fw;
      double y;

      // Filter
      for (y=0,j=0; j <= 2*m ;j++) 
      {
        k = (N+j-m)%n;
        if (N+j-m < 0)	fw=x[(k != 0) ? k+n : k];	    // cyclic mapping "from down"
        else		        fw=x[k];		                  // cyclic mapping "from top"
        y += fw*h[j];
      }
      // ready
      return y;
    }

    /// <summary>
    /// Description: 
    ///  Integration (Quadrature).
    ///  Method: SIMPSON's extended trapezoidal rule.
    /// </summary>
    /// <param name="array"> The signal to be integrated</param>
    /// <param name="ia">The begin of the integration interval </param>
    /// <param name="ie">The end of the integration interval</param>
    /// <returns>The integration result</returns>
    float Integration (int[] array, int ia, int ie)
    {
      float fXspacing, fArea;
      int i;
      // Init. proofs
      if (ie < ia) { i=ia; ia=ie; ie=i; }
      // Distance between the signal's sample points = index difference between 2 successive samples = 1
      fXspacing = 1.0f;
      // Integration (Trapezoidal rule)
      fArea = (array[ia]+array[ie])/2;
      for (i=ia+1; i < ie; i++) fArea += array[i];
      fArea *= fXspacing;
      // Done
      return fArea;
    }

    /// <summary>
    /// Calculates the peak areas.
    /// Method: Area calculation based on Gauss peak shape assumption, 2nd derivative based Sigma
    ///         determination and the usage of the trapezoidal quadrature rule.
    /// </summary>
    /// <param name="array">The signal to be investigated (int signal)</param>
    /// <param name="nData">The array part, the peak areas should be detected within</param>
    /// <remarks>
    /// 1.
    ///   Globals, that must be known at input:
    ///     - g_ariPeakPos: The peak position (index) array containing the bin numbers of the 'nPeaks' peaks found (output)
    ///     - g_nPeaks: The number of peaks found (output)
    /// 2.
    ///   The Sigma parameter of a Gauss peak calculates as follows:
    ///     Sigma = sqrt(- h/h''), 
    ///   with: h   - Height of the peak (peak height),
    ///         h'' - 2nd derivative at peak position
    /// 3.
    ///   Peak overlapping is considered.
    /// </remarks>
    void PeakAreaDetect_Sigma_Calc (int[] array, int nData) 
    {
      byte j;
      int pos, ia, ie, iaprev=0, ieprev=0, k;
      float y2d, sigma, fArea;

      // Do nothing, if no peaks are present
      if (0 == g_nPeaks) return;
      // Loop over all peaks
      for (j=0; j < g_nPeaks; j++)
      {
        // The current peak position
        pos=g_ariPeakPos[j];  
        // 2nd derivative at current peak position
        y2d=(float)sg_FIR_zyk_p_position_2_I (array, nData, g_pDer2_8, 8, pos);
        // Sigma (based on Gauss peak shape assumption)
        sigma=array[pos]/y2d;
        if (sigma < 0) sigma = -sigma;
        sigma=(float) Math.Sqrt(sigma);
        // Gauss peak begin/end
        ia=(int) (pos - 3*sigma); if (ia < 0) ia = 0;                        
        ie=(int) (pos + 3*sigma); if (ie > nData-1) ie=nData-1;
        // Check: Is the current peak the 2nd, 3rd, ... peak?
        if (j > 0)
        {
          // Yes:
          // Check: Do the current & the previous peaks overlap?
          if (ia <= ieprev)
          {
            // Yes:
            // Resolve overlapping
            k=(ia+ieprev)/2;
            ia=k+1;
            ieprev=k;
          }
          // Previous peak: Integration
          fArea = Integration (array, iaprev, ieprev);
          // Previous peak: Store peak area
          g_arfPeakArea[j-1]=fArea;  
        }
        // Remember current peak begin/end
        iaprev=ia;
        ieprev=ie;
      }
      // Last peak: Integration
      fArea = Integration (array, iaprev, ieprev);
      // Last peak: Store peak area
      g_arfPeakArea[j-1]=fArea;  
    }

    /// <summary>
    /// Calculates the peak areas - Extended version.
    /// Method: Area calculation based on Gauss peak shape assumption, 2nd derivative based Sigma
    ///         determination and the usage of the trapezoidal quadrature rule. Minima positions are included.
    /// </summary>
    /// <param name="array">The signal to be investigated (int signal)</param>
    /// <param name="nData">The array part, the peak areas should be detected within</param>
    /// <param name="parameter">The Peak detection parameter</param>
    /// <remarks>
    /// 1.
    ///   Globals, that must be known at input:
    ///     - g_ariPeakPos: The peak position (index) array containing the bin numbers of the 'nPeaks' peaks found (output)
    ///     - g_nPeaks: The number of peaks found (output)
    ///     - g_ariMinPos: The Min position (index) array containing the bin numbers of the 'nMins' Min's found (output)
    ///     - g_nMins: The number of Min's found (output)
    /// 2.
    ///   The Sigma parameter of a Gauss peak calculates as follows:
    ///     Sigma = sqrt(- h/h''), 
    ///   with: h   - Height of the peak (peak height),
    ///         h'' - 2nd derivative at peak position
    /// 3.
    ///   Peak overlapping is considered.
    /// </remarks>
    void PeakAreaDetectEx_Sigma_Calc (int[] array, int nData, SpecEvalParameter parameter) 
    {
      byte j;
      int pos, ia, ie, iaprev=0, ieprev=0, k, posmin=0, l;
      float y2d, sigma, fArea;

      // Do nothing, if no peaks are present
      if (0 == g_nPeaks) return;
      // Loop over all peaks
      for (j=0; j < g_nPeaks; j++)
      {
        // The current peak position
        pos=g_ariPeakPos[j];  
        // 2nd derivative at current peak position
        y2d=(float)sg_FIR_zyk_p_position_2_I (array, nData, g_pDer2_8, 8, pos);
        // Sigma (based on Gauss peak shape assumption)
        sigma=array[pos]/y2d;
        if (sigma < 0) sigma = -sigma;
        sigma=(float) Math.Sqrt(sigma);
        // Gauss peak begin/end
        ia=(int) (pos - 3*sigma); if (ia < 0) ia = 0;                        
        ie=(int) (pos + 3*sigma); if (ie > nData-1) ie=nData-1;
        // Cleaning up with respect to negative area parts:
        // Peak begin/end positions are overcalc'd in a manner, so that only positive channels
        // are located between.
        for (int i = pos-1; i >= ia ;i--)         // Loop left from the peak
        {
          // Check: Current value less than the noise level?
          if (array[i] < parameter.fNoiseLevel)
          {
            // Yes:
            ia = i+1;     // The left limiting peak position (above noise level)
            break;
          }
        }
        for (int i = pos+1; i <= ie ;i++)         // Loop right from the peak
        {
          // Check: Current value less than the noise level?
          if (array[i] < parameter.fNoiseLevel)
          {
            // Yes:
            ie = i-1;     // The right limiting peak position (above noise level)
            break;
          }
        }
        // Check: Is the current peak the 2nd, 3rd, ... peak?
        if (j > 0)
        {
          // Yes:
          // Check: Do the current & the preceding peaks overlap?
          if (ia <= ieprev)
          {
            // Yes:
            // Resolve overlapping:
            //    Loop over all Min's
            for (l=0; l < g_nMins;l++)
            {
              // Position of the current Min.
              posmin=g_ariMinPos[l];
              // Check: Min. located between the current peak and the preceding one?
              if ((posmin > g_ariPeakPos[j-1]) && (posmin < pos))
              {
                // Yes
                break;
              }
            }
            //    Assignment of the peak separator position:
            if (l < g_nMins)  k=posmin;         // Min. position, if found
            else              k=(ia+ieprev)/2;  // If not found: 50-50 
            //    Overdefinition of peak begin/end positions:
            ia=k+1;                             // Begin position of the current peak
            ieprev=k;                           // End position of the preceding peak
          }
          // Preceding peak: Integration
          fArea = Integration (array, iaprev, ieprev);
          // Preceding peak: Store peak area
          g_arfPeakArea[j-1]=fArea;  
        }
        // Remember current peak begin/end
        iaprev=ia;
        ieprev=ie;
      }
      // Last peak: Integration
      fArea = Integration (array, iaprev, ieprev);
      // Last peak: Store peak area
      g_arfPeakArea[j-1]=fArea;  
    }
    
    /// <summary>
    /// Calculates the peak areas - Extended version.
    ///  Method: Area calculation based on peak begin/end position search with break-off on Noise level 
    ///         and using the trapezoidal quadrature rule. Minima positions are included.
    /// </summary>
    /// <param name="array">The signal to be investigated (int signal)</param>
    /// <param name="nData">The # of signal points, that should be included in the peak area detection</param>
    /// <param name="parameter">The Peak detection parameter</param>
    /// <remarks>
    /// 1.
    ///   Globals, that must be known at input:
    ///     - g_ariPeakPos: The peak position (index) array containing the bin numbers of the 'nPeaks' peaks found (output)
    ///     - g_nPeaks: The number of peaks found (output)
    ///     - g_ariMinPos: The Min position (index) array containing the bin numbers of the 'nMins' Min's found (output)
    ///     - g_nMins: The number of Min's found (output)
    /// 2.
    ///   Peak overlapping is considered.
    ///   Note, that if the peaks have big broadenings on its roots, this method is less suitable!     
    /// </remarks>
    void PeakAreaDetectEx_NoiseLevel (int[] array, int nData, SpecEvalParameter parameter) 
    {
      byte j;
      int pos, llim, rlim,  i, il, ir, l, ug, og;
      float fArea;
  
      // Do nothing, if no peaks are present
      if (0 == g_nPeaks) return;
      // Loop over all peaks
      for (j=0; j < g_nPeaks; j++)
      {
        // The current peak position
        pos = g_ariPeakPos[j];  
   
        // ---------------------------------
        // 0. part: Search limiters determination
        
        // Search limiters to the left / right
        //  1. Init.
        llim=(j==0) ? 0 : g_ariPeakPos[j-1];
        rlim=(j==g_nPeaks-1) ? nData-1 : g_ariPeakPos[j+1];
        //  2. Min. inclusion
        if (g_nMins > 0)
        {
          // Determint the flanking Min's
          for (l=0; l < g_nMins;l++)
          {
            if (pos < g_ariMinPos[l])
              break;
          }
          if      (l==0)          { ug=0; og=g_ariMinPos[l]; }
          else if (l==g_nMins)    { ug=g_ariMinPos[l-1]; og=nData-1; }
          else                    { ug=g_ariMinPos[l-1]; og=g_ariMinPos[l]; }
          // Superposition
          llim = Math.Max (llim, ug);
          rlim = Math.Min (rlim, og);
        }

        // ---------------------------------
        // 1. part: Noise level
        
        // Search to the left: Limiting peak position
        for (i=pos-1; i > llim ;i--)  // Loop left from the peak
        {
          // Check: Current value less than the noise level?
          if (array[i] < parameter.fNoiseLevel) 
            break;  // Yes
        }
        il = i+1;   // The left limiting peak position (above noise level)  
    
        // Search to the right: Limiting peak position
        for (i=pos+1; i < rlim ;i++)  // Loop right from the peak
        {
          // Check: Current value less than the noise level?
          if (array[i] < parameter.fNoiseLevel) 
            break;  // Yes
        }
        ir = i-1;   // The right limiting peak position (above noise level)      
    
        // ---------------------------------
        // 2. part: Integration
        
        // Peak area determination
        fArea = Integration (array, il, ir);
    
        // Store peak area
        g_arfPeakArea[j] = fArea;    
      }
    }

   
    /// <summary>
    /// Calculates the peak areas - Extended version.
    ///  Method: Combined
    ///   1. For PA mode Simple/Ext1/Ext2:  peak begin/end position search with break-off on Noise level
    ///   2. For PA mode Ext1/Ext2:         peak begin/end position search using Parabola fitting
    ///   3. For PA mode Ext2:              peak begin/end position search using Gaussian fitting
    ///   4. The greater peak begin position and the smaller peak end position are used for
    ///      Area calculation based on the trapezoidal quadrature rule.
    /// </summary>
    /// <param name="array">The signal to be investigated (int signal)</param>
    /// <param name="nData">The # of signal points, that should be included in the peak area detection</param>
    /// <param name="parameter">The Peak detection parameter</param>
    /// <param name="nModePA">The peak area determination mode</param>
    /// <returns>1 - OK,  0 - error</returns>
    /// <remarks>
    /// 1.
    ///   Globals, that must be known at input:
    ///     - g_ariPeakPos: The peak position (index) array containing the bin numbers of the 'nPeaks' peaks found (output)
    ///     - g_nPeaks: The number of peaks found (output)
    ///     - g_ariMinPos: The Min position (index) array containing the bin numbers of the 'nMins' Min's found (output)
    ///     - g_nMins: The number of Min's found (output)
    /// 2.
    ///   Peak overlapping is considered.
    /// </remarks>
    void PeakAreaDetectEx_Combined (int[] array, int nData, SpecEvalParameter parameter, int nModePA) 
    {
      byte j, res1, res2;
      int pos, llim, rlim,  i, il, ir, l, ug, og, il1, ir1, il2, ir2, il3, ir3, ilInterim, irInterim;
      float fArea;

      // Do nothing, if no peaks are present
      if (0 == g_nPeaks) return;
      // Loop over all peaks
      for (j=0; j < g_nPeaks; j++)
      {
        // The current peak position
        pos = g_ariPeakPos[j];  
   
        // ---------------------------------
        // 0. part: Search limiters determination

        // Search limiters to the left / right
        //  1. Init.
        llim=(j==0) ? 0 : g_ariPeakPos[j-1];
        rlim=(j==g_nPeaks-1) ? nData-1 : g_ariPeakPos[j+1];
        //  2. Min. inclusion
        if (g_nMins > 0)
        {
          // Determint the flanking Min's
          for (l=0; l < g_nMins;l++)
          {
            if (pos < g_ariMinPos[l])
              break;
          }
          if      (l==0)          { ug=0; og=g_ariMinPos[l]; }
          else if (l==g_nMins)    { ug=g_ariMinPos[l-1]; og=nData-1; }
          else                    { ug=g_ariMinPos[l-1]; og=g_ariMinPos[l]; }
          // Superposition
          llim = Math.Max (llim, ug);
          rlim = Math.Min (rlim, og);
        }

        // ---------------------------------
        // 1. part: Noise level
        
        // Search to the left: Limiting peak position
        for (i=pos-1; i > llim ;i--)  // Loop left from the peak
        {
          // Check: Current value less than the noise level?
          if (array[i] < parameter.fNoiseLevel) 
            break;  // Yes
        }
        il1 = i+1;   // The left limiting peak position (above noise level)  
    
        // Search to the right: Limiting peak position
        for (i=pos+1; i < rlim ;i++)  // Loop right from the peak
        {
          // Check: Current value less than the noise level?
          if (array[i] < parameter.fNoiseLevel) 
            break;  // Yes
        }
        ir1 = i-1;   // The right limiting peak position (above noise level)      
    
        // CD: peak area detection mode
        if (nModePA == 0)
        {
          // Simple: 
          // The results of the Noiselevel method act as limiters 
          // for the peak area determination.
          il = il1;
          ir = ir1;
        }
        else
        {
          // Extended:
          
          // ---------------------------------
          // 2. part: Parabola fit
        
          // SVD directly (NRU)
          res1=svd_direct (array, nData, pos, llim, rlim, out il2, out ir2);
          // Notes:
          //  The routine 'svd_fit' yields the same results as routine 'svd_direct'.
          //  Nevertheless for usage on the PID device the routine 'svd_direct' seems to be more reliable.
          //res1=svd_fit (array, nData, pos, llim, rlim, out il2, out ir2);

          // CD: peak area detection mode
          if (nModePA == 1)
          {
            // Extended - level 1:
            // Firstly, the results of Parabola fit are overtaken.
            // Secondly, the results of the Noiselevel method act as absolue limiters downwards (max)
            // resp. upwards (min).
            // The superposition results act as limiters for the peak area determination.
            if (res1==1) { ilInterim=il2; irInterim=ir2; }
            else         { ilInterim=0; irInterim=int.MaxValue; }
            il = Math.Max (il1, ilInterim);
            ir = Math.Min (ir1, irInterim);
          }
          else
          {
            // Extended - level 2:
            
            // ---------------------------------
            // 3. part: Gaussian fit
          
            // Levenberg-Marquardt method (SpevRout)
            res2=ml_sr_ad (array, nData, pos, llim, rlim, out il3, out ir3);     // device-like version
            // Notes:
            //  The routines 'ml_nru'/'ml_nru_ad' and the routines 'ml_sr'/'ml_sr_ad' yield the same results.
            //  For usage on the PID device the routine 'ml_sr_ad' seems to be most reliable.
            //res2=ml_sr (array, nData, pos, llim, rlim, out il3, out ir3);        // PC-like version
            //res2=ml_nru (array, nData, pos, llim, rlim, out il3, out ir3);       // PC-like version
            //res2=ml_nru_ad (array, nData, pos, llim, rlim, out il3, out ir3);    // device-like version

            // ---------------------------------
            // 4. part: Superposition

            // Firstly, the results of Parabola and Gaussian fit are arithmetically averaged.
            // Secondly, the results of Noiselevel method act as absolue limiters downwards (max)
            // resp. upwards (min).
            if (res1==1 && res2==1) { ilInterim=(il2+il3)/2; irInterim=(ir2+ir3)/2; }
            else                    { ilInterim=0; irInterim=int.MaxValue; }
            il = Math.Max (il1, ilInterim);
            ir = Math.Min (ir1, irInterim);
          }
        }

        // Peak area determination
        fArea = Integration (array, il, ir);
    
        // Store peak area
        g_arfPeakArea[j] = fArea;    
      }
    }
    
    /// <summary>
    /// Parabola fit by means of SVD (using NRU routines 'svdcmp' & 'svbksb' directly)
    /// </summary>
    /// <returns>1 - OK, 0 - error</returns>
    /// <remarks>
    /// This routine and the routine 'svd_fit' yield the same results. Because this routine is 
    /// less expansive in dynamical memory alloc's, we prefer it with regard to a usage in the PID device SW.
    /// </remarks>
    public byte svd_direct (int[] array, int nData, int pos, int llim, int rlim, out int il2, out int ir2)     
    {
      int i;
      bool bSuccess;
      float s, sx, sy, sxx, sxy, sxxx, sxxy, sxxxx, a, b, c, q, p, D, Dsqr, x1, x2;
      float[,] ma, v;
      float[] w, vb, vx;

      // Search to the left: Limiting peak position
      for (i=pos-1; i > llim ;i--)  // Loop left from the peak
      {
        // Check: Current value less than half maximum?
        if (array[i] < array[pos]/2) 
          break;  // Yes
      }
      il2 = i+1;   // The left limiting peak position (above half maximum)  
    
      // Search to the right: Limiting peak position
      for (i=pos+1; i < rlim ;i++)  // Loop right from the peak
      {
        // Check: Current value less half maximum?
        if (array[i] < array[pos]/2) 
          break;  // Yes
      }
      ir2 = i-1;   // The right limiting peak position (above half maximum)      

      // Parabola fit: Fitting function y=a + bx + cx^2
      // Equation system:
      //  S * a   + Sx * b   + Sxx * c   = Sy
      //  Sx * a  + Sxx * b  + Sxxx * c  = Sxy
      //  Sxx * a + Sxxx * b + Sxxxx * c = Sxxy
      // with:
      //  S = Sum(i) 1; Sx = Sum(i) x(i); Sxx = Sum(i) x(i)^2; Sxxx = Sum(i) x(i)^3; Sxxxx = Sum(i) x(i)^4;
      //  Sy = Sum(i) y(i); Sxy = Sum(i) x(i)*y(i); Sxxy = Sum(i) x(i)*x(i)*y(i);

      // Reset success indication
      bSuccess = false;
      // Calculate coeff's
      s = sx = sy = sxx = sxy = sxxx = sxxy = sxxxx = 0;  
      for (i=il2; i <= ir2; i++)
      {
        int ii = i - pos;           // Data points 0-centered in abscissa (goal: keep small coeff's of equation system)
        s += 1;
        sx += ii;
        sy += array[i];
        sxx += ii*ii;
        sxy += ii*array[i];
        sxxx += ii*ii*ii;
        sxxy += ii*ii*array[i];
        sxxxx += ii*ii*ii*ii;
      }
      // Calculate a, b, c by means of SVD algorithm
      int m=3;
      int n=3;
      ma = new float[m+1,n+1];                    // due to 1-offset of arrays in routine 'svdcmp'
      ma[1,1]=s; ma[1,2]=sx; ma[1,3]=sxx; 
      ma[2,1]=sx; ma[2,2]=sxx; ma[2,3]=sxxx; 
      ma[3,1]=sxx; ma[3,2]=sxxx; ma[3,3]=sxxxx; 
      w = new float[n+1];                         // due to 1-offset of arrays in routine 'svdcmp'
      v = new float[n+1,n+1];                     // due to 1-offset of arrays in routine 'svdcmp'
      string psErr="";
      // SVD
      cNRU nru = new cNRU ();
      byte res=nru.svdcmp(ma,m,n,w,v,ref psErr);
      if (res==1)
      {
        // SVD successfull:
        // Calculate solution vector
        vb = new float[m+1];                    // due to 1-offset of arrays in routine 'svbksb'
        vb[1]=sy; vb[2]=sxy; vb[3]=sxxy; 
        vx = new float[n+1];                    // due to 1-offset of arrays in routine 'svbksb'
        nru.svbksb(ma,w,v,m,n,vb,vx);
        // Assign a, b, c
        a=vx[1];
        b=vx[2];
        c=vx[3];
        // Fitting parabola: Zero points (Nullstellen)
        q=a/c;
        p=b/c;
        Dsqr=p*p/4-q;
        if (Dsqr >= 0)
        {
          D=(float) Math.Sqrt(Dsqr);
          x1=-p/2-D;    // left
          x2=-p/2+D;    // right
          // Re-assignment: zero points -> channels
          il2 = (int) x1; il2 += pos;
          ir2 = (int) x2; ir2 += pos;
          // Set success indication
          bSuccess = true;  
        }
      }

      // Ensure, that noise level values will be used in Peak area determination in case of failure
      if (!bSuccess)
      {
        il2=0;
        ir2 = int.MaxValue;
      }
      
      // ready
      res = bSuccess ? (byte) 1 : (byte) 0;
      return res;
    }

    /// <summary>
    /// Parabola fit by means of SVD (NRU routine 'svdfit')
    /// </summary>
    /// <returns>1 - OK, 0 - error</returns>
    /// <remarks>
    /// This routine and the routine 'svd_direct' yield the same results. Because the routine 'svd_direct' is 
    /// less expansive in dynamical memory alloc's, we prefer it with regard to a usage in the PID device SW.
    /// </remarks>
    public byte svd_fit (int[] array, int nData, int pos, int llim, int rlim, out int il2, out int ir2)     
    {
      int i, j, ma, ndata;
      float q, p, D, Dsqr, x1, x2, chisq;
      float[,] u, v;
      float[] x, y, sig, a, w;
      byte res;
      string sErr = "";

      // Search to the left: Limiting peak position
      for (i=pos-1; i > llim ;i--)  // Loop left from the peak
      {
        // Check: Current value less than half maximum?
        if (array[i] < array[pos]/2) 
          break;  // Yes
      }
      il2 = i+1;   // The left limiting peak position (above half maximum)  
    
      // Search to the right: Limiting peak position
      for (i=pos+1; i < rlim ;i++)  // Loop right from the peak
      {
        // Check: Current value less half maximum?
        if (array[i] < array[pos]/2) 
          break;  // Yes
      }
      ir2 = i-1;   // The right limiting peak position (above half maximum)      

      // ---------------------------------------------------------
      // Parabola fit: Fitting function y=a(1) + a(2)*x + a(3)*x^2
     
      // Init.
      ndata = ir2-il2+1;                    // # of data points
      ma=3;                                 // polynomial degree of fitting function + 1

      // Alloc. (+1 due to 1-offset of all arrays in 'svdfit' routine)
      x=new float [ndata+1];                // data arry (x) for SVD fit
      y=new float [ndata+1];                // data arry (y) for SVD fit
      sig=new float [ndata+1];
      
      a = new float[ma+1];                  // coeff's of the fitting function        
      u = new float[ndata+1,ma+1];          //workspace          
      v = new float[ma+1,ma+1];             // dito       
      w = new float[ma+1];                  // dito       
      
      // Fill
      //  x, y, sig
      //  Notes:
      //  The abscissas of the data segment to be fitted is shifted in a manner, so that the central
      //  parabola axis passes the point of origin.
      for (i=il2; i <= ir2; i++)  
      {
        j=i-il2+1;
        x[j] = (float)(i-pos);
        y[j] = array[i];
        sig[j] = 1;
      }
    
      // Routine
      cNRU nru = new cNRU ();
      res=nru.svdfit(x,y,sig,ndata,a,ma,u,v,w,out chisq,new cNRU.GenLinLeastSquareFuncDelegate(nru.fpoly),ref sErr);
      if (1 == res)
      {
        // OK:
        // Fitting parabola: Zero points (Nullstellen)
        q=a[1]/a[3];
        p=a[2]/a[3];
        Dsqr=p*p/4-q;
        if (Dsqr >= 0)
        {
          D=(float) Math.Sqrt(Dsqr);
          x1=-p/2-D;    // left
          x2=-p/2+D;    // right
          // Re-assignment: zero points -> channels
          il2 = (int) x1; il2 += pos;
          ir2 = (int) x2; ir2 += pos;
        }
        else
          res=0;
      }
      
      if (0 == res)
      {
        // error:
        // Ensure, that noise level values will be used in Peak area determination in case of failure
        il2=0;
        ir2 = int.MaxValue;
      }

      // ready
      return res;
    }

    /// <summary>
    /// Gaussian fit by means of the Levenberg-Marquardt method (NRU)
    /// (PC-like version)
    /// </summary>
    /// <returns>1 - OK, 0 - error</returns>
    /// <remarks>
    /// 1.
    /// Gaussian model:
    ///  y = B * exp (-((x - E)/G)^2)
    ///    with: B - amplitude (height) of the peak, E - center (position) of the peak, G - width of the peak 
    ///          (G is correlated with the sigma-parameter as follows: G = sqrt(2) * sigma)
    /// 2.    
    /// This routine and the routine 'ml_sr' yield the same results. Because the routine 'ml_sr' is 
    /// more compact and less expansive in dynamical memory alloc's, we prefer it with regard to
    /// a usage in the PID device SW.
    /// The device-like version of this routine is 'ml_nru_ad'. Unnecessary to mention, that this routine and 
    /// its device-like version yield the same results. 
    /// </remarks>
    public byte ml_nru (int[] array, int nData, int pos, int llim, int rlim, out int il2, out int ir2, out string sErr)     
    {
      int[] ia;
      int MA, NPT, maxiter, i, j;
      float[] a, x, y, sig; 
      float[,] covar, alpha;
      float alamda, chisq, threesig, cridif, fwhm;
      byte res;

      // Init
      MA=3;                                     // # of Gaussians: 'MA'/3,
      NPT=rlim-llim+1;                          // # of data points: 'NPT'
      cridif = 1e-4f;                           // critical chisq-delta for stopping the routine
      maxiter = 100;                            // max. # of iterations permitted   
    
      // Alloc (+1 due to 1-offset of all arrays in 'mrqmin' routine)
      x=new float [NPT+1];
      y=new float [NPT+1];
      sig=new float [NPT+1];

      a=new float [MA+1];   	                  // on input: initial guess of the param's of the nonlinear fitting function
                                                // on output: current best-fit values for the parameters
      ia=new int [MA+1];                        // components that should be fitted for (1) - held fixed (0)
      covar=new float [MA+1,MA+1];  	          // working space: covariance matrix
                                                // on output: covar[i][i] - variances (= squared uncertainities) of the a[i],
                                                //	        i.e.: uncertainties=sqrt(covar[i][i])
      alpha=new float [MA+1,MA+1];	            // working space: curvature matrix
    
      // Fill
      //  x, y, sig (Achtung: rescale in x)
      for (i=llim; i <= rlim; i++)  
      {
        j=i-llim+1;
        x[j] = (float)i;
        y[j] = array[i];
        sig[j] = 1;
      }
  
      //  a
      a[1]=array[pos];   	                      // Amplitude
      a[2]=(float)pos;	                        // Center
      for (i=pos-1; i > llim ;i--)              // (Loop left from the peak)
        if (array[i] < array[pos]/2) break;
      if (i > llim) 
        fwhm=(float)(2*(pos-i));                // FWHM
      else
      {
        for (i=pos+1; i < rlim ;i++)            // (Loop right from the peak)
          if (array[i] < array[pos]/2) break;
        fwhm=(float)(2*(i-pos));               
      }
      a[3]=(float)(0.6*fwhm);                   // Width: = sqrt(2)*sigma = sqrt(2)*(FWHM/2.3548) = 0.6*FWHM

      for (i=1; i <= MA ;i++) 
        ia[i]=1;					                      // all param's should be fitted.
    
      // Routine
      cNRU nru = new cNRU ();
      res=nru.mrqmin_env(x,y,sig,NPT,a,ia,MA,covar,alpha,out chisq, new cNRU.NonLinFuncDelegate(nru.fgauss), 
                         out alamda,maxiter,cridif,out sErr);
      if (res == 1)
      {
        // OK:
        // Channel assignment: ug/og = pos -/+ 3*sigma
        threesig=(float)(3*(a[3]/Math.Sqrt(2)));                    
        il2 = (int)(pos - threesig);
        ir2 = (int)(pos + threesig);
      }
      else
      {
        // error:
        // Ensure, that noise level values will be used in Peak area determination in case of failure
        il2=0;
        ir2 = int.MaxValue;
      }

      // ready
      return res;
    }
    
    /// <summary>
    /// Gaussian fit by means of the Levenberg-Marquardt method (NRU)
    /// (Device-like version)
    /// </summary>
    /// <returns>1 - OK, 0 - error</returns>
    /// <remarks>
    /// 1.
    /// Gaussian model:
    ///  y = B * exp (-((x - E)/G)^2)
    ///    with: B - amplitude (height) of the peak, E - center (position) of the peak, G - width of the peak 
    ///          (G is correlated with the sigma-parameter as follows: G = sqrt(2) * sigma)
    /// 2.    
    /// This routine and the routine 'ml_sr' yield the same results. Because the routine 'ml_sr' is 
    /// more compact and less expansive in dynamical memory alloc's, we prefer it with regard to
    /// a usage in the PID device SW.
    /// The PC-like version of this routine is 'ml_nru'. Unnecessary to mention, that this routine and 
    /// its PC-like version yield the same results. 
    /// </remarks>
    unsafe public byte ml_nru_ad (int[] array, int nData, int pos, int llim, int rlim, out int il2, out int ir2)     
    {
      int[] ia;
      int MA, NPT, maxiter, i;
      float[] a; 
      float[,] covar, alpha;
      float x0, dx, alamda, chisq, threesig, cridif, fwhm;
      byte res;
      string sErr;

      // Init
      MA=3;                                     // # of Gaussians: 'MA'/3,
      NPT=rlim-llim+1;                          // # of data points: 'NPT'
      cridif = 1e-4f;                           // critical chisq-delta for stopping the routine
      maxiter = 100;                            // max. # of iterations permitted   
    
      // Alloc (+1 due to 1-offset of all arrays in 'mrqmin' routine)
      a=new float [MA+1];   	                  // on input: initial guess of the param's of the nonlinear fitting function
      // on output: current best-fit values for the parameters
      ia=new int [MA+1];                        // components that should be fitted for (1) - held fixed (0)
      covar=new float [MA+1,MA+1];  	          // working space: covariance matrix
      // on output: covar[i][i] - variances (= squared uncertainities) of the a[i],
      //	        i.e.: uncertainties=sqrt(covar[i][i])
      alpha=new float [MA+1,MA+1];	            // working space: curvature matrix
    
      // Fill
  
      //  a
      a[1]=array[pos];   	                      // Amplitude
      a[2]=(float)pos;	                        // Center
      for (i=pos-1; i > llim ;i--)              // (Loop left from the peak)
        if (array[i] < array[pos]/2) break;
      if (i > llim) 
        fwhm=(float)(2*(pos-i));                // FWHM
      else
      {
        for (i=pos+1; i < rlim ;i++)            // (Loop right from the peak)
          if (array[i] < array[pos]/2) break;
        fwhm=(float)(2*(i-pos));               
      }
      a[3]=(float)(0.6*fwhm);                   // Width: = sqrt(2)*sigma = sqrt(2)*(FWHM/2.3548) = 0.6*FWHM

      for (i=1; i <= MA ;i++) 
        ia[i]=1;					                      // all param's should be fitted.
    
      //  x
      x0=llim; dx=1;                            // data points - x: begin point & spacing
      
      // Routine
      fixed (int* y = &array[llim])             // data points - y 
      {
        int* yy = y-1;                          // '-1' due to 1-offset of the y-array in routine 'marqfit': y = &array[llim]-1
        // Routine
        cNRU_ad nru = new cNRU_ad ();
        res=nru.mrqmin_env(x0,dx,yy,NPT,a,ia,MA,covar,alpha,out chisq, new cNRU_ad.NonLinFuncDelegate(nru.fgauss), 
                          out alamda,maxiter,cridif,out sErr);
      }
      
      // Eval
      if (res == 1)
      {
        // OK:
        // Channel assignment: ug/og = pos -/+ 3*sigma
        threesig=(float)(3*(a[3]/Math.Sqrt(2)));                    
        il2 = (int)(pos - threesig);
        ir2 = (int)(pos + threesig);
      }
      else
      {
        // error:
        // Ensure, that noise level values will be used in Peak area determination in case of failure
        il2=0;
        ir2 = int.MaxValue;
      }

      // ready
      return res;
    }

    /// <summary>
    /// Gaussian fit by means of the Levenberg-Marquardt method (SpevRout)
    /// (PC-like version)
    /// </summary>
    /// <returns>1 - OK, 0 - error</returns>
    /// <remarks>
    /// 1.
    /// Gaussian model:
    ///  y = (A/sqrt(2Pi)/sigma) * exp (-((x - �)/sigma)^2/2)
    ///    with: A - area of the peak, � - center (position) of the peak, sigma - difference between center and inflection point
    ///          of the peak ('width' parameter) 
    /// 2.    
    /// This routine and the routine 'ml_nru' yield the same results. Because this routine is 
    /// more compact and less expansive in dynamical memory alloc's, we prefer it with regard to
    /// a usage in the PID device SW.
    /// The device-like version of this routine is 'ml_sr_ad'. Unnecessary to mention, that this routine and 
    /// its device-like version yield the same results. 
    /// </remarks>
    public byte ml_sr (int[] array, int nData, int pos, int llim, int rlim, out int il2, out int ir2)     
    {
      int i, j, ierr, maxiter, npts, nterms, nb, np;
      float chisqr, flamda, cridif, fwhm, threesig;
      float[] x, y, w, yfit, a, sa, b, beta, deriv, alfa;
      double[] arr;
      byte res;

      // Init
      cridif = 1e-4f;                           // critical chisq-delta for stopping the routine
      maxiter = 100;                            // max. # of iterations permitted
      npts = rlim-llim+1;
      nb = 0;                                   // # of polynomial background terms
      np = 1;                                   // # of Gaussians
      nterms = nb + 3*np;                       // overall # of fitting parameters

      // Alloc (+1 due to 1-offset of all arrays in 'marqfit' routine)
      x = new float [npts+1];
      y = new float [npts+1];
      w = new float [npts+1];
      yfit = new float [npts+1];
      a = new float [nterms+1];
      sa = new float [nterms+1];
      b = new float [nterms+1];
      beta = new float [nterms+1];
      deriv = new float [nterms+1];
      alfa = new float [nterms*(nterms+1)/2+1];
      arr = new double [nterms*(nterms+1)/2+1];

      // Fill
      //  x, y, w (Achtung: rescale in x)
      for (i=llim; i <= rlim; i++)  
      {
        j=i-llim+1;
        x[j] = (float)i;
        y[j] = array[i];
        w[j] = 1;
      }

      //  a: Gaussian parameters
      a[2]=(float)pos;	                        // position
      for (i=pos-1; i > llim ;i--)              // (Loop left from the peak)
        if (array[i] < array[pos]/2) break;
      if (i > llim) 
        fwhm=(float)(2*(pos-i));                // FWHM
      else
      {
        for (i=pos+1; i < rlim ;i++)            // (Loop right from the peak)
          if (array[i] < array[pos]/2) break;
        fwhm= (float)(2*(i-pos));                        
      }
      a[3]=(float)(fwhm / 2.3548);              // 'width': = sigma = FWHM / 2.3548
      a[1]=(float)(array[pos]*2.50663*a[3]);    // area: = height * sqrt(2Pi) * sigma

      // Routine
      cSpevRout se = new cSpevRout ();
      se.marqfit(out ierr, out chisqr, out flamda, cridif, maxiter, x, y, w, yfit, npts, 
                 a, sa, nb, np, b, beta, deriv, alfa, arr);
      if (-1 != ierr)
      {
        // OK:
        // Channel assignement: ug/og = pos -/+ 3*sigma
        threesig = 3*a[3];
        il2 = (int)(pos - threesig);
        ir2 = (int)(pos + threesig);
      }
      else
      {
        // error:
        // Ensure, that noise level values will be used in Peak area determination in case of failure
        il2=0;
        ir2 = int.MaxValue;
      }

      // ready
      res = (-1 != ierr) ? (byte) 1 : (byte) 0;
      return res;
    }

    /// <summary>
    /// Gaussian fit by means of the Levenberg-Marquardt method (SpevRout)
    /// (Device-like version)
    /// </summary>
    /// <returns>1 - OK, 0 - error</returns>
    /// <remarks>
    /// 1.
    /// Gaussian model:
    ///  y = (A/sqrt(2Pi)/sigma) * exp (-((x - �)/sigma)^2/2)
    ///    with: A - area of the peak, � - center (position) of the peak, sigma - difference between center and inflection point
    ///          of the peak ('width' parameter) 
    /// 2.    
    /// This routine and the routine 'ml_nru' yield the same results. Because this routine is 
    /// more compact and less expansive in dynamical memory alloc's, we prefer it with regard to
    /// a usage in the PID device SW .
    /// The PC-like version of this routine is 'ml_sr'. Unnecessary to mention, that this routine and 
    /// its PC-like version yield the same results. 
    /// </remarks>
    unsafe public byte ml_sr_ad (int[] array, int nData, int pos, int llim, int rlim, out int il2, out int ir2)     
    {
      int i, ierr, maxiter, npts, nterms, nb, np;
      float x0, dx, chisqr, flamda, cridif, fwhm, threesig;
      float[] a, sa, b, beta, deriv, alfa;
      double[] arr;
      byte res;

      // Init
      cridif = 1e-4f;                           // critical chisq-delta for stopping the routine
      maxiter = 100;                            // max. # of iterations permitted
      npts = rlim-llim+1;
      nb = 0;                                   // # of polynomial background terms
      np = 1;                                   // # of Gaussians
      nterms = nb + 3*np;                       // overall # of fitting parameters

      // Alloc (+1 due to 1-offset of all arrays in 'marqfit' routine)
      a = new float [nterms+1];
      sa = new float [nterms+1];
      b = new float [nterms+1];
      beta = new float [nterms+1];
      deriv = new float [nterms+1];
      alfa = new float [nterms*(nterms+1)/2+1];
      arr = new double [nterms*(nterms+1)/2+1];

      // Fill

      //  a: Gaussian parameters
      a[2]=(float)pos;	                        // position
      for (i=pos-1; i > llim ;i--)              // (Loop left from the peak)
        if (array[i] < array[pos]/2) break;
      if (i > llim) 
        fwhm=(float)(2*(pos-i));                // FWHM
      else
      {
        for (i=pos+1; i < rlim ;i++)            // (Loop right from the peak)
          if (array[i] < array[pos]/2) break;
        fwhm= (float)(2*(i-pos));                        
      }
      a[3]=(float)(fwhm / 2.3548);              // 'width': = sigma = FWHM / 2.3548
      a[1]=(float)(array[pos]*2.50663*a[3]);    // area: = height * sqrt(2Pi) * sigma

      //  x
      x0=llim; dx=1;                            // data points - x: begin point & spacing
      
      // Routine
      fixed (int* y = &array[llim])             // data points - y 
      {
        int* yy = y-1;                          // '-1' due to 1-offset of the y-array in routine 'mrqmin': y = &array[llim]-1
        // Routine
        cSpevRout_ad se = new cSpevRout_ad ();
        se.marqfit(out ierr, out chisqr, out flamda, cridif, maxiter, x0, dx, yy, npts, 
          a, sa, nb, np, b, beta, deriv, alfa, arr);
      }

      // Eval.
      if (-1 != ierr)
      {
        // OK:
        // Channel assignement: ug/og = pos -/+ 3*sigma
        threesig = 3*a[3];
        il2 = (int)(pos - threesig);
        ir2 = (int)(pos + threesig);
      }
      else
      {
        // error:
        // Ensure, that noise level values will be used in Peak area determination in case of failure
        il2=0;
        ir2 = int.MaxValue;
      }

      // ready
      res = (-1 != ierr) ? (byte) 1 : (byte) 0;
      return res;
    }

    #endregion peak area detection methods
  
    #region smoothing methods

    /// <summary>
    /// Savitzky-Golay polynomial smoothing.
    /// </summary>
    /// <param name="y">Orig. spectrum</param>
    /// <param name="s">Smoothed spectrum in the region ['ich1','ich2']</param>
    /// <param name="nchan">Length of the spectrum (= number of channels in the spectrum)</param>
    /// <param name="ich1">Begin channel of the smoothing region</param>
    /// <param name="ich2">End channel of the smoothing region</param>
    /// <param name="iwid">Width of the smoothing filter: (2m+1), < 42</param>
    /// <remarks>
    /// Source: Kap. 4: "Spectrum evaluation", v. Espen, Janssens
    /// </remarks>
    void sgsmth(int[] y, ref int[] s, int nchan, int ich1, int ich2, int iwid)
    {
      // 1.
      // NOTE:
      //  This is the common case of the SG smoothing routine, depending on the filter width 'iwid'.
      //  Wrap around is not included.

      int i, j;
      double f;
      
      // Calculate filter coefficients
      int iw = Math.Min(iwid, 41);
      int m = iw/2;
      int sum = (2*m-1)*(2*m+1)*(2*m+3);
      float[] c = new float [41];
      // orig.:   c[j]=3*(3*m*m+3*m-1-5*j*j);           j in [-m,m]
      // trans.:  c[j]=3*(3*m*m+3*m-1-5*(j-m)*(j-m));   j in [0,2*m]
      for (j=0; j <= 2*m ;j++)
        c[j] = 3*(3*m*m+3*m-1-5*(j-m)*(j-m));
      // Convolute spectrum with filter
      int jch1 = Math.Max(ich1, m);
      int jch2 = Math.Min(ich2, nchan-1-m);
      for (i=jch1; i <= jch2 ; i++) 
      {
        // orig.:   f += c[j]*y[i+j];     j in [-m,m]
        // trans.:  f += c[j]*y[i+(j-m)]; j in [0,2*m] 
        for (f=0, j=0; j <= 2*m ;j++)
          f += c[j]*y[i+(j-m)];
        f /= sum;
        s[i] = (int) Math.Round (f);
      }
    }

    /// <summary>
    /// Savitzky-Golay polynomial smoothing.
    /// </summary>
    /// <param name="y">Orig. spectrum</param>
    /// <param name="s">Smoothed spectrum in the region ['ich1','ich2']</param>
    /// <param name="nchan">Length of the spectrum (= number of channels in the spectrum)</param>
    /// <param name="ich1">Begin channel of the smoothing region</param>
    /// <param name="ich2">End channel of the smoothing region</param>
    void sgsmth(int[] y, ref int[] s, int nchan, int ich1, int ich2)
    {
      // 2.
      // NOTE:
      //  This is a special case of the SG smoothing routine, which takes NOT into account
      //  the filter width parameter 'iwid'. Rather it works with a given filter width of 41
      //  and the corr'ing precalculated SG-coeff-array 'g_sgsm_41'.
      //  Wrap around is not included.

      int i, j;
      double f;
      
      // Convolute spectrum with filter
      int m = 20;                                       // filter half width: = 41/2
      int jch1 = Math.Max(ich1, m);
      int jch2 = Math.Min(ich2, nchan-1-m);
      for (i=jch1; i <= jch2 ; i++) 
      {
        for (f=0, j=0; j <= 2*m ;j++)
          f += g_sgsm_41[j]*y[i+(j-m)];
        s[i] = (int) Math.Round (f);
      }
    }

    /// <summary>
    /// Savitzky-Golay polynomial smoothing.
    /// </summary>
    /// <param name="y">Orig. spectrum</param>
    /// <param name="s">Smoothed spectrum in the region ['ich1','ich2']</param>
    /// <param name="nchan">Length of the spectrum (= number of channels in the spectrum)</param>
    /// <param name="nData"># of points to be smoothed</param>
    public void sgsmth(int[] y, ref int[] s, int nchan, int nData)
    {
      // 3.
      // NOTE:
      //  This is a special case of the SG smoothing routine, which takes NOT into account
      //  the filter width parameter 'iwid'. Rather it works with a given filter width of 41
      //  and the corr'ing precalculated SG-coeff-array 'g_sgsm_41'.
      //  Wrap around is included.
      
      // 3a.
      // Detailed code.

//      int i, j, fw, k;
//      double f;
//
//      // Init's
//      int m = 20;                                       // filter half width: = 41/2 (corr'ing to the filter array 'g_sgsm_41()')
//      // Convolute spectrum with filter
//      for (i=0; i < nData ; i++) 
//      {
//        for (f=0, j=0; j <= 2*m ;j++)
//        {
//          if (i+j-m < 0)	fw=y[((k=(i+j-m)%nchan) != 0) ? k+nchan : k];	    // cyclic mapping "from down"
//          else		        fw=y[(i+j-m)%nchan];		                          // cyclic mapping "from top"
//          f += g_sgsm_41[j]*fw;
//        }
//        s[i] = (int) Math.Round (f);
//      }    
      
      // 3b.
      // Shortened code.

      int i;
      double f;
  
      // Convolute spectrum with filter
      for (i=0; i < nData ; i++) 
      {
        f = sg_FIR_zyk_p_position_2_I (y, nchan, g_sgsm_41, 20, i); // Smooth at current pos. 
        s[i] = (f >= 0) ? (int)(f+0.5) : (int)(f-0.5);              // Round & write smoothed spectrum
      }
    }


    /// <summary>
    /// Smoothes a spectrum array (Wrapping included). The results are deposed in a smoothed spectrum array. 
    /// Method: SG (Savitzky/Golay) smoothing filter, filter width in [5,41].
    /// </summary>
    /// <param name="y">The signal (in)</param>
    /// <param name="s">The smoothed signal (out)</param>
    /// <param name="nchan">The size of the signal</param>
    /// <param name="nData">The # of signal points, that should be included in the smoothing procedure</param>
    /// <param name="width">The width of the smoothing filter</param>
    public void sg_smooth(int[] y, ref int[] s, int nchan, int nData, int width)
    {
      // 4.
      // NOTE:
      //  This is the common case of the SG smoothing routine, depending on the filter width 'width'.
      //  Wrap around is included.

      // 4a.
      // Detailed code
  
//      int m, i, j, k, fw;
//      double f;
//        
//      // Init's
//      m = width/2;                                                      // filter half width: = width/2 (corr'ing to the filter array 'g_sgsm()')
//      // Convolute spectrum with filter
//      for (i=0; i < nData ; i++) 
//      {
//          
//        for (f=0, j=0; j <= 2*m ;j++)
//        {
//          k = (i+j-m)%nchan; 
//          if (i+j-m < 0)	fw=y[(k != 0) ? k+nchan : k];                 // cyclic mapping "from down"
//          else		        fw=y[k];		                                  // cyclic mapping "from top"
//          f += g_sgsm[j]*fw;
//        }
//        s[i] = (f >= 0) ? (int)(f+0.5) : (int)(f-0.5);                  // Rounding
//      }

      // 4b.
      // Shortened code

      int m, i;
      double f;
  
      // Init's
      m = width/2;                                                      // filter half width: = width/2 (corr'ing to the filter array 'g_sgsm()')
      // Convolute spectrum with filter
      for (i=0; i < nData ; i++) 
      {
        f = sg_FIR_zyk_p_position_2_I (y, nchan, g_sgsm, (byte) m, i);  // Smooth at current pos. 
        s[i] = (f >= 0) ? (int)(f+0.5) : (int)(f-0.5);                  // Rounding
      }
    }

    /// <summary>
    /// Smoothing:
    /// Calculates the SG (Savitzky/Golay) coeff's for a given filter (in [5,7,...,41]).
    /// </summary>
    /// <param name="width">The width of the smoothing filter</param>
    /// <remarks>
    /// Affected globals:  - g_sgsm[]
    /// </remarks>
    public void sg_smooth_CalcSGCoeff (int width)
    {
      int m, j;
      double sum;
  
      // The filter half width
      m=width/2;
      // Calculate the array
      // orig.:   g_sgsm[j]=3*(3*m*m+3*m-1-5*j*j);           j in [-m,m]
      // trans.:  g_sgsm[j]=3*(3*m*m+3*m-1-5*(j-m)*(j-m));   j in [0,2*m]
      for (j=0; j <= 2*m ;j++)
        g_sgsm[j] = 3*(3*m*m+3*m-1-5*(j-m)*(j-m));
      sum = (2*m-1)*(2*m+1)*(2*m+3);
      for (j=0; j <= 2*m ;j++)
        g_sgsm[j] /= sum;
    }


    /// <summary>
    /// Smoothing: Movering average
    /// </summary>
    /// <param name="y">The signal (in)</param>
    /// <param name="s">The smoothed signal (out)</param>
    /// <param name="nchan">The size of the signal</param>
    /// <param name="nData">The # of signal points, that should be included in the smoothing procedure</param>
    /// <param name="width">The width of the smoothing filter</param>
    public void ma_smooth(int[] y, ref int[] s, int nchan, int nData, int width)
    {
      // NOTE:
      //  This is the Movering average smoothing as described in "Spectrum evaluation", v. Espen, Janssens,
      //  4.III.B.1.
      //  Wrap around is NOT included.
      int m, i, j, ave, anz, lastchan;
   
      // Init's
      m = width/2;                                  // filter half width
      lastchan = Math.Min (nData-1, nchan-1);       // last channel included in the smoothing procedure
      // MA smoothing
      for (i=0; i < nData; i++)
      {
        for (ave=0, anz=0, j=i-m; j <= i+m; j++)
        {
          if (j >= 0 && j <= lastchan) { ave += y[j]; anz++; }  
        }
        ave /= anz;
        s[i] = ave;  
      }
    }
    
    #endregion smoothing methods

    #region properties

    /// <summary>
    /// The number of peaks found
    /// </summary>
    public int PeakNo
    {
      get { return g_nPeaks; }
    }

    /// <summary>
    /// The Peak position (index) array (unit: [pts])
    /// </summary>
    public int[] PeakPositions
    {
      get
      {
        int[] ar = new int[g_nPeaks];
        if (g_nPeaks > 0)
          Array.Copy (g_ariPeakPos, 0, ar, 0, g_nPeaks);
        return ar;
      }
    }
 
    /// <summary>
    /// The Peak invalid array (unit: none)
    /// </summary>
    public byte[] PeakInvalid
    {
      get
      {
        byte[] ar = new byte[g_nPeaks];
        if (g_nPeaks > 0)
          Array.Copy (g_arbyPeakInvalid, 0, ar, 0, g_nPeaks);
        return ar;
      }
    }
    
    /// <summary>
    /// The  Peak area array (unit: [adc*pts])
    /// </summary>
    public float[] PeakAreas
    {
      get
      {
        float[] ar = new float[g_nPeaks];
        if (g_nPeaks > 0)
          Array.Copy (g_arfPeakArea, 0, ar, 0, g_nPeaks);
        return ar;
      }
    }

    #endregion properties

  }

}
