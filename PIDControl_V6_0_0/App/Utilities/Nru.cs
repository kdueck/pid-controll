using System;

namespace Utilities
{
  /// <summary>
	/// Class NRU:
	/// Numerical recipes in C
	/// 
  /// Dies sind die Recipes-Routinen aus
  ///		"Numerical Recipes in C", W.H.Press a.o., Cambridge Univ. Press, 1992,
  ///		ISBN 0-521-43108-5
  /// </summary>
	public class cNRU
	{
		
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public cNRU()
		{
		}

    #endregion constructors
  
    #region utilities
    
    /// <summary>
    /// Square calculation
    /// </summary>
    float SQR(float a) 
    { 
      return (a == 0.0f)? 0.0f : a*a; 
    }
    /// <summary>
    /// Sign calculation
    /// </summary>
    float SIGN (float a, float b)
    {
      return (b >= 0.0 ? (a >= 0.0 ? (float)a : (float)(-a)) : (a <= 0.0 ? (float)a : (float)(-a)));
    }

    /// <summary>
    /// Swapping
    /// </summary>
    void SWAP (ref float a, ref float b)
    {
      float swap=a; a = b; b = swap;
    }

    #endregion utilities
    
    #region Par2.1: Gauss-Jordan elimination
    
    /// <summary>
    /// Chapter 2: Solution of linear algebraic equations
    /// 2.1: Gauss-Jordan elimination
    /// Linear equation solution by Gauss-Jordan elimination. a[i,n][1,n] is the input matrix.
    /// b[1,n][1,m] is input containing the m right-hand side vectors. On output, a is replaced
    /// by its matrix inverse, and b is replaced by the corresponding set of solution vectors.
    /// </summary>
    /// <returns>1 - OK, 0 - error</returns>
    byte gaussj(float[,] a, int n, float[,] b, int m, out string psErr)
    {
      int[] indxc,indxr,ipiv;
      int i,icol=0,irow=0,j,k,l,ll;
      float big,dum,pivinv;

      indxc=new int[n+1];
      indxr=new int[n+1];
      ipiv=new int[n+1];
      for (j=1;j<=n;j++) ipiv[j]=0;
      for (i=1;i<=n;i++) 
      {
        big=0.0f;
        for (j=1;j<=n;j++)
          if (ipiv[j] != 1)
            for (k=1;k<=n;k++) 
            {
              if (ipiv[k] == 0) 
              {
                if (Math.Abs(a[j,k]) >= big) 
                {
                  big=Math.Abs(a[j,k]);
                  irow=j;
                  icol=k;
                }
              } 
              else if (ipiv[k] > 1) 
              {
                psErr="Routine 'gaussj': Singular Matrix-1.";
                return 0;
              }
            }
        ++(ipiv[icol]);
        if (irow != icol) 
        {
          for (l=1;l<=n;l++) SWAP(ref a[irow,l], ref a[icol,l]);
          for (l=1;l<=m;l++) SWAP(ref b[irow,l], ref b[icol,l]);
        }
        indxr[i]=irow;
        indxc[i]=icol;
        if (a[icol,icol] == 0.0) 
        {
          psErr="Routine 'gaussj': Singular Matrix-2.";
          return 0;
        }
        pivinv=1.0f/a[icol,icol];
        a[icol,icol]=1.0f;
        for (l=1;l<=n;l++) a[icol,l] *= pivinv;
        for (l=1;l<=m;l++) b[icol,l] *= pivinv;
        for (ll=1;ll<=n;ll++)
          if (ll != icol) 
          {
            dum=a[ll,icol];
            a[ll,icol]=0.0f;
            for (l=1;l<=n;l++) a[ll,l] -= a[icol,l]*dum;
            for (l=1;l<=m;l++) b[ll,l] -= b[icol,l]*dum;
          }
      }
      for (l=n;l>=1;l--) 
      {
        if (indxr[l] != indxc[l])
          for (k=1;k<=n;k++)
            SWAP(ref a[k,indxr[l]], ref a[k,indxc[l]]);
      }
      //ready
      psErr="";
      return 1;
    }

    #endregion Par2.1: Gauss-Jordan elimination
    
    #region Par2.6: Singular value decomposition

    /// <summary>
    /// Chapter 2: Solution of linear algebraic equations
    /// 2.6: Singular value decomposition (SVD)
    /// Solves A*X=B for a vector X, where A is specified by the arrays u[1,m][1,n],
    /// w[1,n], v[1,n][1,n] as returned by svdcmp. m and n are the dimensions of a, and will be 
    /// equal for square matrices. b[1,m] is the input right-hand side. x[1,n] is the output
    /// solution vector. No input quantities are destroyed, so the routine may be called 
    /// sequentially with different b's.
    public void svbksb(float[,] u, float[] w, float[,] v, int m, int n, float[] b, float[] x)
    {
      int jj,j,i;
      float s;
      float[] tmp;

      tmp=new float[n+1];
      for (j=1;j<=n;j++)                          // Calculate U^T*B
      {
        s=0.0f;
        if (w[j] != 0)                            // Nonzero result only if w(j) is non-zero.
        {
          for (i=1;i<=m;i++) s += u[i,j]*b[i];
          s /= w[j];                              // This is the divide by w(j).
        }
        tmp[j]=s;
      }
      for (j=1;j<=n;j++)                          // Matrix multiply by V to get answer.
      {
        s=0.0f;
        for (jj=1;jj<=n;jj++) s += v[j,jj]*tmp[jj];
        x[j]=s;
      }
    }

    /// <summary>
    /// Chapter 2: Solution of linear algebraic equations
    /// 2.6: Singular value decomposition
    /// Given a matrix a[1,m][1,n], this routine computes its singular value decomposition,
    /// A=U*W*V(T). The matrix U replaces a on output. The diagonal matrix of singular
    /// values W is output as a vector w[1,n]. The matrix V (not the transpose V(T)) is
    /// Output as v[1,n][1,n].
    /// </summary>
    /// <returns>1 - OK, 0 - error</returns>
    public byte svdcmp(float[,] a, int m, int n, float[] w, float[,] v, ref string psErr)
    {
      int flag,i,its,j,jj,k,l,nm;
      float anorm,c,f,g,h,s,scale,x,y,z;
      float[] rv1;
      
      // JM stuff
      psErr="";
      l=nm=0;
      // NRU stuff
      rv1=new float[n+1];
      g=scale=anorm=0.0f;                         // Householder reduction to bidiagonal form.
      for (i=1;i<=n;i++) 
      {
        l=i+1;
        rv1[i]=scale*g;
        g=s=scale=0.0f;
        if (i <= m) 
        {
          for (k=i;k<=m;k++) scale += (float)Math.Abs(a[k,i]);
          if (scale != 0) 
          {
            for (k=i;k<=m;k++) 
            {
              a[k,i] /= scale;
              s += a[k,i]*a[k,i];
            }
            f=a[i,i];
            g = -SIGN((float)Math.Sqrt(s),f);
            h=f*g-s;
            a[i,i]=f-g;
            for (j=l;j<=n;j++) 
            {
              for (s=0.0f,k=i;k<=m;k++) s += a[k,i]*a[k,j];
              f=s/h;
              for (k=i;k<=m;k++) a[k,j] += f*a[k,i];
            }
            for (k=i;k<=m;k++) a[k,i] *= scale;
          }
        }
        w[i]=scale *g;
        g=s=scale=0.0f;
        if (i <= m && i != n) 
        {
          for (k=l;k<=n;k++) scale += (float)Math.Abs(a[i,k]);
          if (scale != 0) 
          {
            for (k=l;k<=n;k++) 
            {
              a[i,k] /= scale;
              s += a[i,k]*a[i,k];
            }
            f=a[i,l];
            g = -SIGN((float)Math.Sqrt(s),f);
            h=f*g-s;
            a[i,l]=f-g;
            for (k=l;k<=n;k++) rv1[k]=a[i,k]/h;
            for (j=l;j<=m;j++) 
            {
              for (s=0.0f,k=l;k<=n;k++) s += a[j,k]*a[i,k];
              for (k=l;k<=n;k++) a[j,k] += s*rv1[k];
            }
            for (k=l;k<=n;k++) a[i,k] *= scale;
          }
        }
        anorm=Math.Max(anorm,(Math.Abs(w[i])+Math.Abs(rv1[i])));
      }
      for (i=n;i>=1;i--)                          // Accumulation of right-hand transformations.
      {
        if (i < n) 
        {
          if (g != 0) 
          {
            for (j=l;j<=n;j++)                    // Double division to avoid possible underflow
              v[j,i]=(a[i,j]/a[i,l])/g;
            for (j=l;j<=n;j++) 
            {
              for (s=0.0f,k=l;k<=n;k++) s += a[i,k]*v[k,j];
              for (k=l;k<=n;k++) v[k,j] += s*v[k,i];
            }
          }
          for (j=l;j<=n;j++) v[i,j]=v[j,i]=0.0f;
        }
        v[i,i]=1.0f;
        g=rv1[i];
        l=i;
      }
      for (i=Math.Min(m,n);i>=1;i--)              // Accumulation of left-hand transformations.
      {
        l=i+1;
        g=w[i];
        for (j=l;j<=n;j++) a[i,j]=0.0f;
        if (g != 0) 
        {
          g=1.0f/g;
          for (j=l;j<=n;j++) 
          {
            for (s=0.0f,k=l;k<=m;k++) s += a[k,i]*a[k,j];
            f=(s/a[i,i])*g;
            for (k=i;k<=m;k++) a[k,j] += f*a[k,i];
          }
          for (j=i;j<=m;j++) a[j,i] *= g;
        } 
        else for (j=i;j<=m;j++) a[j,i]=0.0f;
        ++a[i,i];
      }
      for (k=n;k>=1;k--)                          // Diagonalization of the bidiagonal form: loop over 
      {                                           // singular values, and over allowed iterations.
        for (its=1;its<=30;its++) 
        {
          flag=1;
          for (l=k;l>=1;l--)                      // Test for splitting.
          {
            nm=l-1;                               // Note, that rv1[1] is always zero.
            if ((float)(Math.Abs(rv1[l])+anorm) == anorm) 
            {
              flag=0;
              break;
            }
            if ((float)(Math.Abs(w[nm])+anorm) == anorm) break;
          }
          if (flag != 0) 
          {
            c=0.0f;                               // Cancellation of rv1[l], if l > 1
            s=1.0f;
            for (i=l;i<=k;i++) 
            {
              f=s*rv1[i];
              rv1[i]=c*rv1[i];
              if ((float)(Math.Abs(f)+anorm) == anorm) break;
              g=w[i];
              h=pythag(f,g);
              w[i]=h;
              h=1.0f/h;
              c=g*h;
              s = -f*h;
              for (j=1;j<=m;j++) 
              {
                y=a[j,nm];
                z=a[j,i];
                a[j,nm]=y*c+z*s;
                a[j,i]=z*c-y*s;
              }
            }
          }
          z=w[k];
          if (l == k) 
          {                                       // Convergence.
            if (z < 0.0)                          // Singular value is made non-negative.
            {
              w[k] = -z;
              for (j=1;j<=n;j++) v[j,k] = -v[j,k];
            }
            break;
          }
          if (its == 30) 
          {
            psErr = "Routine 'svdcmp': No convergence in 30 iterations.";
            return 0;
          }
          x=w[l];                                 // Shift from bottom 2-by-2 minor.
          nm=k-1;
          y=w[nm];
          g=rv1[nm];
          h=rv1[k];
          f=((y-z)*(y+z)+(g-h)*(g+h))/(2.0f*h*y);
          g=pythag(f,1.0f);
          f=((x-z)*(x+z)+h*((y/(f+SIGN(g,f)))-h))/x;
          c=s=1.0f;                               // Next QR transformation.
          for (j=l;j<=nm;j++) 
          {
            i=j+1;
            g=rv1[i];
            y=w[i];
            h=s*g;
            g=c*g;
            z=pythag(f,h);
            rv1[j]=z;
            c=f/z;
            s=h/z;
            f=x*c+g*s;
            g = g*c-x*s;
            h=y*s;
            y *= c;
            for (jj=1;jj<=n;jj++) 
            {
              x=v[jj,j];
              z=v[jj,i];
              v[jj,j]=x*c+z*s;
              v[jj,i]=z*c-x*s;
            }
            z=pythag(f,h);
            w[j]=z;                               // Rotation can be arbitrary if z = 0.
            if (z != 0) 
            {
              z=1.0f/z;
              c=f*z;
              s=h*z;
            }
            f=c*g+s*y;
            x=c*y-s*g;
            for (jj=1;jj<=m;jj++) 
            {
              y=a[jj,j];
              z=a[jj,i];
              a[jj,j]=y*c+z*s;
              a[jj,i]=z*c-y*s;
            }
          }
          rv1[l]=0.0f;
          rv1[k]=f;
          w[k]=x;
        }
      }
      //ready
      return 1;
    }

    /// <summary>
    /// Chapter 2: Solution of linear algebraic equations
    /// 2.6: Singular value decomposition
    /// Computes sqrt(a�+b�) without destructive underflow or overflow.
    /// </summary>
    float pythag(float a, float b)
    {
      float absa,absb;

      absa=Math.Abs (a);
      absb=Math.Abs (b);
      if (absa > absb) 
        return (float)(absa*Math.Sqrt(1.0+SQR(absb/absa)));
      else 
        return (absb == 0.0f ? 0.0f : (float)(absb*Math.Sqrt(1.0+SQR(absa/absb))));
    }

    #endregion Par2.6: Singular value decomposition

    #region Par15.4: General linear least squares
    
    #region delegates

    /// <summary>
    /// Delegate for user-supplied fitting function for General linear least squares
    /// </summary>
    public delegate void GenLinLeastSquareFuncDelegate(float x, float[] p, int np);

    #endregion delegates
    
    /// <summary>
    /// Chapter 15: Modeling of data
    /// 15.4: General linear least squares
    /// Given a set of data points x[1,ndat], y[1,ndat] with individual standard deviations
    /// sig[1,ndat], use chi-square-minimization to determine the coeff's
    /// a[1,ma] of the fitting function y=Sum(i) a[i]*afunc(i)[x].
    /// Here we solve the fitting equations using singular value decomposition of the 
    /// ndata by ma matrix, as in �2.6. Arrays u[1,ndata][1,ma], v[1,ma][1,ma], and
    /// w[1,ma] provide workspace on in input; on output they define the singular value 
    /// decomposition, and can be uset to obtain the covariance matrix. 
    /// The program returns values for the ma fit parameters a and chi-square.
    /// The user supplies a routine funcs(x,afunc,ma) that returns the ma basis functions
    /// evaluated at x=x in the array afunc[1,ma].
    /// </summary>
    /// <returns>1 - OK, 0 - error</returns>
    public byte svdfit(float[] x, float[] y, float[] sig, int ndata, float[] a, int ma,
			                float[,] u, float[,] v, float[] w, out float chisq,
			                GenLinLeastSquareFuncDelegate funcs, ref string psErr)
    {	
      int j,i;
	    float wmax,tmp,thresh,sum;
      float[] b, afunc;

      // Init
	    b=new float [ndata+1];
	    afunc=new float [ma+1];
      chisq=0;
	    // Accumulate coeff's of the fitting matrix
      for (i=1;i<=ndata;i++) {
		    funcs(x[i],afunc,ma);
		    tmp=1.0f/sig[i];
		    for (j=1;j<=ma;j++) u[i,j]=afunc[j]*tmp;
		    b[i]=y[i]*tmp;
	    }
      // SVD
	    if (0 == svdcmp(u,ndata,ma,w,v,ref psErr)) {
		    return 0;
	    }
      // Edit the singular values
	    wmax=0.0f;
	    for (j=1;j<=ma;j++)
		    if (w[j] > wmax) wmax=w[j];
	    thresh=1e-5f*wmax;
	    for (j=1;j<=ma;j++)
		    if (w[j] < thresh) w[j]=0.0f;
	    // SVD - BS
      svbksb(u,w,v,ndata,ma,b,a);
	    // Evaluate chi-square
	    for (i=1;i<=ndata;i++) {
		    funcs(x[i],afunc,ma);
		    for (sum=0.0f,j=1;j<=ma;j++) sum += a[j]*afunc[j];
		    tmp=(y[i]-sum)/sig[i];
        chisq += tmp*tmp;
	    }
	    //ready
	    return 1;
    }
    
    /// <summary>
    /// Chapter 15: Modeling of data
    /// 15.4: General linear least squares
    /// Fitting routine for a polynomial of degree np-1, with coefficients in the array
    /// p[1,np].
    /// </summary>
    public void fpoly(float x, float[] p, int np)
    {
      int j;

      p[1]=1.0f;
      for (j=2;j<=np;j++) p[j]=p[j-1]*x;
    }
    
    #endregion Par15.4: General linear least squares
    
    #region Par15.5: Nonlinear models

    #region members

    // statics for: 'mrqmin'
    int mfit;
    float ochisq;
    float[] atry, beta, da;
    float[,] oneda;

    #endregion members
    
    #region delegates

    /// <summary>
    /// Delegate for user-supplied fitting function for Levenberg-Marquardt method
    /// </summary>
    public delegate void NonLinFuncDelegate(float x, float[] a, out float y, float[] dyda, int na);

    #endregion delegates

    #region methods

    /// <summary>
    /// Chapter 15: Modeling of data
    /// 15.4: General linear least squares
    /// Expand in storage the covariance matrix covar, so as to take into account parameters
    /// that are being held fixed. (For the latter, return zero covariances.)
    /// </summary>
    void covsrt(float[,] covar, int ma, int[] ia, int mfit)
    {
      int i,j,k;

      for (i=mfit+1;i<=ma;i++)
        for (j=1;j<=i;j++) covar[i,j]=covar[j,i]=0.0f;
      k=mfit;
      for (j=ma;j>=1;j--) 
      {
        if (ia[j] != 0) 
        {
          for (i=1;i<=ma;i++) SWAP(ref covar[i,k], ref covar[i,j]);
          for (i=1;i<=ma;i++) SWAP(ref covar[k,i], ref covar[j,i]);
          k--;
        }
      }
    }
    
    /// <summary>
    /// Envelope routine for Levenberg-Marquardt method 'mrqmin'
    /// Additional parameters:
    ///  - cridif: critical chisq-delta for stopping the routine
    ///  - maxiter: max. # of iterations permitted   
    /// (Added by JM)
    /// </summary>
    /// <returns>1 - OK, 0 - error</returns>
    public byte mrqmin_env(float[] x, float[] y, float[] sig, int ndata, float[] a, int[] ia,
      int ma, float[,] covar, float[,] alpha, out float chisq,
      NonLinFuncDelegate funcs, 
      out float alamda, int maxiter, float cridif, out string psErr)
    {
      int niter;
      float ochisq, perdif=0;

      // Init
      chisq=0;                              // will be adjusted within the 'mrqmin' routine
      // Init. step
      alamda = -1.0f;	                         
      if (0 == mrqmin(x,y,sig,ndata,a,ia,ma,covar,alpha,ref chisq,funcs,ref alamda,out psErr)) 
        return 0;
      // Iteration loop
      niter=0;	                             
      do 
      {
        niter++;
        ochisq = chisq;
        if (0 == mrqmin(x,y,sig,ndata,a,ia,ma,covar,alpha,ref chisq,funcs,ref alamda,out psErr)) 
          return 0;
        // Evaluate chisqr for convergence estimation
        if (chisq > ochisq)
          continue;   
        perdif = 100*(ochisq-chisq)/chisq;
      }
      while (niter < maxiter && perdif > cridif);
      // Finishing step: Calc. covariance and curvature matrix 
      alamda=0.0f;
      if (0 == mrqmin(x,y,sig,ndata,a,ia,ma,covar,alpha,ref chisq,funcs,ref alamda,out psErr)) 
        return 0;
      // ready
      return 1;
    }

    /// <summary>
    /// Chapter 15: Modeling of data
    /// 15.5: Nonlinear models
    /// Levenberg-Marquardt method,attempting to reduce the value chisq of a fit between a set of data
    /// points x[1,ndata], y[1,ndata] with individual standard deviations sig[1,ndata].,
    /// and a nonlinear function dependent on ma coefficients a[1,ma]. The input array ia[1,ma]
    /// indicates by nonzero entries those components of a that should be fitted for, and by zero
    /// entries those components that should be held fixed at their input values. The program re-
    /// turns current best fit values for the parameters a[1,ma], and chisq. The arrays
    /// covar[1,ma][1,ma], alpha[1,ma][1,ma] are used as working space during most
    /// iterations. Supply a routine funcs(x,a,yfit,dyda,ma) that evaluates the fitting function
    /// yfit, and its derivatives dyda[1,ma] with respect to the fitting parameters a at x. On
    /// the first call provide an initial guess for the parameters a, and set alamda less than 0 for initialisation
    /// (which then sets alamda=0.001). If a step succeeds chisq becomes smaller and alamda de-
    /// creases by a factor of 10. If a step failes alamda grows by a factor of 10. You must call this
    /// routine repeatedly until convergence is achieved. Then, make one final call with almda=0, so
    /// that covar[1,ma][1,ma] returns the covariance matrix, and alpha the curvature matrix.
    /// (Parameters held fixed will return zero covariances.)
    /// </summary>
    /// <returns>1 - OK, 0 - error</returns>
    byte mrqmin(float[] x, float[] y, float[] sig, int ndata, float[] a, int[] ia,
			                int ma, float[,] covar, float[,] alpha, ref float chisq,
			                NonLinFuncDelegate funcs, 
                      ref float alamda, out string psErr)
    {	
      int j,k,l,m;

      //Initialisation.
	    if (alamda < 0) {	
		    atry=new float [ma+1];
		    beta=new float [ma+1];
		    da=new float [ma+1];
		    for (mfit=0,j=1;j<=ma;j++)
			    if (ia[j] != 0) mfit++;
		    oneda=new float[mfit+1,2];
		    alamda=0.001f;
		    mrqcof(x,y,sig,ndata,a,ia,ma,alpha,beta,out chisq,funcs);
		    ochisq=chisq;
		    for (j=1;j<=ma;j++) atry[j]=a[j];
	    }
      //Alter linearized fitting matrix, by augmenting diagonal elements.
	    for (j=0,l=1;l<=ma;l++) {	
		    if (ia[l] != 0) {				
			    for (j++,k=0,m=1;m<=ma;m++) {
				    if (ia[m] != 0) {
					    k++;
					    covar[j,k]=alpha[j,k];
				    }
			    }
			    covar[j,j]=alpha[j,j]*(1.0f+alamda);
			    oneda[j,1]=beta[j];
		    }
	    }
      //Matrix solution.
	    if (0 == gaussj(covar,mfit,oneda,1,out psErr)) {	
		    return 0;
	    }
	    for (j=1;j<=mfit;j++) da[j]=oneda[j,1];
      //Once converged, evaluate covariance matrix.	    
      if (alamda == 0) {	
		    covsrt(covar,ma,ia,mfit);
		    return 1;
	    }
      //Did the trial succeed?
	    for (j=0,l=1;l<=ma;l++)	
		    if (ia[l] != 0) atry[l]=a[l]+da[++j];
	    mrqcof(x,y,sig,ndata,atry,ia,ma,covar,da,out chisq,funcs);
	    if (chisq < ochisq) {	
        //Success, accept the new solution.		  
        alamda *= 0.1f;
		    ochisq=chisq;
		    for (j=0,l=1;l<=ma;l++) {
			    if (ia[l] != 0) {
				    for (j++,k=0,m=1;m<=ma;m++) {
					    if (ia[m] != 0) {
						    k++;
						    alpha[j,k]=covar[j,k];
					    }
				    }
				    beta[j]=da[j];
				    a[l]=atry[l];
			    }
		    }
	    } 
      else {				
        //Failure, increase alamda and return.
		    alamda *= 10.0f;
		    chisq=ochisq;
	    }
	    //ready
	    return 1;
    }

    /// <summary>
    /// Chapter 15: Modeling of data
    /// 15.5: Nonlinear models
    /// Used by mrqmin to evaluate the linearized fitting matrix alpha, and vector beta as in (15.5.8),
    /// and calculate chisq.
    /// </summary>
    void mrqcof(float[] x, float[] y, float[] sig, int ndata, float[] a, int[] ia,
			    int ma, float[,] alpha, float[] beta, out float chisq, NonLinFuncDelegate funcs)
    {	
      int i,j,k,l,m,mfit=0;
	    float ymod,wt,sig2i,dy;
      float[] dyda;

	    dyda=new float [ma+1];
	    for (j=1;j<=ma;j++)
		    if (ia[j] != 0) mfit++;
	    //Initialize (symmetric) alpha, beta.
      for (j=1;j<=mfit;j++) {		
		    for (k=1;k<=j;k++) alpha[j,k]=0.0f;
		    beta[j]=0.0f;
	    }
	    chisq=0.0f;
      //Summation loop over all data.
	    for (i=1;i<=ndata;i++) {	
		    funcs(x[i],a,out ymod,dyda,ma);
		    sig2i=1.0f/(sig[i]*sig[i]);
		    dy=y[i]-ymod;
		    for (j=0,l=1;l<=ma;l++) {
			    if (ia[l] != 0) {
				    wt=dyda[l]*sig2i;
				    for (j++,k=0,m=1;m<=l;m++)
					    if (ia[m] != 0) alpha[j,++k] += wt*dyda[m];
				    beta[j] += dy*wt;
			    }
		    }
        //And find chisq.
		    chisq += dy*dy*sig2i;	
	    }
      //Fill in the symmetric side.
	    for (j=2;j<=mfit;j++)		
		    for (k=1;k<j;k++) alpha[k,j]=alpha[j,k];
    }

    /// <summary>
    /// Chapter 15: Modeling of data
    /// 15.5: Nonlinear models
    /// y(x;a) is the sum of na/3 Gaussians (15.5.16). The amplitude, center, and width of the
    /// Gaussians are stored in consecutive locations of a: a[i]=B(k), a[i+1]=E(k), a[i+2]=G(k),
    /// k=1,...,na/3. The dimensions of the arrays are a[1,na], dyda[1,na].
    /// </summary>
    public void fgauss(float x, float[] a, out float y, float[] dyda, int na)
    {	
      int i;
	    float fac,ex,arg;

	    y=0.0f;
	    for (i=1;i<=na-1;i+=3) {
		    arg=(x-a[i+1])/a[i+2];
		    ex=(float)Math.Exp(-arg*arg);
		    fac=a[i]*ex*2.0f*arg;
		    y += a[i]*ex;
		    dyda[i]=ex;					      //partial deviation dy/dB
		    dyda[i+1]=fac/a[i+2];		  //dito dy/dE
		    dyda[i+2]=fac*arg/a[i+2];	//dito dy/dG
	    }
    }

    #endregion methods

    #endregion Par15.5: Nonlinear models

  }
}
