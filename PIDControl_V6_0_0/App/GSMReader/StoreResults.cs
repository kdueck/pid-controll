using System;
using System.Collections;
using System.IO;

namespace App
{
	/// <summary>
	/// Class StoreResults:
	/// GSMReader data storage for Eeprom storage (for IUT-built devices):
  /// Stored result list/collection
  /// </summary>
  public class CStoreResults : IEnumerable, ICollection
  {
    #region constants

    // File write: NL
    public const string NL = "\r\n";

    #endregion  // constants

    #region member
  
    /// <summary>
    /// The list
    /// </summary>
    private ArrayList _Items = new ArrayList();

    // The 'valitity array':
    // Indicates whether the 'MAX_SCRIPT_WINDOWS' substance windows contain valid stored results
    // ( element at corr'ing idx 'true' ) or not ( false )
    private bool[] _arbActive = new bool [Script.MAX_SCRIPT_WINDOWS];
  
    /// <summary>
    /// The list, which contains the different script indices that are present in a result list
    /// </summary>
    ArrayList _alScriptIndices = new ArrayList ();

    /// <summary>
    /// The list, which contains the header information regarding the scripts refered to in a result list
    /// </summary>
    ArrayList _alHeaderInfo = new ArrayList ();
    
    #endregion // member
  
    #region indexer
    
    /// <summary>
    /// The indexer
    /// </summary>
    public CStoreResultItem this[int index]
    {
      get { return (CStoreResultItem) _Items[index]; }
    }

    #endregion  //indexer
    
    #region IEnumerable Members

    /// <summary>
    /// The enumerator
    /// </summary>
    /// <returns>The enumerator</returns>
    public IEnumerator GetEnumerator ()
    {
      return _Items.GetEnumerator ();
    }

    #endregion // IEnumerable Members
    
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public CStoreResults()
    {
    }
	
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="res">The CStoreResults object to be initialized with</param>
    public CStoreResults ( CStoreResults res )
    {
      // Remove all objects from the list
      RemoveAll ();
      // Copy the CStoreResults object into the current instance
      Copy ( res );
    }
    
    #endregion  // constructors
    
    #region methods
  
    /// <summary>
    /// Builds a list, which contains the different script indices that are present in the result list.
    /// </summary>
    public void Analyze ()
    {
      _alScriptIndices.Clear ();
      foreach (CStoreResultItem item in this)
      {
        if (_alScriptIndices.Contains (item.byScriptIndex))
          continue;
        _alScriptIndices.Add (item.byScriptIndex);
      }
    }
    
    /// <summary>
    /// Builds the 'valitity array':
    /// Checks, whether the 'MAX_SCRIPT_WINDOWS' substance windows contain valid stored results
    /// ( array element at corr'ing idx is set to 'true' ) or not ( ... false ).
    /// </summary>
    /// <remarks>
    /// The matrix to be checked looks as follows:
    ///         win0 win1 ... win15
    /// ---------------------------
    /// item 0  -1   -1   ... -1
    /// item 1  20   40   ... -1
    /// .       ...           -1
    /// .       ...           -1
    /// .       ...           -1
    /// item N  -1   -1   ... -1
    /// ---------------------------
    /// array:  T    T    ... F
    /// </remarks>
    public void FindActive ()
    {
      // Initiate the 'valitity array'
      for ( int nWnd=0; nWnd < Script.MAX_SCRIPT_WINDOWS; nWnd++ )
        _arbActive[nWnd] = false;
      // Loop over the columns ( = 'MAX_SCRIPT_WINDOWS' substance windows )
      for ( int nCount=0; nCount < Script.MAX_SCRIPT_WINDOWS; nCount++ )
      {
        // Loop over the rows ( = 'Count' result collection items )
        for ( int nPos = 0; nPos < this.Count; nPos++ )
        {
          // Get the current result
          CStoreResultItem si = ElementAt(nPos);
          // Check: Valid stored result?
          if ( CStoreResultItem.invalid == si.arfResults[nCount] ) 
            continue; // No.
          // Yes: Update the 'valitity array' corr'ly
          _arbActive[nCount] = true;
          // Next column ...
          break;
        }
      }
    }  

    /// <summary>
    /// Writes the stored results to a file.
    /// </summary>
    /// <param name="sPath">The full path of the file</param>
    /// <returns>True, if all went OK; otherwise false</returns>
    public bool FileWrite ( string sPath )
    {
      StreamWriter file = null;
      try
      {
        System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;  
        System.Globalization.NumberFormatInfo lfi = System.Globalization.NumberFormatInfo.CurrentInfo;
        string sx;

        // Create the file
        file = File.CreateText ( sPath );

        // Write the Data  
        int byPrevScriptIdx = -1; 
        for ( int nItem=0; nItem < this.Count; nItem++ )
        {
          // The current result item
          CStoreResultItem item = ElementAt(nItem);
          
          // Header, if req.
          if (byPrevScriptIdx != item.byScriptIndex)
          {
            WriteHeaderInfo (file, item.byScriptIndex); 
            byPrevScriptIdx = item.byScriptIndex;
          }
          
          // Data row:
          //  #
          file.Write ( "{0}", item.dwPosition );
          
          //  DateTime
          file.Write ( "\t{0}", item.dtTime.ToString ( "G" ) );
          
          //  Script: empty
          file.Write ( "\t" );

          //  Substance results
          for ( int nWnd = 0; nWnd < Script.MAX_SCRIPT_WINDOWS; nWnd++ )
          {
            // Only consider valid stored results
            if ( !_arbActive[nWnd] ) continue;
        
            sx = string.Format ( "\t{0}", item.arfResults[nWnd].ToString ( "F3", nfi ) );
            sx = sx.Replace ( ".", lfi.NumberDecimalSeparator );
            file.Write ( sx );
          }

          // Terminating NL
          file.Write ( NL );
        }
      }
      catch
      {
        return false;
      }
      finally 
      {
        // Close the file
        if ( null != file ) 
          file.Close ();
      }

      return true;
    }

    /// <summary>
    /// Writes the header information to a file.
    /// </summary>
    /// <param name="file">The file</param>
    /// <param name="byScriptIndex">The script index</param>
    void WriteHeaderInfo (StreamWriter file, byte byScriptIndex)
    {
      string sx;

      // 1. Header line: Empty line
      file.Write ( NL );
      // 2. Header line: Column titles
      file.Write ( "Nr.\tDatum\tScript" );
      for ( int nWnd = 0; nWnd < Script.MAX_SCRIPT_WINDOWS; nWnd++ )
      {
        // Only consider valid stored results
        if ( !_arbActive[nWnd] ) continue;

        sx = string.Format ( "\tWnd {0}", nWnd );
        file.Write ( sx );
      }
      file.Write ( NL );
      // Preparation: 3. & 4. Header line: Script name, Substance window names,  Substance window conc. units 
      int idx = this._alScriptIndices.IndexOf ( byScriptIndex );
      string sHeaderInfo = this._alHeaderInfo[idx].ToString ();
      string[] ars = sHeaderInfo.Split (new char[] { ',' }); 
      // 3. Header line: Script name, Substance window names
      sx = "\t\t";                                // Empty: No, Date
      sx += string.Format ("{0}\t", ars[0]);      // Script name
      for (int i=1; i < ars.Length; i += 2)       // Substance window names
      {
        sx += string.Format ("{0}\t", ars[i]);
      }
      file.Write ( sx );
      file.Write ( NL );
      // 4. Header line: Substance window conc. units
      sx = "\t\t\t";                              // Empty: No, Date, Script name
      for (int i=2; i < ars.Length; i += 2)       // Substance window conc. units
      {
        sx += string.Format ("{0}\t", ars[i]);
      }
      file.Write ( sx );
      file.Write ( NL );
      // 5. Header line: Empty line
      file.Write ( NL );
    }

    #endregion // methods

    #region properties

    /// <summary>
    /// The list, which contains the different script indices that are present in a result list
    /// </summary>
    public ArrayList ScriptIndices
    {
      get { return this._alScriptIndices; }
    }

    /// <summary>
    /// The list, which contains the header information regarding the scripts refered to in a result list
    /// </summary>
    public ArrayList HeaderInfo
    {
      get { return this._alHeaderInfo; }
    }

    #endregion properties
    
    #region List handling

    /// <summary>
    /// Removes all objects from the list
    /// </summary>
    public void RemoveAll () 
    {
      _Items.Clear ();
    }

    /// <summary>
    /// Adds an item to the list
    /// </summary>
    /// <param name="item">The item to be added</param>
    public void Add ( CStoreResultItem item ) 
    {
      _Items.Add ( item );
    }

    /// <summary>
    /// Gets the list element at a given index position
    /// </summary>
    /// <param name="idx">The index position</param>
    /// <returns>The list element ( a reference )</returns>
    CStoreResultItem ElementAt ( int idx ) 
    {
      return this[idx];
    }
    
    /// <summary>
    /// Copies a CStoreResults object into the current instance 
    /// </summary>
    /// <param name="res">The CStoreResults object to be copied</param>
    void Copy ( CStoreResults res ) 
    {
      _Items.AddRange ( res );
    }

    #endregion  // List handling

    #region ICollection Members

    /// <summary>
    /// Gets a value indicating whether access to the StoreResults collection is synchronized (thread-safe).
    /// </summary>
    public bool IsSynchronized
    {
      get
      {
        // Add StoreResults.IsSynchronized getter implementation
        return _Items.IsSynchronized;
      }
    }

    /// <summary>
    /// Gets the number of elements actually contained in the StoreResults collection.
    /// </summary>
    public int Count
    {
      get
      {
        // Add StoreResults.Count getter implementation
        return _Items.Count;
      }
    }

    /// <summary>
    /// Copies the entire StoreResults collection to a compatible one-dimensional Array, 
    /// starting at the specified index of the target array.
    /// </summary>
    /// <param name="array">
    /// The one-dimensional Array that is the destination of the elements copied from the StoreResults collection.
    /// The Array must have zero-based indexing.
    /// </param>
    /// <param name="index">The zero-based index in array at which copying begins.</param>
    public void CopyTo(Array array, int index)
    {
      // Add StoreResults.CopyTo implementation
      _Items.CopyTo ( array, index );
    }

    /// <summary>
    /// Gets an object that can be used to synchronize access to the StoreResults collection.
    /// </summary>
    public object SyncRoot
    {
      get
      {
        // Add StoreResults.SyncRoot getter implementation
        return _Items.SyncRoot;
      }
    }

    #endregion // ICollection Members
  
  }


  /// <summary>
  /// Class CStoreResultItem:
  /// GSMReader data storage (for IUT-built devices):
  /// Stored result item
  /// </summary>
  /// <remarks>
  /// The device SW defines the STORERESULTITEM structure for eeprom data storage as follows:
  /// 
  /// typedef struct tagSTORERESULTITEM
  /// {
  ///   char DateTime[7];                   // DateTime stamp (Y-M-D-H-M-S + 0xFF-termination)
  ///   Int8U ScriptIndex;                  // Index of the script producing the result
  ///   Int16U Result[GSM_MAX_WINDOWS];     // Substance concentrations
  /// } STORERESULTITEM;
  /// 
  /// </remarks>
  public class CStoreResultItem
  {
    #region constructors
    
    /// <summary>
    /// Constructor
    /// </summary>
    public CStoreResultItem ()
    {
      dwPosition    = 0xFFFFFFFF;
      dtTime        = DateTime.MinValue;
      byScriptIndex = 0xFF;
      for (int i=0; i < arfResults.Length ;i++) arfResults[i] = invalid;
    }
	
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="sPar">A CStoreResultItem string</param>
    /// <remarks>
    /// The format of the string is based on the conversion of a storage result item into a
    /// string representation as realized in the routine 'StoreResults_StructToString()' of
    /// the device SW, and it looks as follows:
    ///   "(DT)SPACE(ScriptIdx)SPACE(win0) ... SPACE(winNm1)",
    /// with: 
    ///   (DT): DateTime-String: "(Y:2B)(M:2B)(D:2B)(H:2B)(M:2B)(S:2B)",
    ///   N:    Max. number of substance windows 'MAX_SCRIPT_WINDOWS'
    /// </remarks>
    public CStoreResultItem( string sPar ) 
    {
      try 
      {
        // Parse the string
        // ( # of elements: Time + ScriptIdx + 'MAX_SCRIPT_WINDOWS' substance results )
        string[] ars = sPar.Split ( ' ' );
        if ( ars.Length != (Script.MAX_SCRIPT_WINDOWS + 2) ) 
          throw new ApplicationException ();

        // Time
        _StringToDateTime ( ars[0], out dtTime );
        
        // Script index 
        byScriptIndex = Byte.Parse ( ars[1] );

        // Results
        for ( int nCount = 0; nCount < Script.MAX_SCRIPT_WINDOWS; nCount++ )
        {
          ushort w = ushort.Parse ( ars[nCount+2] );
          arfResults[nCount] = _StoreResults_ReMap ( w );
        }
      }
      catch
      {
        throw;
      }
    }

    #endregion // constructors
  
    #region member
    
    /// <summary>
    /// The position of the item within the list
    /// </summary>
    public uint dwPosition;
    /// <summary>
    /// The DateTime stamp of the item
    /// </summary>
    public DateTime dtTime;
    /// <summary>
    /// The script index of the item
    /// </summary>
    public Byte byScriptIndex;
    /// <summary>
    /// The substance concentrations of the item
    /// </summary>
    public float[] arfResults = new float[Script.MAX_SCRIPT_WINDOWS];

    #endregion // member
  
    #region constants
  
    /// <summary>
    /// Item: Invalid substance result
    /// </summary>
    public const float invalid         = -1.0f;

    #endregion // constants
  
    #region overloading
  
    /// <summary>
    /// Operator overloading
    /// </summary>
    public static bool operator > ( CStoreResultItem si1, CStoreResultItem si2 ) 
    {
      return ( si1.dtTime > si2.dtTime );
    }

    /// <summary>
    /// Operator overloading
    /// </summary>
    public static bool operator < ( CStoreResultItem si1, CStoreResultItem si2 ) 
    {
      return ( si1.dtTime < si2.dtTime );
    }

    #endregion // overloading
  
    #region methods

    /// <summary>
    /// Initiates the members year, month, day, hour, minute, second of a DateTime structure by means of 
    /// a DateTime string.
    /// </summary>
    /// <param name="sDT">The Datetime string</param>
    /// <param name="dt">The DateTime object (output)</param>
    /// <remarks>
    /// The DateTime-String has the following format: 
    ///   "(Y:2B)(M:2B)(D:2B)(H:2B)(M:2B)(S:2B)".
    /// </remarks>
    void _StringToDateTime (string sDT, out DateTime dt)
    {
      dt = new DateTime (
        int.Parse ( sDT.Substring( 0, 2 ) ) + 2000,
        int.Parse ( sDT.Substring( 2, 2 ) ),
        int.Parse ( sDT.Substring( 4, 2 ) ),
        int.Parse ( sDT.Substring( 6, 2 ) ),
        int.Parse ( sDT.Substring( 8, 2 ) ),
        int.Parse ( sDT.Substring( 10, 2 ) )
        );
    }

    /// <summary>
    /// Re-maps (re-transforms) a float value from a 2-byte integer value.
    /// </summary>
    /// <param name="wval">The 2-byte integer value to be re-transformed</param>
    /// <returns>The float value</returns>
    /// <remarks>
    /// 1.
    /// This routine is originally deposed in the device SW.
    /// 2.
    /// On remapping the 2-byte integer into a float value one has to check whether bits 14 and/or 15
    /// are set: if so, the int value has to be divided by 10 (bits 14,15: 10), 100 (01), or 1000 (11) 
    /// corr'ly.
    /// </remarks>
    float _StoreResults_ReMap(ushort wval)
    {
      float fval;

      if (wval == 0xFFFF)
      {
        // Indicates, that a substance window is not used -> no remapping required
        fval=invalid;
      }
      else if ((wval|0x3FFF) == 0xFFFF)
      {
        // Recognition bits 14 & 15 are set: fval in [0, 1) with 3 decimal places   
        wval &= 0x3FFF;       // reset bits 14 & 15
        fval=wval/1000.0f;
      }
      else if ((wval|0x3FFF) == 0xBFFF)
      {
        // Recognition bit 15 is set: fval in [1, 100) with 2 decimal places   
        wval &= 0x7FFF;       // reset bit 15
        fval=wval/100.0f;
      }
      else if ((wval|0x3FFF) == 0x7FFF)
      {
        // Recognition bit 14 is set: fval in [100, 1000) with 1 decimal place   
        wval &= 0xBFFF;       // reset bit 14
        fval=wval/10.0f;
      }
      else
      {
        // No recognition bit set: fval in [1000, +INF) with no decimal places
        fval=wval; 
      }
      return fval;
    }

    #endregion methods

  }

}
