using System;

namespace App
{
  /// <summary>
  /// The TransferTask enumeration
  /// </summary>
  /// <remarks>
  /// The member values MUST coincide with the assignments of the global 'g_byAppTask'
  /// ( Application task indicator ) of the device SW:
  ///  'g_byAppTask' =  1 - Task 'Script transfer' active
  ///                   
  ///                   2 - Task 'Eeprom data transfer' active
  ///                   3 - Task 'Eeprom SriptInfo transfer' active
  ///                   4 - Task 'Eeprom data clear (transfer)' active
  ///                   
  ///                  10  - Task 'SDcard data transfer' active
  ///                  11 - Task 'SDcard SriptInfo transfer' active
  ///                  12 - Task 'SDcard data clear (transfer)' active
  ///                  13 - Task 'SDcard reinitialize (transfer)' active
  ///                   
  ///                  20 - Task 'Service-Read transfer' active
  ///                  21 - Task 'Service-Write transfer' active
  ///                  22 - Task 'Service-Adjust Flow (transfer)' active 
  ///                   
  ///                  25 - Task 'CTS-Read transfer'
  ///                  26 - Task 'CTS-Write transfer'
  ///                  
  ///                  30 - Task 'SDcard data ( Diagnosis ) transfer' active 
  ///                  
  ///                  40 - Task 'MP-Read transfer' active
  ///                  41 - Task 'MP-Write transfer' active
  /// </remarks>
  public enum TransferTask
  {
    Script              = 1,  // Script transfer
    
    Eeprom_Data         = 2,  // Eeprom data transfer
    Eeprom_ScriptInfo   = 3,  // Eeprom SriptInfo transfer
    Eeprom_Data_Clear   = 4,  // Eeprom data clear (transfer)
    
    SDcard_Data         = 10, // SDcard data transfer
    SDcard_ScriptInfo   = 11, // SDcard SriptInfo transfer: Curr'ly not needed.
    SDcard_Data_Clear   = 12, // SDcard data clear (transfer)
    SDcard_InitDisk     = 13, // SDcard reinitialize (transfer)

    Service_Read        = 20, // Service-Read transfer
    Service_Write       = 21, // Service-Write transfer
    Service_AdjustFlow  = 22, // Service-Adjust Flow (transfer)

    CTS_Read            = 25, // Clock-timed scripts - Read transfer
    CTS_Write           = 26, // Clock-timed scripts - Write transfer
    
    SDcard_Data_Diag    = 30, // Diagnosis transfer: Curr'ly not needed.

    MP_Read             = 40, // MP-Read transfer
    MP_Write            = 41, // MP-Write transfer
  };  
  
}
