using System;
using System.Runtime.InteropServices;
using lc_handle_t = System.UInt32;

namespace App
{
	/// <summary>
	/// Class EnkyLC32:
	/// Enky LC software protection kit - 32 bit version
	/// </summary>
	/// <remarks>
  /// 1.
  /// The corr'ing DLL 'lc.dll' must be placed in the directory of the app's executing file.
  /// 2.
  /// The format of the EnkyLC stuff string is as follows:
  ///   "(Name)\t(Comment)\t(Actions)\t(Usertype)\t(PermissionsSW)\t(Editor)\t(Date_Creation)\t", 
  /// with: 
  ///   Name:           Name of the Dongle owner
  ///   Comment:        Comment
  ///   Actions:        IMS - 1, PID - 2, Combi - 4
  ///   Usertype:       C / T / E
  ///   PermissionSW:   Edit scripts? - 1, Edit service? - 2, Edit extended service? - 4
  ///   Editor:         The person, who edited database and dongle
  ///   Date_Creation:  The creation date of the record
  /// 3.
  /// The administrator and user passwords of the test EnkyLC dongle are (state 4.7.14):
  ///   PW type                       default (initial)                 new
  ///   -------------------------------------------------------------------------------
  ///   admin                         "12345678"                        "*adm5513"
  ///   user                          "12345678"                        "R2D2R2D2"
  /// </remarks>
  public class EnkyLC32 : EnkyLC
  {
	  /// <summary>
	  /// Constructor
	  /// </summary>
    public EnkyLC32()
	  {
  	}

  
    // ******************************************************************************************
    // Basic stuff
    // ******************************************************************************************

    #region LC constants and typedefs

    /// <summary>
    /// Name of the ENKY LC DLL:
    /// This dll is taken from the following folder of the EnkyLC CD: "\Enky LC2\api\DLL".
    /// This is the 32bit dll.
    /// </summary>
    const string _ENKYLC_DLL_NAME = "lc.dll";

    #endregion LC constants and typedefs

    #region LC API function interface

    /// <summary>
    /// Opens a matching dongle according to a valid Developer ID and index 
    /// (in case of more than one dongles plugged in).
    /// </summary>
    /// <param name="vendor">[IN] Developer ID (0=all)</param>
    /// <param name="index">[IN] Index of devices (0=first, and so on)</param>
    /// <param name="handle">[OUT] Returned device handle</param>
    /// <returns>see error codes</returns>
    [DllImport (_ENKYLC_DLL_NAME)]
    static extern int LC_open(int vendor, int index, ref lc_handle_t handle);
    
    /// <summary>
    /// Closes an open device.
    /// </summary>
    /// <param name="handle">[IN] Open device handle</param>
    /// <returns>see error codes</returns>
    [DllImport (_ENKYLC_DLL_NAME)]
    static extern int LC_close(lc_handle_t handle);
    
    /// <summary>
    /// Validates a password.
    /// </summary>
    /// <param name="handle">[IN] Open device handle</param>
    /// <param name="type">[IN] Password type (0=Administration, 1=Generic)</param>
    /// <param name="passwd">[IN]The password to be validated</param>
    /// <returns>see error codes</returns>
    [DllImport (_ENKYLC_DLL_NAME)]
    static extern int LC_passwd(lc_handle_t handle, int type, byte[] passwd);
    
    /// <summary>
    /// Reads data from specified block.
    /// </summary>
    /// <param name="handle">[IN] Open device handle</param>
    /// <param name="block">[IN] Block number (0~3)</param>
    /// <param name="buffer">[OUT] Read from data buffer (>=512 bytes)</param>
    /// <returns>see error codes</returns>
    [DllImport (_ENKYLC_DLL_NAME)]
    static extern int LC_read(lc_handle_t handle, int block, byte[] buffer);
    
    /// <summary>
    /// Writes data to specified block.
    /// </summary>
    /// <param name="handle">[IN] Open device handle</param>
    /// <param name="block">[IN] Block number (0~3)</param>
    /// <param name="buffer">[IN] Write into data buffer (>=512 bytes)</param>
    /// <returns>see error codes</returns>
    [DllImport (_ENKYLC_DLL_NAME)]
    static extern int LC_write(lc_handle_t handle, int block, byte[] buffer);
    
    /// <summary>
    /// Uses embedded AES algorithm to encrypt data.
    /// </summary>
    /// <param name="handle">[IN] Open device handle</param>
    /// <param name="plaintext">[IN] Plaintext to be encrypted (16 bytes)</param>
    /// <param name="ciphertext">[OUT] Ciphertext encrypted (16 bytes)</param>
    /// <returns>see error codes</returns>
    [DllImport (_ENKYLC_DLL_NAME)]
    static extern int LC_encrypt(lc_handle_t handle, byte[] plaintext, byte[] ciphertext);
    
    /// <summary>
    /// Uses embedded AES algorithm to encrypt data.
    /// </summary>
    /// <param name="handle">[IN] Open device handle</param>
    /// <param name="ciphertext">[IN] Ciphertext to be decrypted (16 bytes)</param>
    /// <param name="plaintext">[OUT] Plaintext decrypted (16 bytes)</param>
    /// <returns>see error codes</returns>
    [DllImport (_ENKYLC_DLL_NAME)]
    static extern int LC_decrypt(lc_handle_t handle, byte[] ciphertext, byte[] plaintext);
    
    /// <summary>
    /// Sets up a new password.
    /// </summary>
    /// <param name="handle">[IN] Open device handle</param>
    /// <param name="type">[IN] Password type (0=Administration, 1=Generic)</param>
    /// <param name="newpasswd">[IN] New password (8 bytes)</param>
    /// <param name="retries">[IN] Error count (1~15), -1 stands for no error count</param>
    /// <returns>see error codes</returns>
    [DllImport (_ENKYLC_DLL_NAME)]
    static extern int LC_set_passwd(lc_handle_t handle, int type, byte[] newpasswd, int retries);
    
    /// <summary>
    /// Changes password.
    /// </summary>
    /// <param name="handle">[IN] Open device handle</param>
    /// <param name="type">[IN] Password type (0=Administration, 1=Generic)</param>
    /// <param name="oldpasswd">[IN] Old password (8 bytes)</param>
    /// <param name="newpasswd">[IN] New password (8 bytes)</param>
    /// <returns>see error codes</returns>
    [DllImport (_ENKYLC_DLL_NAME)]
    static extern int LC_change_passwd(lc_handle_t handle, int type, byte[] oldpasswd, byte[] newpasswd);
    
    /// <summary>
    /// Reads device information.
    /// </summary>
    /// <param name="handle">[IN] Open device handle</param>
    /// <param name="info">[OUT] Structured pointer storing hardware information</param>
    /// <returns>see error codes</returns>
    [DllImport (_ENKYLC_DLL_NAME)]
    static extern int LC_get_hardware_info(lc_handle_t handle, ref LC_hardware_info info);
    
    /// <summary>
    /// Reads version information.
    /// </summary>
    /// <param name="info">[OUT] Structured pointer storing hardware information</param>
    /// <returns>see error codes</returns>
    [DllImport (_ENKYLC_DLL_NAME)]
    static extern int LC_get_software_info(ref LC_software_info info);

    /// <summary>
    /// Calculates HMAC value by hardware.
    /// </summary>
    /// <param name="handle">[IN] Opened device handle</param>
    /// <param name="text">[IN] The data that needs to be HMAC value</param>
    /// <param name="textlen">[IN] Length of text (by byte, >=0)</param>
    /// <param name="digest">[OUT] HMAC value (20 bytes)</param>
    /// <returns>see error codes</returns>
    [DllImport (_ENKYLC_DLL_NAME)]
    static extern int LC_hmac(lc_handle_t handle, byte[] text, int textlen, byte[] digest);

    /// <summary>
    /// Calculates HMAC value by software.
    /// </summary>
    /// <param name="text">[IN] The data that needs to be HMAC value</param>
    /// <param name="textlen">[IN] Length of text (by byte, >=0)</param>
    /// <param name="key">[IN]key of HMAC algorithm (20 bytes Hex string)</param>
    /// <param name="digest">[OUT] HMAC value (20 bytes)</param>
    /// <returns>see error codes</returns>
    [DllImport (_ENKYLC_DLL_NAME)]
    static extern int LC_hmac_software(byte[] text, int textlen, byte[] key, byte[] digest);

    /// <summary>
    /// Updates remotely.
    /// </summary>
    /// <param name="handle">[IN] Open device handle</param>
    /// <param name="buffer">[IN] Buffer area of updating package</param>
    /// <returns>see error codes</returns>
    [DllImport (_ENKYLC_DLL_NAME)]
    static extern int LC_update(lc_handle_t handle, byte[] buffer);
 
    #endregion LC API function interface


    // ******************************************************************************************
    // Enhanced stuff
    // ******************************************************************************************

    #region methods

    /// <summary>
    /// Tries to open a dongle
    /// </summary>
    /// <param name="handle">Returned device handle (out)</param>
    /// <returns>True, if the dongle is either the test dongle or a real-life dongle; False otherwise</returns>
    static int OpenDongle(ref lc_handle_t handle)
    {
      int res;

      // Try to open test dongle
      res = LC_open(DeveloperID_Test, 0, ref handle);
      if (res == LC_SUCCESS)
        return res;   // Here we know, that the dongle is our - unique - test dongle
      // Try to open real-life dongle
      res = LC_open(DeveloperID_RealLife, 0, ref handle);
      if (res == LC_SUCCESS)
        return res;   // Here we know, that the dongle is a real-life dongle
      // Here we know, that the dongle is neighter the test dongle nor a real-life dongle
      return res;
    }
    
    /// <summary>
    /// Checks for EnkyLC presence
    /// </summary>
    /// <param name="nInterval_ms">The interval of the timer, from which this routine is invoked</param>
    /// <returns>True, if check ocurred; False otherwise</returns>
    internal static bool Check32(int nInterval_ms)
    {
      bool bRet;

      // Incr. timer counter
      nTimerCount++;
      // Check: 2 sec elapsed?
      if ((2 * 1000) == (nTimerCount * nInterval_ms))
      {
        // Yes:
        bRet = true;

        // Reset timer counter
        nTimerCount = 0;

        // Init. member var's
        UserType = UserType.Customer;
        bEditScripts = false;
        bEditSvc = false;
        bEditSvcExt = false;

        // Check: Enky LC present?
        lc_handle_t handle = 0;
        int res = OpenDongle(ref handle);
        if (res != LC_SUCCESS)
        {
          // No:
          return bRet;
        }
        // Get hardware info
        res = LC_get_hardware_info(handle, ref hwinfo);
        if (res != LC_SUCCESS)
        {
          LC_close(handle);
          return bRet;
        }
        // Verify User Password
        // (Reason: User privileges are required in order to read block 1)
        byte[] passwd = AppData.CommonData.USERPW;
        res = LC_passwd(handle, 1, passwd);
        if (res != LC_SUCCESS)
        {
          LC_close(handle);
          return bRet;
        }
        // Read Block 1 
        byte[] outdata = new byte[512];   // not less than 512
        res = LC_read(handle, 1, outdata);
        if (res != LC_SUCCESS)
        {
          LC_close(handle);
          return bRet;
        }
        // Close device 
        LC_close(handle);

        // Get info from Block 1
        string sout = System.Text.Encoding.Default.GetString(outdata);
        try
        {
          // Parse stuff string
          string[] ars = sout.Split ('\t');
          // Interesting items:
          //   Actions:        IMS - 1, PID - 2, Combi - 4
          //   Usertype:       C / T / E
          //   PermissionSW:   Edit scripts? - 1, Edit service? - 2, Edit extended service? - 4
          string sActions = ars[2].Trim ();
          string sUsertype = ars[3].Trim ();
          string sPermissionSW = ars[4].Trim ();
          // Actions
          // (for the PIDControl SW only the PID part is of interest)
          int actions = int.Parse (sActions);
          bool bPID = ((actions & 0x02) != 0);
          // Usertype
          switch (sUsertype)
          {
            default:
            case "C": UserType = UserType.Customer; break;
            case "T": UserType = UserType.TrainedCustomer; break;
            case "E": UserType = UserType.Expert; break;
          }
          // PermissionSW
          //  1. as taken directly from the stuff string (i.e. without involvement of the 'action' item)
          int permissionSW = int.Parse (sPermissionSW);
          bEditScripts = ((permissionSW & 0x01) != 0);  // Is the user allowed to edit scripts?
          bEditSvc = ((permissionSW & 0x02) != 0);      // Is the user allowed to edit service data?
          bEditSvcExt = ((permissionSW & 0x04) != 0);   // Is the user allowed to edit extended service data?
          //  2. summarily (i.e. with involvement of the 'action' item)      
          bEditScripts &= bPID;
          bEditSvc &= bPID;
          bEditSvcExt &= bPID;
        }
        catch
        {
        }
      }
      else
      {
        // No
        bRet = false;
      }
      // Done
      return bRet;
    }

    #endregion methods

  }
}
