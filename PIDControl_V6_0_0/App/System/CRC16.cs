using System;

namespace App
{
	/// <summary>
	/// Class CRC16:
	/// CRC16
	/// </summary>
	public class CRC16
	{
		/// <summary>
		/// Constructor
		/// </summary>
    protected CRC16()
		{
		}

    #region enums & constants
    
    /// <summary>
    /// CRC16 calculation types enumeration
    /// </summary>
    public enum Types 
    {
      /// <summary>
      /// calculation in one process
      /// </summary>
      OneProcess,
      /// <summary>
      /// calculation piecewise
      /// </summary>
      Piecewise,
    };

    #endregion enums & constants
    
    #region members
    
    /// <summary>
    /// CRC16
    /// </summary>
    static UInt16 m_uiCRC16 = 0;

    #endregion members

    #region methods
 
    /// <summary>
    /// CRC16: Calculation (routine corr's to the the BASCOM library routine 'CRC16').
    /// </summary>
    /// <param name="arby">The byte array to get the CRC16 value from</param>
    /// <param name="t">The CRC16 calculation type: in one process OR piecewise</param>
    /// <returns>
    /// The final CRC16 value, if type is 'in one process'.
    /// The interim CRC16 value, if type is 'piecewise'.
    /// </returns>
    public static UInt16 crc16(byte[] arby, Types t)
    {
      UInt16 crc;

      if (t == Types.Piecewise) crc= m_uiCRC16;
      else                      crc= 0;
      int len = arby.Length;
      int idx = 0;
      while( len-- > 0 ) 
      {
        crc ^= (UInt16)(arby[idx++] << 8);
        for(int i = 0; i < 8; ++i ) 
        {
          if( (crc & 0x8000) != 0 )   crc = (UInt16)((crc << 1) ^ 0x1021);
          else                        crc = (UInt16)(crc << 1);
        }
      }
      if (t == Types.Piecewise) m_uiCRC16= crc;
      return crc;
    }

    #endregion methods

  }
}

