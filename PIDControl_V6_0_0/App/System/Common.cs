using System;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Printing;
using System.Collections;

namespace App
{
  #region Common stuff

  /// <summary>
	/// Class Common:
	/// Shared methods
	/// </summary>
	public class Common
	{
		/// <summary>
		/// Constructor
		/// </summary>
    private Common()
		{
		}
  
    #region enums and constants

    /// <summary>
    /// Form open mode enumeration
    /// </summary>
    public enum FormOpenMode
    {
      Add = 0,
      Edit
    };

    #endregion enums and constants

    #region methods

    /// <summary>
    /// Converts a double value into its string representation with a given number
    /// of decimal digits.
    /// </summary>
    /// <param name="dVal">The double value</param>
    /// <param name="nPostDigits">The number of decimal digits</param>
    /// <returns>The string representation</returns>
    /// <remarks>TODO - the exact behaviour must be reconsidered.</remarks>
    public static string ConvertToScience ( double dVal, int nPostDigits )
    {
      string format = string.Format ( "F{0}", nPostDigits );
      string s = dVal.ToString ( format );
      return s;
    }
  
    /// <summary>
    /// Replaces all ',' in a string by '.'.
    /// </summary>
    /// <param name="s">The string</param>
    public static void PointToComma ( ref string s )
    {
      string sNew = s.Replace ( '.', ',' );
      s = sNew;
    } 

    /// <summary>
    /// Centers a form over another ( a parent form ) one.
    /// </summary>
    /// <param name="fOwned">The form to be centered</param>
    /// <param name="fOwner">The parent form</param>
    public static void CenterWindow(Form fOwned,Form fOwner)
    {
      try 
      {
        Rectangle rOwner=fOwner.DesktopBounds;
        Rectangle rOwned=fOwned.DesktopBounds;
        int cw=(rOwner.Width-rOwned.Width)/2;
        int ch=(rOwner.Height-rOwned.Height)/2;
        Point pNewLoc=new Point(rOwner.Left+cw, rOwner.Top+ch);
        if (pNewLoc.X < 0) 
          pNewLoc.X=0;
        else if (pNewLoc.X + rOwned.Width > Screen.PrimaryScreen.WorkingArea.Width)
          pNewLoc.X = Screen.PrimaryScreen.WorkingArea.Width-rOwned.Width;
        if (pNewLoc.Y < 0) 
          pNewLoc.Y=0;
        else if (pNewLoc.Y + rOwned.Height > Screen.PrimaryScreen.WorkingArea.Height)
          pNewLoc.Y = Screen.PrimaryScreen.WorkingArea.Height-rOwned.Height;
        fOwned.DesktopLocation=pNewLoc;
      }
      catch 
      {
      }
    }

    /// <summary>
    /// Loads a cursor from the embedded resource of the executed assembly.
    /// </summary>
    /// <param name="fullpath">Full path of the cursor resource</param>
    /// <returns>The cursor</returns>
    public static Cursor LoadCursor ( string fullpath )
    {
      Cursor cursor = null;
      try
      {
        System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
        System.IO.Stream stream = asm.GetManifestResourceStream (fullpath);
        cursor = new Cursor ( stream );
      }
      catch
      {
      }
      return cursor;
    }
    
    /// <summary>
    /// Loads a bitmap from the embedded resource of the executed assembly.
    /// </summary>
    /// <param name="fullpath">Full path of the bitmap resource</param>
    /// <returns>The bitmap</returns>
    public static Bitmap LoadBitmap ( string fullpath )
    {
      Bitmap bmp = null;
      try
      {
        System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
        System.IO.Stream stream = asm.GetManifestResourceStream (fullpath);
        bmp = new Bitmap ( stream );
      }
      catch
      {
      }
      return bmp;
    }

    /// <summary>
    /// Preprocess the text of a control, which precedes in the TAB sequence a specified control
    /// in a form.
    /// </summary>
    /// <param name="f">The form</param>
    /// <param name="ctl">The specified control</param>
    /// <returns>The preprocessed text of the predecessor control</returns>
    /// <remarks>The predecessor control is as a rule a Label control or a GroupBox control</remarks>
    public static string GetAdjustedText (Form f, Control ctl)
    {
      // Get the preceding control with regard to the control to be checked 
      // (should be a Label or a GroupBox as a rule)
      // Notes:
      //  We perform at most 2 steps back in the Tab order in order to find the control
      //  that contains the text for the specified control. 
      Control ctlText = f.GetNextControl ( ctl, false );    // 1. step                   
      if ( ( ctlText.GetType () != typeof ( System.Windows.Forms.Label ) ) &&
           ( ctlText.GetType () != typeof ( System.Windows.Forms.GroupBox ) ) )
        ctlText = f.GetNextControl ( ctlText, false );      // 2. step
      // Its text
      string sText = ctlText.Text;
      // Discard an ending colon, if any
      if (sText.EndsWith (":")) sText = sText.Substring (0, sText.Length-1);
      // Extract ampersand chars (&), if any
      int pos = sText.IndexOf ("&");
      if (pos != -1)
      {
        // Check: Is the & char found the last char in the string?
        if (sText.Length > pos+1)
        {
          // No: Check: Perhaps it is followed by another & char?
          if ((sText.ToCharArray())[pos+1] != '&')
          {
            // No: Extract the & char
            sText = string.Format ("{0}{1}", sText.Substring(0, pos), sText.Substring(pos+1));
          }
        }
      }
      // done
      return sText;
    }

    /// <summary>
    /// Converts a seconds value of long type into a formatted time string. 
    /// </summary>
    /// <param name="sec">The seconds value</param>
    /// <returns>The formatted time string</returns>
    public static string UInt32ToTimeString ( UInt32 sec )
    {
      // min
      UInt32 min = sec/60;
      sec %= 60;
      // h
      UInt32 hours = min/60;
      min %= 60;
      // d
      UInt32 days = hours/24;
      hours %= 24;
      // Formatted string
      string sTime;
      if ( days > 0 )
        sTime = string.Format ( "{0}d {1:D2}h {2:D2}' {3:D2}''", days, hours, min, sec );
      else
        sTime = string.Format ( "{0:D2}h {1:D2}' {2:D2}''", hours, min, sec );
      return sTime;
    }

    #endregion methods

    #region Win macros

    /// <summary>
    /// Retrieves the low-order byte from the specified 16-bit value. 
    /// </summary>
    /// <remarks>
    /// Win makro: #define LOBYTE(w)   ((BYTE) (w))
    /// </remarks>
    public static byte LOBYTE ( ushort w ) 
    {
      return ( ( byte ) w );
    }
    
    /// <summary>
    /// Retrieves the high-order byte from the specified 16-bit value. 
    /// </summary>
    /// <remarks>
    /// Win makro: #define HIBYTE(w)   ((BYTE) (((WORD) (w) >> 8) & 0xFF))
    /// </remarks>
    public static byte HIBYTE ( ushort w ) 
    {
      return ( (byte) ( ( w >> 8 ) & 0xFF ) );
    }
    
    /// <summary>
    /// Creates an unsigned 16-bit integer by concatenating two specified unsigned character values. 
    /// </summary>
    /// <remarks>
    /// Win makro: #define MAKEWORD(a, b) ((WORD) (((BYTE) (a)) | ((WORD) ((BYTE) (b))) << 8))
    /// </remarks>
    public static ushort MAKEWORD ( byte bL, byte bH ) 
    {
      return ( (ushort) ( ((byte) (bL)) | ((ushort) ((byte) (bH))) << 8 ) );
    }

    #endregion Win macros

  }

  #endregion Common stuff

  #region Validity check

  /// <summary>
  /// Validity check: Data type enumeration
  /// </summary>
  public enum CheckType
  {
    /// <summary>
    /// Data type: Integer
    /// </summary>
    Int,            
    /// <summary>
    /// Data type: UInt16
    /// </summary>
    UInt16,
    /// <summary>
    /// Data type: UInt32
    /// </summary>
    UInt32,
    /// <summary>
    /// Data type: Float
    /// </summary>
    Float,
    /// <summary>
    /// Data type: DateTime
    /// </summary>
    DateTime,
    /// <summary>
    /// Data type: String 
    /// </summary>
    String,
  };  
    
  /// <summary>
  /// Validity check: Relational operator enumeration
  /// </summary>
  public enum CheckRelation
  {
    /// <summary>
    /// None
    /// </summary>
    None,
    /// <summary>
    /// Greater than
    /// </summary>
    GT,
    /// <summary>
    /// Greater than or Equal
    /// </summary>
    GE,
    /// <summary>
    /// Less than
    /// </summary>
    LT,
    /// <summary>
    /// Less than or Equal
    /// </summary>
    LE,
    /// <summary>
    /// Equal
    /// </summary>
    EQU,
    /// <summary>
    /// Inside region
    /// </summary>
    IN,
    /// <summary>
    /// Outside region
    /// </summary>
    NIN,
  };  


  /// <summary>
  /// Class CheckRelationItem:
  /// A CheckRelation item
  /// </summary>
  public class CheckRelationItem
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public CheckRelationItem()
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pcr">The check relation</param>
    /// <param name="pref1">The reference value 1: The 1st (lower) limit</param>
    /// <param name="pref2">The reference value 2: The 2nd (upper) limit</param>
    public CheckRelationItem(CheckRelation pcr, double pref1, double pref2)
    {
      cr = pcr;
      ref1 = pref1;
      ref2 = pref2;
    }

    #endregion constructors

    #region members

    /// <summary>
    /// The Check relation
    /// </summary>
    CheckRelation cr = CheckRelation.None;
    /// <summary>
    /// Reference value 1: The 1st (lower) limit
    /// </summary>
    double ref1 = 0;
    /// <summary>
    /// Reference value 2: The 2nd (upper) limit
    /// </summary>
    double ref2 = 0; 

    #endregion members

    #region properties

    /// <summary>
    /// The Check relation
    /// </summary>
    public CheckRelation Relation
    {
      get { return cr; }
    }
    
    /// <summary>
    /// The 1st (lower) reference value
    /// </summary>
    public double Ref1
    {
      get { return ref1; }
    }
    
    /// <summary>
    /// The 2nd (upper) reference value
    /// </summary>
    public double Ref2
    {
      get { return ref2; }
    }
  
    #endregion properties
  }


  /// <summary>
  /// Class CheckRelationItemList:
  /// CheckRelation item list
  /// </summary>
  public class CheckRelationItemList : IEnumerable
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public CheckRelationItemList ()
    {
    }

    #endregion constructors

    #region members
    
    /// <summary>
    /// The list
    /// </summary>
    private ArrayList _Items = new ArrayList();

    #endregion members
    
    #region methods

    /// <summary>
    /// Clears the list
    /// </summary>
    public void Clear () 
    {
      _Items.Clear ();
    }

    /// <summary>
    /// Adds a CheckRelationItem object to the list
    /// </summary>
    /// <param name="res">The CheckRelationItem object to be added</param>
    public void Add ( CheckRelationItem item ) 
    {
      _Items.Add ( item );
    }
    
    #endregion methods

    #region properties

    /// <summary>
    /// Gets the size of the list
    /// </summary>
    public int Count 
    {
      get { return _Items.Count; }
    }
    
    #endregion properties

    #region indexer
    
    /// <summary>
    /// The indexer
    /// </summary>
    public CheckRelationItem this[int index]
    {
      get { return (CheckRelationItem)_Items[index]; }
    }

    #endregion indexer
    
    #region IEnumerable Members

    /// <summary>
    /// The enumerator
    /// </summary>
    /// <returns>The enumerator</returns>
    public IEnumerator GetEnumerator ()
    {
      return _Items.GetEnumerator ();
    }

    #endregion IEnumerable Members
  }


  /// <summary>
  /// Class Check:
  /// Validity check methods
  /// </summary>
  public class Check
  {

    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    private Check()
    {
    }
  
    #endregion constructors

    #region methods

    /// <summary>
    /// Checks a Controls text with respect to the fulfillment of given validity check criteria.
    /// </summary>
    /// <param name="c">The Control</param>
    /// <param name="ct">The validity check type</param>
    /// <param name="cr">The validity check relation</param>
    /// <param name="n1">Reference value 1</param>
    /// <param name="n2">Reference value 2</param>
    /// <param name="bShowMsg">Indicates, whether to display an error message or not</param>
    /// <param name="o">In case of success, the value that represents the Controls text (output)</param>
    /// <returns>True, if the Controls text fulfills the check criteria; false otherwise</returns>
    public static Boolean Execute (Control c, CheckType ct, CheckRelation cr, int n1, int n2, Boolean bShowMsg, out object o)
    {
      int n = 0;
      UInt16 ui16 = 0;
      UInt32 ui = 0;
      float f = 0.0f;
      DateTime dt = DateTime.MinValue;
      Boolean bEr;

      // Init's
      o = null;
      string s = c.Text.Trim ();

      // Check
      try 
      {
        // CD: Check type
        switch ( ct )
        {
          case CheckType.Int:                         // Integer check
            // Get the integer value, if any
            n = int.Parse (s);
            break;
          case CheckType.UInt16:                      // UInt16 check
            // Get the UInt16 value, if any
            ui16 = UInt16.Parse (s);
            break;
          case CheckType.UInt32:                      // UInt32 check
            // Get the UInt32 value, if any
            ui = UInt32.Parse (s);
            break;
          case CheckType.Float:                       // Float check
            // Get the float value, if any
            System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
            s = s.Replace ( ',', '.' );
            f = float.Parse (s, nfi);
            break;
          case CheckType.String:                      // String check
            // Get the string length
            n = s.Length;
            break;
          case CheckType.DateTime:                    // DateTime check
            // Get the DateTime value, if any
            if ( c.GetType().Equals (typeof(System.Windows.Forms.DateTimePicker)) )
              dt = ((DateTimePicker) c).Value;
            else
            {
              System.Globalization.DateTimeFormatInfo dtfi = System.Globalization.DateTimeFormatInfo.InvariantInfo;
              dt = DateTime.Parse ( s, dtfi );
            }
            break;
        }
      
        // CD: Check relation
        switch ( cr )
        {
          case CheckRelation.GT:
            if      ( ct==CheckType.Int || ct==CheckType.String )     bEr=!(n > n1);
            else if ( ct==CheckType.UInt16)                           bEr=!(ui16 > n1);
            else if ( ct==CheckType.UInt32)                           bEr=!(ui > n1);
            else                                                      bEr=!(f > n1);  
            if ( bEr )
              throw new ArgumentOutOfRangeException ();
            break;
          
          case CheckRelation.GE:
            if      ( ct==CheckType.Int || ct==CheckType.String )     bEr=!(n >= n1);
            else if ( ct==CheckType.UInt16)                           bEr=!(ui16 >= n1);
            else if ( ct==CheckType.UInt32)                           bEr=!(ui >= n1);
            else                                                      bEr=!(f >= n1);  
            if ( bEr )
              throw new ArgumentOutOfRangeException ();
            break;
                        
          case CheckRelation.LT:
            if      ( ct==CheckType.Int || ct==CheckType.String )     bEr=!(n < n1);
            else if ( ct==CheckType.UInt16)                           bEr=!(ui16 < n1);
            else if ( ct==CheckType.UInt32)                           bEr=!(ui < n1);
            else                                                      bEr=!(f < n1);  
            if ( bEr )
              throw new ArgumentOutOfRangeException ();
            break;
                        
          case CheckRelation.LE:
            if      ( ct==CheckType.Int || ct==CheckType.String )     bEr=!(n <= n1);
            else if ( ct==CheckType.UInt16)                           bEr=!(ui16 <= n1);
            else if ( ct==CheckType.UInt32)                           bEr=!(ui <= n1);
            else                                                      bEr=!(f <= n1);  
            if ( bEr )
              throw new ArgumentOutOfRangeException ();
            break;
                        
          case CheckRelation.IN:
            if      ( ct==CheckType.Int || ct==CheckType.String )     bEr=!(n >= n1 && n <= n2);
            else if ( ct==CheckType.UInt16)                           bEr=!(ui16 >= n1 && ui16 <= n2);
            else if ( ct==CheckType.UInt32)                           bEr=!(ui >= n1 && ui <= n2);
            else                                                      bEr=!(f >= n1 && f <= n2);  
            if ( bEr )
              throw new ArgumentOutOfRangeException ();
            break;
            
          case CheckRelation.NIN:
            if      ( ct==CheckType.Int || ct==CheckType.String )     bEr=(n >= n1 && n <= n2);
            else if ( ct==CheckType.UInt16)                           bEr=(ui16 >= n1 && ui16 <= n2);
            else if ( ct==CheckType.UInt32)                           bEr=(ui >= n1 && ui <= n2);
            else                                                      bEr=(f >= n1 && f <= n2);  
            if ( bEr )
              throw new ArgumentOutOfRangeException ();
            break;
                      
          default:
            break;
          
        }
      
        // Check OK: Overgive the value
        if      ( ct==CheckType.Int )       o = n;
        else if ( ct==CheckType.UInt16 )    o = ui16; 
        else if ( ct==CheckType.UInt32 )    o = ui; 
        else if ( ct==CheckType.Float )     o = f; 
        else if ( ct==CheckType.String )    o = s; 
        else if ( ct==CheckType.DateTime )  o = dt;
      }
      catch 
      {
        // Check NOT OK
        if ( bShowMsg )
        {
          App app = App.Instance;
          Ressources r = app.Ressources;
        
          string sText = Common.GetAdjustedText ((Form)c.TopLevelControl, c);
          string sFmt = r.GetString ("Form_Common_Error_InvalidValue"); // "Ungültiger Wert:\r\n'{0} = {1}'"
          string sMsg = string.Format (sFmt, sText, s);
          string sCap = r.GetString ("Form_Common_TextError");          // "Fehler"
          MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        // Find the TabPage (if any), that contains the erroneous control, and select it (thus making this TabPage visible)
        // (The code snippet of course considers the fact, that a control is maybe not localized within a TabPage) 
        Control cErr = c;
        Control c1;
        while (true)
        {
          c1 = c.Parent;
          if (c1 == c.TopLevelControl) break;
          if (c1 is TabPage)
          {
            ((TabControl)c1.Parent).SelectedTab = (TabPage)c1;
            break;
          }
          c = c1;
        }
        c = cErr;
        // Focus erroneous control
        c.Focus ();
        return false;
      }
      
      // Check OK
      return true;
    }

 
    /// <summary>
    /// Checks the text of a control with respect to the fulfillment of given validity check criteria.
    /// </summary>
    /// <param name="c">The Control</param>
    /// <param name="bNotEmpty">True, if the control is not allowed to be empty; False otherwise</param>
    /// <param name="ct">The validity check type</param>
    /// <param name="list">The CheckRelation item list</param>
    /// <param name="bShowMsg">Indicates, whether to display an error message or not</param>
    /// <param name="o">Represents the Controls text in case of success, equals to null otherwise (output)</param>
    /// <returns>True, if the Controls text fulfills the check criteria; false otherwise</returns>
    public static Boolean Execute (Control c, bool bNotEmpty, CheckType ct, CheckRelationItemList list, 
      bool bShowMsg, out object o)
    {
      // Init's
      o = null;
      string s = c.Text.Trim ();

      // Check
      try 
      {
        // Check for Empty:
        // If the control is allowed to be empty, and it is empty, then return True.
        if (!bNotEmpty)
        {
          if (s.Trim().Length == 0)
            return true;
        }
        
        // CD: Check type
        int n = 0;
        float f = 0.0f;
        DateTime dt = DateTime.MinValue;
        switch ( ct )
        {
          case CheckType.Int:                         // Integer check
            // Get the integer value, if any
            n = int.Parse (s);
            break;
          case CheckType.Float:                       // Float check
            // Get the float value, if any
            System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
            s = s.Replace ( ',', '.' );
            f = float.Parse (s, nfi);
            break;
          case CheckType.String:                      // String check
            // Get the string length
            n = s.Length;
            break;
          case CheckType.DateTime:                    // DateTime check
            // Get the DateTime value, if any
            if ( c.GetType().Equals (typeof(System.Windows.Forms.DateTimePicker)) )
              dt = ((DateTimePicker) c).Value;
            else
            {
              System.Globalization.DateTimeFormatInfo dtfi = System.Globalization.DateTimeFormatInfo.InvariantInfo;
              dt = DateTime.Parse ( s, dtfi );
            }
            break;
        }
      
        // CD: Check relation(s)
        Boolean bOK = true;   // In case of an empty CheckRelation item list we suppose validity of the control contents.
        foreach (CheckRelationItem item in list)
        {
          switch ( item.Relation )
          {
            case CheckRelation.GT:
              if      ( ct==CheckType.Int || ct==CheckType.String )    bOK = (n > item.Ref1);
              else if ( ct==CheckType.Float)                           bOK = (f > item.Ref1);  
              else                                                     bOK = true;
              break;
          
            case CheckRelation.GE:
              if      ( ct==CheckType.Int || ct==CheckType.String )    bOK = (n >= item.Ref1);
              else if ( ct==CheckType.Float)                           bOK = (f >= item.Ref1);  
              else                                                     bOK = true;
              break;
                        
            case CheckRelation.LT:
              if      ( ct==CheckType.Int || ct==CheckType.String )    bOK = (n < item.Ref1);
              else if ( ct==CheckType.Float)                           bOK = (f < item.Ref1);  
              else                                                     bOK = true;
              break;
                        
            case CheckRelation.LE:
              if      ( ct==CheckType.Int || ct==CheckType.String )    bOK = (n <= item.Ref1);
              else if ( ct==CheckType.Float)                           bOK = (f <= item.Ref1);  
              else                                                     bOK = true;
              break;
                        
            case CheckRelation.EQU:
              if      ( ct==CheckType.Int || ct==CheckType.String )    bOK = (n == item.Ref1);
              else if ( ct==CheckType.Float)                           bOK = (f == item.Ref1);  
              else                                                     bOK = true;
              break;

            case CheckRelation.IN:
              if      ( ct==CheckType.Int || ct==CheckType.String )    bOK = (n >= item.Ref1 && n <= item.Ref2);
              else if ( ct==CheckType.Float)                           bOK = (f >= item.Ref1 && f <= item.Ref2);  
              else                                                     bOK = true;
              break;
            
            case CheckRelation.NIN:
              if      ( ct==CheckType.Int || ct==CheckType.String )    bOK = !(n >= item.Ref1 && n <= item.Ref2);
              else if ( ct==CheckType.Float)                           bOK = !(f >= item.Ref1 && f <= item.Ref2);  
              else                                                     bOK = true;
              break;
          
            default:
              bOK = true;
              break;
          
          }
          if (bOK)
            break;                                      // Success!
        }
        if ( !bOK )
          throw new ArgumentOutOfRangeException ();     // Check failed!

        // Check OK: Overgive the value
        if      ( ct==CheckType.Int )       o = n;
        else if ( ct==CheckType.Float )     o = f; 
        else if ( ct==CheckType.String )    o = s; 
        else if ( ct==CheckType.DateTime )  o = dt;
      }
      catch 
      {
        // Check NOT OK
        if ( bShowMsg )
        {
          App app = App.Instance;
          Ressources r = app.Ressources;
        
          string sText = Common.GetAdjustedText ((Form)c.TopLevelControl, c);
          string sFmt = r.GetString ("Form_Common_Error_InvalidValue"); // "Ungültiger Wert:\r\n'{0} = {1}'"
          string sMsg = string.Format (sFmt, sText, s);
          string sCap = r.GetString ("Form_Common_TextError");          // "Fehler"
          MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        // Find the TabPage (if any), that contains the erroneous control, and select it (thus making this TabPage visible)
        // (The code snippet of course considers the fact, that a control is maybe not localized within a TabPage) 
        Control cErr = c;
        Control c1;
        while (true)
        {
          c1 = c.Parent;
          if (c1 == c.TopLevelControl) break;
          if (c1 is TabPage)
          {
            ((TabControl)c1.Parent).SelectedTab = (TabPage)c1;
            break;
          }
          c = c1;
        }
        c = cErr;
        // Focus erroneous control
        c.Focus ();
        return false;
      }
      
      // Check OK
      return true;
    }

    #endregion methods

  }

  #endregion Validity check

  #region Password

  /// <summary>
  /// Password management: Usertype enumeration
  /// </summary>
  public enum UserType
  {
    None = 0,
    Customer,
    TrainedCustomer,
    Expert,
  }
  #endregion Password

}
