using System;
using System.Security.Cryptography;
using System.Diagnostics;
using System.Text;
using System.IO;

namespace App
{
  /// <summary>
  /// Class Crypt:
  /// Cryptographics
  /// </summary>
  public class Crypt
  {
    /// <summary>
    /// Constructor
    /// </summary>
    protected Crypt()
    {
    }

    #region RSA

    /// <summary>
    /// Name of the container for RSA key storage
    /// </summary>
    const string _ContainerName = "KeyContainer"; 
  
    /// <summary>
    /// Encrypts data with the RSA algorithm.
    /// </summary>
    /// <param name="arbyDataToEncrypt">The data to encrypt</param>
    /// <returns>The encrypted data</returns>
    public static byte[] RSAEncrypt(byte[] arbyDataToEncrypt)
    {
      try
      {    
        // Create the CspParameters object and set the key container 
        // name used to store the RSA key pair.
        CspParameters cp = new CspParameters();
        cp.KeyContainerName = _ContainerName;

        // Create a new instance of RSACryptoServiceProvider that accesses
        // the named key container.
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(cp);
        
        // Encrypt the byte array (with specified OAEP padding).  
        byte[] arbyEncryptedData = rsa.Encrypt(arbyDataToEncrypt, false);
        
        // Return the encrypted data
        return arbyEncryptedData;
      }
      catch(CryptographicException e)
      {
        string s = string.Format ("RSAEncrypt->Error: {0}", e.Message);
        Debug.WriteLine ( s );
        return null;
      }
    }
    
    /// <summary>
    /// Decrypts data with the RSA algorithm.
    /// </summary>
    /// <param name="arbyDataToDecrypt">The data to decrypt</param>
    /// <returns>The decrypted data</returns>
    public static byte[] RSADecrypt(byte[] arbyDataToDecrypt)
    {
      try
      {
        // Create the CspParameters object and set the key container 
        // name used to store the RSA key pair.
        CspParameters cp = new CspParameters();
        cp.KeyContainerName = _ContainerName;

        // Create a new instance of RSACryptoServiceProvider that accesses
        // the named key container.
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(cp);

        // Decrypt the byte array (with specified OAEP padding).  
        byte[] arbyDecryptedData = rsa.Decrypt(arbyDataToDecrypt, false);

        // Return the decrypted data
        return arbyDecryptedData;
      }
      catch(CryptographicException e)
      {
        string s = string.Format ("RSADecrypt->Error: {0}", e.Message);
        Debug.WriteLine ( s );
        return null;
      }
    } 

    #endregion RSA
    
    #region JM

    /// <summary>
    /// Encode - Plus
    /// </summary>
    static int m_EncodeNumber=100;
    
    /// <summary>
    /// Encrypts data with the JM algorithm.
    /// </summary>
    /// <param name="arbyDataToEncrypt">The data to encrypt</param>
    /// <returns>The encrypted data</returns>
    public static byte[] JMEncrypt(byte[] arbyDataToEncrypt)
    {
      byte[] arbyEncryptedData = EncodeBuffer (arbyDataToEncrypt);
      return arbyEncryptedData;
    }
    
    /// <summary>
    ///  Codieren eines Strings
    /// </summary>
    /// <param name="buffer">normaler (= DEcodierter) String 'buffer' (in) / Codierter String 'buffer' (out)</param>
    static byte[] EncodeBuffer(byte[] buffer)
    {
      int l, i, ic;
   
      l = buffer.Length;
      byte[] arbyEn = new byte[l];
      for (i=0; i < l ;i++) 
      {
        // Lfd. Char
        ic=(int) buffer[i];
        if ( ic < 0 ) ic += 256;	// Bereich: 1 - 255
        // dessen codierter Wert
        ic += m_EncodeNumber;
        ic %= 255;			// Bereich: 0 - 244
        ic++;						// Bereich: 1 - 255
        // codierten Wert 'rückschreiben
        arbyEn[i]=(byte) ic;
      }
      return arbyEn;
    }

    /// <summary>
    /// Decrypts data with the JM algorithm.
    /// </summary>
    /// <param name="arbyDataToDecrypt">The data to decrypt</param>
    /// <returns>The decrypted data</returns>
    public static byte[] JMDecrypt(byte[] arbyDataToDecrypt)
    {
      byte[] arbyDecryptedData = DecodeBuffer (arbyDataToDecrypt);
      return arbyDecryptedData;
    } 

    /// <summary>
    /// DEcodieren eines (vordem per 'EncodeBuffer') codierten Strings
    /// </summary>
    /// <param name="buffer">codierter String 'buffer' (in) / DEcodierter (= normaler) String 'buffer' (out)</param>
    static byte[] DecodeBuffer(byte[] buffer)
    {
      int l, i, ic;

      l = buffer.Length;
      byte[] arbyDe = new byte[l];
      for (i=0; i < l ;i++) 
      {
        // Lfd. Char
        ic=(int) buffer[i];
        if ( ic < 0 ) ic += 256;	// Bereich: 1 - 255
        // dessen DEcodierter Wert
        ic--;
        ic -= m_EncodeNumber;
        ic %= 255;
        if ( ic < 0 ) ic += 255;	// Bereich: 1 - 255
        // DEcodierten Wert 'rückschreiben
        arbyDe[i]=(byte) ic;
      }
      return arbyDe;
    }

    #endregion JM

    #region RC2

    /// <summary>
    /// Key (Note: The # of Key bytes must be within [5, 16])
    /// </summary>
    static byte[] _Key = ASCIIEncoding.ASCII.GetBytes ("Zuckerschnecke");
    /// <summary>
    /// IV (Initialisation vector, Note: The # of IV bytes must be within [5, 16])
    /// </summary>
    static byte[] _IV = ASCIIEncoding.ASCII.GetBytes ("InitVektor");

    /// <summary>
    /// Encrypts data with the RC2 algorithm.
    /// </summary>
    /// <param name="arbyDataToEncrypt">The data to encrypt</param>
    /// <returns>The encrypted data</returns>
    public static byte[] RC2Encrypt (byte[] arbyDataToEncrypt)
    {
      // Get an encryptor.
      RC2CryptoServiceProvider rc2CSP = new RC2CryptoServiceProvider();
      ICryptoTransform encryptor = rc2CSP.CreateEncryptor(_Key, _IV);
                  
      // Encrypt the data.
      MemoryStream msEncrypt = new MemoryStream();
      CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
      
      // Write all data to the crypto stream and flush it.
      csEncrypt.Write(arbyDataToEncrypt, 0, arbyDataToEncrypt.Length);
      csEncrypt.FlushFinalBlock();
      
      // Get encrypted array of bytes.
      byte[] arbyEncryptedData = msEncrypt.ToArray();
      return arbyEncryptedData;
    }

    /// <summary>
    /// Decrypts data with the RC2 algorithm.
    /// </summary>
    /// <param name="arbyDataToDecrypt">The data to decrypt</param>
    /// <returns>The decrypted data</returns>
    public static byte[] RC2Decrypt (byte[] arbyDataToDecrypt)
    {
      // Get a decryptor that uses the same key and IV as the encryptor.
      RC2CryptoServiceProvider rc2CSP = new RC2CryptoServiceProvider();
      ICryptoTransform decryptor = rc2CSP.CreateDecryptor(_Key, _IV);
      
      // Decrypt the data.
      MemoryStream msDecrypt = new MemoryStream(arbyDataToDecrypt);
      CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
      
      // Allocate the decrypted array of bytes.
      byte[] arbyDecryptedData = new byte[arbyDataToDecrypt.Length];
      
      //Read the data out of the crypto stream.
      csDecrypt.Read(arbyDecryptedData, 0, arbyDecryptedData.Length);
      return arbyDecryptedData;
    }

    #endregion RC2

    #region AES

    /// <summary>
    /// AES - Encryption
    /// </summary>
    /// <param name="plainText">The text to be encrypted</param>
    /// <param name="Key">The secret key</param>
    /// <param name="IV">The initialisation vector</param>
    /// <returns>The encrypted byte array</returns>
    public static byte[] AESEncrypt(string plainText, byte[] Key, byte[] IV)
    {
      // Check arguments.
      if (plainText == null || plainText.Length <= 0)
        throw new ArgumentNullException("plainText");
      if (Key == null || Key.Length <= 0)
        throw new ArgumentNullException("Key");
      if (IV == null || IV.Length <= 0)
        throw new ArgumentNullException("Key");

      // Declare the streams used to encrypt to an in memory array of bytes.
      MemoryStream msEncrypt = null;
      CryptoStream csEncrypt = null;
      StreamWriter swEncrypt = null;

      // Declare the Aes object used to encrypt the data.
      Aes aesAlg = null;

      // Declare the bytes used to hold the encrypted data.
      byte[] encrypted = null;

      try
      {
        // Create an Aes object with the specified key and IV.
        aesAlg = Aes.Create();
        aesAlg.Key = Key;
        aesAlg.IV = IV;

        // Create a decryptor to perform the stream transform.
        ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

        // Create the streams used for encryption.
        msEncrypt = new MemoryStream();
        csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
        swEncrypt = new StreamWriter(csEncrypt);

        // Write all data to the stream.
        swEncrypt.Write(plainText);
      }
      finally
      {
        // Clean things up.

        // Close the streams.
        if (swEncrypt != null)
          swEncrypt.Close();
        if (csEncrypt != null)
          csEncrypt.Close();
        if (msEncrypt != null)
          msEncrypt.Close();

        // Clear the Aes object.
        if (aesAlg != null)
          aesAlg.Clear();
      }

      // Get the encrypted bytes from the memory stream.
      encrypted = msEncrypt.ToArray();
      // Return the encrypted bytes.
      return encrypted;
    }

    /// <summary>
    /// AES - Decryption
    /// </summary>
    /// <param name="cipherText">The byte array to be decrypted</param>
    /// <param name="Key">The secret key</param>
    /// <param name="IV">The initialisation vector</param>
    /// <returns>The decrypted string</returns>
    public static string AESDecrypt(byte[] cipherText, byte[] Key, byte[] IV)
    {
      // Check arguments.
      if (cipherText == null || cipherText.Length <= 0)
        throw new ArgumentNullException("cipherText");
      if (Key == null || Key.Length <= 0)
        throw new ArgumentNullException("Key");
      if (IV == null || IV.Length <= 0)
        throw new ArgumentNullException("Key");

      // Declare the streams used to decrypt to an in memory array of bytes.
      MemoryStream msDecrypt = null;
      CryptoStream csDecrypt = null;
      StreamReader srDecrypt = null;

      // Declare the Aes object used to decrypt the data.
      Aes aesAlg = null;

      // Declare the string used to hold the decrypted text.
      string plaintext = null;

      try
      {
        // Create an Aes object with the specified key and IV.
        aesAlg = Aes.Create();
        aesAlg.Key = Key;
        aesAlg.IV = IV;

        // Create a decryptor to perform the stream transform.
        ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

        // Create the streams used for decryption.
        msDecrypt = new MemoryStream(cipherText);
        csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
        srDecrypt = new StreamReader(csDecrypt);

        // Read the decrypted bytes from the decrypting stream
        // and place them in a string.
        plaintext = srDecrypt.ReadToEnd();
      }
      finally
      {
        // Clean things up.

        // Close the streams.
        if (srDecrypt != null)
          srDecrypt.Close();
        if (csDecrypt != null)
          csDecrypt.Close();
        if (msDecrypt != null)
          msDecrypt.Close();

        // Clear the Aes object.
        if (aesAlg != null)
          aesAlg.Clear();
      }

      // Return the decrypted string. 
      return plaintext;
    }

    #endregion AES

  }
}

