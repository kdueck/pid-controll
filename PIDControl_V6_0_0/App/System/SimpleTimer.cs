using System;
using System.Threading;

namespace App
{
	/// <summary>
	/// Class SimpleTimer:
	/// General System.Threading timer.
	/// </summary>
	public class SimpleTimer : IDisposable
	{
    
    #region constructors
    
    /// <summary>
    /// Constructor:
    /// Creates a timer with duetime = 100 msec and period = 100 msec. 
    /// </summary>
    public SimpleTimer()
		{
      // Create the timer
      CreateTimer ();
      // Indicate, that the timer is disabled.
      _bEnabled = false;
    }

    /// <summary>
    /// Constructor:
    /// Creates a timer with the given duetime and period = 100 msec. 
    /// </summary>
    /// <param name="duetime">The duetime (in msec)</param>
    public SimpleTimer (int duetime)
    {
      // Set the duetime (in msec)
      _duetime = duetime;
      // Create the timer
      CreateTimer ();
      // Indicate, that the timer is disabled.
      _bEnabled = false;
    }

    /// <summary>
    /// Constructor:
    /// Creates a timer with given duetime and period. 
    /// </summary>
    /// <param name="duetime">The duetime (in msec)</param>
    /// <param name="period">The period (in msec)</param>
    public SimpleTimer (int duetime, int period)
    {
      // Set the duetime (in msec)
      _duetime = duetime;
      // Set the period (in msec)
      _period = period;
      // Create the timer
      CreateTimer ();
      // Indicate, that the timer is disabled.
      _bEnabled = false;
    }

    #endregion constructors
  
    #region members

    /// <summary>
    /// The timer
    /// </summary>
    Timer _Timer = null;

    /// <summary>
    /// The default duetime (in msec)
    /// </summary>
    int _duetime = 100;

    /// <summary>
    /// The default period (in msec)
    /// </summary>
    int _period = 100;

    /// <summary>
    /// Indicates, whether the timer is enabled (True) or not (False)
    /// </summary>
    bool _bEnabled = false;

    #endregion members
    
    #region methods

    /// <summary>
    /// Creates the timer
    /// </summary>
    void CreateTimer () 
    {
      // Timer parameters:
      //    Timer start (in msec): Dont start
      int duetime = System.Threading.Timeout.Infinite; 
      //    Timer period (in msec)
      int period  = _period;  
                   
      // Create the timer
      TimerCallback timerDelegate = new TimerCallback (_TimerCallback);
      _Timer = new Timer (timerDelegate, null, duetime, period);
    }
  
    /// <summary>
    /// Timer callback 
    /// </summary>
    void _TimerCallback (object state)
    {
      // Trigger the 'Tick' event
      OnTick (new System.EventArgs ());
    }
    
    #endregion methods
  
    #region properties
   
    /// <summary>
    /// Gets the timer duetime (in msec)
    /// </summary>
    public int Duetime
    {
      get { return _duetime; }
    }
    
    /// <summary>
    /// Gets the timer period (in msec)
    /// </summary>
    public int Period
    {
      get { return _period; }
    }

    /// <summary>
    /// Gets or sets whether the timer is running
    /// </summary>
    public bool Enabled
    {
      get 
      {
        return _bEnabled; 
      }
      set 
      {
        _bEnabled = value;                                        // Indicate the corr'ing running state
        // Change timer parameters:
        int duetime = (_bEnabled) ? _duetime : Timeout.Infinite;  // Timer duetime (in msec): Start time / Don't start
        int period  = _period;                                    // Timer period (in msec)
        _Timer.Change (duetime, period);                          // Change
      }
    }
    
    #endregion properties

    #region events

    /// <summary>
    /// The 'Tick' event
    /// </summary>
    public event EventHandler Tick;
    
    /// <summary>
    /// Triggers the 'Tick' event.
    /// </summary>
    /// <param name="e">The event arguments</param>
    protected virtual void OnTick (EventArgs e)
    {
      if (null != Tick)
        Tick (this, e);
    }

    #endregion events

    #region IDisposable Members

    /// <summary>
    /// Disposes the timer ressources
    /// </summary>
    public void Dispose()
    {
      // Dispose the Timeout timer
      if ( null != _Timer ) 
      {
        _Timer.Dispose ();
        _Timer = null;
      }
    }

    #endregion IDisposable Members

  }
}
