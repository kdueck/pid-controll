using System;
using System.Runtime.InteropServices;
using lc_handle_t = System.UInt32;

namespace App
{
	/// <summary>
	/// Class EnkyLC:
  /// Enky LC software protection kit - wrapper class for 32 & 64 bit versions
	/// </summary>
	/// <remarks>
  /// 1.
  ///  The corr'ing DLLs 'lc.dll' (32 bit) and 'LC1.dll' (64 bit) must be placed in the directory of the app's executing file.
  /// 2.
  /// The format of the EnkyLC stuff string is as follows:
  ///   "(Name)\t(Comment)\t(Actions)\t(Usertype)\t(PermissionsSW)\t(Editor)\t(Date_Creation)\t", 
  /// with: 
  ///   Name:           Name of the Dongle owner
  ///   Comment:        Comment
  ///   Actions:        IMS - 1, PID - 2, Combi - 4
  ///   Usertype:       C / T / E
  ///   PermissionSW:   Edit scripts? - 1, Edit service? - 2, Edit extended service? - 4
  ///   Editor:         The person, who edited database and dongle
  ///   Date_Creation:  The creation date of the record
  /// 3.
  /// The administrator and user passwords of the test EnkyLC dongle are (state 4.7.14):
  ///   PW type                       default (initial)                 new
  ///   -------------------------------------------------------------------------------
  ///   admin                         "12345678"                        "*adm5513"
  ///   user                          "12345678"                        "R2D2R2D2"
  /// </remarks>
	public class EnkyLC
	{
		/// <summary>
		/// Constructor
		/// </summary>
    public EnkyLC()
		{
		}
	
    // ******************************************************************************************
    // Basic stuff
    // ******************************************************************************************

    #region LC constants and typedefs

    /// <summary>
    /// Developer ID - Testing 
    /// NOTE: 
    ///   This ID is given & installed on the dongle by the manufacturer. Do not change! 
    /// </summary>
    internal static int DeveloperID_Test = 0x44454D4F;
    /// <summary>
    /// Developer ID - Real life 
    /// NOTE: 
    ///   This ID is given & installed on the dongle by the manufacturer. Do not change! 
    /// </summary>
    internal static int DeveloperID_RealLife = 0x41333733;
    
    // Error Codes
    static public uint LC_SUCCESS                     = 0;  // Successful
    static public uint LC_OPEN_DEVICE_FAILED          = 1;  // Open device failed
    static public uint LC_FIND_DEVICE_FAILED          = 2;  // No matching device was found
    static public uint LC_INVALID_PARAMETER           = 3;  // Parameter Error
    static public uint LC_INVALID_BLOCK_NUMBER        = 4;  // Block Error
    static public uint LC_HARDWARE_COMMUNICATE_ERROR  = 5;  // Communication error with hardware
    static public uint LC_INVALID_PASSWORD            = 6;  // Invalid Password
    static public uint LC_ACCESS_DENIED               = 7;  // No privileges
    static public uint LC_ALREADY_OPENED              = 8;  // Device is open
    static public uint LC_ALLOCATE_MEMORY_FAILED      = 9;  // Allocate memory failed
    static public uint LC_INVALID_UPDATE_PACKAGE      = 10; // Invalid update package
    static public uint LC_SYN_ERROR                   = 11; // thread Synchronization error
    static public uint LC_OTHER_ERROR                 = 12; // Other unknown exceptions
  
    /// <summary>
    /// Hardware information structure
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public struct LC_hardware_info
    {
      public int developerNumber;             // Developer ID
      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
      public byte[] serialNumber;             // Unique Device Serial Number
      public int setDate;                     // Manufacturing date
      public int reservation;                 // Reserve
    }

    /// <summary>
    /// Software information structure
    /// </summary>
    public struct LC_software_info
    {
      public int version;                     // Software edition
      public int reservation;                 // Reserve
    }

    #endregion LC constants and typedefs


    // ******************************************************************************************
    // Enhanced stuff
    // ******************************************************************************************

    #region methods

    /// <summary>
    /// Checks for EnkyLC presence
    /// </summary>
    /// <param name="nInterval_ms">The interval of the timer, from which this routine is invoked</param>
    /// <returns>True, if check ocurred; False otherwise</returns>
    internal static bool Check(int nInterval_ms)
    {
      if (Environment.Is64BitOperatingSystem)
        return EnkyLC64.Check64 (nInterval_ms);
      else
        return EnkyLC32.Check32 (nInterval_ms);
    }

    #endregion methods

    #region members

    /// <summary>
    /// Timer counter
    /// </summary>
    internal static int nTimerCount = 0;

    /// <summary>
    /// Hardware information
    /// </summary>
    internal static LC_hardware_info hwinfo = new LC_hardware_info();

    /// <summary>
    /// The access type (user type), stored on the device
    /// </summary>
    internal static UserType UserType = UserType.None;
    /// <summary>
    /// EnkyLC adjustments: Is the user allowed to edit scripts?
    /// </summary>
    internal static bool bEditScripts = false;
    /// <summary>
    /// EnkyLC adjustments: Is the user allowed to edit service data?
    /// </summary>
    internal static bool bEditSvc = false;
    /// <summary>
    /// EnkyLC adjustments: Is the user allowed to edit extended service data?
    /// </summary>
    internal static bool bEditSvcExt = false;

    #endregion members

  }
}
