using System;
using System.Runtime.InteropServices;


namespace App
{
  /// <summary>
  /// Windows API access:
  /// Access to Win32 API
  /// </summary>
  public sealed class Win
  {
    #region Buzzer
  
    /// <summary>
    /// Soundtypes ( predefined in the registry ) 
    /// </summary>
    public enum BeepType
    {
      SimpleBeep      = -1,
      IconAsterisk    = 0x00000040,
      IconExclamation = 0x00000030,
      IconHand        = 0x00000010,
      IconQuestion    = 0x00000020,
      Ok              = 0x00000000,
    }

    /// <summary>
    /// Plays a waveform sound
    /// </summary>
    /// <param name="beepType">Sound type, as identified by an entry in the registry</param>
    /// <returns>True on success, otherwise false</returns>
    [DllImport("user32.dll")]
    public static extern bool MessageBeep
      ( 
      BeepType beepType // Sound type, as identified by an entry in the registry
      );

    #endregion // Buzzer

  }

}
