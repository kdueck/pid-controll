using System;
using System.Diagnostics;
using Microsoft.Win32;
using System.Windows.Forms;

namespace App
{
	/// <summary>
	/// Class Registry:
	/// Registry handling
	/// </summary>
	/// <remarks>
	/// This is a reengineered version compared to the original ACI version: object type is implemented.
	/// </remarks>
	public class AppRegistry
	{
    #region constructors 	

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="uiMode">Registry access mode</param>
    public AppRegistry( uint uiMode )
    {
      Debug.Assert ( (uiMode == regmodeWrite) || ( uiMode==regmodeRead ) );
  
      // Init.:
      // Access mode
      _uiMode  = uiMode;
      // Registry target
      _hTarget = Registry.CurrentUser;
      // Products registry key
      // Note: 
      //  The corr'ing AssemblyInfo entries are used.
      //  From the ProductVersion are used only the Major and Minor numbers, sofar available.
      string sVersion = Application.ProductVersion;
      string[] ars = sVersion.Split ('.');
      try 
      {
        sVersion = string.Format ("{0}.{1}.{2}", ars[0], ars[1], ars[2]);
      }
      catch {}
      _sKey = string.Format ( @"Software\{0}\{1}\{2}", Application.CompanyName, Application.ProductName, sVersion );
      // Subkey
      _sSubkey = string.Empty;
    }
    
    /// <summary>
    /// Constructor
    /// </summary>
    private AppRegistry()
		{
      _uiMode   = 0;
      _hTarget  = null;
      _sKey     = "";
      _sSubkey  = "";
    }

    #endregion constructors 	
  
    #region constants 	
    
    /// <summary>
    /// Registry access mode: Read
    /// </summary>
    public const int regmodeRead  = 1;
    
    /// <summary>
    /// Registry access mode: Write
    /// </summary>
    public const int regmodeWrite = 2;

    #endregion constants 	

    #region members 	
  
    /// <summary>
    /// Registry access mode
    /// </summary>
    uint _uiMode; 
     
    /// <summary>
    /// Registry target
    /// </summary>
    RegistryKey _hTarget;  
   
    /// <summary>
    /// Registry key
    /// </summary>
    string _sKey;   

    /// <summary>
    ///  Registry subKey with regard to the given Registry key
    /// </summary>
    string _sSubkey;
    
    /// <summary>
    /// Name of value to store data in (Write), resp. Name of the value to retrieve (Read)
    /// </summary>
    string _sValuename;  

    #endregion members 	
  
    #region methods	

    /////////////////////////////////////////////////////////////////
    // Registry - setting section and target

    /// <summary>
    /// Sets the registry access mode
    /// </summary>
    /// <param name="uiMode">The registry access mode</param>
    void SetMode ( uint uiMode )
    {
      Debug.Assert ( (uiMode == regmodeWrite) || (uiMode == regmodeRead) );
      _uiMode   = uiMode;
    }
 
    /// <summary>
    /// Sets the registry target
    /// </summary>
    /// <param name="hTarget">The target</param>
    void SetTarget ( RegistryKey hTarget )
    {
      Debug.Assert  ( 
        ( hTarget == Registry.CurrentUser  ) || 
        ( hTarget == Registry.LocalMachine ) ||
        ( hTarget == Registry.ClassesRoot  ) ||
        ( hTarget == Registry.Users        )
        );
      _hTarget = hTarget;
    } 
  
    /// <summary>
    /// Sets a registry key
    /// </summary>
    /// <param name="szKey">The key</param>
    void SetKey ( string szKey )
    {
      _sKey = szKey;
    } 
  
    /// <summary>
    /// Sets a registry subkey
    /// </summary>
    /// <param name="szSubkey">The subkey</param>
    public void SetSubkey ( string szSubkey )
    {
      _sSubkey = szSubkey;
    }

    /// <summary>
    /// Sets a registry value name
    /// </summary>
    /// <param name="szValuename">The value name</param>
    void SetValuename ( string szValuename )
    {
      _sValuename = szValuename;
    }
  

    /////////////////////////////////////////////////////////////////
    // Registry - read and write operations

    /// <summary>
    /// Reads a value from the registry ( a registry entry ) 
    /// </summary>
    /// <param name="o">The value</param>
    void Read ( ref object o )
    {
      RegistryKey hKey = null;
      try 
      {
        string sCompleteKey = _sKey;
        if ( _sSubkey.Length > 0 ) 
        { 
          sCompleteKey += @"\";  
          sCompleteKey += _sSubkey; 
        }
      
        hKey = _hTarget.OpenSubKey ( sCompleteKey, false );
        if ( null == hKey ) 
        {
          Debug.WriteLine ( "AppRegistry.Read -> OpenSubKey failed." );
          throw new ApplicationException ();
        }
      
        o = hKey.GetValue ( _sValuename ); // o can be of type DWORD, binary, or string.
        if ( null == o ) 
        {
          Debug.WriteLine ( "AppRegistry.Read -> GetValue failed." );
          throw new ApplicationException ();
        }
      }
      catch
      {
        throw;
      }
      finally 
      {
        if ( null != hKey ) 
          hKey.Close ();
      }
    }
  
    /// <summary>
    /// Writes a value to the registry ( a registry entry ) 
    /// </summary>
    /// <param name="o">The value</param>
    void Write ( object o )
    {
      RegistryKey hKey = null;
      try 
      {
        string sCompleteKey = _sKey;
        if ( _sSubkey.Length > 0 ) 
        { 
          sCompleteKey += @"\";  
          sCompleteKey += _sSubkey; 
        }
      
        hKey = _hTarget.CreateSubKey ( sCompleteKey );
        if ( null == hKey ) 
        {
          Debug.WriteLine ( "AppRegistry.Write -> CreateSubKey failed." );
          throw new ApplicationException ();
        }
      
        try 
        {
          hKey.SetValue ( _sValuename, o ); // values allowed can be of type DWORD, binary, or string.
        }
        catch 
        {
          Debug.WriteLine ( "AppRegistry.Write -> SetValue failed." );
          throw new ApplicationException ();
        }
      }
      catch
      {
        throw;
      }
      finally 
      {
        if ( null != hKey ) 
          hKey.Close ();
      }
    }


    /////////////////////////////////////////////////////////////////
    // Registry - access functions

    /// <summary>
    /// Reads / Writes a 'key-value' string from / to the registry 
    /// </summary>
    /// <param name="szValuename">The key string ( value name )</param>
    /// <param name="o">The value</param>
    public void Access ( string szValuename, ref object o )
    {
      SetValuename ( szValuename );  
      
      if ( _uiMode == regmodeRead ) 
        ReadRegistry ( this, ref o ); 
      else
        WriteRegistry ( this, o ); 
    }

    /// <summary>
    /// Reads the registry
    /// </summary>
    /// <param name="reg">The registry</param>
    /// <param name="o">The object to be read</param>
    void ReadRegistry ( AppRegistry reg, ref object o )
    {
      Debug.Assert ( reg != null );

      try
      {
        reg.Read ( ref o );
      }
      catch
      {
        Debug.WriteLine ( "AppRegistry.ReadRegistry failed\n");
        throw;
      }
    }
 
    /// <summary>
    /// Writes the registry
    /// </summary>
    /// <param name="reg">The registry</param>
    /// <param name="o">The object to be written</param>
    void WriteRegistry ( AppRegistry reg, object o )
    {
      Debug.Assert ( reg != null );

      try
      {
        reg.Write ( o );
      }
      catch
      {
        Debug.WriteLine ( "AppRegistry.WriteRegistry failed\n");
        throw;
      }
    }
    
 
    /////////////////////////////////////////////////////////////////
    // Registry - test function
    
    /// <summary>
    /// Test function
    /// </summary>
    public static void Test ()
    {
      string s = string.Empty;
      object o = null;

      try
      {
        AppRegistry reg = new AppRegistry ();

        //-------------
        // Read test -> works, 07.11.07

        reg.SetMode    ( regmodeRead );
        reg.SetTarget  ( Registry.ClassesRoot );
        reg.SetKey     ( @"Excel.Sheet.8\Shell\Open" );
        reg.SetSubkey  ( "Command" );

        reg.Access ( "", ref o );           // returns the default value, here the path of the EXCEL app on the computer 
        s = (string) o;
//        reg.Access ( "xxx", ref o );        // doesn't exist in the 'command' subfolder -> returns an empty string
//        s = (string) o;

        //-------------
        // Write test -> works, 07.11.07

        reg.SetMode    ( regmodeWrite );
        reg.SetTarget  ( Registry.CurrentUser );
        reg.SetKey     ( @"Software\Analytical Control Instruments\Test" );
        reg.SetSubkey  ( "Command" );

        s = "das ist ein Test Kommando";
        o = s;
        reg.Access ( "", ref o );           // Sets the default value
        reg.Access ( "TestValue", ref o );  // Sets the value 'TestValue'
      
        //-------------
        // Write/Read test with byte array -> works, 07.11.07
        
        reg.SetMode    ( regmodeWrite );
        reg.SetTarget  ( Registry.CurrentUser );
        reg.SetKey     ( @"Software\Analytical Control Instruments\Test" );
        reg.SetSubkey  ( "Command" );

        byte[] arby = new byte[] { 0x52, 0x32, 0x44, 0x32 };
        byte[] arbyPassword = Crypt.JMEncrypt (arby);
        o = arbyPassword;
        reg.Access ( "TestByteArray", ref o );     // Sets the value 'TestByteArray'

        reg.SetMode    ( regmodeRead );
        o = null;
        reg.Access ( "TestByteArray", ref o );     // Gets the value 'TestByteArray'
        byte[] arbyPasswordGot = (byte[]) o;
        o = null;

        //-------------
        // Write/Read test with DateTime -> works, 07.11.07

        reg.SetMode    ( regmodeWrite );
        reg.SetTarget  ( Registry.CurrentUser );
        reg.SetKey     ( @"Software\Analytical Control Instruments\Test" );
        reg.SetSubkey  ( "Command" );

        DateTime dt = DateTime.Now;                // Write
        o = dt.ToString ();
        reg.Access ( "TestDateTime", ref o );

        reg.SetMode    ( regmodeRead );            // Read
        o = null;
        reg.Access ( "TestDateTime", ref o );
        DateTime dtGot = DateTime.Parse ( ( string ) o );
        o = null;

        //-------------
        // Write/Read test with Enumeration -> works, 07.11.07

        reg.SetMode    ( regmodeWrite );
        reg.SetTarget  ( Registry.CurrentUser );
        reg.SetKey     ( @"Software\Analytical Control Instruments\Test" );
        reg.SetSubkey  ( "Command" );

        AppData.LanguageData l = new AppData.LanguageData (); // Write
        l.eLanguage = AppData.LanguageData.Languages.de;
        o = l.eLanguage.ToString ();
        reg.Access ( "TestEnumeration", ref o );

        reg.SetMode    ( regmodeRead );                       // Read
        o = null;
        reg.Access ( "TestEnumeration", ref o );
        l.eLanguage = ( AppData.LanguageData.Languages ) Enum.Parse ( typeof( AppData.LanguageData.Languages ), (string) o );
        o = null;

        //-------------
        // Write/Read test with UInt32 -> works, 07.11.07

        reg.SetMode    ( regmodeWrite );
        reg.SetTarget  ( Registry.CurrentUser );
        reg.SetKey     ( @"Software\Analytical Control Instruments\Test" );
        reg.SetSubkey  ( "Command" );

        UInt32 dw = UInt32.MaxValue / 2;           // Write
        o = dw.ToString ();
        reg.Access ( "TestUInt32", ref o );

        reg.SetMode    ( regmodeRead );            // Read
        o = null;
        reg.Access ( "TestUInt32", ref o );
        UInt32 dwGot = UInt32.Parse ( ( string ) o );
        o = null;
      }
      catch (Exception exc)
      {
        string s1 = string.Format ("AppRegistry.Test failed\nError: {0}\n", exc.Message );
        Debug.WriteLine ( s1 );
      }
    }

    #endregion methods	

  }
}
