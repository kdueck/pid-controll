Notes:
------

1.	The #ziplib (SharpZipLib, formerly NZipLib) is a Zip, GZip, Tar and BZip2 library written 
	entirely in C# for the .NET platform. It is implemented as an assembly (installable in the GAC),
	and thus can easily be incorporated into other projects (in any .NET language).
	
	The source code is taken (i.e. downloaded) from www.icsharpcode.net.

2.	In the SharpZipLib code the following defines are used:
		NET_1_0		
		NET_1_1	
		NETCF_1_0
		NETCF_2_0
	These defines specify the underlaying .NET Framework the app is using.
	Depending on the .NET Framework the corr'ing define must be included	
	into the Build/Conditional Compilation Constants section of the projects properties
	(for both Debug and Release modi).
	
	
	