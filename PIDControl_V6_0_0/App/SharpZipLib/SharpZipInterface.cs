// SharpZipInterface.cs
//
// Copyright 02/2008 Joachim Matz

using System;
using System.IO;
using System.Collections;
using ICSharpCode.SharpZipLib.Zip;

namespace ICSharpCode.SharpZipLib
{
  
  /// <summary>
  /// Delegate declaration: EntryUnzipped event handler
  /// </summary>
  public delegate void EntryUnzippedEventHandler(object sender, EntryUnzippedEventArgs e);

  /// <summary>
  /// Class EntryUnzippedEventArgs:
  /// Class that contains the data for the EntryUnzipped event. Derives from System.EventArgs.
  /// </summary>
  public class EntryUnzippedEventArgs : EventArgs 
  {  
    // The name of the file curr'ly unzipped
    string sFileName;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="sFileName">The name of the file curr'ly unzipped</param>
    public EntryUnzippedEventArgs (string sFileName) 
    {
      this.sFileName = sFileName;
    }
      
    /// <summary>
    /// The name of the file curr'ly unzipped
    /// </summary>
    public string FileName
    {
      get {return sFileName;}
    }
  }
  
  /// <summary>
	/// Class SharpZipInterface:
	/// Interface (Wrapper) for the SharpZipLib classes
	/// </summary>
	public class SharpZipInterface
	{
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public SharpZipInterface()
		{
		}
	
    #endregion constructors

    #region constants & enums

    /// <summary>
    /// Zip/Unzip notification enumeration
    /// </summary>
    public enum Notification
    {
      ZipSuccess,             // Zip file was created.
      UnzipSuccess,           // Zip file was unzipped.
      NoexistingDirectory,    // Directory (Zip: source dir, Unzip: dest. dir) doesn't exist.
      EmptyZipFilePath,       // No Zip file specified.
      NoexistingZipFile,      // Zip file doesn't exist.
      ZipError,               // An error ocurred during Zip processing.
      UnzipError              // An error ocurred during UnZip processing.
    };

    #endregion constants & enums

    #region members
    #endregion members

    #region methods
  
    /// <summary>
    /// Zips the files, contained in a specified directory, into a zip file.
    /// </summary>
    /// <param name="sDirectory">The directory, whose files should be zipped</param>
    /// <param name="sZipFilePath">The zip file path</param>
    /// <returns>The Zip/Unzip notification</returns>
    public Notification Zip (string sDirectory, string sZipFilePath)
    {
      // Check: Does the directory exist?
      if (!Directory.Exists(sDirectory))
      {
        return Notification.NoexistingDirectory; // No.
      }
      string[] filenames = Directory.GetFiles( sDirectory );
      return Zip (filenames, sZipFilePath);
    }

    /// <summary>
    /// Zips an array of files into a zip file.
    /// </summary>
    /// <param name="filenames">The array of files to be zipped</param>
    /// <param name="sZipFilePath">The zip file path</param>
    /// <returns>The Zip/Unzip notification</returns>
    public Notification Zip (string[] filenames, string sZipFilePath)
    {
      // Check: Zip file path specified?
      if (sZipFilePath.Length == 0)
        return Notification.EmptyZipFilePath; // No
      // Check: Does the array of files to be zipped contain the zip file path ?
      int idx = Array.IndexOf (filenames, sZipFilePath);
      if (idx != filenames.GetLowerBound(0)-1)
      {
        // Yes: Remove the zip file path from the array
        ArrayList al = new ArrayList (filenames);
        al.Remove (sZipFilePath);
        filenames = (string[]) al.ToArray (typeof(string));
      }

      // Zip
      try 
      {
        // Check: Are there any files of size > 4GB?
        // (The 4GB size is the maximum size for a 'normal' 32bit zip: 4GB = 2^32 Byte.
        //  If there are files with sizes exceeding this limit, then Zip64 extensions must be used 
        //  for zipping.
        //  See remarks for 'UseZip64'.)
        Boolean bUseZip64 = false;
        foreach (string file in filenames) 
        {
          FileInfo fi = new FileInfo ( file );
          if ( fi.Length > 4294967296 ) // = 2^32 Byte
          {
            // Yes
            bUseZip64 = true;
            break;
          }
        }

        // Create an deflater output stream that writes the files into a zip archive one after another. 
        // ('using' statements guarantee the stream is closed properly which is a big source
        //  of problems otherwise. Its exception safe as well which is great.)
        using (ZipOutputStream s = new ZipOutputStream(File.Create(sZipFilePath))) 
        {
          // Set the Compression level: 0 - store only ... 9 - best compression
          s.SetLevel(9);
          // Zip64 forcing ( Dynamic (default) vs. Off ) DEPENDING on the file sizes
          // (See remarks for 'UseZip64')
          if ( !bUseZip64 ) s.UseZip64 = UseZip64.Off;
          // The fixed size buffer
          byte[] buffer = new byte[4096];
          // Step through the files
          foreach (string file in filenames) 
          {
            // Create a new zip entry for the current file
            // (Using GetFileName makes the result compatible with XP
            //  as the resulting path is not absolute.)
            ZipEntry entry = new ZipEntry(Path.GetFileName(file));
            // Setup the entry data as required
            // (Crc and size are handled by the library for seakable streams
            //  so no need to do them here.)
            //    Set the entry modification time
            FileInfo fi = new FileInfo (file);
            entry.DateTime = fi.LastWriteTime;
            // Start the zip entry
            s.PutNextEntry(entry);
            // Open the file for reading
            using (FileStream fs = File.OpenRead(file)) 
            {
              int sourceBytes;
              do 
              {
                // Read a block of bytes ...
                sourceBytes = fs.Read(buffer, 0, buffer.Length);
                // ... & write them to the zip entry
                s.Write(buffer, 0, sourceBytes);
              } while ( sourceBytes > 0 );
            }
          }
          // Finish the stream
          // (Finish is important to ensure trailing information for a Zip file is appended.  Without this
          //  the created file would be invalid.)
          s.Finish();
          // Close the stream
          // (Close is important to wrap things up and unlock the file.)
          s.Close();
          // Done
          return Notification.ZipSuccess;
        }
      }
      catch 
      {
        return Notification.ZipError;
      }
 
    }

    /// <summary>
    /// Unzips the files, contained in a zip file, into a specified directory.
    /// </summary>
    /// <param name="sZipFilePath">The zip file path</param>
    /// <param name="sDirectory">The directory, the unzipped files should be written into</param>
    /// <returns>The Zip/Unzip notification</returns>
    public Notification Unzip (string sZipFilePath, string sDirectory)
    {
      // Check: Does the Zip file exist?
      if (!File.Exists(sZipFilePath)) 
        return Notification.NoexistingZipFile; // No
      // Check: Does the directory exist?
      if (!Directory.Exists(sDirectory)) 
        return Notification.NoexistingDirectory; // No.

      // UnZip
      try 
      {
        // Create an Inflater input stream that reads the files from an zip archive one after another.
        using (ZipInputStream s = new ZipInputStream(File.OpenRead(sZipFilePath))) 
        {
          ZipEntry entry;
          // Step through the Zip entries
          while ((entry = s.GetNextEntry()) != null) 
          {
            // Current Zip entry:
            
            // Build the filepath, the unzipped files should be stored wherein
            string directoryName = sDirectory;
            string fileName      = Path.GetFileName(entry.Name);
            string filePath      = Path.Combine (directoryName, fileName);
            // Create the directory, if req.
            if (directoryName.Length > 0) 
              Directory.CreateDirectory(directoryName);
            // Unzip
            if (filePath != string.Empty) 
            {
              // Create the file the unzipped data should be written into
              using (FileStream streamWriter = File.Create(filePath)) 
              {
                int nBytes;
                byte[] buffer = new byte[4096];
                while (true) 
                {
                  // Read a block of bytes from the input stream ...
                  nBytes = s.Read(buffer, 0, buffer.Length);
                  if (nBytes == 0) break;
                  // ... & write them to the file
                  streamWriter.Write(buffer, 0, nBytes);
                }
              }
              // Set the file's LastWriteTime corr'ing to the DateTime member of the Zip entry
              File.SetLastWriteTime (filePath, entry.DateTime);
              // Raise the EntryUnzipped event
              OnEntryUnzipped (new EntryUnzippedEventArgs (fileName));
            }
          }
        }
        // Done
        return Notification.UnzipSuccess;
      }
      catch
      {
        return Notification.UnzipError;
      }
    }
   
    #endregion methods

    #region events

    /// <summary>
    /// EntryUnzipped:
    /// The event member that is of type EntryUnzippedEventHandler.
    /// </summary>
    public event EntryUnzippedEventHandler EntryUnzipped;

    /// <summary>
    /// The protected OnEntryUnzipped method raises the EntryUnzipped event by invoking the delegates. 
    /// The sender is always this, the current instance of the class.
    /// </summary>
    /// <param name="e"> The data for the EntryUnzipped event</param>
    protected virtual void OnEntryUnzipped (EntryUnzippedEventArgs e)
    {
      if (EntryUnzipped != null) 
      {
        // Invoke the delegates. 
        EntryUnzipped(this, e);
      }
    }

    #endregion events

  }
}
