using System;
using System.Diagnostics;

namespace App
{
  /// <summary>
  /// Class RessourcesBase: 
  /// Base class for resource management
  /// </summary>
  /// <remarks>
  /// In order to be able to differentiate between the (system) namespace 
  /// 'System.Resources' and this class we write it with 'ss'. 
  /// </remarks>
  public abstract class RessourcesBase
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="baseName">The root name of the resources</param>
    /// <param name="asm">The assembly where the resources are located</param>
    public RessourcesBase ( string baseName, System.Reflection.Assembly asm )
    {
      try 
      {
        // Language ( by configuration settings )
        App app = App.Instance;
        string sLanguage = app.Data.Language.eLanguage.ToString ( "F" );
        // Instantiate culture
        _ci = new System.Globalization.CultureInfo ( sLanguage );
        // Instantiate resource manager        
        _rm = new System.Resources.ResourceManager ( baseName, asm );
      }
      catch ( System.Exception exc )
      {
        Debug.WriteLine ( exc.Message );
      }
    }
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="baseName">The root name of the resources</param>
    /// <param name="asm">The assembly where the resources are located</param>
    /// <param name="sLanguage">The language specifying the culture</param>
    public RessourcesBase ( string baseName, System.Reflection.Assembly asm, string sLanguage )
    {
      try 
      {
        // Instantiate culture
        _ci = new System.Globalization.CultureInfo ( sLanguage );
        // Instantiate resource manager        
        _rm = new System.Resources.ResourceManager ( baseName, asm );
      }
      catch ( System.Exception exc )
      {
        Debug.WriteLine ( exc.Message );
      }
    }

    #endregion // constructors

    #region members

    /// <summary>
    /// Resource manager
    /// </summary>
    private System.Resources.ResourceManager _rm = null;

    /// <summary>
    /// Cultureinfo
    /// </summary>
    private System.Globalization.CultureInfo _ci = null;

    #endregion members

    #region methods

    /// <summary>
    /// Gets the value of the String resource localized for the specified culture.
    /// </summary>
    /// <param name="key">The name of the resource to get.</param>
    /// <returns>The value of the resource localized for the specified culture.</returns>
    public string GetString ( string key ) 
    {
      return _rm.GetString ( key, _ci );
    }

    /// <summary>
    /// Updates the Cultureinfo member
    /// </summary>
    /// <param name="sLanguage">The language</param>
    public void UpdateCulture ( string sLanguage ) 
    {
      // Re-Instantiate Cultureinfo
      try 
      {
        if ( _ci.TwoLetterISOLanguageName != sLanguage ) 
        {
          _ci = new System.Globalization.CultureInfo ( sLanguage );
        }
      }
      catch ( System.Exception exc )
      {
        Debug.WriteLine ( exc.Message );
      }
    }
    
    #endregion methods

    #region properties

    /// <summary>
    /// Resource manager
    /// </summary>
    public System.Resources.ResourceManager Manager 
    {
      get { return _rm; }
    }

    /// <summary>
    /// Cultureinfo
    /// </summary>
    public System.Globalization.CultureInfo Culture 
    {
      get { return _ci; }
    }

    #endregion properties

  }
}
