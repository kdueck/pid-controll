using System;

namespace App
{
  /// <summary>
  /// Class Ressources: 
  /// Resource management
  /// </summary>
  /// <remarks>
  /// In order to be able to differentiate between the (system) namespace 
  /// 'System.Resources' and this class we write it with 'ss'. 
  /// </remarks>
  public class Ressources : RessourcesBase
  {
    /// <summary>
    /// Constructor
    /// </summary>
    public Ressources () : 
      base ( "App.Ressources.Strings", System.Reflection.Assembly.GetExecutingAssembly() )
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="sLanguage">The desired language</param>
    public Ressources ( string sLanguage) : 
      base ( "App.Ressources.Strings", System.Reflection.Assembly.GetExecutingAssembly(), sLanguage )
    {
    }
  
  }
}
