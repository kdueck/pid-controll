Betr.: DLL's
============

Die DLL's werden f�r die Einbindung des EnkyLC-Dongles in die PC-SW ben�tigt, und sind wie folgt spez.:

1) lc.dll: 
- Diese DLL wird f�r 32bit Windows ben�tigt (z.B. XP)
- Verzeichnis auf der EnkyLC-CD: Enky LC2\api\DLL 
- liegt dort als 'lc.dll', und ist mit diesem Namen in die PC-SW eingebunden worden

2) LC1.dll:
- Diese DLL wird f�r 64bit Windows ben�tigt (z.B. Win7, 64bit)
- Verzeichnis auf der EnkyLC-CD: Enky LC2\api\LC_64_bit_dll 
- liegt dort als 'LC.dll', und ist f�r die Einbindung in die PC-SW von mir in 'LC1.dll' umbenannt worden
- diese DLL erwies sich im Test am 8.3.16 als universell geeignet im Vgl. mit 3) und wird daher zuk�nftig verwendet

3) LC1.old:
- Diese DLL (mit der Erweiterung .dll) wurde urspr�nglich f�r 64bit Windows ben�tigt (z.B. Win7, 64bit)
- Verzeichnis auf der EnkyLC-CD: Enky LC2\api\VS2010 64 bit dll&lib\64bitVS2010 dll&lib 
- liegt dort als 'LC1.dll', und ist mit diesem Namen in die PC-SW eingebunden worden
- diese DLL erwies sich im Test am 8.3.16 als ungeeignet auf dem HP-Laptop von JL und wird daher zuk�nftig NICHT mehr verwendet
  (deshalb die Umbenennung in '.old'). 


JM,
8.3.16
