using System;
using System.Windows.Forms;

namespace App
{
	/// <summary>
	/// Class StartUp:
	/// Applications entry point 
	/// </summary>
	public sealed class StartUp
	{

    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main() 
    {
      try 
      {
        MainForm mf = new MainForm ();
        Application.Run ( mf );
      }
      catch (System.Exception exc)
      {
        App app = App.Instance;
        Ressources r = app.Ressources;
        string fmt = r.GetString ("StartUp_Error");           // "The following error ocurred:\r\n\r\n{0}\r\n\r\nThe application will be closed."
        string sMsg = string.Format (fmt, exc.Message);
        string sCap = r.GetString ("Form_Common_TextError");  // "Fehler"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }
  
  }
}
