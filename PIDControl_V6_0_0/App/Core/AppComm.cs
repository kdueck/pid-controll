using System;
using System.Diagnostics;
using System.Collections;
using System.Text;
using System.Windows.Forms;

using CommRS232;

namespace App
{
	/// <summary>
	/// Class AppComm:
	/// Communication
	/// </summary>
	public class AppComm : CommRS232.CommRS232
	{
    #region constructors
    
    /// <summary>
    /// Constructor
    /// </summary>
    internal AppComm() : base ()
    {
    }

    #endregion // constructors

    #region constants & enums
    #endregion // constants & enums

    #region event handling
    #endregion event handling

    #region members
    
    //---------------------------------------------      
    // General Messages
    //---------------------------------------------      

    public CommMessage  msgVersion          = new CommMessage ( "VER" );
    public CommMessage  msgTransferStart    = new CommMessage ( "TRNSTART" );
    public CommMessage  msgTransferComplete = new CommMessage ( "TRNCOMPLETE" );
    public CommMessage  msgLanguageSynchro  = new CommMessage ( "LANGSYN" );

    //---------------------------------------------      
    // Scan Messages
    //---------------------------------------------      

    public CommMessage  msgScanParams       = new CommMessage ( "SCANPARAMS" );
    public CommMessage  msgScanAllData      = new CommMessage ( "SCANALLDATA" );

    //---------------------------------------------      
    // Script Messages
    //---------------------------------------------      

    public CommMessage  msgScriptClear      = new CommMessage ( "SCRCLEAR" );
    public CommMessage  msgScriptUserID     = new CommMessage("SCRUSERID");
    public CommMessage  msgScriptFilename   = new CommMessage("SCRFILENAME");
    public CommMessage  msgScriptOpen       = new CommMessage ( "SCROPEN" );
    public CommMessage  msgScriptParameter  = new CommMessage ( "SCRPARAM" );
    public CommMessage  msgScriptList       = new CommMessage ( "SCRLIST" );
    public CommMessage  msgScriptWindow     = new CommMessage ( "SCRWINDOW" );
    public CommMessage  msgScriptEvent      = new CommMessage ( "SCREVENT" );
    public CommMessage  msgScriptClose      = new CommMessage ( "SCRCLOSE" );
    public CommMessage  msgScriptComplete   = new CommMessage ( "SCRCOMPLETE" );

    public CommMessage  msgScriptSelect     = new CommMessage ( "SCRSELECT" );
    public CommMessage  msgScriptCurrent    = new CommMessage ( "SCRCURRENT" );
    public CommMessage  msgScriptStart      = new CommMessage ( "SCRSTART" );
    public CommMessage  msgScriptStop       = new CommMessage ( "SCRSTOP" );
    public CommMessage  msgScriptReset      = new CommMessage ( "SCRRESET" );
               
    //---------------------------------------------      
    // KEY triggering messages
    //---------------------------------------------      

    public CommMessage  msgTriggerKeyCmd    = new CommMessage ( "KEYTRIG" );

    //---------------------------------------------      
    //    GSMReader dialog
    //---------------------------------------------      
    
    // Data storage messages: Eeprom-Data
    public CommMessage  msgStoreEDCount     = new CommMessage ( "STREDCOUNT" );
    public CommMessage  msgStoreEDItem      = new CommMessage ( "STREDITEM" );
    public CommMessage  msgStoreEDHeader    = new CommMessage ( "STREDHEADER" );
    public CommMessage  msgStoreEDClear     = new CommMessage ( "STREDCLEAR" );
    
    // Data storage messages: Eeprom-ScriptInfo
    public CommMessage  msgStoreESOpen        = new CommMessage ( "STRESOPEN" );
    public CommMessage  msgStoreESParameter   = new CommMessage ( "STRESPARAM" );
    public CommMessage  msgStoreESWindowCount = new CommMessage ( "STRESWINCNT" );
    public CommMessage  msgStoreESWindow      = new CommMessage ( "STRESWIN" );
    public CommMessage  msgStoreESEventCount  = new CommMessage ( "STRESEVTCNT" );
    public CommMessage  msgStoreESEvent       = new CommMessage ( "STRESEVT" );

    //---------------------------------------------      
    //    Service dialog
    //---------------------------------------------      

    // Service messages
    public CommMessage  msgServiceReadData      = new CommMessage ( "SVCREADDATA" );
    public CommMessage  msgServiceReadCmtStart  = new CommMessage ( "SVCREADCMT_ST" );
    public CommMessage  msgServiceReadCmt       = new CommMessage ( "SVCREADCMT" );
    public CommMessage  msgServiceWriteData     = new CommMessage ( "SVCWRITEDATA" );
    public CommMessage  msgServiceWriteCmtStart = new CommMessage ( "SVCWRITECMT_ST" );
    public CommMessage  msgServiceWriteCmt      = new CommMessage ( "SVCWRITECMT" );

    //---------------------------------------------      
    //    SDcardReader dialog
    //    Notes:
    //      The 'Empty folder' routines for the 'Data/Alarm/Script/Scan' root folders, implemented in the 
    //      device SW ("EYDATFO", "EYALFO", "EYSCRFO", "EYSCNFO"), are NOT used from within this PC SW. 
    //      Instead of the 'REMFO' cmd is used for removing operations within these root folders.
    //      On the other hand, for removing operations within the 'Service' root folder the 'Empty folder' 
    //      routine for the 'Service' root folder ("EYSVCFO") is used.
    //      Reason: 
    //      'Data/Alarm/Script/Scan' root folders use subfolders. Here the REMFO-implementation of the 
    //      device SW works fine: The root folder themselves are not cleared.
    //      The 'Service' root folder doesn't use subfolders. Here the REMFO-implementation of the 
    //      device SW doesn't work: The root folder itself would be cleared. Thats why EYSVCFO is used here. 
    //---------------------------------------------      

    // SDcard storage messages
    //  Get the path of the device Data root folder
    public CommMessage  msgReadDataFolder         = new CommMessage ( "RDDATFO" );
    //  Get the path of the device Alarm root folder
    public CommMessage  msgReadAlarmFolder        = new CommMessage ( "RDALFO" );
    //  Get the path of the device Script root folder
    public CommMessage  msgReadScriptFolder       = new CommMessage ( "RDSCRFO" );
    //  Get the path of the device Scan root folder
    public CommMessage  msgReadScanFolder         = new CommMessage ( "RDSCNFO" );
    //  Get the path of the device Service root folder
    public CommMessage  msgReadServiceFolder      = new CommMessage ( "RDSVCFO" );

    //  Get (sub)folders of the device Data folder
    public CommMessage  msgReadDataFolders        = new CommMessage ( "RDDATFOS" );
    //  Get (sub)folders of the device Alarm folder
    public CommMessage  msgReadAlarmFolders       = new CommMessage ( "RDALFOS" );
    //  Get (sub)folders of the device Scan folder
    public CommMessage  msgReadScanFolders        = new CommMessage ( "RDSCNFOS" );
    //  Get (sub)folders of the device Script folder
    public CommMessage  msgReadScriptFolders      = new CommMessage ( "RDSCRFOS" );
    
    //  Get files of the current device Data subfolder
    public CommMessage  msgReadDataFiles          = new CommMessage ( "RDDATFI" );
    //  Get files of the current device Alarm subfolder
    public CommMessage  msgReadAlarmFiles         = new CommMessage ( "RDALFI" );
    //  Get files of the device Script folder
    public CommMessage  msgReadScriptFiles        = new CommMessage ( "RDSCRFI" );
    //  Get files of the device Scan folder
    public CommMessage  msgReadScanFiles          = new CommMessage ( "RDSCNFI" );
    //  Get files of the device Service folder
    public CommMessage  msgReadServiceFiles       = new CommMessage ( "RDSVCFI" );
    
    //  Get file contents of a device text (Data/Script/Scan) file
    public CommMessage  msgReadTextFileContents   = new CommMessage ( "RDTFI" );
    //  Get file contents of a device binary file
    public CommMessage  msgReadBinaryFileContents = new CommMessage ( "RDBFI" );
    
    // Empty the Service root folder
    public CommMessage  msgEmptyServiceFolder     = new CommMessage ( "EYSVCFO" );
    //  Remove a device folder
    public CommMessage  msgRemoveFolder           = new CommMessage ( "REMFO" );
    //  (Re)initialize the disk - Start
    public CommMessage msgInitDisk_Start          = new CommMessage ( "INDSK_S" );
    //  (Re)initialize the disk - Perform
    public CommMessage msgInitDisk_Perform        = new CommMessage ( "INDSK_P" );
    
    //---------------------------------------------      
    //    SDcardReaderExt dialog
    //    Notes:
    //      The 'Flow' & 'Error' root folders are treated analogically with the 'Service' root folder.
    //---------------------------------------------      
    
    //  Get the path of the device Flow root folder
    public CommMessage  msgReadFlowFolder         = new CommMessage ( "RDFLWFO" );
    //  Get files of the device Flow folder
    public CommMessage  msgReadFlowFiles          = new CommMessage ( "RDFLWFI" );
    // Empty the Flow root folder
    public CommMessage  msgEmptyFlowFolder        = new CommMessage ( "EYFLWFO" );
  
    //  Get the path of the device Error root folder
    public CommMessage  msgReadErrorFolder        = new CommMessage ( "RDERRFO" );
    //  Get files of the device Error folder
    public CommMessage  msgReadErrorFiles         = new CommMessage ( "RDERRFI" );
    // Empty the Error root folder
    public CommMessage  msgEmptyErrorFolder       = new CommMessage ( "EYERRFO" );

    // --------------------------
    // Sdcard reinitialisations
    // --------------------------

    // Get the # of SDcard reinitialisations
    public CommMessage msgSDGetReinitNo           = new CommMessage ("SDGETRIN");
    // Reset the # of SDcard reinitialisations
    public CommMessage msgSDResetReinitNo         = new CommMessage ("SDRESETRIN");
    
    //---------------------------------------------      
    //  CombiNG embedding Messages
    //---------------------------------------------      

    // Get the device type (ONLY the CombiNG SW)
    public CommMessage  msgDeviceType             = new CommMessage ( "DEVTYPE" );
    // Set the DateTime on the device
    public CommMessage  msgDateTime               = new CommMessage ( "DATI" );
    // Remote error confirmation
    public CommMessage  msgErrConf                = new CommMessage ( "ERRCONF" );

    //---------------------------------------------      
    // Clock-timed script Messages
    //---------------------------------------------      

    // Read
    public CommMessage  msgCTSRead                = new CommMessage ( "CTSREAD" );
    // Write
    public CommMessage  msgCTSWrite               = new CommMessage ( "CTSWRITE" );
    
    //---------------------------------------------      
    // MP
    //---------------------------------------------      

    // Read
    public CommMessage  msgServiceMPRead          = new CommMessage ( "MPREAD" );
    // Read
    public CommMessage  msgServiceMPWrite         = new CommMessage ( "MPWRITE" );
 
    //---------------------------------------------      
    // Error handling messages
    //---------------------------------------------      

    // Read
    public CommMessage  msgEHRead                 = new CommMessage ( "EHREAD" );
    // Write
    public CommMessage  msgEHWrite                = new CommMessage ( "EHWRITE" );

    //---------------------------------------------      
    // User ID's on device Eeprom
    //---------------------------------------------      

    public CommMessage msgGetUserIDs              = new CommMessage("GETUSERIDS");

  
    /// <summary>
    /// The 'Set' messages array:
    /// These messages return no information from device, but only an 'OK' answer.
    /// </summary>
    /// <remarks>
    /// Currently not needed.
    /// </remarks>
    public static string[] SetCmds = new string[] 
    { 
      "LANGSYN",
      "SCRCLEAR",
      "SCRFILENAME",
      "SCROPEN",
      "SCRPARAM",
      "SCRWINDOW", 
      "SCREVENT",
      "SCRCLOSE",
      "SCRCOMPLETE",
      "SCRSELECT",
      "SCRSTART",
      "SCRSTOP",
      "SCRRESET",
      "TRNSTART",
      "TRNCOMPLETE",
      "STREDCLEAR",
      "SVCWRITEDATA",
      "SVCWRITECMT_ST",
      "SVCWRITECMT",
      "MPWRITE",
      "KEYTRIG",
      "EYDATFO",
      "EYALFO",
      "EYSCNFO",
      "EYSCRFO",
      "EYSVCFO",
      "EYERRFO",
      "REMFO",
      "INDSK_S",
      "INDSK_P",
      "EYFLWFO",
      "DATI",
      "ERRCONF"
    };
    
    #endregion //  members

    #region methods
    
    /// <summary>
    /// Opens the communication.
    /// </summary>
    public void Open ()
    {
      App app = App.Instance;
      AppData appData = app.Data;
      string sChannel = appData.Communication.sCommChannel;
      UInt32 dwBaudrate = appData.Communication.dwBaudRate;

      try
      {
        if ( !IsOpen() )
        {
          OpenChannel ( sChannel, dwBaudrate );
        }
      }
      catch 
      {
        string s = string.Format ( "Communication channel open failed on {0}.\n", sChannel );
        Debug.WriteLine (s);
        throw;
      }
    }  
  
    /// <summary>
    /// Closes the communication.
    /// </summary>
    public void Close ()
    {
      if ( !IsClosed() )
        CloseChannel ();
    }
  
    #endregion methods

    #region properties
    #endregion properties

  }
}
