using System;
using System.Windows.Forms;

namespace App
{
	/// <summary>
	/// Class App:
	/// The application class
	/// </summary>
	public class App
	{
    #region Singleton
    
    /// <summary>
    /// The one and only application instance
    /// </summary>
    public static App Instance 
    {
      get 
      {
        if ( null == _instance )
          _instance = new App ();
        return _instance;
      }
    }
    static App _instance = null;

    #endregion // Singleton

    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public App()
		{
      // Initiate the resource manager
      string sLanguage = this.Data.Language.eLanguage.ToString ( "F" );
      _resources = new Ressources ( sLanguage );
    }

    #endregion // constructors

    #region private members

    /// <summary>
    /// The MainForm instance
    /// </summary>
    MainForm _frmMain = null;

    /// <summary>
    /// The application data instance
    /// </summary>
    AppData _data = new AppData ();

    /// <summary>
    /// The Document instance
    /// </summary>
    Doc _doc = new Doc ();
    
    /// <summary>
    /// The communication instance
    /// </summary>
    AppComm _comm = new AppComm ();

    /// <summary>
    /// The measurement instance
    /// </summary>
    Measure _measure = new Measure ();

    /// <summary>
    /// The View instance
    /// </summary>
    Vw _vw = new Vw ();

    /// <summary>
    /// The command UI
    /// </summary>
    CmdUI _cmdUI = new CmdUI ();
    
    /// <summary>
    /// The resource manager instance
    /// </summary>
    Ressources _resources = null;
    
    #endregion // private members
    
    #region properties

    /// <summary>
    /// The MainForm
    /// </summary>
    public MainForm MainForm 
    {
      get { return _frmMain; }
      set { _frmMain = value; }
    }

    /// <summary>
    /// The application data
    /// </summary>
    public AppData Data 
    {
      get { return _data; }
    }

    /// <summary>
    /// The Document
    /// </summary>
    public Doc Doc 
    {
      get { return _doc; }
    }

    /// <summary>
    /// The communication
    /// </summary>
    public AppComm Comm 
    {
      get { return _comm; }
    }

    /// <summary>
    /// The measurement
    /// </summary>
    public Measure Measurement 
    {
      get { return _measure; }
      set { _measure = value; }
    }

    /// <summary>
    /// The view
    /// </summary>
    public Vw View 
    {
      get { return _vw; }
    }

    /// <summary>
    /// The command UI
    /// </summary>
    public CmdUI CmdUI 
    {
      get { return _cmdUI; }
    }

    /// <summary>
    /// The resource manager
    /// </summary>
    public Ressources Ressources 
    {
      get { return _resources; }
    }

    #endregion // properties

  }
}
