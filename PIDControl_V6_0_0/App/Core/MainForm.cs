using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Diagnostics;
using System.Text;

using CommRS232;

namespace App
{
	/// <summary>
	/// Class MainForm:
	/// Applications main form
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.MenuItem menuItem7;
		private System.Windows.Forms.MenuItem menuItem11;
		private System.Windows.Forms.MenuItem menuItem14;
		private System.Windows.Forms.MenuItem menuItem20;
		private System.Windows.Forms.MenuItem _miFile;
		private System.Windows.Forms.MenuItem _miView;
		private System.Windows.Forms.MenuItem _miControl;
		private System.Windows.Forms.MenuItem _miDataRecording;
		private System.Windows.Forms.MenuItem _miHelp;
		private System.Windows.Forms.MenuItem _miFile_OpenConfigFile_Txt;
		private System.Windows.Forms.MenuItem _miFile_Print;
		private System.Windows.Forms.MenuItem _miFile_PrintPreview;
		private System.Windows.Forms.MenuItem _miFile_PrinterAdjustment;
		private System.Windows.Forms.MenuItem _miFile_Properties;
		private System.Windows.Forms.MenuItem _miFile_Exit;
		private System.Windows.Forms.MenuItem _miView_Toolbar;
		private System.Windows.Forms.MenuItem _miView_Statusbar;
		private System.Windows.Forms.MenuItem _miControl_ConnectDevice;
		private System.Windows.Forms.MenuItem _miControl_TransmitConfigData;
		private System.Windows.Forms.MenuItem _miControl_RereadScriptNames;
		private System.Windows.Forms.MenuItem _miControl_SelectScript;
		private System.Windows.Forms.MenuItem _miControl_ExecuteScript;
		private System.Windows.Forms.MenuItem _miControl_AcknowledgeAlarm;
		private System.Windows.Forms.MenuItem _miDataRecording_CurrentScan;
		private System.Windows.Forms.MenuItem _miDataRecording_ScansContinuously;
		private System.Windows.Forms.MenuItem _miDataRecording_ResultsContinuously;
		private System.Windows.Forms.MenuItem _miHelp_About;
		private System.Windows.Forms.MainMenu _MainMenu;
		private System.Windows.Forms.StatusBarPanel _StatusBarPanel1;
		private System.Windows.Forms.StatusBarPanel _StatusBarPanel2;
		private System.Windows.Forms.StatusBarPanel _StatusBarPanel3;
		private System.Windows.Forms.ImageList _ImageList;
		private System.Windows.Forms.ToolBarButton _tbbConnectDevice;
		private System.Windows.Forms.ToolBarButton _tbbOpenConfigFile;
		private System.Windows.Forms.ToolBarButton _tbbSep1;
		private System.Windows.Forms.ToolBarButton _tbbSep2;
		private System.Windows.Forms.ToolBarButton _tbbTransmitConfigData;
		private System.Windows.Forms.ToolBarButton _tbbRereadConfigData;
		private System.Windows.Forms.ToolBarButton _tbbSelectScript;
		private System.Windows.Forms.ToolBarButton _tbbSep3;
		private System.Windows.Forms.ToolBarButton _tbbExecuteScript;
		private System.Windows.Forms.ToolBarButton _tbbSep4;
		private System.Windows.Forms.ToolBarButton _tbbProperties;
		private System.Windows.Forms.ToolBarButton _tbbAbout;
		private System.Windows.Forms.Button _btnPlot_FitXY;
    private System.Windows.Forms.ToolBarButton _tbbSep5;
    private System.Windows.Forms.MenuItem _miView_Default;
    private System.Windows.Forms.Timer _Timer;
    private System.Windows.Forms.Label _lblRes_0;
    private System.Windows.Forms.TextBox _txtRes_0;
    private System.Windows.Forms.Label _lblResUnit_0;
    private System.Windows.Forms.CheckBox _chkRes_0;
    private System.Windows.Forms.Label _lblRes_5;
    private System.Windows.Forms.Label _lblRes_4;
    private System.Windows.Forms.Label _lblRes_3;
    private System.Windows.Forms.Label _lblRes_2;
    private System.Windows.Forms.Label _lblRes_1;
    private System.Windows.Forms.Label _lblRes_15;
    private System.Windows.Forms.Label _lblRes_14;
    private System.Windows.Forms.Label _lblRes_13;
    private System.Windows.Forms.Label _lblRes_12;
    private System.Windows.Forms.Label _lblRes_11;
    private System.Windows.Forms.Label _lblRes_10;
    private System.Windows.Forms.Label _lblRes_9;
    private System.Windows.Forms.Label _lblRes_8;
    private System.Windows.Forms.Label _lblRes_7;
    private System.Windows.Forms.Label _lblRes_6;
    private System.Windows.Forms.TextBox _txtRes_15;
    private System.Windows.Forms.TextBox _txtRes_14;
    private System.Windows.Forms.TextBox _txtRes_13;
    private System.Windows.Forms.TextBox _txtRes_12;
    private System.Windows.Forms.TextBox _txtRes_11;
    private System.Windows.Forms.TextBox _txtRes_10;
    private System.Windows.Forms.TextBox _txtRes_9;
    private System.Windows.Forms.TextBox _txtRes_8;
    private System.Windows.Forms.TextBox _txtRes_7;
    private System.Windows.Forms.TextBox _txtRes_6;
    private System.Windows.Forms.TextBox _txtRes_5;
    private System.Windows.Forms.TextBox _txtRes_4;
    private System.Windows.Forms.TextBox _txtRes_3;
    private System.Windows.Forms.TextBox _txtRes_2;
    private System.Windows.Forms.TextBox _txtRes_1;
    private System.Windows.Forms.Label _lblResUnit_13;
    private System.Windows.Forms.Label _lblResUnit_12;
    private System.Windows.Forms.Label _lblResUnit_11;
    private System.Windows.Forms.Label _lblResUnit_10;
    private System.Windows.Forms.Label _lblResUnit_9;
    private System.Windows.Forms.Label _lblResUnit_8;
    private System.Windows.Forms.Label _lblResUnit_7;
    private System.Windows.Forms.Label _lblResUnit_6;
    private System.Windows.Forms.Label _lblResUnit_5;
    private System.Windows.Forms.Label _lblResUnit_4;
    private System.Windows.Forms.Label _lblResUnit_3;
    private System.Windows.Forms.Label _lblResUnit_2;
    private System.Windows.Forms.Label _lblResUnit_1;
    private System.Windows.Forms.Label _lblResUnit_15;
    private System.Windows.Forms.Label _lblResUnit_14;
    private System.Windows.Forms.CheckBox _chkRes_15;
    private System.Windows.Forms.CheckBox _chkRes_14;
    private System.Windows.Forms.CheckBox _chkRes_13;
    private System.Windows.Forms.CheckBox _chkRes_12;
    private System.Windows.Forms.CheckBox _chkRes_11;
    private System.Windows.Forms.CheckBox _chkRes_10;
    private System.Windows.Forms.CheckBox _chkRes_9;
    private System.Windows.Forms.CheckBox _chkRes_8;
    private System.Windows.Forms.CheckBox _chkRes_7;
    private System.Windows.Forms.CheckBox _chkRes_6;
    private System.Windows.Forms.CheckBox _chkRes_5;
    private System.Windows.Forms.CheckBox _chkRes_4;
    private System.Windows.Forms.CheckBox _chkRes_3;
    private System.Windows.Forms.CheckBox _chkRes_2;
    private System.Windows.Forms.CheckBox _chkRes_1;
    private System.Windows.Forms.Timer _TimerTitle;
    private System.Windows.Forms.ToolTip _ToolTip;
    internal System.Windows.Forms.MenuItem _miHelp_Test;
    private System.Windows.Forms.MenuItem _miView_StaticalPrintPreview;
    private System.Windows.Forms.ToolBarButton _tbbSep6;
    private System.Windows.Forms.MenuItem _miFile_ScriptEditor;
    private System.Windows.Forms.Panel pnlLayer0;
    private System.Windows.Forms.MenuItem _miControl_TriggerKeyCmd;
    private System.Windows.Forms.ToolBarButton _tbbReadStoredSDcardData;
    private System.Windows.Forms.Panel pnlLayer1a;
    private System.Windows.Forms.Panel pnlLayer1b;
    internal System.Windows.Forms.Timer _TimerTest;
    internal System.Windows.Forms.PrintPreviewControl PrintPreview;
    internal System.Drawing.Printing.PrintDocument PrintDocument;
    internal System.Windows.Forms.Label lblCurScr_Name;
    internal System.Windows.Forms.Panel ResultPanel;
    internal System.Windows.Forms.PictureBox pbDraw;
    internal System.Windows.Forms.Label lblXcur;
    internal System.Windows.Forms.Label lblXmax;
    internal System.Windows.Forms.Label lblXmin;
    internal System.Windows.Forms.Label lblYmin;
    internal System.Windows.Forms.Label lblYmax;
    internal System.Windows.Forms.Panel GraphPanel;
    internal System.Windows.Forms.Panel RecordPanel;
    internal System.Windows.Forms.StatusBar StatusBar;
    internal System.Windows.Forms.ToolBar ToolBar;
    private System.Windows.Forms.Button _btnPlot_FitDef;
    private System.Windows.Forms.Button _btnPlot_FitX;
    private System.Windows.Forms.Button _btnPlot_FitY;
    internal System.Windows.Forms.Label lblXunit;
    internal System.Windows.Forms.Label lblYunit;
    private System.Windows.Forms.MenuItem menuItem1;
    private System.Windows.Forms.MenuItem _miTools;
    private System.Windows.Forms.MenuItem _miTools_EEStore;
    private System.Windows.Forms.MenuItem _miTools_SDStore;
    private System.Windows.Forms.MenuItem _miTools_Service;
    private System.Windows.Forms.MenuItem _miTools_Diag;
    internal System.Windows.Forms.ListView lvPeakInfo;
    internal System.Windows.Forms.ColumnHeader colAbscissa;
    internal System.Windows.Forms.ColumnHeader colHeight;
    internal System.Windows.Forms.ColumnHeader colArea;
    private System.Windows.Forms.GroupBox gbResults;
    private System.Windows.Forms.GroupBox gbPeakInfo;
    private System.Windows.Forms.GroupBox gbSpecRec;
    private System.Windows.Forms.CheckBox _chkCurrScan;
    private System.Windows.Forms.CheckBox _chkScansCont;
    private System.Windows.Forms.CheckBox _chkResultsCont;
    private System.Windows.Forms.GroupBox gbDeviceInfo;
    internal System.Windows.Forms.Label lblCurScr;
    internal System.Windows.Forms.Label lblTemp;
    internal System.Windows.Forms.Label lblTemp_Val;
    internal System.Windows.Forms.Label lblPres;
    internal System.Windows.Forms.Label lblPres_Val;
    private System.Windows.Forms.MenuItem _miView_Sep1;
    internal System.Windows.Forms.Label lblNoOfPts;
    internal System.Windows.Forms.Label lblNoOfPts_Val;
    private System.Windows.Forms.ToolBarButton _tbbTriggerKeyCmd;
    internal System.Windows.Forms.Label lblOffset;
    internal System.Windows.Forms.Label lblOffset_Val;
    private System.Windows.Forms.MenuItem menuItem2;
    private System.Windows.Forms.MenuItem _miDataRecording_Grafic;
    internal System.Windows.Forms.Label lblYcur;
    private System.Windows.Forms.ComboBox cbScansToDraw;
    internal System.Windows.Forms.Label lblX;
    internal System.Windows.Forms.Label lblY;
    internal System.Windows.Forms.Button btnDisplayedGrafic;
    private System.Windows.Forms.MenuItem menuItem3;
    private System.Windows.Forms.MenuItem _miTools_LoadScanFromHD;
    internal System.Windows.Forms.Label lblOffline;
    internal System.Windows.Forms.Label lblErr;
    internal System.Windows.Forms.Label lblErr_Val;
    internal System.Windows.Forms.Label lblDeviceNo;
    internal System.Windows.Forms.Label lblDeviceNo_Val;
    private System.Windows.Forms.StatusBarPanel _StatusBarPanel4;
    private System.Windows.Forms.MenuItem _miTools_MP;
    private System.Windows.Forms.MenuItem _miTools_CTS;
    internal System.Windows.Forms.Label lblFlow;
    internal System.Windows.Forms.Label lblFlow_Val;
    private System.Windows.Forms.MenuItem _miTools_PCcommListener;
    internal System.Windows.Forms.Button btnAlarm;
    internal System.Windows.Forms.Label lblWar;
    internal System.Windows.Forms.Label lblWar_Val;
    private System.Windows.Forms.Button _btnErrAck;
    private MenuItem _miFile_OpenConfigFile;
    private MenuItem _miFile_ServiceDataEditor;
    private MenuItem _miFile_OpenServiceDataFile;
    private MenuItem menuItem4;
    private MenuItem _miControl_TransmitServiceData;
    private ToolBarButton _tbbOpenServiceDataFile;
    private ToolBarButton _tbbTransmitServiceData;
		private System.ComponentModel.IContainer components;

		/// <summary>
		/// Constructor
		/// </summary>
    public MainForm()
		{
			InitializeComponent();
      
      // Init.
      _Init ();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
      this._MainMenu = new System.Windows.Forms.MainMenu(this.components);
      this._miFile = new System.Windows.Forms.MenuItem();
      this._miFile_ScriptEditor = new System.Windows.Forms.MenuItem();
      this._miFile_OpenConfigFile_Txt = new System.Windows.Forms.MenuItem();
      this._miFile_OpenConfigFile = new System.Windows.Forms.MenuItem();
      this.menuItem7 = new System.Windows.Forms.MenuItem();
      this._miFile_ServiceDataEditor = new System.Windows.Forms.MenuItem();
      this._miFile_OpenServiceDataFile = new System.Windows.Forms.MenuItem();
      this.menuItem4 = new System.Windows.Forms.MenuItem();
      this._miFile_Print = new System.Windows.Forms.MenuItem();
      this._miFile_PrintPreview = new System.Windows.Forms.MenuItem();
      this._miFile_PrinterAdjustment = new System.Windows.Forms.MenuItem();
      this.menuItem11 = new System.Windows.Forms.MenuItem();
      this._miFile_Properties = new System.Windows.Forms.MenuItem();
      this.menuItem14 = new System.Windows.Forms.MenuItem();
      this._miFile_Exit = new System.Windows.Forms.MenuItem();
      this._miView = new System.Windows.Forms.MenuItem();
      this._miView_Toolbar = new System.Windows.Forms.MenuItem();
      this._miView_Statusbar = new System.Windows.Forms.MenuItem();
      this._miView_Default = new System.Windows.Forms.MenuItem();
      this._miView_Sep1 = new System.Windows.Forms.MenuItem();
      this._miView_StaticalPrintPreview = new System.Windows.Forms.MenuItem();
      this._miControl = new System.Windows.Forms.MenuItem();
      this._miControl_ConnectDevice = new System.Windows.Forms.MenuItem();
      this.menuItem20 = new System.Windows.Forms.MenuItem();
      this._miControl_TransmitConfigData = new System.Windows.Forms.MenuItem();
      this._miControl_TransmitServiceData = new System.Windows.Forms.MenuItem();
      this._miControl_RereadScriptNames = new System.Windows.Forms.MenuItem();
      this._miControl_SelectScript = new System.Windows.Forms.MenuItem();
      this._miControl_ExecuteScript = new System.Windows.Forms.MenuItem();
      this.menuItem1 = new System.Windows.Forms.MenuItem();
      this._miControl_AcknowledgeAlarm = new System.Windows.Forms.MenuItem();
      this._miControl_TriggerKeyCmd = new System.Windows.Forms.MenuItem();
      this._miDataRecording = new System.Windows.Forms.MenuItem();
      this._miDataRecording_CurrentScan = new System.Windows.Forms.MenuItem();
      this._miDataRecording_ScansContinuously = new System.Windows.Forms.MenuItem();
      this._miDataRecording_ResultsContinuously = new System.Windows.Forms.MenuItem();
      this.menuItem2 = new System.Windows.Forms.MenuItem();
      this._miDataRecording_Grafic = new System.Windows.Forms.MenuItem();
      this._miTools = new System.Windows.Forms.MenuItem();
      this._miTools_EEStore = new System.Windows.Forms.MenuItem();
      this._miTools_SDStore = new System.Windows.Forms.MenuItem();
      this._miTools_Service = new System.Windows.Forms.MenuItem();
      this._miTools_MP = new System.Windows.Forms.MenuItem();
      this._miTools_CTS = new System.Windows.Forms.MenuItem();
      this._miTools_Diag = new System.Windows.Forms.MenuItem();
      this._miTools_PCcommListener = new System.Windows.Forms.MenuItem();
      this.menuItem3 = new System.Windows.Forms.MenuItem();
      this._miTools_LoadScanFromHD = new System.Windows.Forms.MenuItem();
      this._miHelp = new System.Windows.Forms.MenuItem();
      this._miHelp_About = new System.Windows.Forms.MenuItem();
      this._miHelp_Test = new System.Windows.Forms.MenuItem();
      this.StatusBar = new System.Windows.Forms.StatusBar();
      this._StatusBarPanel1 = new System.Windows.Forms.StatusBarPanel();
      this._StatusBarPanel2 = new System.Windows.Forms.StatusBarPanel();
      this._StatusBarPanel3 = new System.Windows.Forms.StatusBarPanel();
      this._StatusBarPanel4 = new System.Windows.Forms.StatusBarPanel();
      this.ToolBar = new System.Windows.Forms.ToolBar();
      this._tbbConnectDevice = new System.Windows.Forms.ToolBarButton();
      this._tbbSep1 = new System.Windows.Forms.ToolBarButton();
      this._tbbOpenConfigFile = new System.Windows.Forms.ToolBarButton();
      this._tbbSep2 = new System.Windows.Forms.ToolBarButton();
      this._tbbTransmitConfigData = new System.Windows.Forms.ToolBarButton();
      this._tbbRereadConfigData = new System.Windows.Forms.ToolBarButton();
      this._tbbSelectScript = new System.Windows.Forms.ToolBarButton();
      this._tbbSep3 = new System.Windows.Forms.ToolBarButton();
      this._tbbExecuteScript = new System.Windows.Forms.ToolBarButton();
      this._tbbTriggerKeyCmd = new System.Windows.Forms.ToolBarButton();
      this._tbbSep4 = new System.Windows.Forms.ToolBarButton();
      this._tbbProperties = new System.Windows.Forms.ToolBarButton();
      this._tbbSep5 = new System.Windows.Forms.ToolBarButton();
      this._tbbReadStoredSDcardData = new System.Windows.Forms.ToolBarButton();
      this._tbbSep6 = new System.Windows.Forms.ToolBarButton();
      this._tbbAbout = new System.Windows.Forms.ToolBarButton();
      this._ImageList = new System.Windows.Forms.ImageList(this.components);
      this.ResultPanel = new System.Windows.Forms.Panel();
      this.gbPeakInfo = new System.Windows.Forms.GroupBox();
      this.lvPeakInfo = new System.Windows.Forms.ListView();
      this.colAbscissa = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.colHeight = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.colArea = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.gbResults = new System.Windows.Forms.GroupBox();
      this._chkRes_15 = new System.Windows.Forms.CheckBox();
      this._lblResUnit_15 = new System.Windows.Forms.Label();
      this._txtRes_15 = new System.Windows.Forms.TextBox();
      this._lblRes_15 = new System.Windows.Forms.Label();
      this._chkRes_14 = new System.Windows.Forms.CheckBox();
      this._lblResUnit_14 = new System.Windows.Forms.Label();
      this._txtRes_14 = new System.Windows.Forms.TextBox();
      this._lblRes_14 = new System.Windows.Forms.Label();
      this._chkRes_13 = new System.Windows.Forms.CheckBox();
      this._lblResUnit_13 = new System.Windows.Forms.Label();
      this._txtRes_13 = new System.Windows.Forms.TextBox();
      this._lblRes_13 = new System.Windows.Forms.Label();
      this._chkRes_12 = new System.Windows.Forms.CheckBox();
      this._lblResUnit_12 = new System.Windows.Forms.Label();
      this._txtRes_12 = new System.Windows.Forms.TextBox();
      this._lblRes_12 = new System.Windows.Forms.Label();
      this._chkRes_11 = new System.Windows.Forms.CheckBox();
      this._lblResUnit_11 = new System.Windows.Forms.Label();
      this._txtRes_11 = new System.Windows.Forms.TextBox();
      this._lblRes_11 = new System.Windows.Forms.Label();
      this._chkRes_10 = new System.Windows.Forms.CheckBox();
      this._lblResUnit_10 = new System.Windows.Forms.Label();
      this._txtRes_10 = new System.Windows.Forms.TextBox();
      this._lblRes_10 = new System.Windows.Forms.Label();
      this._chkRes_9 = new System.Windows.Forms.CheckBox();
      this._lblResUnit_9 = new System.Windows.Forms.Label();
      this._txtRes_9 = new System.Windows.Forms.TextBox();
      this._lblRes_9 = new System.Windows.Forms.Label();
      this._chkRes_8 = new System.Windows.Forms.CheckBox();
      this._lblResUnit_8 = new System.Windows.Forms.Label();
      this._txtRes_8 = new System.Windows.Forms.TextBox();
      this._lblRes_8 = new System.Windows.Forms.Label();
      this._chkRes_7 = new System.Windows.Forms.CheckBox();
      this._lblResUnit_7 = new System.Windows.Forms.Label();
      this._txtRes_7 = new System.Windows.Forms.TextBox();
      this._lblRes_7 = new System.Windows.Forms.Label();
      this._chkRes_6 = new System.Windows.Forms.CheckBox();
      this._lblResUnit_6 = new System.Windows.Forms.Label();
      this._txtRes_6 = new System.Windows.Forms.TextBox();
      this._lblRes_6 = new System.Windows.Forms.Label();
      this._chkRes_5 = new System.Windows.Forms.CheckBox();
      this._lblResUnit_5 = new System.Windows.Forms.Label();
      this._txtRes_5 = new System.Windows.Forms.TextBox();
      this._lblRes_5 = new System.Windows.Forms.Label();
      this._chkRes_4 = new System.Windows.Forms.CheckBox();
      this._lblResUnit_4 = new System.Windows.Forms.Label();
      this._txtRes_4 = new System.Windows.Forms.TextBox();
      this._lblRes_4 = new System.Windows.Forms.Label();
      this._chkRes_3 = new System.Windows.Forms.CheckBox();
      this._lblResUnit_3 = new System.Windows.Forms.Label();
      this._txtRes_3 = new System.Windows.Forms.TextBox();
      this._lblRes_3 = new System.Windows.Forms.Label();
      this._chkRes_2 = new System.Windows.Forms.CheckBox();
      this._lblResUnit_2 = new System.Windows.Forms.Label();
      this._txtRes_2 = new System.Windows.Forms.TextBox();
      this._lblRes_2 = new System.Windows.Forms.Label();
      this._chkRes_1 = new System.Windows.Forms.CheckBox();
      this._lblResUnit_1 = new System.Windows.Forms.Label();
      this._txtRes_1 = new System.Windows.Forms.TextBox();
      this._lblRes_1 = new System.Windows.Forms.Label();
      this._chkRes_0 = new System.Windows.Forms.CheckBox();
      this._lblResUnit_0 = new System.Windows.Forms.Label();
      this._txtRes_0 = new System.Windows.Forms.TextBox();
      this._lblRes_0 = new System.Windows.Forms.Label();
      this.gbDeviceInfo = new System.Windows.Forms.GroupBox();
      this._btnErrAck = new System.Windows.Forms.Button();
      this.lblWar = new System.Windows.Forms.Label();
      this.lblWar_Val = new System.Windows.Forms.Label();
      this.lblFlow = new System.Windows.Forms.Label();
      this.lblFlow_Val = new System.Windows.Forms.Label();
      this.lblDeviceNo = new System.Windows.Forms.Label();
      this.lblDeviceNo_Val = new System.Windows.Forms.Label();
      this.lblErr = new System.Windows.Forms.Label();
      this.lblErr_Val = new System.Windows.Forms.Label();
      this.lblOffset = new System.Windows.Forms.Label();
      this.lblOffset_Val = new System.Windows.Forms.Label();
      this.lblPres = new System.Windows.Forms.Label();
      this.lblPres_Val = new System.Windows.Forms.Label();
      this.lblTemp = new System.Windows.Forms.Label();
      this.lblTemp_Val = new System.Windows.Forms.Label();
      this.lblCurScr = new System.Windows.Forms.Label();
      this.lblCurScr_Name = new System.Windows.Forms.Label();
      this.GraphPanel = new System.Windows.Forms.Panel();
      this.lblOffline = new System.Windows.Forms.Label();
      this.lblY = new System.Windows.Forms.Label();
      this.lblX = new System.Windows.Forms.Label();
      this.cbScansToDraw = new System.Windows.Forms.ComboBox();
      this.lblNoOfPts_Val = new System.Windows.Forms.Label();
      this.lblNoOfPts = new System.Windows.Forms.Label();
      this.lblYunit = new System.Windows.Forms.Label();
      this.lblXunit = new System.Windows.Forms.Label();
      this._btnPlot_FitY = new System.Windows.Forms.Button();
      this._btnPlot_FitX = new System.Windows.Forms.Button();
      this._btnPlot_FitDef = new System.Windows.Forms.Button();
      this._btnPlot_FitXY = new System.Windows.Forms.Button();
      this.lblYcur = new System.Windows.Forms.Label();
      this.lblXcur = new System.Windows.Forms.Label();
      this.lblXmax = new System.Windows.Forms.Label();
      this.lblXmin = new System.Windows.Forms.Label();
      this.lblYmin = new System.Windows.Forms.Label();
      this.lblYmax = new System.Windows.Forms.Label();
      this.pbDraw = new System.Windows.Forms.PictureBox();
      this._Timer = new System.Windows.Forms.Timer(this.components);
      this.btnAlarm = new System.Windows.Forms.Button();
      this._TimerTitle = new System.Windows.Forms.Timer(this.components);
      this.PrintDocument = new System.Drawing.Printing.PrintDocument();
      this.PrintPreview = new System.Windows.Forms.PrintPreviewControl();
      this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
      this.btnDisplayedGrafic = new System.Windows.Forms.Button();
      this.RecordPanel = new System.Windows.Forms.Panel();
      this.gbSpecRec = new System.Windows.Forms.GroupBox();
      this._chkResultsCont = new System.Windows.Forms.CheckBox();
      this._chkCurrScan = new System.Windows.Forms.CheckBox();
      this._chkScansCont = new System.Windows.Forms.CheckBox();
      this.pnlLayer0 = new System.Windows.Forms.Panel();
      this.pnlLayer1b = new System.Windows.Forms.Panel();
      this.pnlLayer1a = new System.Windows.Forms.Panel();
      this._TimerTest = new System.Windows.Forms.Timer(this.components);
      this._tbbOpenServiceDataFile = new System.Windows.Forms.ToolBarButton();
      this._tbbTransmitServiceData = new System.Windows.Forms.ToolBarButton();
      ((System.ComponentModel.ISupportInitialize)(this._StatusBarPanel1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._StatusBarPanel2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._StatusBarPanel3)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._StatusBarPanel4)).BeginInit();
      this.ResultPanel.SuspendLayout();
      this.gbPeakInfo.SuspendLayout();
      this.gbResults.SuspendLayout();
      this.gbDeviceInfo.SuspendLayout();
      this.GraphPanel.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbDraw)).BeginInit();
      this.RecordPanel.SuspendLayout();
      this.gbSpecRec.SuspendLayout();
      this.pnlLayer0.SuspendLayout();
      this.pnlLayer1b.SuspendLayout();
      this.pnlLayer1a.SuspendLayout();
      this.SuspendLayout();
      // 
      // _MainMenu
      // 
      this._MainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this._miFile,
            this._miView,
            this._miControl,
            this._miDataRecording,
            this._miTools,
            this._miHelp});
      // 
      // _miFile
      // 
      this._miFile.Index = 0;
      this._miFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this._miFile_ScriptEditor,
            this._miFile_OpenConfigFile_Txt,
            this._miFile_OpenConfigFile,
            this.menuItem7,
            this._miFile_ServiceDataEditor,
            this._miFile_OpenServiceDataFile,
            this.menuItem4,
            this._miFile_Print,
            this._miFile_PrintPreview,
            this._miFile_PrinterAdjustment,
            this.menuItem11,
            this._miFile_Properties,
            this.menuItem14,
            this._miFile_Exit});
      this._miFile.Text = "&File";
      this._miFile.Popup += new System.EventHandler(this._mi_Popup);
      // 
      // _miFile_ScriptEditor
      // 
      this._miFile_ScriptEditor.Index = 0;
      this._miFile_ScriptEditor.Text = "&Script editor ...";
      this._miFile_ScriptEditor.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miFile_OpenConfigFile_Txt
      // 
      this._miFile_OpenConfigFile_Txt.Index = 1;
      this._miFile_OpenConfigFile_Txt.Text = "&Open configuration file (plain text) ...";
      this._miFile_OpenConfigFile_Txt.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miFile_OpenConfigFile
      // 
      this._miFile_OpenConfigFile.Index = 2;
      this._miFile_OpenConfigFile.Text = "Open &configuration file ...";
      this._miFile_OpenConfigFile.Click += new System.EventHandler(this._mi_Click);
      // 
      // menuItem7
      // 
      this.menuItem7.Index = 3;
      this.menuItem7.Text = "-";
      // 
      // _miFile_ServiceDataEditor
      // 
      this._miFile_ServiceDataEditor.Index = 4;
      this._miFile_ServiceDataEditor.Text = "Serv&ice data editor ...";
      this._miFile_ServiceDataEditor.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miFile_OpenServiceDataFile
      // 
      this._miFile_OpenServiceDataFile.Index = 5;
      this._miFile_OpenServiceDataFile.Text = "Ope&n service data file ...";
      this._miFile_OpenServiceDataFile.Click += new System.EventHandler(this._mi_Click);
      // 
      // menuItem4
      // 
      this.menuItem4.Index = 6;
      this.menuItem4.Text = "-";
      // 
      // _miFile_Print
      // 
      this._miFile_Print.Index = 7;
      this._miFile_Print.Text = "&Print ...";
      this._miFile_Print.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miFile_PrintPreview
      // 
      this._miFile_PrintPreview.Index = 8;
      this._miFile_PrintPreview.Text = "P&rint preview";
      this._miFile_PrintPreview.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miFile_PrinterAdjustment
      // 
      this._miFile_PrinterAdjustment.Index = 9;
      this._miFile_PrinterAdjustment.Text = "Printer &adjustment ...";
      this._miFile_PrinterAdjustment.Click += new System.EventHandler(this._mi_Click);
      // 
      // menuItem11
      // 
      this.menuItem11.Index = 10;
      this.menuItem11.Text = "-";
      // 
      // _miFile_Properties
      // 
      this._miFile_Properties.Index = 11;
      this._miFile_Properties.Text = "Proper&ties ...";
      this._miFile_Properties.Click += new System.EventHandler(this._mi_Click);
      // 
      // menuItem14
      // 
      this.menuItem14.Index = 12;
      this.menuItem14.Text = "-";
      // 
      // _miFile_Exit
      // 
      this._miFile_Exit.Index = 13;
      this._miFile_Exit.Text = "&Exit";
      this._miFile_Exit.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miView
      // 
      this._miView.Index = 1;
      this._miView.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this._miView_Toolbar,
            this._miView_Statusbar,
            this._miView_Default,
            this._miView_Sep1,
            this._miView_StaticalPrintPreview});
      this._miView.Text = "&View";
      this._miView.Popup += new System.EventHandler(this._mi_Popup);
      // 
      // _miView_Toolbar
      // 
      this._miView_Toolbar.Index = 0;
      this._miView_Toolbar.Text = "&Toolbar";
      this._miView_Toolbar.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miView_Statusbar
      // 
      this._miView_Statusbar.Index = 1;
      this._miView_Statusbar.Text = "&Statusbar";
      this._miView_Statusbar.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miView_Default
      // 
      this._miView_Default.Index = 2;
      this._miView_Default.Text = "&Default size";
      this._miView_Default.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miView_Sep1
      // 
      this._miView_Sep1.Index = 3;
      this._miView_Sep1.Text = "-";
      // 
      // _miView_StaticalPrintPreview
      // 
      this._miView_StaticalPrintPreview.Index = 4;
      this._miView_StaticalPrintPreview.Text = "Statical &print preview";
      this._miView_StaticalPrintPreview.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miControl
      // 
      this._miControl.Index = 2;
      this._miControl.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this._miControl_ConnectDevice,
            this.menuItem20,
            this._miControl_TransmitConfigData,
            this._miControl_TransmitServiceData,
            this._miControl_RereadScriptNames,
            this._miControl_SelectScript,
            this._miControl_ExecuteScript,
            this.menuItem1,
            this._miControl_AcknowledgeAlarm,
            this._miControl_TriggerKeyCmd});
      this._miControl.Text = "&Control";
      this._miControl.Popup += new System.EventHandler(this._mi_Popup);
      // 
      // _miControl_ConnectDevice
      // 
      this._miControl_ConnectDevice.Index = 0;
      this._miControl_ConnectDevice.Text = "&Connect device";
      this._miControl_ConnectDevice.Click += new System.EventHandler(this._mi_Click);
      // 
      // menuItem20
      // 
      this.menuItem20.Index = 1;
      this.menuItem20.Text = "-";
      // 
      // _miControl_TransmitConfigData
      // 
      this._miControl_TransmitConfigData.Index = 2;
      this._miControl_TransmitConfigData.Text = "&Transmit configuration data";
      this._miControl_TransmitConfigData.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miControl_TransmitServiceData
      // 
      this._miControl_TransmitServiceData.Index = 3;
      this._miControl_TransmitServiceData.Text = "Transmit service &data";
      this._miControl_TransmitServiceData.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miControl_RereadScriptNames
      // 
      this._miControl_RereadScriptNames.Index = 4;
      this._miControl_RereadScriptNames.Text = "&Reread script names";
      this._miControl_RereadScriptNames.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miControl_SelectScript
      // 
      this._miControl_SelectScript.Index = 5;
      this._miControl_SelectScript.Text = "&Select script ...";
      this._miControl_SelectScript.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miControl_ExecuteScript
      // 
      this._miControl_ExecuteScript.Index = 6;
      this._miControl_ExecuteScript.Text = "&Execute script";
      this._miControl_ExecuteScript.Click += new System.EventHandler(this._mi_Click);
      // 
      // menuItem1
      // 
      this.menuItem1.Index = 7;
      this.menuItem1.Text = "-";
      // 
      // _miControl_AcknowledgeAlarm
      // 
      this._miControl_AcknowledgeAlarm.Index = 8;
      this._miControl_AcknowledgeAlarm.Text = "Confirm &alarm";
      this._miControl_AcknowledgeAlarm.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miControl_TriggerKeyCmd
      // 
      this._miControl_TriggerKeyCmd.Index = 9;
      this._miControl_TriggerKeyCmd.Text = "Trigger KE&Y command";
      this._miControl_TriggerKeyCmd.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miDataRecording
      // 
      this._miDataRecording.Index = 3;
      this._miDataRecording.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this._miDataRecording_CurrentScan,
            this._miDataRecording_ScansContinuously,
            this._miDataRecording_ResultsContinuously,
            this.menuItem2,
            this._miDataRecording_Grafic});
      this._miDataRecording.Text = "&Data recording";
      this._miDataRecording.Popup += new System.EventHandler(this._mi_Popup);
      // 
      // _miDataRecording_CurrentScan
      // 
      this._miDataRecording_CurrentScan.Index = 0;
      this._miDataRecording_CurrentScan.Text = "&Current scan";
      this._miDataRecording_CurrentScan.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miDataRecording_ScansContinuously
      // 
      this._miDataRecording_ScansContinuously.Index = 1;
      this._miDataRecording_ScansContinuously.Text = "Sc&ans continuously";
      this._miDataRecording_ScansContinuously.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miDataRecording_ResultsContinuously
      // 
      this._miDataRecording_ResultsContinuously.Index = 2;
      this._miDataRecording_ResultsContinuously.Text = "&Results continuously";
      this._miDataRecording_ResultsContinuously.Click += new System.EventHandler(this._mi_Click);
      // 
      // menuItem2
      // 
      this.menuItem2.Index = 3;
      this.menuItem2.Text = "-";
      // 
      // _miDataRecording_Grafic
      // 
      this._miDataRecording_Grafic.Index = 4;
      this._miDataRecording_Grafic.Text = "Displayed c&hromatogram";
      this._miDataRecording_Grafic.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miTools
      // 
      this._miTools.Index = 4;
      this._miTools.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this._miTools_EEStore,
            this._miTools_SDStore,
            this._miTools_Service,
            this._miTools_MP,
            this._miTools_CTS,
            this._miTools_Diag,
            this._miTools_PCcommListener,
            this.menuItem3,
            this._miTools_LoadScanFromHD});
      this._miTools.Text = "&Tools";
      this._miTools.Popup += new System.EventHandler(this._mi_Popup);
      // 
      // _miTools_EEStore
      // 
      this._miTools_EEStore.Index = 0;
      this._miTools_EEStore.Text = "Read/Clear stored &Eeprom data ...";
      this._miTools_EEStore.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miTools_SDStore
      // 
      this._miTools_SDStore.Index = 1;
      this._miTools_SDStore.Text = "Read/Clear stored &SDcard data ...";
      this._miTools_SDStore.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miTools_Service
      // 
      this._miTools_Service.Index = 2;
      this._miTools_Service.Text = "&Transfer service data ...";
      this._miTools_Service.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miTools_MP
      // 
      this._miTools_MP.Index = 3;
      this._miTools_MP.Text = "Transfer &MP data ...";
      this._miTools_MP.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miTools_CTS
      // 
      this._miTools_CTS.Index = 4;
      this._miTools_CTS.Text = "Transfer &clock-timed script data ...";
      this._miTools_CTS.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miTools_Diag
      // 
      this._miTools_Diag.Index = 5;
      this._miTools_Diag.Text = "&Diagnosis";
      this._miTools_Diag.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miTools_PCcommListener
      // 
      this._miTools_PCcommListener.Index = 6;
      this._miTools_PCcommListener.Text = "&PC communication listener";
      this._miTools_PCcommListener.Click += new System.EventHandler(this._mi_Click);
      // 
      // menuItem3
      // 
      this.menuItem3.Index = 7;
      this.menuItem3.Text = "-";
      // 
      // _miTools_LoadScanFromHD
      // 
      this._miTools_LoadScanFromHD.Index = 8;
      this._miTools_LoadScanFromHD.Text = "&Load and display scan from HD ...";
      this._miTools_LoadScanFromHD.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miHelp
      // 
      this._miHelp.Index = 5;
      this._miHelp.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this._miHelp_About,
            this._miHelp_Test});
      this._miHelp.Text = "&?";
      this._miHelp.Popup += new System.EventHandler(this._mi_Popup);
      // 
      // _miHelp_About
      // 
      this._miHelp_About.Index = 0;
      this._miHelp_About.Text = "&About ...";
      this._miHelp_About.Click += new System.EventHandler(this._mi_Click);
      // 
      // _miHelp_Test
      // 
      this._miHelp_Test.Index = 1;
      this._miHelp_Test.Text = "&Test";
      this._miHelp_Test.Click += new System.EventHandler(this._mi_Click);
      // 
      // StatusBar
      // 
      this.StatusBar.Location = new System.Drawing.Point(0, 625);
      this.StatusBar.Name = "StatusBar";
      this.StatusBar.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
            this._StatusBarPanel1,
            this._StatusBarPanel2,
            this._StatusBarPanel3,
            this._StatusBarPanel4});
      this.StatusBar.ShowPanels = true;
      this.StatusBar.Size = new System.Drawing.Size(896, 22);
      this.StatusBar.TabIndex = 0;
      this.StatusBar.DrawItem += new System.Windows.Forms.StatusBarDrawItemEventHandler(this.StatusBar_DrawItem);
      // 
      // _StatusBarPanel1
      // 
      this._StatusBarPanel1.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
      this._StatusBarPanel1.BorderStyle = System.Windows.Forms.StatusBarPanelBorderStyle.None;
      this._StatusBarPanel1.Name = "_StatusBarPanel1";
      this._StatusBarPanel1.Text = "<message>";
      this._StatusBarPanel1.Width = 579;
      // 
      // _StatusBarPanel2
      // 
      this._StatusBarPanel2.Name = "_StatusBarPanel2";
      this._StatusBarPanel2.Style = System.Windows.Forms.StatusBarPanelStyle.OwnerDraw;
      this._StatusBarPanel2.Text = "<connected>";
      // 
      // _StatusBarPanel3
      // 
      this._StatusBarPanel3.Name = "_StatusBarPanel3";
      this._StatusBarPanel3.Text = "<scan>";
      // 
      // _StatusBarPanel4
      // 
      this._StatusBarPanel4.Name = "_StatusBarPanel4";
      this._StatusBarPanel4.Text = "<meas. chan.>";
      // 
      // ToolBar
      // 
      this.ToolBar.AutoSize = false;
      this.ToolBar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this._tbbConnectDevice,
            this._tbbSep1,
            this._tbbOpenConfigFile,
            this._tbbTransmitConfigData,
            this._tbbRereadConfigData,
            this._tbbSep2,
            this._tbbOpenServiceDataFile,
            this._tbbTransmitServiceData,
            this._tbbSep3,
            this._tbbSelectScript,
            this._tbbExecuteScript,
            this._tbbTriggerKeyCmd,
            this._tbbSep4,
            this._tbbProperties,
            this._tbbSep5,
            this._tbbReadStoredSDcardData,
            this._tbbSep6,
            this._tbbAbout});
      this.ToolBar.DropDownArrows = true;
      this.ToolBar.ImageList = this._ImageList;
      this.ToolBar.Location = new System.Drawing.Point(0, 0);
      this.ToolBar.Name = "ToolBar";
      this.ToolBar.ShowToolTips = true;
      this.ToolBar.Size = new System.Drawing.Size(896, 28);
      this.ToolBar.TabIndex = 1;
      // 
      // _tbbConnectDevice
      // 
      this._tbbConnectDevice.ImageIndex = 0;
      this._tbbConnectDevice.Name = "_tbbConnectDevice";
      this._tbbConnectDevice.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
      this._tbbConnectDevice.ToolTipText = "Connect device";
      // 
      // _tbbSep1
      // 
      this._tbbSep1.Name = "_tbbSep1";
      this._tbbSep1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
      // 
      // _tbbOpenConfigFile
      // 
      this._tbbOpenConfigFile.ImageIndex = 1;
      this._tbbOpenConfigFile.Name = "_tbbOpenConfigFile";
      this._tbbOpenConfigFile.ToolTipText = "Open configuration file";
      // 
      // _tbbSep2
      // 
      this._tbbSep2.Name = "_tbbSep2";
      this._tbbSep2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
      // 
      // _tbbTransmitConfigData
      // 
      this._tbbTransmitConfigData.ImageIndex = 2;
      this._tbbTransmitConfigData.Name = "_tbbTransmitConfigData";
      this._tbbTransmitConfigData.ToolTipText = "Transmit configuration data";
      // 
      // _tbbRereadConfigData
      // 
      this._tbbRereadConfigData.ImageIndex = 3;
      this._tbbRereadConfigData.Name = "_tbbRereadConfigData";
      this._tbbRereadConfigData.ToolTipText = "Reread configuration data";
      // 
      // _tbbSelectScript
      // 
      this._tbbSelectScript.ImageIndex = 4;
      this._tbbSelectScript.Name = "_tbbSelectScript";
      this._tbbSelectScript.ToolTipText = "Select script";
      // 
      // _tbbSep3
      // 
      this._tbbSep3.Name = "_tbbSep3";
      this._tbbSep3.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
      // 
      // _tbbExecuteScript
      // 
      this._tbbExecuteScript.ImageIndex = 5;
      this._tbbExecuteScript.Name = "_tbbExecuteScript";
      this._tbbExecuteScript.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
      this._tbbExecuteScript.ToolTipText = "Execute script";
      // 
      // _tbbTriggerKeyCmd
      // 
      this._tbbTriggerKeyCmd.ImageIndex = 10;
      this._tbbTriggerKeyCmd.Name = "_tbbTriggerKeyCmd";
      this._tbbTriggerKeyCmd.ToolTipText = "Trigger Key command";
      // 
      // _tbbSep4
      // 
      this._tbbSep4.Name = "_tbbSep4";
      this._tbbSep4.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
      // 
      // _tbbProperties
      // 
      this._tbbProperties.ImageIndex = 7;
      this._tbbProperties.Name = "_tbbProperties";
      this._tbbProperties.ToolTipText = "Properties";
      // 
      // _tbbSep5
      // 
      this._tbbSep5.Name = "_tbbSep5";
      this._tbbSep5.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
      // 
      // _tbbReadStoredSDcardData
      // 
      this._tbbReadStoredSDcardData.ImageIndex = 9;
      this._tbbReadStoredSDcardData.Name = "_tbbReadStoredSDcardData";
      this._tbbReadStoredSDcardData.ToolTipText = "Read stored SDcard data";
      // 
      // _tbbSep6
      // 
      this._tbbSep6.Name = "_tbbSep6";
      this._tbbSep6.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
      // 
      // _tbbAbout
      // 
      this._tbbAbout.ImageIndex = 8;
      this._tbbAbout.Name = "_tbbAbout";
      this._tbbAbout.ToolTipText = "About";
      // 
      // _ImageList
      // 
      this._ImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_ImageList.ImageStream")));
      this._ImageList.TransparentColor = System.Drawing.Color.Magenta;
      this._ImageList.Images.SetKeyName(0, "");
      this._ImageList.Images.SetKeyName(1, "");
      this._ImageList.Images.SetKeyName(2, "");
      this._ImageList.Images.SetKeyName(3, "");
      this._ImageList.Images.SetKeyName(4, "");
      this._ImageList.Images.SetKeyName(5, "");
      this._ImageList.Images.SetKeyName(6, "");
      this._ImageList.Images.SetKeyName(7, "");
      this._ImageList.Images.SetKeyName(8, "");
      this._ImageList.Images.SetKeyName(9, "");
      this._ImageList.Images.SetKeyName(10, "");
      this._ImageList.Images.SetKeyName(11, "OpenServiceDataFile.bmp");
      this._ImageList.Images.SetKeyName(12, "TransmitServiceData.bmp");
      // 
      // ResultPanel
      // 
      this.ResultPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.ResultPanel.Controls.Add(this.gbPeakInfo);
      this.ResultPanel.Controls.Add(this.gbResults);
      this.ResultPanel.Controls.Add(this.gbDeviceInfo);
      this.ResultPanel.Dock = System.Windows.Forms.DockStyle.Left;
      this.ResultPanel.Location = new System.Drawing.Point(0, 28);
      this.ResultPanel.Name = "ResultPanel";
      this.ResultPanel.Size = new System.Drawing.Size(266, 597);
      this.ResultPanel.TabIndex = 5;
      // 
      // gbPeakInfo
      // 
      this.gbPeakInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.gbPeakInfo.Controls.Add(this.lvPeakInfo);
      this.gbPeakInfo.Location = new System.Drawing.Point(8, 510);
      this.gbPeakInfo.Name = "gbPeakInfo";
      this.gbPeakInfo.Size = new System.Drawing.Size(250, 82);
      this.gbPeakInfo.TabIndex = 2;
      this.gbPeakInfo.TabStop = false;
      this.gbPeakInfo.Text = "Peak information";
      // 
      // lvPeakInfo
      // 
      this.lvPeakInfo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colAbscissa,
            this.colHeight,
            this.colArea});
      this.lvPeakInfo.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lvPeakInfo.FullRowSelect = true;
      this.lvPeakInfo.GridLines = true;
      this.lvPeakInfo.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
      this.lvPeakInfo.HideSelection = false;
      this.lvPeakInfo.Location = new System.Drawing.Point(3, 16);
      this.lvPeakInfo.MultiSelect = false;
      this.lvPeakInfo.Name = "lvPeakInfo";
      this.lvPeakInfo.Size = new System.Drawing.Size(244, 63);
      this.lvPeakInfo.TabIndex = 25;
      this.lvPeakInfo.UseCompatibleStateImageBehavior = false;
      this.lvPeakInfo.View = System.Windows.Forms.View.Details;
      // 
      // colAbscissa
      // 
      this.colAbscissa.Text = "Pos. [pts]";
      // 
      // colHeight
      // 
      this.colHeight.Text = "Height [%]";
      this.colHeight.Width = 70;
      // 
      // colArea
      // 
      this.colArea.Text = "Area [adc*pts]";
      this.colArea.Width = 95;
      // 
      // gbResults
      // 
      this.gbResults.BackColor = System.Drawing.SystemColors.Control;
      this.gbResults.Controls.Add(this._chkRes_15);
      this.gbResults.Controls.Add(this._lblResUnit_15);
      this.gbResults.Controls.Add(this._txtRes_15);
      this.gbResults.Controls.Add(this._lblRes_15);
      this.gbResults.Controls.Add(this._chkRes_14);
      this.gbResults.Controls.Add(this._lblResUnit_14);
      this.gbResults.Controls.Add(this._txtRes_14);
      this.gbResults.Controls.Add(this._lblRes_14);
      this.gbResults.Controls.Add(this._chkRes_13);
      this.gbResults.Controls.Add(this._lblResUnit_13);
      this.gbResults.Controls.Add(this._txtRes_13);
      this.gbResults.Controls.Add(this._lblRes_13);
      this.gbResults.Controls.Add(this._chkRes_12);
      this.gbResults.Controls.Add(this._lblResUnit_12);
      this.gbResults.Controls.Add(this._txtRes_12);
      this.gbResults.Controls.Add(this._lblRes_12);
      this.gbResults.Controls.Add(this._chkRes_11);
      this.gbResults.Controls.Add(this._lblResUnit_11);
      this.gbResults.Controls.Add(this._txtRes_11);
      this.gbResults.Controls.Add(this._lblRes_11);
      this.gbResults.Controls.Add(this._chkRes_10);
      this.gbResults.Controls.Add(this._lblResUnit_10);
      this.gbResults.Controls.Add(this._txtRes_10);
      this.gbResults.Controls.Add(this._lblRes_10);
      this.gbResults.Controls.Add(this._chkRes_9);
      this.gbResults.Controls.Add(this._lblResUnit_9);
      this.gbResults.Controls.Add(this._txtRes_9);
      this.gbResults.Controls.Add(this._lblRes_9);
      this.gbResults.Controls.Add(this._chkRes_8);
      this.gbResults.Controls.Add(this._lblResUnit_8);
      this.gbResults.Controls.Add(this._txtRes_8);
      this.gbResults.Controls.Add(this._lblRes_8);
      this.gbResults.Controls.Add(this._chkRes_7);
      this.gbResults.Controls.Add(this._lblResUnit_7);
      this.gbResults.Controls.Add(this._txtRes_7);
      this.gbResults.Controls.Add(this._lblRes_7);
      this.gbResults.Controls.Add(this._chkRes_6);
      this.gbResults.Controls.Add(this._lblResUnit_6);
      this.gbResults.Controls.Add(this._txtRes_6);
      this.gbResults.Controls.Add(this._lblRes_6);
      this.gbResults.Controls.Add(this._chkRes_5);
      this.gbResults.Controls.Add(this._lblResUnit_5);
      this.gbResults.Controls.Add(this._txtRes_5);
      this.gbResults.Controls.Add(this._lblRes_5);
      this.gbResults.Controls.Add(this._chkRes_4);
      this.gbResults.Controls.Add(this._lblResUnit_4);
      this.gbResults.Controls.Add(this._txtRes_4);
      this.gbResults.Controls.Add(this._lblRes_4);
      this.gbResults.Controls.Add(this._chkRes_3);
      this.gbResults.Controls.Add(this._lblResUnit_3);
      this.gbResults.Controls.Add(this._txtRes_3);
      this.gbResults.Controls.Add(this._lblRes_3);
      this.gbResults.Controls.Add(this._chkRes_2);
      this.gbResults.Controls.Add(this._lblResUnit_2);
      this.gbResults.Controls.Add(this._txtRes_2);
      this.gbResults.Controls.Add(this._lblRes_2);
      this.gbResults.Controls.Add(this._chkRes_1);
      this.gbResults.Controls.Add(this._lblResUnit_1);
      this.gbResults.Controls.Add(this._txtRes_1);
      this.gbResults.Controls.Add(this._lblRes_1);
      this.gbResults.Controls.Add(this._chkRes_0);
      this.gbResults.Controls.Add(this._lblResUnit_0);
      this.gbResults.Controls.Add(this._txtRes_0);
      this.gbResults.Controls.Add(this._lblRes_0);
      this.gbResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.gbResults.ForeColor = System.Drawing.SystemColors.ControlText;
      this.gbResults.Location = new System.Drawing.Point(8, 168);
      this.gbResults.Name = "gbResults";
      this.gbResults.Size = new System.Drawing.Size(250, 340);
      this.gbResults.TabIndex = 1;
      this.gbResults.TabStop = false;
      this.gbResults.Text = "Results";
      // 
      // _chkRes_15
      // 
      this._chkRes_15.Location = new System.Drawing.Point(228, 320);
      this._chkRes_15.Name = "_chkRes_15";
      this._chkRes_15.Size = new System.Drawing.Size(20, 18);
      this._chkRes_15.TabIndex = 63;
      this._chkRes_15.CheckedChanged += new System.EventHandler(this._chk_CheckedChanged);
      // 
      // _lblResUnit_15
      // 
      this._lblResUnit_15.Location = new System.Drawing.Point(178, 320);
      this._lblResUnit_15.Name = "_lblResUnit_15";
      this._lblResUnit_15.Size = new System.Drawing.Size(48, 18);
      this._lblResUnit_15.TabIndex = 62;
      this._lblResUnit_15.Text = "ppm";
      // 
      // _txtRes_15
      // 
      this._txtRes_15.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtRes_15.Location = new System.Drawing.Point(108, 320);
      this._txtRes_15.Name = "_txtRes_15";
      this._txtRes_15.ReadOnly = true;
      this._txtRes_15.Size = new System.Drawing.Size(68, 13);
      this._txtRes_15.TabIndex = 61;
      this._txtRes_15.Text = "<sample>";
      // 
      // _lblRes_15
      // 
      this._lblRes_15.Location = new System.Drawing.Point(8, 320);
      this._lblRes_15.Name = "_lblRes_15";
      this._lblRes_15.Size = new System.Drawing.Size(98, 18);
      this._lblRes_15.TabIndex = 60;
      this._lblRes_15.Text = "Window 0";
      // 
      // _chkRes_14
      // 
      this._chkRes_14.Location = new System.Drawing.Point(228, 300);
      this._chkRes_14.Name = "_chkRes_14";
      this._chkRes_14.Size = new System.Drawing.Size(20, 18);
      this._chkRes_14.TabIndex = 59;
      this._chkRes_14.CheckedChanged += new System.EventHandler(this._chk_CheckedChanged);
      // 
      // _lblResUnit_14
      // 
      this._lblResUnit_14.Location = new System.Drawing.Point(178, 300);
      this._lblResUnit_14.Name = "_lblResUnit_14";
      this._lblResUnit_14.Size = new System.Drawing.Size(48, 18);
      this._lblResUnit_14.TabIndex = 58;
      this._lblResUnit_14.Text = "ppm";
      // 
      // _txtRes_14
      // 
      this._txtRes_14.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtRes_14.Location = new System.Drawing.Point(108, 300);
      this._txtRes_14.Name = "_txtRes_14";
      this._txtRes_14.ReadOnly = true;
      this._txtRes_14.Size = new System.Drawing.Size(68, 13);
      this._txtRes_14.TabIndex = 57;
      this._txtRes_14.Text = "<sample>";
      // 
      // _lblRes_14
      // 
      this._lblRes_14.Location = new System.Drawing.Point(8, 300);
      this._lblRes_14.Name = "_lblRes_14";
      this._lblRes_14.Size = new System.Drawing.Size(98, 18);
      this._lblRes_14.TabIndex = 56;
      this._lblRes_14.Text = "Window 0";
      // 
      // _chkRes_13
      // 
      this._chkRes_13.Location = new System.Drawing.Point(228, 280);
      this._chkRes_13.Name = "_chkRes_13";
      this._chkRes_13.Size = new System.Drawing.Size(20, 18);
      this._chkRes_13.TabIndex = 55;
      this._chkRes_13.CheckedChanged += new System.EventHandler(this._chk_CheckedChanged);
      // 
      // _lblResUnit_13
      // 
      this._lblResUnit_13.Location = new System.Drawing.Point(178, 280);
      this._lblResUnit_13.Name = "_lblResUnit_13";
      this._lblResUnit_13.Size = new System.Drawing.Size(48, 18);
      this._lblResUnit_13.TabIndex = 54;
      this._lblResUnit_13.Text = "ppm";
      // 
      // _txtRes_13
      // 
      this._txtRes_13.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtRes_13.Location = new System.Drawing.Point(108, 280);
      this._txtRes_13.Name = "_txtRes_13";
      this._txtRes_13.ReadOnly = true;
      this._txtRes_13.Size = new System.Drawing.Size(68, 13);
      this._txtRes_13.TabIndex = 53;
      this._txtRes_13.Text = "<sample>";
      // 
      // _lblRes_13
      // 
      this._lblRes_13.Location = new System.Drawing.Point(8, 280);
      this._lblRes_13.Name = "_lblRes_13";
      this._lblRes_13.Size = new System.Drawing.Size(98, 18);
      this._lblRes_13.TabIndex = 52;
      this._lblRes_13.Text = "Window 0";
      // 
      // _chkRes_12
      // 
      this._chkRes_12.Location = new System.Drawing.Point(228, 260);
      this._chkRes_12.Name = "_chkRes_12";
      this._chkRes_12.Size = new System.Drawing.Size(20, 18);
      this._chkRes_12.TabIndex = 51;
      this._chkRes_12.CheckedChanged += new System.EventHandler(this._chk_CheckedChanged);
      // 
      // _lblResUnit_12
      // 
      this._lblResUnit_12.Location = new System.Drawing.Point(178, 260);
      this._lblResUnit_12.Name = "_lblResUnit_12";
      this._lblResUnit_12.Size = new System.Drawing.Size(48, 18);
      this._lblResUnit_12.TabIndex = 50;
      this._lblResUnit_12.Text = "ppm";
      // 
      // _txtRes_12
      // 
      this._txtRes_12.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtRes_12.Location = new System.Drawing.Point(108, 260);
      this._txtRes_12.Name = "_txtRes_12";
      this._txtRes_12.ReadOnly = true;
      this._txtRes_12.Size = new System.Drawing.Size(68, 13);
      this._txtRes_12.TabIndex = 49;
      this._txtRes_12.Text = "<sample>";
      // 
      // _lblRes_12
      // 
      this._lblRes_12.Location = new System.Drawing.Point(8, 260);
      this._lblRes_12.Name = "_lblRes_12";
      this._lblRes_12.Size = new System.Drawing.Size(98, 18);
      this._lblRes_12.TabIndex = 48;
      this._lblRes_12.Text = "Window 0";
      // 
      // _chkRes_11
      // 
      this._chkRes_11.Location = new System.Drawing.Point(228, 240);
      this._chkRes_11.Name = "_chkRes_11";
      this._chkRes_11.Size = new System.Drawing.Size(20, 18);
      this._chkRes_11.TabIndex = 47;
      this._chkRes_11.CheckedChanged += new System.EventHandler(this._chk_CheckedChanged);
      // 
      // _lblResUnit_11
      // 
      this._lblResUnit_11.Location = new System.Drawing.Point(178, 240);
      this._lblResUnit_11.Name = "_lblResUnit_11";
      this._lblResUnit_11.Size = new System.Drawing.Size(48, 18);
      this._lblResUnit_11.TabIndex = 46;
      this._lblResUnit_11.Text = "ppm";
      // 
      // _txtRes_11
      // 
      this._txtRes_11.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtRes_11.Location = new System.Drawing.Point(108, 240);
      this._txtRes_11.Name = "_txtRes_11";
      this._txtRes_11.ReadOnly = true;
      this._txtRes_11.Size = new System.Drawing.Size(68, 13);
      this._txtRes_11.TabIndex = 45;
      this._txtRes_11.Text = "<sample>";
      // 
      // _lblRes_11
      // 
      this._lblRes_11.Location = new System.Drawing.Point(8, 240);
      this._lblRes_11.Name = "_lblRes_11";
      this._lblRes_11.Size = new System.Drawing.Size(98, 18);
      this._lblRes_11.TabIndex = 44;
      this._lblRes_11.Text = "Window 0";
      // 
      // _chkRes_10
      // 
      this._chkRes_10.Location = new System.Drawing.Point(228, 220);
      this._chkRes_10.Name = "_chkRes_10";
      this._chkRes_10.Size = new System.Drawing.Size(20, 18);
      this._chkRes_10.TabIndex = 43;
      this._chkRes_10.CheckedChanged += new System.EventHandler(this._chk_CheckedChanged);
      // 
      // _lblResUnit_10
      // 
      this._lblResUnit_10.Location = new System.Drawing.Point(178, 220);
      this._lblResUnit_10.Name = "_lblResUnit_10";
      this._lblResUnit_10.Size = new System.Drawing.Size(48, 18);
      this._lblResUnit_10.TabIndex = 42;
      this._lblResUnit_10.Text = "ppm";
      // 
      // _txtRes_10
      // 
      this._txtRes_10.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtRes_10.Location = new System.Drawing.Point(108, 220);
      this._txtRes_10.Name = "_txtRes_10";
      this._txtRes_10.ReadOnly = true;
      this._txtRes_10.Size = new System.Drawing.Size(68, 13);
      this._txtRes_10.TabIndex = 41;
      this._txtRes_10.Text = "<sample>";
      // 
      // _lblRes_10
      // 
      this._lblRes_10.Location = new System.Drawing.Point(8, 220);
      this._lblRes_10.Name = "_lblRes_10";
      this._lblRes_10.Size = new System.Drawing.Size(98, 18);
      this._lblRes_10.TabIndex = 40;
      this._lblRes_10.Text = "Window 0";
      // 
      // _chkRes_9
      // 
      this._chkRes_9.Location = new System.Drawing.Point(228, 200);
      this._chkRes_9.Name = "_chkRes_9";
      this._chkRes_9.Size = new System.Drawing.Size(20, 18);
      this._chkRes_9.TabIndex = 39;
      this._chkRes_9.CheckedChanged += new System.EventHandler(this._chk_CheckedChanged);
      // 
      // _lblResUnit_9
      // 
      this._lblResUnit_9.Location = new System.Drawing.Point(178, 200);
      this._lblResUnit_9.Name = "_lblResUnit_9";
      this._lblResUnit_9.Size = new System.Drawing.Size(48, 18);
      this._lblResUnit_9.TabIndex = 38;
      this._lblResUnit_9.Text = "ppm";
      // 
      // _txtRes_9
      // 
      this._txtRes_9.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtRes_9.Location = new System.Drawing.Point(108, 200);
      this._txtRes_9.Name = "_txtRes_9";
      this._txtRes_9.ReadOnly = true;
      this._txtRes_9.Size = new System.Drawing.Size(68, 13);
      this._txtRes_9.TabIndex = 37;
      this._txtRes_9.Text = "<sample>";
      // 
      // _lblRes_9
      // 
      this._lblRes_9.Location = new System.Drawing.Point(8, 200);
      this._lblRes_9.Name = "_lblRes_9";
      this._lblRes_9.Size = new System.Drawing.Size(98, 18);
      this._lblRes_9.TabIndex = 36;
      this._lblRes_9.Text = "Window 0";
      // 
      // _chkRes_8
      // 
      this._chkRes_8.Location = new System.Drawing.Point(228, 180);
      this._chkRes_8.Name = "_chkRes_8";
      this._chkRes_8.Size = new System.Drawing.Size(20, 18);
      this._chkRes_8.TabIndex = 35;
      this._chkRes_8.CheckedChanged += new System.EventHandler(this._chk_CheckedChanged);
      // 
      // _lblResUnit_8
      // 
      this._lblResUnit_8.Location = new System.Drawing.Point(178, 180);
      this._lblResUnit_8.Name = "_lblResUnit_8";
      this._lblResUnit_8.Size = new System.Drawing.Size(48, 18);
      this._lblResUnit_8.TabIndex = 34;
      this._lblResUnit_8.Text = "ppm";
      // 
      // _txtRes_8
      // 
      this._txtRes_8.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtRes_8.Location = new System.Drawing.Point(108, 180);
      this._txtRes_8.Name = "_txtRes_8";
      this._txtRes_8.ReadOnly = true;
      this._txtRes_8.Size = new System.Drawing.Size(68, 13);
      this._txtRes_8.TabIndex = 33;
      this._txtRes_8.Text = "<sample>";
      // 
      // _lblRes_8
      // 
      this._lblRes_8.Location = new System.Drawing.Point(8, 180);
      this._lblRes_8.Name = "_lblRes_8";
      this._lblRes_8.Size = new System.Drawing.Size(98, 18);
      this._lblRes_8.TabIndex = 32;
      this._lblRes_8.Text = "Window 0";
      // 
      // _chkRes_7
      // 
      this._chkRes_7.Location = new System.Drawing.Point(228, 160);
      this._chkRes_7.Name = "_chkRes_7";
      this._chkRes_7.Size = new System.Drawing.Size(20, 18);
      this._chkRes_7.TabIndex = 31;
      this._chkRes_7.CheckedChanged += new System.EventHandler(this._chk_CheckedChanged);
      // 
      // _lblResUnit_7
      // 
      this._lblResUnit_7.Location = new System.Drawing.Point(178, 160);
      this._lblResUnit_7.Name = "_lblResUnit_7";
      this._lblResUnit_7.Size = new System.Drawing.Size(48, 18);
      this._lblResUnit_7.TabIndex = 30;
      this._lblResUnit_7.Text = "ppm";
      // 
      // _txtRes_7
      // 
      this._txtRes_7.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtRes_7.Location = new System.Drawing.Point(108, 160);
      this._txtRes_7.Name = "_txtRes_7";
      this._txtRes_7.ReadOnly = true;
      this._txtRes_7.Size = new System.Drawing.Size(68, 13);
      this._txtRes_7.TabIndex = 29;
      this._txtRes_7.Text = "<sample>";
      // 
      // _lblRes_7
      // 
      this._lblRes_7.Location = new System.Drawing.Point(8, 160);
      this._lblRes_7.Name = "_lblRes_7";
      this._lblRes_7.Size = new System.Drawing.Size(98, 18);
      this._lblRes_7.TabIndex = 28;
      this._lblRes_7.Text = "Window 0";
      // 
      // _chkRes_6
      // 
      this._chkRes_6.Location = new System.Drawing.Point(228, 140);
      this._chkRes_6.Name = "_chkRes_6";
      this._chkRes_6.Size = new System.Drawing.Size(20, 18);
      this._chkRes_6.TabIndex = 27;
      this._chkRes_6.CheckedChanged += new System.EventHandler(this._chk_CheckedChanged);
      // 
      // _lblResUnit_6
      // 
      this._lblResUnit_6.Location = new System.Drawing.Point(178, 140);
      this._lblResUnit_6.Name = "_lblResUnit_6";
      this._lblResUnit_6.Size = new System.Drawing.Size(48, 18);
      this._lblResUnit_6.TabIndex = 26;
      this._lblResUnit_6.Text = "ppm";
      // 
      // _txtRes_6
      // 
      this._txtRes_6.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtRes_6.Location = new System.Drawing.Point(108, 140);
      this._txtRes_6.Name = "_txtRes_6";
      this._txtRes_6.ReadOnly = true;
      this._txtRes_6.Size = new System.Drawing.Size(68, 13);
      this._txtRes_6.TabIndex = 25;
      this._txtRes_6.Text = "<sample>";
      // 
      // _lblRes_6
      // 
      this._lblRes_6.Location = new System.Drawing.Point(8, 140);
      this._lblRes_6.Name = "_lblRes_6";
      this._lblRes_6.Size = new System.Drawing.Size(98, 18);
      this._lblRes_6.TabIndex = 24;
      this._lblRes_6.Text = "Window 0";
      // 
      // _chkRes_5
      // 
      this._chkRes_5.Location = new System.Drawing.Point(228, 120);
      this._chkRes_5.Name = "_chkRes_5";
      this._chkRes_5.Size = new System.Drawing.Size(20, 18);
      this._chkRes_5.TabIndex = 23;
      this._chkRes_5.CheckedChanged += new System.EventHandler(this._chk_CheckedChanged);
      // 
      // _lblResUnit_5
      // 
      this._lblResUnit_5.Location = new System.Drawing.Point(178, 120);
      this._lblResUnit_5.Name = "_lblResUnit_5";
      this._lblResUnit_5.Size = new System.Drawing.Size(48, 18);
      this._lblResUnit_5.TabIndex = 22;
      this._lblResUnit_5.Text = "ppm";
      // 
      // _txtRes_5
      // 
      this._txtRes_5.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtRes_5.Location = new System.Drawing.Point(108, 120);
      this._txtRes_5.Name = "_txtRes_5";
      this._txtRes_5.ReadOnly = true;
      this._txtRes_5.Size = new System.Drawing.Size(68, 13);
      this._txtRes_5.TabIndex = 21;
      this._txtRes_5.Text = "<sample>";
      // 
      // _lblRes_5
      // 
      this._lblRes_5.Location = new System.Drawing.Point(8, 120);
      this._lblRes_5.Name = "_lblRes_5";
      this._lblRes_5.Size = new System.Drawing.Size(98, 18);
      this._lblRes_5.TabIndex = 20;
      this._lblRes_5.Text = "Window 0";
      // 
      // _chkRes_4
      // 
      this._chkRes_4.Location = new System.Drawing.Point(228, 100);
      this._chkRes_4.Name = "_chkRes_4";
      this._chkRes_4.Size = new System.Drawing.Size(20, 18);
      this._chkRes_4.TabIndex = 19;
      this._chkRes_4.CheckedChanged += new System.EventHandler(this._chk_CheckedChanged);
      // 
      // _lblResUnit_4
      // 
      this._lblResUnit_4.Location = new System.Drawing.Point(178, 100);
      this._lblResUnit_4.Name = "_lblResUnit_4";
      this._lblResUnit_4.Size = new System.Drawing.Size(48, 18);
      this._lblResUnit_4.TabIndex = 18;
      this._lblResUnit_4.Text = "ppm";
      // 
      // _txtRes_4
      // 
      this._txtRes_4.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtRes_4.Location = new System.Drawing.Point(108, 100);
      this._txtRes_4.Name = "_txtRes_4";
      this._txtRes_4.ReadOnly = true;
      this._txtRes_4.Size = new System.Drawing.Size(68, 13);
      this._txtRes_4.TabIndex = 17;
      this._txtRes_4.Text = "<sample>";
      // 
      // _lblRes_4
      // 
      this._lblRes_4.Location = new System.Drawing.Point(8, 100);
      this._lblRes_4.Name = "_lblRes_4";
      this._lblRes_4.Size = new System.Drawing.Size(98, 18);
      this._lblRes_4.TabIndex = 16;
      this._lblRes_4.Text = "Window 0";
      // 
      // _chkRes_3
      // 
      this._chkRes_3.Location = new System.Drawing.Point(228, 80);
      this._chkRes_3.Name = "_chkRes_3";
      this._chkRes_3.Size = new System.Drawing.Size(20, 18);
      this._chkRes_3.TabIndex = 15;
      this._chkRes_3.CheckedChanged += new System.EventHandler(this._chk_CheckedChanged);
      // 
      // _lblResUnit_3
      // 
      this._lblResUnit_3.Location = new System.Drawing.Point(178, 80);
      this._lblResUnit_3.Name = "_lblResUnit_3";
      this._lblResUnit_3.Size = new System.Drawing.Size(48, 18);
      this._lblResUnit_3.TabIndex = 14;
      this._lblResUnit_3.Text = "ppm";
      // 
      // _txtRes_3
      // 
      this._txtRes_3.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtRes_3.Location = new System.Drawing.Point(108, 80);
      this._txtRes_3.Name = "_txtRes_3";
      this._txtRes_3.ReadOnly = true;
      this._txtRes_3.Size = new System.Drawing.Size(68, 13);
      this._txtRes_3.TabIndex = 13;
      this._txtRes_3.Text = "<sample>";
      // 
      // _lblRes_3
      // 
      this._lblRes_3.Location = new System.Drawing.Point(8, 80);
      this._lblRes_3.Name = "_lblRes_3";
      this._lblRes_3.Size = new System.Drawing.Size(98, 18);
      this._lblRes_3.TabIndex = 12;
      this._lblRes_3.Text = "Window 0";
      // 
      // _chkRes_2
      // 
      this._chkRes_2.Location = new System.Drawing.Point(228, 60);
      this._chkRes_2.Name = "_chkRes_2";
      this._chkRes_2.Size = new System.Drawing.Size(20, 18);
      this._chkRes_2.TabIndex = 11;
      this._chkRes_2.CheckedChanged += new System.EventHandler(this._chk_CheckedChanged);
      // 
      // _lblResUnit_2
      // 
      this._lblResUnit_2.Location = new System.Drawing.Point(178, 60);
      this._lblResUnit_2.Name = "_lblResUnit_2";
      this._lblResUnit_2.Size = new System.Drawing.Size(48, 18);
      this._lblResUnit_2.TabIndex = 10;
      this._lblResUnit_2.Text = "ppm";
      // 
      // _txtRes_2
      // 
      this._txtRes_2.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtRes_2.Location = new System.Drawing.Point(108, 60);
      this._txtRes_2.Name = "_txtRes_2";
      this._txtRes_2.ReadOnly = true;
      this._txtRes_2.Size = new System.Drawing.Size(68, 13);
      this._txtRes_2.TabIndex = 9;
      this._txtRes_2.Text = "<sample>";
      // 
      // _lblRes_2
      // 
      this._lblRes_2.Location = new System.Drawing.Point(8, 60);
      this._lblRes_2.Name = "_lblRes_2";
      this._lblRes_2.Size = new System.Drawing.Size(98, 18);
      this._lblRes_2.TabIndex = 8;
      this._lblRes_2.Text = "Window 0";
      // 
      // _chkRes_1
      // 
      this._chkRes_1.Location = new System.Drawing.Point(228, 40);
      this._chkRes_1.Name = "_chkRes_1";
      this._chkRes_1.Size = new System.Drawing.Size(20, 18);
      this._chkRes_1.TabIndex = 7;
      this._chkRes_1.CheckedChanged += new System.EventHandler(this._chk_CheckedChanged);
      // 
      // _lblResUnit_1
      // 
      this._lblResUnit_1.Location = new System.Drawing.Point(178, 40);
      this._lblResUnit_1.Name = "_lblResUnit_1";
      this._lblResUnit_1.Size = new System.Drawing.Size(48, 18);
      this._lblResUnit_1.TabIndex = 6;
      this._lblResUnit_1.Text = "ppm";
      // 
      // _txtRes_1
      // 
      this._txtRes_1.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtRes_1.Location = new System.Drawing.Point(108, 40);
      this._txtRes_1.Name = "_txtRes_1";
      this._txtRes_1.ReadOnly = true;
      this._txtRes_1.Size = new System.Drawing.Size(68, 13);
      this._txtRes_1.TabIndex = 5;
      this._txtRes_1.Text = "<sample>";
      // 
      // _lblRes_1
      // 
      this._lblRes_1.Location = new System.Drawing.Point(8, 40);
      this._lblRes_1.Name = "_lblRes_1";
      this._lblRes_1.Size = new System.Drawing.Size(98, 18);
      this._lblRes_1.TabIndex = 4;
      this._lblRes_1.Text = "Window 0";
      // 
      // _chkRes_0
      // 
      this._chkRes_0.Location = new System.Drawing.Point(228, 20);
      this._chkRes_0.Name = "_chkRes_0";
      this._chkRes_0.Size = new System.Drawing.Size(20, 18);
      this._chkRes_0.TabIndex = 3;
      this._chkRes_0.CheckedChanged += new System.EventHandler(this._chk_CheckedChanged);
      // 
      // _lblResUnit_0
      // 
      this._lblResUnit_0.Location = new System.Drawing.Point(178, 20);
      this._lblResUnit_0.Name = "_lblResUnit_0";
      this._lblResUnit_0.Size = new System.Drawing.Size(48, 18);
      this._lblResUnit_0.TabIndex = 2;
      this._lblResUnit_0.Text = "ppm";
      // 
      // _txtRes_0
      // 
      this._txtRes_0.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtRes_0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._txtRes_0.ForeColor = System.Drawing.SystemColors.WindowText;
      this._txtRes_0.Location = new System.Drawing.Point(108, 20);
      this._txtRes_0.Name = "_txtRes_0";
      this._txtRes_0.ReadOnly = true;
      this._txtRes_0.Size = new System.Drawing.Size(68, 13);
      this._txtRes_0.TabIndex = 1;
      this._txtRes_0.Text = "<sample>";
      // 
      // _lblRes_0
      // 
      this._lblRes_0.Location = new System.Drawing.Point(8, 20);
      this._lblRes_0.Name = "_lblRes_0";
      this._lblRes_0.Size = new System.Drawing.Size(98, 18);
      this._lblRes_0.TabIndex = 0;
      this._lblRes_0.Text = "Window 0";
      // 
      // gbDeviceInfo
      // 
      this.gbDeviceInfo.Controls.Add(this._btnErrAck);
      this.gbDeviceInfo.Controls.Add(this.lblWar);
      this.gbDeviceInfo.Controls.Add(this.lblWar_Val);
      this.gbDeviceInfo.Controls.Add(this.lblFlow);
      this.gbDeviceInfo.Controls.Add(this.lblFlow_Val);
      this.gbDeviceInfo.Controls.Add(this.lblDeviceNo);
      this.gbDeviceInfo.Controls.Add(this.lblDeviceNo_Val);
      this.gbDeviceInfo.Controls.Add(this.lblErr);
      this.gbDeviceInfo.Controls.Add(this.lblErr_Val);
      this.gbDeviceInfo.Controls.Add(this.lblOffset);
      this.gbDeviceInfo.Controls.Add(this.lblOffset_Val);
      this.gbDeviceInfo.Controls.Add(this.lblPres);
      this.gbDeviceInfo.Controls.Add(this.lblPres_Val);
      this.gbDeviceInfo.Controls.Add(this.lblTemp);
      this.gbDeviceInfo.Controls.Add(this.lblTemp_Val);
      this.gbDeviceInfo.Controls.Add(this.lblCurScr);
      this.gbDeviceInfo.Controls.Add(this.lblCurScr_Name);
      this.gbDeviceInfo.Location = new System.Drawing.Point(8, 4);
      this.gbDeviceInfo.Name = "gbDeviceInfo";
      this.gbDeviceInfo.Size = new System.Drawing.Size(250, 162);
      this.gbDeviceInfo.TabIndex = 0;
      this.gbDeviceInfo.TabStop = false;
      this.gbDeviceInfo.Text = "Device information";
      // 
      // _btnErrAck
      // 
      this._btnErrAck.Location = new System.Drawing.Point(212, 126);
      this._btnErrAck.Name = "_btnErrAck";
      this._btnErrAck.Size = new System.Drawing.Size(38, 18);
      this._btnErrAck.TabIndex = 28;
      this._btnErrAck.Text = "ACK";
      this._btnErrAck.Click += new System.EventHandler(this._btnErrAck_Click);
      // 
      // lblWar
      // 
      this.lblWar.Location = new System.Drawing.Point(8, 144);
      this.lblWar.Name = "lblWar";
      this.lblWar.Size = new System.Drawing.Size(82, 16);
      this.lblWar.TabIndex = 22;
      this.lblWar.Text = "Warnungen:";
      // 
      // lblWar_Val
      // 
      this.lblWar_Val.Location = new System.Drawing.Point(92, 144);
      this.lblWar_Val.Name = "lblWar_Val";
      this.lblWar_Val.Size = new System.Drawing.Size(118, 16);
      this.lblWar_Val.TabIndex = 21;
      this.lblWar_Val.Text = "<warnings>";
      // 
      // lblFlow
      // 
      this.lblFlow.Location = new System.Drawing.Point(8, 90);
      this.lblFlow.Name = "lblFlow";
      this.lblFlow.Size = new System.Drawing.Size(82, 16);
      this.lblFlow.TabIndex = 20;
      this.lblFlow.Text = "Flow  [ml/min]:";
      // 
      // lblFlow_Val
      // 
      this.lblFlow_Val.Location = new System.Drawing.Point(92, 90);
      this.lblFlow_Val.Name = "lblFlow_Val";
      this.lblFlow_Val.Size = new System.Drawing.Size(152, 16);
      this.lblFlow_Val.TabIndex = 19;
      // 
      // lblDeviceNo
      // 
      this.lblDeviceNo.Location = new System.Drawing.Point(7, 36);
      this.lblDeviceNo.Name = "lblDeviceNo";
      this.lblDeviceNo.Size = new System.Drawing.Size(82, 16);
      this.lblDeviceNo.TabIndex = 18;
      this.lblDeviceNo.Text = "Device No.:";
      // 
      // lblDeviceNo_Val
      // 
      this.lblDeviceNo_Val.Location = new System.Drawing.Point(91, 36);
      this.lblDeviceNo_Val.Name = "lblDeviceNo_Val";
      this.lblDeviceNo_Val.Size = new System.Drawing.Size(152, 16);
      this.lblDeviceNo_Val.TabIndex = 17;
      this.lblDeviceNo_Val.Text = "<devno>";
      // 
      // lblErr
      // 
      this.lblErr.Location = new System.Drawing.Point(7, 126);
      this.lblErr.Name = "lblErr";
      this.lblErr.Size = new System.Drawing.Size(82, 16);
      this.lblErr.TabIndex = 16;
      this.lblErr.Text = "Ger�tefehler:";
      // 
      // lblErr_Val
      // 
      this.lblErr_Val.Location = new System.Drawing.Point(91, 126);
      this.lblErr_Val.Name = "lblErr_Val";
      this.lblErr_Val.Size = new System.Drawing.Size(119, 16);
      this.lblErr_Val.TabIndex = 15;
      this.lblErr_Val.Text = "<errors>";
      // 
      // lblOffset
      // 
      this.lblOffset.Location = new System.Drawing.Point(8, 108);
      this.lblOffset.Name = "lblOffset";
      this.lblOffset.Size = new System.Drawing.Size(82, 16);
      this.lblOffset.TabIndex = 14;
      this.lblOffset.Text = "Offset [adc]:";
      // 
      // lblOffset_Val
      // 
      this.lblOffset_Val.Location = new System.Drawing.Point(92, 108);
      this.lblOffset_Val.Name = "lblOffset_Val";
      this.lblOffset_Val.Size = new System.Drawing.Size(152, 16);
      this.lblOffset_Val.TabIndex = 13;
      // 
      // lblPres
      // 
      this.lblPres.Location = new System.Drawing.Point(8, 72);
      this.lblPres.Name = "lblPres";
      this.lblPres.Size = new System.Drawing.Size(82, 16);
      this.lblPres.TabIndex = 12;
      this.lblPres.Text = "Pres. [kPa]:";
      // 
      // lblPres_Val
      // 
      this.lblPres_Val.Location = new System.Drawing.Point(92, 72);
      this.lblPres_Val.Name = "lblPres_Val";
      this.lblPres_Val.Size = new System.Drawing.Size(152, 16);
      this.lblPres_Val.TabIndex = 11;
      // 
      // lblTemp
      // 
      this.lblTemp.Location = new System.Drawing.Point(8, 54);
      this.lblTemp.Name = "lblTemp";
      this.lblTemp.Size = new System.Drawing.Size(82, 16);
      this.lblTemp.TabIndex = 10;
      this.lblTemp.Text = "Temp. [�C]:";
      // 
      // lblTemp_Val
      // 
      this.lblTemp_Val.Location = new System.Drawing.Point(92, 54);
      this.lblTemp_Val.Name = "lblTemp_Val";
      this.lblTemp_Val.Size = new System.Drawing.Size(152, 16);
      this.lblTemp_Val.TabIndex = 9;
      // 
      // lblCurScr
      // 
      this.lblCurScr.Location = new System.Drawing.Point(8, 18);
      this.lblCurScr.Name = "lblCurScr";
      this.lblCurScr.Size = new System.Drawing.Size(48, 16);
      this.lblCurScr.TabIndex = 8;
      this.lblCurScr.Text = "Script:";
      // 
      // lblCurScr_Name
      // 
      this.lblCurScr_Name.Location = new System.Drawing.Point(58, 18);
      this.lblCurScr_Name.Name = "lblCurScr_Name";
      this.lblCurScr_Name.Size = new System.Drawing.Size(186, 16);
      this.lblCurScr_Name.TabIndex = 7;
      this.lblCurScr_Name.Text = "<curscriptname>";
      // 
      // GraphPanel
      // 
      this.GraphPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.GraphPanel.Controls.Add(this.lblOffline);
      this.GraphPanel.Controls.Add(this.lblY);
      this.GraphPanel.Controls.Add(this.lblX);
      this.GraphPanel.Controls.Add(this.cbScansToDraw);
      this.GraphPanel.Controls.Add(this.lblNoOfPts_Val);
      this.GraphPanel.Controls.Add(this.lblNoOfPts);
      this.GraphPanel.Controls.Add(this.lblYunit);
      this.GraphPanel.Controls.Add(this.lblXunit);
      this.GraphPanel.Controls.Add(this._btnPlot_FitY);
      this.GraphPanel.Controls.Add(this._btnPlot_FitX);
      this.GraphPanel.Controls.Add(this._btnPlot_FitDef);
      this.GraphPanel.Controls.Add(this._btnPlot_FitXY);
      this.GraphPanel.Controls.Add(this.lblYcur);
      this.GraphPanel.Controls.Add(this.lblXcur);
      this.GraphPanel.Controls.Add(this.lblXmax);
      this.GraphPanel.Controls.Add(this.lblXmin);
      this.GraphPanel.Controls.Add(this.lblYmin);
      this.GraphPanel.Controls.Add(this.lblYmax);
      this.GraphPanel.Controls.Add(this.pbDraw);
      this.GraphPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.GraphPanel.Location = new System.Drawing.Point(0, 0);
      this.GraphPanel.Name = "GraphPanel";
      this.GraphPanel.Size = new System.Drawing.Size(630, 539);
      this.GraphPanel.TabIndex = 7;
      // 
      // lblOffline
      // 
      this.lblOffline.Image = ((System.Drawing.Image)(resources.GetObject("lblOffline.Image")));
      this.lblOffline.Location = new System.Drawing.Point(32, 68);
      this.lblOffline.Name = "lblOffline";
      this.lblOffline.Size = new System.Drawing.Size(16, 160);
      this.lblOffline.TabIndex = 27;
      // 
      // lblY
      // 
      this.lblY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lblY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblY.Location = new System.Drawing.Point(320, 8);
      this.lblY.Name = "lblY";
      this.lblY.Size = new System.Drawing.Size(20, 20);
      this.lblY.TabIndex = 26;
      this.lblY.Text = "Y:";
      this.lblY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblX
      // 
      this.lblX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lblX.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblX.Location = new System.Drawing.Point(234, 8);
      this.lblX.Name = "lblX";
      this.lblX.Size = new System.Drawing.Size(20, 20);
      this.lblX.TabIndex = 25;
      this.lblX.Text = "X:";
      this.lblX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // cbScansToDraw
      // 
      this.cbScansToDraw.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.cbScansToDraw.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbScansToDraw.Location = new System.Drawing.Point(412, 8);
      this.cbScansToDraw.Name = "cbScansToDraw";
      this.cbScansToDraw.Size = new System.Drawing.Size(102, 21);
      this.cbScansToDraw.TabIndex = 24;
      this.cbScansToDraw.SelectedIndexChanged += new System.EventHandler(this.cbScansToDraw_SelectedIndexChanged);
      // 
      // lblNoOfPts_Val
      // 
      this.lblNoOfPts_Val.Location = new System.Drawing.Point(128, 8);
      this.lblNoOfPts_Val.Name = "lblNoOfPts_Val";
      this.lblNoOfPts_Val.Size = new System.Drawing.Size(68, 20);
      this.lblNoOfPts_Val.TabIndex = 23;
      this.lblNoOfPts_Val.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lblNoOfPts
      // 
      this.lblNoOfPts.Location = new System.Drawing.Point(82, 8);
      this.lblNoOfPts.Name = "lblNoOfPts";
      this.lblNoOfPts.Size = new System.Drawing.Size(44, 20);
      this.lblNoOfPts.TabIndex = 22;
      this.lblNoOfPts.Text = "Punkte:";
      this.lblNoOfPts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lblYunit
      // 
      this.lblYunit.Location = new System.Drawing.Point(8, 240);
      this.lblYunit.Name = "lblYunit";
      this.lblYunit.Size = new System.Drawing.Size(68, 20);
      this.lblYunit.TabIndex = 21;
      this.lblYunit.Text = "[%]";
      this.lblYunit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblXunit
      // 
      this.lblXunit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.lblXunit.Location = new System.Drawing.Point(315, 510);
      this.lblXunit.Name = "lblXunit";
      this.lblXunit.Size = new System.Drawing.Size(68, 20);
      this.lblXunit.TabIndex = 20;
      this.lblXunit.Text = "[pts]";
      this.lblXunit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // _btnPlot_FitY
      // 
      this._btnPlot_FitY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this._btnPlot_FitY.Image = ((System.Drawing.Image)(resources.GetObject("_btnPlot_FitY.Image")));
      this._btnPlot_FitY.Location = new System.Drawing.Point(576, 10);
      this._btnPlot_FitY.Name = "_btnPlot_FitY";
      this._btnPlot_FitY.Size = new System.Drawing.Size(16, 16);
      this._btnPlot_FitY.TabIndex = 18;
      this._ToolTip.SetToolTip(this._btnPlot_FitY, "Fit to Y");
      this._btnPlot_FitY.Click += new System.EventHandler(this._btnPlot_Click);
      this._btnPlot_FitY.MouseHover += new System.EventHandler(this._btnPlot_MouseHover);
      // 
      // _btnPlot_FitX
      // 
      this._btnPlot_FitX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this._btnPlot_FitX.Image = ((System.Drawing.Image)(resources.GetObject("_btnPlot_FitX.Image")));
      this._btnPlot_FitX.Location = new System.Drawing.Point(550, 10);
      this._btnPlot_FitX.Name = "_btnPlot_FitX";
      this._btnPlot_FitX.Size = new System.Drawing.Size(16, 16);
      this._btnPlot_FitX.TabIndex = 17;
      this._ToolTip.SetToolTip(this._btnPlot_FitX, "Fit to X");
      this._btnPlot_FitX.Click += new System.EventHandler(this._btnPlot_Click);
      this._btnPlot_FitX.MouseHover += new System.EventHandler(this._btnPlot_MouseHover);
      // 
      // _btnPlot_FitDef
      // 
      this._btnPlot_FitDef.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this._btnPlot_FitDef.Image = ((System.Drawing.Image)(resources.GetObject("_btnPlot_FitDef.Image")));
      this._btnPlot_FitDef.Location = new System.Drawing.Point(524, 10);
      this._btnPlot_FitDef.Name = "_btnPlot_FitDef";
      this._btnPlot_FitDef.Size = new System.Drawing.Size(16, 16);
      this._btnPlot_FitDef.TabIndex = 16;
      this._ToolTip.SetToolTip(this._btnPlot_FitDef, "Fit to Default");
      this._btnPlot_FitDef.Click += new System.EventHandler(this._btnPlot_Click);
      this._btnPlot_FitDef.MouseHover += new System.EventHandler(this._btnPlot_MouseHover);
      // 
      // _btnPlot_FitXY
      // 
      this._btnPlot_FitXY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this._btnPlot_FitXY.Image = ((System.Drawing.Image)(resources.GetObject("_btnPlot_FitXY.Image")));
      this._btnPlot_FitXY.Location = new System.Drawing.Point(602, 10);
      this._btnPlot_FitXY.Name = "_btnPlot_FitXY";
      this._btnPlot_FitXY.Size = new System.Drawing.Size(16, 16);
      this._btnPlot_FitXY.TabIndex = 15;
      this._ToolTip.SetToolTip(this._btnPlot_FitXY, "Fit to XY");
      this._btnPlot_FitXY.Click += new System.EventHandler(this._btnPlot_Click);
      this._btnPlot_FitXY.MouseHover += new System.EventHandler(this._btnPlot_MouseHover);
      // 
      // lblYcur
      // 
      this.lblYcur.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lblYcur.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblYcur.Location = new System.Drawing.Point(342, 8);
      this.lblYcur.Name = "lblYcur";
      this.lblYcur.Size = new System.Drawing.Size(62, 20);
      this.lblYcur.TabIndex = 14;
      this.lblYcur.Text = "50.00";
      this.lblYcur.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblXcur
      // 
      this.lblXcur.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lblXcur.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblXcur.Location = new System.Drawing.Point(256, 8);
      this.lblXcur.Name = "lblXcur";
      this.lblXcur.Size = new System.Drawing.Size(62, 20);
      this.lblXcur.TabIndex = 13;
      this.lblXcur.Text = "10.00";
      this.lblXcur.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblXmax
      // 
      this.lblXmax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.lblXmax.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblXmax.Location = new System.Drawing.Point(550, 510);
      this.lblXmax.Name = "lblXmax";
      this.lblXmax.Size = new System.Drawing.Size(68, 20);
      this.lblXmax.TabIndex = 12;
      this.lblXmax.Text = "20.00";
      this.lblXmax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblXmin
      // 
      this.lblXmin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.lblXmin.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblXmin.Location = new System.Drawing.Point(82, 510);
      this.lblXmin.Name = "lblXmin";
      this.lblXmin.Size = new System.Drawing.Size(68, 20);
      this.lblXmin.TabIndex = 11;
      this.lblXmin.Text = "0.00";
      this.lblXmin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lblYmin
      // 
      this.lblYmin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.lblYmin.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblYmin.Location = new System.Drawing.Point(8, 484);
      this.lblYmin.Name = "lblYmin";
      this.lblYmin.Size = new System.Drawing.Size(68, 20);
      this.lblYmin.TabIndex = 10;
      this.lblYmin.Text = "-10.00";
      this.lblYmin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblYmax
      // 
      this.lblYmax.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblYmax.Location = new System.Drawing.Point(8, 32);
      this.lblYmax.Name = "lblYmax";
      this.lblYmax.Size = new System.Drawing.Size(68, 20);
      this.lblYmax.TabIndex = 9;
      this.lblYmax.Text = "110.00";
      this.lblYmax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // pbDraw
      // 
      this.pbDraw.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.pbDraw.BackColor = System.Drawing.SystemColors.ControlLightLight;
      this.pbDraw.Location = new System.Drawing.Point(82, 34);
      this.pbDraw.Name = "pbDraw";
      this.pbDraw.Size = new System.Drawing.Size(536, 470);
      this.pbDraw.TabIndex = 0;
      this.pbDraw.TabStop = false;
      this.pbDraw.SizeChanged += new System.EventHandler(this.pbDraw_SizeChanged);
      this.pbDraw.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbDraw_MouseDown);
      this.pbDraw.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbDraw_MouseMove);
      this.pbDraw.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbDraw_MouseUp);
      // 
      // _Timer
      // 
      this._Timer.Tick += new System.EventHandler(this._Timer_Tick);
      // 
      // btnAlarm
      // 
      this.btnAlarm.BackColor = System.Drawing.Color.Red;
      this.btnAlarm.Dock = System.Windows.Forms.DockStyle.Right;
      this.btnAlarm.ForeColor = System.Drawing.Color.White;
      this.btnAlarm.Location = new System.Drawing.Point(544, 0);
      this.btnAlarm.Name = "btnAlarm";
      this.btnAlarm.Size = new System.Drawing.Size(86, 58);
      this.btnAlarm.TabIndex = 14;
      this.btnAlarm.Text = "Alarm (Bitte best�tigen)";
      this.btnAlarm.UseVisualStyleBackColor = false;
      this.btnAlarm.Click += new System.EventHandler(this.btnAlarm_Click);
      // 
      // _TimerTitle
      // 
      this._TimerTitle.Interval = 200;
      this._TimerTitle.Tick += new System.EventHandler(this._TimerTitle_Tick);
      // 
      // PrintDocument
      // 
      this.PrintDocument.BeginPrint += new System.Drawing.Printing.PrintEventHandler(this.PrintDocument_BeginPrint);
      this.PrintDocument.EndPrint += new System.Drawing.Printing.PrintEventHandler(this.PrintDocument_EndPrint);
      this.PrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument_PrintPage);
      // 
      // PrintPreview
      // 
      this.PrintPreview.AutoZoom = false;
      this.PrintPreview.Dock = System.Windows.Forms.DockStyle.Fill;
      this.PrintPreview.Location = new System.Drawing.Point(0, 0);
      this.PrintPreview.Name = "PrintPreview";
      this.PrintPreview.Size = new System.Drawing.Size(630, 539);
      this.PrintPreview.TabIndex = 15;
      this.PrintPreview.Visible = false;
      this.PrintPreview.Zoom = 0.41402908468776733D;
      // 
      // btnDisplayedGrafic
      // 
      this.btnDisplayedGrafic.Image = ((System.Drawing.Image)(resources.GetObject("btnDisplayedGrafic.Image")));
      this.btnDisplayedGrafic.Location = new System.Drawing.Point(128, 22);
      this.btnDisplayedGrafic.Name = "btnDisplayedGrafic";
      this.btnDisplayedGrafic.Size = new System.Drawing.Size(16, 16);
      this.btnDisplayedGrafic.TabIndex = 28;
      this._ToolTip.SetToolTip(this.btnDisplayedGrafic, "Store the currently displayed chromatogram");
      this.btnDisplayedGrafic.Click += new System.EventHandler(this.btnDisplayedGrafic_Click);
      this.btnDisplayedGrafic.MouseHover += new System.EventHandler(this.btnDisplayedGrafic_MouseHover);
      // 
      // RecordPanel
      // 
      this.RecordPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.RecordPanel.Controls.Add(this.gbSpecRec);
      this.RecordPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.RecordPanel.Location = new System.Drawing.Point(0, 0);
      this.RecordPanel.Name = "RecordPanel";
      this.RecordPanel.Size = new System.Drawing.Size(544, 58);
      this.RecordPanel.TabIndex = 16;
      // 
      // gbSpecRec
      // 
      this.gbSpecRec.Controls.Add(this.btnDisplayedGrafic);
      this.gbSpecRec.Controls.Add(this._chkResultsCont);
      this.gbSpecRec.Controls.Add(this._chkCurrScan);
      this.gbSpecRec.Controls.Add(this._chkScansCont);
      this.gbSpecRec.Location = new System.Drawing.Point(8, 4);
      this.gbSpecRec.Name = "gbSpecRec";
      this.gbSpecRec.Size = new System.Drawing.Size(528, 48);
      this.gbSpecRec.TabIndex = 0;
      this.gbSpecRec.TabStop = false;
      this.gbSpecRec.Text = "Chromatogram recording";
      // 
      // _chkResultsCont
      // 
      this._chkResultsCont.Location = new System.Drawing.Point(332, 20);
      this._chkResultsCont.Name = "_chkResultsCont";
      this._chkResultsCont.Size = new System.Drawing.Size(186, 20);
      this._chkResultsCont.TabIndex = 27;
      this._chkResultsCont.Text = "Messergebnisse - kontinuierlich";
      this._chkResultsCont.Click += new System.EventHandler(this._chkRecording_Click);
      // 
      // _chkCurrScan
      // 
      this._chkCurrScan.Location = new System.Drawing.Point(4, 18);
      this._chkCurrScan.Name = "_chkCurrScan";
      this._chkCurrScan.Size = new System.Drawing.Size(118, 26);
      this._chkCurrScan.TabIndex = 25;
      this._chkCurrScan.Text = "Aktuelles Chromatogramm";
      this._chkCurrScan.Click += new System.EventHandler(this._chkRecording_Click);
      // 
      // _chkScansCont
      // 
      this._chkScansCont.Location = new System.Drawing.Point(164, 18);
      this._chkScansCont.Name = "_chkScansCont";
      this._chkScansCont.Size = new System.Drawing.Size(160, 26);
      this._chkScansCont.TabIndex = 26;
      this._chkScansCont.Text = "Chromatogramme - kontinuierlich";
      this._chkScansCont.Click += new System.EventHandler(this._chkRecording_Click);
      // 
      // pnlLayer0
      // 
      this.pnlLayer0.Controls.Add(this.pnlLayer1b);
      this.pnlLayer0.Controls.Add(this.pnlLayer1a);
      this.pnlLayer0.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlLayer0.Location = new System.Drawing.Point(266, 28);
      this.pnlLayer0.Name = "pnlLayer0";
      this.pnlLayer0.Size = new System.Drawing.Size(630, 597);
      this.pnlLayer0.TabIndex = 17;
      // 
      // pnlLayer1b
      // 
      this.pnlLayer1b.Controls.Add(this.GraphPanel);
      this.pnlLayer1b.Controls.Add(this.PrintPreview);
      this.pnlLayer1b.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlLayer1b.Location = new System.Drawing.Point(0, 58);
      this.pnlLayer1b.Name = "pnlLayer1b";
      this.pnlLayer1b.Size = new System.Drawing.Size(630, 539);
      this.pnlLayer1b.TabIndex = 17;
      // 
      // pnlLayer1a
      // 
      this.pnlLayer1a.Controls.Add(this.RecordPanel);
      this.pnlLayer1a.Controls.Add(this.btnAlarm);
      this.pnlLayer1a.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlLayer1a.Location = new System.Drawing.Point(0, 0);
      this.pnlLayer1a.Name = "pnlLayer1a";
      this.pnlLayer1a.Size = new System.Drawing.Size(630, 58);
      this.pnlLayer1a.TabIndex = 16;
      // 
      // _TimerTest
      // 
      this._TimerTest.Tick += new System.EventHandler(this._TimerTest_Tick);
      // 
      // _tbbOpenServiceDataFile
      // 
      this._tbbOpenServiceDataFile.ImageIndex = 11;
      this._tbbOpenServiceDataFile.Name = "_tbbOpenServiceDataFile";
      this._tbbOpenServiceDataFile.ToolTipText = "Open service data";
      // 
      // _tbbTransmitServiceData
      // 
      this._tbbTransmitServiceData.ImageIndex = 12;
      this._tbbTransmitServiceData.Name = "_tbbTransmitServiceData";
      this._tbbTransmitServiceData.ToolTipText = "Transmit service data";
      // 
      // MainForm
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.ClientSize = new System.Drawing.Size(896, 647);
      this.Controls.Add(this.pnlLayer0);
      this.Controls.Add(this.ResultPanel);
      this.Controls.Add(this.ToolBar);
      this.Controls.Add(this.StatusBar);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Menu = this._MainMenu;
      this.Name = "MainForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "PID Control";
      this.Closed += new System.EventHandler(this.MainForm_Closed);
      this.Load += new System.EventHandler(this.MainForm_Load);
      ((System.ComponentModel.ISupportInitialize)(this._StatusBarPanel1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._StatusBarPanel2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._StatusBarPanel3)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._StatusBarPanel4)).EndInit();
      this.ResultPanel.ResumeLayout(false);
      this.gbPeakInfo.ResumeLayout(false);
      this.gbResults.ResumeLayout(false);
      this.gbResults.PerformLayout();
      this.gbDeviceInfo.ResumeLayout(false);
      this.GraphPanel.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pbDraw)).EndInit();
      this.RecordPanel.ResumeLayout(false);
      this.gbSpecRec.ResumeLayout(false);
      this.pnlLayer0.ResumeLayout(false);
      this.pnlLayer1b.ResumeLayout(false);
      this.pnlLayer1a.ResumeLayout(false);
      this.ResumeLayout(false);

    }

		#endregion

    #region event handling form

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void MainForm_Load(object sender, System.EventArgs e)
    {
      // The one and only App instance
      App app = App.Instance;

      // MainForm as App member
      app.MainForm = this;
      
      // Init's the hashtable, which containes the required Enabled states of all menuitems
      // when the print preview is active (shown) 
      foreach ( MenuItem mi in this._MainMenu.MenuItems ) 
      {
        foreach ( MenuItem miSub in mi.MenuItems ) 
        {
          bool bEnableMask = ( 
            miSub == _miFile_Print ||             // The menuItems listed here are enabled 
            miSub == _miFile_PrintPreview ||      // when the print preview is active
            miSub == _miHelp_About ||
            miSub == _miView_StaticalPrintPreview
            );
          _htMenuItemsEnabledStateOnPrintPreview.Add ( miSub, bEnableMask );
        }
      }
      
      // Init. the language of the MainForm
      UpdateCulture ();
      
      // CommRS232 event handling: Chain event handler
      app.Comm.CommRs232Response += _crh;
      
      // Init. the program functionality: Adjustment according to the initial usertype settings 
      // in the application data object
      UpdateAccordingUserMode ();

      // Init. App data      
      this.ToolBar.Visible = true;                    // F.a.F. ...
      this.StatusBar.Visible = true;                  // F.a.F. ...
      app.Data.MainForm.rDefaultBounds = this.DesktopBounds;
      app.Data.MainForm.bMaximizeBoxPresent = this.MaximizeBox;
      app.Data.MainForm.bStaticalPrintPreview = true; // Printpreview: Statically

      // Init. document
      Doc doc = app.Doc;
      doc.sScriptFilename = app.Data.Common.sScriptFileName;  // script configuration file
      doc.sDataFolder = app.Data.Common.sDataFolder;          // Folder where data storage takes place
      doc.sScanFilePath = app.Data.Common.sDisplayScanFile;   // lastly loaded & displayed scan file

      // Init. Alarm button visibility
      this.btnAlarm.Visible = false;

      // Init. view
      Vw vw = app.View;
      vw.OnInitialUpdate ();

      // Init. Statusbar - Panel 0 (Messages)
      this.StatusBar.Panels[0].Text = string.Empty;   
 
      // Enable timer
      _Timer.Enabled = true;            // UI update
      _TimerTitle.Enabled = true;       // Intro form displaying
    }
    
    /// <summary>
    /// 'Closed' event of the form
    /// </summary>
    private void MainForm_Closed(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;

      // Disable timer
      _Timer.Enabled = false;           // UI update
      _TimerTitle.Enabled = false;      // Intro form displaying
      _TimerTest.Enabled = false;       // Testing stuff

      // Write app data:  Last access to the application, script configuration file, 
      //                  Folder where data storage takes place, lastly loaded & displayed scan file
      app.Data.Common.dtlastUsage = DateTime.Now;
      app.Data.Common.sScriptFileName = doc.sScriptFilename;
      app.Data.Common.sDataFolder = doc.sDataFolder;
      app.Data.Common.sDisplayScanFile = doc.sScanFilePath;
      app.Data.Write ();

      // CommRS232 event handling: Unchain event handler
      comm.CommRs232Response -= _crh;
      // Close the communication
      comm.Close ();
    }

    #endregion // event handling form
    
    #region event handling timer

    /// <summary>
    /// 'Tick' event of the _TimerTitle timer 
    /// (Intro form displaying, each 200 msec)
    /// </summary>
    private void _TimerTitle_Tick(object sender, System.EventArgs e)
    {
      // Disable it
      this._TimerTitle.Enabled = false;

      // Show the Title form 
      TitleForm tf = new TitleForm ( this );
      tf.ShowDialog ();
      tf.Dispose ();
    }

    /// <summary>
    /// 'Tick' event of the _Timer timer 
    /// (UI update, each 100 msec)
    /// </summary>
    private void _Timer_Tick(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Measure meas = app.Measurement;
      Ressources r = app.Ressources;
  
      // Incr. counter
      _nTimerCounter++;

      // -------------------------------------
      // each 100 ms
      if ((_nTimerCounter % 1) == 0)
      {
        // Where necessary: Push the commands, that should be permanenty executed (i.e. the 'OnCommEmpty()' routine)
        meas.PushOnCommEmpty ();
        // Where necessary: Reconnect the device
        app.CmdUI.PushStartReConnect ();
      }

      // -------------------------------------
      // each 0,5 sec
      if ((_nTimerCounter % 5) == 0)
      {
        // Statusbar: 
        // Define panel texts
        //    Panel 0 (Messages)
        ;
        //    Panel 1: Connection & meas. state
        //    NOTE: 
        //      1.
        //      If the 'Style' property of Panel1 was set to 'Text', so all drawing of this panel is done here
        //      in this routine. The 'StatusBar_DrawItem()' event handling is not needed in this case.
        //      If the 'Style' property of Panel1 was set to 'OwnerDraw', so all drawing of this panel
        //      takes place via the 'StatusBar_DrawItem()' event handling. The related stuff of this routine
        //      is not needed in this case, but not harmful. 
        //      2.
        //      For explanation of the if-else structure see 'StatusBar_DrawItem()'.
        string sPanel1 = "";
        //    Check: Comm. errors present?
        int errCount = comm.ErrorCount;
#if !DEBUG
        // Release: Do NOT visualize comm. errors
        errCount = 0;
#endif
        if (errCount > 0)
        {
          // Yes:
          string sFmt = r.GetString ( "Main_Status_P1_CommError" );     //"Comm. errors: {0}"
          sPanel1 = string.Format (sFmt, errCount);
        }
        else
        {
          // No:
          // Check: Comm. open?
          if ( !comm.IsOpen () ) 
          {
            // No:
            sPanel1 = r.GetString ( "Main_Status_P1_NotConnected" );    //"Not connected"
          }
          else
          {
            // Yes:
            // Check: WarmUp running?
            if (doc.bWarmUpRunning)
            {
              // Yes:
              sPanel1 = r.GetString ( "Main_Status_P1_WarmUp" );        //"WarmUp running"
            }
            else
            {
              // No:
              // Check: Measurement running?
              if ( doc.bRunning )
                // Yes:
                sPanel1 = r.GetString ( "Main_Status_P1_Running" );     //"Measuring in execution"
              else
                // No:
                sPanel1 = r.GetString ( "Main_Status_P1_Stopped" );     //"Flush mode"
            }
          }
        }
        //    Panel 2 (Measurement info's)
        UInt32 scans  = doc.dwScanCount;
        UInt32 noData = doc.dwNoDataCount;
        UInt32 timeUser = doc.ScanInfo.dwUserTime / 10; // Usertime in sec
        UInt32 timePlay = doc.ScanInfo.dwPlayTime / 10; // Playtime in sec ( only informational )
        string fmt = r.GetString ( "Main_Status_P2" );  // "Transfers (Gesamt/Scans): {0}/{1}   Zeit: {2}"
        string sPanel2 = string.Format ( fmt, noData+scans, scans, Common.UInt32ToTimeString ( timeUser ) );
        //    Panel 3: Meas. channel
        fmt =  r.GetString ( "Main_Status_P3" );        //"Messkanal: {0}"
        string sPanel3 = string.Format ( fmt, doc.ScanInfo.byChan); 
        // Set panel texts
        Graphics g = StatusBar.CreateGraphics ();
        //    Panel 1
        Size si = g.MeasureString ( sPanel1, StatusBar.Font ).ToSize (); 
        StatusBar.Panels[1].Width = si.Width + 10;
        StatusBar.Panels[1].Text = sPanel1;    
        //    Panel 2
        si = g.MeasureString ( sPanel2, StatusBar.Font ).ToSize (); 
        StatusBar.Panels[2].Width = si.Width + 10;
        StatusBar.Panels[2].Text = sPanel2;    
        //    Panel 3
        si = g.MeasureString ( sPanel3, StatusBar.Font ).ToSize (); 
        StatusBar.Panels[3].Width = si.Width + 10;
        StatusBar.Panels[3].Text = sPanel3;    
        //    Done
        g.Dispose ();
      }

      // -------------------------------------
      // each 1 sec
      if ((_nTimerCounter % 10) == 0)
      {
        _nTimerCounter = 0;

        // ---------------------------------------------------
        // ==> B: SCANALLDATA cmd receive handling

  #if DEBUG
        // Debug window output (every 30 sec)
        nDbgWnd++;
        if (nDbgWnd == 30) { bDbgWnd = true; nDbgWnd=0; }
        // Optical announcement 
        // (Change the text of GB 'Results' in order to announce optically, 
        //  that no further SCANALLDATA cmds have been received)
        string sResults = r.GetString ( "Main_gbResults" );
        if (this.gbResults.Text != sResults)
          this.gbResults.Text = sResults;
  #endif 

        // ==> E: SCANALLDATA cmd receive handling
        // ---------------------------------------------------


        // Check, whether a measurement should be stopped passively
        if (meas.StopPassively)
        {
          // Yes: 
          // Reset the indicator
          meas.StopPassively = false;
          // Stop the measurement pass'ly
          meas.StopMeasure ( Measure.StopMode.Passive );
        }
        
        // Check, whether a script change has taken place on device 
        if (meas.MeasureChanged)
        {
          // Yes: 
          // Reset the indicator
          meas.MeasureChanged = false;
          // "Start" measurement ( in reaction of a script change on device )
          meas.StartMeasure ( Measure.StartMode.AfterScriptChangeOnDevice );
        }
        
        // Check communication state: Set the documents 'bRunning' member corr'ly
        // Notes:
        //  This is required only in normal mode, not in testing mode.
        if (this._TimerTest.Enabled == false)
        {
          if ( !comm.IsOpen() )
            doc.bRunning = false;
        }

        // Check alarm state: Show/hide the Alarm button corr'ly
        bool bVis = doc.bResultAlarm;
        if (!btnAlarm.Visible)
          bVis &= !doc.ScanInfo.GetReadyForMeas ();
        btnAlarm.Visible = bVis;
        // ToolTip text for the 'Trigger Key cmd' TBB, depending on alarm state
        string sTriggerKeyCmd = "";
        if (btnAlarm.Visible)                                                       // an alarm is present:
          sTriggerKeyCmd = r.GetString ( "Main_tbbTriggerKeyCmd_AlarmPresent" );    //    "Trigger Key command (confirm alarm first)"
        else                                                                        // an alarm is NOT present:
          sTriggerKeyCmd = r.GetString ( "Main_tbbTriggerKeyCmd" );                 //    "Trigger Key command"
        this.StatusMessages.SetStatusMessage(this._tbbTriggerKeyCmd, sTriggerKeyCmd);                         
        this._tbbTriggerKeyCmd.ToolTipText = sTriggerKeyCmd;

        // Check error state: Show/hide the Error acknowledge ('ACK') button corr'ly
        if (doc.bRunning)
        {
          // Script is running: 
          // Show/hide ACK button corr'ing to the error Dword
          if (doc.ScanInfo.dwError > 0)
          {
            if (!this._btnErrAck.Visible)
              this._btnErrAck.Visible = true;
          }
          else
          {
            if (this._btnErrAck.Visible)
              this._btnErrAck.Visible = false;
          }
        }
        else
        {
          // No script is running:
          // Hide ACK button
          if (this._btnErrAck.Visible)
            this._btnErrAck.Visible = false;
        }
        
        // Record panel:
        // Checked state of the recording CheckBoxes
        this._chkCurrScan.Checked     = doc.bRecordSingleScan;
        this._chkScansCont.Checked    = doc.bRecordScans;
        this._chkResultsCont.Checked  = doc.bRecordResults;
        // Enabled state of the recording CheckBoxes
        this._chkCurrScan.Enabled     = doc.bRunning;
        this._chkScansCont.Enabled    = doc.bRunning;
        this._chkResultsCont.Enabled  = doc.bRunning;
        // 'Displayed grafic' Button
        this.btnDisplayedGrafic.Enabled = true;

      
        // Update Toolbar buttons
        _UpdateToolbarButtons ();

        // EnkyLC: Check for presence and update corr'ly
        if (EnkyLC.Check(this._Timer.Interval * 10))
        {
          app.Data.MainForm.User = EnkyLC.UserType;
          UpdateAccordingUserMode();
        }

        // Flow display
        app.Data.Program.bDisplayFlow = (app.Data.MainForm.User == UserType.Expert);
        this.lblFlow.Visible = app.Data.Program.bDisplayFlow;
        this.lblFlow_Val.Visible = app.Data.Program.bDisplayFlow;
      } // E: each 1 sec

    }

    /// <summary>
    /// 'Tick' event of the _TimerTest timer 
    /// (The '_TimerTest' timer is enabled only in Debug mode via the 'Test' menuitem, 
    ///  when the 'UI-/PrintPreview test via RS232 emulation' is performed)
    /// </summary>
    private void _TimerTest_Tick(object sender, System.EventArgs e)
    {
      // UI-/PrintPreview test via RS232 emulation: 
      if (Test.Enabled)
        Test.Execute ();    // 'Test' stuff
      if (Test1.Enabled)
        Test1.Execute ();   // 'Test1' stuff
    }

    #endregion // event handling timer

    #region event handling menu & toolbar

    /// <summary>
    /// 'Click' event of a MenuItem
    /// </summary>
    private void _mi_Click(object sender, System.EventArgs e)
		{
      App app = App.Instance;
      CmdUI cmdUI = app.CmdUI;
      
      ///////////////////////////////////////////////////////////////
      // File menu

      // Script editor ...
      if (sender == this._miFile_ScriptEditor )                       cmdUI.OnScriptEditor();
      // Open configuration file (plain text) ...
      else if (sender == this._miFile_OpenConfigFile_Txt )            cmdUI.OnOpenConfigFile_Txt();
      // Open configuration file ...
      else if (sender == this._miFile_OpenConfigFile)                 cmdUI.OnOpenConfigFile();
      // Service data editor ...
      else if (sender == this._miFile_ServiceDataEditor)              cmdUI.OnServiceDataEditor();
      // Open service data file ...
      else if (sender == this._miFile_OpenServiceDataFile)            cmdUI.OnOpenServiceDataFile();
      // Print ...
      else if (sender == this._miFile_Print )                         cmdUI.OnPrint();
      // Print preview
      else if (sender == this._miFile_PrintPreview )                  cmdUI.OnPrintPreview();
      // Printer adjustment ...
      else if (sender == this._miFile_PrinterAdjustment )             cmdUI.OnPrinterAdjustment();
      // Properties ...
      else if (sender == this._miFile_Properties )                    cmdUI.OnProperties();
      // Exit
      else if (sender == this._miFile_Exit )                          cmdUI.OnExit();
      
        ///////////////////////////////////////////////////////////////
        // View menu

      // Toolbar
      else if (sender == this._miView_Toolbar )                       cmdUI.OnViewToolbar();
      // Statusbar
      else if (sender == this._miView_Statusbar )                     cmdUI.OnViewStatusbar();
      // Default
      else if (sender == this._miView_Default )                       cmdUI.OnViewDefault();
      // Statical print preview
      else if (sender == this._miView_StaticalPrintPreview )          cmdUI.OnViewStaticalPrintPreview();
      
        ///////////////////////////////////////////////////////////////
        // Control menu

      // Connect device
      else if (sender == this._miControl_ConnectDevice )              cmdUI.OnConnectDevice();
      // Transmit configuration data
      else if (sender == this._miControl_TransmitConfigData )         cmdUI.OnTransmitConfigData();
      // Transmit service data
      else if (sender == this._miControl_TransmitServiceData)         cmdUI.OnTransmitServiceData();
      // Reread script names
      else if (sender == this._miControl_RereadScriptNames )          cmdUI.OnRereadScriptNames();
      // Select script ...
      else if (sender == this._miControl_SelectScript )               cmdUI.OnSelectScript();
      // Execute script
      else if (sender == this._miControl_ExecuteScript )              cmdUI.OnExecuteScript();
      // Acknowledge alarm
      else if (sender == this._miControl_AcknowledgeAlarm )           cmdUI.OnAlarmAcknowledge();
      // Trigger KEY cmd
      else if (sender == this._miControl_TriggerKeyCmd )              cmdUI.OnTriggerKeyCmd();
      
        ///////////////////////////////////////////////////////////////
        // Data recording menu

      // Current scan
      else if (sender == this._miDataRecording_CurrentScan )          cmdUI.OnRecCurrentScan();
      // Scans cont.
      else if (sender == this._miDataRecording_ScansContinuously )    cmdUI.OnRecScansContinuously();
      // Results cont.
      else if (sender == this._miDataRecording_ResultsContinuously )  cmdUI.OnRecResultsContinuously();
      // Grafic
      else if (sender == this._miDataRecording_Grafic )               cmdUI.OnRecGrafic();
      
        ///////////////////////////////////////////////////////////////
        // Tools menu

      // Read stored Eeprom data ...
      else if (sender == this._miTools_EEStore )                      cmdUI.OnReadEepromStore();
        // Read stored SDcard data ...
      else if (sender == this._miTools_SDStore )                      cmdUI.OnReadSDcardStore();
        // Transfer service data ...
      else if (sender == this._miTools_Service )                      cmdUI.OnTransferService();
        // Transfer MP data ...
      else if (sender == this._miTools_MP )                           cmdUI.OnTransferMP();
        // Transfer clock-timed script data ...
      else if (sender == this._miTools_CTS )                          cmdUI.OnTransferCTS();
        // Diagnosis
      else if (sender == this._miTools_Diag )                         cmdUI.OnDiag();
        // PC communication listener
      else if (sender == this._miTools_PCcommListener )               cmdUI.OnPCcommListener();
        // Load scan from HD
      else if (sender == this._miTools_LoadScanFromHD )               cmdUI.OnLoadScanFromHD ();

        ///////////////////////////////////////////////////////////////
        // ? menu

      // About ...
      else if (sender == this._miHelp_About )                         cmdUI.OnAbout();
      // Test
      else if (sender == this._miHelp_Test )                          cmdUI.OnTest();
    }

    /// <summary>
    /// 'Popup' event of a MenuItem
    /// </summary>
    private void _mi_Popup(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      bool bPrintPreview = app.Data.MainForm.bPrintPreview; // True, if we are in the PrintPreview view
      bool bEnable, bCheck;

      ///////////////////////////////////////////////////////////////
      // File menu
      
      if      (sender == this._miFile) 
      {

        // Script editor ...
        bEnable = !comm.IsOpen();
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miFile_ScriptEditor];  
        _miFile_ScriptEditor.Enabled = bEnable;

        // Open configuration file (plain text) ...
        bEnable = !comm.IsOpen();
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miFile_OpenConfigFile_Txt];  
        _miFile_OpenConfigFile_Txt.Enabled = bEnable;

        // Open configuration file ...
        bEnable = !comm.IsOpen();
        if (bPrintPreview) bEnable &= (bool)_htMenuItemsEnabledStateOnPrintPreview[_miFile_OpenConfigFile];
        _miFile_OpenConfigFile.Enabled = bEnable;

        // Service data editor ...
        bEnable = !comm.IsOpen();
        if (bPrintPreview) bEnable &= (bool)_htMenuItemsEnabledStateOnPrintPreview[_miFile_ServiceDataEditor];
        _miFile_ServiceDataEditor.Enabled = bEnable;

        // Open service data file ...
        bEnable = !comm.IsOpen();
        if (bPrintPreview) bEnable &= (bool)_htMenuItemsEnabledStateOnPrintPreview[_miFile_OpenServiceDataFile];
        _miFile_OpenServiceDataFile.Enabled = bEnable;
        
        // Print ...
        bEnable = System.Drawing.Printing.PrinterSettings.InstalledPrinters.Count > 0;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miFile_Print];  
        this._miFile_Print.Enabled = bEnable;
        
        // Print preview 
        bEnable = System.Drawing.Printing.PrinterSettings.InstalledPrinters.Count > 0;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miFile_PrintPreview];  
        _miFile_PrintPreview.Enabled = bEnable;
        bCheck = app.Data.MainForm.bPrintPreview;
        _miFile_PrintPreview.Checked = bCheck;
        
        // Printer adjustment ...
        bEnable = System.Drawing.Printing.PrinterSettings.InstalledPrinters.Count > 0;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miFile_PrinterAdjustment];  
        _miFile_PrinterAdjustment.Enabled = bEnable;
        
        // Properties ...
        bEnable = true;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miFile_Properties];  
        _miFile_Properties.Enabled = bEnable;
        
        // Exit
        bEnable = true;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miFile_Exit];  
        _miFile_Exit.Enabled = bEnable;
      }
      
        ///////////////////////////////////////////////////////////////
        // View menu
      
      else if (sender == this._miView) 
      {
        // Toolbar
        bCheck = this.ToolBar.Visible;
        _miView_Toolbar.Checked = bCheck;   
        bEnable = true;  
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miView_Toolbar];  
        _miView_Toolbar.Enabled = bEnable;   
        
        // Statusbar
        bCheck = this.StatusBar.Visible;
        _miView_Statusbar.Checked = bCheck;   
        bEnable = true;  
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miView_Statusbar];  
        _miView_Statusbar.Enabled = bEnable;   
        
        // Default
        bEnable = true;  
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miView_Default];  
        _miView_Default.Enabled = bEnable;   
        
        // Statical print preview
        bEnable = true;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miView_StaticalPrintPreview];  
        _miView_StaticalPrintPreview.Enabled = bEnable;
        bCheck = app.Data.MainForm.bStaticalPrintPreview;
        _miView_StaticalPrintPreview.Checked = bCheck;
      }
      
        ///////////////////////////////////////////////////////////////
        // Control menu
      
      else if (sender == this._miControl) 
      {
        // Connect device
        bCheck = comm.IsOpen(); 
        _miControl_ConnectDevice.Checked = bCheck;
        bEnable = !doc.bRunning; 
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miControl_ConnectDevice];  
        _miControl_ConnectDevice.Enabled = bEnable;
        
        // Transmit configuration data
        bEnable = comm.IsOpen() &&  !doc.bRunning; 
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miControl_TransmitConfigData];  
        _miControl_TransmitConfigData.Enabled = bEnable;

        // Transmit service data
        bEnable = comm.IsOpen() && !doc.bRunning;
        if (bPrintPreview) bEnable &= (bool)_htMenuItemsEnabledStateOnPrintPreview[_miControl_TransmitServiceData];
        _miControl_TransmitServiceData.Enabled = bEnable;
        
        // Reread script names
        bEnable = comm.IsOpen() &&  !doc.bRunning; 
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miControl_RereadScriptNames];  
        _miControl_RereadScriptNames.Enabled = bEnable;
        
        // Select script ...
        bEnable = comm.IsOpen() &&  !doc.bRunning; 
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miControl_SelectScript];  
        _miControl_SelectScript.Enabled = bEnable;
        
        // Execute script
        bEnable = comm.IsOpen(); 
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miControl_ExecuteScript];  
        _miControl_ExecuteScript.Enabled = bEnable;
        bCheck = doc.bRunning; 
        _miControl_ExecuteScript.Checked = bCheck;
        
        // Acknowledge alarm
        bEnable = btnAlarm.Visible;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miControl_AcknowledgeAlarm];  
        _miControl_AcknowledgeAlarm.Enabled = bEnable;   
      
        // Trigger Key Cmd
        bEnable = comm.IsOpen() &&  doc.bRunning; // Enabled, if  1) comm. is established & a script runs AND
        bEnable &= !btnAlarm.Visible;             //              2) an alarm is NOT present AND
        bool bDevReadyForMeas = doc.ScanInfo.GetReadyForMeas();   
        bEnable &= bDevReadyForMeas;              //              3) the device is ready for measurement
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miControl_TriggerKeyCmd];  
        _miControl_TriggerKeyCmd.Enabled = bEnable;
      }
      
        ///////////////////////////////////////////////////////////////
        // Data recording menu
      
      else if (sender == this._miDataRecording) 
      {
        // Current scan
        bEnable = doc.bRunning;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miDataRecording_CurrentScan];  
        bCheck  = doc.bRecordSingleScan;
        _miDataRecording_CurrentScan.Enabled = bEnable;
        _miDataRecording_CurrentScan.Checked = bCheck;
        
        // Scans cont.
        bEnable = doc.bRunning;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miDataRecording_ScansContinuously];  
        bCheck  = doc.bRecordScans;
        _miDataRecording_ScansContinuously.Enabled = bEnable;
        _miDataRecording_ScansContinuously.Checked = bCheck;
        
        // Results cont.
        bEnable = doc.bRunning;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miDataRecording_ResultsContinuously];  
        bCheck  = doc.bRecordResults;
        _miDataRecording_ResultsContinuously.Enabled = bEnable;
        _miDataRecording_ResultsContinuously.Checked = bCheck;

        // Displayed grafic
        bEnable = true;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miDataRecording_Grafic];  
        _miDataRecording_Grafic.Enabled = bEnable;
      }
      
        ///////////////////////////////////////////////////////////////
        // Tools menu
      
      else if (sender == this._miTools) 
      {
        // Read stored Eeprom data ...
        bEnable = comm.IsOpen() && !doc.bRunning;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miTools_EEStore];  
        _miTools_EEStore.Enabled = bEnable;

        // Read stored SDcard data ...
        bEnable = comm.IsOpen() && !doc.bRunning;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miTools_SDStore];  
        _miTools_SDStore.Enabled = bEnable;

        // Transfer service data ...
        bEnable = comm.IsOpen() && !doc.bRunning;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miTools_Service];  
        _miTools_Service.Enabled = bEnable;
 
        // Transfer MP data ...
        bEnable = comm.IsOpen() && !doc.bRunning;
        bEnable &= (doc.nMPChan > 0);             // Enabled only, if MP connected
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miTools_MP];  
        _miTools_MP.Enabled = bEnable;
       
        // Transfer clock-timed script data ...
        bEnable = comm.IsOpen() && !doc.bRunning;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miTools_CTS];  
        _miTools_CTS.Enabled = bEnable;

        // Diagnosis
        bEnable = comm.IsOpen() && doc.bRunning;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miTools_Diag];  
        bCheck = doc.Diag.Enabled;
        _miTools_Diag.Enabled = bEnable;
        _miTools_Diag.Checked = bCheck;
        
        // PC communication listener
        bEnable = true;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miTools_PCcommListener];  
        bCheck = comm.ExcMsgFile.Enabled;
        _miTools_PCcommListener.Enabled = bEnable;
        _miTools_PCcommListener.Checked = bCheck;
        
        // Load scan from HD
        bEnable = !doc.bRunning;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miTools_LoadScanFromHD];  
        _miTools_LoadScanFromHD.Enabled = bEnable;
      }
        
        ///////////////////////////////////////////////////////////////
        // ? menu
      
      else if (sender == this._miHelp) 
      {
        // About ...
        bEnable = true;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miHelp_About];  
        _miHelp_About.Enabled = bEnable;  
        
        // Test
#if DEBUG
        bEnable = true;
        if ( bPrintPreview ) bEnable &= ( bool ) _htMenuItemsEnabledStateOnPrintPreview[_miHelp_Test];  
        _miHelp_Test.Enabled = bEnable;  
#else
        _miHelp_Test.Visible = false;  
#endif // DEBUG
      }
      
    }
    
    #endregion // event handling menu & toolbar

    #region event handling communication

    /// <summary>
    /// 'Comm. channel - Receive process completed' event of the communication object 
    /// </summary>
    internal void OnRXCompleted(object sender, CommRs232ResponseEventArgs e)
    {
      // Handle the RX'd comm. message
      HandleRX (e.Message);
    }

    /// <summary>
    /// Delegate for the 'HandleRX' method invocation
    /// </summary>
    delegate void HandleRXDelegate (CommMessage msgRX);
    /// <summary>
    /// Handles the RX'd comm. message.
    /// </summary>
    /// <param name="msgRX">The RX'd comm. message</param>
    void HandleRX (CommMessage msgRX)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;

      try
      {
        // Exception message file: Append helper stuff
        comm.ExcMsgFile.Append (msgRX, AppendSpec.OnHandleRX_Entry);
        
        // Check: Valid RX'd comm. message?
        if (null == msgRX)
          return;   // No
  
        // Invocation required?
        if (this.InvokeRequired)
        {
          // Yes: Invoke synchronously
          this.Invoke (new HandleRXDelegate (HandleRX), new object[] {msgRX});
          return;
        }

        // Exception message file: Append helper stuff
        comm.ExcMsgFile.Append (msgRX, AppendSpec.OnHandleRX_AfterInv);
        
        // Get the owner form
        Form f = (Form) Control.FromHandle (msgRX.WindowInfo.Owner);
        // Error (Timeout / String length error)?
        if ((msgRX.ResponseID == AppComm.CMID_TIMEOUT) || (msgRX.ResponseID == AppComm.CMID_STRLENERR))
        {
          // Yes: 
          // Error info  
          // Notes:
          //  The following #tor works fine, even if the 'f' parameter was null 
          //  (though this should not happen).
          CommErrorForm dlg = new CommErrorForm (f, msgRX);
          dlg.ShowDialog ();
          dlg.Dispose ();

          // Update documents 'bRunning' member: No script is running
          doc.bRunning = false;
          // Close communication channel
          comm.Close ();
 
          // Exception message file: Append helper stuff
          comm.ExcMsgFile.Append (msgRX, AppendSpec.OnHandleRX_CommClose);
        }
      
        // Check: WarmUp running?
        string sPar = Encoding.ASCII.GetString (msgRX.ParameterResponse);
        if (sPar == CommMessage.SZWARMUP)
        {
          // Yes:
          doc.bWarmUpRunning = true;
 
          // Exception message file: Append helper stuff
          comm.ExcMsgFile.Append (msgRX, AppendSpec.OnHandleRX_WarmUp);
        }
        else
        {
          // No:
          doc.bWarmUpRunning = false;

          // Exception message file: Append helper stuff
          comm.ExcMsgFile.Append (msgRX, AppendSpec.OnHandleRX_NotWarmUp);
          
          // Dispatch the message acc'ing to the owner form
          if      (f is MainForm)           ((MainForm)f).AfterRXCompleted (msgRX);
          else if (f is ConnectForm)        ((ConnectForm)f).AfterRXCompleted (msgRX);
          else if (f is ScriptTransferForm) ((ScriptTransferForm)f).AfterRXCompleted (msgRX);
          else if (f is CTSForm)            ((CTSForm)f).AfterRXCompleted (msgRX);
          else if (f is GSMReaderForm)      ((GSMReaderForm)f).AfterRXCompleted (msgRX);
          else if (f is SDcardReaderForm)   ((SDcardReaderForm)f).AfterRXCompleted (msgRX);
          else if (f is SDcardReaderNewForm)((SDcardReaderNewForm)f).AfterRXCompleted (msgRX);
          else if (f is ServiceForm)        ((ServiceForm)f).AfterRXCompleted (msgRX);
          else if (f is ServieNewForm)      ((ServieNewForm)f).AfterRXCompleted (msgRX);
          else if (f is ServiceDataTransferForm) ((ServiceDataTransferForm)f).AfterRXCompleted (msgRX);
          else if (f is ServiceMPForm)      ((ServiceMPForm)f).AfterRXCompleted(msgRX);
          else if (f is ServiceExtForm)     ((ServiceExtForm)f).AfterRXCompleted (msgRX);
        }
      }
      catch (System.Exception exc)
      {
        string sMsg = string.Format ("HandleRX() failed: {0}", exc.Message);
        Debug.WriteLine (sMsg);
      }
    }


    /// <summary>
    /// Performs actions in reaction of the receipt of a comm. message  
    /// </summary>
    /// <param name="msgRX">The comm. message</param>
    void AfterRXCompleted(CommMessage msgRX)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      Measure meas = app.Measurement;
      Ressources r = app.Ressources;
      AppComm comm = app.Comm;

      // CD: Message response ID
      if (msgRX.ResponseID == AppComm.CMID_EMPTY)
      {
        // The message list is empty: 
        // Push the commands, that should be permanenty executed
        if (doc.bRunning)
        {
          //meas.OnCommEmpty ();
          meas.bOnCommEmpty = true;   
        }

        // Exception message file: Append helper stuff
        comm.ExcMsgFile.Append (msgRX, AppendSpec.OnAfterRXCompleted_CMID_EMPTY);
      }  
        
      else if ((msgRX.ResponseID == AppComm.CMID_TIMEOUT) || (msgRX.ResponseID == AppComm.CMID_STRLENERR))
      {
        // A comm. error (Timeout, ...) occurred:
      
        // Terminate the Diagnosis, if req.
        doc.Diag.Terminate ();
        // Update documents 'bRecord ...' members: Disable recording
        doc.bRecordSingleScan = false;
        doc.bRecordScans = false;
        doc.bRecordResults = false;
 
        // Ensure, that the mouse is not captured & the cursor is not clipped
        // (Capture & clipping takes place on mouse pressing/moving  over the 'pbDraw' PictureBox.) 
        this.pbDraw.Capture = false;
        Cursor.Clip = Screen.PrimaryScreen.Bounds;

        // Exception message file: Append helper stuff
        comm.ExcMsgFile.Append (msgRX, AppendSpec.OnAfterRXCompleted_CMID_TIMEOUT);
      }
      
      else if (msgRX.ResponseID == AppComm.CMID_ANSWER)
      {

        // Exception message file: Append helper stuff
        comm.ExcMsgFile.Append (msgRX, AppendSpec.OnAfterRXCompleted_CMID_ANSWER);
        
        // The message response is present:
        string sCmd = msgRX.CommandResponse;
        byte[] arbyPar = msgRX.ParameterResponse;
    
        System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
   
        // Get the parameter string from the parameter Byte array
        string sPar = Encoding.ASCII.GetString (arbyPar);
        
        // CD according to the message command
        switch ( sCmd ) 
        {
  
            //----------------------------------------------------------
            // General messages
            //----------------------------------------------------------
          
          case "LANGSYN":
            // Language synchronisation
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgLanguageSynchro->function failed" );
            }
            break;
          
            //----------------------------------------------------------
            // Script messages
            //----------------------------------------------------------
        
          case "SCRLIST":
            // ScriptList
            try
            {
              string sMsg = string.Format ("Main: SCRLIST: {0}", sPar); 
              Debug.WriteLine ( sMsg );  
              
              // Update the Script list (= documents 'arsDeviceScripts' member)
              doc.UpdateScriptList (sPar);
              // Update view
              doc.UpdateAllViews ();
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptList->function failed" );
            }
            break;
        
          case "SCRSELECT":
            // ScriptSelect
            try
            {
              // Update view
              doc.UpdateAllViews ();
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptSelect->function failed" );
            }
            break;

          case "SCRSTART":
            // ScriptStart
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptStart->function failed" );
            }
            break;

          case "SCRSTOP":
            // ScriptStop
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptStop->function failed" );
            }
            break;

          case "SCRRESET":
            // ScriptReset
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptReset->function failed" );
            }
            break;

            //----------------------------------------------------------
            // Scan messages
            //----------------------------------------------------------

          case "SCANPARAMS":
            // ScanParams
            try
            {
              // Update the documents ScanParams member
              doc.ScanParams = new ScanParams (sPar);
              
              // Update the grafics default scale
              // NOTE:
              //  Ordinate: Because a scan on receiving is scaled to % unit, the basic ordinate unit is %.
              //  Abscissa: Because a scan on receiving is given in pts unit, the basic abscissa unit is pts.
              float xmin = 0;                    // Left abscissa margin in pts
              float xmax = Doc.NSCANDATA - 1;    // Right abscissa margin in pts
              app.View.GraphData.SetDefaultScale (xmin, xmax, -10, 110);  // Basic ordinate is in %: [ymin,ymax] = [0 - delta/10, 100 + delta/10], delta=(100 - 0)
              app.View.GraphData.FitScaleToDefaultData ();
            
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScanParams->function failed" );
            }
            break;
        
          case "SCANALLDATA":
            // ScanAllData
            try
            {
              // ---------------------------------------------------
              // ==> B: SCANALLDATA cmd receive handling

#if DEBUG 
              // CD: NODATA recognition or data received?
              string sMsgOpt;
              if ( sPar.StartsWith ("NODATA") ) sMsgOpt = "NODATA";   // NODATA recognition
              else                              sMsgOpt = "DATA";     // Data received
              // Debug announcement (Perform corr'ing Debug window output)
              if (bDbgWnd)
              {
                bDbgWnd = false;
                string sMsgDbg = DateTime.Now.ToString ("dd.MM.yy HH:mm:ss") + ": ==> " + sMsgOpt;
                Debug.WriteLine ( sMsgDbg );
              }
              // Optical announcement 
              // (Change the text of GB 'Results' in order to announce optically, 
              //  that a SCANALLDATA cmd has been received)
              string sResults = r.GetString ( "Main_gbResults" );
              if (this.gbResults.Text == sResults)
                this.gbResults.Text = sResults + " (==> RX: " + sMsgOpt + " <==)";
#endif

              // ==> E: SCANALLDATA cmd receive handling
              // ---------------------------------------------------


              // CD: NODATA recognition or data received?
              if ( sPar.StartsWith ("NODATA") )
              {
                // ***********************
                // NODATA recognition
                // ***********************

                string sScanInfo = "";
                int idxa = 0, idxe;

                // Exception message file: Append helper stuff
                comm.ExcMsgFile.Append (msgRX, AppendSpec.OnAfterRXCompleted_SAD_NODATA);
                
                // Retrieve the Scan info block
                idxe = sPar.IndexOf ('\t', idxa);
                if (-1 == idxe)
                {
                  string sMsg = string.Format ("ScanAllData - Erroneous NODATA recognition");
                  Debug.WriteLine ( sMsg );
                  return;
                }
                idxa = idxe+1;
                sScanInfo = sPar.Substring(idxa);     // Block: Scan info

                //-----------------------------------
                // SCANINFO data block
                // Note:
                //  This block must be analyzed first, because it must be checked, if the measurement 
                //  should be possibly stopped ( passively ).
              
                // Update the documents 'ScanInfo' member
                ScanInfo si = new ScanInfo ( sScanInfo );
                doc.ScanInfo = si;
              
                // Check, whether a measurement should be stopped passively:
                // This should be done, if the following 2 conditions are fulfilled: 
                //  1. The device is in the reset phase ( Menu ) and 
                //  2. The GSM SW runs a measurement
                if ( si.wDeviceAppStatus == 0 )       // Device in Reset phase ( Menu )
                {
                  if ( doc.bRunning )                 // Measurement is running on GMS
                  {
                    // Indicate, that the meas. should be stopped passively
                    meas.StopPassively = true;
                    break;
                  }
                }

                // Error / Warning Tooltip hints
                string sHint = doc.ScanInfo.ErrorAsHint ();
                this._ToolTip.SetToolTip(this.lblErr_Val, sHint);
                sHint = doc.ScanInfo.WarningAsHint ();
                this._ToolTip.SetToolTip(this.lblWar_Val, sHint);
                
                // Ensure, that at this point the Data recording routine was included
                // (Goal: Proper user notification after the Data recording has completed)
                ScanData scn = doc.arScanData[0];     
                doc.RecordData (scn);                 // parameter has no importance here

                // Update partial view
                // Notes:
                //  The error/warning members of the ScanInfo may change independently from 
                //  PC comm. intervals and so on, thats why they must be considered also in evaluation
                //  of NODATA receipts.
                app.View.UpdateDI_ErrWar ();          // Error/Warning labels

                // Increment the # of incoming NODATA responses
                doc.dwNoDataCount++;
              
              }
              else
              {
                // ***********************
                // DATA received
                // ***********************

                string sDeviceInfo = "", sScanInfo = "", sScanResult = "", sPeakInfo = "";
                int idxa = 0, idxe;
                            
                // Retrieve the data blocks (5 blocks):
                // First 4 blocks (Device info, Scan info, Scan result, Peak info)
                for (int i=0; i < 4 ;i++)
                {
                  idxe = sPar.IndexOf ('\t', idxa);
                  if (-1 == idxe)
                  {
                    string sMsg = string.Format ("ScanAllData - {0}. block separator is missed", i+1);
                    Debug.WriteLine ( sMsg );
                    return;
                  }
                  switch (i) 
                  {
                    case 0:
                      sDeviceInfo = sPar.Substring(idxa, idxe-idxa);  // 1. block: Device info
                      break;
                    case 1:
                      sScanInfo   = sPar.Substring(idxa, idxe-idxa);  // 2. block: Scan info
                      break;
                    case 2:
                      sScanResult = sPar.Substring(idxa, idxe-idxa);  // 3. block: Scan result
                      break;
                    case 3:
                      sPeakInfo   = sPar.Substring(idxa, idxe-idxa);  // 4. block: Peak info
                      break;
                  }
                  idxa = idxe+1;
                }
                // 5. block: Enhanced Scan data (string with (2+'nPoints') elements, separated by commas) 
                // Notes:
                //  Format: "<Point No>,<Gain>,<Elem_0>, ... ,<Elem_N>",
                //  with: N := <Point No> - 1 
                string sScanEnh = sPar.Substring (idxa);    // The enhanced Scan data string
                //      5.1. Point #
                int idx = sScanEnh.IndexOf (',');
                if (-1 == idx)
                  return;
                string sNo    = sScanEnh.Substring(0, idx); // The # of points curr'ly transferred
                doc.nPoints   = int.Parse (sNo);
                //      5.2. Gain
                idxa = idx+1;
                idx = sScanEnh.IndexOf (',', idxa);
                if (-1 == idx)
                  return;
                string sGain  = sScanEnh.Substring(idxa, idx-idxa); // The Gain of the scan curr'ly transferred 
                doc.Gain = int.Parse(sGain);
                //      5.3. Scan itself
                string sScan  = sScanEnh.Substring(idx+1);  // The scan itself
                string[] arsScan;
                if (sScan.Length == 0) arsScan = new string[0];
                else                   arsScan= sScan.Split (','); // Build the scan element string array
                if (arsScan.Length != doc.nPoints)
                {
                  // Invalid scan
                  string s = string.Format ( "Scan error: {0} elements\n", arsScan.Length );
                  Debug.WriteLine ( s );
                  return;
                }
              
    
#if DEBUG                
                // Testing stuff: Lengths of the 4 data blocks
                int nDI = sDeviceInfo.Length;
                int nSI = sScanInfo.Length;
                int nSR = sScanResult.Length;
                int nPI = sPeakInfo.Length;
#endif
                
                
                //-----------------------------------
                // SCANINFO data block
                // Note:
                //  This block must be analyzed first, because it must be checked, if the measurement 
                //  should be possibly stopped ( passively ).
              
                // Update the documents 'ScanInfo' member
                ScanInfo si = new ScanInfo ( sScanInfo );
                doc.ScanInfo = si;
              
                // Check, whether a measurement should be stopped passively:
                // This should be done, if the following 2 conditions are fulfilled: 
                //  1. The device is in the reset phase ( Menu ) and 
                //  2. The GSM SW runs a measurement
                if ( si.wDeviceAppStatus == 0 )       // Device in Reset phase ( Menu )
                {
                  if ( doc.bRunning )                 // Measurement is running on GMS
                  {
                    // Indicate, that the meas. should be stopped passively
                    meas.StopPassively = true;
                    break;
                  }
                }
              
                // Check, whether a script change has taken place on device
                bool bScriptChanged = (si.byScriptIdx != doc.ScriptNameToIdx(doc.sCurrentScript));
                if (bScriptChanged)
                {
                  // Yes:
                  // Remember the name of the changed script 
                  // (because this is now the script currently selected = currently running on device)
                  doc.sCurrentScript = doc.ScriptNameFromIdx (si.byScriptIdx);
                }
                  
                // Error / Warning Tooltip hints
                string sHint = doc.ScanInfo.ErrorAsHint ();
                this._ToolTip.SetToolTip(this.lblErr_Val, sHint);
                sHint = doc.ScanInfo.WarningAsHint ();
                this._ToolTip.SetToolTip(this.lblWar_Val, sHint);
                
                //-----------------------------------
                // INFO data block
              
                // Update the documents 'DeviceInfo' member
                try
                {
                  DeviceInfo di = new DeviceInfo ( sDeviceInfo );
                  doc.DeviceInfo = di;
                }
                catch
                {
                  // Stop the measurement
                  // (This must be done depending on the program execution mode)
                  CmdUI cmdUI = app.CmdUI;
                  if (this._TimerTest.Enabled == true)
                  {
                    // testing mode:
                    // Simulate pressing of the 'Test' menu item
                    cmdUI.OnTest ();
                  }
                  else
                  {
                    // normal mode:
                    // B - until 16.8.10  
                    //                    // Simulate pressing of the 'Execute script' button
                    //                    AppComm comm = app.Comm;
                    //                    if (comm.IsOpen() && doc.bRunning)
                    //                    {
                    //                      cmdUI.OnExecuteScript();
                    //                    }
                    // E - until 16.8.10  
                    // B - from 16.8.10  
                    // Indicate, that the meas. should be stopped passively
                    meas.StopPassively = true;
                    // B - from 16.8.10  
                  }
                  // Message
                  string sMsg =  r.GetString ( "Main_Err_DI" );           // "Fehler beim Lesen der Ger�teinformation.\r\nBitte �berpr�fen Sie, ob die Uhrzeit des Ger�ts korrekt eigestellt wurde."
                  string sCap = r.GetString ( "Form_Common_TextError" );  // "Error"
                  MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error ); 
                  // rethrow the caught exception
                  throw;
                }

                // If required, append the device info to the diagnosis file
                doc.Diag.Append ( Diagnosis.Typ.Info );
              
                //-----------------------------------
                // SCANRESULT data block
              
                // Update the documents 'arScanResult' member:
                // Note: Because a ScanResult contains 9 elements, the value 'nCount' must be a true 
                //       multiple of 9 - this must be proofed for. 
                string[] ars = sScanResult.Split ( ',' );
                int nCount = ars.Length;
                if (nCount%9 > 0) nCount = (nCount/9)*9;
              
                doc.arScanResult.Clear ();
                for ( int i=0; i<nCount; i+=9 )
                {
                  ScanResult res = new ScanResult ();
              
                  res.sSubstance  = ars[i];                           // Substance name
                  res.dResult     = double.Parse ( ars[i+1], nfi );   // Meas. result
                  res.dwBeginTime = UInt32.Parse ( ars[i+2] );        // Meas. window begin (in pts)
                  res.dwWidthTime = UInt32.Parse ( ars[i+3] );        // Meas. window width (in pts)
                  res.dA1         = double.Parse ( ars[i+4], nfi );   // LO Alarm value
                  res.dA2         = double.Parse ( ars[i+5], nfi );   // HI Alarm value
                  res.sConcUnit   = ars[i+6];                         // Conc. unit
                  res.byConcIncredible = byte.Parse( ars[i+7] );      // Conc. incredibly?
                  res.dSpanFac    = double.Parse ( ars[i+8], nfi );   // Span factor

                  doc.arScanResult.Add ( res );
                }

                // If required, append the results to the diagnosis file
                doc.Diag.Append ( Diagnosis.Typ.Result );
              
                //-----------------------------------
                // PEAKINFO data block
              
                // Update the documents 'PeakInfo' member
                PeakInfo pi = new PeakInfo ( sPeakInfo );
                doc.PeakInfo = pi;
              
              
                //-----------------------------------
                // SCANDATA data block
              
                // Select the scan receive buffer for the raw scan
                ScanData scn = doc.arScanData[0];
              
                // Build the raw scan
                doc.BuildScanArray ( arsScan, ref scn );
              
                // Select the scan receive buffer for the smoothed scan
                ScanData scn_sm = doc.arScanData[1];

                // Build the smoothed scan
                doc.BuildSmoothedScanArray ( scn, ref scn_sm );
                
                // Perform Data recording (storage)
                doc.RecordData (scn);
              
                // Update view
                doc.UpdateAllViews ();
              
                // If required, append the scan to the diagnosis file
                doc.Diag.Append ( Diagnosis.Typ.Scan );
              
                // Increment the # of incoming DATA responses (scans)
                doc.dwScanCount++;

                // Indicate, whether a script change has taken place on device or not
                meas.MeasureChanged = bScriptChanged;
              
              }// E - if-else ( sPar.StartsWith ("NODATA") 
 
            }
            catch (System.Exception exc)
            {
              string sMsg = string.Format ("OnMsgScanAllData->function failed\nDetail: {0}", exc.Message);
              Debug.WriteLine (sMsg );
            }
            break;
          
            //----------------------------------------------------------
            // KEY triggering messages
            //----------------------------------------------------------
      
          case "KEYTRIG":
            // Trigger KEY cmd
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgTriggerKeyCmd->function failed" );
            }
            break;

            //----------------------------------------------------------
            // Service dialog
            //----------------------------------------------------------

          case "SVCREADDATA":
            // Service Read Data
            // RX: The service data from the device (Device section)
            try
            {
              string[] ars = sPar.Split ( '\t' );
              // The 1. string in the array is the Device No.            
              doc.sDevNo = ars[0];                
              // Update the 'Device information / Device No.' label
              this.lblDeviceNo_Val.Text = doc.sDevNo;
            }
            catch
            {
              Debug.WriteLine ( "OnMsgServiceReadData->function failed" );
            }
            break;
        
            //---------------------------------------------      
            // CombiNG embedding Messages
            //---------------------------------------------      
        
          case "ERRCONF":
            // Remote error confirmation
            // RX: "OK"
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgErrConf->function failed" );
            }
            break;
        
        }
      }//E - if (e.Message.Id = AppComm.CMID_ANSWER)

    }

    #endregion // event handling communication

    #region event handling controls

    /// <summary>
    /// 'Click' event of the btnAlarm control ( Alarm button )
    /// </summary>
    private void btnAlarm_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      CmdUI cmdUI = app.CmdUI;

      cmdUI.OnAlarmAcknowledge();
    }

    
    //---------------------------------------------------------------
    // Record panel
    
    /// <summary>
    /// 'Click' event of the data recording CheckBox controls
    /// </summary>
    private void _chkRecording_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      CmdUI cmdUI = app.CmdUI;

      if      ( sender == this._chkCurrScan )     cmdUI.OnRecCurrentScan(); 
      else if ( sender == this._chkScansCont )    cmdUI.OnRecScansContinuously();
      else if ( sender == this._chkResultsCont )  cmdUI.OnRecResultsContinuously();
    }

    /// <summary>
    /// 'Click' event of the 'Displayed grafic' Button control
    /// </summary>
    private void btnDisplayedGrafic_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      CmdUI cmdUI = app.CmdUI;

      cmdUI.OnRecGrafic ();
    }

    /// <summary>
    /// 'MouseHover' event of the Plot-Fit-Button controls
    /// </summary>
    private void btnDisplayedGrafic_MouseHover(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      string s = r.GetString ( "Main_btnDisplayedGrafic_Hover" );  // "Store the currently displayed scan";

      this.StatusBar.Panels[0].Text = s;
    }
    
    //---------------------------------------------------------------
    // Graph panel

    /// <summary>
    /// 'Click' event of the Plot-Fit-Button controls
    /// </summary>
    private void _btnPlot_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Vw vw = app.View;
 
      if ( !vw.GraphData.HasData())
        return;
      
      if      ( sender == this._btnPlot_FitDef )  vw.GraphData.FitScaleToDefaultData ();
      else if ( sender == this._btnPlot_FitX )    vw.GraphData.FitScaleToXData ();
      else if ( sender == this._btnPlot_FitY )    vw.GraphData.FitScaleToYData ();
      else if ( sender == this._btnPlot_FitXY )   vw.GraphData.FitScaleToXYData ();

      vw.UpdateGraph ();
    }

    /// <summary>
    /// 'MouseHover' event of the Plot-Fit-Button controls
    /// </summary>
    private void _btnPlot_MouseHover(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      string s = string.Empty;
      if      ( sender == this._btnPlot_FitDef )  s = r.GetString ( "Main_btnPlot_FitDef_Hover" );  // "Fit to Default";
      else if ( sender == this._btnPlot_FitX )    s = r.GetString ( "Main_btnPlot_FitX_Hover" );    // "Fit to X";
      else if ( sender == this._btnPlot_FitY )    s = r.GetString ( "Main_btnPlot_FitY_Hover" );    // "Fit to Y";
      else if ( sender == this._btnPlot_FitXY )   s = r.GetString ( "Main_btnPlot_FitXY_Hover" );   // "Fit to XY";

      this.StatusBar.Panels[0].Text = s;
    }
    
    /// <summary>
    /// 'SizeChanged' event of the 'pbDraw' Picturebox control
    /// </summary>
    private void pbDraw_SizeChanged(object sender, System.EventArgs e)
    {
      try 
      {
        App app = App.Instance;
        Vw vw = app.View;

        vw.UpdateGraph ();
      }
      catch ( System.Exception exc ) 
      {
        string sMsg = string.Format ( "MainForm.pbDraw_SizeChanged -> function failed\nMessage: {0}\n", exc.Message );
        Debug.WriteLine ( sMsg );
      }
    }

    /// <summary>
    /// 'MouseDown' event of the 'pbDraw' Picturebox control
    /// </summary>
    private void pbDraw_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
    {
      App app = App.Instance;
      Vw vw = app.View;

      vw.OnMouseDown ( sender, e );
    }

    /// <summary>
    /// 'MouseMove' event of the 'pbDraw' Picturebox control
    /// </summary>
    private void pbDraw_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
    {
      App app = App.Instance;
      Vw vw = app.View;

      vw.OnMouseMove ( sender, e );
    }

    /// <summary>
    /// 'MouseUp' event of the 'pbDraw' Picturebox control
    /// </summary>
    private void pbDraw_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
    {
      App app = App.Instance;
      Vw vw = app.View;

      vw.OnMouseUp ( sender, e );
    }

    /// <summary>
    /// 'SelectedIndexChanged' event of the 'cbScansToDraw' CB control
    /// </summary>
    private void cbScansToDraw_SelectedIndexChanged(object sender, System.EventArgs e)
    {
      int idx = cbScansToDraw.SelectedIndex;
      if (-1 == idx)
        return;

      App app = App.Instance;
      Doc doc = app.Doc;
      Vw vw = app.View;

      // Reset the "Scan(s) to be drawn" array
      for (int i=0; i < Doc.MAX_SCANBUFFERS; i++)
        doc.arScanToDraw[i] = false;
      // Set the "Scan(s) to be drawn" array elememts corr'ing to the selection
      switch (idx)
      {
        case 0:   // Both
          for (int i=0; i < Doc.MAX_SCANBUFFERS; i++)
            doc.arScanToDraw[i] = true;
          break;
        case 1:   // Raw scan
          doc.arScanToDraw[0] = true;
          break;
        case 2:   // Complete scan
          doc.arScanToDraw[1] = true;
          break;
      }
      
      // Update the graphics
      vw.UpdateGraph ();
    }

    //---------------------------------------------------------------
    // Result panel

    /// <summary>
    /// 'Click' event of the ACK-Button control
    /// </summary>
    private void _btnErrAck_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;

      // TX: Error confirmation
      CommMessage msg = new CommMessage (comm.msgErrConf);
      msg.WindowInfo = new WndInfo (this.Handle, false);
      comm.WriteMessage (msg);
    }
    
    /// <summary>
    /// 'CheckedChanged' event of the Result-Checkbox controls
    /// </summary>
    private void _chk_CheckedChanged(object sender, System.EventArgs e)
    {
      int nID = 0;
      if      ( sender == this._chkRes_0 )    nID = 0;
      else if ( sender == this._chkRes_1 )    nID = 1;
      else if ( sender == this._chkRes_2 )    nID = 2;
      else if ( sender == this._chkRes_3 )    nID = 3;
      else if ( sender == this._chkRes_4 )    nID = 4;
      else if ( sender == this._chkRes_5 )    nID = 5;
      else if ( sender == this._chkRes_6 )    nID = 6;
      else if ( sender == this._chkRes_7 )    nID = 7;
      else if ( sender == this._chkRes_8 )    nID = 8;
      else if ( sender == this._chkRes_9 )    nID = 9;
      else if ( sender == this._chkRes_10 )   nID = 10;
      else if ( sender == this._chkRes_11 )   nID = 11;
      else if ( sender == this._chkRes_12 )   nID = 12;
      else if ( sender == this._chkRes_13 )   nID = 13;
      else if ( sender == this._chkRes_14 )   nID = 14;
      else if ( sender == this._chkRes_15 )   nID = 15;
          
      App app = App.Instance;
      Doc doc = app.Doc;
            
      // Indicate, that the window, that corr's to the ChB, should be (un)marked
      doc.nMarkedWindow[nID] = ((CheckBox)sender).Checked;
      // Emphasize the marked window
      doc.UpdateAllViews ();
    }

 
    //---------------------------------------------------------------
    // Printing

    /// <summary>
    /// 'BeginPrint' event of the PrintDocument control
    /// </summary>
    private void PrintDocument_BeginPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
      App app = App.Instance;
      Vw vw = app.View;
      
      vw.OnBeginPrinting ( e );
    }

    /// <summary>
    /// 'EndPrint' event of the PrintDocument control
    /// </summary>
    private void PrintDocument_EndPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
      App app = App.Instance;
      Vw vw = app.View;
      
      vw.OnEndPrinting ( e );
    }

    /// <summary>
    /// 'PrintPage' event of the PrintDocument control
    /// </summary>
    private void PrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
    {
      App app = App.Instance;
      Vw vw = app.View;
      
      vw.OnPrint ( e );
    }


    /// <summary>
    /// 'DrawItem' event of the StatusBar control
    /// (used here for Panel1 with the Ownerdraw style)
    /// </summary>
    /// <remarks>
    /// Regarding the output contents the following combinations exist:
    /// Comm. error?    WarmUp?   Comm. open?     Combi       Output
    ///                                           sinnvoll?              
    /// -------------------------------------------------------------------------------------
    /// Yes             Yes       Yes               o         "Comm. errors: ..."
    /// Yes             Yes       No                x         ["Comm. errors: ..."]
    /// Yes             No        Yes               o         "Comm. errors: ..."
    /// Yes             No        No                x         ["Comm. errors: ..."]
    /// No              Yes       Yes               x         "WarmUp running"
    /// No              Yes       No                x         "Not connected"
    /// No              No        Yes               x         "Measuring in execution" resp. "Flush mode" 
    /// No              No        No                x         "Not connected"
    /// </remarks>
    private void StatusBar_DrawItem(object sender, System.Windows.Forms.StatusBarDrawItemEventArgs sbdevent)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      Font f;
      Size si;
      string sPanel1 = "";

      // Panel1: Text alignment
      StringFormat sf = new StringFormat();
      sf.Alignment = StringAlignment.Near;        // hor'ly: Left analog to Panels 0, 2, 3
      sf.LineAlignment = StringAlignment.Center;  // ver'ly: centered

      // Check: Comm. errors present?
      int errCount = comm.ErrorCount;
#if !DEBUG
      // Release: Do NOT visualize comm. errors
      errCount = 0;
#endif
      if (errCount > 0)
      {
        // Yes:
        // Text
        string sFmt = r.GetString ( "Main_Status_P1_CommError" );     //"Comm. errors: {0}"
        sPanel1 = string.Format (sFmt, errCount);
        // UI
        sbdevent.Graphics.FillRectangle(Brushes.White, sbdevent.Bounds);
        f = StatusBar.Font;
        sbdevent.Graphics.DrawString(sPanel1, f, Brushes.Red, sbdevent.Bounds, sf);
      }
      else
      {
        // No:
        // Check: Comm. open?
        if ( !comm.IsOpen () ) 
        {
          // No:
          // Text
          sPanel1 = r.GetString ( "Main_Status_P1_NotConnected" );    //"Not connected"
          // UI
          sbdevent.Graphics.FillRectangle(SystemBrushes.Control, sbdevent.Bounds);
          f = StatusBar.Font;
          sbdevent.Graphics.DrawString(sPanel1, f, Brushes.Black, sbdevent.Bounds, sf);
        }
        else
        {
          // Yes:
          // Check: WarmUp running?
          if (doc.bWarmUpRunning)
          {
            // Yes:
            // Text
            sPanel1 = r.GetString ( "Main_Status_P1_WarmUp" );        //"WarmUp running"
            // UI
            sbdevent.Graphics.FillRectangle(Brushes.White, sbdevent.Bounds);
            f = new Font (StatusBar.Font, FontStyle.Bold);
            sbdevent.Graphics.DrawString(sPanel1, f, Brushes.Blue, sbdevent.Bounds, sf);
          }
          else
          {
            // No:
            // Check: Measurement running?
            if ( doc.bRunning )
            {
              // Yes:
              // Text
              sPanel1 = r.GetString ( "Main_Status_P1_Running" );     //"Measuring in execution"
            }
            else
            {
              // No:
              // Text
              sPanel1 = r.GetString ( "Main_Status_P1_Stopped" );     //"Flush mode"
            }
            // UI
            sbdevent.Graphics.FillRectangle(SystemBrushes.Control, sbdevent.Bounds);
            f = StatusBar.Font;
            sbdevent.Graphics.DrawString(sPanel1, f, Brushes.Black, sbdevent.Bounds, sf);
          }
        }
      }

      // Panel width
      si = sbdevent.Graphics.MeasureString ( sPanel1, f ).ToSize (); 
      sbdevent.Panel.Width = si.Width + 10;
    }
    
    #endregion // event handling controls

    #region members
    
    /// <summary>
    /// CommRS232 event handling
    /// </summary>
    CommRs232ResponseHandler _crh;
    
    /// <summary>
    /// Label array, containing the substance names of a scan result
    /// </summary>
    internal Label[] arlblRes = null;
    
    /// <summary>
    /// TextBox array, containing the result values of a scan result
    /// </summary>
    internal TextBox[] artxtRes = null;
    
    /// <summary>
    /// Label array, containing the display units of a scan result
    /// </summary>
    internal Label[] arlblResUnit = null;
    
    /// <summary>
    /// CheckBox array, indicating whether a scan result was checked
    /// </summary>
    internal CheckBox[] archkRes = null;
    
    /// <summary>
    /// The hashtable, which containes the required Enabled states of all menuitems
    /// when the PrintPreview is active (shown)
    /// </summary>
    Hashtable _htMenuItemsEnabledStateOnPrintPreview = new Hashtable ();

    /// <summary>
    /// Manages the display of status messages from menu items and/or tool bar buttons 
    /// in a status bar panel
    /// </summary>
    Utilities.StatusMessage StatusMessages = new Utilities.StatusMessage ();

    /// <summary>
    /// Manages the operation of a ToolBarButton like the corr'ing MenuItem was selected
    /// </summary>
    Utilities.ToolbarFunction TBFunction = new Utilities.ToolbarFunction ();
 
    /// <summary>
    /// Timer counter
    /// </summary>
    int _nTimerCounter = 0;

#if DEBUG 
    // Debug window output during PC comm.
    int nDbgWnd = 0;
    bool bDbgWnd = false;
#endif

    #endregion // members

    #region methods
   
    /// <summary>
    /// Initiates the form.
    /// </summary>
    void _Init () 
    {
      //-------------------------------
      // Definition of the control arrays
      
      // Label array, containing the substance names of a scan result
      arlblRes = new Label[] {
                               _lblRes_0, _lblRes_1, _lblRes_2, _lblRes_3, _lblRes_4, _lblRes_5, _lblRes_6, _lblRes_7,
                               _lblRes_8, _lblRes_9, _lblRes_10, _lblRes_11, _lblRes_12, _lblRes_13, _lblRes_14, _lblRes_15 
                             };
      // TextBox array, containing the result values of a scan result
      artxtRes = new TextBox[] { 
                                 _txtRes_0, _txtRes_1, _txtRes_2, _txtRes_3, _txtRes_4, _txtRes_5, _txtRes_6, _txtRes_7,
                                 _txtRes_8, _txtRes_9, _txtRes_10, _txtRes_11, _txtRes_12, _txtRes_13, _txtRes_14, _txtRes_15 
                               };
      // Label array, containing the display units of a scan result
      arlblResUnit = new Label[] { 
                                   _lblResUnit_0, _lblResUnit_1, _lblResUnit_2, _lblResUnit_3, _lblResUnit_4, _lblResUnit_5, _lblResUnit_6, _lblResUnit_7,
                                   _lblResUnit_8, _lblResUnit_9, _lblResUnit_10, _lblResUnit_11, _lblResUnit_12, _lblResUnit_13, _lblResUnit_14, _lblResUnit_15
                                 };
      // CheckBox array, indicating whether a scan result was checked
      archkRes = new CheckBox[] {
                                  _chkRes_0, _chkRes_1, _chkRes_2, _chkRes_3, _chkRes_4, _chkRes_5, _chkRes_6, _chkRes_7,
                                  _chkRes_8, _chkRes_9, _chkRes_10, _chkRes_11, _chkRes_12, _chkRes_13, _chkRes_14, _chkRes_15
                                };
      
    
      //-------------------------------
      // Printcontroller problem

      // Notes (22.1.08):
      //  In order to suppress the undesirable 'Generate page view' status window on print preview, 
      //  try to replace the PrintDocuments default PrintController ('PrintControllerWithStatusDialog')
      //  by the 'StandardPrintController' or the 'PreviewPrintController' - may be it works?!
      //  -> Result: It doesn't work: The status windows remains unsuppressed.
//      this.PrintDocument.PrintController = new System.Drawing.Printing.StandardPrintController ();
//      this.PrintDocument.PrintController = new System.Drawing.Printing.PreviewPrintController ();
    

      //-------------------------------
      // Not required menuitems
      
      // View menu: Statical PrintPreview
      this._miView_Sep1.Visible = false;
      this._miView_StaticalPrintPreview.Visible = false;


      //-------------------------------
      // Statusbar, Toolbar

      // Assign the status bar panel, the messages should appear within
      this.StatusMessages.StatusBar = this.StatusBar.Panels[0];
      
      // Assign 'Click' events to toolbar buttons, based on the corr'ing MenuItems
      this.TBFunction.SetToolbarFunction(this._tbbConnectDevice, this._miControl_ConnectDevice);             // Connect device
      this.TBFunction.SetToolbarFunction(this._tbbOpenConfigFile, this._miFile_OpenConfigFile);              // Open configuration file ...
      this.TBFunction.SetToolbarFunction (this._tbbTransmitConfigData, this._miControl_TransmitConfigData);   // Transmit configuration data
      this.TBFunction.SetToolbarFunction (this._tbbRereadConfigData, this._miControl_RereadScriptNames);      // Reread script names
      this.TBFunction.SetToolbarFunction(this._tbbOpenServiceDataFile, this._miFile_OpenServiceDataFile);              // Open configuration file ...
      this.TBFunction.SetToolbarFunction(this._tbbTransmitServiceData, this._miControl_TransmitServiceData);   // Transmit configuration data
      this.TBFunction.SetToolbarFunction(this._tbbSelectScript, this._miControl_SelectScript);               // Select script ...
      this.TBFunction.SetToolbarFunction (this._tbbExecuteScript, this._miControl_ExecuteScript);             // Execute script
      this.TBFunction.SetToolbarFunction(this._tbbTriggerKeyCmd, this._miControl_TriggerKeyCmd);             // Trigger Key cmd
      this.TBFunction.SetToolbarFunction(this._tbbProperties, this._miFile_Properties);                      // Properties ...
      this.TBFunction.SetToolbarFunction(this._tbbReadStoredSDcardData, this._miTools_SDStore);              // Read stored SDcard data ...
      this.TBFunction.SetToolbarFunction (this._tbbAbout, this._miHelp_About);                                // About ...

      //-------------------------------
      // CommRS232 event handling
      _crh = new CommRs232ResponseHandler (OnRXCompleted);
   
      //-------------------------------
      // Flow display
      App app = App.Instance;
      this.lblFlow.Visible = app.Data.Program.bDisplayFlow;
      this.lblFlow_Val.Visible = app.Data.Program.bDisplayFlow;
    }

    /// <summary>
    /// Updates the Toolbar buttons.
    /// </summary>
    void _UpdateToolbarButtons () 
    {
      bool bEnable, bCheck;

      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;

      ///////////////////////////////////////////////////////////////
      // File menu

      // Open configuration file ...
      bEnable = !comm.IsOpen(); 
      this._tbbOpenConfigFile.Enabled = bEnable;
      // Open service data file ...
      bEnable = !comm.IsOpen();
      this._tbbOpenServiceDataFile.Enabled = bEnable;
      // Properties ...
      bEnable = true; 
      this._tbbProperties.Enabled = bEnable;

      ///////////////////////////////////////////////////////////////
      // View menu

      ///////////////////////////////////////////////////////////////
      // Control menu

      // Connect device
      bCheck = comm.IsOpen(); 
      this._tbbConnectDevice.Pushed = bCheck;
      bEnable = !doc.bRunning; 
      this._tbbConnectDevice.Enabled = bEnable;
      // Transmit configuration data
      bEnable = comm.IsOpen() &&  !doc.bRunning; 
      this._tbbTransmitConfigData.Enabled = bEnable;
      // Transmit service data
      bEnable = comm.IsOpen() && !doc.bRunning;
      this._tbbTransmitServiceData.Enabled = bEnable;
      // Reread script names
      bEnable = comm.IsOpen() &&  !doc.bRunning; 
      this._tbbRereadConfigData.Enabled = bEnable;
      // Select script ...
      bEnable = comm.IsOpen() &&  !doc.bRunning; 
      this._tbbSelectScript.Enabled = bEnable;
      // Execute script
      bEnable = comm.IsOpen(); 
      this._tbbExecuteScript.Enabled = bEnable;
      bCheck = doc.bRunning; 
      this._tbbExecuteScript.Pushed = bCheck;
      // Trigger Key cmd
      bEnable = comm.IsOpen() &&  doc.bRunning; // Enabled, if  1) comm. is established & a script runs AND  
      bEnable &= !btnAlarm.Visible;             //              2) an alarm is NOT present AND
      bool bDevReadyForMeas = doc.ScanInfo.GetReadyForMeas();   
      bEnable &= bDevReadyForMeas;              //              3) the device is ready for measurement
      this._tbbTriggerKeyCmd.Enabled = bEnable;

      ///////////////////////////////////////////////////////////////
      // Data recording menu

      ///////////////////////////////////////////////////////////////
      // Data transfer menu

      // Read stored data ...
      bEnable = comm.IsOpen() && !doc.bRunning;
      this._tbbReadStoredSDcardData.Enabled = bEnable;

      ///////////////////////////////////////////////////////////////
      // Help menu

      // About ...
      this._tbbAbout.Enabled = true;
    }
    
    /// <summary>
    /// Updates the language.
    /// </summary>
    public void UpdateCulture () 
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      //---------------------------------------
      // Controls
      this.Text                       = r.GetString ( "Main_Title" );                 // "PID Control"  
      
      this.gbDeviceInfo.Text          = r.GetString ( "Main_gbDeviceInfo" );          // "Device information"
      this.lblCurScr.Text             = r.GetString ( "Main_lblCurScr" );             // "Script:"
      this.lblDeviceNo.Text           = r.GetString ( "Main_lblDeviceNo" );           // "Device No.:"
      this.lblTemp.Text               = r.GetString ( "Main_lblTemp" );               // "Temp. [�C]:"
      this.lblPres.Text               = r.GetString ( "Main_lblPres" );               // "Pres. [kPa]:"
      this.lblFlow.Text               = r.GetString ( "Main_lblFlow" );               // "Flow [ml/min]:"
      this.lblErr.Text                = r.GetString ( "Main_lblErr" );                // "Ger�tefehler:"
   
      this.gbResults.Text             = r.GetString ( "Main_gbResults" );             // "Results"

      this.gbPeakInfo.Text            = r.GetString ( "Main_gbPeakInfo" );            // "Peak information"
      this.colAbscissa.Text           = r.GetString ( "Main_lvPeakInfo_colAbscissa" );// "Position [pts]"     
      this.colHeight.Text             = r.GetString ( "Main_lvPeakInfo_colHeight" );  // "Height [%]"   
      this.colArea.Text               = r.GetString ( "Main_lvPeakInfo_colArea" );    // "Area" 

      this.gbSpecRec.Text             = r.GetString ( "Main_gbSpecRec" );             // "Spectrum recording"
      this._chkCurrScan.Text          = r.GetString ( "Main_chkCurrScan" );           // "Current Spectrum"
      this._chkScansCont.Text         = r.GetString ( "Main_chkScansCont" );          // "Spectra continuously"
      this._chkResultsCont.Text       = r.GetString ( "Main_chkResultsCont" );        // "Results continuously"
      this.btnAlarm.Text              = r.GetString ( "Main_btnAlarm" );              // "Alarm (Please confirm)"

      this.lblNoOfPts.Text            = r.GetString ( "Main_lblNoOfPts" );            // "Points:"

      // CB 'cbScansToDraw'
      int idx = this.cbScansToDraw.SelectedIndex;
      this.cbScansToDraw.Items.Clear ();
      this.cbScansToDraw.Items.Add (r.GetString ( "Main_cbScansToDraw_Both" ));       // "Beide"
      this.cbScansToDraw.Items.Add (r.GetString ( "Main_cbScansToDraw_Raw" ));        // "Rohdaten"
      this.cbScansToDraw.Items.Add (r.GetString ( "Main_cbScansToDraw_Smooth" ));     // "Enddaten"
      if (-1 == idx) idx = 0;      
      this.cbScansToDraw.SelectedIndex = idx;

      //---------------------------------------
      // MainMenu: Items
      this._miFile.Text                   = r.GetString("Main_miFile");                  // "&File"
      this._miFile_ScriptEditor.Text      = r.GetString("Main_miFile_ScriptEditor");     // "&Script editor ..."
      this._miFile_OpenConfigFile_Txt.Text = r.GetString("Main_miFile_OpenConfigFile_Txt"); // "&Open configuration file (plain text) ..."
      this._miFile_OpenConfigFile.Text    = r.GetString("Main_miFile_OpenConfigFile");      // "&Open configuration file ..."
      this._miFile_ServiceDataEditor.Text = r.GetString("Main_miFile_ServiceDataEditor");   // "Serv&ice data editor ..."
      this._miFile_OpenServiceDataFile.Text = r.GetString("Main_miFile_OpenServiceDataFile"); // "Ope&n service data file ..."
      this._miFile_Print.Text = r.GetString("Main_miFile_Print");            // "&Print ..."
      this._miFile_PrinterAdjustment.Text = r.GetString ( "Main_miFile_PrinterAdjustment" ); // "Printer &adjustment ..."
      this._miFile_PrintPreview.Text      = r.GetString ( "Main_miFile_PrintPreview" );     // "P&rint preview"
      this._miFile_Properties.Text        = r.GetString ( "Main_miFile_Properties" );       // "Proper&ties ..."
      this._miFile_Exit.Text = r.GetString("Main_miFile_Exit");             // "&Exit"

      this._miView.Text                       = r.GetString ( "Main_miView" );            // "&View"
      this._miView_Default.Text               = r.GetString ( "Main_miView_Default" );    // "&Default size"
      this._miView_StaticalPrintPreview.Text  = r.GetString ( "Main_miView_StaticalPrintPreview" ); // "Statical &print preview"
      this._miView_Statusbar.Text             = r.GetString ( "Main_miView_Statusbar" );  // "&Statusbar"
      this._miView_Toolbar.Text               = r.GetString ( "Main_miView_Toolbar" );    // "&Toolbar"

      this._miControl.Text                      = r.GetString ( "Main_miControl" );                     // "&Control"
      this._miControl_AcknowledgeAlarm.Text     = r.GetString ( "Main_miControl_AcknowledgeAlarm" );    // "Confirm &alarm"
      this._miControl_ConnectDevice.Text        = r.GetString ( "Main_miControl_ConnectDevice" );       // "&Connect device"
      this._miControl_ExecuteScript.Text        = r.GetString ( "Main_miControl_ExecuteScript" );       // "&Execute script"
      this._miControl_RereadScriptNames.Text    = r.GetString ( "Main_miControl_RereadScriptNames" );   // "&Reread script names"
      this._miControl_SelectScript.Text         = r.GetString ( "Main_miControl_SelectScript" );        // "&Select script ..."
      this._miControl_TransmitConfigData.Text   = r.GetString ( "Main_miControl_TransmitConfigData" );  // "&Transmit configuration data"
      this._miControl_TriggerKeyCmd.Text        = r.GetString ( "Main_miControl_TriggerKeyCmd" );       // "Trigger KE&Y command"
      this._miControl_TransmitServiceData.Text  = r.GetString("Main_miControl_TransmitServiceData"); // "Transmit service &data"

      this._miDataRecording.Text                      = r.GetString ( "Main_miDataRecording" );                   // "&Data recording"
      this._miDataRecording_CurrentScan.Text          = r.GetString ( "Main_miDataRecording_CurrentScan" );       // "&Current scan"
      this._miDataRecording_ResultsContinuously.Text  = r.GetString ( "Main_miDataRecording_ResultsContinuously" ); // "&Results continuously"
      this._miDataRecording_ScansContinuously.Text    = r.GetString ( "Main_miDataRecording_ScansContinuously" ); // "Sc&ans continuously"
      this._miDataRecording_Grafic.Text               = r.GetString ( "Main_miDataRecording_Grafic" );            // "Displayed &grafic"

      this._miTools.Text                = r.GetString ( "Main_miTools" );                // "&Tools"
      this._miTools_EEStore.Text        = r.GetString ( "Main_miTools_EEStore" );        // "Read/Clear stored &Eeprom data ..."
      this._miTools_SDStore.Text        = r.GetString ( "Main_miTools_SDStore" );        // "Read/Clear stored &SDcard data ..."
      this._miTools_Service.Text        = r.GetString ( "Main_miTools_Service" );        // "&Transfer service data ..."
      this._miTools_MP.Text             = r.GetString ( "Main_miTools_MP" );             // "&Transfer MP data ..."
      this._miTools_CTS.Text            = r.GetString ( "Main_miTools_CTS" );            // "Transfer &clock-timed script data ..."
      this._miTools_Diag.Text           = r.GetString ( "Main_miTools_Diag" );           // "&Diagnosis"
      this._miTools_PCcommListener.Text = r.GetString ( "Main_miTools_PCcommListener" ); // "&PC communication listener"
      this._miTools_LoadScanFromHD.Text = r.GetString ( "Main_miTools_LoadScanFromHD" ); // "&Load scan from HD ..."

      this._miHelp.Text       = r.GetString ( "Main_miHelp" );        // "&?"
      this._miHelp_About.Text = r.GetString ( "Main_miHelp_About" );  // "&About ..."
      this._miHelp_Test.Text  = r.GetString ( "Main_miHelp_Test" );   // "&Test"

      //---------------------------------------
      // Assign status messages to menu items:
      string s = string.Empty;
      
      // File menu
      this.StatusMessages.SetStatusMessage(this._miFile, null);
      s = r.GetString ( "Main_miFile_ScriptEditor_Select" );                              // "Opens the script editor";
      this.StatusMessages.SetStatusMessage(this._miFile_ScriptEditor, s);                 // Script editor ...
      s = r.GetString("Main_miFile_OpenConfigFile_Txt_Select");                           // "Opens an existing configuration file (plain text)";
      this.StatusMessages.SetStatusMessage(this._miFile_OpenConfigFile_Txt, s);           // Open configuration file (plain text) ...
      s = r.GetString("Main_miFile_OpenConfigFile_Select");                               // "Opens an existing configuration file";
      this.StatusMessages.SetStatusMessage(this._miFile_OpenConfigFile, s);               // Open configuration file ...
      s = r.GetString("Main_miFile_ServiceDataEditor_Select");                            // "Opens the service data editor";
      this.StatusMessages.SetStatusMessage(this._miFile_ServiceDataEditor, s);            // Service data editor ...
      s = r.GetString("Main_miFile_OpenServiceDataFile_Select");                          // "Opens an existing service data file";
      this.StatusMessages.SetStatusMessage(this._miFile_OpenServiceDataFile, s);          // Open service data file ...
      s = r.GetString ( "Main_miFile_Print_Select" );                                     // "Prints the active document";
      this.StatusMessages.SetStatusMessage(this._miFile_Print, s);                        // Print ...
      s = r.GetString ( "Main_miFile_PrintPreview_Select" );                              // "Shows the print preview";
      this.StatusMessages.SetStatusMessage(this._miFile_PrintPreview, s);                 // Print preview
      s = r.GetString ( "Main_miFile_PrinterAdjustment_Select" );                         // "Changes the printer and printing options";
      this.StatusMessages.SetStatusMessage(this._miFile_PrinterAdjustment, s);            // Printer adjustment ...
      s = r.GetString ( "Main_miFile_Properties_Select" );                                // "Permits the properties adjustment";
      this.StatusMessages.SetStatusMessage(this._miFile_Properties, s);                   // Properties ...
      s = r.GetString ( "Main_miFile_Exit_Select" );                                      // "Quits the application";
      this.StatusMessages.SetStatusMessage(this._miFile_Exit, s);                         // Exit
      
      // View menu
      this.StatusMessages.SetStatusMessage(this._miView, null);
      s = r.GetString ( "Main_miView_Toolbar_Select" );                                   // "Shows or hides the toolbar";
      this.StatusMessages.SetStatusMessage(this._miView_Toolbar, s);                      // Toolbar
      s = r.GetString ( "Main_miView_Statusbar_Select" );                                 // "Shows or hides the status bar";
      this.StatusMessages.SetStatusMessage(this._miView_Statusbar, s);                    // Statusbar
      s = r.GetString ( "Main_miView_Default_Select" );                                   // "Sets the default size and location";
      this.StatusMessages.SetStatusMessage(this._miView_Default, s);                      // Default
      s = r.GetString ( "Main_miView_StaticalPrintPreview_Select" );                      // "Shows the print preview either statically or dynamically";
      this.StatusMessages.SetStatusMessage(this._miView_StaticalPrintPreview, s);         // Statical print preview
      
      // Control menu
      this.StatusMessages.SetStatusMessage(this._miControl, null);
      s = r.GetString ( "Main_miControl_ConnectDevice_Select" );                          // "Opens the connection to the device";
      this.StatusMessages.SetStatusMessage(this._miControl_ConnectDevice, s);             // Connect device
      s = r.GetString ( "Main_miControl_TransmitConfigData_Select" );                     // "Transfers the data of the current configuration file to the device";
      this.StatusMessages.SetStatusMessage(this._miControl_TransmitConfigData, s);        // Transmit configuration data
      s = r.GetString("Main_miControl_TransmitServiceData_Select");                       // "Transfers the data of the current service data file to the device";
      this.StatusMessages.SetStatusMessage(this._miControl_TransmitServiceData, s);       // Transmit service data
      s = r.GetString("Main_miControl_RereadScriptNames_Select");                         // "Reads the names of the scripts stored in the device";
      this.StatusMessages.SetStatusMessage(this._miControl_RereadScriptNames, s);         // Reread script names
      s = r.GetString ( "Main_miControl_SelectScript_Select" );                           // "Selects a script from the list of the stored scripts";
      this.StatusMessages.SetStatusMessage(this._miControl_SelectScript, s);              // Select script ...
      s = r.GetString ( "Main_miControl_ExecuteScript_Select" );                          // "Executes the selected script";
      this.StatusMessages.SetStatusMessage(this._miControl_ExecuteScript, s);             // Execute script
      s = r.GetString ( "Main_miControl_AcknowledgeAlarm_Select" );                       // "Confirms alert";
      this.StatusMessages.SetStatusMessage(this._miControl_AcknowledgeAlarm, s);          // Acknowledge alarm
      s = r.GetString ( "Main_miControl_TriggerKeyCmd_Select" );                          // "Triggers the KEY command";
      this.StatusMessages.SetStatusMessage(this._miControl_TriggerKeyCmd, s);             // Trigger Key cmd
      
      // Data recording menu
      this.StatusMessages.SetStatusMessage(this._miDataRecording, null);
      s = r.GetString ( "Main_miDataRecording_CurrentScan_Select" );                      // "Records the next incoming spectrum";
      this.StatusMessages.SetStatusMessage(this._miDataRecording_CurrentScan, s);         // Current scan
      s = r.GetString ( "Main_miDataRecording_ScansContinuously_Select" );                // "Records the spectra continuously beginning with the next one";
      this.StatusMessages.SetStatusMessage(this._miDataRecording_ScansContinuously, s);   // Scans cont.
      s = r.GetString ( "Main_miDataRecording_ResultsContinuously_Select" );              // "Records the measuring results continuously beginning with the next value";
      this.StatusMessages.SetStatusMessage(this._miDataRecording_ResultsContinuously, s); // Results cont.
      s = r.GetString ( "Main_miDataRecording_Grafic_Select" );                           // "Records the displayed spectrum";
      this.StatusMessages.SetStatusMessage(this._miDataRecording_Grafic, s);              // Displayed grafic

      // Tools menu
      this.StatusMessages.SetStatusMessage(this._miTools, null);
      s = r.GetString ( "Main_miTools_EEStore_Select" );                                  // "Permits reading of the stored Eeprom data";
      this.StatusMessages.SetStatusMessage(this._miTools_EEStore, s);                     // Read stored Eeprom data ...
      s = r.GetString ( "Main_miTools_SDStore_Select" );                                  // "Permits reading of the stored SDcard data";
      this.StatusMessages.SetStatusMessage(this._miTools_SDStore, s);                     // Read stored SDcard data ...
      s = r.GetString ( "Main_miTools_Service_Select" );                                  // "Permits access to the service data";
      this.StatusMessages.SetStatusMessage(this._miTools_Service, s);                     // Transfer service data ...
      s = r.GetString ( "Main_miTools_MP_Select" );                                       // "Permits access to the MP data";
      this.StatusMessages.SetStatusMessage(this._miTools_MP, s);                          // Transfer MP data ...
      s = r.GetString ( "Main_miTools_CTS_Select" );                                      // "Permits access to the clock-timed script data";
      this.StatusMessages.SetStatusMessage(this._miTools_CTS, s);                         // Transfer clock-timed script data ...
      s = r.GetString ( "Main_miTools_Diag_Select" );                                     // "Performs a diagnostic routine";
      this.StatusMessages.SetStatusMessage(this._miTools_Diag, s);                        // Diagnosis
      s = r.GetString ( "Main_miTools_PCcommListener_Select" );                           // "Logs the PC communication to file";
      this.StatusMessages.SetStatusMessage(this._miTools_PCcommListener, s);              // PC communication listener
      s = r.GetString ( "Main_miTools_LoadScanFromHD_Select" );                           // "Loads and displays a scan from HD";
      this.StatusMessages.SetStatusMessage(this._miTools_LoadScanFromHD, s);              // Load scan from HD ...

      // ? menu
      this.StatusMessages.SetStatusMessage(this._miHelp, null);
      s = r.GetString ( "Main_miHelp_About_Select" );                                     // "Displays program information, version number and copyright";
      this.StatusMessages.SetStatusMessage(this._miHelp_About, s);                        // About ...
      s = r.GetString ( "Main_miHelp_Test_Select" );                                      // "Test functionality";
      this.StatusMessages.SetStatusMessage(this._miHelp_Test, s);                         // Test

      //---------------------------------------
      // Assign status messages to toolbar buttons:
      // Notes:
      //  The status message for the 'TriggerKeyCmd' TBB is set in the Timer routine, because its text 
      //  is related with the alarm case.

      s = r.GetString ( "Main_tbbConnectDevice" );                                        // "Connect device"
      this.StatusMessages.SetStatusMessage(this._tbbConnectDevice, s);
      s = r.GetString("Main_tbbOpenConfigFile");                                       // "Open configuration file"
      this.StatusMessages.SetStatusMessage(this._tbbOpenConfigFile, s);
      s = r.GetString("Main_tbbTransmitConfigData");                                   // "Transmit configuration data"
      this.StatusMessages.SetStatusMessage(this._tbbTransmitConfigData, s);
      s = r.GetString("Main_tbbRereadConfigData");                                     // "Reread script names"
      this.StatusMessages.SetStatusMessage(this._tbbRereadConfigData, s);
      s = r.GetString("Main_tbbOpenServiceDataFile");                                     // "Open service data file"
      this.StatusMessages.SetStatusMessage(this._tbbOpenServiceDataFile, s);
      s = r.GetString("Main_tbbTransmitServiceData");                                  // "Transmit service data"
      this.StatusMessages.SetStatusMessage(this._tbbTransmitServiceData, s);
      s = r.GetString("Main_tbbSelectScript");                                         // "Select script"
      this.StatusMessages.SetStatusMessage(this._tbbSelectScript, s);
      s = r.GetString("Main_tbbExecuteScript");                                        // "Execute script"
      this.StatusMessages.SetStatusMessage(this._tbbExecuteScript, s);
      s = r.GetString("Main_tbbProperties");                                           // "Properties"
      this.StatusMessages.SetStatusMessage(this._tbbProperties, s);
      s = r.GetString("Main_tbbReadStoredSDcardData");                                 // "Read stored SDcard data"
      this.StatusMessages.SetStatusMessage(this._tbbReadStoredSDcardData, s);                         
      s = r.GetString("Main_tbbAbout");                                                // "About"
      this.StatusMessages.SetStatusMessage(this._tbbAbout, s);                         

      // PARALLEL to that:
      // Assign Tooltip messages to toolbar buttons:
      // Notes:
      //  The Tooltip message for the 'TriggerKeyCmd' TBB is set in the Timer routine, because its text 
      //  is related with the alarm case.
      s = r.GetString ( "Main_tbbConnectDevice" ); this._tbbConnectDevice.ToolTipText = s;                // "Connect device"
      s = r.GetString("Main_tbbOpenConfigFile"); this._tbbOpenConfigFile.ToolTipText = s;              // "Open configuration file"
      s = r.GetString("Main_tbbTransmitConfigData"); this._tbbTransmitConfigData.ToolTipText = s;      // "Transmit configuration data"
      s = r.GetString("Main_tbbRereadConfigData"); this._tbbRereadConfigData.ToolTipText = s;          // "Reread script names"
      s = r.GetString("Main_tbbOpenServiceDataFile"); this._tbbOpenServiceDataFile.ToolTipText = s;    // "Open configuration file"
      s = r.GetString("Main_tbbTransmitServiceData"); this._tbbTransmitServiceData.ToolTipText = s;    // "Transmit service data"
      s = r.GetString("Main_tbbSelectScript"); this._tbbSelectScript.ToolTipText = s;                  // "Select script"
      s = r.GetString("Main_tbbExecuteScript"); this._tbbExecuteScript.ToolTipText = s;                // "Execute script"
      s = r.GetString("Main_tbbProperties"); this._tbbProperties.ToolTipText = s;                      // "Properties"
      s = r.GetString("Main_tbbReadStoredSDcardData"); this._tbbReadStoredSDcardData.ToolTipText = s;  // "Read stored SDcard data"
      s = r.GetString("Main_tbbAbout"); this._tbbAbout.ToolTipText = s;                                // "About"

      // Image Buttons
      s = r.GetString ( "Main_btnDisplayedGrafic_Hover" );
      this._ToolTip.SetToolTip(this.btnDisplayedGrafic, s);
      
      //---------------------------------------
      // Modeless dialogs  

    }

    /// <summary>
    /// Adjusts the program functionality according to the UserType settings 
    /// in the application data object (which are adjusted by means of the password input)
    /// </summary>
    /// <remarks>
    /// If the password input was correct for the Expert or the Trained Customer usertype,
    /// the menu items & toolbar buttons accessibility is structured as follows:
    /// Expert: All menu items & toolbar buttons are available,
    /// TC: A subset from this is available,
    /// Customer: A still smaller subset from this is available.
    /// </remarks>
    public void UpdateAccordingUserMode ()
    {
      App app = App.Instance;
      UserType ut = app.Data.MainForm.User;
      
      // 1. group: Access for Expert & TC
      Boolean b = ( ut == UserType.Customer ) ? false : true;

      // Menu items
      //    File
      this._miFile_OpenConfigFile_Txt.Visible = b;  // "Open configuration file (plain text)"

      // Toolbar buttons: None
    
      // 2. group: Access for Expert only
      b = ( ut == UserType.Expert ) ? true : false;

      // Menu items
      //    Tools
      this._miTools_MP.Visible                    = b; // Transfer MP data ..."
      this._miTools_PCcommListener.Visible        = b; // "PC communication listener" 
    
      // Toolbar buttons: None
    }


    #endregion // methods

 	}
}
