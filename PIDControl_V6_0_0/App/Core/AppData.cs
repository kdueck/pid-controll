using System;
using System.Windows.Forms;
using System.Drawing;
using System.Reflection;
using System.IO;
using System.Resources;
using System.Collections;
using System.Diagnostics;

namespace App
{
	/// <summary>
	/// Class AppData:
	/// The application data, hold in a resource file or in the registry
	/// </summary>
	public class AppData
	{
    #region constructors
    
    /// <summary>
    /// Constructor
    /// </summary>
    internal AppData()
		{
      // Name of the application data file 
      Assembly ass = Assembly.GetExecutingAssembly ();
      _sAppFilepath = ass.Location;
      _sAppDataFilepath = string.Format ( @"{0}\{1}{2}", 
        Path.GetDirectoryName ( _sAppFilepath ),
        Path.GetFileNameWithoutExtension ( _sAppFilepath ),
        ".resources"
        );
    
      // Read the data ( Init. members )
      Read ();
   
      // Secure, that the comm. Baudrate was set to 115 KB
      Communication.dwBaudRate = CommunicationData.CBR_115200;
    }

    #endregion // constructors

    #region constants

    // The keys of the resource file items:

    //-------------------------------
    // Common data

    // Full path of the script configuration file
    const string sScriptFilename = "ScriptFilename";
    // Full path of the service data file
    const string sServiceFilename = "ServiceFilename";
    // Folder where data storage takes place
    const string sDataFolder     = "DataFolder";
    // Folder where SDcard data storage takes place
    const string sSDcardFolder   = "SDcard DataFolder";
    // File where data storage takes place: Last Eeprom data file
    const string sEepromDataFile   = "Eeprom DataFilename";
    // File where data storage takes place: Last Eeprom script file
    const string sEepromScriptFile = "Eeprom ScriptFilename";
    // Lastly loaded & displayed Scanfile
    const string sDisplayScanFile = "DisplayScanFilename";

    // Last access to the application
    const string sLastUsage      = "Last usage";
    
    //-------------------------------
    // Program data

    // Y axis unit
    const string sYUnit          = "Yunit";
    const string sXUnit          = "Xunit";

    // New Service dialog style?
    const string sServiceNew = "Service";
    // New SDcard reader style?
    const string sSDcardReaderNew = "SDcardReader"; 
    
    //-------------------------------
    // Communication data

    // Communication channel used ( COM1, ... )
    const string sChannel        = "Channel";
    // Communication baudrate used
    const string sBaudRate       = "BaudRate";
    
    //-------------------------------
    // Configuration data

    // Full path of the script configuration file last used
    const string sLastFile       = "LastFile";
    
    //-------------------------------
    // Language data

    // Language used ( de / en )
    const string sLanguage       = "Language";
    // Language synchronisation
    const string sLangSyn        = "LangSyn"; 

    #endregion // constants
    
    #region members

    /// <summary>
    /// Full path of the application file
    /// </summary>
    string _sAppFilepath = string.Empty;

    /// <summary>
    /// Full path of the application data file
    /// </summary>
    string _sAppDataFilepath = string.Empty;
    
    /// <summary>
    /// Indicates, whether for application data storage the registry should be used (true) or not (false)
    /// - in the 2nd case a resource file, placed in the app's directory, is managed.
    /// </summary>
    Boolean _bUseRegistry = true;

    // Data subclasses:

    /// <summary>
    /// MainForm data
    /// </summary>
    public MainFormData MainForm = new MainFormData ();

    /// <summary>
    /// Common data
    /// </summary>
    public CommonData Common = new CommonData ();

    /// <summary>
    /// Program data
    /// </summary>
    public ProgramData Program = new ProgramData ();

    /// <summary>
    /// Communication data
    /// </summary>
    public CommunicationData Communication = new CommunicationData ();
    
    /// <summary>
    /// Configuration data
    /// </summary>
    public ConfigurationData Configuration = new ConfigurationData ();

    /// <summary>
    /// Language data
    /// </summary>
    public LanguageData Language = new LanguageData ();

    #endregion // members

    #region internal classes

    /// <summary>
    /// Class MainForm data
    /// </summary>
    public class MainFormData 
    {
      /// <summary>
      /// Dafault bounds of the main window
      /// </summary>
      public Rectangle rDefaultBounds   = Rectangle.Empty;

      /// <summary>
      /// Reflects whether the print preview panel is visible ( true ) or not ( false ) 
      /// </summary>
      public bool bPrintPreview         = false;
      
      /// <summary>
      /// Reflects whether the mainform has a MaximizeBox ( true ) or not ( false )
      /// </summary>
      public bool bMaximizeBoxPresent   = true;

      /// <summary>
      /// Reflects whether the print preview is shown statically ( without update ) or dynamically
      /// ( with update )
      /// </summary>
      public bool bStaticalPrintPreview = true;

      /// <summary>
      /// Indicates, whether an Expert logged in (usertype = Expert), or a Trained Customer logged in 
      /// (usertype = TrainedCustomer), or a Customer logged in (usertype = Customer, Default) 
      /// </summary>
      public UserType User = UserType.Customer;
    }
    
    /// <summary>
    /// Class Common data
    /// </summary>
    public class CommonData 
    {
      /// <summary>
      /// Name of the script configuration file
      /// </summary>
      public string sScriptFileName = "Test.gsm";
      /// <summary>
      /// Name of the service data file
      /// </summary>
      public string sServiceFileName = "";
      /// <summary>
      /// Folder where data storage takes place
      /// </summary>
      public string sDataFolder     = System.IO.Path.GetTempPath ();
      /// <summary>
      /// Folder where SDcard data storage takes place
      /// </summary>
      public string sSDcardFolder   = System.IO.Path.GetTempPath ();
      /// <summary>
      /// File where data storage takes place: Last Eeprom data file 
      /// </summary>
      public string sEepromDataFile = "";
      /// <summary>
      /// File where data storage takes place: Last Eeprom script file 
      /// </summary>
      public string sEepromScriptFile = "";
      /// <summary>
      /// Lastly loaded & displayed Scanfile
      /// </summary>
      public string sDisplayScanFile = "";
 
      /// <summary>
      /// Last access to the application
      /// </summary>
      public DateTime dtlastUsage   = DateTime.MinValue;

      /// <summary>
      /// AES: Secret key
      /// </summary>
      internal static byte[] AES_KEY = new byte[] { 0x58, 0x6B, 0x18, 0x91, 0x6B, 0xFD, 0x44, 0x1B,
                                         0xEC, 0xAF, 0x6C, 0xCB, 0xD3, 0x54, 0xC8, 0xAB,
                                         0x1D, 0xE6, 0x7A, 0x38, 0x64, 0x84, 0x2B, 0xF6,
                                         0x75, 0x56, 0x0E, 0xDF, 0xB6, 0x24, 0xDD, 0x98 };
      /// <summary>
      /// AES: Init. vector
      /// </summary>
      internal static byte[] AES_IV = new byte[] { 0xD2, 0xE5, 0xEA, 0x54, 0x47, 0xF8, 0xCA, 0x19,
                                        0x90, 0xE1, 0xD0, 0xBD, 0x36, 0xA9, 0xA0, 0x42 };

      /// <summary>
      /// User PF (8B hard coded, as used in Enky database)
      ///  NOTE:
      ///   This PW and the global 'PWusr' in the EnkyLCProgrammer PC SW must coincide!
      /// </summary>
      internal static byte[] USERPW = new byte[] { (byte)'R', (byte)'2', (byte)'D', (byte)'2', (byte)'R', (byte)'2', (byte)'D', (byte)'2' }; 
   
    }
  
    /// <summary>
    /// Class program data
    /// </summary>
    public class ProgramData 
    {
      /// <summary>
      /// Y unit used ( % / adc / mV )
      /// </summary>
      public YUnits eYUnit = YUnits.Percent;
      /// <summary>
      /// X unit used ( pts / s )
      /// </summary>
      public XUnits eXUnit = XUnits.Points;

      /// <summary>
      /// YUnits enumeration
      /// </summary>
      public enum YUnits 
      {
        Percent,
        Adc,
        mV
      };
      
      /// <summary>
      /// XUnits enumeration
      /// </summary>
      public enum XUnits 
      {
        Points,
        Seconds
      };

      /// <summary>
      /// New Service dialog style?
      /// </summary>
      public bool bServiceNew = false;

      /// <summary>
      /// New SDcard reader style?
      /// </summary>
      public bool bSDcardReaderNew = false;

      /// <summary>
      /// Indicates, whether the Flow data are displayed in UI (sec. device information), in printing stuff, and in file output:
      /// True, if yes; False otherwise
      /// </summary>
      /// <remarks>
      /// This adjustment is NOT stored in ressource file or registry - it is set depending on the presence and the adjustments of an EnkyLC dongle.
      /// </remarks>
      public bool bDisplayFlow = false;
    }
  
    /// <summary>
    /// Class Communication data
    /// </summary>
    public class CommunicationData
    {
      // Baud rates at which a communication device may operate
      public const UInt32 CBR_57600   = 57600;    // 57,6 KBaud
      public const UInt32 CBR_115200  = 115200;   // 115,2 KBaud
      
      /// <summary>
      /// Communication channel used ( COM1, ... ) (adjustable by the user)
      /// </summary>
      public string sCommChannel  = "COM1";
      /// <summary>
      /// Communication baudrate used (NOT adjustable by the user)
      /// </summary>
      public UInt32 dwBaudRate    = CBR_115200;
      /// <summary>
      /// Indicates, whether the Baudrate is fixed (True) or modifiable (False)
      /// </summary>
      public bool bBaudRateFixed  = true;
    }
  
    /// <summary>
    /// Class Configuration data 
    /// </summary>
    public class ConfigurationData 
    {
      /// <summary>
      /// Full path of the script configuration file last used
      /// </summary>
      public string sLastFile = string.Empty;
    }
  
    /// <summary>
    /// Class language data
    /// </summary>
    public class LanguageData 
    {
      /// <summary>
      /// Language used ( de / en )
      /// </summary>
      public Languages eLanguage = Languages.en;

      /// <summary>
      /// Languages enumeration
      /// </summary>
      public enum Languages 
      {
        /// <summary>
        /// Deutsch
        /// </summary>
        en,
        /// <summary>
        /// English
        /// </summary>
        de
      };

      /// <summary>
      /// Language synchronisation On (True) / Off (False)
      /// </summary>
      public bool bLangSyn = false;
    }

    #endregion // internal classes

    #region methods

    /// <summary>
    /// Reads the application data
    /// </summary>
    public void Read () 
    {
      if ( _bUseRegistry )  _ReadRegistry ();
      else                  _ReadResourceFile ();
    }

    /// <summary>
    /// Reads the application data via the registry
    /// </summary>
    void _ReadRegistry ()
    {
      try 
      {
        AppRegistry reg = new AppRegistry ( AppRegistry.regmodeRead );
        object o = null;
      
        // Common 
        reg.SetSubkey ( "Common" );
        reg.Access ( sScriptFilename, ref o );
        Common.sScriptFileName      = ( string ) o;
        reg.Access(sServiceFilename, ref o);
        Common.sServiceFileName     = (string)o;
        reg.Access(sDataFolder, ref o);
        Common.sDataFolder          = ( string ) o;
        reg.Access ( sSDcardFolder, ref o );
        Common.sSDcardFolder        = ( string ) o;
        reg.Access ( sEepromDataFile, ref o );
        Common.sEepromDataFile      = ( string ) o;
        reg.Access ( sEepromScriptFile, ref o );
        Common.sEepromScriptFile    = ( string ) o;
        reg.Access ( sDisplayScanFile, ref o );
        Common.sDisplayScanFile    = ( string ) o;
        reg.Access ( sLastUsage, ref o );
        Common.dtlastUsage          = DateTime.Parse ( ( string ) o );

        // Program
        reg.SetSubkey ( "Program" );
        reg.Access ( sYUnit, ref o );
        Program.eYUnit              = ( ProgramData.YUnits ) Enum.Parse ( typeof( ProgramData.YUnits ), (string) o );
        reg.Access ( sXUnit, ref o );
        Program.eXUnit              = ( ProgramData.XUnits ) Enum.Parse ( typeof( ProgramData.XUnits ), (string) o );
        reg.Access (sServiceNew, ref o);
        Program.bServiceNew         = bool.Parse ((string)o);
        reg.Access (sSDcardReaderNew, ref o);
        Program.bSDcardReaderNew    = bool.Parse ((string) o);

        // Communication
        reg.SetSubkey ( "Communication" );
        //    Channel: Ensure, that only non-empty channel strings are assigned
        reg.Access ( sChannel, ref o );
        string scommch = ( string ) o;
        if (scommch.Trim().Length > 0) Communication.sCommChannel  = scommch;
        //    Baudrate    
        if (!Communication.bBaudRateFixed)
        {
          reg.Access ( sBaudRate, ref o );
          Communication.dwBaudRate    = UInt32.Parse ( ( string ) o );
        }
        
        // Configuration
        reg.SetSubkey ( "Configuration" );
        reg.Access ( sLastFile, ref o );
        Configuration.sLastFile     = ( string ) o;
        
        // Language
        reg.SetSubkey ( "Language" );
        reg.Access ( sLanguage, ref o );
        Language.eLanguage          = ( LanguageData.Languages ) Enum.Parse ( typeof( LanguageData.Languages ), (string) o );
        reg.Access ( sLangSyn, ref o );
        Language.bLangSyn           = bool.Parse ((string) o);
      
      }
      catch
      {
        Debug.WriteLine ( "AppData._ReadRegistry() failed\n" );
      }
    }
    
    /// <summary>
    /// Reads the application data via a resource file (key - value pairs)
    /// </summary>
    void _ReadResourceFile () 
    {
      if ( System.IO.File.Exists ( _sAppDataFilepath ) ) 
      {
        ResourceReader resReader = null;
        try 
        {
          resReader = new ResourceReader( _sAppDataFilepath );
          IDictionaryEnumerator enRes = resReader.GetEnumerator ();
          while ( enRes.MoveNext() ) 
          {
            string sKey = enRes.Key.ToString();
            switch ( sKey ) 
            {
                // Common 
              case sScriptFilename:
                Common.sScriptFileName = ( string ) enRes.Value;
                break;
              case sServiceFilename:
                Common.sServiceFileName = (string)enRes.Value;
                break;
              case sDataFolder:
                Common.sDataFolder = ( string ) enRes.Value;
                break;
              case sSDcardFolder:
                Common.sSDcardFolder = ( string ) enRes.Value;
                break;
              case sEepromDataFile:
                Common.sEepromDataFile = ( string ) enRes.Value;
                break;
              case sEepromScriptFile:
                Common.sEepromScriptFile = ( string ) enRes.Value;
                break;
              case sDisplayScanFile:
                Common.sDisplayScanFile = ( string ) enRes.Value;
                break;
              case sLastUsage:
                Common.dtlastUsage = ( DateTime ) enRes.Value;
                break;

                // Program
              case sYUnit:
                Program.eYUnit = ( ProgramData.YUnits ) enRes.Value;
                break;
              case sXUnit:
                Program.eXUnit = ( ProgramData.XUnits ) enRes.Value;
                break;
              case sServiceNew:
                Program.bServiceNew = (bool)enRes.Value;
                break;
              case sSDcardReaderNew:
                Program.bSDcardReaderNew  = (bool) enRes.Value;
                break;

                // Communication
              case sChannel:
                Communication.sCommChannel = ( string ) enRes.Value;
                break;
              case sBaudRate:
                if (!Communication.bBaudRateFixed)
                {
                  Communication.dwBaudRate = ( UInt32 ) enRes.Value;
                }
                break;

                // Configuration
              case sLastFile:
                Configuration.sLastFile  = enRes.Value.ToString ();
                break;

                // Language
              case sLanguage:
                Language.eLanguage       = ( LanguageData.Languages ) enRes.Value;
                break;
              case sLangSyn:
                Language.bLangSyn        = (bool) enRes.Value;
                break;

            }
          }
        }
        catch 
        {
          Debug.WriteLine ( "AppData._ReadResourceFile() failed\n" );
        }
        finally 
        {
          if ( null != resReader ) resReader.Close();
        }
      }
    }

    /// <summary>
    /// Writes the application data
    /// </summary>
    public void Write () 
    {
      if ( _bUseRegistry )  _WriteRegistry ();
      else                  _WriteResourceFile ();
    }

    /// <summary>
    /// Writes the application data via the registry
    /// </summary>
    void _WriteRegistry ()
    {
      try 
      {
        AppRegistry reg = new AppRegistry ( AppRegistry.regmodeWrite );
        object o = null;
      
        // Common 
        reg.SetSubkey ( "Common" );
        o = Common.sScriptFileName;
        reg.Access ( sScriptFilename, ref o );
        o = Common.sServiceFileName;
        reg.Access(sServiceFilename, ref o);
        o = Common.sDataFolder;
        reg.Access ( sDataFolder, ref o );
        o = Common.sSDcardFolder;
        reg.Access ( sSDcardFolder, ref o );
        o = Common.sEepromDataFile;
        reg.Access ( sEepromDataFile, ref o );
        o = Common.sEepromScriptFile;
        reg.Access ( sEepromScriptFile, ref o );
        o = Common.sDisplayScanFile;
        reg.Access ( sDisplayScanFile, ref o );
        o = Common.dtlastUsage.ToString ();
        reg.Access ( sLastUsage, ref o );
       
        // Program
        reg.SetSubkey ( "Program" );
        o = Program.eYUnit.ToString ();
        reg.Access ( sYUnit, ref o );
        o = Program.eXUnit.ToString ();
        reg.Access ( sXUnit, ref o );
        o = Program.bServiceNew;
        reg.Access (sServiceNew, ref o);
        o = Program.bSDcardReaderNew;
        reg.Access ( sSDcardReaderNew, ref o );
     
        // Communication
        reg.SetSubkey ( "Communication" );
        //    Ensure, that only non-empty channel strings are stored
        if (Communication.sCommChannel.Length > 0)
        {
          o = Communication.sCommChannel;
          reg.Access ( sChannel, ref o );
        }
        //
        o = Communication.dwBaudRate.ToString ();
        reg.Access ( sBaudRate, ref o );
        
        // Configuration
        reg.SetSubkey ( "Configuration" );
        o = Configuration.sLastFile;
        reg.Access ( sLastFile, ref o );
        
        // Language
        reg.SetSubkey ( "Language" );
        o = Language.eLanguage.ToString ();
        reg.Access ( sLanguage, ref o );
        o = Language.bLangSyn;
        reg.Access ( sLangSyn, ref o );
      
      }
      catch
      {
        Debug.WriteLine ( "AppData._WriteRegistry() failed\n" );
      }
    }
  
    /// <summary>
    /// Writes the application data via a resource file (key - value pairs)
    /// </summary>
    void _WriteResourceFile () 
    {
      ResourceWriter resWriter = null;
      try 
      {
        resWriter = new ResourceWriter ( _sAppDataFilepath );

        // Common 
        resWriter.AddResource ( sScriptFilename, Common.sScriptFileName );
        resWriter.AddResource (sServiceFilename, Common.sServiceFileName);
        resWriter.AddResource (sDataFolder, Common.sDataFolder);
        resWriter.AddResource ( sSDcardFolder, Common.sSDcardFolder );
        resWriter.AddResource ( sEepromDataFile, Common.sEepromDataFile );
        resWriter.AddResource ( sEepromScriptFile, Common.sEepromScriptFile );
        resWriter.AddResource ( sDisplayScanFile, Common.sDisplayScanFile );
        resWriter.AddResource ( sLastUsage, Common.dtlastUsage );
        // Program
        resWriter.AddResource ( sYUnit, Program.eYUnit );
        resWriter.AddResource ( sXUnit, Program.eXUnit );
        resWriter.AddResource (sServiceNew, Program.bServiceNew);
        resWriter.AddResource (sSDcardReaderNew, Program.bSDcardReaderNew);
        // Communication
        resWriter.AddResource ( sChannel, Communication.sCommChannel );
        resWriter.AddResource ( sBaudRate, Communication.dwBaudRate );
        // Configuration
        resWriter.AddResource ( sLastFile, Configuration.sLastFile );
        // Language
        resWriter.AddResource ( sLanguage, Language.eLanguage );
        resWriter.AddResource ( sLangSyn, Language.bLangSyn );

        resWriter.Generate ();
      }
      catch 
      {
        Debug.WriteLine ( "AppData._WriteResourceFile() failed\n" );
      }
      finally 
      {
        if ( null != resWriter ) resWriter.Close();
      }
    }

    #endregion // methods

    #region properties

    /// <summary>
    /// The folder where the application is located
    /// </summary>
    public string AppFolder { get { return Path.GetDirectoryName ( _sAppFilepath ); } }

    #endregion // properties

  }
}
