// If defined, the device is reconnected after a script transfer.
// If not defined, the device is NOT reconnected after a script transfer.
#define PUSH_DEVICE_RECONNECTION

using System;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;
using System.Drawing.Printing;
using System.Collections;

using CommRS232;
using Utilities;

namespace App
{
	/// <summary>
	/// Class CmdUI:
	/// Actions initiated by means of the command ( menu, tool ) bars
	/// </summary>
	public class CmdUI
	{
    #region constructors
    
    /// <summary>
    /// Constructor
    /// </summary>
    internal CmdUI()
		{
    }

    #endregion // constructors

    #region constants and enums 

    /// <summary>
    /// Device reconnection: Interval (in 100 msec) 
    /// </summary>
    const int _WAIT_DEV_RECONNECTION = 10;

    #endregion constants and enums 

    #region members

    /// <summary>
    /// The printer settings used
    /// </summary>
    PrinterSettings _ps = null;
    
    /// <summary>
    /// The current Toolbar visibility state
    /// </summary>
    bool _bToolbarVisible = false;
    
    /// <summary>
    /// Device reconnection: Indicates, whether device reconnection should be started (true) or not (false)
    /// </summary>
    bool _bStartReConnect = false;
    /// <summary>
    /// Device reconnection: (Time) counter
    /// </summary>
    int _nStartReConnect = 0;
    /// <summary>
    /// Device reconnection: Step #
    /// </summary>
    int _nStepReConnect = 0;
    
    #endregion // members

    #region methods

    //---------------------------------------------------------------
    // Menu File
    //---------------------------------------------------------------
    
    /// <summary>
    /// MenuItem "Script editor ...":
    /// Opens the script editor.
    /// </summary>
    public void OnScriptEditor() 
    {
      App app = App.Instance;
      MainForm f = app.MainForm;
      
      // Show the script collection editor form
      ScriptCollEditorForm sef = new ScriptCollEditorForm (); 
      DialogResult dr = sef.ShowDialog ( f );
      sef.Dispose ();
    }

    /// <summary>
    /// MenuItem "Open configuration file (plain text) ...":
    /// Selects and loads a script configuration file (plain text)
    /// </summary>
    public void OnOpenConfigFile_Txt()
    {
      App app = App.Instance;
      Doc doc = app.Doc;

      // Load a script configuration file
      doc.LoadScript(Doc.CryptMode.Decrypted);
      // Init. the documents 'selected script' member
      doc.sCurrentScript = "";
    }

    /// <summary>
    /// MenuItem "Open configuration file ...":
    /// Selects and loads a script configuration file (encrypted)
    /// </summary>
    public void OnOpenConfigFile()
    {
      App app = App.Instance;
      Doc doc = app.Doc;

      // Load a script configuration file
      doc.LoadScript(Doc.CryptMode.Encrypted);
      // Init. the documents 'selected script' member
      doc.sCurrentScript = "";
    }

    /// <summary>
    /// MenuItem "Service data editor ...":
    /// Opens the service data editor
    /// </summary>
    public void OnServiceDataEditor ()
    {
      App app = App.Instance;
      MainForm f = app.MainForm;

      // Show the service data form (editor mode)
      // CD: Service dialog style
      bool bUseNewServiceStyle = app.Data.Program.bServiceNew;
      if (bUseNewServiceStyle)
      {
        // Instantiate a ServiceNew dialog 
        ServieNewForm sfn = new ServieNewForm (true);
        // Show the dialog
        DialogResult drn = sfn.ShowDialog (f);
        sfn.Dispose ();
      }
      else
      {
        // Instantiate a Service dialog 
        ServiceForm sf = new ServiceForm (true);
        // Show the dialog
        DialogResult dr = sf.ShowDialog (f);
        sf.Dispose ();
      }
    }

    /// <summary>
    /// MenuItem "Open service data file ...":
    /// Selects and loads a service data file (encrypted)
    /// </summary>
    public void OnOpenServiceDataFile()
    {
      App app = App.Instance;
      Doc doc = app.Doc;

      // Load a service data file
      doc.LoadServiceData(Doc.CryptMode.Encrypted);
    }
    
    /// <summary>
    /// MenuItem "Print ...":
    /// Prints the document.
    /// </summary>
    public void OnPrint() 
    {
      App app = App.Instance;
      MainForm f = app.MainForm;
      
      // Attach the printer settings to the print document
      if ( null != _ps )
        f.PrintDocument.PrinterSettings = _ps;
      // Set the maximum page that can be selected 
      f.PrintDocument.PrinterSettings.MaximumPage = 1;
      
      // Create a print dialog
      PrintDialog dlg = new PrintDialog();
      // Attach the print document
      dlg.Document = f.PrintDocument;
      
      // Show the print dialog
      DialogResult dr = dlg.ShowDialog();
      dlg.Dispose ();
      if ( dr == DialogResult.OK )
      {
        // Print the document
        f.PrintDocument.Print ();
      }
    }

    /// <summary>
    /// MenuItem "Print preview":
    /// Shows the print preview.
    /// </summary>
    public void OnPrintPreview() 
    {
      ///////////////////////////////////////////////////////////////
      // Print preview by means of a PrintPreviewDialog

//      App app = App.Instance;
//      MainForm f = app.MainForm;
//      
//      if ( null != _ps )
//        f.PrintDocument.PrinterSettings = _ps;
//      f.PrintDocument.PrinterSettings.MaximumPage = 1;
//
//      PrintPreviewDialog dlg = new PrintPreviewDialog();
//      dlg.Document = f.PrintDocument;
//
//      Rectangle r = Screen.GetBounds ( f );
//      r.Width /= 2;
//      dlg.SetDesktopBounds ( r.Left, r.Top, r.Width, r.Height );
//      
//      dlg.ShowDialog ();
//      dlg.Dispose ();

 
      ///////////////////////////////////////////////////////////////
      // Print preview by means of a PrintPreviewControl control
      
      // Proof: Does the main form own a MaximumBox? 
      App app = App.Instance;
      MainForm f = app.MainForm;
      bool bMaximizeBoxPresent = app.Data.MainForm.bMaximizeBoxPresent;
      
      // Toggle print preview
      app.Data.MainForm.bPrintPreview = !app.Data.MainForm.bPrintPreview;
      if ( app.Data.MainForm.bPrintPreview ) 
      {
        // Print preview checked:
        // Show MainForm with print preview:
      
        // Comment input
        CommentForm cf = new CommentForm ();
        cf.ShowDialog (f);
        cf.Dispose ();

        // Maximize Mainform
        // ( Hide MaximizeBox, if it was init'ly present )
        f.WindowState = FormWindowState.Maximized;
        if ( bMaximizeBoxPresent ) f.MaximizeBox = false;

        // Hide Toolbar
        ToolBar tb = f.ToolBar;
        _bToolbarVisible = tb.Visible;
        if ( _bToolbarVisible ) OnViewToolbar();  // hide it, if shown

        // Show print preview ( Attach print document, hide graph panel )
        if ( null != _ps )
          f.PrintDocument.PrinterSettings = _ps;
        f.PrintPreview.Document = f.PrintDocument;
        f.PrintPreview.AutoZoom = true;
        
        f.GraphPanel.Visible = false;
        f.PrintPreview.Visible = true;
      }
      else 
      {
        // Print preview unchecked:
        // Show MainForm with normal appereance:

        // Show Toolbar with previous visibility state
        ToolBar tb = f.ToolBar;
        if ( _bToolbarVisible && !tb.Visible ) OnViewToolbar();  // show it, if previously visible

        // Show mainform in previous size
        // ( Show MaximizeBox, if it was init'ly present )
        f.WindowState = FormWindowState.Normal;
        if ( bMaximizeBoxPresent ) f.MaximizeBox = true;

        // Hide print preview ( Deattach print document, show graph panel )
        f.PrintPreview.Document = null;
        
        f.GraphPanel.Visible = true;
        f.PrintPreview.Visible = false;
      }
    
    }

    /// <summary>
    /// MenuItem "Printer adjustment ...":
    /// Selects the printer.
    /// </summary>
    public void OnPrinterAdjustment() 
    {
      App app = App.Instance;
      MainForm f = app.MainForm;
      
      // Create a print dialog
      PrintDialog dlg = new PrintDialog();
      // Attach the print document
      dlg.Document = f.PrintDocument;
      
      // Show the print dialog
      DialogResult dr = dlg.ShowDialog();
      if ( dr == DialogResult.OK )
      {
        // Remember the printer settings choosen 
        _ps = ( PrinterSettings ) dlg.PrinterSettings.Clone ();
      }
      dlg.Dispose ();
    }

    /// <summary>
    /// MenuItem "Properties ...":
    /// Adjusts the properties.
    /// </summary>
    public void OnProperties() 
    {
      App app = App.Instance;
      MainForm f = app.MainForm;

      // Show the 'Properties' dialog
      PropertiesForm pf = new PropertiesForm ();
      DialogResult dr = pf.ShowDialog ( f );
      pf.Dispose ();
    
      // If we closed the dialog by means of OK ...
      if ( dr == DialogResult.OK ) 
      {
        // Update documents data folder
        Doc doc = app.Doc;
        doc.sDataFolder = app.Data.Common.sDataFolder;

        // Update the resource manager and the language of the MainForm, if req.
        string sLanguage = app.Data.Language.eLanguage.ToString ( "F" );
        Ressources r = app.Ressources;
        if ( sLanguage != r.Culture.TwoLetterISOLanguageName ) 
        {
          // Update the resource manager
          r.UpdateCulture ( sLanguage );
          // Update the language of the MainForm
          f.UpdateCulture ();
        }

        // TX: Language synchronisation, if req.
        // Notes:
        //    - En: 0
        //    - De: 1
        if (app.Data.Language.bLangSyn)
        {
          AppComm  comm = app.Comm;
          if (comm.IsOpen())
          {
            CommMessage msg = new CommMessage (comm.msgLanguageSynchro);
            int nLang;
            if ( app.Data.Language.eLanguage == AppData.LanguageData.Languages.en ) nLang=0;
            else                                                                    nLang=1;
            msg.Parameter = nLang.ToString ();
            msg.WindowInfo = new WndInfo (app.MainForm.Handle, false);
            comm.WriteMessage (msg);
          }
        }

        // Consider a possible change in the axes unit: 
        //    Update view
        doc.UpdateAllViews ();
      }
    }

    /// <summary>
    /// MenuItem "Exit":
    /// Quits the program.
    /// </summary>
    public void OnExit() 
    {
      App app = App.Instance;
      MainForm f = app.MainForm;

      f.Close ();
    }


    //---------------------------------------------------------------
    // Menu View
    //---------------------------------------------------------------
    
    /// <summary>
    /// MenuItem "Toolbar":
    /// Shows/hides the Toolbar.
    /// </summary>
    public void OnViewToolbar() 
    {
      App app = App.Instance;
      MainForm f = app.MainForm;

      // Toggle Toolbars visibility state
      ToolBar tb = f.ToolBar;
      tb.Visible = !tb.Visible;
    }

    /// <summary>
    /// MenuItem "Statusbar":
    /// Shows/hides the Statusbar.
    /// </summary>
    public void OnViewStatusbar() 
    {
      App app = App.Instance;
      MainForm f = app.MainForm;

      // Toggle Statusbars visibility state
      StatusBar sb = f.StatusBar;
      sb.Visible = !sb.Visible;
    }

    /// <summary>
    /// MenuItem "Default":
    /// Shows the main window in default size.
    /// </summary>
    public void OnViewDefault()
    {
      App app = App.Instance;
      MainForm f = app.MainForm;

      // Restore default main window bounds
      if ( f.WindowState == FormWindowState.Maximized ) f.WindowState = FormWindowState.Normal;
      f.DesktopBounds = app.Data.MainForm.rDefaultBounds;
    }
      
    /// <summary>
    /// MenuItem "Statical print preview":
    /// Shows the print preview either statically or dynamically.
    /// </summary>
    public void OnViewStaticalPrintPreview()
    {
      App app = App.Instance;
      app.Data.MainForm.bStaticalPrintPreview = !app.Data.MainForm.bStaticalPrintPreview;
    }    
    
    
    //---------------------------------------------------------------
    // Menu Control
    //---------------------------------------------------------------

    /// <summary>
    /// MenuItem "Connect device":
    /// Connects the IMS device
    /// </summary>
    public void OnConnectDevice() 
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Measure meas = app.Measurement;
      Ressources r = app.Ressources;

      // Check: Communication channel open?
      bool bCommOpen = comm.IsOpen ();
      if (bCommOpen)
      {
        // Yes: Close it
        
        // Stop measurement ( actively )
        if ( doc.bRunning ) 
          meas.StopMeasure( Measure.StopMode.Active );
        
        // Close communication channel
        comm.Close ();
      }
      else
      {
        // No: Open it

        // Open communication channel
        try
        {
          comm.Open ();
        }
        catch
        {
          string sMsg = r.GetString ( "cCmdUI_Err_OpenCommChannel" ); // "Beim �ffnen der Schittstelle ist ein Fehler aufgetreten.\r\n(Benutzen Sie den Eigenschaften-Dialog, um Einstellungen vorzunehmen.)"
          string sCap = r.GetString ( "Form_Common_TextError" );      // "Fehler"
          MessageBox.Show ( sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error );
          return;
        }

        // Init. Exception message file handling
        try
        {
          comm.ExcMsgFile.Init ();
        }
        catch
        {
          // Close communication channel
          comm.Close();
          // Show error msg
          string sMsg = r.GetString ( "cCmdUI_Err_InitExcMsgFile" );  // "Bei der Initialisierung der Kommunikationsprotokollierung ist ein Fehler aufgetreten.\r\n(Schliessen Sie bitte alle eventuell ge�ffneten Protokolldateien.)"
          string sCap = r.GetString ( "Form_Common_TextError" );      // "Fehler"
          MessageBox.Show ( sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error );
          return;
        }

        // Show 'Establish connection' dialog:
        // If this dialog returns OK, the connection was established.
        // If this dialog returns Cancel, the connection was NOT established.
        ConnectForm dlg = new ConnectForm ();
        dlg.ShowWait = true;
        DialogResult dr = dlg.ShowDialog();
        dlg.Dispose ();
        if ( DialogResult.Cancel == dr ) 
        {
          // Connection failed.
          comm.Close();
        }
      }
    }
    
    /// <summary>
    /// MenuItem "Transmit configuration data":
    /// Transmits the contents of a script configuration file into the PID device.
    /// </summary>
    /// <remarks>
    /// A script file may contain a 'Device No.' Top line at top of the file.
    /// If such a line doesn't exist, then the script file is transferred to device.
    /// If such a line exists, and its Device No. corresponds with the Device No. of the device,
    /// then the script file is transferred to device.
    /// If such a line exists, and its Device No. doesn't correspond with the Device No. of the device,
    /// then the script file is NOT transferred to device and a corr'ing error msg is shown.
    /// </remarks>
    public void OnTransmitConfigData() 
    {
      bool bCommOpen;
      
      App app = App.Instance;
      Vw vw = app.View;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // Check: Is there a 'Device No.' Top line present in the script configuration file?
      if (doc.arScript.sDevNo.Length > 0)
      {
        // Yes:
        string sDevNoScript = doc.arScript.sDevNo;  // Device No. from the script file
        string sDevNoDevice = doc.sDevNo;           // Device No. from the device
        // Check: Do the Device No. from the script file and the Device No. from the device correspond?
        if (string.Compare (sDevNoScript, sDevNoDevice, true) != 0)
        {
          // No:
          // Show error msg and prevent the script file from being transferred to device
          string sFmt = r.GetString ( "cCmdUI_Err_DevNoMismatch" );     // "Die Ger�tenummern von Scriptdatei ({0}) und Ger�t ({1}) stimmen nicht �berein.\r\nDie �bertragung wird abgebrochen."
          string sMsg = string.Format (sFmt, sDevNoScript, sDevNoDevice);
          string sCap = r.GetString ( "Form_Common_TextError" );        // "Fehler"
          MessageBox.Show ( sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error );
          return;
        }
      }

      // Show 'Transmit script' dialog
      ScriptTransferForm dlg = new ScriptTransferForm ();
      dlg.ShowDialog();
      dlg.Dispose ();
      
#if PUSH_DEVICE_RECONNECTION      
      // --> The device is reconnected after script transfer
      
      // Notes:
      //  If we went here, a script file has been transferred to device by means of the Script transfer form. 
      //  Unfortunately it could happen in earlier versions (up to 4.0.0), that after a certain comm. error 
      //  combination ocurrence (InBuf/CRC) the RX buffer contained waste information (apparent in a 
      //  disturbance of the symmetry of requests and answers in the PCCommListener files). The reason for
      //  this was that the damaged message was carried out on device a 2nd time without external (PC-sided)
      //  push, what leaded to the fact, that TX and RX of a message on device were processed not 
      //  synchroneously, but shifted (see "Phantom messages", 1.11.13).
      //  In consequence 
      //    1) the lastly transferred command was not served on device,
      //    2) the further communication was negatively affected.
      //  Thats why it was a good idea to reconnect the device at this place in order to maintain
      //  furthermore a clean communication.
      
      // Reconnect device
      // (see remarks for 'PushStartReConnect()')
      _nStartReConnect = 0;         // Reset the counter
      _nStepReConnect = 0;          // Reset the step #
      _bStartReConnect = true;      // Indicate, that device reconnection should be started
      while (_bStartReConnect)      // Perform device reconnection
      {
        Application.DoEvents ();
      }
      // Check for comm. open
      bCommOpen = comm.IsOpen ();
      
#else
      // --> The device is NOT reconnected after script transfer
    
      bCommOpen = true;

#endif

      // Update Script List
      if (bCommOpen)
      {
        // TX: Script List
        CommMessage msg = new CommMessage (comm.msgScriptList);
        msg.WindowInfo = new WndInfo (app.MainForm.Handle, false);
        comm.WriteMessage (msg);
        // Wait until the communication is empty
        comm.WaitForEmpty ();
      }

      // Update result panel
      vw.UpdateResultPanel ();
    }

    /// <summary>
    /// Reconnects the device: Closes and (re)opens the communication channel.
    /// </summary>
    /// <remarks>
    /// 1.
    /// The routine works timer-based:
    /// After '_bStartReConnect' is set to true, the following procedure is started:
    /// 1. The system lets 1 sec lapse away
    /// 2. The comm. is closed
    /// 3. The system lets 1 sec lapse away
    /// 4. The comm. is opened
    /// 5. The system lets 1 sec lapse away
    /// 6. The comm. is checked for idle state
    /// If idle state is reached, the procedure is finished.
    /// 2.
    /// The constant '_WAIT_DEV_RECONNECTION' is fixed here to '10' for the given timer interval (100 ms),
    /// in order to give an absolute interval of 1 sec.
    /// More flexible, one could calculate this value ('soll_value') with respect to the adjusted timer 
    /// interval. The relation is:
    ///   (soll_value) * (timer_interval_in_ms) = (desired_wait_interval_in_sec) * 1000
    /// </remarks>
    public void PushStartReConnect()
    {
      // Perform reconnection?
      if (_bStartReConnect)
      {
        // Yes:
        App app = App.Instance;
        AppComm comm = app.Comm;

        // Incr. the counter
        _nStartReConnect++;     
        // Interval elapsed?
        if (_nStartReConnect == _WAIT_DEV_RECONNECTION)    
        {
          // Yes:
          _nStartReConnect = 0;             // Reset the counter
          _nStepReConnect++;                // Update the step #
          switch (_nStepReConnect)          // Perform step
          {
            case 1:         // 1. step: Close comm.
              comm.Close ();
              Debug.WriteLine ("ReConnection: Close");
              break;

            case 2:         // 2. step: Open comm.
              try
              {
                comm.Open ();
              }
              catch
              {
              }
              Debug.WriteLine ("ReConnection: Open");
              break;
            
            default:        // 3. step: Check for comm. idle state
              if (comm.IsIdle ())
              {
                // Comm. is idle
                _nStepReConnect=0;          // Reset the step #
                _bStartReConnect = false;   // Reset indication
              }
              Debug.WriteLine ("ReConnection: IsIdle?");
              break;
          }
        }
      }
    }

    /// <summary>
    /// MenuItem "Transmit service data":
    /// Transmits the contents of a service data file into the IMS device
    /// </summary>
    public void OnTransmitServiceData()
    {
      // Show 'Transmit service data' dialog
      ServiceDataTransferForm dlg = new ServiceDataTransferForm();
      dlg.ShowDialog();
      dlg.Dispose();
    }
    
    /// <summary>
    /// MenuItem "Reread script names":
    /// Reads the script names from the PID device.
    /// </summary>
    public void OnRereadScriptNames() 
    {
      App app = App.Instance;
      AppComm comm = app.Comm;

      // TX; Script List
      CommMessage msg = new CommMessage (comm.msgScriptList);
      msg.WindowInfo = new WndInfo (app.MainForm.Handle, false);
      comm.WriteMessage (msg);
      
      // Wait until the communication is empty
      comm.WaitForEmpty ();

      // Update result panel
      // TODO: ggf. doppelter Aufruf, da in Reaktion auf Auswertung SCRIPTLIST bereits angesto�en 
      Vw vw = app.View;
      vw.UpdateResultPanel ();
    }

    /// <summary>
    /// MenuItem "Select script ...":
    /// Selects a script.
    /// </summary>
    public void OnSelectScript() 
    {
      // Show 'Script selection' dialog
      ScriptSelectForm dlg = new ScriptSelectForm ();
      dlg.ShowDialog ();
      dlg.Dispose ();
    }

    /// <summary>
    /// MenuItem "Execute script":
    /// Executes a script.
    /// </summary>
    public void OnExecuteScript() 
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      Measure meas = app.Measurement;
      MainForm f = app.MainForm;
 
      if ( doc.bRunning )
      {
        // Stop measurement ( actively )
        meas.StopMeasure ( Measure.StopMode.Active );
      }
      else
      {
        // Start measurement ( directly from PC )
        meas.StartMeasure (Measure.StartMode.FromPC);
        // We go online: Hide 'Offline' information
        f.lblOffline.Visible = false;
      }
    }

    /// <summary>
    /// MenuItem "Confirm alarm":
    /// Confirms a substance alarm.
    /// </summary>
    public void OnAlarmAcknowledge () 
    {
      App app = App.Instance;

      // Hide Alarm button
      app.MainForm.btnAlarm.Visible = false;
    }

    /// <summary>
    /// MenuItem "Trigger KEY command":
    /// Triggers the KEY command.
    /// </summary>
    public void OnTriggerKeyCmd() 
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      Measure meas = app.Measurement;

      if ( doc.bRunning )
      {
        // Trigger the KEY command
        meas.TriggerKeyCmd ();
      }
    }


    //---------------------------------------------------------------
    // Menu Data recording
    //---------------------------------------------------------------

    /// <summary>
    /// MenuItem "Current scan":
    /// Records the current scan.
    /// </summary>
    public void OnRecCurrentScan() 
    {
      App app = App.Instance;
      Doc doc = app.Doc;

      Debug.Assert ( doc.bRunning );
      doc.bRecordSingleScan = true;
    }

    /// <summary>
    /// MenuItem "Scans continuously":
    /// Records spectra continuously.
    /// </summary>
    public void OnRecScansContinuously() 
    {
      App app = App.Instance;
      Doc doc = app.Doc;

      Debug.Assert ( doc.bRunning );
  
      // Toggle recording state
      doc.bRecordScans = !doc.bRecordScans;
    }

    /// <summary>
    /// MenuItem "Results continuously":
    /// Records measuring results continuously.
    /// </summary>
    public void OnRecResultsContinuously() 
    {
      App app = App.Instance;
      Doc doc = app.Doc;

      Debug.Assert ( doc.bRunning );
  
      // Toggle recording state
      doc.bRecordResults = !doc.bRecordResults;
    }
      
    /// <summary>
    /// MenuItem / Button "Displayed grafic":
    /// Records the displayed grafic.
    /// </summary>
    public void OnRecGrafic() 
    {
      App app = App.Instance;
      Doc doc = app.Doc;

      ScanData scn = doc.arScanData[0];           // Get the displayed scan, if any
      if (scn.Count > 0)
        doc.RecordSingleScanWithMessage (scn);    // Record it
    }

    //---------------------------------------------------------------
    // Menu Tools
    //---------------------------------------------------------------
  
    /// <summary>
    /// MenuItem "Read stored Eeprom data ...":
    /// Provides the GSMReader dialog.
    /// </summary>
    public void OnReadEepromStore () 
    {
      App app = App.Instance;
      // Instantiate a GSMReader dialog 
      GSMReaderForm gf = new GSMReaderForm ();
      gf.ShowWait = true;
      // Show the dialog
      gf.ShowDialog ();
      gf.Dispose ();
    }
    
    /// <summary>
    /// MenuItem "Read stored SDcard data ...":
    /// Provides the SDcardReader dialog.
    /// </summary>
    public void OnReadSDcardStore () 
    {
      App app = App.Instance;
      // CD: SDcardReader dialog style
      bool bUseNewSDcardReaderStyle = app.Data.Program.bSDcardReaderNew;
      if ( bUseNewSDcardReaderStyle )
      {
        // Instantiate a SDcardReaderNew dialog 
        SDcardReaderNewForm sfn = new SDcardReaderNewForm ();
        sfn.ShowWait = true;
        // Show the dialog
        sfn.ShowDialog ();
        sfn.Dispose ();
      }
      else
      {
        // Instantiate a SDcardReader dialog 
        SDcardReaderForm sf = new SDcardReaderForm ();
        sf.ShowWait = true;
        // Show the dialog
        sf.ShowDialog ();
        sf.Dispose ();
      }
    }

    /// <summary>
    /// MenuItem "Transfer service data ...":
    /// Provides the Service dialog
    /// </summary>
    public void OnTransferService ()
    {
      App app = App.Instance;

      // Instantiate a Service dialog (transfer mode)
      // CD: Service dialog style
      bool bUseNewServiceStyle = app.Data.Program.bServiceNew;
      if (bUseNewServiceStyle)
      {
        // Instantiate a ServiceNew dialog 
        ServieNewForm sfn = new ServieNewForm (false);
        sfn.ShowWait = true;
        // Show the dialog
        sfn.ShowDialog ();
        sfn.Dispose ();
      }
      else
      {
        // Instantiate a Service dialog 
        ServiceForm sf = new ServiceForm (false);
        sf.ShowWait = true;
        // Show the dialog
        sf.ShowDialog ();
        sf.Dispose ();
      }

      // Trigger device no. update in  main screen
      MainForm f = app.MainForm;
      AppComm comm = app.Comm;
      if (comm.IsOpen ())
      {
        // TX: Read service data (Device section)
        CommMessage msg = new CommMessage (comm.msgServiceReadData);
        msg.Parameter = "0";
        msg.WindowInfo = new WndInfo (f.Handle, false);
        comm.WriteMessage (msg);
      }
    }

    /// <summary>
    /// MenuItem "Transfer MP data ...":
    /// Provides the MP dialog
    /// </summary>
    public void OnTransferMP () 
    {
      App app = App.Instance;
      // Instantiate a MP dialog 
      ServiceMPForm sf = new ServiceMPForm ();
      sf.ShowWait = true;
      // Show the dialog
      sf.ShowDialog ();
      sf.Dispose ();
    }
    
    /// <summary>
    /// MenuItem "Transfer clock-timed script data ...":
    /// Provides the CTS dialog
    /// </summary>
    public void OnTransferCTS () 
    {
      App app = App.Instance;
      // Instantiate a Service dialog 
      CTSForm cf = new CTSForm ();
      cf.ShowWait = true;
      // Show the dialog
      cf.ShowDialog ();
      cf.Dispose ();
    }
    
    /// <summary>
    /// MenuItem "Diagnosis":
    /// Performs diagnosis tasks.
    /// </summary>
    public void OnDiag () 
    {
      App app = App.Instance;
      Doc doc = app.Doc;

      Debug.Assert ( doc.bRunning );
  
      // Check, whether the Diagnosis is enabled or not
      if ( !doc.Diag.Enabled )
      {
        // Diagnosis is disabled: Initiate it
        doc.Diag.Initiate ();
      }
      else
      {
        // Diagnosis is enabled: Terminate it
        doc.Diag.Terminate ();
      }
    }

    /// <summary>
    /// MenuItem "PC communication listener":
    /// Logs the PC communication to file.
    /// </summary>
    public void OnPCcommListener () 
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
     
      // Check whether PC comm logging is enabled or not
      if (!comm.ExcMsgFile.Enabled)
      {
        // PC comm logging is disabled:
        // Enable it
        comm.ExcMsgFile.Enabled = true;
      }
      else
      {
        // PC comm logging is enabled:
        // Disable it
        comm.ExcMsgFile.Enabled = false;
      }
    }

    /// <summary>
    /// MenuItem "Load scan from HD":
    /// Load a scan from HD and updates the view corr'ly.
    /// </summary>
    public void OnLoadScanFromHD ()
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      MainForm f = app.MainForm;
      
      if (doc.GetScanFile ())
      {
        doc.FillDataStructures ();
        doc.UpdateAllViews ();
        // We went offline: Show 'Offline' information
        f.lblOffline.Visible = true;
      }
    }

    //---------------------------------------------------------------
    // Menu ?
    //---------------------------------------------------------------
    
    /// <summary>
    /// MenuItem "About":
    /// Shows the product information.
    /// </summary>
    public void OnAbout() 
    {
      App app = App.Instance;
      MainForm f = app.MainForm;

      AboutForm af = new AboutForm (); 
      af.ShowDialog ( f );
      af.Dispose ();
    }

    /// <summary>
    /// MenuItem "Test":
    /// Provides testing stuff (For testing purposes only - only in DEBUG mode).
    /// </summary>
    public void OnTest() 
    {

      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Vw vw = app.View;
      MainForm f = app.MainForm;
      Ressources r = app.Ressources;

      //-----------------------------------------
      //  DateTime - msec: _3 sollte gleich sein bei aufeinanderfolgenden Aufrufen: ok

      //int dw100sec_1 = (Environment.TickCount % 1000) / 10;
      //int dw100sec_2 = DateTime.Now.Millisecond / 10;
      //int dw100sec_3 = dw100sec_2 - dw100sec_1;

      //-----------------------------------------
      // SetCmds
      CommMessage msg = new CommMessage (app.Comm.msgScriptOpen);
      bool b = msg.IsSetCmd ();
      msg = new CommMessage (app.Comm.msgServiceReadData);
      b = msg.IsSetCmd ();

      //-----------------------------------------
      //  Data transfer - decoding (EHREAD/-WRITE)

//      byte[] arby = new byte[] { 1, 100, 2, 50 };
//      for (int i=0; i < arby.Length; i++)
//        arby[i] |= 0x80;
//      string s = ServiceExtForm._EHStringFromBytearray (arby);

      //-----------------------------------------
      // Exceptions
      // -> ok, 8.3.10

//      try 
//      {
//        int i=5;
//        if (i==5)
//          throw new ScrCmdTimeNotAscendingException ();
//      }
//      catch (System.Exception exc)
//      {
//        MessageBox.Show (exc.Message);
//        return;
//      }
      

      //-----------------------------------------
      // Rounding
      // -> ok, 8.3.10

//      float ymin= -10f;
//      float ymax= 10f;
//      float delta;
//
////      ymin *= 100;
////      ymin = (float) Math.Floor (ymin);
////      ymin /= 100;
////      ymax *= 100;
////      ymax = (float) Math.Ceiling (ymax);
////      ymax /= 100;     
//      delta = ((ymax-ymin)/100)*10;
//      ymin -= delta; ymin *= 100; ymin = (float)Math.Floor (ymin); ymin /= 100;
//      ymax += delta; ymax *= 100; ymax = (float)Math.Ceiling (ymax); ymax /= 100;
//      if (ymin == ymax)
//        ymax = ymin + 0.01f;


      //-----------------------------------------
      // Plot - TEST
      // -> ok, 8.3.10
      
//      ScanData scn = new ScanData ();
//      Random rnd = new Random ();
//      for (int i=0; i < 2048 ;i++) 
//      {
//        float fY = (float) (rnd.NextDouble() * 100);
//        scn.Add (fY);
//      }
//      doc.arScanData[0]= scn;
//
//      float xmin = 0;                                 // Left abscissa margin
//      float xmax = (float)(Doc.NSCANDATA - 1);        // Right abscissa margin
//      vw.GraphData.SetDefaultScale (xmin, xmax, -10.0f, 110.0f);
//      vw.GraphData.FitScaleToDefaultData ();
//
//      vw.UpdateGraph ();


      //-----------------------------------------
      // Load-/SaveScript - TEST 1
      // -> ok, 8.3.10

//      // Load: Script file -> Config
//      doc.LoadScript ();
//      
//      // new filepath for scriptfile to be saved
//      string path = doc.sScriptFilename;
//      string path_old = path;
//            
//      string filename = System.IO.Path.GetFileNameWithoutExtension ( path );
//      string ext = System.IO.Path.GetExtension ( path );
//      string dir = System.IO.Path.GetDirectoryName ( path );
//            
//      filename += "_Test";
//      filename += ext;
//      
//      path = System.IO.Path.Combine ( dir, filename );
//      doc.sScriptFilename = path;
//      
//      // Save: Config -> script file 
//      doc.SaveScript ();
//      
//      // Restore script file path
//      doc.sScriptFilename = path_old;
    

      //-----------------------------------------
      // Load-/SaveScript - TEST 2
      // -> ok, 8.3.10
    
//      string sFilename = @"D:\Eigene Dateien\MSVS2003Ent\WindowsApps\PID_IUT\Testscripts\PID_V2_0__PIDControl_V2_0\Test_0.gsm";
//      ScriptCollection sf = new ScriptCollection ();
//      try 
//      {
//        sf.Load (sFilename);
//      }
//      catch (System.Exception exc)
//      {
//        Debug.WriteLine (exc.Message); 
//      }
//
//      string sFilenameNew = @"D:\Eigene Dateien\MSVS2003Ent\WindowsApps\PID_IUT\Testscripts\PID_V2_0__PIDControl_V2_0\Test_0_1.gsm";
//      try 
//      {
//        sf.Write (sFilenameNew);
//      }
//      catch (System.Exception exc)
//      {
//        Debug.WriteLine (exc.Message); 
//      }


      //-----------------------------------------
      // UI-/PrintPreview test via RS232 emulation
      // == 'Test' stuff ==
      // -> ok, 8.3.10

//      // Enable (Ensure that 'Test1' stuff was disabled)
//      Test.Enabled = true;
//      Test1.Enabled = false;
//      //      // Set the Test timer interval: 3 sec
//      f._TimerTest.Interval = 3000;
//      // Init. the test
//      Test.Init ();
//      // Dis/Enable the test
//      bool bTestEnable = f._TimerTest.Enabled;
//      bTestEnable = !bTestEnable;
//      if (bTestEnable)
//      {
//        // Test enabled:
//        // Check: Test mode
//        if (Test.eTestMode == Test.TestMode.Real)
//        {
//          // Testmode: Real: Get scan file path
//          bool b = Test.GetScanFile ();
//          if (!b)
//          {
//            MessageBox.Show ("Test execution canceled.");
//            bTestEnable=false;
//          }
//          else
//          {
//            Test.SetParams ();
//          }
//        }
//      }
//      f._miHelp_Test.Checked = bTestEnable;   // Check/Uncheck the 'Test' menuitem corr'ly
//      f._TimerTest.Enabled = bTestEnable;     // En-/Disable the test timer corr'ly
//      if (!bTestEnable)
//      {
//        MessageBox.Show ("Test execution stopped.");
//      }


      //-----------------------------------------
      // UI-/PrintPreview test via RS232 emulation
      // == 'Test1' stuff ==

//      // Enable (Ensure that 'Test' stuff was disabled)
//      Test1.Enabled = true;
//      Test.Enabled = false;
//      // Set the Test timer interval: 3 sec
//      f._TimerTest.Interval = 3000;
//      // Init. the test
//      Test1.Init ();
//      // Dis/Enable the test
//      bool bTestEnable = f._TimerTest.Enabled;
//      bTestEnable = !bTestEnable;
//      if (bTestEnable)
//      {
//        // Test enabled:
//        // Check: Test mode
//        if (Test1.eTestMode == Test1.TestMode.Real)
//        {
//          // Testmode: Real: Get scan file path
//          bool b = Test1.GetScanFile ();
//          if (!b)
//          {
//            MessageBox.Show ("Test1 execution canceled.");
//            bTestEnable=false;
//          }
//          else
//          {
//            Test1.SetParams ();
//          }
//        }
//      }
//      f._miHelp_Test.Checked = bTestEnable;   // Check/Uncheck the 'Test' menuitem corr'ly
//      f._TimerTest.Enabled = bTestEnable;     // En-/Disable the test timer corr'ly
//      if (!bTestEnable)
//      {
//        MessageBox.Show ("Test execution stopped.");
//      }

      
      //-----------------------------------------
      // Test-scan storage to HD (fix path)
      // (OUTSIDE & INDEPENDENT FROM PID SW!)
      // -> ok, 8.3.10

//      System.IO.StreamWriter file = null;
//      try
//      {
//        System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;  
//        System.Globalization.NumberFormatInfo lfi = System.Globalization.NumberFormatInfo.CurrentInfo;
//
//        // Create the file
//        string sPath = @"D:\Temp\test_scan_100115.txt";
//        file = System.IO.File.CreateText ( sPath );
//        double[] ard = new double[2048];
//        string s;
//        float f1;
//        // Overtake the test array
//        for ( int i=0; i < ard.Length ;i++ )
//        {
//          try 
//          {
//            ard[i] = Test.g_adcData2[i]/2;
//          }
//          catch
//          {
//            ard[i] = 0.0;
//          }
//          f1=i+1;
//          s = string.Format ("{0}\t{1}", f1.ToString("F3"), ard[i].ToString ("F3"));
//          file.WriteLine (s);
//        }
//      }
//      catch
//      {
//      }
//      finally 
//      {
//        // Close the file
//        if ( null != file ) 
//          file.Close ();
//      }


      //-----------------------------------------
      // Encoding - 1
      // -> ok, 8.3.10
      
//      byte[] arbyOrig = new byte[] { 74, 77, 128, 135, 135, 243, 135, 243, 135, 246, 135, 254, 135, 255 };
//      
//      System.Text.ASCIIEncoding ae = new System.Text.ASCIIEncoding ();
//      int nACoded = ae.GetCharCount (arbyOrig);                   // 14
//      string sACoded = new string ( ae.GetChars ( arbyOrig ) );   // "JM????????????"
//      char[] carACoded = sACoded.ToCharArray ();                  // J,M,?,...,?
//      byte[] arbyACoded = ae.GetBytes ( sACoded );                // 74,77,63,...,63
//
//      System.Text.UTF8Encoding u8e = new System.Text.UTF8Encoding ();
//      int nU8Coded = u8e.GetCharCount (arbyOrig);                 // 2
//      string sU8Coded = new string ( u8e.GetChars ( arbyOrig ) ); // "JM"
//      char[] carU8Coded = sU8Coded.ToCharArray ();                // J,M
//      byte[] arbyU8Coded = u8e.GetBytes ( sU8Coded );             // 74,77
//
//      System.Text.UTF7Encoding u7e = new System.Text.UTF7Encoding ();
//      int nU7Coded = u7e.GetCharCount (arbyOrig);                 // 14
//      string sU7Coded = new string ( u7e.GetChars ( arbyOrig ) ); // "JM(Eulen..)"
//      char[] carU7Coded = sU7Coded.ToCharArray ();                // J,M,(Eule),...    
//      byte[] arbyU7Coded = u7e.GetBytes ( sU7Coded );             // 74,77,43,65,...
//
//      System.Text.UnicodeEncoding ue = new System.Text.UnicodeEncoding ();
//      int nUCoded = ue.GetCharCount (arbyOrig);                   // 7
//      string sUCoded = new string ( ue.GetChars ( arbyOrig ) );   // "(Eulen)"          (Length 7)
//      char[] carUCoded = sUCoded.ToCharArray ();                  // (Eule)=19786,...   (Length 7)
//      byte[] arbyUCoded = ue.GetBytes ( sUCoded );                // 74,77,128,...
      

      //-----------------------------------------
      // CommChannelWait Form
      // -> ok, 8.3.10

//      CommMessage msg = new CommMessage ();
//      msg.WindowInfo = new WndInfo (f.Handle, false);
//      Form fOwner = msg.GetOwnerForm ();
//
//      CommChannelWaitForm dlg = new CommChannelWaitForm (comm);
//      dlg.ShowDialog (fOwner);  // Use this method in order to secure the centering of the dialog above its owner 
//      dlg.Dispose ();

      

      //-----------------------------------------
      // Misc - TEST
      // -> ok, 8.3.10

//    {
//      string s = string.Format ( "{0}.{1:D2}:{2:D2}:{3:D2}", 1,2,3,4 );
//      MessageBox.Show ( s );
//      string s1 = string.Format ( "{0}.{1:D2}:{2:D2}:{3:D2}", 100,22,37,44 );
//      MessageBox.Show ( s1 );
//      string s2 = string.Format ( "{0}d{1:D2}h{2:D2}'{3:D2}''", 100,22,37,44 );
//      MessageBox.Show ( s2 );
//      string s3 = string.Format ( "{0}d {1:D2}h {2:D2}' {3:D2}''", 100,22,37,44 );
//      MessageBox.Show ( s3 ); // --> Best variant!!!
//    }
//    {
//      string s4 = Common.UInt32ToTimeString (1234);
//      MessageBox.Show ( s4 );
//      s4 = Common.UInt32ToTimeString (12345);
//      MessageBox.Show ( s4 );
//      s4 = Common.UInt32ToTimeString (123456);
//      MessageBox.Show ( s4 );
//    }
//    {
//      byte[] byBuffer = new byte[] { 86, 69, 82, 32, 48, 48, 51, 46, 50, 48, 57, 13 };
//      System.Text.ASCIIEncoding ae = new System.Text.ASCIIEncoding ();
//      // OK
//      string sRead = new string ( ae.GetChars( byBuffer ) );
//      // OK
//      string sRead1 = new string ( ae.GetChars ( byBuffer, 0, byBuffer.Length-1 ) );
//      int i=5; if (i==0) {}
//    }
//    {
//      string sTemp = "46.600";
//      // Error: 46600
//      double dTemperature = double.Parse ( sTemp );
//      // OK: 46.6
//      dTemperature = double.Parse ( sTemp, System.Globalization.NumberFormatInfo.InvariantInfo );
//      int i=5; if (i==0) {}
//    }
//    {
//      double d1 = 4.7, d2 = 0.25, d3 = 4;
//      // Error: "4,7,0,25,4"
//      string sdoub1 = string.Format ( "{0},{1},{2}", d1, d2, d3 );
//      // OK: "4.7,0.25,4"
//      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;  
//      string sdoub2 = string.Format ( "{0},{1},{2}", d1.ToString(nfi), d2.ToString(nfi), d3.ToString(nfi) );
//      int i=5; if (i==0) {}
//    }
//    {
//      DateTime dtref = DateTime.Now;
//      // Error: "01.01.0001 00:02:22"
//      uint nsec = (uint) (dtref.Ticks / 10000000);
//      DateTime dtu = new DateTime ( nsec * 10000000 );
//      string sTimeu = string.Format ( "{0}", dtu.ToString ( "G" ) );
//      // OK: "12.01.2010 14:40:34"
//      long lsec = dtref.Ticks / 10000000;
//      DateTime dtl = new DateTime ( lsec * 10000000 );
//      string sTimel = string.Format ( "{0}", dtl.ToString ( "G" ) );
//      int i=5; if (i==0) {}
//    }
//    {
//      string sMsg = string.Format ( "AAAAAAAAAAAAAAAA\nBBBBBBBBBBBB\r\nCCCCCCCCCCCCCC" );
//      MessageBox.Show ( sMsg );
//    }
//    {
//      // MessageBox MIT string aus .resx-Datei
//      string sMsg1 = r.GetString ( "ScriptSelect_txtIntro" );
//      MessageBox.Show ( sMsg1 );
//      
//      string fmt = r.GetString ( "cDoc_Record_Result_Success" );
//      string sx = string.Format ( fmt, "<Dateipfad>" );
//      MessageBox.Show ( sx );
//
//      // Textfeld MIT string aus .resx-Datei
//      ScriptSelectForm ssf = new ScriptSelectForm ();
//      ssf.ShowDialog ();
//      ssf.Dispose ();
//    }
//    {
//      System.Drawing.Text.InstalledFontCollection ifc = new System.Drawing.Text.InstalledFontCollection ();
//      FontFamily[] arff = ifc.Families;
//      ArrayList al = new ArrayList ();
//      foreach ( FontFamily ff in arff ) 
//      {
//        al.Add ( ff.Name );
//      }
//      int i=5; if (i==0) {}
//    }


      //-----------------------------------------
      // Recording - TESTs
      // -> ok, 8.3.10

//      // RecordSingleScan
//      int[] ari = new int[2048];
//      Test.FillTestArray_Sim (Doc.ADC_RESOLUTION, false, ref ari);
//      ScanData sd = new ScanData ();
//      for (int i=0; i < 2048; i++) 
//      {
//        sd.Add ( ari[i] );
//      }
//      doc.RecordSingleScan ( sd );
//    
//      // RecordScan
//      ScanData sd1 = new ScanData ();
//      for (int i=0; i < 2048; i++) 
//      {
//        sd1.Add ( ari[i] );
//      }
//      doc.RecordScan ( sd1 );
//    
//      // RecordResult
//      ScanResultArray arsr = doc.arScanResult;
//      Random rnd = new Random ();
//      for (int no=0; no < Doc.MAX_SCAN_RESULT; no++) 
//      {
//        ScanResult sr = new ScanResult ();
//        
//        sr.sSubstance = "Substance" + no.ToString();
//        sr.dResult = 10.0 + rnd.NextDouble() * 90;
//        sr.dwBeginTime = (uint) no;
//        sr.dwWidthTime = (uint) (no + 100);
//        sr.dA2 = 100;
//        sr.sConcUnit = ConcUnits.ppm;
//
//        arsr.Add ( sr );
//      }
//      doc.RecordResult ();
//      arsr.Clear ();
    

      //-----------------------------------------
      // Script Event - TEST: Building TX string with >= 1 parameters
      // Note: The TX string should be: "SCREVENT 50,TEMPQUITALL,60,80,100\0\r"
      // -> ok, 8.3.10
      
//      ScriptEvent se = new ScriptEvent ();
//      se.dwTime = 50;
//      se.szCommand = "TEMPQUITALL";
//      se.wParNumber=3;
//      se.ardwParameter[0]=60;
//      se.ardwParameter[1]=80;
//      se.ardwParameter[2]=100;
//      
//      CommMessage msg = new CommMessage (comm.msgScriptEvent);
//      msg.Parameter = se.ToString ();
//
//      string msgwritten=msg.Command + " " + msg.Parameter + "\0\r";
//      MessageBox.Show (msgwritten);
    
    
      //-----------------------------------------
      // Registry
      // (Test it no longer, because we know, that it works.) 

//      // 1.
//      AppRegistry ar = new AppRegistry ( 1 );
//      // 2.
//      AppRegistry.Test ();


      //-----------------------------------------
      // Crypt
      // -> ok, 8.3.10

//      byte[] arby       = new byte[] { 0x52, 0x32, 0x44, 0x32 };    // "R2D2"
//      
//      byte[] arbyEncrypted = Crypt.JMEncrypt ( arby );
//      byte[] arbyDecrypted = Crypt.JMDecrypt ( arbyEncrypted );
//      
//      arbyEncrypted = Crypt.JMEncrypt ( arbyDecrypted );
//      arbyDecrypted = Crypt.JMDecrypt ( arbyEncrypted );


      //-----------------------------------------
      // TransferStart - TEST: Enum-to-string translation
      // -> ok, 8.3.10
      
//      CommMessage msg = new CommMessage ( comm.msgTransferStart );
//      msg.Parameter = TransferTask.SDcard_Data.ToString ( "D" );
    

      //-----------------------------------------
      // ServiceForm

//      // 1. (Execute only, if the righr device PN is connected via RS232!)
//      OnTransferService ();

      // 2. Provoke index error
      // -> ok, 8.3.10
//      string[] _arsServiceData = new string[4];  
//      try 
//      {
//        _arsServiceData[0] = "1";
//        _arsServiceData[1] = "1";
//        _arsServiceData[2] = "1";
//        _arsServiceData[3] = "1";
//        _arsServiceData[4] = "1";
//      }
//      catch ( Exception exc )
//      {
//        MessageBox.Show (exc.Message);
//      }

      //-----------------------------------------
      // Levenberg-Marquardt method

//      //  y (base line corrected): simulated spectrum
//      cFakeSpec fs = new cFakeSpec ();
//      int[] y = new int[2048]; 
//      fs.jm_simspectrum (y, y.Length);      // simulated spectrum (0-offset)
//      float ave = 0;
//      for (int i=0; i < 128; i++) ave += y[i];
//      ave /= 128;
//      for (int i=0; i < y.Length; i++) y[i] -= (int) ave;
//
//      // geg.: Peak pos., search delimiters
//      int pos = 597;                        // simulated spectrum: peak pos.
//      int llim = 0;
//      int rlim = 2047;
//      // ges.: 
//      int il2, ir2;
//      string sErr;
//
//      SpecEval se = new SpecEval ();
//      // Gaussian fit by means of the Levenberg-Marquardt method (NRU)
//      se.ml_nru (y, 2048, pos, llim, rlim, out il2, out ir2, out sErr);
//      // Gaussian fit by means of the Levenberg-Marquardt method (SpevRout)
//      se.ml_sr (y, 2048, pos, llim, rlim, out il2, out ir2);
    
    }

    #endregion // methods

  }	
	
}
