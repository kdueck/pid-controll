using System;
using System.Diagnostics;
using System.Windows.Forms;

using CommRS232;

namespace App
{
  /// <summary>
	/// Class Measure:
	/// Measurement
	/// </summary>
	public class Measure
	{
    #region constructors
    
    /// <summary>
    /// Constructor
    /// </summary>
    internal Measure()
		{
		}
  
    #endregion // constructors

    #region constants and enums 

    /// <summary>
    /// Measurement Stop management: StopMode enumeration
    /// </summary>
    public enum StopMode
    {
      /// <summary>
      /// GSM stops a measurement on the device actively, i.e. by TX'ing a script stop cmd
      /// and updating the view corr'ly
      /// </summary>
      Active,
      /// <summary>
      /// GSM reacts on the measurement stop on the device passively, i.e. by
      /// updating the view corr'ly
      /// </summary>
      Passive,
    }

    /// <summary>
    /// Measurement Start management: StartMode enumeration
    /// </summary>
    public enum StartMode
    {
      /// <summary>
      /// The measurement should be started directly from PC
      /// </summary>
      FromPC,
      /// <summary>
      /// The measurement should be "started" in reaction of a script change on device
      /// </summary>
      AfterScriptChangeOnDevice,
    }
    
    /// <summary>
    /// Push cmd's that should be permanenty executed: Interval (in 100 ms) 
    /// </summary>
    const int _WAIT_ONCOMMEMPTY_PID = 10;
    
    #endregion constants and enums 

    #region members

    /// <summary>
    /// Indicates, whether a measurement should be stopped passively (True) or not (False)
    /// </summary>
    public bool StopPassively = false;

    /// <summary>
    /// Push cmd's that should be permanenty executed: Indication 
    /// </summary>
    public bool bOnCommEmpty = false;
    /// <summary>
    /// Push cmd's that should be permanenty executed: Counter 
    /// </summary>
    int nOnCommEmpty = 0;
    
    /// <summary>
    /// Indicates, whether a script change has taken place on device (True) or not (False)
    /// </summary>
    public bool MeasureChanged = false;

    #endregion members

    #region methods
 
    /// <summary>
    /// Starts a measurement.
    /// </summary>
    /// <param name="startMode">Indicates, whether the measurement should be started directly from PC
    /// or in reaction of a script change on device</param>
    public void StartMeasure (StartMode startMode)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Vw vw = app.View;

      CommMessage msg;

      // CD: Start mode
      if ( startMode == StartMode.FromPC )
      {
        // Directly from PC:
        
        Debug.Assert ( comm.IsOpen() );
        Debug.Assert ( !doc.bRunning );

        // Select a script
        if ( doc.sCurrentScript.Length == 0 )
        {
          ScriptSelectForm dlg = new ScriptSelectForm ();
          dlg.ShowDialog ();
          dlg.Dispose ();
          if ( doc.sCurrentScript.Length == 0 )
            return;
        }
    
        // Clear the result panel
        vw.ClearResultPanel ();

        // TX: Script Select
        msg = new CommMessage ( comm.msgScriptSelect );
        msg.Parameter = doc.ScriptNameToIdx ( doc.sCurrentScript ).ToString ();
        msg.WindowInfo = new WndInfo (app.MainForm.Handle, false);
        comm.WriteMessage ( msg );
        // TX: Script Reset
        msg = new CommMessage (comm.msgScriptReset);
        msg.WindowInfo = new WndInfo (app.MainForm.Handle, false);
        comm.WriteMessage (msg);
        // TX: Script Start
        msg = new CommMessage (comm.msgScriptStart);
        msg.WindowInfo = new WndInfo (app.MainForm.Handle, false);
        comm.WriteMessage (msg);
      
        // Wait until the communication is empty
        comm.WaitForEmpty ();
      }
      
      // TX: Scan Parameter
      msg = new CommMessage (comm.msgScanParams);
      msg.WindowInfo = new WndInfo (app.MainForm.Handle, false);
      comm.WriteMessage (msg);
 
      // Push the commands, that should be permanenty executed 
      // (Initially)
      //OnCommEmpty ();
      bOnCommEmpty = true;    // Indicate, that these commands should be executed
      nOnCommEmpty = 0;       // Init. the counter

      // Update documents 'bRunning' member: Script is running
      doc.bRunning    = true;
  
      // Reset the documents scan data
      doc.ResetScanData ();

      // Indicate: No alarm present
      doc.bResultAlarm = false;

      // Consider the fact, that after script change on device already one DATA transfer has been performed
      // (Above in routine 'ResetScanData' the # of incoming DATA responses has been reset. That means, 
      // we have to increment retroactivaly this #.) 
      if ( startMode == StartMode.AfterScriptChangeOnDevice )
        doc.dwScanCount++;
    }
    
    /// <summary>
    /// Stops a measurement
    /// </summary>
    /// <param name="stopMode">Indicates, whether the measurement should be stopped actively or passively</param>
    public void StopMeasure ( StopMode stopMode )
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;

      Debug.Assert ( comm.IsOpen() );
      Debug.Assert ( doc.bRunning );
      
      // Check: Should the measurement be stopped actively?
      if ( stopMode == StopMode.Active )
      {
        // Yes:
        // TX: Script Stop
        CommMessage msg = new CommMessage (comm.msgScriptStop);
        msg.WindowInfo = new WndInfo (app.MainForm.Handle, false);
        comm.WriteMessage (msg);
      }

      // Update documents 'bRunning' member: No script is running
      doc.bRunning    = false;

      // Wait until the communication is empty
      comm.WaitForEmpty ();
      
      // Terminate the Diagnosis, if req.
      doc.Diag.Terminate ();

      // Update documents 'bRecord ...' members: Disable recording
      doc.bRecordSingleScan = false;
      doc.bRecordScans = false;
      doc.bRecordResults = false;
    }
    
    /// <summary>
    /// Triggers the KEY command
    /// </summary>
    public void TriggerKeyCmd() 
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;

      Debug.Assert ( comm.IsOpen() );
      Debug.Assert ( doc.bRunning );

      // TX: Trigger KEY cmd
      CommMessage msg = new CommMessage (comm.msgTriggerKeyCmd);
      msg.WindowInfo = new WndInfo (app.MainForm.Handle, false);
      comm.WriteMessage (msg);
    }
 
    /// <summary>
    /// Pushes the commands, that should be permanenty executed.
    /// </summary>
    public void OnCommEmpty ()
    {
      App app = App.Instance;
      AppComm comm = app.Comm;

      // The one & only permanent called cmd
      // TX: ScanAllData
      CommMessage msg = new CommMessage (comm.msgScanAllData);
      msg.WindowInfo = new WndInfo (app.MainForm.Handle, false);
      msg.PermanentyCalled = true;
      comm.WriteMessage (msg);
    }

    /// <summary>
    /// Pushes the commands, that should be permanenty executed (i.e. the 'OnCommEmpty()' routine).
    /// </summary>
    /// <remarks>
    /// The constant '_WAIT_ONCOMMEMPTY_PID' is fixed here to '10' for the given timer interval (100 ms),
    /// in order to give an absolute interval of 1 sec.
    /// More flexible, one could calculate this value ('soll_value') with respect to the adjusted timer 
    /// interval. The relation is:
    ///   (soll_value) * (timer_interval_in_ms) = (desired_wait_interval_in_sec) * 1000
    /// </remarks>
    public void PushOnCommEmpty()
    {
      // Comm. empty?
      if (bOnCommEmpty)
      {
        // Yes:
        // Incr. the counter
        nOnCommEmpty++;     
        // Interval elapsed?
        if (nOnCommEmpty == _WAIT_ONCOMMEMPTY_PID)    
        {
          // Yes:
          bOnCommEmpty = false;       // Reset indication
          nOnCommEmpty = 0;           // Reset the counter
          OnCommEmpty ();             // Push the commands
        }
      }
    }

    #endregion methods
  
  }
}
