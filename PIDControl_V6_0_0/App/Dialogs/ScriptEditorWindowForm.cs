using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace App
{
  /// <summary>
  /// Class SriptEditorWindowForm:
  /// Sript editor: Processing of an script window entry
  /// </summary>
  /// <remarks>
  /// Regarding the correlation between the values of the calibration array members (PNx, PNy, N in [0,7]) 
  /// and the corr'ing values of the subitems of a Windows-ListView item:
  ///   PNx, PNy            subitems
  ///   ------------------------------------------------------
  ///   -1                  ""
  ///   Float, >= 0         String representation of the Float
  /// </remarks>
  public class ScriptEditorWindowForm : System.Windows.Forms.Form
  {
    private System.Windows.Forms.Button _btnOK;
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.TextBox _txtSubstance;
    private System.Windows.Forms.TextBox _txtCenter;
    private System.Windows.Forms.Label _lblCenter;
    private System.Windows.Forms.Label _lblWidth;
    private System.Windows.Forms.TextBox _txtWidth;
    private System.Windows.Forms.TextBox _txtDispLimit;
    private System.Windows.Forms.Label _lblZeroVal;
    private System.Windows.Forms.TextBox _txtZeroVal;
    private System.Windows.Forms.Label _lblP0;
    private System.Windows.Forms.Label _lblP1;
    private System.Windows.Forms.Label _lblP2;
    private System.Windows.Forms.Label _lblP3;
    private System.Windows.Forms.Label _lblP4;
    private System.Windows.Forms.Label _lblP5;
    private System.Windows.Forms.Label _lblP6;
    private System.Windows.Forms.Label _lblP7;
    private System.Windows.Forms.ToolTip _ToolTip;
    private System.Windows.Forms.ComboBox _cbConcUnit;
    private System.Windows.Forms.Label _lblConcUnit;
    private System.Windows.Forms.TextBox _txtP0x;
    private System.Windows.Forms.TextBox _txtP1x;
    private System.Windows.Forms.TextBox _txtP2x;
    private System.Windows.Forms.TextBox _txtP3x;
    private System.Windows.Forms.TextBox _txtP4x;
    private System.Windows.Forms.TextBox _txtP5x;
    private System.Windows.Forms.TextBox _txtP6x;
    private System.Windows.Forms.TextBox _txtP7x;
    private System.Windows.Forms.TextBox _txtP7y;
    private System.Windows.Forms.TextBox _txtP6y;
    private System.Windows.Forms.TextBox _txtP5y;
    private System.Windows.Forms.TextBox _txtP4y;
    private System.Windows.Forms.TextBox _txtP3y;
    private System.Windows.Forms.TextBox _txtP2y;
    private System.Windows.Forms.TextBox _txtP1y;
    private System.Windows.Forms.TextBox _txtP0y;
    private System.Windows.Forms.Label _lblSubstance;
    private System.Windows.Forms.Label _lblDispLimit;
    private System.Windows.Forms.ComboBox _cbCalType;
    private System.Windows.Forms.Label _lblCalType;
    private System.Windows.Forms.Label _lblUserVal;
    private System.Windows.Forms.TextBox _txtUserVal;
    private System.Windows.Forms.Label _lblA2;
    private System.Windows.Forms.TextBox _txtA2;
    private System.Windows.Forms.TextBox _txtA1;
    private System.Windows.Forms.Label _lblA1;
    private System.Windows.Forms.Label _lblSpanFac;
    private System.Windows.Forms.TextBox _txtSpanFac;
    private System.ComponentModel.IContainer components;

    /// <summary>
    /// Constructor
    /// </summary>
    public ScriptEditorWindowForm()
    {
      InitializeComponent();
      // Init.
      Init ();
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this._txtSubstance = new System.Windows.Forms.TextBox();
      this._txtCenter = new System.Windows.Forms.TextBox();
      this._lblSubstance = new System.Windows.Forms.Label();
      this._lblCenter = new System.Windows.Forms.Label();
      this._btnOK = new System.Windows.Forms.Button();
      this._btnCancel = new System.Windows.Forms.Button();
      this._lblWidth = new System.Windows.Forms.Label();
      this._txtWidth = new System.Windows.Forms.TextBox();
      this._lblDispLimit = new System.Windows.Forms.Label();
      this._txtDispLimit = new System.Windows.Forms.TextBox();
      this._lblZeroVal = new System.Windows.Forms.Label();
      this._lblA2 = new System.Windows.Forms.Label();
      this._txtZeroVal = new System.Windows.Forms.TextBox();
      this._txtA2 = new System.Windows.Forms.TextBox();
      this._lblP0 = new System.Windows.Forms.Label();
      this._txtP0x = new System.Windows.Forms.TextBox();
      this._lblP1 = new System.Windows.Forms.Label();
      this._txtP1x = new System.Windows.Forms.TextBox();
      this._lblP2 = new System.Windows.Forms.Label();
      this._txtP2x = new System.Windows.Forms.TextBox();
      this._lblP3 = new System.Windows.Forms.Label();
      this._txtP3x = new System.Windows.Forms.TextBox();
      this._lblP4 = new System.Windows.Forms.Label();
      this._txtP4x = new System.Windows.Forms.TextBox();
      this._lblP5 = new System.Windows.Forms.Label();
      this._txtP5x = new System.Windows.Forms.TextBox();
      this._lblP6 = new System.Windows.Forms.Label();
      this._txtP6x = new System.Windows.Forms.TextBox();
      this._lblP7 = new System.Windows.Forms.Label();
      this._txtP7x = new System.Windows.Forms.TextBox();
      this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
      this._cbConcUnit = new System.Windows.Forms.ComboBox();
      this._lblConcUnit = new System.Windows.Forms.Label();
      this._txtP7y = new System.Windows.Forms.TextBox();
      this._txtP6y = new System.Windows.Forms.TextBox();
      this._txtP5y = new System.Windows.Forms.TextBox();
      this._txtP4y = new System.Windows.Forms.TextBox();
      this._txtP3y = new System.Windows.Forms.TextBox();
      this._txtP2y = new System.Windows.Forms.TextBox();
      this._txtP1y = new System.Windows.Forms.TextBox();
      this._txtP0y = new System.Windows.Forms.TextBox();
      this._cbCalType = new System.Windows.Forms.ComboBox();
      this._lblCalType = new System.Windows.Forms.Label();
      this._lblUserVal = new System.Windows.Forms.Label();
      this._txtUserVal = new System.Windows.Forms.TextBox();
      this._txtA1 = new System.Windows.Forms.TextBox();
      this._lblA1 = new System.Windows.Forms.Label();
      this._txtSpanFac = new System.Windows.Forms.TextBox();
      this._lblSpanFac = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // _txtSubstance
      // 
      this._txtSubstance.Location = new System.Drawing.Point(112, 8);
      this._txtSubstance.Name = "_txtSubstance";
      this._txtSubstance.TabIndex = 1;
      this._txtSubstance.Text = "";
      this._txtSubstance.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtCenter
      // 
      this._txtCenter.Location = new System.Drawing.Point(112, 36);
      this._txtCenter.Name = "_txtCenter";
      this._txtCenter.TabIndex = 3;
      this._txtCenter.Text = "";
      this._txtCenter.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblSubstance
      // 
      this._lblSubstance.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblSubstance.Location = new System.Drawing.Point(8, 8);
      this._lblSubstance.Name = "_lblSubstance";
      this._lblSubstance.TabIndex = 0;
      this._lblSubstance.Text = "Substance:";
      // 
      // _lblCenter
      // 
      this._lblCenter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblCenter.Location = new System.Drawing.Point(8, 36);
      this._lblCenter.Name = "_lblCenter";
      this._lblCenter.TabIndex = 2;
      this._lblCenter.Text = "Center:";
      // 
      // _btnOK
      // 
      this._btnOK.Location = new System.Drawing.Point(148, 296);
      this._btnOK.Name = "_btnOK";
      this._btnOK.TabIndex = 46;
      this._btnOK.Text = "OK";
      this._btnOK.Click += new System.EventHandler(this._btnOK_Click);
      // 
      // _btnCancel
      // 
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point(248, 296);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.TabIndex = 47;
      this._btnCancel.Text = "Cancel";
      // 
      // _lblWidth
      // 
      this._lblWidth.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblWidth.Location = new System.Drawing.Point(8, 64);
      this._lblWidth.Name = "_lblWidth";
      this._lblWidth.TabIndex = 4;
      this._lblWidth.Text = "Width:";
      // 
      // _txtWidth
      // 
      this._txtWidth.Location = new System.Drawing.Point(112, 64);
      this._txtWidth.Name = "_txtWidth";
      this._txtWidth.TabIndex = 5;
      this._txtWidth.Text = "";
      this._txtWidth.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblDispLimit
      // 
      this._lblDispLimit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblDispLimit.Location = new System.Drawing.Point(8, 176);
      this._lblDispLimit.Name = "_lblDispLimit";
      this._lblDispLimit.TabIndex = 12;
      this._lblDispLimit.Text = "DispLimit:";
      // 
      // _txtDispLimit
      // 
      this._txtDispLimit.Location = new System.Drawing.Point(112, 176);
      this._txtDispLimit.Name = "_txtDispLimit";
      this._txtDispLimit.TabIndex = 13;
      this._txtDispLimit.Text = "";
      this._txtDispLimit.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblZeroVal
      // 
      this._lblZeroVal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblZeroVal.Location = new System.Drawing.Point(8, 148);
      this._lblZeroVal.Name = "_lblZeroVal";
      this._lblZeroVal.TabIndex = 10;
      this._lblZeroVal.Text = "ZeroVal:";
      // 
      // _lblA2
      // 
      this._lblA2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblA2.Location = new System.Drawing.Point(8, 120);
      this._lblA2.Name = "_lblA2";
      this._lblA2.TabIndex = 8;
      this._lblA2.Text = "A2:";
      // 
      // _txtZeroVal
      // 
      this._txtZeroVal.Location = new System.Drawing.Point(112, 148);
      this._txtZeroVal.Name = "_txtZeroVal";
      this._txtZeroVal.TabIndex = 11;
      this._txtZeroVal.Text = "";
      this._txtZeroVal.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtA2
      // 
      this._txtA2.Location = new System.Drawing.Point(112, 120);
      this._txtA2.Name = "_txtA2";
      this._txtA2.TabIndex = 9;
      this._txtA2.Text = "";
      this._txtA2.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblP0
      // 
      this._lblP0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblP0.Location = new System.Drawing.Point(220, 8);
      this._lblP0.Name = "_lblP0";
      this._lblP0.Size = new System.Drawing.Size(32, 23);
      this._lblP0.TabIndex = 20;
      this._lblP0.Text = "P0:";
      // 
      // _txtP0x
      // 
      this._txtP0x.Location = new System.Drawing.Point(256, 8);
      this._txtP0x.Name = "_txtP0x";
      this._txtP0x.TabIndex = 21;
      this._txtP0x.Text = "";
      this._txtP0x.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblP1
      // 
      this._lblP1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblP1.Location = new System.Drawing.Point(220, 36);
      this._lblP1.Name = "_lblP1";
      this._lblP1.Size = new System.Drawing.Size(32, 23);
      this._lblP1.TabIndex = 23;
      this._lblP1.Text = "P1:";
      // 
      // _txtP1x
      // 
      this._txtP1x.Location = new System.Drawing.Point(256, 36);
      this._txtP1x.Name = "_txtP1x";
      this._txtP1x.TabIndex = 24;
      this._txtP1x.Text = "";
      this._txtP1x.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblP2
      // 
      this._lblP2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblP2.Location = new System.Drawing.Point(220, 64);
      this._lblP2.Name = "_lblP2";
      this._lblP2.Size = new System.Drawing.Size(32, 23);
      this._lblP2.TabIndex = 26;
      this._lblP2.Text = "P2:";
      // 
      // _txtP2x
      // 
      this._txtP2x.Location = new System.Drawing.Point(256, 64);
      this._txtP2x.Name = "_txtP2x";
      this._txtP2x.TabIndex = 27;
      this._txtP2x.Text = "";
      this._txtP2x.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblP3
      // 
      this._lblP3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblP3.Location = new System.Drawing.Point(220, 92);
      this._lblP3.Name = "_lblP3";
      this._lblP3.Size = new System.Drawing.Size(32, 23);
      this._lblP3.TabIndex = 29;
      this._lblP3.Text = "P3:";
      // 
      // _txtP3x
      // 
      this._txtP3x.Location = new System.Drawing.Point(256, 92);
      this._txtP3x.Name = "_txtP3x";
      this._txtP3x.TabIndex = 30;
      this._txtP3x.Text = "";
      this._txtP3x.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblP4
      // 
      this._lblP4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblP4.Location = new System.Drawing.Point(220, 120);
      this._lblP4.Name = "_lblP4";
      this._lblP4.Size = new System.Drawing.Size(32, 23);
      this._lblP4.TabIndex = 32;
      this._lblP4.Text = "P4:";
      // 
      // _txtP4x
      // 
      this._txtP4x.Location = new System.Drawing.Point(256, 120);
      this._txtP4x.Name = "_txtP4x";
      this._txtP4x.TabIndex = 33;
      this._txtP4x.Text = "";
      this._txtP4x.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblP5
      // 
      this._lblP5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblP5.Location = new System.Drawing.Point(220, 148);
      this._lblP5.Name = "_lblP5";
      this._lblP5.Size = new System.Drawing.Size(32, 23);
      this._lblP5.TabIndex = 35;
      this._lblP5.Text = "P5:";
      // 
      // _txtP5x
      // 
      this._txtP5x.Location = new System.Drawing.Point(256, 148);
      this._txtP5x.Name = "_txtP5x";
      this._txtP5x.TabIndex = 36;
      this._txtP5x.Text = "";
      this._txtP5x.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblP6
      // 
      this._lblP6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblP6.Location = new System.Drawing.Point(220, 176);
      this._lblP6.Name = "_lblP6";
      this._lblP6.Size = new System.Drawing.Size(32, 23);
      this._lblP6.TabIndex = 38;
      this._lblP6.Text = "P6:";
      // 
      // _txtP6x
      // 
      this._txtP6x.Location = new System.Drawing.Point(256, 176);
      this._txtP6x.Name = "_txtP6x";
      this._txtP6x.TabIndex = 39;
      this._txtP6x.Text = "";
      this._txtP6x.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblP7
      // 
      this._lblP7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblP7.Location = new System.Drawing.Point(220, 204);
      this._lblP7.Name = "_lblP7";
      this._lblP7.Size = new System.Drawing.Size(32, 23);
      this._lblP7.TabIndex = 41;
      this._lblP7.Text = "P7:";
      // 
      // _txtP7x
      // 
      this._txtP7x.Location = new System.Drawing.Point(256, 204);
      this._txtP7x.Name = "_txtP7x";
      this._txtP7x.TabIndex = 42;
      this._txtP7x.Text = "";
      this._txtP7x.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _ToolTip
      // 
      this._ToolTip.AutoPopDelay = 10000;
      this._ToolTip.InitialDelay = 500;
      this._ToolTip.ReshowDelay = 100;
      // 
      // _cbConcUnit
      // 
      this._cbConcUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this._cbConcUnit.Location = new System.Drawing.Point(324, 232);
      this._cbConcUnit.Name = "_cbConcUnit";
      this._cbConcUnit.Size = new System.Drawing.Size(100, 21);
      this._cbConcUnit.TabIndex = 45;
      this._cbConcUnit.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblConcUnit
      // 
      this._lblConcUnit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblConcUnit.Location = new System.Drawing.Point(220, 232);
      this._lblConcUnit.Name = "_lblConcUnit";
      this._lblConcUnit.TabIndex = 44;
      this._lblConcUnit.Text = "Conc. unit:";
      // 
      // _txtP7y
      // 
      this._txtP7y.Location = new System.Drawing.Point(360, 204);
      this._txtP7y.Name = "_txtP7y";
      this._txtP7y.TabIndex = 43;
      this._txtP7y.Text = "";
      this._txtP7y.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtP6y
      // 
      this._txtP6y.Location = new System.Drawing.Point(360, 176);
      this._txtP6y.Name = "_txtP6y";
      this._txtP6y.TabIndex = 40;
      this._txtP6y.Text = "";
      this._txtP6y.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtP5y
      // 
      this._txtP5y.Location = new System.Drawing.Point(360, 148);
      this._txtP5y.Name = "_txtP5y";
      this._txtP5y.TabIndex = 37;
      this._txtP5y.Text = "";
      this._txtP5y.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtP4y
      // 
      this._txtP4y.Location = new System.Drawing.Point(360, 120);
      this._txtP4y.Name = "_txtP4y";
      this._txtP4y.TabIndex = 34;
      this._txtP4y.Text = "";
      this._txtP4y.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtP3y
      // 
      this._txtP3y.Location = new System.Drawing.Point(360, 92);
      this._txtP3y.Name = "_txtP3y";
      this._txtP3y.TabIndex = 31;
      this._txtP3y.Text = "";
      this._txtP3y.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtP2y
      // 
      this._txtP2y.Location = new System.Drawing.Point(360, 64);
      this._txtP2y.Name = "_txtP2y";
      this._txtP2y.TabIndex = 28;
      this._txtP2y.Text = "";
      this._txtP2y.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtP1y
      // 
      this._txtP1y.Location = new System.Drawing.Point(360, 36);
      this._txtP1y.Name = "_txtP1y";
      this._txtP1y.TabIndex = 25;
      this._txtP1y.Text = "";
      this._txtP1y.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtP0y
      // 
      this._txtP0y.Location = new System.Drawing.Point(360, 8);
      this._txtP0y.Name = "_txtP0y";
      this._txtP0y.TabIndex = 22;
      this._txtP0y.Text = "";
      this._txtP0y.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _cbCalType
      // 
      this._cbCalType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this._cbCalType.Location = new System.Drawing.Point(112, 260);
      this._cbCalType.Name = "_cbCalType";
      this._cbCalType.Size = new System.Drawing.Size(100, 21);
      this._cbCalType.TabIndex = 19;
      this._cbCalType.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblCalType
      // 
      this._lblCalType.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblCalType.Location = new System.Drawing.Point(8, 260);
      this._lblCalType.Name = "_lblCalType";
      this._lblCalType.TabIndex = 18;
      this._lblCalType.Text = "Calibration type:";
      // 
      // _lblUserVal
      // 
      this._lblUserVal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblUserVal.Location = new System.Drawing.Point(8, 232);
      this._lblUserVal.Name = "_lblUserVal";
      this._lblUserVal.TabIndex = 16;
      this._lblUserVal.Text = "UserVal:";
      // 
      // _txtUserVal
      // 
      this._txtUserVal.Location = new System.Drawing.Point(112, 232);
      this._txtUserVal.Name = "_txtUserVal";
      this._txtUserVal.TabIndex = 17;
      this._txtUserVal.Text = "";
      this._txtUserVal.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtA1
      // 
      this._txtA1.Location = new System.Drawing.Point(112, 92);
      this._txtA1.Name = "_txtA1";
      this._txtA1.TabIndex = 7;
      this._txtA1.Text = "";
      this._txtA1.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblA1
      // 
      this._lblA1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblA1.Location = new System.Drawing.Point(8, 92);
      this._lblA1.Name = "_lblA1";
      this._lblA1.TabIndex = 6;
      this._lblA1.Text = "A1:";
      // 
      // _txtSpanFac
      // 
      this._txtSpanFac.Location = new System.Drawing.Point(112, 204);
      this._txtSpanFac.Name = "_txtSpanFac";
      this._txtSpanFac.TabIndex = 15;
      this._txtSpanFac.Text = "";
      this._txtSpanFac.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblSpanFac
      // 
      this._lblSpanFac.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblSpanFac.Location = new System.Drawing.Point(8, 204);
      this._lblSpanFac.Name = "_lblSpanFac";
      this._lblSpanFac.TabIndex = 14;
      this._lblSpanFac.Text = "Span factor:";
      // 
      // ScriptEditorWindowForm
      // 
      this.AcceptButton = this._btnOK;
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.CancelButton = this._btnCancel;
      this.ClientSize = new System.Drawing.Size(470, 332);
      this.Controls.Add(this._txtSpanFac);
      this.Controls.Add(this._txtA1);
      this.Controls.Add(this._txtUserVal);
      this.Controls.Add(this._txtP7y);
      this.Controls.Add(this._txtP6y);
      this.Controls.Add(this._txtP5y);
      this.Controls.Add(this._txtP4y);
      this.Controls.Add(this._txtP3y);
      this.Controls.Add(this._txtP2y);
      this.Controls.Add(this._txtP1y);
      this.Controls.Add(this._txtP0y);
      this.Controls.Add(this._txtP7x);
      this.Controls.Add(this._txtP6x);
      this.Controls.Add(this._txtP5x);
      this.Controls.Add(this._txtP4x);
      this.Controls.Add(this._txtP3x);
      this.Controls.Add(this._txtP2x);
      this.Controls.Add(this._txtP1x);
      this.Controls.Add(this._txtP0x);
      this.Controls.Add(this._txtDispLimit);
      this.Controls.Add(this._txtZeroVal);
      this.Controls.Add(this._txtA2);
      this.Controls.Add(this._txtWidth);
      this.Controls.Add(this._txtCenter);
      this.Controls.Add(this._txtSubstance);
      this.Controls.Add(this._lblSpanFac);
      this.Controls.Add(this._lblA1);
      this.Controls.Add(this._lblUserVal);
      this.Controls.Add(this._cbCalType);
      this.Controls.Add(this._lblCalType);
      this.Controls.Add(this._cbConcUnit);
      this.Controls.Add(this._lblConcUnit);
      this.Controls.Add(this._lblP7);
      this.Controls.Add(this._lblP6);
      this.Controls.Add(this._lblP5);
      this.Controls.Add(this._lblP4);
      this.Controls.Add(this._lblP3);
      this.Controls.Add(this._lblP2);
      this.Controls.Add(this._lblP1);
      this.Controls.Add(this._lblP0);
      this.Controls.Add(this._lblDispLimit);
      this.Controls.Add(this._lblZeroVal);
      this.Controls.Add(this._lblA2);
      this.Controls.Add(this._lblWidth);
      this.Controls.Add(this._btnCancel);
      this.Controls.Add(this._btnOK);
      this.Controls.Add(this._lblCenter);
      this.Controls.Add(this._lblSubstance);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.HelpButton = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ScriptEditorWindowForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Script editor - Window";
      this.Load += new System.EventHandler(this.SriptEditorWindowForm_Load);
      this.ResumeLayout(false);

    }

    #endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void SriptEditorWindowForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      try
      {
        // Resources
        this.Text               = r.GetString ( "ScriptEditorWindow_Title" );       // "Scripteditor - Fenster"
        this._btnCancel.Text    = r.GetString ( "ScriptEditorWindow_btnCancel" );   // "Abbruch"
        this._btnOK.Text        = r.GetString ( "ScriptEditorWindow_btnOK" );       // "OK"
        
        this._lblSubstance.Text = r.GetString ( "ScriptEditorWindow_lblSubstance" );// "Substanz:"
        this._lblCenter.Text    = r.GetString ( "ScriptEditorWindow_lblCenter" );   // "Zentrum:"
        this._lblWidth.Text     = r.GetString ( "ScriptEditorWindow_lblWidth" );    // "Breite:"
        
        this._lblA1.Text        = r.GetString ( "ScriptEditorWindow_lblA1" );       // "LO-Alarm:"
        this._lblA2.Text        = r.GetString ( "ScriptEditorWindow_lblA2" );       // "HI-Alarm:"
        this._lblZeroVal.Text   = r.GetString ( "ScriptEditorWindow_lblZeroVal" );  // "Nullwert:"
        this._lblDispLimit.Text = r.GetString ( "ScriptEditorWindow_lblDispLimit" );// "Displaygrenzwert:"
        this._lblSpanFac.Text   = r.GetString ( "ScriptEditorWindow_lblSpanFac" );  // "Spanfaktor:"
        this._lblUserVal.Text   = r.GetString ( "ScriptEditorWindow_lblUserVal" );  // "UserVal:"
        
        this._lblCalType.Text   = r.GetString ( "ScriptEditorWindow_lblCalType" );  // "Kalibrationstyp:"
        
        this._lblConcUnit.Text  = r.GetString ( "ScriptEditorWindow_lblConcUnit" ); // "Konz.-einheit:"
      }
      catch
      {
      }

      // Control initialisation
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
      _txtSubstance.Text  = _Substance;
      _txtCenter.Text     = _Center.ToString ();
      _txtWidth.Text      = _Width.ToString ();
      _txtA1.Text         = _A1.ToString (nfi);
      _txtA2.Text         = _A2.ToString (nfi);
      _txtZeroVal.Text    = _ZeroVal.ToString (nfi);
      _txtDispLimit.Text  = _DispLimit.ToString (nfi);
      _txtSpanFac.Text    = _SpanFac.ToString (nfi);
      _txtUserVal.Text    = _UserVal.ToString (nfi);

      _cbCalType.SelectedIndex = _CalType;

      _txtP0x.Text = (_P0x == -1) ? "" : _P0x.ToString (nfi);
      _txtP0y.Text = (_P0y == -1) ? "" : _P0y.ToString (nfi);
      _txtP1x.Text = (_P1x == -1) ? "" : _P1x.ToString (nfi); 
      _txtP1y.Text = (_P1y == -1) ? "" : _P1y.ToString (nfi);
      _txtP2x.Text = (_P2x == -1) ? "" : _P2x.ToString (nfi); 
      _txtP2y.Text = (_P2y == -1) ? "" : _P2y.ToString (nfi);
      _txtP3x.Text = (_P3x == -1) ? "" : _P3x.ToString (nfi); 
      _txtP3y.Text = (_P3y == -1) ? "" : _P3y.ToString (nfi);
      _txtP4x.Text = (_P4x == -1) ? "" : _P4x.ToString (nfi); 
      _txtP4y.Text = (_P4y == -1) ? "" : _P4y.ToString (nfi);
      _txtP5x.Text = (_P5x == -1) ? "" : _P5x.ToString (nfi); 
      _txtP5y.Text = (_P5y == -1) ? "" : _P5y.ToString (nfi);
      _txtP6x.Text = (_P6x == -1) ? "" : _P6x.ToString (nfi); 
      _txtP6y.Text = (_P6y == -1) ? "" : _P6y.ToString (nfi);
      _txtP7x.Text = (_P7x == -1) ? "" : _P7x.ToString (nfi); 
      _txtP7y.Text = (_P7y == -1) ? "" : _P7y.ToString (nfi);

      _cbConcUnit.SelectedItem = _ConcUnit;
    }

    /// <summary>
    /// 'Click' event of the 'OK' button
    /// </summary>
    private void _btnOK_Click(object sender, System.EventArgs e)
    {
      object o;

      App app = App.Instance;
      Ressources r = app.Ressources;
      
      // Substance
      if ( !Check.Execute ( this._txtSubstance, CheckType.String, CheckRelation.IN, 1, ScriptWindow.MAX_SCRIPT_WINDOWSUBSTANCENAME, true, out o) )
        return;
      _Substance=(string) o;
      
      // Center
      if ( !Check.Execute ( this._txtCenter, CheckType.Int, CheckRelation.IN, 0, Doc.NSCANDATA, true, out o) )
        return;
      _Center=(int) o;
      // Width
      if ( !Check.Execute ( this._txtWidth, CheckType.Int, CheckRelation.IN, 1, Doc.NSCANDATA, true, out o) )
        return;
      _Width=(int) o;
      // LO alert
      if ( !Check.Execute ( this._txtA1, CheckType.Float, CheckRelation.GE, 0, 0, true, out o) )
        return;
      _A1=(float) o;
      // HI alert
      if ( !Check.Execute ( this._txtA2, CheckType.Float, CheckRelation.GE, 0, 0, true, out o) )
        return;
      _A2=(float) o;
      if (!(_A1 < _A2))
      {
        // Show message
        string sMsg = r.GetString ("ScriptEditorWindow_btnOK_Error");   // "Inconsistent alert values: LO alert must be less than HI alert."
        string sCap = r.GetString ("Form_Common_TextError");            // "Fehler"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        // Focus A1
        this._txtA1.Focus ();
        return;
      }
      // ZeroVal
      if ( !Check.Execute ( this._txtZeroVal, CheckType.Float, CheckRelation.GE, 0, 0, true, out o) )
        return;
      _ZeroVal=(float) o;
      // DispLimit
      if ( !Check.Execute ( this._txtDispLimit, CheckType.Float, CheckRelation.GE, 0, 0, true, out o) )
        return;
      _DispLimit=(float) o;
      
      // SpanFac
      if ( !Check.Execute ( this._txtSpanFac, CheckType.Float, CheckRelation.GT, 0, 0, true, out o) )
        return;
      _SpanFac=(float) o;
      
      // UserVal
      if ( !Check.Execute ( this._txtUserVal, CheckType.Float, CheckRelation.GE, 0, 0, true, out o) )
        return;
      _UserVal=(float) o;

      // Calib. type (No need to check for validity, because the CB has the DropDownList style)
      _CalType = this._cbCalType.SelectedIndex;

      // Calibration arrays
      // Notes:
      //  The first 'MIN_CALIB_POINTS' calib. points MUST contain valid data - empty fields are NOT allowed.
      //  The subsequent calib. points NEED NOT to contain valid data  - empty fields are allowed.
      //    The checklist for all calib. points: All calib. points must be >= 0
      CheckRelationItemList checklist = new CheckRelationItemList ();   
      checklist.Add (new CheckRelationItem (CheckRelation.GE, 0, 0));   
      bool bNotEmpty;
      int count = 0;
      //    P0
      bNotEmpty = (count++ < ScriptWindow.MIN_CALIB_POINTS) ? true : false;
      if ( !Check.Execute ( this._txtP0x, bNotEmpty, CheckType.Float, checklist, true, out o) )
        return;
      _P0x = (null == o) ? -1 : (float) o;
      if ( !Check.Execute ( this._txtP0y, bNotEmpty, CheckType.Float, checklist, true, out o) )
        return;
      _P0y = (null == o) ? -1 : (float) o;
      //    P1
      bNotEmpty = (count++ < ScriptWindow.MIN_CALIB_POINTS) ? true : false;
      if ( !Check.Execute ( this._txtP1x, bNotEmpty, CheckType.Float, checklist, true, out o) )
        return;
      _P1x = (null == o) ? -1 : (float) o;
      if ( !Check.Execute ( this._txtP1y, bNotEmpty, CheckType.Float, checklist, true, out o) )
        return;
      _P1y = (null == o) ? -1 : (float) o;
      //    P2
      bNotEmpty = (count++ < ScriptWindow.MIN_CALIB_POINTS) ? true : false;
      if ( !Check.Execute ( this._txtP2x, bNotEmpty, CheckType.Float, checklist, true, out o) )
        return;
      _P2x = (null == o) ? -1 : (float) o;
      if ( !Check.Execute ( this._txtP2y, bNotEmpty, CheckType.Float, checklist, true, out o) )
        return;
      _P2y = (null == o) ? -1 : (float) o;
      //    P3
      bNotEmpty = (count++ < ScriptWindow.MIN_CALIB_POINTS) ? true : false;
      if ( !Check.Execute ( this._txtP3x, bNotEmpty, CheckType.Float, checklist, true, out o) )
        return;
      _P3x = (null == o) ? -1 : (float) o;
      if ( !Check.Execute ( this._txtP3y, bNotEmpty, CheckType.Float, checklist, true, out o) )
        return;
      _P3y = (null == o) ? -1 : (float) o;
      //    P4
      bNotEmpty = (count++ < ScriptWindow.MIN_CALIB_POINTS) ? true : false;
      if ( !Check.Execute ( this._txtP4x, bNotEmpty, CheckType.Float, checklist, true, out o) )
        return;
      _P4x = (null == o) ? -1 : (float) o;
      if ( !Check.Execute ( this._txtP4y, bNotEmpty, CheckType.Float, checklist, true, out o) )
        return;
      _P4y = (null == o) ? -1 : (float) o;
      //    P5
      bNotEmpty = (count++ < ScriptWindow.MIN_CALIB_POINTS) ? true : false;
      if ( !Check.Execute ( this._txtP5x, bNotEmpty, CheckType.Float, checklist, true, out o) )
        return;
      _P5x = (null == o) ? -1 : (float) o;
      if ( !Check.Execute ( this._txtP5y, bNotEmpty, CheckType.Float, checklist, true, out o) )
        return;
      _P5y = (null == o) ? -1 : (float) o;
      //    P6
      bNotEmpty = (count++ < ScriptWindow.MIN_CALIB_POINTS) ? true : false;
      if ( !Check.Execute ( this._txtP6x, bNotEmpty, CheckType.Float, checklist, true, out o) )
        return;
      _P6x = (null == o) ? -1 : (float) o;
      if ( !Check.Execute ( this._txtP6y, bNotEmpty, CheckType.Float, checklist, true, out o) )
        return;
      _P6y = (null == o) ? -1 : (float) o;
      //    P7
      bNotEmpty = (count++ < ScriptWindow.MIN_CALIB_POINTS) ? true : false;
      if ( !Check.Execute ( this._txtP7x, bNotEmpty, CheckType.Float, checklist, true, out o) )
        return;
      _P7x = (null == o) ? -1 : (float) o;
      if ( !Check.Execute ( this._txtP7y, bNotEmpty, CheckType.Float, checklist, true, out o) )
        return;
      _P7y = (null == o) ? -1 : (float) o;

      // Conc. unit (No need to check for validity, because the CB has the DropDownList style)
      _ConcUnit = this._cbConcUnit.SelectedItem.ToString ();
      
      // Check, whether the corr'ing script window is valid
      try 
      {
        // Build the script window, that corresponds to the data
        // Notes:
        //  The conversion (d - double, f - float)
        //    d = double.Parse ( f.ToString(nfi), nfi );
        //  returns d with the same signature as f.
        //  The obvious assignment
        //    d = f;
        //  returns d with more decimal digits than f has, what is NOT desired here: The subsequent Check
        //  routine for a window determines among others the length of the window string, and this length
        //  would differ a lot due to the number of decimal digits.
        ScriptWindow wnd = new ScriptWindow ();
        
        wnd.szSubstance = _Substance;
        
        System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
        wnd.dwBegin     = (_Center - _Width/2 > 0) ? (UInt32)(_Center - _Width/2) : 0;
        wnd.dwWidth     = (UInt32)_Width;
        wnd.dA1Limit    = double.Parse ( _A1.ToString(nfi), nfi );
        wnd.dA2Limit    = double.Parse ( _A2.ToString(nfi), nfi );
        wnd.dZeroVal    = double.Parse ( _ZeroVal.ToString(nfi), nfi );
        wnd.dDispLimit  = double.Parse ( _DispLimit.ToString(nfi), nfi );
        wnd.dSpanfac    = double.Parse ( _SpanFac.ToString(nfi), nfi );
        wnd.dUserVal    = double.Parse ( _UserVal.ToString(nfi), nfi );
        
        wnd.nCalType    = _CalType;
      
        wnd.ardXcal[0] = double.Parse ( _P0x.ToString(nfi), nfi );  wnd.ardYcal[0] = double.Parse ( _P0y.ToString(nfi), nfi );
        wnd.ardXcal[1] = double.Parse ( _P1x.ToString(nfi), nfi );  wnd.ardYcal[1] = double.Parse ( _P1y.ToString(nfi), nfi );
        wnd.ardXcal[2] = double.Parse ( _P2x.ToString(nfi), nfi );  wnd.ardYcal[2] = double.Parse ( _P2y.ToString(nfi), nfi );
        wnd.ardXcal[3] = double.Parse ( _P3x.ToString(nfi), nfi );  wnd.ardYcal[3] = double.Parse ( _P3y.ToString(nfi), nfi );
        wnd.ardXcal[4] = double.Parse ( _P4x.ToString(nfi), nfi );  wnd.ardYcal[4] = double.Parse ( _P4y.ToString(nfi), nfi );
        wnd.ardXcal[5] = double.Parse ( _P5x.ToString(nfi), nfi );  wnd.ardYcal[5] = double.Parse ( _P5y.ToString(nfi), nfi );
        wnd.ardXcal[6] = double.Parse ( _P6x.ToString(nfi), nfi );  wnd.ardYcal[6] = double.Parse ( _P6y.ToString(nfi), nfi );
        wnd.ardXcal[7] = double.Parse ( _P7x.ToString(nfi), nfi );  wnd.ardYcal[7] = double.Parse ( _P7y.ToString(nfi), nfi );

        wnd.sConcUnit   = _ConcUnit;
        
        // Check, whether the script window is valid
        wnd.Check ();
      }
      catch (System.Exception exc)
      {
        // No: Show the corr'ing error message & quit
        string sMsg = exc.Message;
        string sCap = r.GetString ( "Form_Common_TextError" ); // "Error"
        MessageBox.Show ( sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error ); 
        return;
      }
      
      // OK
      this.DialogResult = DialogResult.OK;
      this.Close ();
    }
 
    /// <summary>
    /// 'HelpRequested' event of the TextBox controls
    /// </summary>
    private void _ctl_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Help requesting control
      Control requestingControl = (Control)sender;
      // Assign help text
      string s = "", fmt;
      if      (requestingControl == _txtSubstance)
        s = r.GetString ("ScriptEditorWindow_Hlp_txtSubstance");  // "The name of the substance.";
      else if (requestingControl == _txtCenter)
        s = r.GetString ("ScriptEditorWindow_Hlp_txtCenter");     // "The center of the substance window (in [0, 2047] pts)."
      else if (requestingControl == _txtWidth)
        s = r.GetString ("ScriptEditorWindow_Hlp_txtWidth");      // "The width of the substance window (in pts).";
      
      else if (requestingControl == _txtA1)
        s = r.GetString ("ScriptEditorWindow_Hlp_txtA1");         // "The LO alert value (in concentration units): A LO alert is signaled, when the substance concentration is above this concentration threshold.";
      else if (requestingControl == _txtA2)
        s = r.GetString ("ScriptEditorWindow_Hlp_txtA2");         // "The HI alert value (in concentration units): A HI alert is signaled, when the substance concentration is above this concentration threshold.";
      else if (requestingControl == _txtZeroVal)
        s = r.GetString ("ScriptEditorWindow_Hlp_txtZeroVal");    // "The zero value (in concentration units): This value is subtracted from the measured (calculated) concentration.";
      else if (requestingControl == _txtDispLimit)
        s = r.GetString ("ScriptEditorWindow_Hlp_txtDispLimit");  // "The display limit (in concentration units): The substance is displayed on the LCD if its concentration exceeds this value.";
      else if (requestingControl == _txtSpanFac)
        s = r.GetString ("ScriptEditorWindow_Hlp_txtSpanFac");    // "The span factor (dimensionless): Must be set to 1.0 initially.";
      else if (requestingControl == _txtUserVal)
        s = r.GetString ("ScriptEditorWindow_Hlp_txtUserVal");    // "The user value (in concentration units): If the concentration exceeds this value, then 20mA = 4096 digits are output via current loop.";
      
      else if (requestingControl == _cbCalType)
        s = r.GetString ("ScriptEditorWindow_Hlp_cbCalType");     // "The calibration type of the substance.";

      else if (requestingControl == _txtP0x)
      {
        fmt = r.GetString ("ScriptEditorWindow_Hlp_txtPx");         // "{0}. Punkt der Kalibrationstabelle: Abszisse (Peakh�he/fl�che)"
        s = string.Format (fmt, "1");
      }
      else if (requestingControl == _txtP1x)
      {
        fmt = r.GetString ("ScriptEditorWindow_Hlp_txtPx");       
        s = string.Format (fmt, "2");
      }
      else if (requestingControl == _txtP2x)
      {
        fmt = r.GetString ("ScriptEditorWindow_Hlp_txtPx");       
        s = string.Format (fmt, "3");
      }
      else if (requestingControl == _txtP3x)
      {
        fmt = r.GetString ("ScriptEditorWindow_Hlp_txtPx");        
        s = string.Format (fmt, "4");
      }
      else if (requestingControl == _txtP4x)
      {
        fmt = r.GetString ("ScriptEditorWindow_Hlp_txtPx");        
        s = string.Format (fmt, "5");
      }
      else if (requestingControl == _txtP5x)
      {
        fmt = r.GetString ("ScriptEditorWindow_Hlp_txtPx");        
        s = string.Format (fmt, "6");
      }
      else if (requestingControl == _txtP6x)
      {
        fmt = r.GetString ("ScriptEditorWindow_Hlp_txtPx");        
        s = string.Format (fmt, "7");
      }
      else if (requestingControl == _txtP7x)
      {
        fmt = r.GetString ("ScriptEditorWindow_Hlp_txtPx");        
        s = string.Format (fmt, "8");
      }

      else if (requestingControl == _txtP0y)
      {
        fmt = r.GetString ("ScriptEditorWindow_Hlp_txtPy");         // "{0}. Punkt der Kalibrationstabelle: Ordinate (Konzentration)"
        s = string.Format (fmt, "1");
      }
      else if (requestingControl == _txtP1y)
      {
        fmt = r.GetString ("ScriptEditorWindow_Hlp_txtPy");         
        s = string.Format (fmt, "2");
      }
      else if (requestingControl == _txtP2y)
      {
        fmt = r.GetString ("ScriptEditorWindow_Hlp_txtPy");         
        s = string.Format (fmt, "3");
      }
      else if (requestingControl == _txtP3y)
      {
        fmt = r.GetString ("ScriptEditorWindow_Hlp_txtPy");        
        s = string.Format (fmt, "4");
      }
      else if (requestingControl == _txtP4y)
      {
        fmt = r.GetString ("ScriptEditorWindow_Hlp_txtPy");        
        s = string.Format (fmt, "5");
      }
      else if (requestingControl == _txtP5y)
      {
        fmt = r.GetString ("ScriptEditorWindow_Hlp_txtPy");        
        s = string.Format (fmt, "6");
      }
      else if (requestingControl == _txtP6y)
      {
        fmt = r.GetString ("ScriptEditorWindow_Hlp_txtPy");        
        s = string.Format (fmt, "7");
      }
      else if (requestingControl == _txtP7y)
      {
        fmt = r.GetString ("ScriptEditorWindow_Hlp_txtPy");        
        s = string.Format (fmt, "8");
      }

      else if (requestingControl == _cbConcUnit)
        s = r.GetString ("ScriptEditorWindow_Hlp_cbConcUnit");    // "The concentration unit of the substance.";
      
      // Show help
      this._ToolTip.SetToolTip (requestingControl, s);
      hlpevent.Handled=true;
    }

    #endregion handling

    #region members

    /// <summary>
    /// The substance name
    /// </summary>
    string _Substance = "";
    
    /// <summary>
    /// The window center (in pts)
    /// </summary>
    int _Center = 100;

    /// <summary>
    /// The window width (in pts)
    /// </summary>
    int _Width = 10;

    /// <summary>
    /// The LO alert value (unit as concentration)
    /// </summary>
    float _A1 = 90.0f;
    
    /// <summary>
    /// The HI alert value (unit as concentration)
    /// </summary>
    float _A2 = 100.0f;
    
    /// <summary>
    /// The ZeroVal value (unit as concentration)
    /// </summary>
    float _ZeroVal = 0.0f;

    /// <summary>
    /// The DispLimit value (unit as concentration)
    /// </summary>
    float _DispLimit = 0.0f;
    
    /// <summary>
    /// The Span factor (dimensionless)
    /// </summary>
    float _SpanFac = 1.0f;
    
    /// <summary>
    /// The user value (unit as concentration)
    /// </summary>
    float _UserVal = 0.0f;
    
    /// <summary>
    /// The calibration type: 0 - Height, 1 - Area 
    /// </summary>
    int _CalType = 0;

    /// <summary>
    /// The calibration arrays
    /// </summary>
    float _P0x = -1; float _P0y = -1;
    float _P1x = -1; float _P1y = -1;
    float _P2x = -1; float _P2y = -1;
    float _P3x = -1; float _P3y = -1;
    float _P4x = -1; float _P4y = -1;
    float _P5x = -1; float _P5y = -1;
    float _P6x = -1; float _P6y = -1;
    float _P7x = -1; float _P7y = -1;
    
    /// <summary>
    /// The concentration unit
    /// </summary>
    string _ConcUnit = ConcUnits.ppb;
    
    #endregion members

    #region methods

    /// <summary>
    /// Init's the form.
    /// </summary>
    void Init ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // CB 'Conc. unit'
      this._cbConcUnit.Items.Clear ();
      // NOTE: 
      //  The following concentration units MUST coincide with those used by the device SW
      //  (array 'g_arsConcUnit[]').
      this._cbConcUnit.Items.AddRange (new object[] {
                                                      ConcUnits.ppt,
                                                      ConcUnits.ppb, 
                                                      ConcUnits.ppm, 
                                                      ConcUnits.ug_cbm, 
                                                      ConcUnits.mg_cbm, 
                                                      ConcUnits.Percent
                                                    }
        );

      // CB 'Calib. type'
      this._cbCalType.Items.Clear ();
      this._cbCalType.Items.Add (r.GetString ( "ScriptEditorWindow_cbCalType_Height" ));      // "Peakh�he"
      this._cbCalType.Items.Add (r.GetString ( "ScriptEditorWindow_cbCalType_Area" ));        // "Peakfl�che"
    }

    #endregion methods

    #region properties

    /// <summary>
    /// The substance name
    /// </summary>
    public string Substance
    {
      get { return _Substance; }
      set { _Substance = value; }
    }

    /// <summary>
    /// The window center (in pts)
    /// </summary>
    public int Center
    {
      get { return _Center; }
      set { _Center = value; }
    }

    /// <summary>
    /// The window width (in pts)
    /// </summary>
    public new int Width
    {
      get { return _Width; }
      set { _Width = value; }
    }

    /// <summary>
    /// The LO alert value (unit as concentration)
    /// </summary>
    public float A1
    {
      get { return _A1; }
      set { _A1 = value; }
    }
    
    /// <summary>
    /// The HI alert value (unit as concentration)
    /// </summary>
    public float A2
    {
      get { return _A2; }
      set { _A2 = value; }
    }
    
    /// <summary>
    /// The ZeroVal value (unit as concentration)
    /// </summary>
    public float ZeroVal
    {
      get { return _ZeroVal; }
      set { _ZeroVal = value; }
    }
    
    /// <summary>
    /// The DispLimit value (unit as concentration)
    /// </summary>
    public float DispLimit
    {
      get { return _DispLimit; }
      set { _DispLimit = value; }
    }

    /// <summary>
    /// The Span factor (dimensionless)
    /// </summary>
    public float SpanFac
    {
      get { return _SpanFac; }
      set { _SpanFac = value; }
    }

    /// <summary>
    /// The User value (unit as concentration)
    /// </summary>
    public float UserVal
    {
      get { return _UserVal; }
      set { _UserVal = value; }
    }

    /// <summary>
    /// The calibration type: 0 - Height, 1 - Area 
    /// </summary>
    public int CalType
    {
      get { return _CalType; }
      set { _CalType = value; }
    }

    /// <summary>
    /// 1. calibration array point: abscissa (peak area)
    /// </summary>
    public float P0x
    {
      get { return _P0x; }
      set { _P0x = value; }
    }
    
    /// <summary>
    /// 2. calibration array point: abscissa (peak area)
    /// </summary>
    public float P1x
    {
      get { return _P1x; }
      set { _P1x = value; }
    }
    
    /// <summary>
    /// 3. calibration array point: abscissa (peak area)
    /// </summary>
    public float P2x
    {
      get { return _P2x; }
      set { _P2x = value; }
    }
    
    /// <summary>
    /// 4. calibration array point: abscissa (peak area)
    /// </summary>
    public float P3x
    {
      get { return _P3x; }
      set { _P3x = value; }
    }

    /// <summary>
    /// 5. calibration array point: abscissa (peak area)
    /// </summary>
    public float P4x
    {
      get { return _P4x; }
      set { _P4x = value; }
    }
    
    /// <summary>
    /// 6. calibration array point: abscissa (peak area)
    /// </summary>
    public float P5x
    {
      get { return _P5x; }
      set { _P5x = value; }
    }
    
    /// <summary>
    /// 7. calibration array point: abscissa (peak area)
    /// </summary>
    public float P6x
    {
      get { return _P6x; }
      set { _P6x = value; }
    }
    
    /// <summary>
    /// 8. calibration array point: abscissa (peak area)
    /// </summary>
    public float P7x
    {
      get { return _P7x; }
      set { _P7x = value; }
    }
    
    
    /// <summary>
    /// 1. calibration array point: ordinate (concentration, unit as ConcUnit member)
    /// </summary>
    public float P0y
    {
      get { return _P0y; }
      set { _P0y = value; }
    }
    
    /// <summary>
    /// 2. calibration array point: ordinate (concentration, unit as ConcUnit member)
    /// </summary>
    public float P1y
    {
      get { return _P1y; }
      set { _P1y = value; }
    }
    
    /// <summary>
    /// 3. calibration array point: ordinate (concentration, unit as ConcUnit member)
    /// </summary>
    public float P2y
    {
      get { return _P2y; }
      set { _P2y = value; }
    }
    
    /// <summary>
    /// 4. calibration array point: ordinate (concentration, unit as ConcUnit member)
    /// </summary>
    public float P3y
    {
      get { return _P3y; }
      set { _P3y = value; }
    }

    /// <summary>
    /// 5. calibration array point: ordinate (concentration, unit as ConcUnit member)
    /// </summary>
    public float P4y
    {
      get { return _P4y; }
      set { _P4y = value; }
    }
    
    /// <summary>
    /// 6. calibration array point: ordinate (concentration, unit as ConcUnit member)
    /// </summary>
    public float P5y
    {
      get { return _P5y; }
      set { _P5y = value; }
    }
    
    /// <summary>
    /// 7. calibration array point: ordinate (concentration, unit as ConcUnit member)
    /// </summary>
    public float P6y
    {
      get { return _P6y; }
      set { _P6y = value; }
    }
    
    /// <summary>
    /// 8. calibration array point: ordinate (concentration, unit as ConcUnit member)
    /// </summary>
    public float P7y
    {
      get { return _P7y; }
      set { _P7y = value; }
    }

    
    /// <summary>
    /// The concentration unit
    /// </summary>
    public string ConcUnit
    {
      get { return _ConcUnit; }
      set { _ConcUnit = value; }
    }

    #endregion properties

  }

}
