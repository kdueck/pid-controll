using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace App
{
  /// <summary>
  /// Class SriptEditorSpanConcForm:
  /// Sript editor: Processing of a Span concentration entry
  /// </summary>
  public class ScriptEditorSpanConcForm : System.Windows.Forms.Form
  {
    private System.Windows.Forms.Button _btnOK;
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.ToolTip _ToolTip;
    private System.Windows.Forms.TextBox _txtIdx;
    private System.Windows.Forms.Label _lblValue;
    private System.Windows.Forms.Label _lblIdx;
    private System.Windows.Forms.TextBox _txtValue;
    private System.ComponentModel.IContainer components;

    /// <summary>
    /// Constructor
    /// </summary>
    public ScriptEditorSpanConcForm()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this._txtIdx = new System.Windows.Forms.TextBox();
      this._lblIdx = new System.Windows.Forms.Label();
      this._btnOK = new System.Windows.Forms.Button();
      this._btnCancel = new System.Windows.Forms.Button();
      this._lblValue = new System.Windows.Forms.Label();
      this._txtValue = new System.Windows.Forms.TextBox();
      this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
      this.SuspendLayout();
      // 
      // _txtIdx
      // 
      this._txtIdx.Location = new System.Drawing.Point(112, 8);
      this._txtIdx.Name = "_txtIdx";
      this._txtIdx.TabIndex = 3;
      this._txtIdx.Text = "";
      this._txtIdx.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblIdx
      // 
      this._lblIdx.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblIdx.Location = new System.Drawing.Point(8, 8);
      this._lblIdx.Name = "_lblIdx";
      this._lblIdx.TabIndex = 2;
      this._lblIdx.Text = "Index:";
      // 
      // _btnOK
      // 
      this._btnOK.Location = new System.Drawing.Point(24, 72);
      this._btnOK.Name = "_btnOK";
      this._btnOK.TabIndex = 40;
      this._btnOK.Text = "OK";
      this._btnOK.Click += new System.EventHandler(this._btnOK_Click);
      // 
      // _btnCancel
      // 
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point(124, 72);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.TabIndex = 41;
      this._btnCancel.Text = "Cancel";
      // 
      // _lblValue
      // 
      this._lblValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblValue.Location = new System.Drawing.Point(8, 36);
      this._lblValue.Name = "_lblValue";
      this._lblValue.TabIndex = 6;
      this._lblValue.Text = "Value:";
      // 
      // _txtValue
      // 
      this._txtValue.Location = new System.Drawing.Point(112, 36);
      this._txtValue.Name = "_txtValue";
      this._txtValue.TabIndex = 7;
      this._txtValue.Text = "";
      this._txtValue.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _ToolTip
      // 
      this._ToolTip.AutoPopDelay = 10000;
      this._ToolTip.InitialDelay = 500;
      this._ToolTip.ReshowDelay = 100;
      // 
      // ScriptEditorSpanConcForm
      // 
      this.AcceptButton = this._btnOK;
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.CancelButton = this._btnCancel;
      this.ClientSize = new System.Drawing.Size(222, 104);
      this.Controls.Add(this._lblValue);
      this.Controls.Add(this._txtValue);
      this.Controls.Add(this._btnCancel);
      this.Controls.Add(this._btnOK);
      this.Controls.Add(this._lblIdx);
      this.Controls.Add(this._txtIdx);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.HelpButton = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ScriptEditorSpanConcForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Span concentrations";
      this.Load += new System.EventHandler(this.SriptEditorSpanConcForm_Load);
      this.ResumeLayout(false);

    }

    #endregion

    #region members

    /// <summary>
    /// The Span concentration index (in [0, 'MAX_SCRIPT_WINDOWS'-1])
    /// </summary>
    int _Idx = 0;

    /// <summary>
    /// The Span concentration value (unit as concentration of the corr'ing substance window)
    /// </summary>
    float _Value = 100.0f;

    #endregion members

    #region events

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void SriptEditorSpanConcForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      try
      {
        // Resources
        this.Text               = r.GetString ( "ScriptEditorSpanConc_Title" );     // "Span concentrations"
        this._btnCancel.Text    = r.GetString ( "ScriptEditorWindow_btnCancel" );   // "Abbruch"
        this._btnOK.Text        = r.GetString ( "ScriptEditorWindow_btnOK" );       // "OK"
        this._lblIdx.Text       = r.GetString ( "ScriptEditorSpanConc_lblIdx" );    // "Index:"
        this._lblValue.Text     = r.GetString ( "ScriptEditorSpanConc_lblValue" );  // "Value:"
      }
      catch
      {
      }

      // Control initialisation
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
      _txtIdx.Text        = _Idx.ToString ();
      _txtValue.Text      = _Value.ToString (nfi);
    }

    /// <summary>
    /// 'Click' event of the 'OK' button
    /// </summary>
    private void _btnOK_Click(object sender, System.EventArgs e)
    {
      object o;

      // Index: in [0, 'MAX_SCRIPT_WINDOWS'-1]
      if ( !Check.Execute ( this._txtIdx, CheckType.Int, CheckRelation.IN, 0, Script.MAX_SCRIPT_WINDOWS-1, true, out o) )
        return;
      _Idx=(int) o;
      // Value: > 0
      if ( !Check.Execute ( this._txtValue, CheckType.Float, CheckRelation.GT, 0, 0, true, out o) )
        return;
      _Value=(float) o;

      // OK
      this.DialogResult = DialogResult.OK;
      this.Close ();
    }
 
    /// <summary>
    /// 'HelpRequested' event of the controls
    /// </summary>
    private void _ctl_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Help requesting control
      Control requestingControl = (Control)sender;
      // Assign help text
      string s = "";
      if      (requestingControl == _txtIdx)
        s = r.GetString ("ScriptEditorSpanConc_Hlp_txtIdx");      // "The index of the corresponding substance window"
      else if (requestingControl == _txtValue)
        s = r.GetString ("ScriptEditorSpanConc_Hlp_txtValue");    // "The Span concentration value of the corresponding substance window";
      
      // Show help
      this._ToolTip.SetToolTip (requestingControl, s);
      hlpevent.Handled=true;
    }

    #endregion events

    #region methods
    #endregion methods

    #region properties

    /// <summary>
    /// The Span concentration index (in [0, 'MAX_SCRIPT_WINDOWS'-1])
    /// </summary>
    public int Index
    {
      get { return _Idx; }
      set { _Idx = value; }
    }

    /// <summary>
    /// The Span concentration value (unit as concentration of the corr'ing substance window)
    /// </summary>
    public float Value
    {
      get { return _Value; }
      set { _Value = value; }
    }
    
    #endregion properties

  }

}
