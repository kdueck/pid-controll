// If defined, the 'Clear' action is processed in the devices 'Script transfer' mode.
// If not defined, the devices 'Script transfer' mode will not be used.
#define SDCARDREADER_CLEAR_WITH_INTERRUPTION

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Text;
using System.IO;
using System.Diagnostics;

using CommRS232;

namespace App
{
	/// <summary>
	/// Class SDcardReaderForm:
	/// GSMReader implementation for SD card (for IUT-built devices)
	/// </summary>
	/// <remarks>
	/// 1.
  /// The RDTFI (Read Text file contents) cmd is implemented here as follows:
  /// First ALL the paths of the text files to be read from device are put into a transfer list.
  /// Then each file of the transfer list is treated sequentielly: 
  /// The 1st file path in the transfer list is put into the TX message list, after it the corr'ing file 
  /// contents are received, they are processed (storage to HD), and the corr'ing entry is removed from 
  /// the transfer list. This procedure is repeated until the transfer list was empty.
  /// The advantage of this approach is clear:
  /// All the processes are synchronized - one thing happens after another has finished. So
  /// a 'non-syncronisation' should not happen, and all is OK.
  /// Thats why we decided TO USE this implementation.
  /// (See remarks for 'SDcardReaderForm_0'.)
  /// 2.
	/// In order to monitor a SD card reinitialisation process a timer component is used. The following
	/// circumstances have to be considered thereby:
	/// The (initially tried) usage of a System.Windows.Forms.Timer didn't work - its timer callback 
	/// was NOT activated by the system after performing the "INDSK_S" (start reinitialisation) command, 
	/// as desired.
	/// Thats why a System.Threading.Timer was used instead - all works fine.
	/// </remarks>
  public class SDcardReaderForm : System.Windows.Forms.Form
  {
    private System.Windows.Forms.GroupBox gpData;
    private System.Windows.Forms.Label lblDataFiles;
    private System.Windows.Forms.ListBox lbDataFiles;
    private System.Windows.Forms.GroupBox gpScript;
    private System.Windows.Forms.ListBox lbScriptFiles;
    private System.Windows.Forms.Label lblScriptFiles;
    private System.Windows.Forms.GroupBox gpScan;
    private System.Windows.Forms.ListBox lbScanFiles;
    private System.Windows.Forms.Label lblScanFiles;
    private System.Windows.Forms.Label lblDataFolders;
    private System.Windows.Forms.ListBox lbDataFolders;
    private System.Windows.Forms.Label lblFileContents;
    private System.Windows.Forms.TextBox txtFileContents;
    private System.Windows.Forms.Button btnAdd;
    private System.Windows.Forms.Button btnRemove;
    private System.Windows.Forms.Label lblAddedFiles;
    private System.Windows.Forms.ListBox lbAddedFiles;
    private System.Windows.Forms.CheckBox chkFilePreview;
    private System.Windows.Forms.Button btnRemoveAll;
    private System.Windows.Forms.Button btnTransfer;
    private System.Windows.Forms.ProgressBar pgbTransfer;
    private System.Windows.Forms.GroupBox gpTransfer;
    private System.Windows.Forms.Button btnStorageFolder;
    private System.Windows.Forms.Label lblStorageFolder;
    private System.Windows.Forms.Label lblTransferRate;
    private System.Windows.Forms.Button btnScriptFiles_Select;
    private System.Windows.Forms.Button btnDataFiles_Select;
    private System.Windows.Forms.Button btnDataFiles_Read;
    private System.Windows.Forms.Button btnScriptFiles_Read;
    private System.Windows.Forms.Button btnScanFiles_Read;
    private System.Windows.Forms.Button btnDataFolders_Read;
    private System.Windows.Forms.GroupBox gpStorageFolder;
    private System.Windows.Forms.Button btnScanFolders_Read;
    private System.Windows.Forms.Label lblScanFolders;
    private System.Windows.Forms.ListBox lbScanFolders;
    private System.Windows.Forms.Button btnScanFiles_Select;
    private System.Windows.Forms.Label lblScriptFolders;
    private System.Windows.Forms.Button btnScriptFolders_Read;
    private System.Windows.Forms.ListBox lbScriptFolders;
    private System.Windows.Forms.GroupBox gpClear;
    private System.Windows.Forms.Button btnClear;
    private System.Windows.Forms.RadioButton rbScript;
    private System.Windows.Forms.RadioButton rbScan;
    private System.Windows.Forms.RadioButton rbData;
    private System.Windows.Forms.Button btnInitDisk;
    private System.Windows.Forms.ProgressBar pgbClear;
    private System.Windows.Forms.GroupBox gpAlarm;
    private System.Windows.Forms.Button btnAlarmFiles_Select;
    private System.Windows.Forms.Button btnAlarmFolders_Read;
    private System.Windows.Forms.Button btnAlarmFiles_Read;
    private System.Windows.Forms.ListBox lbAlarmFiles;
    private System.Windows.Forms.Label lblAlarmFiles;
    private System.Windows.Forms.Label lblAlarmFolders;
    private System.Windows.Forms.ListBox lbAlarmFolders;
    private System.Windows.Forms.RadioButton rbAlarm;
    private System.Windows.Forms.GroupBox gpService;
    private System.Windows.Forms.Button btnServiceFiles_Select;
    private System.Windows.Forms.Button btnServiceFiles_Read;
    private System.Windows.Forms.ListBox lbServiceFiles;
    private System.Windows.Forms.Label lblServiceFiles;
    private System.Windows.Forms.RadioButton rbService;
    private System.Windows.Forms.GroupBox gpError;
    private System.Windows.Forms.Button btnErrorFiles_Select;
    private System.Windows.Forms.Button btnErrorFiles_Read;
    private System.Windows.Forms.ListBox lbErrorFiles;
    private System.Windows.Forms.Label lblErrorFiles;
    private System.Windows.Forms.RadioButton rbErrorLog;
    private System.Windows.Forms.Button btnTransferSDcard;
    private System.Windows.Forms.Timer _TimerPrepSDTrans;
    private System.Windows.Forms.ToolTip _ToolTip;
    private Timer _Timer_Svc;
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Constructor
    /// </summary>
    public SDcardReaderForm()
    {
      InitializeComponent();
      
      // Init.
      Init ();
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      // Dispose monitoring timer
      _Timer.Dispose ();
      
      if( disposing )
      {
        if (components != null) 
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container ();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager (typeof (SDcardReaderForm));
      this.gpData = new System.Windows.Forms.GroupBox ();
      this.btnDataFiles_Select = new System.Windows.Forms.Button ();
      this.btnDataFolders_Read = new System.Windows.Forms.Button ();
      this.btnDataFiles_Read = new System.Windows.Forms.Button ();
      this.lbDataFiles = new System.Windows.Forms.ListBox ();
      this.lblDataFiles = new System.Windows.Forms.Label ();
      this.lblDataFolders = new System.Windows.Forms.Label ();
      this.lbDataFolders = new System.Windows.Forms.ListBox ();
      this.gpScript = new System.Windows.Forms.GroupBox ();
      this.btnScriptFolders_Read = new System.Windows.Forms.Button ();
      this.lblScriptFolders = new System.Windows.Forms.Label ();
      this.lbScriptFolders = new System.Windows.Forms.ListBox ();
      this.btnScriptFiles_Select = new System.Windows.Forms.Button ();
      this.btnScriptFiles_Read = new System.Windows.Forms.Button ();
      this.lbScriptFiles = new System.Windows.Forms.ListBox ();
      this.lblScriptFiles = new System.Windows.Forms.Label ();
      this.gpScan = new System.Windows.Forms.GroupBox ();
      this.btnScanFiles_Select = new System.Windows.Forms.Button ();
      this.btnScanFolders_Read = new System.Windows.Forms.Button ();
      this.lblScanFolders = new System.Windows.Forms.Label ();
      this.lbScanFolders = new System.Windows.Forms.ListBox ();
      this.btnScanFiles_Read = new System.Windows.Forms.Button ();
      this.lbScanFiles = new System.Windows.Forms.ListBox ();
      this.lblScanFiles = new System.Windows.Forms.Label ();
      this.txtFileContents = new System.Windows.Forms.TextBox ();
      this.btnStorageFolder = new System.Windows.Forms.Button ();
      this.lblFileContents = new System.Windows.Forms.Label ();
      this.lblStorageFolder = new System.Windows.Forms.Label ();
      this.btnAdd = new System.Windows.Forms.Button ();
      this.btnRemove = new System.Windows.Forms.Button ();
      this.lblAddedFiles = new System.Windows.Forms.Label ();
      this.lbAddedFiles = new System.Windows.Forms.ListBox ();
      this.chkFilePreview = new System.Windows.Forms.CheckBox ();
      this.btnRemoveAll = new System.Windows.Forms.Button ();
      this.btnTransfer = new System.Windows.Forms.Button ();
      this.pgbTransfer = new System.Windows.Forms.ProgressBar ();
      this.gpTransfer = new System.Windows.Forms.GroupBox ();
      this.btnTransferSDcard = new System.Windows.Forms.Button ();
      this.lblTransferRate = new System.Windows.Forms.Label ();
      this.gpStorageFolder = new System.Windows.Forms.GroupBox ();
      this.gpClear = new System.Windows.Forms.GroupBox ();
      this.rbErrorLog = new System.Windows.Forms.RadioButton ();
      this.rbService = new System.Windows.Forms.RadioButton ();
      this.rbAlarm = new System.Windows.Forms.RadioButton ();
      this.btnClear = new System.Windows.Forms.Button ();
      this.rbScript = new System.Windows.Forms.RadioButton ();
      this.rbScan = new System.Windows.Forms.RadioButton ();
      this.rbData = new System.Windows.Forms.RadioButton ();
      this.btnInitDisk = new System.Windows.Forms.Button ();
      this.pgbClear = new System.Windows.Forms.ProgressBar ();
      this.gpAlarm = new System.Windows.Forms.GroupBox ();
      this.btnAlarmFiles_Select = new System.Windows.Forms.Button ();
      this.btnAlarmFolders_Read = new System.Windows.Forms.Button ();
      this.btnAlarmFiles_Read = new System.Windows.Forms.Button ();
      this.lbAlarmFiles = new System.Windows.Forms.ListBox ();
      this.lblAlarmFiles = new System.Windows.Forms.Label ();
      this.lblAlarmFolders = new System.Windows.Forms.Label ();
      this.lbAlarmFolders = new System.Windows.Forms.ListBox ();
      this.gpService = new System.Windows.Forms.GroupBox ();
      this.btnServiceFiles_Select = new System.Windows.Forms.Button ();
      this.btnServiceFiles_Read = new System.Windows.Forms.Button ();
      this.lbServiceFiles = new System.Windows.Forms.ListBox ();
      this.lblServiceFiles = new System.Windows.Forms.Label ();
      this.gpError = new System.Windows.Forms.GroupBox ();
      this.btnErrorFiles_Select = new System.Windows.Forms.Button ();
      this.btnErrorFiles_Read = new System.Windows.Forms.Button ();
      this.lbErrorFiles = new System.Windows.Forms.ListBox ();
      this.lblErrorFiles = new System.Windows.Forms.Label ();
      this._TimerPrepSDTrans = new System.Windows.Forms.Timer (this.components);
      this._ToolTip = new System.Windows.Forms.ToolTip (this.components);
      this._Timer_Svc = new System.Windows.Forms.Timer (this.components);
      this.gpData.SuspendLayout ();
      this.gpScript.SuspendLayout ();
      this.gpScan.SuspendLayout ();
      this.gpTransfer.SuspendLayout ();
      this.gpStorageFolder.SuspendLayout ();
      this.gpClear.SuspendLayout ();
      this.gpAlarm.SuspendLayout ();
      this.gpService.SuspendLayout ();
      this.gpError.SuspendLayout ();
      this.SuspendLayout ();
      // 
      // gpData
      // 
      this.gpData.Controls.Add (this.btnDataFiles_Select);
      this.gpData.Controls.Add (this.btnDataFolders_Read);
      this.gpData.Controls.Add (this.btnDataFiles_Read);
      this.gpData.Controls.Add (this.lbDataFiles);
      this.gpData.Controls.Add (this.lblDataFiles);
      this.gpData.Controls.Add (this.lblDataFolders);
      this.gpData.Controls.Add (this.lbDataFolders);
      this.gpData.Location = new System.Drawing.Point (8, 8);
      this.gpData.Name = "gpData";
      this.gpData.Size = new System.Drawing.Size (300, 160);
      this.gpData.TabIndex = 1;
      this.gpData.TabStop = false;
      this.gpData.Text = "Data files on device";
      // 
      // btnDataFiles_Select
      // 
      this.btnDataFiles_Select.Location = new System.Drawing.Point (212, 128);
      this.btnDataFiles_Select.Name = "btnDataFiles_Select";
      this.btnDataFiles_Select.Size = new System.Drawing.Size (76, 23);
      this.btnDataFiles_Select.TabIndex = 6;
      this.btnDataFiles_Select.Text = "Select all";
      this.btnDataFiles_Select.Click += new System.EventHandler (this.btnSelect_Click);
      this.btnDataFiles_Select.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnDataFolders_Read
      // 
      this.btnDataFolders_Read.Location = new System.Drawing.Point (24, 128);
      this.btnDataFolders_Read.Name = "btnDataFolders_Read";
      this.btnDataFolders_Read.Size = new System.Drawing.Size (80, 23);
      this.btnDataFolders_Read.TabIndex = 2;
      this.btnDataFolders_Read.Text = "Read";
      this.btnDataFolders_Read.Click += new System.EventHandler (this.btnRead_Click);
      this.btnDataFolders_Read.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnDataFiles_Read
      // 
      this.btnDataFiles_Read.Location = new System.Drawing.Point (128, 128);
      this.btnDataFiles_Read.Name = "btnDataFiles_Read";
      this.btnDataFiles_Read.Size = new System.Drawing.Size (76, 23);
      this.btnDataFiles_Read.TabIndex = 5;
      this.btnDataFiles_Read.Text = "Read";
      this.btnDataFiles_Read.Click += new System.EventHandler (this.btnRead_Click);
      this.btnDataFiles_Read.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lbDataFiles
      // 
      this.lbDataFiles.Location = new System.Drawing.Point (128, 56);
      this.lbDataFiles.Name = "lbDataFiles";
      this.lbDataFiles.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.lbDataFiles.Size = new System.Drawing.Size (160, 69);
      this.lbDataFiles.Sorted = true;
      this.lbDataFiles.TabIndex = 4;
      this.lbDataFiles.SelectedIndexChanged += new System.EventHandler (this.lbFiles_SelectedIndexChanged);
      this.lbDataFiles.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lblDataFiles
      // 
      this.lblDataFiles.Location = new System.Drawing.Point (128, 20);
      this.lblDataFiles.Name = "lblDataFiles";
      this.lblDataFiles.Size = new System.Drawing.Size (160, 36);
      this.lblDataFiles.TabIndex = 3;
      this.lblDataFiles.Text = "Available files:";
      this.lblDataFiles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lblDataFolders
      // 
      this.lblDataFolders.Location = new System.Drawing.Point (8, 20);
      this.lblDataFolders.Name = "lblDataFolders";
      this.lblDataFolders.Size = new System.Drawing.Size (112, 36);
      this.lblDataFolders.TabIndex = 0;
      this.lblDataFolders.Text = "Available subfolders:";
      this.lblDataFolders.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbDataFolders
      // 
      this.lbDataFolders.Location = new System.Drawing.Point (8, 56);
      this.lbDataFolders.Name = "lbDataFolders";
      this.lbDataFolders.Size = new System.Drawing.Size (112, 69);
      this.lbDataFolders.Sorted = true;
      this.lbDataFolders.TabIndex = 1;
      this.lbDataFolders.SelectedIndexChanged += new System.EventHandler (this.lbFolders_SelectedIndexChanged);
      this.lbDataFolders.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // gpScript
      // 
      this.gpScript.Controls.Add (this.btnScriptFolders_Read);
      this.gpScript.Controls.Add (this.lblScriptFolders);
      this.gpScript.Controls.Add (this.lbScriptFolders);
      this.gpScript.Controls.Add (this.btnScriptFiles_Select);
      this.gpScript.Controls.Add (this.btnScriptFiles_Read);
      this.gpScript.Controls.Add (this.lbScriptFiles);
      this.gpScript.Controls.Add (this.lblScriptFiles);
      this.gpScript.Location = new System.Drawing.Point (8, 500);
      this.gpScript.Name = "gpScript";
      this.gpScript.Size = new System.Drawing.Size (300, 160);
      this.gpScript.TabIndex = 4;
      this.gpScript.TabStop = false;
      this.gpScript.Text = "Script files on device";
      // 
      // btnScriptFolders_Read
      // 
      this.btnScriptFolders_Read.Location = new System.Drawing.Point (24, 128);
      this.btnScriptFolders_Read.Name = "btnScriptFolders_Read";
      this.btnScriptFolders_Read.Size = new System.Drawing.Size (80, 23);
      this.btnScriptFolders_Read.TabIndex = 2;
      this.btnScriptFolders_Read.Text = "Read";
      this.btnScriptFolders_Read.Click += new System.EventHandler (this.btnRead_Click);
      this.btnScriptFolders_Read.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lblScriptFolders
      // 
      this.lblScriptFolders.Location = new System.Drawing.Point (8, 20);
      this.lblScriptFolders.Name = "lblScriptFolders";
      this.lblScriptFolders.Size = new System.Drawing.Size (112, 36);
      this.lblScriptFolders.TabIndex = 0;
      this.lblScriptFolders.Text = "Available subfolders:";
      this.lblScriptFolders.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbScriptFolders
      // 
      this.lbScriptFolders.Location = new System.Drawing.Point (8, 56);
      this.lbScriptFolders.Name = "lbScriptFolders";
      this.lbScriptFolders.Size = new System.Drawing.Size (112, 69);
      this.lbScriptFolders.Sorted = true;
      this.lbScriptFolders.TabIndex = 1;
      this.lbScriptFolders.SelectedIndexChanged += new System.EventHandler (this.lbFolders_SelectedIndexChanged);
      this.lbScriptFolders.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnScriptFiles_Select
      // 
      this.btnScriptFiles_Select.Location = new System.Drawing.Point (212, 128);
      this.btnScriptFiles_Select.Name = "btnScriptFiles_Select";
      this.btnScriptFiles_Select.Size = new System.Drawing.Size (76, 23);
      this.btnScriptFiles_Select.TabIndex = 6;
      this.btnScriptFiles_Select.Text = "Select all";
      this.btnScriptFiles_Select.Click += new System.EventHandler (this.btnSelect_Click);
      this.btnScriptFiles_Select.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnScriptFiles_Read
      // 
      this.btnScriptFiles_Read.Location = new System.Drawing.Point (128, 128);
      this.btnScriptFiles_Read.Name = "btnScriptFiles_Read";
      this.btnScriptFiles_Read.Size = new System.Drawing.Size (76, 23);
      this.btnScriptFiles_Read.TabIndex = 5;
      this.btnScriptFiles_Read.Text = "Read";
      this.btnScriptFiles_Read.Click += new System.EventHandler (this.btnRead_Click);
      this.btnScriptFiles_Read.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lbScriptFiles
      // 
      this.lbScriptFiles.Location = new System.Drawing.Point (128, 56);
      this.lbScriptFiles.Name = "lbScriptFiles";
      this.lbScriptFiles.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.lbScriptFiles.Size = new System.Drawing.Size (160, 69);
      this.lbScriptFiles.Sorted = true;
      this.lbScriptFiles.TabIndex = 4;
      this.lbScriptFiles.SelectedIndexChanged += new System.EventHandler (this.lbFiles_SelectedIndexChanged);
      this.lbScriptFiles.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lblScriptFiles
      // 
      this.lblScriptFiles.Location = new System.Drawing.Point (128, 20);
      this.lblScriptFiles.Name = "lblScriptFiles";
      this.lblScriptFiles.Size = new System.Drawing.Size (160, 36);
      this.lblScriptFiles.TabIndex = 3;
      this.lblScriptFiles.Text = "Available files: ";
      this.lblScriptFiles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // gpScan
      // 
      this.gpScan.Controls.Add (this.btnScanFiles_Select);
      this.gpScan.Controls.Add (this.btnScanFolders_Read);
      this.gpScan.Controls.Add (this.lblScanFolders);
      this.gpScan.Controls.Add (this.lbScanFolders);
      this.gpScan.Controls.Add (this.btnScanFiles_Read);
      this.gpScan.Controls.Add (this.lbScanFiles);
      this.gpScan.Controls.Add (this.lblScanFiles);
      this.gpScan.Location = new System.Drawing.Point (8, 336);
      this.gpScan.Name = "gpScan";
      this.gpScan.Size = new System.Drawing.Size (300, 160);
      this.gpScan.TabIndex = 3;
      this.gpScan.TabStop = false;
      this.gpScan.Text = "Scan files on device";
      // 
      // btnScanFiles_Select
      // 
      this.btnScanFiles_Select.Location = new System.Drawing.Point (212, 128);
      this.btnScanFiles_Select.Name = "btnScanFiles_Select";
      this.btnScanFiles_Select.Size = new System.Drawing.Size (76, 23);
      this.btnScanFiles_Select.TabIndex = 6;
      this.btnScanFiles_Select.Text = "Select all";
      this.btnScanFiles_Select.Click += new System.EventHandler (this.btnSelect_Click);
      this.btnScanFiles_Select.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnScanFolders_Read
      // 
      this.btnScanFolders_Read.Location = new System.Drawing.Point (24, 128);
      this.btnScanFolders_Read.Name = "btnScanFolders_Read";
      this.btnScanFolders_Read.Size = new System.Drawing.Size (80, 23);
      this.btnScanFolders_Read.TabIndex = 2;
      this.btnScanFolders_Read.Text = "Read";
      this.btnScanFolders_Read.Click += new System.EventHandler (this.btnRead_Click);
      this.btnScanFolders_Read.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lblScanFolders
      // 
      this.lblScanFolders.Location = new System.Drawing.Point (8, 20);
      this.lblScanFolders.Name = "lblScanFolders";
      this.lblScanFolders.Size = new System.Drawing.Size (112, 36);
      this.lblScanFolders.TabIndex = 0;
      this.lblScanFolders.Text = "Available subfolders:";
      this.lblScanFolders.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbScanFolders
      // 
      this.lbScanFolders.Location = new System.Drawing.Point (8, 56);
      this.lbScanFolders.Name = "lbScanFolders";
      this.lbScanFolders.Size = new System.Drawing.Size (112, 69);
      this.lbScanFolders.Sorted = true;
      this.lbScanFolders.TabIndex = 1;
      this.lbScanFolders.SelectedIndexChanged += new System.EventHandler (this.lbFolders_SelectedIndexChanged);
      this.lbScanFolders.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnScanFiles_Read
      // 
      this.btnScanFiles_Read.Location = new System.Drawing.Point (128, 128);
      this.btnScanFiles_Read.Name = "btnScanFiles_Read";
      this.btnScanFiles_Read.Size = new System.Drawing.Size (76, 24);
      this.btnScanFiles_Read.TabIndex = 5;
      this.btnScanFiles_Read.Text = "Read";
      this.btnScanFiles_Read.Click += new System.EventHandler (this.btnRead_Click);
      this.btnScanFiles_Read.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lbScanFiles
      // 
      this.lbScanFiles.Location = new System.Drawing.Point (128, 56);
      this.lbScanFiles.Name = "lbScanFiles";
      this.lbScanFiles.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.lbScanFiles.Size = new System.Drawing.Size (160, 69);
      this.lbScanFiles.Sorted = true;
      this.lbScanFiles.TabIndex = 4;
      this.lbScanFiles.SelectedIndexChanged += new System.EventHandler (this.lbFiles_SelectedIndexChanged);
      this.lbScanFiles.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lblScanFiles
      // 
      this.lblScanFiles.Location = new System.Drawing.Point (128, 20);
      this.lblScanFiles.Name = "lblScanFiles";
      this.lblScanFiles.Size = new System.Drawing.Size (160, 36);
      this.lblScanFiles.TabIndex = 3;
      this.lblScanFiles.Text = "Available files:";
      this.lblScanFiles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txtFileContents
      // 
      this.txtFileContents.Location = new System.Drawing.Point (8, 232);
      this.txtFileContents.Multiline = true;
      this.txtFileContents.Name = "txtFileContents";
      this.txtFileContents.ReadOnly = true;
      this.txtFileContents.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this.txtFileContents.Size = new System.Drawing.Size (424, 114);
      this.txtFileContents.TabIndex = 11;
      this.txtFileContents.WordWrap = false;
      this.txtFileContents.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnStorageFolder
      // 
      this.btnStorageFolder.Image = ((System.Drawing.Image)(resources.GetObject ("btnStorageFolder.Image")));
      this.btnStorageFolder.Location = new System.Drawing.Point (412, 24);
      this.btnStorageFolder.Name = "btnStorageFolder";
      this.btnStorageFolder.Size = new System.Drawing.Size (22, 23);
      this.btnStorageFolder.TabIndex = 1;
      this.btnStorageFolder.Click += new System.EventHandler (this.btnStorageFolder_Click);
      this.btnStorageFolder.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lblFileContents
      // 
      this.lblFileContents.Location = new System.Drawing.Point (12, 206);
      this.lblFileContents.Name = "lblFileContents";
      this.lblFileContents.Size = new System.Drawing.Size (420, 24);
      this.lblFileContents.TabIndex = 10;
      this.lblFileContents.Text = "File contents:";
      this.lblFileContents.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lblStorageFolder
      // 
      this.lblStorageFolder.Location = new System.Drawing.Point (8, 24);
      this.lblStorageFolder.Name = "lblStorageFolder";
      this.lblStorageFolder.Size = new System.Drawing.Size (396, 23);
      this.lblStorageFolder.TabIndex = 0;
      this.lblStorageFolder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btnAdd
      // 
      this.btnAdd.Font = new System.Drawing.Font ("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnAdd.Location = new System.Drawing.Point (8, 148);
      this.btnAdd.Name = "btnAdd";
      this.btnAdd.Size = new System.Drawing.Size (136, 24);
      this.btnAdd.TabIndex = 3;
      this.btnAdd.Text = "Add selected files";
      this.btnAdd.Click += new System.EventHandler (this.btnAdd_Click);
      this.btnAdd.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnRemove
      // 
      this.btnRemove.Font = new System.Drawing.Font ("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnRemove.Location = new System.Drawing.Point (154, 148);
      this.btnRemove.Name = "btnRemove";
      this.btnRemove.Size = new System.Drawing.Size (131, 24);
      this.btnRemove.TabIndex = 4;
      this.btnRemove.Text = "Remove selected file";
      this.btnRemove.Click += new System.EventHandler (this.btnRemove_Click);
      this.btnRemove.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lblAddedFiles
      // 
      this.lblAddedFiles.Location = new System.Drawing.Point (8, 20);
      this.lblAddedFiles.Name = "lblAddedFiles";
      this.lblAddedFiles.Size = new System.Drawing.Size (276, 24);
      this.lblAddedFiles.TabIndex = 0;
      this.lblAddedFiles.Text = "Device files to be transferred:";
      this.lblAddedFiles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbAddedFiles
      // 
      this.lbAddedFiles.Location = new System.Drawing.Point (8, 48);
      this.lbAddedFiles.Name = "lbAddedFiles";
      this.lbAddedFiles.Size = new System.Drawing.Size (424, 95);
      this.lbAddedFiles.TabIndex = 1;
      this.lbAddedFiles.SelectedIndexChanged += new System.EventHandler (this.lbAddedFiles_SelectedIndexChanged);
      this.lbAddedFiles.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // chkFilePreview
      // 
      this.chkFilePreview.Location = new System.Drawing.Point (296, 20);
      this.chkFilePreview.Name = "chkFilePreview";
      this.chkFilePreview.Size = new System.Drawing.Size (136, 24);
      this.chkFilePreview.TabIndex = 2;
      this.chkFilePreview.Text = "File preview";
      this.chkFilePreview.CheckedChanged += new System.EventHandler (this.chkFilePreview_CheckedChanged);
      this.chkFilePreview.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnRemoveAll
      // 
      this.btnRemoveAll.Font = new System.Drawing.Font ("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnRemoveAll.Location = new System.Drawing.Point (296, 148);
      this.btnRemoveAll.Name = "btnRemoveAll";
      this.btnRemoveAll.Size = new System.Drawing.Size (136, 24);
      this.btnRemoveAll.TabIndex = 5;
      this.btnRemoveAll.Text = "Remove all files";
      this.btnRemoveAll.Click += new System.EventHandler (this.btnRemoveAll_Click);
      this.btnRemoveAll.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnTransfer
      // 
      this.btnTransfer.BackColor = System.Drawing.SystemColors.Control;
      this.btnTransfer.ForeColor = System.Drawing.SystemColors.ControlText;
      this.btnTransfer.Location = new System.Drawing.Point (8, 178);
      this.btnTransfer.Name = "btnTransfer";
      this.btnTransfer.Size = new System.Drawing.Size (114, 24);
      this.btnTransfer.TabIndex = 6;
      this.btnTransfer.Text = "Transfer";
      this.btnTransfer.UseVisualStyleBackColor = false;
      this.btnTransfer.Click += new System.EventHandler (this.btnTransfer_Click);
      this.btnTransfer.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // pgbTransfer
      // 
      this.pgbTransfer.Location = new System.Drawing.Point (246, 178);
      this.pgbTransfer.Name = "pgbTransfer";
      this.pgbTransfer.Size = new System.Drawing.Size (132, 22);
      this.pgbTransfer.TabIndex = 8;
      // 
      // gpTransfer
      // 
      this.gpTransfer.Controls.Add (this.btnTransferSDcard);
      this.gpTransfer.Controls.Add (this.lblTransferRate);
      this.gpTransfer.Controls.Add (this.txtFileContents);
      this.gpTransfer.Controls.Add (this.lblFileContents);
      this.gpTransfer.Controls.Add (this.lbAddedFiles);
      this.gpTransfer.Controls.Add (this.lblAddedFiles);
      this.gpTransfer.Controls.Add (this.chkFilePreview);
      this.gpTransfer.Controls.Add (this.btnAdd);
      this.gpTransfer.Controls.Add (this.btnRemove);
      this.gpTransfer.Controls.Add (this.btnRemoveAll);
      this.gpTransfer.Controls.Add (this.btnTransfer);
      this.gpTransfer.Controls.Add (this.pgbTransfer);
      this.gpTransfer.Location = new System.Drawing.Point (316, 68);
      this.gpTransfer.Name = "gpTransfer";
      this.gpTransfer.Size = new System.Drawing.Size (444, 352);
      this.gpTransfer.TabIndex = 7;
      this.gpTransfer.TabStop = false;
      this.gpTransfer.Text = "File transfer from device to PC";
      // 
      // btnTransferSDcard
      // 
      this.btnTransferSDcard.Font = new System.Drawing.Font ("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnTransferSDcard.Location = new System.Drawing.Point (128, 178);
      this.btnTransferSDcard.Name = "btnTransferSDcard";
      this.btnTransferSDcard.Size = new System.Drawing.Size (114, 24);
      this.btnTransferSDcard.TabIndex = 7;
      this.btnTransferSDcard.Text = "Transfer SD-Karte";
      this.btnTransferSDcard.Click += new System.EventHandler (this.btnTransferSDcard_Click);
      this.btnTransferSDcard.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lblTransferRate
      // 
      this.lblTransferRate.Location = new System.Drawing.Point (384, 178);
      this.lblTransferRate.Name = "lblTransferRate";
      this.lblTransferRate.Size = new System.Drawing.Size (56, 22);
      this.lblTransferRate.TabIndex = 9;
      this.lblTransferRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // gpStorageFolder
      // 
      this.gpStorageFolder.Controls.Add (this.btnStorageFolder);
      this.gpStorageFolder.Controls.Add (this.lblStorageFolder);
      this.gpStorageFolder.Location = new System.Drawing.Point (316, 8);
      this.gpStorageFolder.Name = "gpStorageFolder";
      this.gpStorageFolder.Size = new System.Drawing.Size (444, 56);
      this.gpStorageFolder.TabIndex = 0;
      this.gpStorageFolder.TabStop = false;
      this.gpStorageFolder.Text = "Storage folder";
      // 
      // gpClear
      // 
      this.gpClear.Controls.Add (this.rbErrorLog);
      this.gpClear.Controls.Add (this.rbService);
      this.gpClear.Controls.Add (this.rbAlarm);
      this.gpClear.Controls.Add (this.btnClear);
      this.gpClear.Controls.Add (this.rbScript);
      this.gpClear.Controls.Add (this.rbScan);
      this.gpClear.Controls.Add (this.rbData);
      this.gpClear.Controls.Add (this.btnInitDisk);
      this.gpClear.Controls.Add (this.pgbClear);
      this.gpClear.Location = new System.Drawing.Point (316, 424);
      this.gpClear.Name = "gpClear";
      this.gpClear.Size = new System.Drawing.Size (444, 100);
      this.gpClear.TabIndex = 8;
      this.gpClear.TabStop = false;
      this.gpClear.Text = "Clear  device storage";
      // 
      // rbErrorLog
      // 
      this.rbErrorLog.Location = new System.Drawing.Point (368, 18);
      this.rbErrorLog.Name = "rbErrorLog";
      this.rbErrorLog.Size = new System.Drawing.Size (70, 24);
      this.rbErrorLog.TabIndex = 5;
      this.rbErrorLog.TabStop = true;
      this.rbErrorLog.Text = "Error";
      this.rbErrorLog.CheckedChanged += new System.EventHandler (this.rbClear_CheckedChanged);
      // 
      // rbService
      // 
      this.rbService.Location = new System.Drawing.Point (296, 18);
      this.rbService.Name = "rbService";
      this.rbService.Size = new System.Drawing.Size (70, 24);
      this.rbService.TabIndex = 4;
      this.rbService.TabStop = true;
      this.rbService.Text = "Service";
      this.rbService.CheckedChanged += new System.EventHandler (this.rbClear_CheckedChanged);
      // 
      // rbAlarm
      // 
      this.rbAlarm.Location = new System.Drawing.Point (80, 18);
      this.rbAlarm.Name = "rbAlarm";
      this.rbAlarm.Size = new System.Drawing.Size (70, 24);
      this.rbAlarm.TabIndex = 1;
      this.rbAlarm.TabStop = true;
      this.rbAlarm.Text = "Alarm";
      this.rbAlarm.CheckedChanged += new System.EventHandler (this.rbClear_CheckedChanged);
      // 
      // btnClear
      // 
      this.btnClear.Location = new System.Drawing.Point (26, 48);
      this.btnClear.Name = "btnClear";
      this.btnClear.Size = new System.Drawing.Size (140, 23);
      this.btnClear.TabIndex = 6;
      this.btnClear.Text = "Clear";
      this.btnClear.Click += new System.EventHandler (this.btnClear_Click);
      this.btnClear.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // rbScript
      // 
      this.rbScript.Location = new System.Drawing.Point (224, 18);
      this.rbScript.Name = "rbScript";
      this.rbScript.Size = new System.Drawing.Size (70, 24);
      this.rbScript.TabIndex = 3;
      this.rbScript.TabStop = true;
      this.rbScript.Text = "Script";
      this.rbScript.CheckedChanged += new System.EventHandler (this.rbClear_CheckedChanged);
      // 
      // rbScan
      // 
      this.rbScan.Location = new System.Drawing.Point (152, 18);
      this.rbScan.Name = "rbScan";
      this.rbScan.Size = new System.Drawing.Size (70, 24);
      this.rbScan.TabIndex = 2;
      this.rbScan.TabStop = true;
      this.rbScan.Text = "Scan";
      this.rbScan.CheckedChanged += new System.EventHandler (this.rbClear_CheckedChanged);
      // 
      // rbData
      // 
      this.rbData.Location = new System.Drawing.Point (8, 18);
      this.rbData.Name = "rbData";
      this.rbData.Size = new System.Drawing.Size (70, 24);
      this.rbData.TabIndex = 0;
      this.rbData.TabStop = true;
      this.rbData.Text = "Resultate";
      this.rbData.CheckedChanged += new System.EventHandler (this.rbClear_CheckedChanged);
      // 
      // btnInitDisk
      // 
      this.btnInitDisk.Location = new System.Drawing.Point (280, 48);
      this.btnInitDisk.Name = "btnInitDisk";
      this.btnInitDisk.Size = new System.Drawing.Size (140, 23);
      this.btnInitDisk.TabIndex = 7;
      this.btnInitDisk.Text = "SD-Karte reinitialisieren";
      this.btnInitDisk.Click += new System.EventHandler (this.btnInitDisk_Click);
      this.btnInitDisk.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // pgbClear
      // 
      this.pgbClear.Location = new System.Drawing.Point (8, 74);
      this.pgbClear.Name = "pgbClear";
      this.pgbClear.Size = new System.Drawing.Size (430, 20);
      this.pgbClear.TabIndex = 8;
      // 
      // gpAlarm
      // 
      this.gpAlarm.Controls.Add (this.btnAlarmFiles_Select);
      this.gpAlarm.Controls.Add (this.btnAlarmFolders_Read);
      this.gpAlarm.Controls.Add (this.btnAlarmFiles_Read);
      this.gpAlarm.Controls.Add (this.lbAlarmFiles);
      this.gpAlarm.Controls.Add (this.lblAlarmFiles);
      this.gpAlarm.Controls.Add (this.lblAlarmFolders);
      this.gpAlarm.Controls.Add (this.lbAlarmFolders);
      this.gpAlarm.Location = new System.Drawing.Point (8, 172);
      this.gpAlarm.Name = "gpAlarm";
      this.gpAlarm.Size = new System.Drawing.Size (300, 160);
      this.gpAlarm.TabIndex = 2;
      this.gpAlarm.TabStop = false;
      this.gpAlarm.Text = "Alarm files on device";
      // 
      // btnAlarmFiles_Select
      // 
      this.btnAlarmFiles_Select.Location = new System.Drawing.Point (212, 128);
      this.btnAlarmFiles_Select.Name = "btnAlarmFiles_Select";
      this.btnAlarmFiles_Select.Size = new System.Drawing.Size (76, 23);
      this.btnAlarmFiles_Select.TabIndex = 6;
      this.btnAlarmFiles_Select.Text = "Select all";
      this.btnAlarmFiles_Select.Click += new System.EventHandler (this.btnSelect_Click);
      this.btnAlarmFiles_Select.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnAlarmFolders_Read
      // 
      this.btnAlarmFolders_Read.Location = new System.Drawing.Point (24, 128);
      this.btnAlarmFolders_Read.Name = "btnAlarmFolders_Read";
      this.btnAlarmFolders_Read.Size = new System.Drawing.Size (80, 23);
      this.btnAlarmFolders_Read.TabIndex = 2;
      this.btnAlarmFolders_Read.Text = "Read";
      this.btnAlarmFolders_Read.Click += new System.EventHandler (this.btnRead_Click);
      this.btnAlarmFolders_Read.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnAlarmFiles_Read
      // 
      this.btnAlarmFiles_Read.Location = new System.Drawing.Point (128, 128);
      this.btnAlarmFiles_Read.Name = "btnAlarmFiles_Read";
      this.btnAlarmFiles_Read.Size = new System.Drawing.Size (76, 23);
      this.btnAlarmFiles_Read.TabIndex = 5;
      this.btnAlarmFiles_Read.Text = "Read";
      this.btnAlarmFiles_Read.Click += new System.EventHandler (this.btnRead_Click);
      this.btnAlarmFiles_Read.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lbAlarmFiles
      // 
      this.lbAlarmFiles.Location = new System.Drawing.Point (128, 56);
      this.lbAlarmFiles.Name = "lbAlarmFiles";
      this.lbAlarmFiles.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.lbAlarmFiles.Size = new System.Drawing.Size (160, 69);
      this.lbAlarmFiles.Sorted = true;
      this.lbAlarmFiles.TabIndex = 4;
      this.lbAlarmFiles.SelectedIndexChanged += new System.EventHandler (this.lbFiles_SelectedIndexChanged);
      this.lbAlarmFiles.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lblAlarmFiles
      // 
      this.lblAlarmFiles.Location = new System.Drawing.Point (128, 20);
      this.lblAlarmFiles.Name = "lblAlarmFiles";
      this.lblAlarmFiles.Size = new System.Drawing.Size (160, 36);
      this.lblAlarmFiles.TabIndex = 3;
      this.lblAlarmFiles.Text = "Available files:";
      this.lblAlarmFiles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lblAlarmFolders
      // 
      this.lblAlarmFolders.Location = new System.Drawing.Point (8, 20);
      this.lblAlarmFolders.Name = "lblAlarmFolders";
      this.lblAlarmFolders.Size = new System.Drawing.Size (112, 36);
      this.lblAlarmFolders.TabIndex = 0;
      this.lblAlarmFolders.Text = "Available subfolders:";
      this.lblAlarmFolders.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbAlarmFolders
      // 
      this.lbAlarmFolders.Location = new System.Drawing.Point (8, 56);
      this.lbAlarmFolders.Name = "lbAlarmFolders";
      this.lbAlarmFolders.Size = new System.Drawing.Size (112, 69);
      this.lbAlarmFolders.Sorted = true;
      this.lbAlarmFolders.TabIndex = 1;
      this.lbAlarmFolders.SelectedIndexChanged += new System.EventHandler (this.lbFolders_SelectedIndexChanged);
      this.lbAlarmFolders.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // gpService
      // 
      this.gpService.Controls.Add (this.btnServiceFiles_Select);
      this.gpService.Controls.Add (this.btnServiceFiles_Read);
      this.gpService.Controls.Add (this.lbServiceFiles);
      this.gpService.Controls.Add (this.lblServiceFiles);
      this.gpService.Location = new System.Drawing.Point (316, 528);
      this.gpService.Name = "gpService";
      this.gpService.Size = new System.Drawing.Size (216, 132);
      this.gpService.TabIndex = 5;
      this.gpService.TabStop = false;
      this.gpService.Text = "Service files on device";
      // 
      // btnServiceFiles_Select
      // 
      this.btnServiceFiles_Select.Location = new System.Drawing.Point (116, 102);
      this.btnServiceFiles_Select.Name = "btnServiceFiles_Select";
      this.btnServiceFiles_Select.Size = new System.Drawing.Size (76, 23);
      this.btnServiceFiles_Select.TabIndex = 3;
      this.btnServiceFiles_Select.Text = "Select all";
      this.btnServiceFiles_Select.Click += new System.EventHandler (this.btnSelect_Click);
      this.btnServiceFiles_Select.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnServiceFiles_Read
      // 
      this.btnServiceFiles_Read.Location = new System.Drawing.Point (32, 102);
      this.btnServiceFiles_Read.Name = "btnServiceFiles_Read";
      this.btnServiceFiles_Read.Size = new System.Drawing.Size (76, 23);
      this.btnServiceFiles_Read.TabIndex = 2;
      this.btnServiceFiles_Read.Text = "Read";
      this.btnServiceFiles_Read.Click += new System.EventHandler (this.btnRead_Click);
      this.btnServiceFiles_Read.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lbServiceFiles
      // 
      this.lbServiceFiles.Location = new System.Drawing.Point (8, 42);
      this.lbServiceFiles.Name = "lbServiceFiles";
      this.lbServiceFiles.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.lbServiceFiles.Size = new System.Drawing.Size (204, 56);
      this.lbServiceFiles.Sorted = true;
      this.lbServiceFiles.TabIndex = 1;
      this.lbServiceFiles.SelectedIndexChanged += new System.EventHandler (this.lbFiles_SelectedIndexChanged);
      this.lbServiceFiles.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lblServiceFiles
      // 
      this.lblServiceFiles.Location = new System.Drawing.Point (8, 20);
      this.lblServiceFiles.Name = "lblServiceFiles";
      this.lblServiceFiles.Size = new System.Drawing.Size (204, 20);
      this.lblServiceFiles.TabIndex = 0;
      this.lblServiceFiles.Text = "Available files: ";
      this.lblServiceFiles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // gpError
      // 
      this.gpError.Controls.Add (this.btnErrorFiles_Select);
      this.gpError.Controls.Add (this.btnErrorFiles_Read);
      this.gpError.Controls.Add (this.lbErrorFiles);
      this.gpError.Controls.Add (this.lblErrorFiles);
      this.gpError.Location = new System.Drawing.Point (540, 528);
      this.gpError.Name = "gpError";
      this.gpError.Size = new System.Drawing.Size (220, 132);
      this.gpError.TabIndex = 6;
      this.gpError.TabStop = false;
      this.gpError.Text = "Error files on device";
      // 
      // btnErrorFiles_Select
      // 
      this.btnErrorFiles_Select.Location = new System.Drawing.Point (116, 102);
      this.btnErrorFiles_Select.Name = "btnErrorFiles_Select";
      this.btnErrorFiles_Select.Size = new System.Drawing.Size (76, 23);
      this.btnErrorFiles_Select.TabIndex = 3;
      this.btnErrorFiles_Select.Text = "Select all";
      this.btnErrorFiles_Select.Click += new System.EventHandler (this.btnSelect_Click);
      this.btnErrorFiles_Select.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnErrorFiles_Read
      // 
      this.btnErrorFiles_Read.Location = new System.Drawing.Point (32, 102);
      this.btnErrorFiles_Read.Name = "btnErrorFiles_Read";
      this.btnErrorFiles_Read.Size = new System.Drawing.Size (76, 23);
      this.btnErrorFiles_Read.TabIndex = 2;
      this.btnErrorFiles_Read.Text = "Read";
      this.btnErrorFiles_Read.Click += new System.EventHandler (this.btnRead_Click);
      this.btnErrorFiles_Read.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lbErrorFiles
      // 
      this.lbErrorFiles.Location = new System.Drawing.Point (8, 42);
      this.lbErrorFiles.Name = "lbErrorFiles";
      this.lbErrorFiles.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.lbErrorFiles.Size = new System.Drawing.Size (204, 56);
      this.lbErrorFiles.Sorted = true;
      this.lbErrorFiles.TabIndex = 1;
      this.lbErrorFiles.SelectedIndexChanged += new System.EventHandler (this.lbFiles_SelectedIndexChanged);
      this.lbErrorFiles.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lblErrorFiles
      // 
      this.lblErrorFiles.Location = new System.Drawing.Point (8, 20);
      this.lblErrorFiles.Name = "lblErrorFiles";
      this.lblErrorFiles.Size = new System.Drawing.Size (204, 20);
      this.lblErrorFiles.TabIndex = 0;
      this.lblErrorFiles.Text = "Available files: ";
      this.lblErrorFiles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // _TimerPrepSDTrans
      // 
      this._TimerPrepSDTrans.Tick += new System.EventHandler (this._TimerPrepSDTrans_Tick);
      // 
      // _ToolTip
      // 
      this._ToolTip.AutoPopDelay = 10000;
      this._ToolTip.InitialDelay = 500;
      this._ToolTip.ReshowDelay = 100;
      // 
      // _Timer_Svc
      // 
      this._Timer_Svc.Interval = 1000;
      this._Timer_Svc.Tick += new System.EventHandler (this._Timer_Svc_Tick);
      // 
      // SDcardReaderForm
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size (5, 13);
      this.ClientSize = new System.Drawing.Size (766, 668);
      this.Controls.Add (this.gpError);
      this.Controls.Add (this.gpService);
      this.Controls.Add (this.gpAlarm);
      this.Controls.Add (this.gpClear);
      this.Controls.Add (this.gpStorageFolder);
      this.Controls.Add (this.gpTransfer);
      this.Controls.Add (this.gpScan);
      this.Controls.Add (this.gpScript);
      this.Controls.Add (this.gpData);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.HelpButton = true;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "SDcardReaderForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Read SD card";
      this.Closed += new System.EventHandler (this.SDcardReaderForm_Closed);
      this.Load += new System.EventHandler (this.SDcardReaderForm_Load);
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler (this.SDcardReaderForm_KeyPress);
      this.gpData.ResumeLayout (false);
      this.gpScript.ResumeLayout (false);
      this.gpScan.ResumeLayout (false);
      this.gpTransfer.ResumeLayout (false);
      this.gpTransfer.PerformLayout ();
      this.gpStorageFolder.ResumeLayout (false);
      this.gpClear.ResumeLayout (false);
      this.gpAlarm.ResumeLayout (false);
      this.gpService.ResumeLayout (false);
      this.gpError.ResumeLayout (false);
      this.ResumeLayout (false);

    }

    #endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form 
    /// </summary>
    private void SDcardReaderForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Resources
      //    Initial Label strings
      _sInitialLblFoldersTxt          = r.GetString ("SDcardReader_Txt_AvailSubfol");   // "Available subfolders:";
      _sInitialLblFilesTxt            = r.GetString ("SDcardReader_Txt_AvailFiles");    // "Available files:";
      _sInitiallblFileContentsTxt     = r.GetString ("SDcardReader_Txt_FileCont");      // "File contents:";
      //    Select Buttons      
      _sBTN_TEXT_SELECT_ALL           = r.GetString ("SDcardReader_Txt_SelectAll");     // "Select all"
      _sBTN_TEXT_UNSELECT_ALL         = r.GetString ("SDcardReader_Txt_UnselectAll");   // "Unselect all"
      //    Controls
      this.Text                       = r.GetString ( "SDcardReader_Title" );           // "Read SDcard"

      this.gpData.Text                = r.GetString ( "SDcardReader_gpData" );          // "Data files on device"
      this.lblDataFolders.Text        = _sInitialLblFoldersTxt;                         // "Available subfolders"
      this.btnDataFolders_Read.Text   = r.GetString ( "SDcardReader_btnRead" );         // "Read"
      this.lblDataFiles.Text          = _sInitialLblFilesTxt;                           // "Available files:";
      this.btnDataFiles_Read.Text     = r.GetString ( "SDcardReader_btnRead" );         // "Read"
      this.btnDataFiles_Select.Text   = _sBTN_TEXT_SELECT_ALL;                          // "Select all"

      this.gpAlarm.Text               = r.GetString ( "SDcardReader_gpAlarm" );         // "Alarm files on device"
      this.lblAlarmFolders.Text        = _sInitialLblFoldersTxt;                         // "Available subfolders"
      this.btnAlarmFolders_Read.Text   = r.GetString ( "SDcardReader_btnRead" );         // "Read"
      this.lblAlarmFiles.Text          = _sInitialLblFilesTxt;                           // "Available files:";
      this.btnAlarmFiles_Read.Text     = r.GetString ( "SDcardReader_btnRead" );         // "Read"
      this.btnAlarmFiles_Select.Text   = _sBTN_TEXT_SELECT_ALL;                          // "Select all"
      
      this.gpScan.Text                = r.GetString ( "SDcardReader_gpScan" );          // "Scan files on device"
      this.lblScanFolders.Text        = _sInitialLblFoldersTxt;                         // "Available subfolders"
      this.btnScanFolders_Read.Text   = r.GetString ( "SDcardReader_btnRead" );         // "Read"
      this.lblScanFiles.Text          = _sInitialLblFilesTxt;                           // "Available files:";
      this.btnScanFiles_Read.Text     = r.GetString ( "SDcardReader_btnRead" );         // "Read"
      this.btnScanFiles_Select.Text   = _sBTN_TEXT_SELECT_ALL;                          // "Select all"

      this.gpScript.Text              = r.GetString ( "SDcardReader_gpScript" );        // "Script files on device"
      this.lblScriptFolders.Text      = _sInitialLblFoldersTxt;                         // "Available subfolders"
      this.btnScriptFolders_Read.Text = r.GetString ( "SDcardReader_btnRead" );         // "Read"
      this.lblScriptFiles.Text        = _sInitialLblFilesTxt;                           // "Available files:";
      this.btnScriptFiles_Read.Text   = r.GetString ( "SDcardReader_btnRead" );         // "Read"
      this.btnScriptFiles_Select.Text = _sBTN_TEXT_SELECT_ALL;                          // "Select all"

      this.gpService.Text             = r.GetString ( "SDcardReader_gpService" );       // "Service files on device"
      this.lblServiceFiles.Text       = _sInitialLblFilesTxt;                           // "Available files:";
      this.btnServiceFiles_Read.Text  = r.GetString ( "SDcardReader_btnRead" );         // "Read"
      this.btnServiceFiles_Select.Text= _sBTN_TEXT_SELECT_ALL;                          // "Select all"

      this.gpError.Text               = r.GetString ( "SDcardReader_gpError" );         // "Error files on device"
      this.lblErrorFiles.Text         = _sInitialLblFilesTxt;                           // "Available files:";
      this.btnErrorFiles_Read.Text    = r.GetString ( "SDcardReader_btnRead" );         // "Read"
      this.btnErrorFiles_Select.Text  = _sBTN_TEXT_SELECT_ALL;                          // "Select all"
      
      this.gpStorageFolder.Text       = r.GetString ( "SDcardReader_gpStorageFolder" ); // "Storage folder"  
      
      this.gpTransfer.Text            = r.GetString ( "SDcardReader_gpTransfer" );      // "File transfer from device to PC"
      this.lblAddedFiles.Text         = r.GetString ( "SDcardReader_lblAddedFiles" );   // "Device files to be transferred"
      this.chkFilePreview.Text        = r.GetString ( "SDcardReader_chkFilePreview" );  // "File preview"
      this.btnAdd.Text                = r.GetString ( "SDcardReader_btnAdd" );          // "Add selected files"
      this.btnRemove.Text             = r.GetString ( "SDcardReader_btnRemove" );       // "Remove selected file"
      this.btnRemoveAll.Text          = r.GetString ( "SDcardReader_btnRemoveAll" );    // "Remove all files"
      this.btnTransfer.Text           = r.GetString ( "SDcardReader_btnTransfer" );     // "Transfer"
      this.btnTransferSDcard.Text     = r.GetString ( "SDcardReader_btnTransferSDcard" ); // "Transfer SD card"
      this.lblFileContents.Text       = _sInitiallblFileContentsTxt;                    // "File contents:";
      
      this.gpClear.Text               = r.GetString ( "SDcardReader_gpClear" );         // "Clear device storage"
      this.rbData.Text                = r.GetString ( "SDcardReader_rbData" );          // "Data"
      this.rbAlarm.Text               = r.GetString ( "SDcardReader_rbAlarm" );         // "Alarm"
      this.rbScan.Text                = r.GetString ( "SDcardReader_rbScan" );          // "Scan"
      this.rbScript.Text              = r.GetString ( "SDcardReader_rbScript" );        // "Script"
      this.rbService.Text             = r.GetString ( "SDcardReader_rbService" );       // "Service"
      this.rbErrorLog.Text            = r.GetString ( "SDcardReader_rbErrorLog" );      // "Error"
      
      this.btnClear.Text              = r.GetString ( "SDcardReader_btnClear" );        // "Clear"
      this.btnInitDisk.Text           = r.GetString ( "SDcardReader_btnInitDisk" );     // "SD-Karte reinitialisieren"

      // Display SDcard data storage folder, if available
      if ( app.Data.Common.sSDcardFolder.Length > 0 ) 
      {
        this.lblStorageFolder.Text = app.Data.Common.sSDcardFolder;
      }
      
      // Update the form
      UpdateData ();

      // Request the device root folder paths
      RequestDeviceRootFolders ();

      // Enable Service file timer
      this._Timer_Svc.Enabled = true;
    }

    /// <summary>
    /// 'Closed' event of the form 
    /// </summary>
    private void SDcardReaderForm_Closed(object sender, System.EventArgs e)
    {
      App app = App.Instance;

      // Stop timer
      this._Timer.Enabled = false;              // monitoring timer
      this._TimerPrepSDTrans.Enabled = false;   // SDcard transfer preparation timer
      this._Timer_Svc.Enabled = false;          // Service file timer
   
      // Write app data: SDcard data storage folder
      app.Data.Common.sSDcardFolder = this.lblStorageFolder.Text;
      app.Data.Write ();
      
      // Check: Is a Transfer process still in progress?
      if ( _bTransferInProgress ) 
      {
        // Yes:
        // Check: Did we already get the name of the script, that was lastly running?
        if ( _sLastScriptName.Length > 0 ) 
        {
          // Yes:
          // Finish the transfer
          _EndTransfer ( );
        }
      }
    }
  
    /// <summary>
    /// 'KeyPress' event of the form
    /// </summary>
    private void SDcardReaderForm_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
    {
      // Close the form on pressing 'Escape'
      if ( e.KeyChar == (char) Keys.Escape )
        this.Close ();
    }
    
    /// <summary>
    /// 'Click' event of the 'Storage folder' Button control
    /// </summary>
    private void btnStorageFolder_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // FolderBrowser dialog
      FolderBrowserDialog dlg = new FolderBrowserDialog ();
      
      dlg.Description  = r.GetString ("SDcardReader_Txt_SelectStorageFolder"); // "Bitte w�hlen Sie den Ordner zur Speicherung der SD-Kartendaten"
      dlg.ShowNewFolderButton = true;
      dlg.SelectedPath = this.lblStorageFolder.Text;

      if (DialogResult.OK == dlg.ShowDialog ())
      {
        // Overtake the storage folder
        this.lblStorageFolder.Text = dlg.SelectedPath;
      }
      dlg.Dispose ();
    }

    /// <summary>
    /// 'Click' event of the 'Data/Alarm/Script/Scan/Service - Read' Button controls 
    /// </summary>
    private void btnRead_Click(object sender, System.EventArgs e)
    {
      string sMsg, sCap;
      App app = App.Instance;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // Build the message

      // --------------------------------------
      // Folders
      
      if      (sender == this.btnDataFolders_Read)   
      {
        // The 'Data (sub)folders - Read' Button was pressed:
        // Read Data (sub)folders: "RDDATFOS\r"
        CommMessage msg = new CommMessage (comm.msgReadDataFolders);
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
      else if (sender == this.btnAlarmFolders_Read)   
      {
        // The 'Alarm (sub)folders - Read' Button was pressed:
        // Read Alarm (sub)folders: "RDALFOS\r"
        CommMessage msg = new CommMessage (comm.msgReadAlarmFolders);
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
      else if (sender == this.btnScanFolders_Read)     
      {
        // The 'Scan (sub)folders - Read' Button was pressed:
        // Read Scan (sub)folders: "RDSCNFOS\r"
        CommMessage msg = new CommMessage (comm.msgReadScanFolders);
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }      
      else if (sender == this.btnScriptFolders_Read)     
      {
        // The 'Script (sub)folders - Read' Button was pressed:
        // Read Script (sub)folders: "RDSCRFOS\r"
        CommMessage msg = new CommMessage (comm.msgReadScriptFolders);
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }      
 
        // --------------------------------------
        // Files
      
      else if (sender == this.btnDataFiles_Read)     
      {
        // The 'Data files - Read' Button was pressed:
        // Get the selected Data (sub)folder
        int idx = this.lbDataFolders.SelectedIndex;
        if (-1 == idx)
        {
          // Error -> No Data (sub)folder selected
          sMsg = r.GetString ( "SDcardReader_Err_NoDataSubFolder" );  // "Kein Data-Unterordner gew�hlt.";
          sCap = r.GetString ("Form_Common_TextError");               // "Fehler";
          MessageBox.Show (this, sMsg,  sCap);
          return;
        }
        _sDataSubFolderName = this.lbDataFolders.SelectedItem.ToString ();        
        // Read Data files: "RDDATFI " + _sDataSubFolderName + "\r"
        CommMessage msg = new CommMessage (comm.msgReadDataFiles);
        msg.Parameter = _sDataSubFolderName;
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
      else if (sender == this.btnAlarmFiles_Read)     
      {
        // The 'Alarm files - Read' Button was pressed:
        // Get the selected Alarm (sub)folder
        int idx = this.lbAlarmFolders.SelectedIndex;
        if (-1 == idx)
        {
          // Error -> No Alarm (sub)folder selected
          sMsg = r.GetString ( "SDcardReader_Err_NoAlarmSubFolder" ); // "Kein Alarm-Unterordner gew�hlt.";
          sCap = r.GetString ("Form_Common_TextError");               // "Fehler";
          MessageBox.Show (this, sMsg,  sCap);
          return;
        }
        _sAlarmSubFolderName = this.lbAlarmFolders.SelectedItem.ToString ();        
        // Read Alarm files: "RDALFI " + _sAlarmSubFolderName + "\r"
        CommMessage msg = new CommMessage (comm.msgReadAlarmFiles);
        msg.Parameter = _sAlarmSubFolderName;
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
      else if (sender == this.btnScanFiles_Read)     
      {
        // The 'Scan files - Read' Button was pressed:
        // Get the selected Scan (sub)folder
        int idx = this.lbScanFolders.SelectedIndex;
        if (-1 == idx)
        {
          // Error -> No Scan (sub)folder selected
          sMsg = r.GetString ( "SDcardReader_Err_NoScanSubFolder" );  // "Kein Scan-Unterordner gew�hlt.";
          sCap = r.GetString ("Form_Common_TextError");               // "Fehler";
          MessageBox.Show (this, sMsg,  sCap);
          return;
        }
        _sScanSubFolderName = this.lbScanFolders.SelectedItem.ToString ();        
        // Read Scan files: "RDSCNFI " + _sScanSubFolderName + "\r"
        CommMessage msg = new CommMessage (comm.msgReadScanFiles);
        msg.Parameter = _sScanSubFolderName;
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
      else if (sender == this.btnScriptFiles_Read)   
      {
        // The 'Script files - Read' Button was pressed:
        // Get the selected Script (sub)folder
        int idx = this.lbScriptFolders.SelectedIndex;
        if (-1 == idx)
        {
          // Error -> No Script (sub)folder selected
          sMsg = r.GetString ( "SDcardReader_Err_NoScriptSubFolder" );// "Kein Script-Unterordner gew�hlt.";
          sCap = r.GetString ("Form_Common_TextError");               // "Fehler";
          MessageBox.Show (this, sMsg,  sCap);
          return;
        }
        _sScriptSubFolderName = this.lbScriptFolders.SelectedItem.ToString ();        
        // Read Script files: "RDSCRFI " + _sScriptSubFolderName + "\r"
        CommMessage msg = new CommMessage (comm.msgReadScriptFiles);
        msg.Parameter = _sScriptSubFolderName;
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
      else if (sender == this.btnServiceFiles_Read)   
      {
        // The 'Service files - Read' Button was pressed:
        // Read Service files: "RDSVCFI\r"
        CommMessage msg = new CommMessage (comm.msgReadServiceFiles);
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
      else if (sender == this.btnErrorFiles_Read)   
      {
        // The 'Error files - Read' Button was pressed:
        // Read Error files: "RDERRFI\r"
        CommMessage msg = new CommMessage (comm.msgReadErrorFiles);
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
    }

    /// <summary>
    /// 'Click' event of the 'Data/Alarm/Script/Scan/Service - Select' Button controls 
    /// </summary>
    private void btnSelect_Click(object sender, System.EventArgs e)
    {
      // Get the ListBox corr'ing to the Select Button control
      ListBox lb = null;
      if      (sender == this.btnDataFiles_Select)    lb=this.lbDataFiles;
      else if (sender == this.btnAlarmFiles_Select)   lb=this.lbAlarmFiles;
      else if (sender == this.btnScriptFiles_Select)  lb=this.lbScriptFiles;
      else if (sender == this.btnScanFiles_Select)    lb=this.lbScanFiles;
      else if (sender == this.btnServiceFiles_Select) lb=this.lbServiceFiles;
      else if (sender == this.btnErrorFiles_Select)   lb=this.lbErrorFiles;
      // React on non-assigned or empty ListBox controls
      if (null == lb)
        return;
      if (lb.Items.Count == 0)
        return;
      // Depending on the current Button text:
      // - If 'Select all':   Select all ListBox items,   Button text: 'Unselect all'
      // - If 'Unselect all': Unselect all ListBox items, Button text: 'Select all'
      Button btn = (Button) sender;
      bool bSelect = (btn.Text == _sBTN_TEXT_SELECT_ALL) ? true : false;
      string sText = (btn.Text == _sBTN_TEXT_SELECT_ALL) ? _sBTN_TEXT_UNSELECT_ALL : _sBTN_TEXT_SELECT_ALL;
      //    Select resp. Unselect all ListBox items
      for (int i=0; i < lb.Items.Count ;i++)
        lb.SetSelected (i, bSelect);
      //    Update the Button text corr'ly
      btn.Text = sText;
    }

    /// <summary>
    /// 'SelectedIndexChanged' event of the 'Data/Alarm/Script/Scan files (sub)folders' ListBox controls
    /// </summary>
    private void lbFolders_SelectedIndexChanged(object sender, System.EventArgs e)
    {
      // Check: Item selected?
      ListBox lb = (ListBox) sender;
      int idx = lb.SelectedIndex;
      if (-1 == idx)
        return;     // No
      
      // Yes:
      // Assign the 'Files' Label & ListBox, that correspond to the 'Folders' ListBox 
      ListBox lbFiles = null;
      Label lblFiles = null;
      string sSubfolderPath = "";
      if      (lb == this.lbDataFolders) 
      {
        lbFiles = this.lbDataFiles;                                                     // Corr'ing 'Files' ListBox
        lblFiles = this.lblDataFiles;                                                   // Corr'ing 'Files' Label
        sSubfolderPath = Path.Combine (_sDataFolderPath, lb.SelectedItem.ToString ());  // Corr'ing subfolder path
      }
      else if (lb == this.lbAlarmFolders)  
      {
        lbFiles = this.lbAlarmFiles;
        lblFiles = this.lblAlarmFiles;
        sSubfolderPath = Path.Combine (_sAlarmFolderPath, lb.SelectedItem.ToString ());
      }
      else if (lb == this.lbScanFolders)  
      {
        lbFiles = this.lbScanFiles;
        lblFiles = this.lblScanFiles;
        sSubfolderPath = Path.Combine (_sScanFolderPath, lb.SelectedItem.ToString ());
      }
      else if (lb == this.lbScriptFolders)  
      {
        lbFiles = this.lbScriptFiles;
        lblFiles = this.lblScriptFiles;
        sSubfolderPath = Path.Combine (_sScriptFolderPath, lb.SelectedItem.ToString ());
      }
      if (null != lbFiles)
      {
        // Update corr'ing 'Files' label
        App app = App.Instance;
        Ressources r = app.Ressources;
        string sFmt = r.GetString ("SDcardReader_Txt_AvailFiles_1");   // "Files available in '{0}':"
        lblFiles.Text = string.Format (sFmt, sSubfolderPath);
        // Clear corr'ing 'Files' ListBox
        lbFiles.Items.Clear ();
      }   
    }
 
    /// <summary>
    /// 'SelectedIndexChanged' event of the 'Data/Alarm/Script/Scan files' ListBox controls
    /// </summary>
    private void lbFiles_SelectedIndexChanged(object sender, System.EventArgs e)
    {
      // Get the Select Button corr'ing to the ListBox control  
      ListBox lb = (ListBox) sender;
      Button btn = null;
      if      (lb == this.lbDataFiles)    btn = this.btnDataFiles_Select;
      else if (lb == this.lbAlarmFiles)   btn = this.btnAlarmFiles_Select;
      else if (lb == this.lbScriptFiles)  btn = this.btnScriptFiles_Select;
      else if (lb == this.lbScanFiles)    btn = this.btnScanFiles_Select;
      else if (lb == this.lbServiceFiles) btn = this.btnServiceFiles_Select;
      else if (lb == this.lbErrorFiles)   btn = this.btnErrorFiles_Select;
      // React on non-assigned Button controls
      if (null == btn)
        return;
      // Get: All / Selected ListBox items
      int nAll = lb.Items.Count;
      int nSel = lb.SelectedItems.Count;
      // Update the Button text corr'ly:
      // - If all items are selected: Button text: 'Unselect all'
      // - If no items are selected:  Button text: 'Select all'
      if      (nSel == nAll)  btn.Text = _sBTN_TEXT_UNSELECT_ALL;
      else if (nSel == 0)     btn.Text = _sBTN_TEXT_SELECT_ALL;
    }
    
    /// <summary>
    /// 'CheckedChanged' event of the 'File preview' CheckBox control 
    /// </summary>
    private void chkFilePreview_CheckedChanged(object sender, System.EventArgs e)
    {
      if (!this.chkFilePreview.Checked)
      {
        // Reset transfer related controls: 'File contents'
        this.lblFileContents.Text = _sInitiallblFileContentsTxt;
        this.txtFileContents.Text = "";
      }
    }
    
    /// <summary>
    /// 'Click' event of the 'Add selected files' Button control 
    /// </summary>
    private void btnAdd_Click(object sender, System.EventArgs e)
    {
      // Add selected Data files
      foreach (object o in this.lbDataFiles.SelectedItems)
      {
        // Compose the device Data file path
        string sFileName = (string) o;
        string sFilePath = Path.Combine (_sDataFolderPath, _sDataSubFolderName);
        sFilePath = Path.Combine (sFilePath, sFileName);
        // Add the path to the ListBox
        if (-1 == this.lbAddedFiles.FindStringExact (sFilePath))
          this.lbAddedFiles.Items.Add (sFilePath);
      }
      // Add selected Alarm files
      foreach (object o in this.lbAlarmFiles.SelectedItems)
      {
        // Compose the device Alarm file path
        string sFileName = (string) o;
        string sFilePath = Path.Combine (_sAlarmFolderPath, _sAlarmSubFolderName);
        sFilePath = Path.Combine (sFilePath, sFileName);
        // Add the path to the ListBox
        if (-1 == this.lbAddedFiles.FindStringExact (sFilePath))
          this.lbAddedFiles.Items.Add (sFilePath);
      }
      // Add selected Script files
      foreach (object o in this.lbScriptFiles.SelectedItems)
      {
        // Compose the device Script file path
        string sFileName = (string) o;
        string sFilePath = Path.Combine (_sScriptFolderPath, _sScriptSubFolderName);
        sFilePath = Path.Combine (sFilePath, sFileName);
        // Add the path to the ListBox
        if (-1 == this.lbAddedFiles.FindStringExact (sFilePath))
          this.lbAddedFiles.Items.Add (sFilePath);
      }
      // Add selected Scan files
      foreach (object o in this.lbScanFiles.SelectedItems)
      {
        // Compose the device Scan file path
        string sFileName = (string) o;
        string sFilePath = Path.Combine (_sScanFolderPath, _sScanSubFolderName);
        sFilePath = Path.Combine (sFilePath, sFileName);
        // Add the path to the ListBox
        if (-1 == this.lbAddedFiles.FindStringExact (sFilePath))
          this.lbAddedFiles.Items.Add (sFilePath);
      }
      // Add selected Service files
      foreach (object o in this.lbServiceFiles.SelectedItems)
      {
        // Compose the device Service file path
        string sFileName = (string) o;
        string sFilePath = Path.Combine (_sServiceFolderPath, sFileName);
        // Add the path to the ListBox
        if (-1 == this.lbAddedFiles.FindStringExact (sFilePath))
          this.lbAddedFiles.Items.Add (sFilePath);
      }
      // Add selected Error files
      foreach (object o in this.lbErrorFiles.SelectedItems)
      {
        // Compose the device Error file path
        string sFileName = (string) o;
        string sFilePath = Path.Combine (_sErrorFolderPath, sFileName);
        // Add the path to the ListBox
        if (-1 == this.lbAddedFiles.FindStringExact (sFilePath))
          this.lbAddedFiles.Items.Add (sFilePath);
      }
    }
    
    /// <summary>
    /// 'Click' event of the 'Remove selected file' Button control 
    /// </summary>
    private void btnRemove_Click(object sender, System.EventArgs e)
    {
      // Check: Item selected?
      int idx = this.lbAddedFiles.SelectedIndex;
      if (-1 == idx)
        return;   // No
      // Yes:
      // Remove the selected file
      this.lbAddedFiles.Items.RemoveAt (idx);
      // Reset transfer related controls: 'File contents'
      this.lblFileContents.Text = _sInitiallblFileContentsTxt;
      this.txtFileContents.Text = "";
    }
    
    
    /// <summary>
    /// 'Click' event of the 'Remove all files' Button control 
    /// </summary>
    private void btnRemoveAll_Click(object sender, System.EventArgs e)
    {
      // Remove all files
      this.lbAddedFiles.Items.Clear ();
      // Reset transfer related controls: 'File contents'
      this.lblFileContents.Text = _sInitiallblFileContentsTxt;
      this.txtFileContents.Text = "";
    }
    
    /// <summary>
    /// 'SelectedIndexChanged' event of the 'Added files' ListBox control 
    /// </summary>
    private void lbAddedFiles_SelectedIndexChanged(object sender, System.EventArgs e)
    {
      // Check: Item selected?
      int idx = this.lbAddedFiles.SelectedIndex;
      if (-1 == idx)
        return;   // No
      // Yes:
      // Check: File preview desired?
      if (this.chkFilePreview.Checked)
      {
        // Yes:
        // Indicate, that the file should NOT be stored after transmission
        _bWriteFile = false;
        // Initiate Transfer progress bar
        this.pgbTransfer.Minimum  = 0;
        this.pgbTransfer.Maximum  = 1;
        this.pgbTransfer.Value    = this.pgbTransfer.Minimum;
        // Initiate 'Transfer rate' Label
        this.lblTransferRate.Text = string.Format ("{0}/{1}", this.pgbTransfer.Value, this.pgbTransfer.Maximum); 
        
        // Indicate that the Transfer process has begun.
        _bTransferInProgress = true;
        // TX: Script Current
        //    Parameter: none
        //    Device: Action - nothing; 
        //            Returns - Name of the script currently running on the PID device
        App app = App.Instance;
        AppComm comm = app.Comm;
        CommMessage msg = new CommMessage ( comm.msgScriptCurrent );
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
        // TX: Transfer Start
        //    Parameter: The transfer task to be performed
        //    Device: Action - Indicate, that the SD card data transfer to PC has begun; 
        //            Returns - OK
        msg = new CommMessage ( comm.msgTransferStart );
        msg.Parameter = TransferTask.SDcard_Data.ToString ( "D" );
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage ( msg );
 
        // Fill the Transfer list with the path of the file to be transferred
        this._alTrans.Clear ();
        this._alTrans.Add (this.lbAddedFiles.SelectedItem.ToString ());

        // Initiate the transfer 
        // TX: Read Text file contents: "RDTFI " + sFilePath + "\r"
        msg = new CommMessage (comm.msgReadTextFileContents);
        msg.Parameter = this._alTrans[0].ToString ();
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
    }
    
    /// <summary>
    /// 'Click' event of the 'Transfer' Button control 
    /// </summary>
    private void btnTransfer_Click(object sender, System.EventArgs e)
    {
      // Check: Are there any files to be transferred?
      if (this.lbAddedFiles.Items.Count > 0)
      {
        // Yes:
        // Indicate, that the files should be stored after transmission
        _bWriteFile = true;
        // Initiate Transfer progress bar
        this.pgbTransfer.Minimum  = 0;
        this.pgbTransfer.Maximum  = this.lbAddedFiles.Items.Count;
        this.pgbTransfer.Value    = this.pgbTransfer.Minimum;
        // Initiate 'Transfer rate' Label
        this.lblTransferRate.Text = string.Format ("{0}/{1}", this.pgbTransfer.Value, this.pgbTransfer.Maximum); 
        
        // Indicate that the Transfer process has begun.
        _bTransferInProgress = true;
        // TX: Script Current
        //    Parameter: none
        //    Device: Action - nothing; 
        //            Returns - Name of the script currently running on the PID device
        App app = App.Instance;
        AppComm comm = app.Comm;
        CommMessage msg = new CommMessage ( comm.msgScriptCurrent );
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
        // TX: Transfer Start
        //    Parameter: The transfer task to be performed
        //    Device: Action - Indicate, that the SD card data transfer to PC has begun; 
        //            Returns - OK
        msg = new CommMessage ( comm.msgTransferStart );
        msg.Parameter = TransferTask.SDcard_Data.ToString ( "D" );
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage ( msg );
      
        // Fill the Transfer list with the paths of the files to be transferred
        this._alTrans.Clear ();
        foreach (object o in this.lbAddedFiles.Items)
        {
          this._alTrans.Add (o);
        }

        // Initiate the transfer 
        // TX: Read Text file contents: "RDTFI " + sFilePath + "\r"
        msg = new CommMessage (comm.msgReadTextFileContents);
        msg.Parameter = this._alTrans[0].ToString ();
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
    }

    /// <summary>
    /// 'Click' event of the 'Transfer SD card' Button control 
    /// </summary>
    private void btnTransferSDcard_Click(object sender, System.EventArgs e)
    {
      ListBox lbFolders = null, lbFiles = null;
      CommMessage msg, msgFolders = null, msgFiles = null;
      string sFolderPath = "", sSubFolder = "", sSubFolderPath = "", sFile = "", sFilePath = "";
      int nFolders;

      App app = App.Instance;
      AppComm comm = app.Comm;

      // Init.
      ResetAll ();

      // ------------------------------------------------
      // Fill the 'Device files to be transferred' LB

      // Enable SDcard transfer preparation timer
      this._TimerPrepSDTrans.Enabled = true;

      // Loop over all file types (i.e. DAT, AL, SCN, SCR, SVC, ERR)
      for (int k=0; k < 6; k++)
      {
        // CD: File type
        switch (k)
        {
          case 0:         // Data
            lbFolders = this.lbDataFolders;       // The (sub)folders of the device Data folder
            lbFiles = this.lbDataFiles;           // The files in a device Data (sub)folder
            msgFolders = comm.msgReadDataFolders; // The communication message concerning Data (sub)folders transfer
            msgFiles = comm.msgReadDataFiles;     // The communication message concerning Data files transfer
            sFolderPath = _sDataFolderPath;       // The path of the device Data root folder
            break;
          case 1:         // Alarm
            lbFolders = this.lbAlarmFolders;      // analog: Alarms
            lbFiles = this.lbAlarmFiles;
            msgFolders = comm.msgReadAlarmFolders; 
            msgFiles = comm.msgReadAlarmFiles;
            sFolderPath = _sAlarmFolderPath;
            break;
          case 2:         // Scan
            lbFolders = this.lbScanFolders;       // analog: Scans
            lbFiles = this.lbScanFiles;
            msgFolders = comm.msgReadScanFolders; 
            msgFiles = comm.msgReadScanFiles;
            sFolderPath = _sScanFolderPath;
            break;
          case 3:         // Script
            lbFolders = this.lbScriptFolders;     // analog: Scripts   
            lbFiles = this.lbScriptFiles;
            msgFolders = comm.msgReadScriptFolders; 
            msgFiles = comm.msgReadScriptFiles;
            sFolderPath = _sScriptFolderPath;
            break;

          case 4:         // Service
            lbFolders = null;                     // analog: Service (w/o (sub)folder usage)
            lbFiles = this.lbServiceFiles;
            msgFolders = null;
            msgFiles = comm.msgReadServiceFiles;  
            sFolderPath = _sServiceFolderPath;
            break;
          case 5:         // Error
            lbFolders = null;                     // analog: Error (w/o (sub)folder usage)
            lbFiles = this.lbErrorFiles;
            msgFolders = null;
            msgFiles = comm.msgReadErrorFiles;
            sFolderPath = _sErrorFolderPath;
            break;
        }
        
        // Read (sub)folders in the corr'ing device root folder, if used
        if (null != lbFolders)
        {
          // (Sub)folders used:
          // TX: Read (sub)folders: "RD<xxx>FOS\r" (<xxx> = DAT, AL, SCN, SCR)
          msg = new CommMessage (msgFolders);
          msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
          comm.WriteMessage (msg);
          // Waits, until the communication is in idle state
          // --> after completion, the corr'ing (sub)folders LB has been filled
          _WaitUntilCommIdle ();
          // # of (sub)folders
          nFolders = lbFolders.Items.Count;
        }
        else
        {
          // (Sub)folders not used:
          nFolders= 1;
        }
        
        // Loop over the (sub)folders in the corr'ing device root folder 
        for (int i=0; i < nFolders; i++)
        {
          // TX: Read files: "RD<xxx>FI " + sSubFolder + "\r" (<xxx> = DAT, AL, SCN, SCR, SVC, ERR)
          msg = new CommMessage (msgFiles);
          if (null != lbFolders)
          {
            // (Sub)folders used:
            // The current (sub)folder
            sSubFolder = lbFolders.Items[i].ToString ();
            msg.Parameter = sSubFolder;
            // The path to the folder, the files to be read are located
            sSubFolderPath = Path.Combine (sFolderPath, sSubFolder);
          }
          else
          {
            // (Sub)folders not used:
            // The path to the folder, the files to be read are located
            sSubFolderPath = sFolderPath;
          }
          msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
          comm.WriteMessage (msg);
          // Waits, until the communication is in idle state 
          // --> after completion, the corr'ing files LB has been filled
          _WaitUntilCommIdle ();
          
          // Fill the 'Device files to be transferred' LB:
          // Loop over the files in the corr'ing device (sub)folder 
          for (int j=0; j < lbFiles.Items.Count; j++)
          {
            // The current file
            sFile = lbFiles.Items[j].ToString ();
            // The absolute file path
            sFilePath = Path.Combine (sSubFolderPath, sFile);
            // Add it to the LB
            this.lbAddedFiles.Items.Add (sFilePath);
          }//E - for (j)
        
        }//E - for (i)

      }//E - for (k)

      // Disable SDcard transfer preparation timer
      this._TimerPrepSDTrans.Enabled = false;
      
      // ------------------------------------------------
      // File transfer device -> PC

      btnTransfer_Click(null, new System.EventArgs());
    }

    /// <summary>
    /// 'CheckedChanged' event of the 'Clear' RadioButton controls 
    /// </summary>
    private void rbClear_CheckedChanged(object sender, System.EventArgs e)
    {
      RadioButton rb = (RadioButton)sender;
      if (rb.Checked)
      {
        // Set the device folder type
        if      (rb == this.rbData)     this._eDeviceFolderType = DeviceFolderType.Data;
        else if (rb == this.rbAlarm)    this._eDeviceFolderType = DeviceFolderType.Alarm;
        else if (rb == this.rbScan)     this._eDeviceFolderType = DeviceFolderType.Scan;
        else if (rb == this.rbScript)   this._eDeviceFolderType = DeviceFolderType.Script;
        else if (rb == this.rbService)  this._eDeviceFolderType = DeviceFolderType.Service;
        else if (rb == this.rbErrorLog) this._eDeviceFolderType = DeviceFolderType.Error;
        // Dis-/Enable the 'Clear' button
        SetClearEnable ();
      }
    }
    
    /// <summary>
    /// 'Click' event of the 'Data/Alarm/Script/Scan/Service - Clear' Button controls 
    /// </summary>
    private void btnClear_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;
      
      CommMessage msg;

      // Safety request: Should the root folder really be emptied?
      //    Preparation (CD regarding the sender)
      string sFmt = "";
      int nItem = 0;
      switch (this._eDeviceFolderType)
      {
        case DeviceFolderType.Data:     sFmt = r.GetString ("SDcardReader_Que_ClearData");    nItem = this.lbDataFolders.Items.Count; break;   // "Should the {0} Data subfolder(s) really be cleared?"
        case DeviceFolderType.Alarm:    sFmt = r.GetString ("SDcardReader_Que_ClearAlarm");   nItem = this.lbAlarmFolders.Items.Count; break;  // "Should the {0} Alarm subfolder(s) really be cleared?"
        case DeviceFolderType.Scan:     sFmt = r.GetString ("SDcardReader_Que_ClearScan");    nItem = this.lbScanFolders.Items.Count; break;   // "Should the {0} Scan subfolder(s) really be cleared?"
        case DeviceFolderType.Script:   sFmt = r.GetString ("SDcardReader_Que_ClearScript");  nItem = this.lbScriptFolders.Items.Count; break; // "Should the {0} Script subfolder(s) really be cleared?"
        case DeviceFolderType.Service:  sFmt = r.GetString ("SDcardReader_Que_ClearService"); nItem = this.lbServiceFiles.Items.Count; break;  // "Should the {0} Service files really be cleared?"
        case DeviceFolderType.Error:    sFmt = r.GetString ("SDcardReader_Que_ClearErr");     nItem = this.lbErrorFiles.Items.Count; break;     // "Should the {0} Error files really be cleared?"
      }
      //    Ensure, that an empty process will be performed ONLY when items (subfolders resp. files) are present (in the sense: enumerated in the corr'ing ListBox)
      if (0 == nItem)
        return;
      //    Request
      string sMsg = string.Format (sFmt, nItem);
      string sCap = r.GetString ("Form_Common_TextSafReq");     // "Safety request"
      DialogResult dr = MessageBox.Show (this, sMsg, sCap, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
      if ( dr == DialogResult.No )
        return; // No.

      // Yes:

#if SDCARDREADER_CLEAR_WITH_INTERRUPTION      
      // Indicate that the Transfer process has begun.
      _bTransferInProgress = true;

      // TX: Script Current
      //    Parameter: none
      //    Device: Action - nothing; 
      //            Returns - Name of the script currently running on the PID device
      msg = new CommMessage (comm.msgScriptCurrent);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      // TX: Transfer Start
      //    Parameter: The transfer task to be performed
      //    Device: Action - Indicate, that the SDcard data clear (transfer) has begun; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgTransferStart );
      msg.Parameter = TransferTask.SDcard_Data_Clear.ToString ( "D" );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );
#endif

      // Clear the subfolders on device
      //    Preparation (CD regarding the sender)
      ListBox lb = null;
      string sRoot = "";
      switch (this._eDeviceFolderType)
      {
        case DeviceFolderType.Data:     sRoot = _sDataFolderPath; lb = this.lbDataFolders; break;
        case DeviceFolderType.Alarm:    sRoot = _sAlarmFolderPath; lb = this.lbAlarmFolders; break;
        case DeviceFolderType.Scan:     sRoot = _sScanFolderPath; lb = this.lbScanFolders; break;
        case DeviceFolderType.Script:   sRoot = _sScriptFolderPath; lb = this.lbScriptFolders; break;
        case DeviceFolderType.Service:  sRoot = _sServiceFolderPath; break;
        case DeviceFolderType.Error:    sRoot = _sErrorFolderPath; break;
      }
      //    Clearing
      if (sRoot.Length > 0)
      {
        // CD: The 'Service' root folder and the 'Error' root folder are emptied by means of the 
        //     corr'ing 'Empty' routine, whereas the 'Data/Alarm/Script/Scan' root folders are emptied 
        //     by means of the 'Remove folder' routine.
        //     Reason: See Message description in AppComm.cs.
        if ((this._eDeviceFolderType == DeviceFolderType.Service) ||
            (this._eDeviceFolderType == DeviceFolderType.Error))
        {
          // Service/Error: Empty the Service/Error folder
          
          // Initiate Clear progress bar
          this.pgbClear.Minimum  = 0;
          this.pgbClear.Maximum  = 1;
          this.pgbClear.Value    = this.pgbClear.Minimum;
          // Disable the form controls
          UpdateData (false);
          // Empty the folder on device:
          // TX: Empty the Service/Error folder
          //    Parameter: none
          //    Device: Action - Empties the Service/Error folder
          //            Returns - OK
          switch (this._eDeviceFolderType)
          {
            case DeviceFolderType.Service:  msg = new CommMessage ( comm.msgEmptyServiceFolder ); break;
            case DeviceFolderType.Error:    msg = new CommMessage ( comm.msgEmptyErrorFolder ); break;
          }
          msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
          comm.WriteMessage (msg);
        }
        else
        {
          // all others: Empty the folder by clearing the subfolders
          if (null != lb)
          {
            // Initiate Clear progress bar
            this.pgbClear.Minimum  = 0;
            this.pgbClear.Maximum  = lb.Items.Count;
            this.pgbClear.Value    = this.pgbClear.Minimum;
            // Disable the form controls
            UpdateData (false);
            // Clear the subfolders on device
            foreach (object o in lb.Items)
            {
              string sSubfolderName = (string) o;
              string sFolderPath = Path.Combine (sRoot, sSubfolderName);
              // TX: Remove device folder
              //    Parameter: the device folder path
              //    Device: Action - Removes the folder
              //            Returns - OK
              msg = new CommMessage ( comm.msgRemoveFolder );
              msg.Parameter = sFolderPath;
              msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
              comm.WriteMessage (msg);
            }
          }
        }
      }

    }

    /// <summary>
    /// 'Click' event of the 'Reinitialize device storage' Button control  
    /// </summary>
    private void btnInitDisk_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;
      
      // Safety request: Should the disk really be reinitialized?
      string sMsg = r.GetString ("SDcardReader_Que_InitDisk");  // "Should the SD card really be reinitialized?"
      string sCap = r.GetString ("Form_Common_TextSafReq");     // "Safety request"
      DialogResult dr = MessageBox.Show (this, sMsg, sCap, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
      if ( dr == DialogResult.No )
        return; // No.

      // Yes:
      // Indicate that the Transfer process has begun.
      _bTransferInProgress = true;

      // TX: Script Current
      //    Parameter: none
      //    Device: Action - nothing; 
      //            Returns - Name of the script currently running on the PID device
      CommMessage msg = new CommMessage ( comm.msgScriptCurrent );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      // TX: Transfer Start
      //    Parameter: The transfer task to be performed
      //    Device: Action - Indicate, that the SD card reinitialize (transfer) has begun; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgTransferStart );
      msg.Parameter = TransferTask.SDcard_InitDisk.ToString ( "D" );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );

      // TX: (Re)initialize disk - Start
      //    Parameter: none
      //    Device: Action - (Re)initializes the disk - Start
      //            Returns - OK
      // Notes:
      //  Formating the disk is a time-consuming process. During this process a feedback from the
      //  device is not possible. In order to avoid a RX Timeout, the RX Timeout handling is  
      //  suspended during execution of the command.
      msg = new CommMessage ( comm.msgInitDisk_Start );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
    }

    /// <summary>
    /// 'HelpRequested' event of some controls
    /// </summary>
    /// <remarks>
    /// </remarks>
    private void _ctl_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Help requesting control
      Control requestingControl = (Control)sender;
      
      // Assign help text
      string sFmt = "", s = "";
     
      // ---------------------------
      // (Data/Alarm/Scan/Script/Service/Error) files on device
    
      // LB Data folders
      if (requestingControl == this.lbDataFolders)
      {
        sFmt = r.GetString ("SDcardReader_Help_lbFolders");         // "Zeigt die Unterordner an, die sich im {0}-Ordner des Ger�ts befinden."
        s = string.Format (sFmt, "Data");
      }
        // LB Data files
      else if (requestingControl == this.lbDataFiles)
      {
        sFmt = r.GetString ("SDcardReader_Help_lbFiles");           // "Zeigt die Dateien an, die sich im aktuellen Unterordner des {0}-Ordners des Ger�ts befinden."
        s = string.Format (sFmt, "Data");
      }
        // Btn. Read Data folders
      else if (requestingControl == this.btnDataFolders_Read)
      {
        sFmt = r.GetString ("SDcardReader_Help_btnFolders_Read");   // "Liest die Namen der Unterordner, die sich im {0}-Ordner des Ger�ts befinden."
        s = string.Format (sFmt, "Data");
      }
        // Btn. Read Data files
      else if (requestingControl == this.btnDataFiles_Read)
      {
        sFmt = r.GetString ("SDcardReader_Help_btnFiles_Read");     // "Liest die Namen der Dateien, die sich im aktuellen Unterordner des {0}-Ordners des Ger�ts befinden."
        s = string.Format (sFmt, "Data");
      }
        // Btn. Select Data files
      else if (requestingControl == this.btnDataFiles_Select)
      {
        sFmt = r.GetString ("SDcardReader_Help_btnFiles_Select");   // "W�hlt {0}-Dateien zur �bertragung aus."
        s = string.Format (sFmt, "Data");
      }

        // LB Alarm folders
      else if (requestingControl == this.lbAlarmFolders)
      {
        sFmt = r.GetString ("SDcardReader_Help_lbFolders");         // "Zeigt die Unterordner an, die sich im {0}-Ordner des Ger�ts befinden."
        s = string.Format (sFmt, "Alarm");
      }
        // LB Alarm files
      else if (requestingControl == this.lbAlarmFiles)
      {
        sFmt = r.GetString ("SDcardReader_Help_lbFiles");           // "Zeigt die Dateien an, die sich im aktuellen Unterordner des {0}-Ordners des Ger�ts befinden."
        s = string.Format (sFmt, "Alarm");
      }
        // Btn. Read Alarm folders
      else if (requestingControl == this.btnAlarmFolders_Read)
      {
        sFmt = r.GetString ("SDcardReader_Help_btnFolders_Read");   // "Liest die Namen der Unterordner, die sich im {0}-Ordner des Ger�ts befinden."
        s = string.Format (sFmt, "Alarm");
      }
        // Btn. Read Alarm files
      else if (requestingControl == this.btnAlarmFiles_Read)
      {
        sFmt = r.GetString ("SDcardReader_Help_btnFiles_Read");     // "Liest die Namen der Dateien, die sich im aktuellen Unterordner des {0}-Ordners des Ger�ts befinden."
        s = string.Format (sFmt, "Alarm");
      }
        // Btn. Select Alarm files
      else if (requestingControl == this.btnAlarmFiles_Select)
      {
        sFmt = r.GetString ("SDcardReader_Help_btnFiles_Select");   // "W�hlt {0}-Dateien zur �bertragung aus."
        s = string.Format (sFmt, "Alarm");
      }

        // LB Scan folders
      else if (requestingControl == this.lbScanFolders)
      {
        sFmt = r.GetString ("SDcardReader_Help_lbFolders");         // "Zeigt die Unterordner an, die sich im {0}-Ordner des Ger�ts befinden."
        s = string.Format (sFmt, "Scan");
      }
        // LB Scan files
      else if (requestingControl == this.lbScanFiles)
      {
        sFmt = r.GetString ("SDcardReader_Help_lbFiles");           // "Zeigt die Dateien an, die sich im aktuellen Unterordner des {0}-Ordners des Ger�ts befinden."
        s = string.Format (sFmt, "Scan");
      }
        // Btn. Read Scan folders
      else if (requestingControl == this.btnScanFolders_Read)
      {
        sFmt = r.GetString ("SDcardReader_Help_btnFolders_Read");   // "Liest die Namen der Unterordner, die sich im {0}-Ordner des Ger�ts befinden."
        s = string.Format (sFmt, "Scan");
      }
        // Btn. Read Scan files
      else if (requestingControl == this.btnScanFiles_Read)
      {
        sFmt = r.GetString ("SDcardReader_Help_btnFiles_Read");     // "Liest die Namen der Dateien, die sich im aktuellen Unterordner des {0}-Ordners des Ger�ts befinden."
        s = string.Format (sFmt, "Scan");
      }
        // Btn. Select Scan files
      else if (requestingControl == this.btnScanFiles_Select)
      {
        sFmt = r.GetString ("SDcardReader_Help_btnFiles_Select");   // "W�hlt {0}-Dateien zur �bertragung aus."
        s = string.Format (sFmt, "Scan");
      }

        // LB Script folders
      else if (requestingControl == this.lbScriptFolders)
      {
        sFmt = r.GetString ("SDcardReader_Help_lbFolders");         // "Zeigt die Unterordner an, die sich im {0}-Ordner des Ger�ts befinden."
        s = string.Format (sFmt, "Script");
      }
        // LB Script files
      else if (requestingControl == this.lbScriptFiles)
      {
        sFmt = r.GetString ("SDcardReader_Help_lbFiles");           // "Zeigt die Dateien an, die sich im aktuellen Unterordner des {0}-Ordners des Ger�ts befinden."
        s = string.Format (sFmt, "Script");
      }
        // Btn. Read Script folders
      else if (requestingControl == this.btnScriptFolders_Read)
      {
        sFmt = r.GetString ("SDcardReader_Help_btnFolders_Read");   // "Liest die Namen der Unterordner, die sich im {0}-Ordner des Ger�ts befinden."
        s = string.Format (sFmt, "Script");
      }
        // Btn. Read Script files
      else if (requestingControl == this.btnScriptFiles_Read)
      {
        sFmt = r.GetString ("SDcardReader_Help_btnFiles_Read");     // "Liest die Namen der Dateien, die sich im aktuellen Unterordner des {0}-Ordners des Ger�ts befinden."
        s = string.Format (sFmt, "Script");
      }
        // Btn. Select Script files
      else if (requestingControl == this.btnScriptFiles_Select)
      {
        sFmt = r.GetString ("SDcardReader_Help_btnFiles_Select");   // "W�hlt {0}-Dateien zur �bertragung aus."
        s = string.Format (sFmt, "Script");
      }

        // LB Service files
      else if (requestingControl == this.lbServiceFiles)
      {
        sFmt = r.GetString ("SDcardReader_Help_lbFiles_1");         // "Zeigt die Dateien an, die sich im {0}-Ordner des Ger�ts befinden."
        s = string.Format (sFmt, "Service");
      }
        // Btn. Read Service files
      else if (requestingControl == this.btnServiceFiles_Read)
      {
        sFmt = r.GetString ("SDcardReader_Help_btnFiles_Read_1");   // "Liest die Namen der Dateien, die sich im {0}-Ordner des Ger�ts befinden."
        s = string.Format (sFmt, "Service");
      }
        // Btn. Select Service files
      else if (requestingControl == this.btnServiceFiles_Select)
      {
        sFmt = r.GetString ("SDcardReader_Help_btnFiles_Select");   // "W�hlt {0}-Dateien zur �bertragung aus."
        s = string.Format (sFmt, "Service");
      }

        // LB Error files
      else if (requestingControl == this.lbErrorFiles)
      {
        sFmt = r.GetString ("SDcardReader_Help_lbFiles_1");         // "Zeigt die Dateien an, die sich im {0}-Ordner des Ger�ts befinden."
        s = string.Format (sFmt, "Error");
      }
        // Btn. Read Error files
      else if (requestingControl == this.btnErrorFiles_Read)
      {
        sFmt = r.GetString ("SDcardReader_Help_btnFiles_Read_1");   // "Liest die Namen der Dateien, die sich im {0}-Ordner des Ger�ts befinden."
        s = string.Format (sFmt, "Error");
      }
        // Btn. Select Error files
      else if (requestingControl == this.btnErrorFiles_Select)
      {
        sFmt = r.GetString ("SDcardReader_Help_btnFiles_Select");   // "W�hlt {0}-Dateien zur �bertragung aus."
        s = string.Format (sFmt, "Error");
      }

        // ---------------------------
        // File transfer from device to PC

        // Btn. Storage folder
      else if (requestingControl == this.btnStorageFolder)
        s = r.GetString ("SDcardReader_Help_btnStorageFolder");     // "W�hlt den Ordner auf dem PC aus, in dem die �bertragenen Dateien gespeichert werden."
        // ChB File preview
      else if (requestingControl == this.chkFilePreview)
        s = r.GetString ("SDcardReader_Help_chkFilePreview");       // "Wenn markiert, wird der Inhalt der �bertragenen Datei angezeigt."
        // LB Added files
      else if (requestingControl == this.lbAddedFiles)
        s = r.GetString ("SDcardReader_Help_lbAddedFiles");         // "Listet die zum PC zu �bertragenden Dateien auf (Transferliste)."
        // Btn. Add
      else if (requestingControl == this.btnAdd)
        s = r.GetString ("SDcardReader_Help_btnAdd");               // "F�gt die ausgew�hlten Dateien zur Liste der zu �bertragenden Dateien hinzu."
        // Btn. Remove
      else if (requestingControl == this.btnRemove)
        s = r.GetString ("SDcardReader_Help_btnRemove");            // "Entfernt die ausgew�hlten Dateien aus der Liste der zu �bertragenden Dateien."
        // Btn. Remove all
      else if (requestingControl == this.btnRemoveAll)
        s = r.GetString ("SDcardReader_Help_btnRemoveAll");         // "Entfernt alle Dateien aus der Liste der zu �bertragenden Dateien."
        // Btn. Transfer
      else if (requestingControl == this.btnTransfer)
        s = r.GetString ("SDcardReader_Help_btnTransfer");          // "�bertr�gt die in der Transferliste enthaltenen Dateien zum PC."
        // Btn. Transfer SD card
      else if (requestingControl == this.btnTransferSDcard)
        s = r.GetString ("SDcardReader_Help_btnTransferSDcard");    // "�bertr�gt alle auf der SD-Karte enthaltenen Dateien zum PC."
        // TB File contents
      else if (requestingControl == this.txtFileContents)
        s = r.GetString ("SDcardReader_Help_txtFileContents");      // "Zeigt den Inhalt der �bertragenen Datei an."

        // ---------------------------
        // Clear  device storage

        // Btn. Clear
      else if (requestingControl == this.btnClear)
        s = r.GetString ("SDcardReader_Help_btnClear");             // "Leert den angegebenen Ger�teordner."
        // Btn. Init. disk
      else if (requestingControl == this.btnInitDisk)
        s = r.GetString ("SDcardReader_Help_btnInitDisk");          // "Re-initialisiert die SD-Karte."

        // ---------------------------
        //  ... El resto del mundo ...

      else
        s = r.GetString ("SDcardReader_Help_Default");              // "(Kein Kommentar verf�gbar)"

      // Show help
      this._ToolTip.SetToolTip (requestingControl, s);
      hlpevent.Handled=true;
    }
    
    /// <summary>
    /// 'Tick' event of the timer used for monitoring the SD card reinitialisation process 
    /// (Interval: 1 sec)
    /// </summary>
    private void _Timer_Tick(object sender, System.EventArgs e)
    {
      // Update Clear progress bar
      this.pgbClear.Value++;
      if (this.pgbClear.Value == this.pgbClear.Maximum)
      {
        this.pgbClear.Value = this.pgbClear.Minimum;
      }
    }

    /// <summary>
    /// 'Tick' event of the timer used for SD card transfer preparation
    /// (Interval: 100 msec)
    /// </summary>
    private void _TimerPrepSDTrans_Tick(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;

      // Wait until communication is idle, afterwards wait a little (here: 500 ms) so that all comm. processes 
      // (RX evaluation) could finish
      if (_bWait)
      {
        // We are waiting for communication idle:
        // Check: Communication idle?
        if ( comm.IsIdle() ) 
        {
          // Yes: Start waiting a little 
          _nWait++;
          if (_nWait == 5)
          {
            // Finish waiting
            _bWait=false;       // Waiting until communication is idle: Indicate finish
            _nWait=0;           // Waiting until communication is idle: Reset count
          }
        }
      }
    }

    /// <summary>
    /// 'Tick' event of the timer used for Service file handling
    /// (Interval: 1 sec)
    /// </summary>
    private void _Timer_Svc_Tick (object sender, EventArgs e)
    {
      // Check: Is the user allowed to edit service data?
      if (EnkyLC.bEditSvc)
      {
        // Yes:
        // Perform only once in the given access mode:
        if (_bSvcEnableOnce)
        {
          // Enable Service file handling
          this.lbServiceFiles.SelectionMode = SelectionMode.MultiExtended;
          this.UpdateData ();
          // Adjust helpers for unique branch execution
          _bSvcEnableOnce = false;
          _bSvcDisableOnce = true;
        }
      }
      else
      {
        // No:
        // Perform only once in the given access mode:
        if (_bSvcDisableOnce)
        {
          // Disable Service file handling
          this.lbServiceFiles.SelectionMode = SelectionMode.None;                     // Service file LB: non-selectable
          lbFiles_SelectedIndexChanged (this.lbServiceFiles, new EventArgs ());       // Adjust corr'ing 'Select' btn. text
          DeleteAddedEntries (this._sServiceFolderPath);                              // Remove Service files from transfer LB, if any 
          if (this.lblFileContents.Text.IndexOf (this._sServiceFolderPath) != -1)     // Empty file contents TB, if a Service file is displayed
            this.txtFileContents.Text = "";
          this.UpdateData ();
          // Adjust helpers for unique branch execution
          _bSvcEnableOnce = true;
          _bSvcDisableOnce = false;
        }
      }
    }
    
    #endregion event handling

    #region constants & enums
 
    /// <summary>
    /// Device Folder type enumeration
    /// </summary>
    enum DeviceFolderType
    {
      None,
      Data,     // The 'Data' root folder
      Alarm,    // The 'Alarm' root folder
      Scan,     // The 'Scan' root folder
      Script,   // The 'Script' root folder
      Service,  // The 'Service' root folder
      Error,    // The 'Error' root folder
    };
    
    #endregion constants & enums

    #region members

    /// <summary>
    /// True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    bool _bShowWait = false;
    
    /// <summary>
    /// Indicates, whether the stored data Transfer process is curr'ly in progress ( true ) or not ( false )
    /// </summary>
    bool _bTransferInProgress = false;
    
    /// <summary>
    /// Name of the script, that was lastly running on the PID device
    /// </summary>
    string _sLastScriptName = string.Empty;

    // Device folder strings
    
    /// <summary>
    /// Path of the device Data root folder
    /// </summary>
    string _sDataFolderPath     = "";             
    /// <summary>
    /// Name of the current (sub)folder of the device Data folder
    /// </summary>
    string _sDataSubFolderName  = "";             
    /// <summary>
    /// Path of the device Alarm root folder
    /// </summary>
    string _sAlarmFolderPath    = "";             
    /// <summary>
    /// Name of the current (sub)folder of the device Alarm folder
    /// </summary>
    string _sAlarmSubFolderName = "";             
    /// <summary>
    /// Path of the device Scan root folder
    /// </summary>
    string _sScanFolderPath     = "";             
    /// <summary>
    /// Name of the current (sub)folder of the device Scan folder
    /// </summary>
    string _sScanSubFolderName  = "";              
    /// <summary>
    /// Path of the device Script root folder
    /// </summary>
    string _sScriptFolderPath   = "";             
    /// <summary>
    /// Name of the current (sub)folder of the device Script folder
    /// </summary>
    string _sScriptSubFolderName  = "";           
    /// <summary>
    /// Path of the device Service root folder
    /// </summary>
    string _sServiceFolderPath  = "";             
    /// <summary>
    /// Path of the device Error root folder
    /// </summary>
    string _sErrorFolderPath  = "";             

    // File storage

    /// <summary>
    /// Indicates, whether a transferred file should be stored (True) or not (False)
    /// </summary>
    bool _bWriteFile = false; 

    // Initial Label strings
    string _sInitialLblFoldersTxt       = "Available subfolders:";
    string _sInitialLblFilesTxt         = "Available files:";
    string _sInitiallblFileContentsTxt  = "File contents:";

    // Select Buttons
    string _sBTN_TEXT_SELECT_ALL       = "Select all";   // (Un)Select Buttons: Select all
    string _sBTN_TEXT_UNSELECT_ALL     = "Unselect all"; // (Un)Select Buttons: Unselect all

    /// <summary>
    /// The device folder type
    /// </summary>
    DeviceFolderType _eDeviceFolderType = DeviceFolderType.None;

    /// <summary>
    /// The timer used for monitoring the SD card reinitialisation process
    /// </summary>
    SimpleTimer _Timer = null;
    
    /// <summary>
    /// The Transfer list, which contains the paths of the files to be transferred (read)
    /// </summary>
    ArrayList _alTrans = new ArrayList ();
    
   
    /// <summary>
    /// Waiting until communication is idle: Indicator
    /// </summary>
    bool _bWait = false;
    /// <summary>
    /// Waiting until communication is idle: Counter
    /// </summary>
    int _nWait = 0;

    // Helper vars for unique branch execution in '_Timer_Svc_Tick()'
    bool _bSvcEnableOnce = true;
    bool _bSvcDisableOnce = true;
    
    #endregion members

    #region methods

    /// <summary>
    /// Performs initialisation tasks
    /// </summary>
    private void Init ()
    {
      // Reset all controls
      ResetAll ();
      // Initiate 'Clear' RadioButton
      this.rbData.Checked = true;
      // Assign the 'Sorted' style to the Service files LB:
      // These files should be shown in the temporal sequence as they were stored on SDcard.
      this.lbServiceFiles.Sorted = true;

      // Create & init. the timer used for monitoring the SD card reinitialisation process 
      // (duetime: restart immediately after activation, interval: 1 sec)
      _Timer = new SimpleTimer (0, 1000);
      _Timer.Tick += new EventHandler(_Timer_Tick);
    }

    /// <summary>
    /// Resets all controls
    /// </summary>
    void ResetAll ()
    {
      // Section 'Data'
      ResetData ();
      // Section 'Alarm'
      ResetAlarm ();
      // Section 'Scan'
      ResetScan ();
      // Section 'Script'
      ResetScript ();
      // Section 'Service'
      ResetService ();
      // Section 'Error'
      ResetError ();

      // Section 'File transfer'
      //    Files to be transferred
      this.lbAddedFiles.Items.Clear ();
      //    Transfer cues
      this.pgbTransfer.Value = this.pgbTransfer.Minimum;
      this.lblTransferRate.Text = ""; 
      //    File contents
      this.lblFileContents.Text = _sInitiallblFileContentsTxt;
      this.txtFileContents.Text = "";
 
      // Section 'Clear'
      this.pgbClear.Value = this.pgbClear.Minimum;
    }
    
    /// <summary>
    /// Resets the 'Data' transfer related controls
    /// </summary>
    void ResetData ()
    {
      // Section 'Data files on device'
      //    Data (sub)folders
      this.lblDataFolders.Text = _sInitialLblFoldersTxt;
      this.lbDataFolders.Items.Clear ();
      //    Data files
      this.lblDataFiles.Text = _sInitialLblFilesTxt;
      this.lbDataFiles.Items.Clear ();
      this.btnDataFiles_Select.Text = _sBTN_TEXT_SELECT_ALL;
      
      // Delete all entries that refer to the Data root folder from the 'lbAddedFiles' ListBox  
      DeleteAddedEntries (this._sDataFolderPath);    
    }

    /// <summary>
    /// Resets the 'Alarm' transfer related controls
    /// </summary>
    void ResetAlarm ()
    {
      // Section 'Alarm files on device'
      //    Alarm (sub)folders
      this.lblAlarmFolders.Text = _sInitialLblFoldersTxt;
      this.lbAlarmFolders.Items.Clear ();
      //    Alarm files
      this.lblAlarmFiles.Text = _sInitialLblFilesTxt;
      this.lbAlarmFiles.Items.Clear ();
      this.btnAlarmFiles_Select.Text = _sBTN_TEXT_SELECT_ALL;
  
      // Delete all entries that refer to the Alarm root folder from the 'lbAddedFiles' ListBox 
      DeleteAddedEntries (this._sAlarmFolderPath);    
    }
    
    /// <summary>
    /// Resets the 'Scan' transfer related controls
    /// </summary>
    void ResetScan ()
    {
      // Section 'Scan files on device'
      //    Scan (sub)folders
      this.lblScanFolders.Text = _sInitialLblFoldersTxt;
      this.lbScanFolders.Items.Clear ();
      //    Scan files
      this.lblScanFiles.Text = _sInitialLblFilesTxt;
      this.lbScanFiles.Items.Clear ();
      this.btnScanFiles_Select.Text = _sBTN_TEXT_SELECT_ALL;
   
      // Delete all entries that refer to the Scan root folder from the 'lbAddedFiles' ListBox  
      DeleteAddedEntries (this._sScanFolderPath);    
    }

    /// <summary>
    /// Resets the 'Script' transfer related controls
    /// </summary>
    void ResetScript ()
    {
      // Section 'Script files on device'
      //    Script (sub)folders
      this.lblScriptFolders.Text = _sInitialLblFoldersTxt;
      this.lbScriptFolders.Items.Clear ();
      //    Script files
      this.lblScriptFiles.Text = _sInitialLblFilesTxt;
      this.lbScriptFiles.Items.Clear ();
      this.btnScriptFiles_Select.Text = _sBTN_TEXT_SELECT_ALL;
 
      // Delete all entries that refer to the Script root folder from the 'lbAddedFiles' ListBox  
      DeleteAddedEntries (this._sScriptFolderPath);    
    }
    
    /// <summary>
    /// Resets the 'Service' transfer related controls
    /// </summary>
    void ResetService ()
    {
      // Section 'Service files on device'
      //    Service files
      this.lblServiceFiles.Text = _sInitialLblFilesTxt;
      this.lbServiceFiles.Items.Clear ();
      this.btnServiceFiles_Select.Text = _sBTN_TEXT_SELECT_ALL;
 
      // Delete all entries that refer to the Service root folder from the 'lbAddedFiles' ListBox  
      DeleteAddedEntries (this._sServiceFolderPath);    
    }

    /// <summary>
    /// Resets the 'Error' transfer related controls
    /// </summary>
    void ResetError ()
    {
      // Section 'Error files on device'
      //    Error files
      this.lblErrorFiles.Text = _sInitialLblFilesTxt;
      this.lbErrorFiles.Items.Clear ();
      this.btnErrorFiles_Select.Text = _sBTN_TEXT_SELECT_ALL;
 
      // Delete all entries that refer to the Error root folder from the 'lbAddedFiles' ListBox  
      DeleteAddedEntries (this._sErrorFolderPath);    
    }
    
    /// <summary>
    /// Deletes all entries that refer to a given root folder from the 'lbAddedFiles' ListBox.
    /// </summary>
    /// <param name="sRoot">The name of a root folder</param>
    void DeleteAddedEntries (string sRoot)
    {
      if (sRoot.Length == 0)
        return;
      bool bCont=true;
      while (bCont)
      {
        bCont=false;
        foreach (object o in this.lbAddedFiles.Items)
        {
          string sFilepath = (string) o;
          if (sFilepath.StartsWith (sRoot))
          {
            this.lbAddedFiles.Items.Remove (o);
            bCont=true;
            break;
          }
        }
      }
    }
    
    /// <summary>
    /// Dis-/Enables the 'Clear' button
    /// </summary>
    void SetClearEnable ()
    {
      ListBox lb = null;
      switch (this._eDeviceFolderType)
      {
        case DeviceFolderType.Data:     lb = this.lbDataFolders; break;
        case DeviceFolderType.Alarm:    lb = this.lbAlarmFolders; break;
        case DeviceFolderType.Scan:     lb = this.lbScanFolders; break;
        case DeviceFolderType.Script:   lb = this.lbScriptFolders; break;
        case DeviceFolderType.Service:  lb = this.lbServiceFiles; break;
        case DeviceFolderType.Error:    lb = this.lbErrorFiles; break;
      }
      if (null != lb)
      {
        this.btnClear.Enabled = lb.Items.Count > 0;
      }
    }
    
    /// <summary>
    /// Stores file contents to file
    /// </summary>
    /// <param name="sDeviceFilePath">The device file path</param>
    /// <param name="sFileContents">The file contents string</param>
    void WriteFile (string sDeviceFilePath, string sFileContents)
    {
      // The StreamWriter object, used for file writing
      StreamWriter file = null;
      // Build the full file path for storage on HD: = Concatenation Storage folder / Device file path
      string sFilePath = sDeviceFilePath;
      if (sFilePath.StartsWith (@"\"))  sFilePath = sFilePath.Substring (1);  // Skip leading '\' due to 'Combine' requirements
      sFilePath = Path.Combine (this.lblStorageFolder.Text, sFilePath);
      // If req., create the directory the file should be stored within
      string sDir = Path.GetDirectoryName(sFilePath);
      if (!Directory.Exists (sDir))
        Directory.CreateDirectory (sDir);
      // Store
      try 
      {
        // Wait cursor
        Cursor.Current = Cursors.WaitCursor;
        // Creates the file for writing UTF-8 encoded text
        file = File.CreateText (sFilePath);
        // Write the file contents to the specified file
        file.Write (sFileContents);
      }
      catch (System.Exception exc)
      {
        // Error:
        App app = App.Instance;
        Ressources r = app.Ressources;

        // Show corr'ing message
        string sFmt = r.GetString ("SDcardReader_Err_FileSave");      // "Beim Speichern der Daten nach Datei\r\n'{0}'\r\nist ein Fehler aufgetreten.\r\n(Detail:\r\n{1})
        string sMsg = string.Format (sFmt, sFilePath, exc.Message);
        string sCap = r.GetString ("Form_Common_TextError");          // "Fehler";
        MessageBox.Show (this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally 
      {
        // Close the file
        if ( null != file ) 
          file.Close ();
        // Normal cursor
        Cursor.Current = Cursors.Default;
      }
    }


    /// <summary>
    /// Finishes the transfer
    /// </summary>
    void _EndTransfer ()
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Doc doc = app.Doc;

      // TX: Script Select
      // (Device: Action - Selects the script to be executed (here: script last used); TX - OK)
      CommMessage msg = new CommMessage ( comm.msgScriptSelect );
      msg.Parameter = doc.ScriptNameToIdx ( _sLastScriptName ).ToString ();
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );
      // TX: Transfer Complete
      //    Parameter: none
      //    Device: Action - Indicate, that the transfer to PC has completed; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgTransferComplete );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);

      // Indicate that the Transfer process has finished.
      _bTransferInProgress = false;

      // Reset: Transfer cues
      //    Value (position) of the Progressbar control
      this.pgbTransfer.Value = this.pgbTransfer.Minimum;
      //    'Transfer rate' Label
      this.lblTransferRate.Text = ""; 
    }

    /// <summary>
    /// Updates the form.
    /// </summary>
    void UpdateData () 
    {
      UpdateData (true);
    }
    
    /// <summary>
    /// Updates the form.
    /// </summary>
    /// <param name="bEnable">Adjusting parameter: True - Controls are enabled pogrammatically; 
    /// False - Controls are forced to disabled state</param>
    void UpdateData (bool bEnable) 
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Doc doc = app.Doc;

      // Primary enable, masked by the adjusting parameter
      bool bEn = ( doc.sVersion.Length > 0 ) && ( _bTransferInProgress == false ) && comm.IsOpen ();
      bEn &= bEnable;
      
      // Storage folder section
      this.gpStorageFolder.Enabled      = bEn;
      
      // Text (Data/Alarm/Script/Scan/Service) file sections 
      bool bEnDSS = bEn && (_sDataFolderPath.Length > 0 && _sAlarmFolderPath.Length > 0 &&
         _sScanFolderPath.Length > 0  && _sScriptFolderPath.Length > 0 && 
         _sServiceFolderPath.Length > 0 && _sErrorFolderPath.Length > 0);
      this.gpData.Enabled   = bEnDSS;
      this.gpAlarm.Enabled  = bEnDSS;
      this.gpScript.Enabled = bEnDSS;
      this.gpScan.Enabled   = bEnDSS;
      this.gpService.Enabled = bEnDSS;
      this.gpError.Enabled  = bEnDSS;
      if (bEnDSS)
      {
        // Special case: Dis-/Enable the Select buttons
        this.btnDataFiles_Select.Enabled   = (this.lbDataFiles.Items.Count > 0);
        this.btnAlarmFiles_Select.Enabled  = (this.lbAlarmFiles.Items.Count > 0);
        this.btnScriptFiles_Select.Enabled = (this.lbScriptFiles.Items.Count > 0);
        this.btnScanFiles_Select.Enabled   = (this.lbScanFiles.Items.Count > 0);
        this.btnServiceFiles_Select.Enabled = (this.lbServiceFiles.Items.Count > 0) && EnkyLC.bEditSvc;   // Enabled state of the Select Service file button depends on access permissions!  
        this.btnErrorFiles_Select.Enabled = (this.lbErrorFiles.Items.Count > 0);
      }
      
      // Transfer section
      bool bEnT = bEn && (this.lblStorageFolder.Text.Length > 0);
      this.gpTransfer.Enabled           = bEnT;
      
      // Clear section
      this.gpClear.Enabled  = bEnDSS;
      if (bEnDSS)
      {
        // Special case: Dis-/Enable the Clear button
        SetClearEnable ();
      }  
    }

    /// <summary>
    /// Requests the device root folder paths
    /// </summary>
    void RequestDeviceRootFolders ()
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
   
      // Read Data root folder path: "RDDATFO\r"
      CommMessage msg = new CommMessage (comm.msgReadDataFolder);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      // Read Alarm root folder path: "RDALFO\r"
      msg = new CommMessage (comm.msgReadAlarmFolder);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      // Read Script root folder path: "RDSCRFO\r"
      msg = new CommMessage (comm.msgReadScriptFolder);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      // Read Scan root folder path: "RDSCNFO\r"
      msg = new CommMessage (comm.msgReadScanFolder);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      // Read Service root folder path: "RDSVCFO\r"
      msg = new CommMessage (comm.msgReadServiceFolder);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      // Read Error root folder path: "RDERRFO\r"
      msg = new CommMessage (comm.msgReadErrorFolder);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
    }

    /// <summary>
    /// Waits, until the communication is in idle state.
    /// </summary>
    void _WaitUntilCommIdle ()
    {
      _bWait = true;    // Waiting until communication is idle: Indicate start
      _nWait = 0;       // Waiting until communication is idle: Reset count
      while (_bWait)    // Wait ...
        Application.DoEvents ();
    }
    
    /// <summary>
    /// Executes a message after completion of a Receive process.
    /// </summary>
    /// <param name="msgRX">The comm. message</param>
    /// <remarks>
    /// The TX/RX messages have the following formats:
    /// 
    /// 
    /// 2.1. Data folder path request
    ///   TX
    ///     syntax:        "RDDATFO\r"
    ///   RX
    ///     syntax:        "RDDATFO (folder_path)\r"
    ///     example:       "RDDATFO \Data\r" 
    /// 2.2. Alarm folder path request
    ///   TX
    ///     syntax:        "RDALFO\r"
    ///   RX
    ///     syntax:        "RDALFO (folder_path)\r"
    ///     example:       "RDALFO \Alarms\r" 
    /// 2.3. Script folder path request
    ///   TX
    ///     syntax:        "RDSCRFO\r"
    ///   RX
    ///     syntax:        "RDSCRFO (folder_path)\r"
    ///     example:       "RDSCRFO \Scripts\r" 
    /// 2.4. Scan folder path request
    ///   TX
    ///     syntax:        "RDSCNFO\r"
    ///   RX
    ///     syntax:        "RDSCNFO (folder_path)\r"
    ///     example:       "RDSCNFO \Scans\r" 
    /// 2.5. Service folder path request
    ///   TX
    ///     syntax:        "RDSVCFO\r"
    ///   RX
    ///     syntax:        "RDSVCFO (folder_path)\r"
    ///     example:       "RDSVCFO \Service\r" 
    ///     
    ///     
    /// 2.6. Data (sub)folders request
    ///   TX
    ///     syntax:        "RDDATFOS\r"
    ///   RX
    ///     syntax:        "RDDATFOS (folder_name_1),...,(folder_name_N)\r"
    ///     example:       "RDDATFOS 000001\r" 
    /// 2.7. Data files request
    ///   TX
    ///     syntax:        "RDDATFI (folder_name)\r"
    ///     example:       "RDDATFI 000001\r"
    ///   RX
    ///     syntax:        "RDDATFI (file_name_1),...,(file_name_N)\r"
    ///     example:       "RDDATFI Res_000101000031.txt\r"
    /// 2.8. Alarm (sub)folders request
    ///   TX
    ///     syntax:        "RDALFOS\r"
    ///   RX
    ///     syntax:        "RDALFOS (folder_name_1),...,(folder_name_N)\r"
    ///     example:       "RDALFOS 000001\r" 
    /// 2.9. Alarm files request
    ///   TX
    ///     syntax:        "RDALFI (folder_name)\r"
    ///     example:       "RDALFI 000001\r"
    ///   RX
    ///     syntax:        "RDALFI (file_name_1),...,(file_name_N)\r"
    ///     example:       "RDALFI Al_000101000031.txt\r"
    /// 2.10. Scan (sub)folders request
    ///   TX
    ///     syntax:        "RDSCNFOS\r"
    ///   RX
    ///     syntax:        "RDSCNFOS (folder_name_1),...,(folder_name_N)\r"
    ///     example:       "RDSCNFOS 000001\r" 
    /// 2.11. Scan files request
    ///   TX
    ///     syntax:        "RDSCNFI (folder_name)\r"
    ///     example:       "RDSCNFI 000001\r"
    ///   RX
    ///     syntax:        "RDSCNFI (file_name_1),...,(file_name_N)\r"
    ///     example:       "RDSCNFI Scn_000101000051.txt\r"
    /// 2.12. Script (sub)folders request
    ///   TX
    ///     syntax:        "RDSCRFOS\r"
    ///   RX
    ///     syntax:        "RDSCRFOS (folder_name_1),...,(folder_name_N)\r"
    ///     example:       "RDSCRFOS 000001\r" 
    /// 2.13. Script files request
    ///   TX
    ///     syntax:        "RDSCRFI (folder_name)\r"
    ///     example:       "RDSCRFI 000001\r"
    ///   RX
    ///     syntax:        "RDSCRFI (file_name_1),...,(file_name_N)\r"
    ///     example:       "RDSCRFI Scr_000101000056.txt\r"
    /// 2.14. Service files request
    ///   TX
    ///     syntax:        "RDSVCFI\r"
    ///   RX
    ///     syntax:        "RDSVCFI (file_name_1),...,(file_name_N)\r"
    ///     example:       "RDSVCFI Svc_000101000056.txt\r"
    ///     
    ///     
    /// 2.15. File contents request (Text)
    ///   TX
    ///     syntax:        "RDTFI (file_path)\r"
    ///     examples:      "RDTFI \Data\000001\Res_000101000031.txt\r",
    ///                    "RDTFI \Scripts\000001\Scr_000101000056.txt\r",
    ///                    "RDTFI \Scans\000001\Scn_000101000051.txt\r",
    ///   RX
    ///     syntax:        "RDTFI (file_contents)\r"
    ///     example:       "RDTFI (NL)Script: ...\r"             (for Data example)
    ///   Notes:
    ///     Don't forget: In the (file_contents) buffer all occurrences of the 'END_CHAR' char are
    ///     replaced by the 'REPL_CHAR' char, that means, that on receiving this buffer on PC
    ///     a re-replacement 'REPL_CHAR' -> 'END_CHAR' must take place!
    /// 2.16. File contents request (Binary)
    ///   TX
    ///     syntax:        "RDBFI (file_path)\r"
    ///     example:       "RDBFI \System\RefScans\Scn.dat\r",
    ///   RX
    ///     syntax:        "RDBFI (file_size(4B))(file_contents)"
    ///     example:       "RDBFI ..."
    ///     
    ///     
    /// 2.17. Empty Service folder
    ///   TX
    ///     syntax:        "EYSVCFO\r"
    ///   RX
    ///     syntax:        "EYSVCFO OK\r"
    ///   (
    ///     Notes:
    ///       The 'Empty Data/Alarm/Script/Scan folder' cmds are NOT used from within this PC SW:
    ///       TX
    ///         syntax:        "EYDATFO\r" / "EYALFO\r" / "EYSCRFO\r" / "EYSCNFO\r"
    ///       RX
    ///         syntax:        "EYDATFO OK\r" / "EYALFO OK\r" / "EYSCRFO OK\r" / "EYSCNFO OK\r"
    ///   )
    ///     
    /// 2.18. Remove device folder cmd
    ///   TX
    ///     syntax:        "REMFO (folder path)\r"
    ///     example:       "REMFO \Data\D000001\r",
    ///   RX
    ///     syntax:        "REMFO OK\r"
    ///     
    /// 2.19. Reinit. device storage - Start/Perform
    ///   TX
    ///     syntax:        "INDSK_S\r" / "INDSK_P\r" 
    ///   RX
    ///     syntax:        "INDSK_S OK\r" / "INDSK_P OK\r"
    ///     
    /// </remarks>
    public void AfterRXCompleted(CommMessage msgRX)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // CD: Message response ID
      if ((msgRX.ResponseID == AppComm.CMID_TIMEOUT) || (msgRX.ResponseID == AppComm.CMID_STRLENERR))
      {
        // A comm. error (Timeout, ...) occurred:

        // Update the form
        UpdateData ();
      }

      else if (msgRX.ResponseID == AppComm.CMID_ANSWER)
      {
        // The message response is present:
        string sCmd = msgRX.CommandResponse;
        byte[] arbyPar = msgRX.ParameterResponse;

        string sMsg, sCap;

        // Get the parameter string from the parameter Byte array
        string sPar = Encoding.ASCII.GetString (arbyPar);
        // CD according to the message command
        switch (sCmd) 
        {
        
            //----------------------------------------------------------
            // Common Transfer messages
            //----------------------------------------------------------

          case "SCRCURRENT":
            // ScriptCurrent
            // RX: Name of the script currently running on the device   
            try
            {
              // Get the name of the script, that was lastly running on the IMS device
              _sLastScriptName = sPar;
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptCurrent->function failed" );
            }
            break;

          case "SCRSELECT":
            // ScriptSelect
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptSelect->function failed" );
            }
            break;
        
          case "TRNSTART":
            // Transfer Start
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgTransferStart->function failed" );
            }
            break;

          case "TRNCOMPLETE":
            // Transfer Complete
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgTransferComplete->function failed" );
            }
            break;

            //----------------------------------------------------------
            // SDcard storage messages
            //----------------------------------------------------------
          
            // --------------------------
            // Data
            // --------------------------
          
          case "RDDATFO":
            // Get the path of the device Data root folder
            // RX: The path of the device Data root folder
            try 
            {
              // Overgive the path of the device Data root folder
              _sDataFolderPath = sPar;
              // Update 'Data (sub)folders' label
              string sFmt = r.GetString ("SDcardReader_Txt_AvailSubfol_1");   // "Subfolders available in '{0}':"
              this.lblDataFolders.Text = string.Format (sFmt, _sDataFolderPath);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadDataFolder->function failed" );
            }
            break;  
          
          case "RDDATFOS":
            // Get the (sub)folders of the device Data folder
            // RX: The (sub)folders of the device Data folder
            try 
            {
              // Add the names of the (sub)folders to the 'Data (sub)folders' ListBox
              string[] ars=sPar.Split (',');
              this.lbDataFolders.Items.Clear ();
              foreach (string s in ars)
              {
                if (s.Length > 0) this.lbDataFolders.Items.Add (s);
              }          
              // Reset 'Files' section
              this.lblDataFiles.Text = _sInitialLblFilesTxt;
              this.lbDataFiles.Items.Clear ();
              // Dis-/Enable the 'Clear' button
              SetClearEnable ();
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadDataFolders->function failed" );
            }
            break;  
          
          case "RDDATFI":
            // Get the files of the current device Data subfolder
            // RX: The files of the current device Data subfolder
            try 
            {
              // Add the names of the files to the 'Data files' ListBox
              string[] ars=sPar.Split (',');
              this.lbDataFiles.Items.Clear ();
              foreach (string s in ars)
              {
                if (s.Length > 0) this.lbDataFiles.Items.Add (s);
              }
              // Set the text of the 'Data files - Select' Button corr'ly
              this.btnDataFiles_Select.Text = _sBTN_TEXT_SELECT_ALL; 
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadDataFiles->function failed" );
            }
            break;  
          
            // --------------------------
            // Alarm
            // --------------------------

          case "RDALFO":
            // Get the path of the device Alarm root folder
            // RX: The path of the device Alarm root folder
            try 
            {
              // Overgive the path of the device Alarm root folder
              _sAlarmFolderPath = sPar;
              // Update 'Alarm (sub)folders' label
              string sFmt = r.GetString ("SDcardReader_Txt_AvailSubfol_1");   // "Subfolders available in '{0}':"
              this.lblAlarmFolders.Text = string.Format (sFmt, _sAlarmFolderPath);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadAlarmFolder->function failed" );
            }
            break;  
          
          case "RDALFOS":
            // Get the (sub)folders of the device Alarm folder
            // RX: The (sub)folders of the device Alarm folder
            try 
            {
              // Add the names of the (sub)folders to the 'Alarm (sub)folders' ListBox
              string[] ars=sPar.Split (',');
              this.lbAlarmFolders.Items.Clear ();
              foreach (string s in ars)
              {
                if (s.Length > 0) this.lbAlarmFolders.Items.Add (s);
              }          
              // Reset 'Files' section
              this.lblAlarmFiles.Text = _sInitialLblFilesTxt;
              this.lbAlarmFiles.Items.Clear ();
              // Dis-/Enable the 'Clear' button
              SetClearEnable ();
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadAlarmFolders->function failed" );
            }
            break;  
          
          case "RDALFI":
            // Get the files of the current device Alarm subfolder
            // RX: The files of the current device Alarm subfolder
            try 
            {
              // Add the names of the files to the 'Alarm files' ListBox
              string[] ars=sPar.Split (',');
              this.lbAlarmFiles.Items.Clear ();
              foreach (string s in ars)
              {
                if (s.Length > 0) this.lbAlarmFiles.Items.Add (s);
              }
              // Set the text of the 'Alarm files - Select' Button corr'ly
              this.btnAlarmFiles_Select.Text = _sBTN_TEXT_SELECT_ALL; 
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadAlarmFiles->function failed" );
            }
            break;  
          
            // --------------------------
            // Scan
            // --------------------------
          
          case "RDSCNFO":
            // Get the path of the device Scan root folder
            // RX: The path of the device Scan root folder
            try 
            {
              // Overgive the path of the device Scan root folder
              _sScanFolderPath = sPar;
              // Update 'Scan (sub)folders' label
              string sFmt = r.GetString ("SDcardReader_Txt_AvailSubfol_1");   // "Subfolders available in '{0}':"
              this.lblScanFolders.Text = string.Format (sFmt, _sScanFolderPath);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadScanFolder->function failed" );
            }
            break;  

          case "RDSCNFOS":
            // Get the (sub)folders of the device Scan folder
            // RX: The (sub)folders of the device Scan folder
            try 
            {
              // Add the names of the (sub)folders to the 'Scan (sub)folders' ListBox
              string[] ars=sPar.Split (',');
              this.lbScanFolders.Items.Clear ();
              foreach (string s in ars)
              {
                if (s.Length > 0) this.lbScanFolders.Items.Add (s);
              }          
              // Reset 'Files' section
              this.lblScanFiles.Text = _sInitialLblFilesTxt;
              this.lbScanFiles.Items.Clear ();
              // Dis-/Enable the 'Clear' button
              SetClearEnable ();
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadScanFolders->function failed" );
            }
            break;         
        
          case "RDSCNFI":
            // Get the files of the device Scan folder
            // RX: The files of the device Scan folder
            try 
            {
              // Add the names of the files to the 'Scan files' ListBox
              string[] ars=sPar.Split (',');
              this.lbScanFiles.Items.Clear ();
              foreach (string s in ars)
              {
                if (s.Length > 0) this.lbScanFiles.Items.Add (s);
              }
              // Set the text of the 'Scan files - Select' Button corr'ly
              this.btnScanFiles_Select.Text = _sBTN_TEXT_SELECT_ALL;
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadScanFiles->function failed" );
            }
            break;     
   
            // --------------------------
            // Script
            // --------------------------
          
          case "RDSCRFO":
            // Get the path of the device Script root folder
            // RX: The path of the device Script root folder
            try 
            {
              // Overgive the path of the device Script root folder
              _sScriptFolderPath = sPar;
              // Update 'Script (sub)folders' label
              string sFmt = r.GetString ("SDcardReader_Txt_AvailSubfol_1");   // "Subfolders available in '{0}':"
              this.lblScriptFolders.Text = string.Format (sFmt, _sScriptFolderPath);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadScriptFolder->function failed" );
            }
            break;  
          
          case "RDSCRFOS":
            // Get the (sub)folders of the device Script folder
            // RX: The (sub)folders of the device Script folder
            try 
            {
              // Add the names of the (sub)folders to the 'Script (sub)folders' ListBox
              string[] ars=sPar.Split (',');
              this.lbScriptFolders.Items.Clear ();
              foreach (string s in ars)
              {
                if (s.Length > 0) this.lbScriptFolders.Items.Add (s);
              }          
              // Reset 'Files' section
              this.lblScriptFiles.Text = _sInitialLblFilesTxt;
              this.lbScriptFiles.Items.Clear ();
              // Dis-/Enable the 'Clear' button
              SetClearEnable ();
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadScriptFolders->function failed" );
            }
            break;         
        
          case "RDSCRFI":
            // Get the files of the device Script folder
            // RX: The files of the device Script folder
            try 
            {
              // Add the names of the files to the 'Script files' ListBox
              string[] ars=sPar.Split (',');
              this.lbScriptFiles.Items.Clear ();
              foreach (string s in ars)
              {
                if (s.Length > 0) this.lbScriptFiles.Items.Add (s);
              }
              // Set the text of the 'Script files - Select' Button corr'ly
              this.btnScriptFiles_Select.Text = _sBTN_TEXT_SELECT_ALL;
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadScriptFiles->function failed" );
            }
            break;  

            // --------------------------
            // Service
            // --------------------------

          case "RDSVCFO":
            // Get the path of the device Service root folder
            // RX: The path of the device Service root folder
            try 
            {
              // Overgive the path of the device Service root folder
              _sServiceFolderPath = sPar;
              // Update 'Service files' label
              string sFmt = r.GetString ("SDcardReader_Txt_AvailFiles_1");    // "Files available in '{0}':"
              this.lblServiceFiles.Text = string.Format (sFmt, _sServiceFolderPath);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadServiceFolder->function failed" );
            }
            break;  

          case "RDSVCFI":
            // Get the files of the device Service folder
            // RX: The files of the device Service folder
            try 
            {
              // Add the names of the files to the 'Service files' ListBox
              string[] ars=sPar.Split (',');
              this.lbServiceFiles.Items.Clear ();
              foreach (string s in ars)
              {
                if (s.Length > 0) this.lbServiceFiles.Items.Add (s);
              }
              // Set the text of the 'Service files - Select' Button corr'ly
              this.btnServiceFiles_Select.Text = _sBTN_TEXT_SELECT_ALL;
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadServiceFiles->function failed" );
            }
            break;  

          case "EYSVCFO":            
            // Empty Service folder
            // RX: "111...OK" or "111...Error", where a "1" stands for each deleted item (file/(sub)folder) in the Service root folder
            try
            {
              // Update Clear progress bar
              this.pgbClear.Value++;
              if (pgbClear.Value == this.pgbClear.Maximum)
              {

#if SDCARDREADER_CLEAR_WITH_INTERRUPTION      
                // Finish the transfer
                _EndTransfer ( );
#endif
                // Update affected controls
                ResetService ();
         
                // Reset Clear progress bar
                System.Threading.Thread.Sleep (1000);                     // in order to see the progress bar filling
                this.pgbClear.Value = this.pgbClear.Minimum;              // Reset
                
                // Display: Success message
                sMsg = r.GetString ( "SDcardReader_Info_ServiceEmptied" );// "Der Service-Ordner wurde geleert."
                sCap = r.GetString ( "Form_Common_TextInfo" );            // "Information"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
              }
            }
            catch
            {
              Debug.WriteLine ( "OnMsgEmptyServiceFolder->function failed" );
            }
            break;

            // --------------------------
            // Error
            // --------------------------

          case "RDERRFO":
            // Get the path of the device Error root folder
            // RX: The path of the device Error root folder
            try 
            {
              // Overgive the path of the device Error root folder
              _sErrorFolderPath = sPar;
              // Update 'Error files' label
              string sFmt = r.GetString ("SDcardReader_Txt_AvailFiles_1");    // "Files available in '{0}':"
              this.lblErrorFiles.Text = string.Format (sFmt, _sErrorFolderPath);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadErrorFolder->function failed" );
            }
            break;  

          case "RDERRFI":
            // Get the files of the device Error folder
            // RX: The files of the device Error folder
            try 
            {
              // Add the names of the files to the 'Error files' ListBox
              string[] ars=sPar.Split (',');
              this.lbErrorFiles.Items.Clear ();
              foreach (string s in ars)
              {
                if (s.Length > 0) this.lbErrorFiles.Items.Add (s);
              }
              // Set the text of the 'Error files - Select' Button corr'ly
              this.btnErrorFiles_Select.Text = _sBTN_TEXT_SELECT_ALL;
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadErrorFiles->function failed" );
            }
            break;  

          case "EYERRFO":            
            // Empty Error folder
            // RX: "111...OK" or "111...Error", where a "1" stands for each deleted item (file/(sub)folder) in the Error root folder
            try
            {
              // Update Clear progress bar
              this.pgbClear.Value++;
              if (pgbClear.Value == this.pgbClear.Maximum)
              {

#if SDCARDREADER_CLEAR_WITH_INTERRUPTION      
                // Finish the transfer
                _EndTransfer ( );
#endif
                
                // Update affected controls
                ResetError ();

                // Reset Clear progress bar
                System.Threading.Thread.Sleep (1000);                     // in order to see the progress bar filling
                this.pgbClear.Value = this.pgbClear.Minimum;              // Reset
                
                // Display: Success message
                sMsg = r.GetString ( "SDcardReader_Info_ErrorEmptied" );  // "Der Fehler-Ordner wurde geleert."
                sCap = r.GetString ( "Form_Common_TextInfo" );            // "Information"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
              }
            }
            catch
            {
              Debug.WriteLine ( "OnMsgEmptyErrorFolder->function failed" );
            }
            break;

            // --------------------------
            // Others
            // --------------------------

          case "RDTFI":
            // Get the file contents of a device text (Data/Script/Scan) file
            // RX: The file contents string
            try 
            {
              // Update Transfer progress bar
              this.pgbTransfer.Value++;
              // Update 'Transfer rate' Label
              this.lblTransferRate.Text = string.Format ("{0}/{1}", this.pgbTransfer.Value, this.pgbTransfer.Maximum); 
              // Get the file path from the message lastly sent
            
              //            string sCurrCmd, sFilePath;
              //            commChannel.GetLastMessage (out sCurrCmd, out sFilePath);
              string sCurrCmd = msgRX.Command;
              string sFilePath = msgRX.Parameter;
          
#if DEBUG            
              // Testing only: Show the path of the file curr'ly received in Debug window
              string sCom = string.Format("{0}.: {1}, {2}", this.pgbTransfer.Value, sCurrCmd, sFilePath);
              Debug.WriteLine ( sCom );
#endif            
            
              // Show the file contents, if req.
              if (this.chkFilePreview.Checked)
              {
                // Update the 'File contents' Label
                string sFmt = r.GetString ("SDcardReader_Txt_FileCont_1");   // "Contents of file '{0}':"
                this.lblFileContents.Text = string.Format (sFmt, sFilePath);
                // Update the 'File contents' TextBox
                this.txtFileContents.Text = sPar;
              }
              // Store the file contents to HD, if req.
              if (_bWriteFile)
              {
                WriteFile (sFilePath, sPar);
              }         

              // Remove the path of the file curr'ly received from the Transfer list
              this._alTrans.RemoveAt (0);
              // Check: Did we read all files?
              if (this._alTrans.Count > 0)
              {
                // No:
                // Continue the transfer 
                // TX: Read Text file contents: "RDTFI " + sFilePath + "\r"
                CommMessage msg = new CommMessage (comm.msgReadTextFileContents);
                msg.Parameter = this._alTrans[0].ToString ();
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage (msg);
              }
              else
              {
                // Yes:
                int nFilesRead = this.pgbTransfer.Value;
              
                // Finish the transfer
                _EndTransfer ( );
                            
                // Display: Files succ'ly read
                string sFmt = r.GetString ( "SDcardReader_Info_FilesRead" );  // "{0} files were successfully read."
                sMsg = string.Format (sFmt, nFilesRead);
                sCap = r.GetString ( "Form_Common_TextInfo" );                // "Information"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
              }
            }
            catch (System.Exception exc)
            {
              Debug.WriteLine ( "OnMsgReadTextFileContents->function failed: {0}", exc.Message );
            }
            break;  

          case "REMFO":            
            // Remove a device folder
            // RX: "111...OK" or "111...Error", where a "1" stands for each deleted item (file/(sub)folder) in the root folder
            try
            {
              // Update Clear progress bar
              this.pgbClear.Value++;
              if (pgbClear.Value == this.pgbClear.Maximum)
              {

#if SDCARDREADER_CLEAR_WITH_INTERRUPTION      
                // Finish the transfer
                _EndTransfer ( );
#endif
                // Update affected controls
                switch (_eDeviceFolderType)
                {
                  case DeviceFolderType.Data:     ResetData (); break;      // Section 'Data'
                  case DeviceFolderType.Alarm:    ResetAlarm (); break;     // Section 'Alarm'
                  case DeviceFolderType.Scan:     ResetScan (); break;      // Section 'Scan'
                  case DeviceFolderType.Script:   ResetScript (); break;    // Section 'Script'
                }
         
                // Reset Clear progress bar
                System.Threading.Thread.Sleep (1000);                     // in order to see the progress bar filling
                this.pgbClear.Value = this.pgbClear.Minimum;              // Reset
                
                // Display: Success message
                sMsg = "";
                switch (_eDeviceFolderType)
                {
                  case DeviceFolderType.Data:     sMsg = r.GetString ( "SDcardReader_Info_DataEmptied" ); break;  // "Der Resultats-Ordner wurde geleert."
                  case DeviceFolderType.Alarm:    sMsg = r.GetString ( "SDcardReader_Info_AlarmEmptied" ); break; // "Der Alarm-Ordner wurde geleert."
                  case DeviceFolderType.Scan:     sMsg = r.GetString ( "SDcardReader_Info_ScanEmptied" ); break;  // "Der Scan-Ordner wurde geleert."
                  case DeviceFolderType.Script:   sMsg = r.GetString ( "SDcardReader_Info_ScriptEmptied" ); break;// "Der Script-Ordner wurde geleert."
                }
                sCap = r.GetString ( "Form_Common_TextInfo" );    // "Information"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
              }
            }
            catch
            {
              Debug.WriteLine ( "OnMsgRemoveFolder->function failed" );
            }
            break;

          case "INDSK_S":               
            // Reinitialize the disk - Start initialisation
            // RX: "OK"
            // At this point the reinitialisation process was started.
            // Notes:
            //  The formatting of a 1GB disk takes about 20 sec. This must be considered on adjusting
            //  the 'Clear' progress bar.
            try
            {
              // Message: Continue the process?
              sMsg = r.GetString ("SDcardReader_SafReq_InitDisk");  // "The reinitialisation process may take a long time.\r\nContinue?"
              sCap = r.GetString ( "Form_Common_TextSafReq" );      // "Sicherheitsabfrage"
              DialogResult dr = MessageBox.Show (this, sMsg, sCap, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
              if ( dr == DialogResult.No )
              {
                // No:
                // Finish the transfer
                _EndTransfer ( );
                return;
              }

              // Yes:
              // Initiate Clear progress bar: 
              // Filled during 1 min (Because the timer routine in which updating
              // takes place is performed each sec, the max. of the bar is set to 60 (sec)).
              this.pgbClear.Minimum  = 0;
              this.pgbClear.Maximum  = 60;
              this.pgbClear.Value    = this.pgbClear.Minimum;
              // Disable the form controls
              UpdateData (false);
              // Push the initialisation process 
              // Notes:
              //  RX Timeout handling must be suspended, because disk formatting is a time-consuming process.
              CommMessage msg = new CommMessage (comm.msgInitDisk_Perform);
              msg.SuspendRXTimeoutHandling = true;
              msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
              comm.WriteMessage ( msg );
              // Start monitoring timer
              this._Timer.Enabled = true;
            }
            catch
            {
              Debug.WriteLine ( "OnMsgInitDiskStart->function failed" );
            }
            break;

          case "INDSK_P":          
            // Reinitialize the disk - Perform initialisation
            // RX: "OK" or "Error"
            // At this point the reinitialisation process was finished.
            try
            {
              // Stop monitoring timer
              this._Timer.Enabled = false;
              // Finish the transfer
              _EndTransfer ( );
              // Success?
              if ( sPar.ToUpper ().EndsWith ("OK") ) 
              {
                // Yes:
                // Update affected controls
                ResetAll ();
                // Request the device root folder paths
                RequestDeviceRootFolders ();
                // Display: Success message
                sMsg = r.GetString ( "SDcardReader_Info_InitDisk" );    // "The disk was reinitialized successfully";
                sCap = r.GetString ( "Form_Common_TextInfo" );          // "Information"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
              }
              else
              {
                // No:
                // Display: Error message
                sMsg = r.GetString ( "SDcardReader_Error_InitDisk" );   // "An error occurred on reinitializing the disk";
                sCap = r.GetString ( "Form_Common_TextError" );         // "Fehler"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error );
              }
            }
            catch
            {
              Debug.WriteLine ( "OnMsgInitDiskPerform->function failed" );
            }
            break;
      
        } // E - switch ( id )
    
        // Update the form
        UpdateData ();
      }//E - if (e.Message.Id = AppComm.CMID_ANSWER)

    }

    #endregion methods

    #region properties

    /// <summary>
    ///  True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    public bool ShowWait
    {
      get { return _bShowWait; }
      set { _bShowWait = value; }
    }

    #endregion properties


  }
}
