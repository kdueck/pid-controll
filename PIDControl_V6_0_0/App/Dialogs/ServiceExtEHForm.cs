using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace App
{
  /// <summary>
  /// Class ServiceExtEHForm:
  /// Input mask for Error handling section in extended service functions dialog
  /// </summary>
  public class ServiceExtEHForm : System.Windows.Forms.Form
  {
    private System.Windows.Forms.Button _btnOK;
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.ToolTip _ToolTip;
    private System.Windows.Forms.TextBox _txtID;
    private System.Windows.Forms.Label _lblID;
    private System.Windows.Forms.Label _lblPercentage;
    private System.Windows.Forms.TextBox _txtPercentage;
    private System.ComponentModel.IContainer components;

    /// <summary>
    /// Constructor
    /// </summary>
    public ServiceExtEHForm()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this._txtID = new System.Windows.Forms.TextBox();
      this._lblID = new System.Windows.Forms.Label();
      this._btnOK = new System.Windows.Forms.Button();
      this._btnCancel = new System.Windows.Forms.Button();
      this._lblPercentage = new System.Windows.Forms.Label();
      this._txtPercentage = new System.Windows.Forms.TextBox();
      this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
      this.SuspendLayout();
      // 
      // _txtID
      // 
      this._txtID.Location = new System.Drawing.Point(112, 8);
      this._txtID.Name = "_txtID";
      this._txtID.ReadOnly = true;
      this._txtID.TabIndex = 3;
      this._txtID.Text = "";
      this._txtID.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblID
      // 
      this._lblID.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblID.Location = new System.Drawing.Point(8, 8);
      this._lblID.Name = "_lblID";
      this._lblID.TabIndex = 2;
      this._lblID.Text = "ID:";
      // 
      // _btnOK
      // 
      this._btnOK.Location = new System.Drawing.Point(24, 72);
      this._btnOK.Name = "_btnOK";
      this._btnOK.TabIndex = 4;
      this._btnOK.Text = "OK";
      this._btnOK.Click += new System.EventHandler(this._btnOK_Click);
      // 
      // _btnCancel
      // 
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point(124, 72);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.TabIndex = 5;
      this._btnCancel.Text = "Cancel";
      // 
      // _lblPercentage
      // 
      this._lblPercentage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblPercentage.Location = new System.Drawing.Point(8, 36);
      this._lblPercentage.Name = "_lblPercentage";
      this._lblPercentage.TabIndex = 0;
      this._lblPercentage.Text = "Percentage:";
      // 
      // _txtPercentage
      // 
      this._txtPercentage.Location = new System.Drawing.Point(112, 36);
      this._txtPercentage.Name = "_txtPercentage";
      this._txtPercentage.TabIndex = 1;
      this._txtPercentage.Text = "";
      this._txtPercentage.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _ToolTip
      // 
      this._ToolTip.AutoPopDelay = 10000;
      this._ToolTip.InitialDelay = 500;
      this._ToolTip.ReshowDelay = 100;
      // 
      // ServiceExtEHForm
      // 
      this.AcceptButton = this._btnOK;
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.CancelButton = this._btnCancel;
      this.ClientSize = new System.Drawing.Size(222, 104);
      this.Controls.Add(this._lblPercentage);
      this.Controls.Add(this._txtPercentage);
      this.Controls.Add(this._btnCancel);
      this.Controls.Add(this._btnOK);
      this.Controls.Add(this._lblID);
      this.Controls.Add(this._txtID);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.HelpButton = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ServiceExtEHForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Percentages";
      this.Load += new System.EventHandler(this.ServiceExtEHForm_Load);
      this.ResumeLayout(false);

    }

    #endregion

    #region members

    /// <summary>
    /// The ID (in [1, 32])
    /// </summary>
    int _ID = 0;

    /// <summary>
    /// The Percentage (in [0, 100])
    /// </summary>
    int _Percentage = 0;

    #endregion members

    #region events

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void ServiceExtEHForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      try
      {
        // Resources
        this.Text               = r.GetString ( "ServiceExtEH_Title" );         // "Percentages"
        this._btnCancel.Text    = r.GetString ( "ServiceExtEH_btnCancel" );     // "Abbruch"
        this._btnOK.Text        = r.GetString ( "ServiceExtEH_btnOK" );         // "OK"
        this._lblID.Text        = r.GetString ( "ServiceExtEH_lblID" );         // "ID:"
        this._lblPercentage.Text= r.GetString ( "ServiceExtEH_lblPercentage" ); // "Percentage:"
      }
      catch
      {
      }

      // Control initialisation
      _txtID.Text         = _ID.ToString ();
      _txtPercentage.Text = _Percentage.ToString ();
   
    }

    /// <summary>
    /// 'Click' event of the 'OK' button
    /// </summary>
    private void _btnOK_Click(object sender, System.EventArgs e)
    {
      object o;

      // ID: Check not req. (control readonly)
      _ID=int.Parse (this._txtID.Text);
      // Percentage: in [0,100]
      if ( !Check.Execute ( this._txtPercentage, CheckType.Int, CheckRelation.IN, 0, 100, true, out o) )
        return;
      _Percentage=(int) o;

      // OK
      this.DialogResult = DialogResult.OK;
      this.Close ();
    }
 
    /// <summary>
    /// 'HelpRequested' event of the controls
    /// </summary>
    private void _ctl_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Help requesting control
      Control requestingControl = (Control)sender;
      // Assign help text
      string s = "";
      if      (requestingControl == _txtID)
        s = r.GetString ("ServiceExtEH_Hlp_txtID");               // "The error ID"
      else if (requestingControl == _txtPercentage)
        s = r.GetString ("ServiceExtEH_Hlp_txtPercentage");       // "The percentage, that denotes the number of non-erroneous passes, before an error is generated.";
      
      // Show help
      this._ToolTip.SetToolTip (requestingControl, s);
      hlpevent.Handled=true;
    }

    #endregion events

    #region methods
    #endregion methods

    #region properties

    /// <summary>
    /// The ID (in [1, 32])
    /// </summary>
    public int ID
    {
      get { return _ID; }
      set { _ID = value; }
    }

    /// <summary>
    /// The Percentage (in [0, 100])
    /// </summary>
    public int Percentage
    {
      get { return _Percentage; }
      set { _Percentage = value; }
    }
    
    #endregion properties

  }

}
