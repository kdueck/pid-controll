using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace App
{
	/// <summary>
	/// Class TestForm:
	/// Used in class 'Test'
	/// </summary>
	public class TestForm : System.Windows.Forms.Form
	{
    private System.Windows.Forms.CheckBox chkSmoothing;
    private System.Windows.Forms.Label lblFilterWidth;
    private System.Windows.Forms.ComboBox cbFilterWidth;
    private System.Windows.Forms.Label lblNSigPD;
    private System.Windows.Forms.ComboBox cbNSigPD;
    private System.Windows.Forms.GroupBox gbMethod;
    private System.Windows.Forms.RadioButton rbSG4;
    private System.Windows.Forms.RadioButton rbSG3;
    private System.Windows.Forms.RadioButton rbSG2;
    private System.Windows.Forms.RadioButton rbSG1;
    private System.Windows.Forms.Label lblMA;
    private System.Windows.Forms.Label lblSG;
    private System.Windows.Forms.RadioButton rbMA;
		
    /// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

    /// <summary>
    /// Constructor
    /// </summary>
    public TestForm()
		{
			InitializeComponent();

      // Init.
      Init ();
    }

    /// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

    /// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.lblFilterWidth = new System.Windows.Forms.Label();
      this.chkSmoothing = new System.Windows.Forms.CheckBox();
      this.cbFilterWidth = new System.Windows.Forms.ComboBox();
      this.cbNSigPD = new System.Windows.Forms.ComboBox();
      this.lblNSigPD = new System.Windows.Forms.Label();
      this.gbMethod = new System.Windows.Forms.GroupBox();
      this.rbMA = new System.Windows.Forms.RadioButton();
      this.lblSG = new System.Windows.Forms.Label();
      this.lblMA = new System.Windows.Forms.Label();
      this.rbSG4 = new System.Windows.Forms.RadioButton();
      this.rbSG3 = new System.Windows.Forms.RadioButton();
      this.rbSG2 = new System.Windows.Forms.RadioButton();
      this.rbSG1 = new System.Windows.Forms.RadioButton();
      this.gbMethod.SuspendLayout();
      this.SuspendLayout();
      // 
      // lblFilterWidth
      // 
      this.lblFilterWidth.Location = new System.Drawing.Point(8, 40);
      this.lblFilterWidth.Name = "lblFilterWidth";
      this.lblFilterWidth.Size = new System.Drawing.Size(175, 20);
      this.lblFilterWidth.TabIndex = 1;
      this.lblFilterWidth.Text = "Smoothing filter width:";
      // 
      // chkSmoothing
      // 
      this.chkSmoothing.Location = new System.Drawing.Point(8, 8);
      this.chkSmoothing.Name = "chkSmoothing";
      this.chkSmoothing.Size = new System.Drawing.Size(128, 24);
      this.chkSmoothing.TabIndex = 0;
      this.chkSmoothing.Text = "Smoothing?";
      this.chkSmoothing.CheckedChanged += new System.EventHandler(this.chkSmoothing_CheckedChanged);
      // 
      // cbFilterWidth
      // 
      this.cbFilterWidth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbFilterWidth.Location = new System.Drawing.Point(192, 40);
      this.cbFilterWidth.Name = "cbFilterWidth";
      this.cbFilterWidth.Size = new System.Drawing.Size(72, 21);
      this.cbFilterWidth.TabIndex = 2;
      // 
      // cbNSigPD
      // 
      this.cbNSigPD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbNSigPD.Location = new System.Drawing.Point(192, 68);
      this.cbNSigPD.Name = "cbNSigPD";
      this.cbNSigPD.Size = new System.Drawing.Size(72, 21);
      this.cbNSigPD.TabIndex = 4;
      // 
      // lblNSigPD
      // 
      this.lblNSigPD.Location = new System.Drawing.Point(9, 68);
      this.lblNSigPD.Name = "lblNSigPD";
      this.lblNSigPD.Size = new System.Drawing.Size(175, 20);
      this.lblNSigPD.TabIndex = 3;
      this.lblNSigPD.Text = "N-sigma (Peak detection):";
      // 
      // gbMethod
      // 
      this.gbMethod.Controls.Add(this.rbMA);
      this.gbMethod.Controls.Add(this.lblSG);
      this.gbMethod.Controls.Add(this.lblMA);
      this.gbMethod.Controls.Add(this.rbSG4);
      this.gbMethod.Controls.Add(this.rbSG3);
      this.gbMethod.Controls.Add(this.rbSG2);
      this.gbMethod.Controls.Add(this.rbSG1);
      this.gbMethod.Location = new System.Drawing.Point(12, 96);
      this.gbMethod.Name = "gbMethod";
      this.gbMethod.Size = new System.Drawing.Size(296, 180);
      this.gbMethod.TabIndex = 5;
      this.gbMethod.TabStop = false;
      this.gbMethod.Text = "Smoothing method";
      // 
      // rbMA
      // 
      this.rbMA.Location = new System.Drawing.Point(52, 140);
      this.rbMA.Name = "rbMA";
      this.rbMA.Size = new System.Drawing.Size(228, 24);
      this.rbMA.TabIndex = 6;
      this.rbMA.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
      // 
      // lblSG
      // 
      this.lblSG.Location = new System.Drawing.Point(8, 24);
      this.lblSG.Name = "lblSG";
      this.lblSG.Size = new System.Drawing.Size(36, 20);
      this.lblSG.TabIndex = 0;
      this.lblSG.Text = "SG:";
      // 
      // lblMA
      // 
      this.lblMA.Location = new System.Drawing.Point(8, 140);
      this.lblMA.Name = "lblMA";
      this.lblMA.Size = new System.Drawing.Size(36, 20);
      this.lblMA.TabIndex = 5;
      this.lblMA.Text = "MA:";
      // 
      // rbSG4
      // 
      this.rbSG4.Location = new System.Drawing.Point(52, 108);
      this.rbSG4.Name = "rbSG4";
      this.rbSG4.Size = new System.Drawing.Size(228, 24);
      this.rbSG4.TabIndex = 4;
      this.rbSG4.Text = "Common case (Wrapping)";
      this.rbSG4.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
      // 
      // rbSG3
      // 
      this.rbSG3.Location = new System.Drawing.Point(52, 80);
      this.rbSG3.Name = "rbSG3";
      this.rbSG3.Size = new System.Drawing.Size(228, 24);
      this.rbSG3.TabIndex = 3;
      this.rbSG3.Text = "Fixed filter width: 41 (Wrapping)";
      this.rbSG3.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
      // 
      // rbSG2
      // 
      this.rbSG2.Location = new System.Drawing.Point(52, 52);
      this.rbSG2.Name = "rbSG2";
      this.rbSG2.Size = new System.Drawing.Size(228, 24);
      this.rbSG2.TabIndex = 2;
      this.rbSG2.Text = "Fixed filter width: 41 (No wrapping)";
      this.rbSG2.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
      // 
      // rbSG1
      // 
      this.rbSG1.Location = new System.Drawing.Point(52, 24);
      this.rbSG1.Name = "rbSG1";
      this.rbSG1.Size = new System.Drawing.Size(228, 24);
      this.rbSG1.TabIndex = 1;
      this.rbSG1.Text = "Common case (No wrapping)";
      this.rbSG1.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
      // 
      // TestForm
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.ClientSize = new System.Drawing.Size(322, 288);
      this.Controls.Add(this.gbMethod);
      this.Controls.Add(this.cbNSigPD);
      this.Controls.Add(this.lblNSigPD);
      this.Controls.Add(this.cbFilterWidth);
      this.Controls.Add(this.chkSmoothing);
      this.Controls.Add(this.lblFilterWidth);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "TestForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Test (Spectrum evaluation)";
      this.Closing += new System.ComponentModel.CancelEventHandler(this.TestForm_Closing);
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TestForm_KeyPress);
      this.Load += new System.EventHandler(this.TestForm_Load);
      this.Closed += new System.EventHandler(this.TestForm_Closed);
      this.gbMethod.ResumeLayout(false);
      this.ResumeLayout(false);

    }
		
    #endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void TestForm_Load(object sender, System.EventArgs e)
    {
    }

    /// <summary>
    /// 'Close' event of the form
    /// </summary>
    private void TestForm_Closed(object sender, System.EventArgs e)
    {
    }

    /// <summary>
    /// 'Closing' event of the form
    /// </summary>
    private void TestForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      Smoothing = this.chkSmoothing.Checked; 

      FilterWidth = int.Parse (this.cbFilterWidth.Text);
      NSigmaPD = int.Parse(this.cbNSigPD.Text);
      if      (this.rbSG1.Checked) Method = 1;
      else if (this.rbSG2.Checked) Method = 2;
      else if (this.rbSG3.Checked) Method = 3;
      else if (this.rbSG4.Checked) Method = 4;
      else if (this.rbMA.Checked) Method = 5;

      if (Method == 5 && FilterWidth > 11)
      {
        string sMsg = "Filterbreite f�r MA in [3,11]!";
        string sCap = "Error";
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        e.Cancel = true;
      }
    }

    /// <summary>
    /// 'KeyPress' event of the form
    /// </summary>
    private void TestForm_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
    {
      // Close the form on pressing 'Enter' or 'Escape'
      if ( e.KeyChar == (char) Keys.Enter || e.KeyChar == (char) Keys.Escape )
      {
        this.Close ();
      }
    }
	
    /// <summary>
    /// 'CheckedChanged' event of the CheckBox control
    /// </summary>
    private void chkSmoothing_CheckedChanged(object sender, System.EventArgs e)
    {
      // CB 'Filterwidth': 
      //  Enabled, if 'Smoothing' is requested AND the smoothing method No.1,4,5
      //  (SG: Common cases, MA) is selected; Disabled otherwise.
      bool b = this.chkSmoothing.Checked && (rbSG1.Checked || rbSG4.Checked || rbMA.Checked); 
      this.cbFilterWidth.Enabled = b;
      // GB 'Smoothing method': 
      //  Enabled, if 'Smoothing' is requested.
      this.gbMethod.Enabled = this.chkSmoothing.Checked;
    }

    /// <summary>
    /// 'CheckedChanged' event of the RB controls
    /// </summary>
    private void rb_CheckedChanged(object sender, System.EventArgs e)
    {
      // CB 'Filterwidth': 
      //  Enabled, if 'Smoothing' is requested AND the smoothing method No.1,4,5
      //  (SG: Common cases, MA) is selected; Disabled otherwise.
      bool b = this.chkSmoothing.Checked && (rbSG1.Checked || rbSG4.Checked || rbMA.Checked); 
      this.cbFilterWidth.Enabled = b;
    }
 
    #endregion event handling

    #region members
    
    /// <summary>
    /// Indicate, whether smoothing is desired (True) or not (False)
    /// </summary>
    public bool Smoothing = false;
    
    /// <summary>
    /// The width of the smoothing filter
    /// </summary>
    public int FilterWidth = 41;
    
    /// <summary>
    /// N-sigma factor - peak detection
    /// </summary>
    public int NSigmaPD = 6;

    /// <summary>
    /// The smoothing method: 
    /// 1-4: SG
    ///   1 - Commom case (No wrapping), 2 - Fixed filter width: 41 (No wrapping), 
    ///   3 - Fixed filter width: 41 (Wrapping), 4 - Commom case (Wrapping)
    /// 5: MA
    /// </summary>
    public int Method = 1;
    
    #endregion members

    #region methods

    /// <summary>
    /// Init's the form.
    /// </summary>
    void Init ()
    {
      // Init. ChB
      this.chkSmoothing.Checked = Smoothing;
      chkSmoothing_CheckedChanged (null, new System.EventArgs ());
      
      // Fill & Init. CB 'Filter width'    
      for (int i=3; i <= 41; i+=2)
        this.cbFilterWidth.Items.Add (i);
      if (this.cbFilterWidth.Items.Count > 0)
        this.cbFilterWidth.SelectedItem = (object) FilterWidth;
      
      // Fill & Init. CB 'N-sigma peak detection'    
      for (int i=0; i <= 20; i++)
        this.cbNSigPD.Items.Add (i);
      if (this.cbNSigPD.Items.Count > 0)
        this.cbNSigPD.SelectedItem = (object) NSigmaPD;
      
      // Init. RB's 'Smoothing method'
      switch (Method)
      {
          // SG
        case 1: this.rbSG1.Checked = true; break; // Commom case (No wrapping)
        case 2: this.rbSG2.Checked = true; break; // Fixed filter width: 41 (No wrapping)
        case 3: this.rbSG3.Checked = true; break; // Fixed filter width: 41 (Wrapping)
        case 4: this.rbSG4.Checked = true; break; // Commom case (Wrapping)
          // MA
        case 5: this.rbMA.Checked = true; break;
      }
    }

    #endregion methods

  }
}
