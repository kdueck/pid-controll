﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

using CommRS232;

namespace App
{
  public partial class ServieNewForm : Form
  {

    #region #tor

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="bEditMode">
    /// Indicates, whether the form is opened as editor (True) or not (False)
    /// </param>
    public ServieNewForm (bool bEditMode)
    {
      InitializeComponent();

      // Init.
      _Init(bEditMode);
    }

    #endregion #tor

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void ServiceForm_Load (object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Resources
      // CD: Editor mode?
      if (_bEditMode)
      {
        // Yes:
        this.Text = r.GetString ("Service_Title_Editor");                                     // "Servicedaten-Editor"
      }
      else
      {
        // No:
        this.Text = r.GetString ("Service_Title_Online");                                     // "Übertragung der Servicedaten"
      }

      // Tabpage 'Device'
      
      this.tpDevice.Text = r.GetString ("ServieNew_tpDevice");                                // "Gerät"
      this._lblDeviceNo.Text = r.GetString ("Service_lblDeviceNo");                           // "Gerätenr.:"
      this._lblCellNo.Text = r.GetString ("Service_lblCellNo");                               // "Zellennummer:"
      this._lblPIDlamp.Text = r.GetString ("Service_lblPIDlamp");                             // "PID lamp time (in h):"
      this._chkResetPIDlamp.Text = r.GetString ("Service_chkReset");                          // "Reset"
      this._chkDontChangePIDlamp.Text = r.GetString ("Service_chkDontChange");                // "Don't change"
      this._chkAccuDisplay.Text = r.GetString ("Service_chkAccuDisplay");                     // "Akku-Anzeige ein / aus"
      this._gbLogo.Text = r.GetString ("Service_gbLogo");                                     // "Logo"
      this._rbLogoIUT.Text = r.GetString ("Service_rbLogoIUT");                               // "IUT"
      this._rbLogoENIT.Text = r.GetString ("Service_rbLogoENIT");                             // "ENIT"
      this._rbLogoENVI.Text = r.GetString ("Service_rbLogoENVI");                             // "ENVI"
      this._gbPumpOnTimes.Text = r.GetString ("Service_gbPumpOnTimes");                       // "Pump On times (in h)"
      this._lblP1On.Text = r.GetString ("Service_lblP1");                                     // "Pumpe1:"
      this._lblP2On.Text = r.GetString ("Service_lblP2");                                     // "Pumpe2:"
      this._lblP3On.Text = r.GetString ("Service_lblP3");                                     // "Pumpe3:"
      this._chkResetP1On.Text = r.GetString ("Service_chkReset");                             // "Reset"
      this._chkResetP2On.Text = r.GetString ("Service_chkReset");                             // "Reset"
      this._chkResetP3On.Text = r.GetString ("Service_chkReset");                             // "Reset"
      this._chkDontChangeP1On.Text = r.GetString ("Service_chkDontChange");                   // "Don't change"
      this._chkDontChangeP2On.Text = r.GetString ("Service_chkDontChange");                   // "Don't change"
      this._chkDontChangeP3On.Text = r.GetString ("Service_chkDontChange");                   // "Don't change"
      string sFmt = r.GetString ("Service_lblSelfCheck");                                     // "Min. duration of self-check OK state (in [{0},{1}] sec):"
      this._lblSelfCheck.Text = string.Format (sFmt, _MIN_SELFCHECK_IN_TUBE_SEC, _MAX_SELFCHECK_IN_TUBE_SEC);
      this._chkErrConf.Text = r.GetString ("Service_chkErrConf");                             // "Fehlerbestätigung ein / aus"
      this._chkPWOnOff.Text = r.GetString ("Service_chkPWOnOff");                             // "Password on / off"
      this._lblPW.Text = r.GetString ("Service_lblPW");                                       // "Password:"

      // Tabpage 'Datetime'

      this.tpDateTime.Text = r.GetString ("ServieNew_tpDatetime");                            // "Datum/Zeit"
      this._lblDate.Text = r.GetString ("Service_lblDate");                                   // "Datum:"
      this._lblTime.Text = r.GetString ("Service_lblTime");                                   // "Zeit:"
      this._lblSvcDate.Text = r.GetString ("Service_lblSvcDate");                             // "Service-Datum:"

      // Tabpage 'Sensor'

      this.tpSensor.Text = r.GetString ("ServieNew_tpSensor");                                // "Sensor"
      this._lblIst.Text = r.GetString ("Service_lblIst");                                     // "Ist:"
      this._lblSoll.Text = r.GetString ("Service_lblSoll");                                   // "Soll:"
      this._lblKP.Text = r.GetString ("Service_lblKP");                                       // "P factor:"
      this._lblT1.Text = r.GetString ("Service_lblT1");                                       // "Temp1 (°C):"
      this._chkT1Off.Text = r.GetString ("Service_chkTOff");                                  // "Aus"
      this._lblT2.Text = r.GetString ("Service_lblT2");                                       // "Temp2 (°C):"
      this._chkT2Off.Text = r.GetString ("Service_chkTOff");                                  // "Aus"
      this._lblT3.Text = r.GetString ("Service_lblT3");                                       // "Temp3 (°C):"
      this._chkT3Off.Text = r.GetString ("Service_chkTOff");                                  // "Aus"
      this._lblT4.Text = r.GetString ("Service_lblT4");                                       // "Temp4 (°C):"
      this._chkT4Off.Text = r.GetString ("Service_chkTOff");                                  // "Aus"
      this._lblPres.Text = r.GetString ("Service_lblPres");                                   // "Druck (kPa):"
      this._chkPresOff.Text = r.GetString ("Service_chkPresOff");                             // "Anzeige aus"

      this._gbSensor_PF.Text = r.GetString ("Service_gbSensor_PF");                           // "Pump-Flow handling"
      this._lblFlow.Text = r.GetString ("Service_lblFlow");                                   // "Flow:"
      this._lblPump.Text = r.GetString ("Service_lblPump");                                   // "Pump:"
      this._lblPFHandling.Text = r.GetString ("Service_lblPFHandling");                       // "P-F handling:"
      this._lblP1.Text = r.GetString ("Service_lblP1");                                       // "Pumpe1:"
      this._lblP2.Text = r.GetString ("Service_lblP2");                                       // "Pumpe2:"
      this._lblP3.Text = r.GetString ("Service_lblP3");                                       // "Pumpe3:"

      this._lblIst_1.Text = r.GetString ("Service_lblIst");                                   // "Ist:"
      this._lblMFCcap.Text = r.GetString ("Service_lblMFCcap");                               // "MFC capacity:"
      this._lblSoll_1.Text = r.GetString ("Service_lblSoll");                                 // "Soll:"
      this._lblTolAbs.Text = r.GetString ("Service_lblTolAbs");                               // "Tol.:"
      this._lblF1.Text = r.GetString ("Service_lblF1");                                       // "Fluss1 (ml/min):"
      this._lblF2.Text = r.GetString ("Service_lblF2");                                       // "Fluss2 (ml/min):"
      this._lblF3.Text = r.GetString ("Service_lblF3");                                       // "Fluss3 (ml/min):"

      // Tabpage 'Spectrum analysis'

      this.tpAnalysis.Text = r.GetString ("ServieNew_tpAnalysis");                            // "Spektrenanalyse"
      this._lblScanOffset.Text = r.GetString ("Service_lblScanOffset");                       // "Scan offset (in [0, 32767]):"
      this._lblScanOffset_Min.Text = r.GetString ("Service_lblScanOffset_Min");               // "Min."
      this._lblScanOffset_Max.Text = r.GetString ("Service_lblScanOffset_Max");               // "Max."
      this._gbPS.Text = r.GetString ("Service_gbPS");                                         // "Peak search"
      this._lblPeakSearchNsig.Text = r.GetString ("Service_lblPeakSearchNsig");               // "'n' sigma-Faktor (>= 0, def. 6):"
      this._lblPSMode.Text = r.GetString ("Service_lblPSMode");                               // "Mode:"
      this._gbPA.Text = r.GetString ("Service_gbPA");                                         // "Peakflächenbestimmung"
      this._lblPeakAreaDetNsig.Text = r.GetString ("Service_lblPeakAreaDetNsig");             // "'n' sigma-Faktor (>= 0, def. 3):"
      this._lblPAMode.Text = r.GetString ("Service_lblPAMode");                               // "Mode:"
      this._lblGainFactor.Text = r.GetString ("Service_lblGainFactor");                       // "Gainfaktor:"
      this._gbSmoothing.Text = r.GetString ("Service_gbSmoothing");                           // "Smoothing"
      this._lblSmoothing_Type.Text = r.GetString ("Service_lblSmoothing_Type");               // "Filter type:"
      this._rbSmoothing_MA.Text = r.GetString ("Service_rbSmoothing_MA");                     // "Moving average"
      this._rbSmoothing_SG.Text = r.GetString ("Service_rbSmoothing_SG");                     // "Savitzky/Golay"
      this._lblSmoothing_MA_width.Text = r.GetString ("Service_lblSmoothing_MA_width");       // "MA: Filter width (in [3,5,...,11]):"
      this._lblSmoothing_SG_width.Text = r.GetString ("Service_lblSmoothing_SG_width");       // "SG: Filter width (in [3,5,...,41]):"
      this._lblStatScanOffset.Text = r.GetString ("Service_lblStatScanOffset");               // "Statischer Scanoffset (adc):"

      // Form

      this._gbComment.Text = r.GetString ("Service_gbComment");                               // "Kommentar"
      this._lblAvCmtSize.Text = r.GetString ("Service_lblAvCmtSize");                         // "Verfügbarer Bereich (B):"

      this._gbError.Text = r.GetString ("Service_gbError");                                   // "Fehleranzeige"

      this._gbLoad.Text = r.GetString ("Service_gbLoad");                                     // "Load service file"
      this._btnLoad.Text = r.GetString ("Service_btnLoad");                                   // "Daten laden ..."
      this._lblCurSvcFile.Text = r.GetString ("Service_lblCurSvcFile");                       // "Geladene Datei:"

      this._chkPeriodicUpdate.Text = r.GetString ("Service_chkPeriodicUpdate");               // "Periodische Aktualisierung"
      // CD: Editor mode?
      if (_bEditMode)
      {
        // Yes
        this._btnRead.Text = r.GetString ("Service_btnLoadFromFile");                         // "Daten laden"
        this._btnWrite.Text = r.GetString ("Service_btnSaveToFile");                          // "Daten speichern"
      }
      else
      {
        // No
        this._btnRead.Text = r.GetString ("Service_btnRead");                                 // "Daten lesen"
        this._btnWrite.Text = r.GetString ("Service_btnWrite");                               // "Daten schreiben"
      }
      this._btnCancel.Text = r.GetString ("Service_btnCancel");                               // "Abbruch"

      this._btnExtended.Text = r.GetString ("Service_btnExtended");                           // "Extended ..."

      // CD: Editor mode?
      if (!_bEditMode)
      {
        // No:
        // Read in initially the service data
        _btnRead_Click (null, new System.EventArgs ());
      }
    }

    /// <summary>
    /// 'Closed' event of the form
    /// </summary>
    private void ServieNewForm_FormClosed (object sender, FormClosedEventArgs e)
    {
      // Stop timer
      _TimerPeriodicUpdate.Enabled = false;
      _Timer.Enabled = false;

      // Check: Is a Transfer process still in progress?
      if (_bTransferInProgress)
      {
        // Yes:
        // Check: Did we already get the name of the script, that was lastly running?
        if (_sLastScriptName.Length > 0)
        {
          // Yes:

          // Finish the transfer
          _EndTransfer ();
        }
      }
    }

    /// <summary>
    /// 'Click' event of the '_btnRead' Button control
    /// </summary>
    /// <remarks>
    /// Regarding non-edit mode:
    /// 1.
    /// Reads in (i.e. transfers from device to PC) the service data and updates the control
    /// contents corr'ly
    /// 2.
    /// The mechanism of reading data from the device is the "chain" mechanism:
    /// The kick-off of a subsequent command takes place after the current command has completed.
    /// Updating a progress bar is done after each step.
    /// </remarks>
    private void _btnRead_Click (object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;

      // CD: Editor mode?
      if (_bEditMode)
      {
        // Yes:
        // In this mode the 'Read' button acts as 'Load' button:
        // The control contents are filled with the contents of a service data file. 

        // Load an encrypted  service data file
        if (doc.LoadServiceData (Doc.CryptMode.Encrypted))
        {
          // OK:          
          // Update the ServiceData based on the contents of this service file
          _UpdateServiceData (doc.sServiceData);
        }
      }
      else
      {
        // No:
        // B - Test sequence: members -> controls 
        // (it is assumed, that the service data string array and the service comment string array 
        //  are filled before execution by means of the 'Write data' button.)
        if (_TEST)
        {
          int idx = 0;
          idx = _arsServiceData[2].IndexOf ('\t', idx);    //T1...
          idx = _arsServiceData[2].IndexOf ('\t', idx + 1);
          idx = _arsServiceData[2].IndexOf ('\t', idx + 1);
          idx = _arsServiceData[2].IndexOf ('\t', idx + 1);  //T4

          string s1 = _arsServiceData[2].Substring (0, idx);  //Add artifically: Pres, F1, F2
          s1 += "\t100\t1000\t2000\t";

          _arsServiceData[2] = s1;

          _UpdateServiceData (false);
          _UpdateServiceCmt (false);
          return;
        }
        // E - Test sequence: members -> controls 

        // Check: Is a Transfer process still in progress?
        if (_bTransferInProgress)
          return; // Yes

        // Check: Communication channel open?
        if (!comm.IsOpen ())
          return; // No

        // Indicate that the Transfer process has begun.
        _bTransferInProgress = true;

        // TX: Script Current
        //    Parameter: none
        //    Device: Action - nothing; 
        //            Returns - Name of the script currently running on the IMS device
        CommMessage msg = new CommMessage (comm.msgScriptCurrent);
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);

        // TX: Transfer Start
        //    Parameter: The transfer task to be performed
        //    Device: Action - Indicate, that the Service-Read transfer has begun; 
        //            Returns - OK
        msg = new CommMessage (comm.msgTransferStart);
        msg.Parameter = TransferTask.Service_Read.ToString ("D");
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);

        // TX: Service Read Comment Start
        //    Parameter: none
        //    Device: Action - nothing; 
        //            Returns - The number of blocks (of a max. size, given by the device SW) 
        //                      that are required in order to transmit the complete service comment 
        //                      from the device
        msg = new CommMessage (comm.msgServiceReadCmtStart);
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
    }

    /// <summary>
    /// 'Click' event of the '_btnWrite' Button control
    /// </summary>
    /// <remarks>
    /// Regarding non-edit mode:
    /// 1.
    /// Writes (i.e. transfers from PC to device) the service data
    /// 2.
    /// There are 2 mechanisms of writing data to the device:
    /// a) The "chain" mechanism (as used here): The kick-off of a subsequent command 
    ///   takes place after the current command has answered. Updating a progress bar
    ///   is done after each step.
    /// b) The sending of all commands one after another (as done by the script transfer): 
    ///   Then by means of a timer the current message queue count can be revised and  
    ///   a progress bar can be updated corr'ly.
    /// </remarks>
    private void _btnWrite_Click (object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // CD: Editor mode?
      if (_bEditMode)
      {
        // Yes:
        // In this mode the 'Write' button acts as 'Save' button:
        // The control contents are stored into a service data file. 

        // Check the validity of the control contents
        if (!_CheckControls ())
          return;
        // Update the ServiceData members
        _UpdateServiceData (true);
        // Save the service data to file
        try
        {
          // The service data file lastly used
          string sFilename = app.Data.Common.sServiceFileName;
          // Save file dialog
          Boolean bStore = app.Doc.GetServiceFileName (ref sFilename, false, Doc.CryptMode.Encrypted);
          if (bStore)
          {
            // OK:

            // Conversion: Service Data string array -> Service data file string
            ServiceDataFile sdf = new ServiceDataFile ();
            string sDecrypted = sdf.ServiceDataAsFileString (this._arsServiceData);
            // Encrypt string
            byte[] arby = Crypt.AESEncrypt (sDecrypted, AppData.CommonData.AES_KEY, AppData.CommonData.AES_IV);
            // Store encrypted data into service data file
            FileStream fs = new FileStream (sFilename, FileMode.Create);
            BinaryWriter w = new BinaryWriter (fs);
            for (int i = 0; i < arby.Length; i++)
              w.Write (arby[i]);
            w.Close ();
            fs.Close ();
          }
        }
        catch (System.Exception exc)
        {
          string sMsg = exc.Message;
          string sCap = r.GetString ("Form_Common_TextError");  // "Fehler"
          MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
      }
      else
      {
        // No:

        // Check: Is a Transfer process still in progress?
        if (_bTransferInProgress)
          return; // Yes

        // Check: Communication channel open?
        if (!comm.IsOpen ())
          return; // No

        // Check the validity of the control contents
        if (!_CheckControls ())
          return;

        // Update the ServiceData members
        _UpdateServiceData (true);

        // Update the ServiceComment members
        _UpdateServiceCmt (true);

        // Test only?
        if (_TEST) return; // Yes.

        // Indicate that the Transfer process has begun.
        _bTransferInProgress = true;

        // TX: Script Current
        //    Parameter: none
        //    Device: Action - nothing; 
        //            Returns - Name of the script currently running on the IMS device
        CommMessage msg = new CommMessage (comm.msgScriptCurrent);
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);

        // TX: Transfer Start
        //    Parameter: The transfer task to be performed
        //    Device: Action - Indicate, that the Service-Write transfer has begun; 
        //            Returns - OK
        msg = new CommMessage (comm.msgTransferStart);
        msg.Parameter = TransferTask.Service_Write.ToString ("D");
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);

        // TX: Service Write Comment Start
        //    Parameter: none
        //    Device: Action - Resets the current service comment size on the device; 
        //            Returns - OK
        msg = new CommMessage (comm.msgServiceWriteCmtStart);
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
    }

    /// <summary>
    /// 'Click' event of the '_btnLoad' Button control
    /// </summary>
    private void _btnLoad_Click (object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;

      // Build the (device) service file path
      string sFileName = this._lbSvcFiles.SelectedItem.ToString ();
      string sFilePath = Path.Combine (_sServiceFolderPath, sFileName);

      // Init. progress bar
      this._pgbTransfer.Minimum = 0;
      this._pgbTransfer.Maximum = 1;
      this._pgbTransfer.Value = 0;

      // TX: Read Text file contents: "RDTFI " + sFilePath + "\r"
      CommMessage msg = new CommMessage (comm.msgReadTextFileContents);
      msg.Parameter = sFilePath;
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
    }

    /// <summary>
    /// 'Click' event of the '_btnExtended' Button control
    /// </summary>
    private void _btnExtended_Click (object sender, System.EventArgs e)
    {
      App app = App.Instance;
      // Instantiate a ServiceExt dialog 
      ServiceExtForm sf = new ServiceExtForm ();
      sf.ShowWait = true;
      // Show the dialog
      sf.ShowDialog ();
      sf.Dispose ();
    }

    /// <summary>
    /// 'TextChanged' event of the '_txtComment' TextBox control
    /// </summary>
    private void _txtComment_TextChanged (object sender, System.EventArgs e)
    {
      // Get the text length of the service comment
      TextBox t = (TextBox)sender;
      int n = t.Text.Length;
      // Display the available service comment size 
      int nAvSize = MAXCMTLENGTH - n;
      this._txtAvCmtSize.Text = nAvSize.ToString ();
      // Change the text color, if the available service comment size was exceeded
      t.ForeColor = (nAvSize < 0) ? Color.Red : _colorCmtDefault;
    }

    /// <summary>
    /// 'HelpRequested' event of some controls
    /// </summary>
    /// <remarks>
    /// </remarks>
    private void _ctl_HelpRequested (object sender, System.Windows.Forms.HelpEventArgs hlpevent)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Help requesting control
      Control requestingControl = (Control)sender;

      // Assign help text
      string s = "";

      // ---------------------------
      // Form level

      //   Button Read
      if (requestingControl == this._btnRead)
      {
        // CD: Editor mode?
        if (_bEditMode)
          // Yes
          s = r.GetString ("Service_Help_btnLoadFromFile");      // "Lädt die Service-Daten aus einer Datei."
        else
          // No
          s = r.GetString ("Service_Help_btnRead");              // "Liest die Service-Daten vom Eeprom des Geräts."
      }
      //   Button Write
      else if (requestingControl == this._btnWrite)
      {
        // CD: Editor mode?
        if (_bEditMode)
          // Yes
          s = r.GetString ("Service_Help_btnSaveToFile");        // "Speichert die Servicedaten in einer Datei."
        else
          // No
          s = r.GetString ("Service_Help_btnWrite");             // "Schreibt die Service-Daten auf den Eeprom des Geräts."
      }
      //  ChB Periodic Update
      else if (requestingControl == this._chkPeriodicUpdate)
        s = r.GetString ("Service_Help_chkPeriodicUpdate");     // "Wenn markiert, wird ein Timer-gestütztes periodisches Monitoring (Lesen) initialisiert."
      //  Button 'Extended ...'
      else if (requestingControl == this._btnExtended)
        s = r.GetString ("Service_Help_btnExtended");           // "Ermöglicht den Zugriff auf die erweiterte Servicefunktionalität."

        // ---------------------------
      // Device section

        // GeräteNr., Zellennr.
      else if (
       (requestingControl == this._txtDeviceNo) ||
       (requestingControl == this._txtCellNo)
        )
        s = r.GetString ("Service_Help_DeviceNo");              // "Die Nummern (Identifikatoren) von Gerät und Zelle."
      // PID lamp time
      else if (requestingControl == this._txtPIDlamp)
        s = r.GetString ("Service_Help_PIDlamp");               // "Die PID-Lampenzeit (in h)."
      //  ChB PID lamp
      else if (requestingControl == this._chkResetPIDlamp)
        s = r.GetString ("Service_Help_chkResetPIDlamp");       // "Wenn markiert, wird die PID-Lampenzeit zurückgesetzt."
      else if (requestingControl == this._chkDontChangePIDlamp)
        s = r.GetString ("Service_Help_chkDontChangePIDlamp");   // "Wenn markiert, wird die PID-Lampenzeit nicht verändert."
      //  ChB Accu display
      else if (requestingControl == this._chkAccuDisplay)
        s = r.GetString ("Service_Help_chkAccuDisplay");        // "Wenn markiert, wird der Akku-Ladezustand angezeigt."
      //  RB's Logo
      else if (
        (requestingControl == this._rbLogoIUT) ||
        (requestingControl == this._rbLogoENIT) ||
        (requestingControl == this._rbLogoENVI)
        )
        s = r.GetString ("Service_Help_rbLogo");                // "Wenn markiert, wird beim Start des Gerätes das entsprechende Logo angezeigt."
      // Pump On times: TB's, ChB's 
      else if (
        (requestingControl == this._txtP1On) ||
        (requestingControl == this._txtP2On) ||
        (requestingControl == this._txtP3On)
        )
        s = r.GetString ("Service_Help_txtPOn");                // "Zeigt die Laufzeit der entsprechenden Pumpe an."
      else if ((requestingControl == this._chkResetP1On) ||
                (requestingControl == this._chkResetP2On) ||
                (requestingControl == this._chkResetP3On))
        s = r.GetString ("Service_Help_chkResetOnTimes");        // "Wenn markiert, wird die entsprechende Pumpen-Ein-Zeit zurückgesetzt."
      else if ((requestingControl == this._chkDontChangeP1On) ||
             (requestingControl == this._chkDontChangeP2On) ||
             (requestingControl == this._chkDontChangeP3On))
        s = r.GetString ("Service_Help_chkDontChangeOnTimes");   // "Wenn markiert, wird die entsprechende Pumpen-Ein-Zeit nicht verändert."

        // ---------------------------
      // ContDevice section

      else if (requestingControl == this._txtSelfCheck)
        s = r.GetString ("Service_Help_txtSelfCheck");          // "Gibt die min. Dauer des Selbsttest-OK-Status (in sec) an: After this time interval has elapsed, the self-check is left."
      else if (requestingControl == this._chkErrConf)
        s = r.GetString ("Service_Help_chkErrConf");            // "Wenn markiert, wird der Hinweis zur Fehlerbestätigung angezeigt."
      else if (requestingControl == this._chkPWOnOff)
        s = r.GetString ("Service_Help_chkPWOnOff");            // "Wenn markiert, wird der Passwortmodus aktiviert.\r\nWenn nicht markiert, wird der Passwortmodus deaktiviert."
      else if (requestingControl == this._txtPW)
        s = r.GetString ("Service_Help_txtPW");                 // "Das Passwort (bestehend aus bis zu 4 Ziffern, d.h. in [0, 9999])"

       // ---------------------------
      // Datetime data section

      else if (requestingControl == this._dtpSvcDate)
        s = r.GetString ("Service_Help_dtpSvcDatet");           // "Gibt das Datum des nächsten Geräteservices an."

        // ---------------------------
      // Sensor data section

       // Textboxes T1-4 Ist/Soll
      else if (
        (requestingControl == this._txtT1_Ist) ||
        (requestingControl == this._txtT2_Ist) ||
        (requestingControl == this._txtT3_Ist) ||
        (requestingControl == this._txtT4_Ist)
        )
        s = r.GetString ("Service_Help_txtT_Ist");              // "Gibt die entsprechende gemessene Temperatur, oder aber einen eventuell aufgetretenen Fehler bei der Temperaturerfassung, an."
      else if (
        (requestingControl == this._nudT1_Soll) ||
        (requestingControl == this._nudT2_Soll) ||
        (requestingControl == this._nudT3_Soll) ||
        (requestingControl == this._nudT4_Soll)
        )
        s = r.GetString ("Service_Help_nudT_Soll");             // "Gibt die Soll-Temperatur für die entsprechende Heizung an."
      //  CheckBoxes T1-T4
      else if (
        (requestingControl == this._chkT1Off) ||
        (requestingControl == this._chkT2Off) ||
        (requestingControl == this._chkT3Off) ||
        (requestingControl == this._chkT4Off)
        )
        s = r.GetString ("Service_Help_chkTOff");               // "Wenn markiert, wird die entsprechende Heizung ausgeschaltet.\r\nWenn nicht markiert, wird die entsprechende Temperatur eingestellt."
      //  TextBoxes 'P'
      else if (
        (requestingControl == this._txtKP1) ||
        (requestingControl == this._txtKP2) ||
        (requestingControl == this._txtKP3) ||
        (requestingControl == this._txtKP4)
        )
        s = r.GetString ("Service_Help_txtKP");                  // "Der P-Anteil für die Heizungsregelung, in (0, 255]"
      //  'Pressure display on/off': TB, ChB
      else if (requestingControl == this._chkPresOff)
        s = r.GetString ("Service_Help_chkPresOff");            // "Wenn markiert, wird der Druck nicht angezeigt.\r\nWenn nicht markiert, wird der Druck angezeigt."
      else if (requestingControl == this._txtPres)
        s = r.GetString ("Service_Help_txtPres");               // "Gibt den gemessenen Druck, oder aber einen eventuell aufgetretenen Fehler bei der Druckerfassung, an."

       // P-F Handling: TextBoxes Flow
      else if (
        (requestingControl == this._txtF1) ||
        (requestingControl == this._txtF2) ||
        (requestingControl == this._txtF3)
        )
        s = r.GetString ("Service_Help_txtF");                  // "Der Flusswert für die Einstellung der entsprechenden Pumpe."
      // P-F Handling: NUD's Pump
      else if (
        (requestingControl == this._nudP1) ||
        (requestingControl == this._nudP2) ||
        (requestingControl == this._nudP3)
        )
        s = r.GetString ("Service_Help_nudP");                  // "Der Pumpenwert für die Einstellung des entsprechenden Flusses."
      //  P-F handling: ChB's
      else if (
        (requestingControl == this._chkPF1) ||
        (requestingControl == this._chkPF2) ||
        (requestingControl == this._chkPF3)
        )
        s = r.GetString ("Service_Help_chkPF");                 // "Wenn nicht markiert, wird der eingegebene Pumpenwert für die Flusseinstellung benutzt.\r\nWenn markiert, wird der eingegebene Flusswert für die Pumpeneinstellung benutzt."

      // Fluss: Ist  
      else if (
        (requestingControl == this._txtF1_Ist) ||
        (requestingControl == this._txtF2_Ist) ||
        (requestingControl == this._txtF3_Ist)
        )
        s = r.GetString ("Service_Help_txtF_Ist");              // "Der gemessene Fluss."
      // Fluss: MFC cap.
      else if (requestingControl == this._txtMFCcap)
        s = r.GetString ("Service_Help_txtMFCcap");             //"The MFC capacity (0 - MFC not present, > 0: capacity in ml/min)
      // Fluss: Soll
      else if (
        (requestingControl == this._txtF1_Soll) ||
        (requestingControl == this._txtF2_Soll) ||
        (requestingControl == this._txtF3_Soll)
        )
        s = r.GetString ("Service_Help_txtF_Soll");             // "Der Sollwert für die Einstellung des entsprechenden Flusses."
      // Fluss: Tolerance
      else if (
        (requestingControl == this._txtF1_Soll_TolAbs) ||
        (requestingControl == this._txtF2_Soll_TolAbs) ||
        (requestingControl == this._txtF3_Soll_TolAbs)
        )
        s = r.GetString ("Service_Help_txtF_Soll_TolAbs");      // "Die Toleranz des Sollwertes für die Einstellung des entsprechenden Flusses."

        // ---------------------------
      // Spectrum analysis section

        // Scan offset
      else if (
        (requestingControl == this._txtScanOffset_Min) ||
        (requestingControl == this._txtScanOffset_Max)
        )
        s = r.GetString ("Service_Help_txtScanOffset");         // "Die MinMax-Werte für einen gültigen Scanoffset."
      //  Peak search  mode: TB &  CB
      else if (requestingControl == this._txtPeakSearchNsig)
        s = r.GetString ("Service_Help_txtPeakSearch");         // "Der bei der Peaksuche verwendete Rauschunterdrückungsfaktor."
      else if (requestingControl == this._cbPSMode)
        s = r.GetString ("Service_Help_cbPSMode");              // "Legt den Modus für die Peaksuche fest:\r\nUnidirektional: Peaksuche in Richtung aufsteigender Scanindizes,\r\nBidirektional: Peaksuche in Richtung auf- und absteigender Scanindizes."
      //  Peak area determination mode: TB & CB
      else if (requestingControl == this._txtPeakAreaDetNsig)
        s = r.GetString ("Service_Help_txtPeakAreaDet");        // "Der bei der Peakflächenbestimmung verwendete Rauschunterdrückungsfaktor."
      else if (requestingControl == this._cbPAMode)
        s = r.GetString ("Service_Help_cbPAMode");              // "Legt den Modus für die Peakflächenbestimmung fest:\r\nSimple: Rauschpegelmethode\r\nExtended_1: zusätzlich Parabel-Fit,\r\nExtended_2: zusätzlich Gaussian-Fit."
      //  Smoothing method: RB's & TB's
      else if (
        (requestingControl == this._rbSmoothing_MA) ||
        (requestingControl == this._rbSmoothing_SG)
        )
        s = r.GetString ("Service_Help_rbSmoothing");           // "Legt den Modus für die Spektrenglättung fest:\r\nMA: Moving average,\r\nSG: Savitzky/Golay-Filter."
      else if (
        (requestingControl == this._txtSmoothing_MA_width) ||
        (requestingControl == this._txtSmoothing_SG_width)
        )
        s = r.GetString ("Service_Help_txtSmoothing");          // "Legt die Filterbreite für den Glättungsfilter fest:\r\nMA: Moving average,\r\nSG: Savitzky/Golay-Filter."
      // Gain factor
      else if (requestingControl == this._cbGainFactor)
        s = r.GetString ("Service_Help_cbGainFactor");          // "Die verfügbaren Gainfaktoren."
        // Stat. scan offset
      else if (requestingControl == this._txtStatScanOffset)
        s = r.GetString ("Service_Help_txtStatScanOffset");     // "Zeigt den aktuell verwendeten statischen Scanoffset an."

        // ---------------------------
      // Comment section

        //   TextBox Comment
      else if (requestingControl == this._txtComment)
        s = r.GetString ("Service_Help_txtComment");            // "Geben Sie hier Kommentare bezüglich der Gerätebetreuung ein."
      //   TextBox Available Comment Size
      else if (requestingControl == this._txtAvCmtSize)
        s = r.GetString ("Service_Help_txtAvCmtSize");          // "Zeigt den noch zur Verfügung stehenden Platz auf dem Gerät für Kommentare an."

        // ---------------------------
      // Error display section

        //   TextBox Errors
      else if (requestingControl == this._txtError)
        s = r.GetString ("Service_Help_txtError");              // "Zeigt die Fehler an, die beim Lesen der Servicedaten vom Gerät aufgetreten sind."

        // ---------------------------
      // Load service file section

        //   LB
      else if (requestingControl == this._lbSvcFiles)
        s = r.GetString ("Service_Help_lbSvcFiles");            // "Enthält die verfügbaren Service-Dateien."
      //   Button Load
      else if (requestingControl == this._btnLoad)
        s = r.GetString ("Service_Help_btnLoad");               // "Aktualisiert die Service-Daten auf Basis einer vorgegebenen Service-Datei."
      //   TB 'Loaded'
      else if (requestingControl == this._txtCurSvcFile)
        s = r.GetString ("Service_Help_txtCurSvcFile");         // "Verweist auf die aktuell verwendete Service-Datei."

        // ---------------------------
      //  ... El resto del mundo ...

      else
        s = r.GetString ("Service_Help_Default");               // "(Kein Kommentar verfügbar)"

      // Show help
      this._ToolTip.SetToolTip (requestingControl, s);
      hlpevent.Handled = true;
    }

    /// <summary>
    /// 'CheckedChanged' event of the T1-T4 CheckBox controls:
    /// If checked, the corr'ing UpDown controls are disabled, and viceversa.
    /// </summary>
    private void _chkTOff_CheckedChanged (object sender, System.EventArgs e)
    {
      CheckBox chk = (CheckBox)sender;
      NumericUpDown nud = null;
      if (sender == this._chkT1Off) nud = this._nudT1_Soll;
      else if (sender == this._chkT2Off) nud = this._nudT2_Soll;
      else if (sender == this._chkT3Off) nud = this._nudT3_Soll;
      else if (sender == this._chkT4Off) nud = this._nudT4_Soll;
      if (null != nud)
      {
        nud.Enabled = !chk.Checked;
      }
    }

    /// <summary>
    /// 'CheckedChanged' event of the 'Pressure display on/off' CheckBox control:
    /// If checked, the Pressure will NOT be shown in the device query's
    /// </summary>
    private void _chkPresOff_CheckedChanged (object sender, System.EventArgs e)
    {
    }

    /// <summary>
    /// 'CheckedChanged' event of the 'Reset PID lamp' and 'Reset Pump1/2/3' CheckBox controls
    /// </summary>
    private void _chkResetOnTimes_CheckedChanged (object sender, System.EventArgs e)
    {
      CheckBox chk = (CheckBox)sender;
      if (chk.Checked)
      {
        App app = App.Instance;
        Ressources r = app.Ressources;

        TextBox tb = null;
        string sFmt, sMsg = "";
        if (chk == _chkResetPIDlamp)
        {
          tb = _txtPIDlamp;
          sMsg = r.GetString ("Service_Que_ResetPIDlamp");  // "Soll die PID-Lampe wirklich zurückgesetzt werden?"
        }
        else if (chk == _chkResetP1On)
        {
          tb = _txtP1On;
          sFmt = r.GetString ("Service_Que_ResetPumpOn");   // "Soll die Pumpe {0} wirklich zurückgesetzt werden?"
          sMsg = string.Format (sFmt, 1);
        }
        else if (chk == _chkResetP2On)
        {
          tb = _txtP2On;
          sFmt = r.GetString ("Service_Que_ResetPumpOn");   // "Soll die Pumpe {0} wirklich zurückgesetzt werden?"
          sMsg = string.Format (sFmt, 2);
        }
        else if (chk == _chkResetP3On)
        {
          tb = _txtP3On;
          sFmt = r.GetString ("Service_Que_ResetPumpOn");   // "Soll die Pumpe {0} wirklich zurückgesetzt werden?"
          sMsg = string.Format (sFmt, 3);
        }
        if (null != tb)
        {
          string sCap = r.GetString ("Form_Common_TextSafReq");   // "Sicherheitsabfrage"
          DialogResult dr = MessageBox.Show (this, sMsg, sCap, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
          if (dr == DialogResult.Yes)
          {
            tb.Text = "0";
          }
          // Uncheck ChB
          chk.Checked = false;
        }
      }
    }

    /// <summary>
    /// 'CheckedChanged' event of the 'Dont change PID lamp' and 'Dont change Pump1/2/3' CheckBox controls
    /// </summary>
    private void _chkDontChangeOnTimes_CheckedChanged (object sender, EventArgs e)
    {
      CheckBox chk = (CheckBox)sender;
      if (chk.Checked)
      {
        TextBox tb = null;
        if (chk == _chkDontChangePIDlamp)
        {
          tb = _txtPIDlamp;
        }
        else if (chk == _chkDontChangeP1On)
        {
          tb = _txtP1On;
        }
        else if (chk == _chkDontChangeP2On)
        {
          tb = _txtP2On;
        }
        else if (chk == _chkDontChangeP3On)
        {
          tb = _txtP3On;
        }
        if (null != tb)
        {
          tb.Text = "1";
          // Uncheck ChB
          chk.Checked = false;
        }
      }
    }

    /// <summary>
    /// 'CheckedChanged' event of the '_chkPeriodic' CheckBox control:
    /// If checked, a Timer-based periodic monitoring (Read) is initiated.
    /// </summary>
    private void _chkPeriodicUpdate_CheckedChanged (object sender, System.EventArgs e)
    {
      CheckBox chk = (CheckBox)sender;
      if (chk.Checked)
      {
        // Start Timer-based periodic monitoring (Read)
        _TimerPeriodicUpdate.Enabled = true;                        // Start
      }
      else
      {
        // Stop Timer-based periodic monitoring (Read)
        _TimerPeriodicUpdate.Enabled = false;                       // Stop
      }
    }

    /// <summary>
    /// 'Tick' event of the '_TimerPeriodicUpdate' control
    /// (every 5 sec)
    /// </summary>
    private void _TimerPeriodicUpdate_Tick (object sender, System.EventArgs e)
    {
      // CD: Editor mode?
      if (!_bEditMode)
      {
        // No:

        // Check: Is a Transfer process still in progress?
        if (_bTransferInProgress)
          return; // Yes

        // Read in the service data
        _btnRead_Click (null, new System.EventArgs ());
      }
    }

    /// <summary>
    /// 'Tick' event of the '_Timer' control 
    /// (every 1 sec)
    /// </summary>
    private void _Timer_Tick (object sender, System.EventArgs e)
    {
      // Update the DateTime in 1 sec clocks
      _dtpDate.Value = DateTime.Now;
      _dtpTime.Value = DateTime.Now;

      // Update the form 
      UpdateData ();
    }

    /// <summary>
    /// 'CheckedChanged' event of the 3 '_chkPF' CheckBox controls:
    /// </summary>
    private void _chkPF_CheckedChanged (object sender, System.EventArgs e)
    {
      CheckBox chk = (CheckBox)sender;
      TextBox tb = null;
      NumericUpDown nud = null;
      if (chk == this._chkPF1) { tb = this._txtF1; nud = this._nudP1; }
      else if (chk == this._chkPF2) { tb = this._txtF2; nud = this._nudP2; }
      else if (chk == this._chkPF3) { tb = this._txtF3; nud = this._nudP3; }
      if ((null != tb) && (null != nud))
      {
        tb.Enabled = chk.Checked;   // If checked, the corr'ing TB is enabled ...
        nud.Enabled = !chk.Checked; // ... and the corr'ing NUD is disabled (& viceversa)
      }
    }

    #endregion event handling

    #region constants & enums

    // Max. length for device information data ( in Bytes )
    // NOTE:
    //  This value must correspond to ist device match!
    const int MAXDEVDATALENGTH = 15;

    // Max. length for service comment ( in Bytes ) 
    // NOTE:
    //  This value must correspond to ist device match!
    const int MAXCMTLENGTH = 4096;

    // Max. length of a string in the Service Comment string array ( in Bytes )
    // Notes:
    //  Must be <= 'MAX_PAR_LEN'
    const int _MAXWRITECMTLENGTH = 100;

    // Service comment: Substitute char's 
    // NOTE:
    //  These char's MUST NOT be equal to the replacement char for the end char, 
    //  as defined & used in the device SW and the 'CommRS232' module of this app: 'REPLCHAR' = (char) 0x01
    //    Substitute for the Comma character 
    readonly char _COMMAREPLCHAR = System.Convert.ToChar (0x02);
    //    Substitute for the Space character 
    readonly char _SPACEREPLCHAR = System.Convert.ToChar (0x03);
    //    Substitute for the LF character 
    readonly char _LFREPLCHAR = System.Convert.ToChar (0x04); // '\n'=0x0A
    //    Substitute for the CR character 
    readonly char _CRREPLCHAR = System.Convert.ToChar (0x05); // '\r'=0x0D

    /// <summary>
    /// 'ReadDataError' Enumeration:
    /// The errors that may occure on reading the Service data from the device
    /// </summary>
    enum ReadDataError
    {
      // Error in section: Device (en bloque)
      Device = 1,
      // Error in section: DateTime (detailed)
      DateTime = 2,
      ServiceDate = 4,
      // Error in section: Sensor - Ist values (en bloque)
      Actual_Sensor_Values = 8,
      // Error in section: Sensor - Soll values (detailed)
      T1_Debit = 16,
      T2_Debit = 32,
      T3_Debit = 64,
      T4_Debit = 128,
      PresOff = 256,
      P1_Debit = 512,
      P2_Debit = 1024,
      P3_Debit = 2048,
      F1_Debit = 4096,
      F2_Debit = 8192,
      F3_Debit = 16384,
      MFC = 32768,
      // Error in section: Sensor - Heater regulation values (en bloque)
      KP = 65536,
      // Error in section: Spectrum analysis (en bloque)
      SpecAnal = 131072,
    };

    /// <summary>
    /// Self-check: Min. duration of the self-check OK state (in sec) - Minimum
    /// Notes:
    ///   The value of this constant MUST conincide with the corr'ing value in the device SW. 
    /// </summary>
    const int _MIN_SELFCHECK_IN_TUBE_SEC = 1;
    /// <summary>
    /// Self-check: Min. duration of the self-check OK state (in sec) - Maximum
    /// Notes:
    ///   The value of this constant MUST conincide with the corr'ing value in the device SW. 
    /// </summary>
    const int _MAX_SELFCHECK_IN_TUBE_SEC = 24;

    #endregion constants & enums

    #region members

    /// <summary>
    /// Indicates, whether the form is opened as editor (True) or not (False)
    /// </summary>
    bool _bEditMode = false;

    /// <summary>
    /// True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    bool _bShowWait = false;

    /// <summary>
    /// The Service Data string array
    /// </summary>
    string[] _arsServiceData = new string[ServiceDataFile.NROFSVCDATASTRINGS];
    /// <summary>
    /// The number of strings in the Service Data string array
    /// </summary>
    int _anzDataStrings = ServiceDataFile.NROFSVCDATASTRINGS;
    /// <summary>
    /// The Service Data type ( representing the indizes of the Service Data string array )
    /// </summary>
    int _currDataType = 0;

    /// <summary>
    /// The Service Comment string array
    /// </summary>
    string[] _arsServiceCmt = null;
    /// <summary>
    /// The number of strings/blocks in the Service Comment string array 
    /// </summary>
    int _anzCmtBlocks = 0;
    /// <summary>
    /// The block counter
    /// </summary>
    int _currCmtBlock = 0;

    /// <summary>
    /// The default text color for the service comment (changed, if its length exceeds the
    /// permissible size)
    /// </summary>
    Color _colorCmtDefault = Color.Black;

    /// <summary>
    /// The default text color for the Sensor-Actual TextBoxes (changed, if an error spec.
    /// is encountered)
    /// </summary>
    Color _colorSensorActualDefault = Color.Black;

    /// <summary>
    /// Indicates, whether a Transfer process is curr'ly in progress ( true ) or not ( false )
    /// </summary>
    bool _bTransferInProgress = false;

    /// <summary>
    /// Name of the script, that was lastly running on the PID device
    /// </summary>
    string _sLastScriptName = string.Empty;

    /// <summary>
    /// Defines, whether only a member <-> controls test should be done (True) or if the full
    /// program should be done (False, Default)
    /// </summary>
    Boolean _TEST = false;

    /// <summary>
    /// Path of the device Service root folder
    /// </summary>
    string _sServiceFolderPath = "";
    /// <summary>
    /// Name of the curr'ly shown Service file
    /// </summary>
    string _sCurServiceFilename = "";

    #endregion members

    #region methods

    /// <summary>
    /// Performs initialisation tasks
    /// </summary>
    /// <param name="bEditMode">
    /// Indicates, whether the form is opened as editor (True) or not (False)
    /// </param>
    void _Init (bool bEditMode)
    {
      // Colors
      _colorCmtDefault = this._txtAvCmtSize.ForeColor;
      _colorSensorActualDefault = this._txtT1_Ist.ForeColor;
      // ServiceComment controls 
      this._txtComment.Text = "";
      this._txtAvCmtSize.Text = MAXCMTLENGTH.ToString ();
      // DateTime timer: 1 sec interval for DateTime update
      _Timer.Interval = 1000;
      _Timer.Enabled = true;
      // Dis-/Enabled state of Pump - Flow handling controls
      _chkPF_CheckedChanged (this._chkPF1, new EventArgs ());
      _chkPF_CheckedChanged (this._chkPF2, new EventArgs ());
      _chkPF_CheckedChanged (this._chkPF3, new EventArgs ());
      // Smoothing
      this._rbSmoothing_MA.Checked = true;
      // Peak search
      this._cbPSMode.Items.Clear ();
      this._cbPSMode.Items.AddRange (new object[] { 
                                                    PeakSearch.Modi.Unidirectional, 
                                                    PeakSearch.Modi.Bidirectional 
                                                  });
      if (bEditMode)
      {
        this._cbPSMode.SelectedItem = PeakSearch.Modi.Unidirectional;
      }
      // Peak area determination
      this._cbPAMode.Items.Clear ();
      this._cbPAMode.Items.AddRange (new object[] { 
                                                    PeakAreaDetermination.Modi.Simple, 
                                                    PeakAreaDetermination.Modi.Extended_1, 
                                                    PeakAreaDetermination.Modi.Extended_2 
                                                  });
      if (bEditMode)
      {
        this._cbPAMode.SelectedItem = PeakAreaDetermination.Modi.Simple;
      }


      // Show/Hide controls depending on the edit mode
      if (bEditMode)
      {
        // Editor mode
        this._txtMFCcap.Visible = false;
        this._lblMFCcap.Visible = false;

        this._gbComment.Visible = false;
        this._gbLoad.Visible = false;
        this._chkPeriodicUpdate.Visible = false;
        this._btnExtended.Visible = false;
        this._pgbTransfer.Visible = false;
      }
      else
      {
        // Transfer mode
        this._chkDontChangePIDlamp.Visible = false;
        this._chkDontChangeP1On.Visible = false;
        this._chkDontChangeP2On.Visible = false;
        this._chkDontChangeP3On.Visible = false;
      }

      // Gain factor 
      this._cbGainFactor.Items.Clear ();
      this._cbGainFactor.Items.AddRange (new object[] { "10.4", "29.8" });
      this._cbGainFactor.SelectedItem = this._cbGainFactor.Items[0];

      // Initiate Logo
      this._rbLogoENIT.Checked = true;

      // Overgive
      _bEditMode = bEditMode;
    }

    /// <summary>
    /// Ckecks the validity of the control contents
    /// </summary>
    /// <returns>True, if the control contents are valid; false otherwise</returns>
    Boolean _CheckControls ()
    {
      object o;

      App app = App.Instance;
      Ressources r = app.Ressources;

      //-----------------------------------
      // Section: Device

      // Device #
      if (!Check.Execute (this._txtDeviceNo, CheckType.String, CheckRelation.IN, 1, MAXDEVDATALENGTH, true, out o))
      {
        // Show detailed message
        string sFmt = r.GetString ("Service_Check_Device_Err");     // "Der eingegebene Text darf {0} Zeichen nicht übersteigen."
        string sMsg = string.Format (sFmt, MAXDEVDATALENGTH);
        string sCap = r.GetString ("Form_Common_TextError");          // "Fehler"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        return false;
      }
      // Cell #
      if (!Check.Execute (this._txtCellNo, CheckType.String, CheckRelation.IN, 1, MAXDEVDATALENGTH, true, out o))
      {
        // Show detailed message
        string sFmt = r.GetString ("Service_Check_Device_Err");     // "Der eingegebene Text darf {0} Zeichen nicht übersteigen."
        string sMsg = string.Format (sFmt, MAXDEVDATALENGTH);
        string sCap = r.GetString ("Form_Common_TextError");          // "Fehler"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        return false;
      }

      // PID lamp -> need for check only in editor mode
      if (this._bEditMode)
      {
        if (!Check.Execute (this._txtPIDlamp, CheckType.String, CheckRelation.GT, 0, 0, true, out o))
        {
          // Show detailed message
          string sMsg = r.GetString ("Service_Check_Device_OnTimes_Err");// "Die Textbox darf nicht leer sein."
          string sCap = r.GetString ("Form_Common_TextError");          // "Fehler"
          MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
          return false;
        }
      }

      // Accu display on/off -> no need to check
      // Logo RB's -> no need to check

      // Pump On Times -> need for check only in editor mode
      if (this._bEditMode)
      {
        if (!Check.Execute (this._txtP1On, CheckType.String, CheckRelation.GT, 0, 0, true, out o))
        {
          // Show detailed message
          string sMsg = r.GetString ("Service_Check_Device_OnTimes_Err");// "Die Textbox darf nicht leer sein."
          string sCap = r.GetString ("Form_Common_TextError");          // "Fehler"
          MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
          return false;
        }
        if (!Check.Execute (this._txtP2On, CheckType.String, CheckRelation.GT, 0, 0, true, out o))
        {
          // Show detailed message
          string sMsg = r.GetString ("Service_Check_Device_OnTimes_Err");// "Die Textbox darf nicht leer sein."
          string sCap = r.GetString ("Form_Common_TextError");          // "Fehler"
          MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
          return false;
        }
        if (!Check.Execute (this._txtP3On, CheckType.String, CheckRelation.GT, 0, 0, true, out o))
        {
          // Show detailed message
          string sMsg = r.GetString ("Service_Check_Device_OnTimes_Err");// "Die Textbox darf nicht leer sein."
          string sCap = r.GetString ("Form_Common_TextError");          // "Fehler"
          MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
          return false;
        }
      }

      //-----------------------------------
      // Section: ContDevice

      // Self-check
      if (!Check.Execute (this._txtSelfCheck, CheckType.Int, CheckRelation.IN, _MIN_SELFCHECK_IN_TUBE_SEC, _MAX_SELFCHECK_IN_TUBE_SEC, true, out o))
        return false;

      // Error confirmation on/off -> no need to check

      // PW on/off -> no need to check

      // PW
      if (!Check.Execute (this._txtPW, CheckType.Int, CheckRelation.IN, 0, 9999, true, out o))
      {

        string sMsg = r.GetString ("Service_Check_Device_PW_Err");        // "Ein gültiges Passwort besteht aus bis zu 4 Ziffern."
        string sCap = r.GetString ("Form_Common_TextInfo");               // "Info"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
        this._txtPW.Focus ();
        return false;
      }

      //-----------------------------------
      // Section: DateTime

      // Date
      if (!Check.Execute (this._dtpDate, CheckType.DateTime, CheckRelation.None, 0, 0, true, out o))
        return false;
      // Time
      if (!Check.Execute (this._dtpTime, CheckType.DateTime, CheckRelation.None, 0, 0, true, out o))
        return false;
      // Service Date
      if (!Check.Execute (this._dtpSvcDate, CheckType.DateTime, CheckRelation.None, 0, 0, true, out o))
        return false;

      //-----------------------------------
      // Section: Sensor

      // T1
      if (!this._chkT1Off.Checked)
      {
        if (!Check.Execute (this._nudT1_Soll, CheckType.Int, CheckRelation.IN, 40, 250, true, out o))
          return false;
      }
      // T2
      if (!this._chkT2Off.Checked)
      {
        if (!Check.Execute (this._nudT2_Soll, CheckType.Int, CheckRelation.IN, 40, 250, true, out o))
          return false;
      }
      // T3
      if (!this._chkT3Off.Checked)
      {
        if (!Check.Execute (this._nudT3_Soll, CheckType.Int, CheckRelation.IN, 40, 250, true, out o))
          return false;
      }
      // T4
      if (!this._chkT4Off.Checked)
      {
        if (!Check.Execute (this._nudT4_Soll, CheckType.Int, CheckRelation.IN, 40, 250, true, out o))
          return false;
      }

      // Pressure display on/off
      if (!this._chkPresOff.Checked)
      {
      }

      // P1/F1 
      // Notes:
      //  If the corr'ing P-F handling ChB is NOT checked, then the pump value is editable & the flow value
      //  is disabled (shown only).
      //  If the corr'ing P-F handling ChB is checked, then the flow value is editable & the pump value
      //  is disabled (shown only).
      if (!this._chkPF1.Checked)
      {
        if (!Check.Execute (this._nudP1, CheckType.Int, CheckRelation.IN, 0, 255, true, out o))
          return false;
      }
      else
      {
        if (!Check.Execute (this._txtF1, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o))
          return false;
      }
      // P2/F2 (dito)
      if (!this._chkPF2.Checked)
      {
        if (!Check.Execute (this._nudP2, CheckType.Int, CheckRelation.IN, 0, 255, true, out o))
          return false;
      }
      else
      {
        if (!Check.Execute (this._txtF2, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o))
          return false;
      }
      // P3/F3 (dito)
      if (!this._chkPF3.Checked)
      {
        if (!Check.Execute (this._nudP3, CheckType.Int, CheckRelation.IN, 0, 255, true, out o))
          return false;
      }
      else
      {
        if (!Check.Execute (this._txtF3, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o))
          return false;
      }

      // F1
      //  Soll: in [0, 1023]
      if (!Check.Execute (this._txtF1_Soll, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o))
        return false;
      //  Soll - abs. tolerance: in [0, 1023]
      if (!Check.Execute (this._txtF1_Soll_TolAbs, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o))
        return false;
      //  Soll tube (+/-): in [0, 1023]
      //  Notes:
      //    Because the default settings (Soll: 512, Abs. tol.: 512) should be fulfilled, the permissible
      //    range must be set here to [0, 1024]. The observance of the exact range [0, 1023] must be
      //    ensured on the device. 
      int iSoll = int.Parse (this._txtF1_Soll.Text);
      int iSollTolAbs = int.Parse (this._txtF1_Soll_TolAbs.Text);
      int ug = iSoll - iSollTolAbs;
      int og = iSoll + iSollTolAbs;
      if ((ug < 0) || (og > 1024))
      {
        string sFmt = r.GetString ("Service_Check_Sensor_Flow_Err");    // "Ungültiger Flußbereich\r\n(Ein gültiger Wert ist in [{0}, {1}].)"
        string sMsg = string.Format (sFmt, 0, 1024);
        string sCap = r.GetString ("Form_Common_TextError");              // "Fehler"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        this._txtF1_Soll.Focus ();
        return false;
      }
      // F2
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
      //  Soll
      if (!Check.Execute (this._txtF2_Soll, CheckType.Float, CheckRelation.GE, 0, 0, true, out o))
        return false;
      float fSoll = float.Parse (this._txtF2_Soll.Text, nfi);
      //  Check Soll vs. MFC capacity -> need for check only, if not in editor mode
      if (!_bEditMode)
      {
        //  MFC capacity (check not required in principle, due to its ReadOnly property)
        if (!Check.Execute (this._txtMFCcap, CheckType.Int, CheckRelation.GE, 0, 0, true, out o))
          return false;
        int nMFCcap = int.Parse (this._txtMFCcap.Text);
        //  Check: MFC present?
        if (nMFCcap > 0)
        {
          // Yes:
          // Check: Does the F2 Soll value exceed the MFC capacity value?
          if (fSoll > nMFCcap)
          {
            // Yes:
            // Error: Soll value must be less than or equal the MFC capacity
            string sFmt = r.GetString ("Service_Check_Sensor_Flow_Err2");     // "Ungültiger Fluß\r\n(Ein gültiger Wert ist <= {0}.)"
            string sMsg = string.Format (sFmt, nMFCcap);
            string sCap = r.GetString ("Form_Common_TextError");              // "Fehler"
            MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
            this._txtF2_Soll.Focus ();
            return false;
          }
        }
      }
      //  Soll - abs. tolerance
      if (!Check.Execute (this._txtF2_Soll_TolAbs, CheckType.Float, CheckRelation.GE, 0, 0, true, out o))
        return false;
      float fSollTolAbs = float.Parse (this._txtF2_Soll_TolAbs.Text, nfi);
      float fug = fSoll - fSollTolAbs;
      if (fug < 0)
      {
        // Error: Soll tube must be in the positive range
        string sMsg = r.GetString ("Service_Check_Sensor_Flow_Err1");     // "Ungültiger Flußbereich\r\n(Ein gültiger Bereich beinhaltet ausschließlich positive Werte.)"
        string sCap = r.GetString ("Form_Common_TextError");              // "Fehler"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        this._txtF2_Soll_TolAbs.Focus ();
        return false;
      }
      // F3
      //  Soll
      if (!Check.Execute (this._txtF3_Soll, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o))
        return false;
      //  Soll - abs. tolerance
      if (!Check.Execute (this._txtF3_Soll_TolAbs, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o))
        return false;
      //  Soll tube (+/-): in [0, 1023]
      //  (Notes see above)
      iSoll = int.Parse (this._txtF3_Soll.Text);
      iSollTolAbs = int.Parse (this._txtF3_Soll_TolAbs.Text);
      ug = iSoll - iSollTolAbs;
      og = iSoll + iSollTolAbs;
      if ((ug < 0) || (og > 1024))
      {
        string sFmt = r.GetString ("Service_Check_Sensor_Flow_Err");    // "Ungültiger Flußbereich\r\n(Ein gültiger Wert ist in [{0}, {1}].)"
        string sMsg = string.Format (sFmt, 0, 1024);
        string sCap = r.GetString ("Form_Common_TextError");              // "Fehler"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        this._txtF3_Soll.Focus ();
        return false;
      }

      // 'P' parts for regulation of heater1-4: in (0, 255]
      if (!Check.Execute (this._txtKP1, CheckType.Int, CheckRelation.IN, 1, 255, true, out o))
        return false;
      if (!Check.Execute (this._txtKP2, CheckType.Int, CheckRelation.IN, 1, 255, true, out o))
        return false;
      if (!Check.Execute (this._txtKP3, CheckType.Int, CheckRelation.IN, 1, 255, true, out o))
        return false;
      if (!Check.Execute (this._txtKP4, CheckType.Int, CheckRelation.IN, 1, 255, true, out o))
        return false;

      //-----------------------------------
      // Section: Spectrum analysis

      // Scan offset
      if (!Check.Execute (this._txtScanOffset_Min, CheckType.Int, CheckRelation.IN, 0, 32767 - 1, true, out o))
        return false;
      int offmin = int.Parse (_txtScanOffset_Min.Text);
      if (!Check.Execute (this._txtScanOffset_Max, CheckType.Int, CheckRelation.IN, offmin + 1, 32767, true, out o))
        return false;
      // Peak search: 'n' sigma factor
      if (!Check.Execute (this._txtPeakSearchNsig, CheckType.Int, CheckRelation.GE, 0, 0, true, out o))
        return false;
      // Peak search: mode
      //    no need in checking
      // Peak area determination: 'n' sigma factor
      if (!Check.Execute (this._txtPeakAreaDetNsig, CheckType.Int, CheckRelation.GE, 0, 0, true, out o))
        return false;
      // Peak area determination: mode
      //    no need in checking
      // Gain factor
      //    no need in checking
      // Smoothing: MA-Filter width
      if (!Check.Execute (this._txtSmoothing_MA_width, CheckType.Int, CheckRelation.IN, 3, 11, true, out o))
        return false;
      int width = int.Parse (this._txtSmoothing_MA_width.Text);
      if ((width % 2) == 0)
      {
        // Show detailed message
        string sMsg = r.GetString ("Service_Check_Smooth_Err");     // "Die Filterbreite muß eine ungerade Zahl sein."
        string sCap = r.GetString ("Form_Common_TextError");          // "Fehler"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        this._txtSmoothing_MA_width.Focus ();
        return false;
      }
      // Smoothing: SG-Filter width
      if (!Check.Execute (this._txtSmoothing_SG_width, CheckType.Int, CheckRelation.IN, 3, 41, true, out o))
        return false;
      width = int.Parse (this._txtSmoothing_SG_width.Text);
      if ((width % 2) == 0)
      {
        // Show detailed message
        string sMsg = r.GetString ("Service_Check_Smooth_Err");     // "Die Filterbreite muß eine ungerade Zahl sein."
        string sCap = r.GetString ("Form_Common_TextError");          // "Fehler"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        this._txtSmoothing_SG_width.Focus ();
        return false;
      }

      //-----------------------------------
      // Section: Comment

      // Comment
      if (!Check.Execute (this._txtComment, CheckType.String, CheckRelation.LE, MAXCMTLENGTH, 0, true, out o))
        return false;

      // ready
      return true;
    }

    /// <summary>
    /// Updates the form
    /// </summary>
    void UpdateData ()
    {
      // CD: Editor mode?
      if (_bEditMode)
      {
        // Yes

        // Button 'Write'
        // Notes:
        //  In this mode the 'Write' button acts as 'Save' button:
        //  The control contents are stored into a service data file. 
        //  (Note, that the enabled state of this button depends from the EnkyLC adjustments)
        this._btnWrite.Enabled = EnkyLC.bEditSvc;
      }
      else
      {
        // No
        App app = App.Instance;
        Doc doc = app.Doc;
        AppComm comm = app.Comm;

        Boolean bEn =
          (doc.sVersion.Length > 0) && comm.IsOpen () &&
          (_bTransferInProgress == false) && (this._chkPeriodicUpdate.Checked == false);

        // Button 'Read'
        // Notes:
        //  In this mode the 'Read' button acts as such:
        //  The control contents are filled with the service data, read from device. 
        this._btnRead.Enabled = bEn;

        // Button 'Write'
        // Notes:
        //  In this mode the 'Write' button acts as such:
        //  The control contents are transferred to the device. 
        //  (Note, that the enabled state of this button also depends from the EnkyLC adjustments)
        Boolean bEn1 = bEn && EnkyLC.bEditSvc;
        this._btnWrite.Enabled = bEn1;

        // Button 'Extended'
        // (Note, that the enabled state of this button depends from the EnkyLC adjustments)
        this._btnExtended.Enabled = bEn && EnkyLC.bEditSvcExt;

        // Button 'Load'
        // (Note, that the enabled state of this button also depends from the EnkyLC adjustments)
        Boolean bEn2 = bEn && (this._lbSvcFiles.Items.Count > 0) && EnkyLC.bEditSvc;
        this._btnLoad.Enabled = bEn2;

        // Progress bar
        this._pgbTransfer.Enabled = doc.sVersion.Length > 0;
      }

      // Common for both modi:
      // Password: 
      //  Display it plain-text in case that the user is allowed to edit service data
      //  Display it password-like in case that the user is NOT allowed to edit service data
      char c = (EnkyLC.bEditSvc) ? (char)0 : (char)'*';
      this._txtPW.PasswordChar = c;
    }

    /// <summary>
    /// Updates the ServiceData string based on the control contents (DDX = true) or viceversa (DDX = false). 
    /// </summary>
    /// <param name="bDDX">The DDX direction</param>
    void _UpdateServiceData (Boolean bDDX)
    {
      if (bDDX)
      {
        // Controls -> Members ( WRITE (to device)):

        // Build the ServiceData string array
        // Notes:
        //  1.
        //  The Servicedata string has depending on the Servicedata type the following format:
        //
        //  Servicedata type        Servicedata string
        // ------------------------------------------------------------------------------------------------------------
        //  0 (device data: 12)     "<device#>TAB<cell#>TAB<PID lamptime>TAB<accu_display>TAB<logo>TAB
        //                           <P1On time>TAB<P2On time>TAB<P3On time>TAB<self-check>TAB<ErrConf>TAB
        //                           <PWOnOff>TAB<PW>TAB"
        //  1 (DateTime data: 2)    "<DT>TAB<DTService>TAB" 
        //  2 (sensor data: 7+1+6+4)
        //                          "<T1>TAB<T2>TAB<T3>TAB<T4>TAB<P1>TAB<P2>TAB<P3>TAB" +
        //                          "<PresDispOnOff>TAB" +
        //                          "<F1_soll>TAB<F1_soll_tolabs>TAB<F2_soll>TAB<F2_soll_tolabs>TAB<F3_soll>TAB<F3_soll_tolabs>TAB" +
        //                          "<Ppart_T1>TAB<Ppart_T2>TAB<Ppart_T3>TAB<Ppart_T4>TAB"
        //  3 (spectrum ananysis data: 10)
        //    (Scan offset: 2)      "<scan offset min>TAB<scan offset max>TAB" +
        //    (Peak search: 2)      "<PeakSearch_Nsig_fac>TAB<PeakSearch_Mode>TAB" +
        //    (Peak area det.: 2)   "<PeakAreaDet_Nsig_fac>TAB<PeakAreaDet_Mode>TAB" +
        //    (Gain factor: 1)      "<Gain factor>TAB"
        //    (Smoothing: 3)        "<Filter_type>TAB<MA_filter_width>TAB<SG_filter_width>TAB"
        //
        //  The DateTime string 'DT' has the following format:
        //  "<Y:2B><M:2B><D:2B><H:2B><M:2B><S:2B>"
        //
        //  The Service Date string 'DTSvc' has the following format:
        //  "<Y:2B><M:2B><D:2B>"
        //
        //  2.
        //  a) 
        //  The length of a whole parameter string should not exceed 'MAX_PAR_LEN' bytes. Because of the 
        //  'SvcDataType' token the Servicedata string should not exceed a length of ('MAX_PAR_LEN'-2) B.
        //  b)
        //  The Servicedata string should not exceed a length of 127 B (see device SW, Notes for 'sf_svcwritedata()').
        //
        //  With respect to a) & b): length of the Servicedata string < Min ('MAX_PAR_LEN'-2, 127) -> This must be proofed for!
        //        
        //  Here we have appr. as an upper margin the following Byte #:
        //
        //  Servicedata type    Servicedata string length (B)
        // ------------------------------------------------------------------------------------------------------------
        //  0                   2 x (15+1) + (4+1) + 2 x (1+1) + 3 x (4+1) + 1 x (2+1) +  2 x (1+1) + 1 x (4+1)         = 68
        //  1                   1 x (12+1) + 1 x (6+1)                                                          = 13+7  = 20
        //  2                   7 x (3+1) + (1+1) + 6 x (4+1) + 4 x (1+1)                                               = 68
        //  3                   2 x (5+1) + 2 x (1+1) + 2 x (1+1) + (4+1) + 3 x (2+1)                                   = 34

        // ---------------------------------
        //  Section: Device

        //    PID Lamptime
        //    Notes:
        //      On the device the lamptime is handled in (10 min) units, here the display is in h units.
        //      Thats why a scaling must be performed.
        string sH = this._txtPIDlamp.Text;                // Lamptime in (h) units
        int nLampTime = int.Parse (sH);                   // dito
        nLampTime *= 6;                                   // Lamptime in (10min) units
        string s10min = nLampTime.ToString ();            // dito
        //    Logo
        int nLogo;
        if (this._rbLogoIUT.Checked) nLogo = (int)Logo.IUT;
        else if (this._rbLogoENIT.Checked) nLogo = (int)Logo.ENIT;
        else if (this._rbLogoENVI.Checked) nLogo = (int)Logo.ENVI;
        else nLogo = (int)Logo.ENIT;   // def.
        //    Pump On times
        //    Notes:
        //      On the device the P1/2/3On times are handled in (1 min) units, here the display is in h units.
        //      Thats why a scaling must be performed.
        sH = this._txtP1On.Text;                          // P1On time in (h) units
        int nPumpTime = int.Parse (sH);                   // dito
        nPumpTime *= 60;                                  // P1On time in (1min) units
        string s1min = nPumpTime.ToString ();             // dito
        sH = this._txtP2On.Text;                          // P2On time in (h) units
        nPumpTime = int.Parse (sH);                       // dito
        nPumpTime *= 60;                                  // P2On time in (1min) units
        string s2min = nPumpTime.ToString ();             // dito
        sH = this._txtP3On.Text;                          // P3On time in (h) units
        nPumpTime = int.Parse (sH);                       // dito
        nPumpTime *= 60;                                  // P3On time in (1min) units
        string s3min = nPumpTime.ToString ();             // dito
        //    Composed
        _arsServiceData[0] = string.Format ("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t",
          this._txtDeviceNo.Text,
          this._txtCellNo.Text,
          s10min,
          (this._chkAccuDisplay.Checked ? "1" : "0"),
          nLogo,
          s1min,
          s2min,
          s3min,
          this._txtSelfCheck.Text,
          (this._chkErrConf.Checked ? "1" : "0"),
          (this._chkPWOnOff.Checked ? "1" : "0"),
          this._txtPW.Text
         );

        // ---------------------------------
        // Section: DateTime

        //    Build the DateTime string
        DateTime dt1 = _dtpDate.Value; // Date
        DateTime dt2 = _dtpTime.Value; // Time
        string sDT = string.Format ("{0:D2}{1:D2}{2:D2}{3:D2}{4:D2}{5:D2}",
          dt1.Year - 2000, dt1.Month, dt1.Day, dt2.Hour, dt2.Minute, dt2.Second
          );
        //    Build the Service Date string
        DateTime dtSvc = this._dtpSvcDate.Value;
        string sDTSvc = string.Format ("{0:D2}{1:D2}{2:D2}",
          dtSvc.Year - 2000, dtSvc.Month, dtSvc.Day
          );
        //    Service data string   
        _arsServiceData[1] = string.Format ("{0}\t{1}\t",
          sDT,
          sDTSvc
          );

        // ---------------------------------
        // Section: Sensor

        //    P1-F1 value
        string sPF1 = (this._chkPF1.Checked ? this._txtF1.Text : this._nudP1.Value.ToString ());
        UInt16 wPF1 = UInt16.Parse (sPF1);
        if (this._chkPF1.Checked) wPF1 |= 0x8000; // Bit 15 set / reset if F1 is regulated / not regulated 
        //    P2-F2 value
        string sPF2 = (this._chkPF2.Checked ? this._txtF2.Text : this._nudP2.Value.ToString ());
        UInt16 wPF2 = UInt16.Parse (sPF2);
        if (this._chkPF2.Checked) wPF2 |= 0x8000; // Bit 15 set / reset if F2 is regulated / not regulated
        //    P3-F3 value
        string sPF3 = (this._chkPF3.Checked ? this._txtF3.Text : this._nudP3.Value.ToString ());
        UInt16 wPF3 = UInt16.Parse (sPF3);
        if (this._chkPF3.Checked) wPF3 |= 0x8000; // Bit 15 set / reset if F3 is regulated / not regulated
        //    Service data string 
        _arsServiceData[2] = string.Format ("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t",
          // 4 x Temp's
          (this._chkT1Off.Checked ? "0" : this._nudT1_Soll.Value.ToString ()),
          (this._chkT2Off.Checked ? "0" : this._nudT2_Soll.Value.ToString ()),
          (this._chkT3Off.Checked ? "0" : this._nudT3_Soll.Value.ToString ()),
          (this._chkT4Off.Checked ? "0" : this._nudT4_Soll.Value.ToString ()),
          // 3 x flow resp. pump values
          wPF1.ToString (),
          wPF2.ToString (),
          wPF3.ToString (),
          // Pressure display on/off spec.: Checked = Pressure display off = 0, Unchecked = Pressure display on = 1
          (this._chkPresOff.Checked ? "0" : "1"),
          // 3 x Flow Soll-Ist
          this._txtF1_Soll.Text,
          this._txtF1_Soll_TolAbs.Text,
          this._txtF2_Soll.Text,
          this._txtF2_Soll_TolAbs.Text,
          this._txtF3_Soll.Text,
          this._txtF3_Soll_TolAbs.Text,
          // 4 x 'P' parts for heater regulation
          this._txtKP1.Text,
          this._txtKP2.Text,
          this._txtKP3.Text,
          this._txtKP4.Text
        );

        // ---------------------------------
        // Section: Spectrum analysis

        //    Peak search mode
        string sPS = this._cbPSMode.SelectedItem.ToString ();
        int nPS;
        switch (sPS)
        {
          case PeakSearch.Modi.Unidirectional: nPS = 0; break;
          default:
          case PeakSearch.Modi.Bidirectional: nPS = 1; break;
        }
        //    Peak area determination mode
        string sPA = this._cbPAMode.SelectedItem.ToString ();
        int nPA;
        switch (sPA)
        {
          case PeakAreaDetermination.Modi.Simple: nPA = 0; break;
          case PeakAreaDetermination.Modi.Extended_1: nPA = 1; break;
          default:
          case PeakAreaDetermination.Modi.Extended_2: nPA = 2; break;
        }
        //    Gain factor: Format %.1f
        //    Notes:
        //       Exception handling for float parsing is not required here, because the contents
        //       of all controls have been checked for correctness prev'ly
        System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
        string suf = this._cbGainFactor.SelectedItem.ToString ().Replace (',', '.');    // Unformatted string
        float f = float.Parse (suf, nfi);                                               // Corr'ing float value
        string sf = string.Format (nfi, "{0:F1}", f);                                   // Formatted string
        //    Service data string
        _arsServiceData[3] = string.Format ("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t",
          this._txtScanOffset_Min.Text,                   // Min. scan offset
          this._txtScanOffset_Max.Text,                   // Max. scan offset
          this._txtPeakSearchNsig.Text,                   // Peak search: 'n' sigma factor
          nPS,                                            // Peak search: mode
          this._txtPeakAreaDetNsig.Text,                  // Peak area determination: 'n' sigma factor
          nPA,                                            // Peak area determination: mode
          sf,                                             // Gain factor
          // Smoothing
          this._rbSmoothing_MA.Checked ? 0 : 1,           // Filter type: 0 - MA, 1 - SG
          this._txtSmoothing_MA_width.Text,               // MA filter width
          this._txtSmoothing_SG_width.Text                // SG filter width
          );
      }
      else
      {
        // Members -> Controls ( READ (from device)):

        // Parse the ServiceData string array and Update the control contents
        // Notes:
        //  1.
        //  The Servicedata string has depending on the Servicedata type the following format:
        //
        //  Servicedata type        Servicedata string
        // ------------------------------------------------------------------------------------------------------------
        //  0 (device data: 12)     "<device#>TAB<cell#>TAB<PID lamptime>TAB<accu_display>TAB<logo>TAB
        //                           <P1On time>TAB<P2On time>TAB<P3On time>TAB<self-check>TAB<ErrConf>TAB
        //                           <PWOnOff>TAB<PW>TAB"
        //  1 (DateTime data: 2)    "<DT>TAB<DTService>TAB" 
        //  2 (sensor data: 8+1+9+6+4) 
        //                          "<T1>TAB<T2>TAB<T3>TAB<T4>TAB<Pres>TAB<F1>TAB<F2>TAB<F3>TAB" +
        //                          "<PresDispOnOff>TAB" +
        //                          "<T1_soll>TAB<T2_soll>TAB<T3_soll>TAB<T4_soll>TAB<P1_soll>TAB<P2_soll>TAB<P3_soll>TAB<MFC_debit>TAB<MFC_capacity>TAB
        //                           <F1_soll>TAB<F1_soll_tolabs>TAB<F2_soll>TAB<F2_soll_tolabs>TAB<F3_soll>TAB<F3_soll_tolabs>TAB" +
        //                          "<Ppart_T1>TAB<Ppart_T2>TAB<Ppart_T3>TAB<Ppart_T4>TAB"
        //  3 (spectrum analysis data: 11)
        //    (Scan offset: 2)      "<scan offset min>TAB<scan offset max>TAB" +
        //    (Peak search: 2)      "<PeakSearch_Nsig_fac>TAB<PeakSearch_Mode>TAB" +
        //    (Peak area det.: 2)   "<PeakAreaDet_Nsig_fac>TAB<PeakAreaDet_Mode>TAB" +
        //    (Gain factor: 1)      "<Gain factor>TAB" +
        //    (Smoothing: 3)        "<Filter_type>TAB<MA_filter_width>TAB<SG_filter_width>TAB" +
        //    (Stat. offset: 1)     "<Stat_off>TAB"
        //
        //  The DateTime string 'DT' has the following format:
        //  "<Y:2B><M:2B><D:2B><H:2B><M:2B><S:2B>"
        //
        //  The Service Date string 'DTSvc' has the following format:
        //  "<Y:2B><M:2B><D:2B>"
        //
        //  2.
        //  Concerning the string length:
        //  Here we have appr. as an upper margin the following Byte #:
        //
        //  Servicedata type    Servicedata string length (B)
        // ------------------------------------------------------------------------------------------------------------
        //  0                   2 x (15+1) + (4+1) + 2 x (1+1) + 3 x (4+1) + 1 x (2+1) + 2 x (1+1) + 1 x (4+1)                      = 68
        //  1                   1 x (12+1) + 1 x (6+1)                                                          = 13+7              = 20
        //  2                   5 x (5+1) + 3 x (4+1) + (1+1) +                                                 = 30+15+2 + 
        //                      4 x (3+1) + 5 x (4+1) + 6 x (4+1) + 4 x (3+1)                                     16+25+30+16       = 134
        //  3                   2 x (5+1) + 2 x (1+1) + 2 x (1+1) + (4+1) + 3 x (2+1) + (6+1)                                       = 41

        // Reset Error handling
        int nError = 0;
        this._txtError.Text = "";

        // ---------------------------------
        // Section: Device

        string[] ars = _arsServiceData[0].Split ('\t');
        try
        {
          this._txtDeviceNo.Text = ars[0];
          this._txtCellNo.Text = ars[1];
          // PID Lamptime
          // Notes:
          //  On the device the lamptime is handled in (10 min) units, here the display is in h units.
          //  Thats why a scaling must be performed.
          string s10min = ars[2];                           // Lamptime in (10min) units
          int nLampTime = int.Parse (s10min);               // dito
          nLampTime /= 6;                                   // Lamptime in (h) units
          this._txtPIDlamp.Text = nLampTime.ToString ();    // dito
          // Accu display: on/off         
          int nAccuDisplay = int.Parse (ars[3]);
          this._chkAccuDisplay.Checked = (nAccuDisplay == 1) ? true : false;
          // Logo
          int nLogo = int.Parse (ars[4]);
          switch (nLogo)
          {
            case (int)Logo.IUT: this._rbLogoIUT.Checked = true; break;
            default:
            case (int)Logo.ENIT: this._rbLogoENIT.Checked = true; break;
            case (int)Logo.ENVI: this._rbLogoENVI.Checked = true; break;
          }
          // Pump On times
          // Notes:
          //  On the device the P1/2/3On times are handled in (1 min) units, here the display is in h units.
          //  Thats why a scaling must be performed.
          string s1min = ars[5];                            // P1On time in (1min) units
          int nPumpTime = int.Parse (s1min);                // ditott
          nPumpTime /= 60;                                  // P1On time in (h) units
          this._txtP1On.Text = nPumpTime.ToString ();       // dito
          string s2min = ars[6];                            // P2On time in (1min) units
          nPumpTime = int.Parse (s2min);                    // dito
          nPumpTime /= 60;                                  // P2On time in (h) units
          this._txtP2On.Text = nPumpTime.ToString ();       // dito
          string s3min = ars[7];                            // P3On time in (1min) units
          nPumpTime = int.Parse (s3min);                    // dito
          nPumpTime /= 60;                                  // P3On time in (h) units
          this._txtP3On.Text = nPumpTime.ToString ();       // dito
          // Self-check
          this._txtSelfCheck.Text = ars[8];
          // Error confirmation: on/off         
          int nErrConf = int.Parse (ars[9]);
          this._chkErrConf.Checked = (nErrConf == 1) ? true : false;
          // PW check: on/off         
          int nPWOnOff = int.Parse (ars[10]);
          this._chkPWOnOff.Checked = (nPWOnOff == 1) ? true : false;
          // PW
          string s = ars[11];
          this._txtPW.Text = s.PadLeft (4, '0');
        }
        catch
        {
          nError |= (int)ReadDataError.Device;
        }

        // ---------------------------------
        // Section: DateTime

        ars = _arsServiceData[1].Split ('\t');
        try
        {
          string sDT = ars[0];
          DateTime dt = new DateTime (
            2000 + int.Parse (sDT.Substring (0, 2)),
            int.Parse (sDT.Substring (2, 2)),
            int.Parse (sDT.Substring (4, 2)),
            int.Parse (sDT.Substring (6, 2)),
            int.Parse (sDT.Substring (8, 2)),
            int.Parse (sDT.Substring (10, 2))
            );
          this._dtpDate.Value = dt;
          this._dtpTime.Value = dt;
        }
        catch
        {
          nError |= (int)ReadDataError.DateTime;
        }

        try
        {
          string sDTSvc = ars[1];
          DateTime dtSvc = new DateTime (
            2000 + int.Parse (sDTSvc.Substring (0, 2)),
            int.Parse (sDTSvc.Substring (2, 2)),
            int.Parse (sDTSvc.Substring (4, 2)),
            0,
            0,
            0
            );
          this._dtpSvcDate.Value = dtSvc;
        }
        catch
        {
          nError |= (int)ReadDataError.ServiceDate;
        }

        // ---------------------------------
        // Section: Sensor

        // 1. Ist values
        //    Notes: 
        //      In case of an AB error the following error spec's are returned in place of
        //      the value of the corr'ing sensor:
        //      E1 - Sensor error (No sensor connected)
        //      E2 - IC error (IC not recognized)
        //      E3 - Fuse error (Fuse has blown)
        //      E4 - Comm. error
        ars = _arsServiceData[2].Split ('\t');
        try
        {
          _CheckErrorSpec (this._txtT1_Ist, ars[0]);
          _CheckErrorSpec (this._txtT2_Ist, ars[1]);
          _CheckErrorSpec (this._txtT3_Ist, ars[2]);
          _CheckErrorSpec (this._txtT4_Ist, ars[3]);

          _CheckErrorSpec (this._txtPres, ars[4]);

          _CheckErrorSpec (this._txtF1_Ist, ars[5]);
          _CheckErrorSpec (this._txtF2_Ist, ars[6]);
          _CheckErrorSpec (this._txtF3_Ist, ars[7]);
        }
        catch
        {
          nError |= (int)ReadDataError.Actual_Sensor_Values;
        }

        // 2. Pressure display on/off
        int nPresOff;
        try
        {
          // Pressure display on/off
          nPresOff = int.Parse (ars[8]);
          this._chkPresOff.Checked = (nPresOff == 0) ? true : false;
          _chkPresOff_CheckedChanged (this._chkPresOff, new EventArgs ());
        }
        catch
        {
          nError |= (int)ReadDataError.PresOff;
        }

        // 3. Soll values 
        // 3.1. Default values string
        int nTemp;
        try
        {
          // T1_soll
          nTemp = int.Parse (ars[9]);
          this._chkT1Off.Checked = (nTemp == 0) ? true : false;
          _chkTOff_CheckedChanged (this._chkT1Off, new EventArgs ());
          if (!this._chkT1Off.Checked)
            this._nudT1_Soll.Value = nTemp;
        }
        catch
        {
          nError |= (int)ReadDataError.T1_Debit;
        }
        try
        {
          // T2_soll
          nTemp = int.Parse (ars[10]);
          this._chkT2Off.Checked = (nTemp == 0) ? true : false;
          _chkTOff_CheckedChanged (this._chkT2Off, new EventArgs ());
          if (!this._chkT2Off.Checked)
            this._nudT2_Soll.Value = nTemp;
        }
        catch
        {
          nError |= (int)ReadDataError.T2_Debit;
        }
        try
        {
          // T3_soll
          nTemp = int.Parse (ars[11]);
          this._chkT3Off.Checked = (nTemp == 0) ? true : false;
          _chkTOff_CheckedChanged (this._chkT3Off, new EventArgs ());
          if (!this._chkT3Off.Checked)
            this._nudT3_Soll.Value = nTemp;
        }
        catch
        {
          nError |= (int)ReadDataError.T3_Debit;
        }
        try
        {
          // T4_soll
          nTemp = int.Parse (ars[12]);
          this._chkT4Off.Checked = (nTemp == 0) ? true : false;
          _chkTOff_CheckedChanged (this._chkT4Off, new EventArgs ());
          if (!this._chkT4Off.Checked)
            this._nudT4_Soll.Value = nTemp;
        }
        catch
        {
          nError |= (int)ReadDataError.T4_Debit;
        }

        int nPump;
        try
        {
          // P1-F1 value:
          // 
          nPump = int.Parse (ars[13]);
          this._chkPF1.Checked = ((nPump & 0x8000) != 0);
          _chkPF_CheckedChanged (this._chkPF1, new EventArgs ());
          if (!this._chkPF1.Checked)
            this._nudP1.Value = nPump;
          else
          {
            nPump &= ~0x8000;
            this._txtF1.Text = nPump.ToString ();
          }
        }
        catch
        {
          nError |= (int)ReadDataError.P1_Debit;
        }
        try
        {
          // P2-F2 value (dito)
          nPump = int.Parse (ars[14]);
          this._chkPF2.Checked = ((nPump & 0x8000) != 0);
          _chkPF_CheckedChanged (this._chkPF2, new EventArgs ());
          if (!this._chkPF2.Checked)
            this._nudP2.Value = nPump;
          else
          {
            nPump &= ~0x8000;
            this._txtF2.Text = nPump.ToString ();
          }
        }
        catch
        {
          nError |= (int)ReadDataError.P2_Debit;
        }
        try
        {
          // P3-F3 value (dito)
          nPump = int.Parse (ars[15]);
          this._chkPF3.Checked = ((nPump & 0x8000) != 0);
          _chkPF_CheckedChanged (this._chkPF3, new EventArgs ());
          if (!this._chkPF3.Checked)
            this._nudP3.Value = nPump;
          else
          {
            nPump &= ~0x8000;
            this._txtF3.Text = nPump.ToString ();
          }
        }
        catch
        {
          nError |= (int)ReadDataError.P3_Debit;
        }
        // MFC debit (integer value)
        // Notes:
        //  This item is substantially the same as 'F2 Soll' below, with the difference, that this item contains the MFC debit value in promille units,
        //  whereas the 'F2 Soll' value below contains the MFC debit value in flow units (ml/min).
        try
        {
          int nMFCSoll = int.Parse (ars[16]);
        }
        catch
        {
          nError |= (int)ReadDataError.MFC;
        }
        // MFC capacity (float value)
        try
        {
          System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
          float nMFCcap = float.Parse (ars[17], nfi);
          this._txtMFCcap.Text = nMFCcap.ToString ();
        }
        catch
        {
          nError |= (int)ReadDataError.MFC;
        }

        // 3.2. Flow
        int nFlow;
        float fFlow;
        try
        {
          // F1: Soll
          nFlow = int.Parse (ars[18]);
          this._txtF1_Soll.Text = nFlow.ToString ();
          // F1: Soll - abs. tolerance
          nFlow = int.Parse (ars[19]);
          this._txtF1_Soll_TolAbs.Text = nFlow.ToString ();
        }
        catch
        {
          nError |= (int)ReadDataError.F1_Debit;
        }
        try
        {
          System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
          // F2 (MFC controlled): Soll
          fFlow = float.Parse (ars[20], nfi);
          this._txtF2_Soll.Text = fFlow.ToString ("F1", nfi);
          // F2 (MFC controlled): Soll - abs. tolerance
          fFlow = float.Parse (ars[21], nfi);
          this._txtF2_Soll_TolAbs.Text = fFlow.ToString ("F1", nfi);
        }
        catch
        {
          nError |= (int)ReadDataError.F2_Debit;
        }
        try
        {
          // F3: Soll
          nFlow = int.Parse (ars[22]);
          this._txtF3_Soll.Text = nFlow.ToString ();
          // F3: Soll - abs. tolerance
          nFlow = int.Parse (ars[23]);
          this._txtF3_Soll_TolAbs.Text = nFlow.ToString ();
        }
        catch
        {
          nError |= (int)ReadDataError.F3_Debit;
        }

        // 4. Heater regulation values 

        int nP;
        try
        {
          nP = int.Parse (ars[24]);
          this._txtKP1.Text = nP.ToString ();
          nP = int.Parse (ars[25]);
          this._txtKP2.Text = nP.ToString ();
          nP = int.Parse (ars[26]);
          this._txtKP3.Text = nP.ToString ();
          nP = int.Parse (ars[27]);
          this._txtKP4.Text = nP.ToString ();
        }
        catch
        {
          nError |= (int)ReadDataError.KP;
        }

        // ---------------------------------
        // Section: Spectrum analysis

        ars = _arsServiceData[3].Split ('\t');
        try
        {
          // Scan offset min.
          this._txtScanOffset_Min.Text = ars[0];
          // Scan offset max.
          this._txtScanOffset_Max.Text = ars[1];
          // Peak search: 'n' sigma factor
          this._txtPeakSearchNsig.Text = ars[2];
          // Peak search: mode
          int nPS = int.Parse (ars[3]);
          string sPS = "";
          switch (nPS)
          {
            case 0: sPS = PeakSearch.Modi.Unidirectional; break;
            case 1: sPS = PeakSearch.Modi.Bidirectional; break;
          }
          this._cbPSMode.SelectedItem = sPS;
          // Peak area determination: 'n' sigma factor
          this._txtPeakAreaDetNsig.Text = ars[4];
          // Peak area determination: mode
          int nPA = int.Parse (ars[5]);
          string sPA = "";
          switch (nPA)
          {
            case 0: sPA = PeakAreaDetermination.Modi.Simple; break;
            case 1: sPA = PeakAreaDetermination.Modi.Extended_1; break;
            case 2: sPA = PeakAreaDetermination.Modi.Extended_2; break;
          }
          this._cbPAMode.SelectedItem = sPA;
          // Gain factor
          string sGainFactor = ars[6];
          this._cbGainFactor.SelectedItem = sGainFactor;
          // Smoothing
          int nType = int.Parse (ars[7]);
          switch (nType)
          {
            case 0: this._rbSmoothing_MA.Checked = true; break;
            default:
            case 1: this._rbSmoothing_SG.Checked = true; break;
          }
          this._txtSmoothing_MA_width.Text = ars[8];    // MA filter width
          this._txtSmoothing_SG_width.Text = ars[9];    // SG filter width
          // Static scan offset
          this._txtStatScanOffset.Text = ars[10];
        }
        catch
        {
          nError |= (int)ReadDataError.SpecAnal;
        }

        // ---------------------------------
        // Error handling

        // Output the errors after Reading the ServiceData
        _WriteErrors (nError);
      }
    }

    /// <summary>
    /// Updates the ServiceData based on the contents of a given service file.
    /// </summary>
    /// <param name="sSvcFile">The service file contents</param>
    void _UpdateServiceData (string sSvcFile)
    {
      string sVal, sVal1;
      ServiceDataFile sdf = new ServiceDataFile ();

      // Reset Error handling
      int nError = 0;

      // Name of the Service file
      try
      {
        sVal = sdf.GetValue (sSvcFile, "Filename");
        _sCurServiceFilename = sVal;
      }
      catch
      { }

      // ---------------------------------
      // Section: Device

      try
      {
        sVal = sdf.GetValue (sSvcFile, "DevNo");
        this._txtDeviceNo.Text = sVal;
        sVal = sdf.GetValue (sSvcFile, "CellNo");
        this._txtCellNo.Text = sVal;
        // PID Lamptime
        //  This content should only be updated in the editor mode.
        //  The local 'bUpdatePIDlamptime', which determines whether updating takes place or not,
        //  reflects this situation.
        bool bUpdatePIDlamptime = _bEditMode;
        sVal = sdf.GetValue (sSvcFile, "PIDLamptime");
        int idx = sVal.IndexOf ("h");
        if (-1 != idx)
        {
          sVal = sVal.Substring (0, idx);
          sVal = sVal.Trim ();
        }
        if (bUpdatePIDlamptime) this._txtPIDlamp.Text = sVal;
        // Accu display: on/off         
        sVal = sdf.GetValue (sSvcFile, "AccuDisplay");
        this._chkAccuDisplay.Checked = (sVal == "On") ? true : false;
        // Logo
        sVal = sdf.GetValue (sSvcFile, "Logo");
        switch (sVal)
        {
          default:
          case "ENIT": this._rbLogoENIT.Checked = true; break;
          case "ENVI": this._rbLogoENVI.Checked = true; break;
          case "IUT": this._rbLogoIUT.Checked = true; break;
        }
        // Pump On times:
        // Notes:
        //  These contents should only be updated in the editor mode.
        //  The local 'bUpdatePumpOnTimes', which determines whether updating takes place or not,
        //  reflects this situation.
        bool bUpdatePumpOnTimes = _bEditMode;
        sVal = sdf.GetValue (sSvcFile, "P1OnTime");
        idx = sVal.IndexOf ("h");
        if (-1 != idx)
        {
          sVal = sVal.Substring (0, idx);
          sVal = sVal.Trim ();
        }
        if (bUpdatePumpOnTimes) this._txtP1On.Text = sVal;
        sVal = sdf.GetValue (sSvcFile, "P2OnTime");
        idx = sVal.IndexOf ("h");
        if (-1 != idx)
        {
          sVal = sVal.Substring (0, idx);
          sVal = sVal.Trim ();
        }
        if (bUpdatePumpOnTimes) this._txtP2On.Text = sVal;
        sVal = sdf.GetValue (sSvcFile, "P3OnTime");
        idx = sVal.IndexOf ("h");
        if (-1 != idx)
        {
          sVal = sVal.Substring (0, idx);
          sVal = sVal.Trim ();
        }
        if (bUpdatePumpOnTimes) this._txtP3On.Text = sVal;
        // Self-check
        sVal = sdf.GetValue (sSvcFile, "SelfCheck");
        this._txtSelfCheck.Text = sVal;
        // Error confirmation: on/off         
        sVal = sdf.GetValue (sSvcFile, "ErrorConfirmation");
        this._chkErrConf.Checked = (sVal == "On") ? true : false;
        // PW check: on/off         
        sVal = sdf.GetValue (sSvcFile, "PWOnOff");
        this._chkPWOnOff.Checked = (sVal == "On") ? true : false;
        // PW
        sVal = sdf.GetValue (sSvcFile, "PW");
        this._txtPW.Text = sVal.PadLeft (4, '0');
      }
      catch
      {
        nError |= (int)ReadDataError.Device;
      }

      // ---------------------------------
      // Section: DateTime

      try
      {
        // DateTime
        sVal = sdf.GetValue (sSvcFile, "DateTime");
        string sDT = sVal;
        DateTime dt = new DateTime (
          int.Parse (sDT.Substring (6, 4)),
          int.Parse (sDT.Substring (3, 2)),
          int.Parse (sDT.Substring (0, 2)),
          int.Parse (sDT.Substring (11, 2)),
          int.Parse (sDT.Substring (14, 2)),
          int.Parse (sDT.Substring (17, 2))
          );
        this._dtpDate.Value = dt;
        this._dtpTime.Value = dt;
      }
      catch
      {
        nError |= (int)ReadDataError.DateTime;
      }

      try
      {
        // Service date
        sVal = sdf.GetValue (sSvcFile, "ServiceDate");
        string sDTSvc = sVal;
        DateTime dtSvc = new DateTime (
          int.Parse (sDTSvc.Substring (6, 4)),
          int.Parse (sDTSvc.Substring (3, 2)),
          int.Parse (sDTSvc.Substring (0, 2)),
          0,
          0,
          0
          );
        this._dtpSvcDate.Value = dtSvc;
      }
      catch
      {
        nError |= (int)ReadDataError.ServiceDate;
      }

      // ---------------------------------
      // Section: Sensor

      // Pressure display on/off
      try
      {
        sVal = sdf.GetValue (sSvcFile, "PresDisp");
        this._chkPresOff.Checked = (sVal == "Off") ? true : false;
        _chkPresOff_CheckedChanged (this._chkPresOff, new EventArgs ());
      }
      catch
      {
        nError |= (int)ReadDataError.PresOff;
      }

      // Soll values 
      // 1. Default values string 
      int nTemp;
      try
      {
        // 1.1. T1_soll
        sVal = sdf.GetValue (sSvcFile, "T1");
        nTemp = int.Parse (sVal);
        this._chkT1Off.Checked = (nTemp == 0) ? true : false;
        _chkTOff_CheckedChanged (this._chkT1Off, new EventArgs ());
        if (!this._chkT1Off.Checked)
          this._nudT1_Soll.Value = nTemp;
      }
      catch
      {
        nError |= (int)ReadDataError.T1_Debit;
      }
      try
      {
        // 1.2. T2_soll
        sVal = sdf.GetValue (sSvcFile, "T2");
        nTemp = int.Parse (sVal);
        this._chkT2Off.Checked = (nTemp == 0) ? true : false;
        _chkTOff_CheckedChanged (this._chkT2Off, new EventArgs ());
        if (!this._chkT2Off.Checked)
          this._nudT2_Soll.Value = nTemp;
      }
      catch
      {
        nError |= (int)ReadDataError.T2_Debit;
      }
      try
      {
        // 1.3. T3_soll
        sVal = sdf.GetValue (sSvcFile, "T3");
        nTemp = int.Parse (sVal);
        this._chkT3Off.Checked = (nTemp == 0) ? true : false;
        _chkTOff_CheckedChanged (this._chkT3Off, new EventArgs ());
        if (!this._chkT3Off.Checked)
          this._nudT3_Soll.Value = nTemp;
      }
      catch
      {
        nError |= (int)ReadDataError.T3_Debit;
      }
      try
      {
        // 1.4. T4_soll
        sVal = sdf.GetValue (sSvcFile, "T4");
        nTemp = int.Parse (sVal);
        this._chkT4Off.Checked = (nTemp == 0) ? true : false;
        _chkTOff_CheckedChanged (this._chkT4Off, new EventArgs ());
        if (!this._chkT4Off.Checked)
          this._nudT4_Soll.Value = nTemp;
      }
      catch
      {
        nError |= (int)ReadDataError.T4_Debit;
      }

      int nPump;
      try
      {
        // 1.5. P1-F1 value
        sVal = sdf.GetValue (sSvcFile, "P1");
        sVal1 = sdf.GetValue (sSvcFile, "P1/F1 handling");
        nPump = int.Parse (sVal);
        this._chkPF1.Checked = (sVal1 == "Checked") ? true : false;
        _chkPF_CheckedChanged (this._chkPF1, new EventArgs ());
        if (!this._chkPF1.Checked)
          this._nudP1.Value = nPump;
        else
          this._txtF1.Text = nPump.ToString ();
      }
      catch
      {
        nError |= (int)ReadDataError.P1_Debit;
      }
      try
      {
        // 1.6. P2-F2 value
        sVal = sdf.GetValue (sSvcFile, "P2");
        sVal1 = sdf.GetValue (sSvcFile, "P2/F2 handling");
        nPump = int.Parse (sVal);
        this._chkPF2.Checked = (sVal1 == "Checked") ? true : false;
        _chkPF_CheckedChanged (this._chkPF2, new EventArgs ());
        if (!this._chkPF2.Checked)
          this._nudP2.Value = nPump;
        else
          this._txtF2.Text = nPump.ToString ();
      }
      catch
      {
        nError |= (int)ReadDataError.P2_Debit;
      }
      try
      {
        // 1.7. P3-F3 value
        sVal = sdf.GetValue (sSvcFile, "P3");
        sVal1 = sdf.GetValue (sSvcFile, "P3/F3 handling");
        nPump = int.Parse (sVal);
        this._chkPF3.Checked = (sVal1 == "Checked") ? true : false;
        _chkPF_CheckedChanged (this._chkPF3, new EventArgs ());
        if (!this._chkPF3.Checked)
          this._nudP3.Value = nPump;
        else
          this._txtF3.Text = nPump.ToString ();
      }
      catch
      {
        nError |= (int)ReadDataError.P3_Debit;
      }

      // 2. Flow
      int nFlow;
      float fFlow;
      try
      {
        // 2.1. F1: Soll
        sVal = sdf.GetValue (sSvcFile, "F1-Soll");
        nFlow = int.Parse (sVal);
        this._txtF1_Soll.Text = nFlow.ToString ();
        //      F1: Soll - abs. tolerance
        sVal = sdf.GetValue (sSvcFile, "F1-TolAbs");
        nFlow = int.Parse (sVal);
        this._txtF1_Soll_TolAbs.Text = nFlow.ToString ();
      }
      catch
      {
        nError |= (int)ReadDataError.F1_Debit;
      }
      try
      {
        System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
        // 2.2. F2: Soll
        sVal = sdf.GetValue (sSvcFile, "F2-Soll");
        fFlow = float.Parse (sVal, nfi);
        this._txtF2_Soll.Text = fFlow.ToString ("F1", nfi);
        //      F2: Soll - abs. tolerance
        sVal = sdf.GetValue (sSvcFile, "F2-TolAbs");
        fFlow = float.Parse (sVal, nfi);
        this._txtF2_Soll_TolAbs.Text = fFlow.ToString ("F1", nfi);
      }
      catch
      {
        nError |= (int)ReadDataError.F2_Debit;
      }
      try
      {
        // 2.3. F3: Soll
        sVal = sdf.GetValue (sSvcFile, "F3-Soll");
        nFlow = int.Parse (sVal);
        this._txtF3_Soll.Text = nFlow.ToString ();
        //      F3: Soll - abs. tolerance
        sVal = sdf.GetValue (sSvcFile, "F3-TolAbs");
        nFlow = int.Parse (sVal);
        this._txtF3_Soll_TolAbs.Text = nFlow.ToString ();
      }
      catch
      {
        nError |= (int)ReadDataError.F3_Debit;
      }

      // Heater regulation values 
      int nP;
      try
      {
        sVal = sdf.GetValue (sSvcFile, "P-factor T1");
        nP = int.Parse (sVal);
        this._txtKP1.Text = nP.ToString ();
        sVal = sdf.GetValue (sSvcFile, "P-factor T2");
        nP = int.Parse (sVal);
        this._txtKP2.Text = nP.ToString ();
        sVal = sdf.GetValue (sSvcFile, "P-factor T3");
        nP = int.Parse (sVal);
        this._txtKP3.Text = nP.ToString ();
        sVal = sdf.GetValue (sSvcFile, "P-factor T4");
        nP = int.Parse (sVal);
        this._txtKP4.Text = nP.ToString ();
      }
      catch
      {
        nError |= (int)ReadDataError.KP;
      }

      // ---------------------------------
      // Section: Spectrum analysis

      try
      {
        // Scan offset min.
        sVal = sdf.GetValue (sSvcFile, "ScanOffsetMin");
        this._txtScanOffset_Min.Text = sVal;
        // Scan offset max.
        sVal = sdf.GetValue (sSvcFile, "ScanOffsetMax");
        this._txtScanOffset_Max.Text = sVal;
        // Peak search: 'n' sigma factor
        sVal = sdf.GetValue (sSvcFile, "NSigPS");
        this._txtPeakSearchNsig.Text = sVal;
        // Peak search: mode
        sVal = sdf.GetValue (sSvcFile, "ModePS");
        string sPS;
        switch (sVal)
        {
          default:
          case "Unidirectional": sPS = PeakSearch.Modi.Unidirectional; break;
          case "Bidirectional": sPS = PeakSearch.Modi.Bidirectional; break;
        }
        this._cbPSMode.SelectedItem = sPS;
        // Peak area determination: 'n' sigma factor
        sVal = sdf.GetValue (sSvcFile, "NSigPA");
        this._txtPeakAreaDetNsig.Text = sVal;
        // Peak area determination: mode
        sVal = sdf.GetValue (sSvcFile, "ModePA");
        string sPA;
        switch (sVal)
        {
          default:
          case "Simple": sPA = PeakAreaDetermination.Modi.Simple; break;
          case "Extended_1": sPA = PeakAreaDetermination.Modi.Extended_1; break;
          case "Extended_2": sPA = PeakAreaDetermination.Modi.Extended_2; break;
        }
        this._cbPAMode.SelectedItem = sPA;
        // Gain factor
        sVal = sdf.GetValue (sSvcFile, "GainFactor");
        this._cbGainFactor.SelectedItem = sVal;
        // Smoothing
        sVal = sdf.GetValue (sSvcFile, "SmoothingFilter");
        switch (sVal)
        {
          default:
          case "MA": this._rbSmoothing_MA.Checked = true; break;
          case "SG": this._rbSmoothing_SG.Checked = true; break;
        }
        sVal = sdf.GetValue (sSvcFile, "MAFilterwidth");
        this._txtSmoothing_MA_width.Text = sVal;      // MA filter width
        sVal = sdf.GetValue (sSvcFile, "SGFilterwidth");
        this._txtSmoothing_SG_width.Text = sVal;      // SG filter width
      }
      catch
      {
        nError |= (int)ReadDataError.SpecAnal;
      }

      // --------------
      // Error handling

      // Output the errors after Loading the ServiceData from file
      _WriteErrors (nError);
    }

    /// <summary>
    /// Outputs the errors after Reading the ServiceData or Loading them from file.
    /// </summary>
    /// <param name="nError">The error word</param>
    void _WriteErrors (int nError)
    {
      string sMsg = "";
      if (nError > 0)
      {
        // Compose error message
        App app = App.Instance;
        Ressources r = app.Ressources;
        sMsg = r.GetString ("Service_Error_ReadData");          // "Fehler beim Lesen der Service-Daten:"
        if ((nError & (int)ReadDataError.Device) != 0) sMsg += "\r\n   Device";
        if ((nError & (int)ReadDataError.DateTime) != 0) sMsg += "\r\n   DateTime";
        if ((nError & (int)ReadDataError.ServiceDate) != 0) sMsg += "\r\n   ServiceDate";
        if ((nError & (int)ReadDataError.Actual_Sensor_Values) != 0) sMsg += "\r\n   Actual Sensor values";
        if ((nError & (int)ReadDataError.T1_Debit) != 0) sMsg += "\r\n   T1 (Debit)";
        if ((nError & (int)ReadDataError.T2_Debit) != 0) sMsg += "\r\n   T2 (Debit)";
        if ((nError & (int)ReadDataError.T3_Debit) != 0) sMsg += "\r\n   T3 (Debit)";
        if ((nError & (int)ReadDataError.T4_Debit) != 0) sMsg += "\r\n   T4 (Debit)";
        if ((nError & (int)ReadDataError.PresOff) != 0) sMsg += "\r\n   Pressure off";
        if ((nError & (int)ReadDataError.P1_Debit) != 0) sMsg += "\r\n   P1 (Debit)";
        if ((nError & (int)ReadDataError.P2_Debit) != 0) sMsg += "\r\n   P2 (Debit)";
        if ((nError & (int)ReadDataError.P3_Debit) != 0) sMsg += "\r\n   P3 (Debit)";
        if ((nError & (int)ReadDataError.F1_Debit) != 0) sMsg += "\r\n   F1 (Debit)";
        if ((nError & (int)ReadDataError.F2_Debit) != 0) sMsg += "\r\n   F2 (Debit)";
        if ((nError & (int)ReadDataError.F3_Debit) != 0) sMsg += "\r\n   F3 (Debit)";
        if ((nError & (int)ReadDataError.MFC) != 0) sMsg += "\r\n   MFC";
        if ((nError & (int)ReadDataError.KP) != 0) sMsg += "\r\n   P factor";
        if ((nError & (int)ReadDataError.SpecAnal) != 0) sMsg += "\r\n   Spectrum analysis";
      }
      // Display error message
      this._txtError.Text = sMsg;
    }

    /// <summary>
    /// Updates the ServiceComment string array based on the control contents (DDX = true) or viceversa (DDX = false). 
    /// </summary>
    /// <param name="bDDX">The DDX direction</param>
    void _UpdateServiceCmt (Boolean bDDX)
    {
      if (bDDX)
      {
        // Controls -> Members:

        // Build the ServiceComment string array
        // Notes:
        //  1.
        //  The char's ',' and ' ' must be replaced by other (non-presentable) chars
        //  due to the cmd-par-string handling in the device SW (see device SW: 'parse.c').
        //  The char's CR and LF must be replaced by other (non-presentable) chars
        //  due to the ENDCHAR recognition in communication handling (see 'CommChannel.cs').
        //  2.
        //  The length of a parameter string should not exceed 'MAX_PAR_LEN' bytes.
        //  Thats why the whole service comment is represented by a string array, where each string 
        //  (except for the last one) has a length of '_MAXWRITECMTLENGTH' Bytes. These strings are 
        //  then transferred consecutively.

        // Substitute the following char's in the service comment string:
        // ',' , ' ' , '\n' , '\r'
        string s = this._txtComment.Text;
        s = s.Replace (',', _COMMAREPLCHAR);
        s = s.Replace (' ', _SPACEREPLCHAR);
        s = s.Replace ('\n', _LFREPLCHAR);
        s = s.Replace ('\r', _CRREPLCHAR);
        // Build the ServiceComment string array
        _anzCmtBlocks = s.Length / _MAXWRITECMTLENGTH;
        if (s.Length % _MAXWRITECMTLENGTH > 0) _anzCmtBlocks++;
        _arsServiceCmt = new string[_anzCmtBlocks];
        for (int i = 0; i < _anzCmtBlocks; i++)
        {
          if (i < _anzCmtBlocks - 1)
            _arsServiceCmt[i] = s.Substring (i * _MAXWRITECMTLENGTH, _MAXWRITECMTLENGTH);
          else
            _arsServiceCmt[i] = s.Substring (i * _MAXWRITECMTLENGTH);
        }
      }
      else
      {
        // Members -> Controls:

        // Update the control contents
        StringBuilder sb = new StringBuilder ();
        for (int i = 0; i < _anzCmtBlocks; i++)
        {
          // The current service comment block
          string s = _arsServiceCmt[i];
          // Re-substitute the following char's:
          // ',' , ' ' , '\n' , '\r'
          s = s.Replace (_COMMAREPLCHAR, ',');
          s = s.Replace (_SPACEREPLCHAR, ' ');
          s = s.Replace (_LFREPLCHAR, '\n');
          s = s.Replace (_CRREPLCHAR, '\r');
          // Build whole service comment
          sb.Append (s);
        }
        // Display the whole service comment
        this._txtComment.Text = sb.ToString ();
      }
    }

    /// <summary>
    /// Finishes the transfer
    /// </summary>
    void _EndTransfer ()
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Doc doc = app.Doc;

      // TX: Script Select
      //    Parameter: The name of the script to be executed
      //    Device: Action - Selects the script to be executed (here: script last used); 
      //            Returns - OK
      CommMessage msg = new CommMessage (comm.msgScriptSelect);
      msg.Parameter = doc.ScriptNameToIdx (_sLastScriptName).ToString ();
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);

      // TX: Transfer Complete
      //    Parameter: none
      //    Device: Action - Indicate, that the transfer has completed; 
      //            Returns - OK
      msg = new CommMessage (comm.msgTransferComplete);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);

      // Indicate that the Transfer process has finished.
      _bTransferInProgress = false;

      // Reset: Value (position) of the Progressbar control
      this._pgbTransfer.Value = 0;
    }

    /// <summary>
    /// Checks a string with respect to the presence of an error spec.
    /// If an error spec. is detected, it is replaced by its understandable expression.
    /// </summary>
    /// <param name="sText">The string to be checked</param>
    /// <returns>The possibly modified string</returns>
    void _CheckErrorSpec (TextBox t, string sText)
    {
      // Default text & text color
      string s = sText;
      Color cr = _colorSensorActualDefault;
      // Replacements in case of the presence of an error spec. 
      switch (sText.Trim ())
      {
        case "E1": s = "Sensor error"; cr = Color.Red; break;
        case "E2": s = "IC error"; cr = Color.Red; break;
        case "E3": s = "Fuse error"; cr = Color.Red; break;
        case "E4": s = "Comm. error"; cr = Color.Red; break;
      }
      // Assign text & text color
      t.Text = s;
      t.ForeColor = cr;
    }

    /// <summary>
    /// Performs actions in reaction of the receipt of a comm. message  
    /// </summary>
    /// <param name="msgRX">The comm. message</param>
    public void AfterRXCompleted (CommMessage msgRX)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // CD: Message response ID
      if ((msgRX.ResponseID == AppComm.CMID_TIMEOUT) || (msgRX.ResponseID == AppComm.CMID_STRLENERR))
      {
        // A comm. error (Timeout, ...) occurred:
      }

      else if (msgRX.ResponseID == AppComm.CMID_ANSWER)
      {
        // The message response is present:
        string sCmd = msgRX.CommandResponse;
        byte[] arbyPar = msgRX.ParameterResponse;

        // Get the parameter string from the parameter Byte array
        string sPar = Encoding.ASCII.GetString (arbyPar);
        // CD according to the message command
        switch (sCmd)
        {

          //----------------------------------------------------------
          // Common Transfer messages
          //----------------------------------------------------------

          case "SCRCURRENT":
            // ScriptCurrent
            // RX: Name of the script currently running on the device   
            try
            {
              // Get the name of the script, that was lastly running on the IMS device
              _sLastScriptName = sPar;
            }
            catch
            {
              Debug.WriteLine ("OnMsgScriptCurrent->function failed");
            }
            break;

          case "SCRSELECT":
            // ScriptSelect
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ("OnMsgScriptSelect->function failed");
            }
            break;

          case "TRNSTART":
            // Transfer Start
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ("OnMsgTransferStart->function failed");
            }
            break;

          case "TRNCOMPLETE":
            // Transfer Complete
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ("OnMsgTransferComplete->function failed");
            }
            break;

          //----------------------------------------------------------
          // Service-Read
          //----------------------------------------------------------

          case "SVCREADCMT_ST":
            // Service Read Comment Start 
            // RX:  The number of blocks (of a max. size, given by the device SW) 
            //      that are required in order to transmit the complete service comment 
            //      from the device
            try
            {
              // The number of blocks
              _anzCmtBlocks = int.Parse (sPar);
              // Allocate the Service Comment string array corr'ly
              _arsServiceCmt = new string[_anzCmtBlocks];
              // Initiate: The block counter, The Service Data type
              _currCmtBlock = 0;
              _currDataType = 0;

              // Init. progress bar
              this._pgbTransfer.Minimum = 0;
              this._pgbTransfer.Maximum = _anzCmtBlocks + _anzDataStrings;
              this._pgbTransfer.Value = 0;

              if (_anzCmtBlocks > 0)
              {
                // TX: Service Read Comment ( Initial )
                //    Parameter: The idx of the service comment block to be read
                //    Device: Action - nothing; 
                //            Returns - The service comment block with the given idx from the device
                CommMessage msg = new CommMessage (comm.msgServiceReadCmt);
                msg.Parameter = _currCmtBlock.ToString ();
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage (msg);
              }
              else
              {
                // TX: Service Read Data ( Initial )
                //    Parameter: The Service Data type for the service data to be read
                //    Device: Action - nothing; 
                //            Returns - The service data string for the given service data type from the device
                CommMessage msg = new CommMessage (comm.msgServiceReadData);
                msg.Parameter = _currDataType.ToString ();
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage (msg);
              }
            }
            catch
            {
              Debug.WriteLine ("OnServiceReadCmtStart->function failed");
            }
            break;

          case "SVCREADCMT":
            // Service Read Comment
            // RX: The service comment block with the given idx from the device
            try
            {
              CommMessage msg;

              // Update the Service Comment string array with the current service comment block
              _arsServiceCmt[_currCmtBlock] = sPar;
              // Update progress bar
              this._pgbTransfer.Value++;

              // Check, whether all service comment block have been read
              if (_currCmtBlock == _anzCmtBlocks - 1)
              {
                // Yes:
                // Update the ServiceComment controls
                _UpdateServiceCmt (false);

                // TX: Service Read Data ( Initial )
                //    Parameter: The Service Data type for the service data to be read
                //    Device: Action - nothing; 
                //            Returns - The service data string for the given service data type from the device
                msg = new CommMessage (comm.msgServiceReadData);
                msg.Parameter = _currDataType.ToString ();
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage (msg);
                break;
              }

              // No:
              // Increment: The block counter
              _currCmtBlock++;

              // TX: Service Read Comment ( Subsequent )
              //    Parameter: The idx of the service comment block to be read
              //    Device: Action - nothing; 
              //            Returns - The service comment block with the given idx from the device
              msg = new CommMessage (comm.msgServiceReadCmt);
              msg.Parameter = _currCmtBlock.ToString ();
              msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
              comm.WriteMessage (msg);
            }
            catch
            {
              Debug.WriteLine ("OnServiceReadCmt->function failed");
            }
            break;

          case "SVCREADDATA":
            // Service Read Data
            // RX: The service data from the device
            try
            {
              CommMessage msg;

              // Update the Service Data string array with the current service data string
              _arsServiceData[_currDataType] = sPar;
              // Update progress bar
              this._pgbTransfer.Value++;

              // Check, whether all service data strings have been read
              if (_currDataType == _anzDataStrings - 1)
              {
                // Yes:
                // Update the ServiceData controls
                _UpdateServiceData (false);

                // Adjust Enabled state: TB's F2 (MFC controlled) Soll, F2 (MFC controlled) Soll - abs. tolerance
                // Notes:
                //  These TB's should be enabled, if a MFC is present, and disabled, if not.
                System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
                string s = this._txtMFCcap.Text;
                try
                {
                  float nMFCcap = float.Parse (s, nfi);
                  this._txtF2_Soll.Enabled = (nMFCcap > 0) ? true : false;
                  this._txtF2_Soll_TolAbs.Enabled = (nMFCcap > 0) ? true : false;
                }
                catch { }

                // At this point the service data have been read & updated. Now we we have to fill the
                // 'Service files' ListBox with the service files residing on the device.

                // Read Service root folder path: "RDSVCFO\r"
                msg = new CommMessage (comm.msgReadServiceFolder);
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage (msg);

                break;
              }

              // No:
              // Update the Service Data type
              _currDataType++;

              // TX: Service Read Data ( Subsequent )
              //    Parameter: The Service Data type for the service data to be read
              //    Device: Action - nothing; 
              //            Returns - The service data string for the given service data type from the device
              msg = new CommMessage (comm.msgServiceReadData);
              msg.Parameter = _currDataType.ToString ();
              msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
              comm.WriteMessage (msg);
            }
            catch
            {
              Debug.WriteLine ("OnServiceReadData->function failed");
            }
            break;

          //----------------------------------------------------------
          // Service-Write
          //----------------------------------------------------------

          case "SVCWRITECMT_ST":
            // Service Write Comment Start 
            // RX: OK
            try
            {
              // Initiate: The block counter, The Service Data type
              _currCmtBlock = 0;
              _currDataType = 0;

              // Init. progress bar
              this._pgbTransfer.Minimum = 0;
              this._pgbTransfer.Maximum = _anzCmtBlocks + _anzDataStrings;
              this._pgbTransfer.Value = 0;

              // Check: Service Comment available?
              if (_anzCmtBlocks > 0)
              {
                // Yes:
                // TX: Service Write Comment ( Initial )
                //    Parameter: The current Service Comment substring
                //    Device: Action - Actualizes the service comment on the device; 
                //            Returns - OK
                CommMessage msg = new CommMessage (comm.msgServiceWriteCmt);
                msg.Parameter = _arsServiceCmt[_currCmtBlock];
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage (msg);
              }
              else
              {
                // No:
                // TX: Service Write Data ( Initial )
                //    Parameter: The Service Data type and the corr'ing Service Data string
                //    Device: Action - Actualizes the service data on the device; 
                //            Returns - OK
                CommMessage msg = new CommMessage (comm.msgServiceWriteData);
                msg.Parameter = string.Format ("{0},{1}", _currDataType, _arsServiceData[_currDataType]);
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage (msg);
                break;
              }
            }
            catch
            {
              Debug.WriteLine ("OnServiceWriteCmtStart->function failed");
            }
            break;

          case "SVCWRITECMT":
            // Service Write Comment
            // RX: OK
            try
            {
              CommMessage msg;

              // Update progress bar
              this._pgbTransfer.Value++;

              // Check, whether all service comment blocks ( substrings ) have been written
              if (_currCmtBlock == _anzCmtBlocks - 1)
              {
                // Yes:
                // TX: Service Write Data ( Initial )
                //    Parameter: The Service Data type and the corr'ing Service Data string
                //    Device: Action - Actualizes the service data on the device; 
                //            Returns - OK
                msg = new CommMessage (comm.msgServiceWriteData);
                msg.Parameter = string.Format ("{0},{1}", _currDataType, _arsServiceData[_currDataType]);
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage (msg);
                break;
              }

              // No:
              // Update block counter
              _currCmtBlock++;

              // TX: Service Write Comment ( Subsequent )
              //    Parameter: The current Service Comment substring
              //    Device: Action - Actualizes the service comment on the device; 
              //            Returns - OK
              msg = new CommMessage (comm.msgServiceWriteCmt);
              msg.Parameter = _arsServiceCmt[_currCmtBlock];
              msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
              comm.WriteMessage (msg);
            }
            catch
            {
              Debug.WriteLine ("OnServiceWriteCmt->function failed");
            }
            break;

          case "SVCWRITEDATA":
            // Service Write Data
            // RX: OK
            try
            {
              CommMessage msg;

              // Update progress bar
              this._pgbTransfer.Value++;

              // Check, whether all Service Data strings have been written
              if (_currDataType == _anzDataStrings - 1)
              {
                // Yes:

                // At this point the 'Service files' ListBox has to be updated, because 1 new service file
                // has been created on the device.

                // Read Service files: "RDSVCFI\r"
                msg = new CommMessage (comm.msgReadServiceFiles);
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage (msg);

                break;
              }

              // No:
              // Update the Service Data type
              _currDataType++;

              // TX: Service Write Data ( Subsequent )
              //    Parameter: The Service Data type and the corr'ing Service Data string
              //    Device: Action - Actualizes the service data on the device; 
              //            Returns - OK
              msg = new CommMessage (comm.msgServiceWriteData);
              msg.Parameter = string.Format ("{0},{1}", _currDataType, _arsServiceData[_currDataType]);
              msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
              comm.WriteMessage (msg);
            }
            catch
            {
              Debug.WriteLine ("OnServiceWriteData->function failed");
            }
            break;

          //----------------------------------------------------------
          // SDcard storage messages
          //----------------------------------------------------------

          case "RDSVCFO":
            // Get the path of the device Service root folder
            // RX: The path of the device Service root folder
            try
            {
              // Overgive the path of the device Service root folder
              _sServiceFolderPath = sPar;

              // Read Service files: "RDSVCFI\r"
              CommMessage msg = new CommMessage (comm.msgReadServiceFiles);
              msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
              comm.WriteMessage (msg);
            }
            catch
            {
              Debug.WriteLine ("Service: OnMsgReadServiceFolder->function failed");
            }
            break;

          case "RDSVCFI":
            // Get the files of the device Service folder
            // RX: The files of the device Service folder
            try
            {
              // Add the names of the files to the 'Service files' ListBox
              string[] ars = sPar.Split (',');
              this._lbSvcFiles.Items.Clear ();
              foreach (string s in ars)
              {
                if (s.Length > 0) this._lbSvcFiles.Items.Add (s);
              }

              // Mark last entry: 
              // This is the name of the Service file that corresponds to the 
              // contents of the ServiceData controls
              int idx = _lbSvcFiles.Items.Count - 1;
              _lbSvcFiles.SelectedIndex = idx;

              // Show the name of this Service file
              if (-1 != idx)
              {
                _sCurServiceFilename = _lbSvcFiles.SelectedItem.ToString ();
                this._txtCurSvcFile.Text = _sCurServiceFilename;
              }

              // Finish the transfer
              _EndTransfer ();
            }
            catch
            {
              Debug.WriteLine ("Service: OnMsgReadServiceFiles->function failed");
            }
            break;

          case "RDTFI":
            // Get the file contents of a device text (Data/Script/Scan/Service) file
            // RX: The file contents string
            try
            {
              // Update progress bar
              this._pgbTransfer.Value++;

              // Update the ServiceData based on the contents of a given service file       
              _UpdateServiceData (sPar);

              // Show the name of this Service file
              this._txtCurSvcFile.Text = _sCurServiceFilename;

              // Clear ServiceComment controls 
              this._txtComment.Text = "";
              this._txtAvCmtSize.Text = MAXCMTLENGTH.ToString ();

              // Reset progress bar (wait a little in order to see the progress bar working) 
              System.Threading.Thread.Sleep (1000);
              this._pgbTransfer.Value = 0;
            }
            catch (System.Exception exc)
            {
              Debug.WriteLine ("Service: OnMsgReadTextFileContents->function failed: {0}", exc.Message);
            }
            break;

        } // E - switch ( id )

      }//E - if (e.Message.Id = AppComm.CMID_ANSWER

    }

    #endregion methods

    #region properties

    /// <summary>
    ///  True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    public bool ShowWait
    {
      get { return _bShowWait; }
      set { _bShowWait = value; }
    }

    #endregion properties

  }
}
