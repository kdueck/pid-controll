using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace App
{
  /// <summary>
  /// Class SriptEditorAutoSpanMonForm:
  /// Sript editor: Processing of an 'Automated span monitoring' entry
  /// </summary>
  public class ScriptEditorAutoSpanMonForm : System.Windows.Forms.Form
  {
    private System.Windows.Forms.Button _btnOK;
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.ToolTip _ToolTip;
    private System.Windows.Forms.Label _lblLowLimit;
    private System.Windows.Forms.TextBox _txtLowLimit;
    private System.Windows.Forms.Label _lblNoErrMeas;
    private System.Windows.Forms.TextBox _txtNoErrMeas;
    private System.Windows.Forms.Label _lblUppLimit;
    private System.Windows.Forms.TextBox _txtUppLimit;
    private System.Windows.Forms.Label _lblIdx;
    private System.Windows.Forms.TextBox _txtIdx;
    private System.ComponentModel.IContainer components;

    /// <summary>
    /// Constructor
    /// </summary>
    public ScriptEditorAutoSpanMonForm()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this._btnOK = new System.Windows.Forms.Button();
      this._btnCancel = new System.Windows.Forms.Button();
      this._lblLowLimit = new System.Windows.Forms.Label();
      this._txtLowLimit = new System.Windows.Forms.TextBox();
      this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
      this._lblNoErrMeas = new System.Windows.Forms.Label();
      this._txtNoErrMeas = new System.Windows.Forms.TextBox();
      this._lblUppLimit = new System.Windows.Forms.Label();
      this._txtUppLimit = new System.Windows.Forms.TextBox();
      this._lblIdx = new System.Windows.Forms.Label();
      this._txtIdx = new System.Windows.Forms.TextBox();
      this.SuspendLayout();
      // 
      // _btnOK
      // 
      this._btnOK.Location = new System.Drawing.Point(68, 140);
      this._btnOK.Name = "_btnOK";
      this._btnOK.TabIndex = 8;
      this._btnOK.Text = "OK";
      this._btnOK.Click += new System.EventHandler(this._btnOK_Click);
      // 
      // _btnCancel
      // 
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point(168, 140);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.TabIndex = 9;
      this._btnCancel.Text = "Cancel";
      // 
      // _lblLowLimit
      // 
      this._lblLowLimit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblLowLimit.Location = new System.Drawing.Point(8, 36);
      this._lblLowLimit.Name = "_lblLowLimit";
      this._lblLowLimit.Size = new System.Drawing.Size(188, 23);
      this._lblLowLimit.TabIndex = 2;
      this._lblLowLimit.Text = "Tolerance range: Lower limit:";
      // 
      // _txtLowLimit
      // 
      this._txtLowLimit.Location = new System.Drawing.Point(200, 36);
      this._txtLowLimit.Name = "_txtLowLimit";
      this._txtLowLimit.TabIndex = 3;
      this._txtLowLimit.Text = "";
      this._txtLowLimit.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _ToolTip
      // 
      this._ToolTip.AutoPopDelay = 10000;
      this._ToolTip.InitialDelay = 500;
      this._ToolTip.ReshowDelay = 100;
      // 
      // _lblNoErrMeas
      // 
      this._lblNoErrMeas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblNoErrMeas.Location = new System.Drawing.Point(8, 92);
      this._lblNoErrMeas.Name = "_lblNoErrMeas";
      this._lblNoErrMeas.Size = new System.Drawing.Size(188, 36);
      this._lblNoErrMeas.TabIndex = 6;
      this._lblNoErrMeas.Text = "No. of erroneous measurements:";
      // 
      // _txtNoErrMeas
      // 
      this._txtNoErrMeas.Location = new System.Drawing.Point(200, 92);
      this._txtNoErrMeas.Name = "_txtNoErrMeas";
      this._txtNoErrMeas.TabIndex = 7;
      this._txtNoErrMeas.Text = "";
      this._txtNoErrMeas.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblUppLimit
      // 
      this._lblUppLimit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblUppLimit.Location = new System.Drawing.Point(8, 64);
      this._lblUppLimit.Name = "_lblUppLimit";
      this._lblUppLimit.Size = new System.Drawing.Size(188, 23);
      this._lblUppLimit.TabIndex = 4;
      this._lblUppLimit.Text = "Tolerance range:  Upper limit:";
      // 
      // _txtUppLimit
      // 
      this._txtUppLimit.Location = new System.Drawing.Point(200, 64);
      this._txtUppLimit.Name = "_txtUppLimit";
      this._txtUppLimit.TabIndex = 5;
      this._txtUppLimit.Text = "";
      this._txtUppLimit.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblIdx
      // 
      this._lblIdx.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblIdx.Location = new System.Drawing.Point(8, 8);
      this._lblIdx.Name = "_lblIdx";
      this._lblIdx.Size = new System.Drawing.Size(188, 23);
      this._lblIdx.TabIndex = 0;
      this._lblIdx.Text = "Index:";
      // 
      // _txtIdx
      // 
      this._txtIdx.Location = new System.Drawing.Point(200, 8);
      this._txtIdx.Name = "_txtIdx";
      this._txtIdx.TabIndex = 1;
      this._txtIdx.Text = "";
      this._txtIdx.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // ScriptEditorAutoSpanMonForm
      // 
      this.AcceptButton = this._btnOK;
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.CancelButton = this._btnCancel;
      this.ClientSize = new System.Drawing.Size(310, 176);
      this.Controls.Add(this._lblIdx);
      this.Controls.Add(this._txtIdx);
      this.Controls.Add(this._lblNoErrMeas);
      this.Controls.Add(this._txtNoErrMeas);
      this.Controls.Add(this._lblUppLimit);
      this.Controls.Add(this._txtUppLimit);
      this.Controls.Add(this._lblLowLimit);
      this.Controls.Add(this._txtLowLimit);
      this.Controls.Add(this._btnCancel);
      this.Controls.Add(this._btnOK);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.HelpButton = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ScriptEditorAutoSpanMonForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Automated span monitoring";
      this.Load += new System.EventHandler(this.SriptEditorAutoSpanMonForm_Load);
      this.ResumeLayout(false);

    }

    #endregion

    #region members

    /// <summary>
    /// The 'Automated span monitoring' index (in [0, 'MAX_SCRIPT_WINDOWS'-1])
    /// </summary>
    int _Idx = 0;
    
    /// <summary>
    /// Tolerance range: lower limit
    /// </summary>
    float _LowLimit = 0;

    /// <summary>
    /// Tolerance range: upper limit
    /// </summary>
    float _UppLimit = 0;

    /// <summary>
    /// # of erroneous measurements
    /// </summary>
    int _NoErrMeas = 0;
    
    #endregion members

    #region events

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void SriptEditorAutoSpanMonForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      try
      {
        // Resources
        this.Text               = r.GetString ( "ScriptEditorAutoSpanMon_Title" );          // "Automated span monitoring"
        this._btnCancel.Text    = r.GetString ( "ScriptEditorWindow_btnCancel" );           // "Abbruch"
        this._btnOK.Text        = r.GetString ( "ScriptEditorWindow_btnOK" );               // "OK"
        this._lblIdx.Text       = r.GetString ( "ScriptEditorAutoSpanMon_lblIdx" );         // "Index:"
        this._lblLowLimit.Text  = r.GetString ( "ScriptEditorAutoSpanMon_lblLowLimit" );    // "Tolerance range: Lower limit:"
        this._lblUppLimit.Text  = r.GetString ( "ScriptEditorAutoSpanMon_lblUppLimit" );    // "Tolerance range: Upper limit:"
        this._lblNoErrMeas.Text = r.GetString ( "ScriptEditorAutoSpanMon_lblNoErrMeas" );   // "No. of erroneous measurements:"
      }
      catch
      {
      }

      // Control initialisation
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
      _txtIdx.Text = _Idx.ToString ();
      _txtLowLimit.Text = _LowLimit.ToString (nfi);
      _txtUppLimit.Text = _UppLimit.ToString (nfi);
      _txtNoErrMeas.Text = _NoErrMeas.ToString ();
    }

    /// <summary>
    /// 'Click' event of the 'OK' button
    /// </summary>
    private void _btnOK_Click(object sender, System.EventArgs e)
    {
      object o;

      App app = App.Instance;
      Ressources r = app.Ressources;

      // Index: in [0, 'MAX_SCRIPT_WINDOWS'-1]
      if ( !Check.Execute ( this._txtIdx, CheckType.Int, CheckRelation.IN, 0, Script.MAX_SCRIPT_WINDOWS-1, true, out o) )
        return;
      _Idx=(int) o;
      // Tolerance range: Lower limit: > 0
      if ( !Check.Execute ( this._txtLowLimit, CheckType.Float, CheckRelation.GT, 0, 0, true, out o) )
        return;
      _LowLimit=(float) o;
      // Tolerance range: Upper limit: > Lower limit
      if ( !Check.Execute ( this._txtUppLimit, CheckType.Float, CheckRelation.GT, 0, 0, true, out o) )
        return;
      _UppLimit=(float) o;
      if (_LowLimit >= _UppLimit)  
      {
        // Show message
        string sMsg = r.GetString ("ScriptEditorAutoSpanMon_btnOK_Error");  // "Inconsistent concentration values: Required relation: Lower limit < Upper limit."
        string sCap = r.GetString ("Form_Common_TextError");                // "Fehler"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        // Focus 
        this._txtUppLimit.Focus ();
        return;
      }
      // # of erroneous measurements: > 0
      if ( !Check.Execute ( this._txtNoErrMeas, CheckType.Int, CheckRelation.GT, 0, 0, true, out o) )
        return;
      _NoErrMeas=(int) o;

      // OK
      this.DialogResult = DialogResult.OK;
      this.Close ();
    }
 
    /// <summary>
    /// 'HelpRequested' event of the controls
    /// </summary>
    private void _ctl_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Help requesting control
      Control requestingControl = (Control)sender;
      // Assign help text
      string s = "";
      if      (requestingControl == _txtIdx)
        s = r.GetString ("ScriptEditorAutoSpanMon_Hlp_txtIdx");         // "The index of the corresponding substance window"
      else if (requestingControl == this._txtLowLimit)
        s = r.GetString ("ScriptEditorAutoSpanMon_Hlp_txtLowLimit");    // "Tolerance range of the target concentration: Lower limit (unit as in Windows section)"
      else if (requestingControl == this._txtUppLimit)
        s = r.GetString ("ScriptEditorAutoSpanMon_Hlp_txtUppLimit");    // "Tolerance range of the target concentration: Upper limit (unit as in Windows section)"
      else if (requestingControl == this._txtNoErrMeas)
        s = r.GetString ("ScriptEditorAutoSpanMon_Hlp_txtNoErrMeas");   // "The number of erroneous measurements, until an error is reported"
      
      // Show help
      this._ToolTip.SetToolTip (requestingControl, s);
      hlpevent.Handled=true;
    }

    #endregion events

    #region methods
    #endregion methods

    #region properties

    /// <summary>
    /// The 'Automated span monitoring' index (in [0, 'MAX_SCRIPT_WINDOWS'-1])
    /// </summary>
    public int Index
    {
      get { return _Idx; }
      set { _Idx = value; }
    }
    
    /// <summary>
    /// Tolerance range: lower limit
    /// </summary>
    public float LowLimit
    {
      get { return _LowLimit; }
      set { _LowLimit = value; }
    }
    
    /// <summary>
    /// Tolerance range: upper limit
    /// </summary>
    public float UppLimit
    {
      get { return _UppLimit; }
      set { _UppLimit = value; }
    }

    /// <summary>
    /// # of erroneous measurements
    /// </summary>
    public int NoErrMeas
    {
      get { return _NoErrMeas; }
      set { _NoErrMeas = value; }
    }
    
    #endregion properties

  }

}
