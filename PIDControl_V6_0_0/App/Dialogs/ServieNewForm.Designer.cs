﻿namespace App
{
  partial class ServieNewForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose (bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose ();
      }
      base.Dispose (disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent ()
    {
      this.components = new System.ComponentModel.Container();
      this.tabControl1 = new System.Windows.Forms.TabControl();
      this.tpDevice = new System.Windows.Forms.TabPage();
      this._txtPW = new System.Windows.Forms.TextBox();
      this._lblPW = new System.Windows.Forms.Label();
      this._chkPWOnOff = new System.Windows.Forms.CheckBox();
      this._chkErrConf = new System.Windows.Forms.CheckBox();
      this._txtSelfCheck = new System.Windows.Forms.TextBox();
      this._lblSelfCheck = new System.Windows.Forms.Label();
      this._chkDontChangePIDlamp = new System.Windows.Forms.CheckBox();
      this._gbPumpOnTimes = new System.Windows.Forms.GroupBox();
      this._chkDontChangeP3On = new System.Windows.Forms.CheckBox();
      this._chkDontChangeP2On = new System.Windows.Forms.CheckBox();
      this._chkDontChangeP1On = new System.Windows.Forms.CheckBox();
      this._chkResetP3On = new System.Windows.Forms.CheckBox();
      this._chkResetP2On = new System.Windows.Forms.CheckBox();
      this._chkResetP1On = new System.Windows.Forms.CheckBox();
      this._txtP3On = new System.Windows.Forms.TextBox();
      this._lblP3On = new System.Windows.Forms.Label();
      this._txtP2On = new System.Windows.Forms.TextBox();
      this._lblP2On = new System.Windows.Forms.Label();
      this._txtP1On = new System.Windows.Forms.TextBox();
      this._lblP1On = new System.Windows.Forms.Label();
      this._gbLogo = new System.Windows.Forms.GroupBox();
      this._rbLogoENVI = new System.Windows.Forms.RadioButton();
      this._rbLogoENIT = new System.Windows.Forms.RadioButton();
      this._rbLogoIUT = new System.Windows.Forms.RadioButton();
      this._chkAccuDisplay = new System.Windows.Forms.CheckBox();
      this._chkResetPIDlamp = new System.Windows.Forms.CheckBox();
      this._txtPIDlamp = new System.Windows.Forms.TextBox();
      this._lblPIDlamp = new System.Windows.Forms.Label();
      this._txtCellNo = new System.Windows.Forms.TextBox();
      this._txtDeviceNo = new System.Windows.Forms.TextBox();
      this._lblCellNo = new System.Windows.Forms.Label();
      this._lblDeviceNo = new System.Windows.Forms.Label();
      this.tpDateTime = new System.Windows.Forms.TabPage();
      this._dtpSvcDate = new System.Windows.Forms.DateTimePicker();
      this._lblSvcDate = new System.Windows.Forms.Label();
      this._dtpTime = new System.Windows.Forms.DateTimePicker();
      this._dtpDate = new System.Windows.Forms.DateTimePicker();
      this._lblTime = new System.Windows.Forms.Label();
      this._lblDate = new System.Windows.Forms.Label();
      this.tpSensor = new System.Windows.Forms.TabPage();
      this._txtMFCcap = new System.Windows.Forms.TextBox();
      this._lblMFCcap = new System.Windows.Forms.Label();
      this._lblpm_3 = new System.Windows.Forms.Label();
      this._txtF3_Soll_TolAbs = new System.Windows.Forms.TextBox();
      this._txtF3_Soll = new System.Windows.Forms.TextBox();
      this._lblF3 = new System.Windows.Forms.Label();
      this._txtF3_Ist = new System.Windows.Forms.TextBox();
      this._lblTolAbs = new System.Windows.Forms.Label();
      this._lblSoll_1 = new System.Windows.Forms.Label();
      this._lblIst_1 = new System.Windows.Forms.Label();
      this._gbSensor_PF = new System.Windows.Forms.GroupBox();
      this._lblPump = new System.Windows.Forms.Label();
      this._lblFlow = new System.Windows.Forms.Label();
      this._lblPFHandling = new System.Windows.Forms.Label();
      this._chkPF3 = new System.Windows.Forms.CheckBox();
      this._chkPF2 = new System.Windows.Forms.CheckBox();
      this._chkPF1 = new System.Windows.Forms.CheckBox();
      this._nudP3 = new System.Windows.Forms.NumericUpDown();
      this._nudP2 = new System.Windows.Forms.NumericUpDown();
      this._nudP1 = new System.Windows.Forms.NumericUpDown();
      this._lblP3 = new System.Windows.Forms.Label();
      this._lblP2 = new System.Windows.Forms.Label();
      this._lblP1 = new System.Windows.Forms.Label();
      this._txtF3 = new System.Windows.Forms.TextBox();
      this._txtF2 = new System.Windows.Forms.TextBox();
      this._txtF1 = new System.Windows.Forms.TextBox();
      this._txtKP4 = new System.Windows.Forms.TextBox();
      this._txtKP3 = new System.Windows.Forms.TextBox();
      this._txtKP2 = new System.Windows.Forms.TextBox();
      this._txtKP1 = new System.Windows.Forms.TextBox();
      this._lblKP = new System.Windows.Forms.Label();
      this._lblpm_1 = new System.Windows.Forms.Label();
      this._lblpm = new System.Windows.Forms.Label();
      this._txtF2_Soll_TolAbs = new System.Windows.Forms.TextBox();
      this._txtF1_Soll_TolAbs = new System.Windows.Forms.TextBox();
      this._txtF2_Soll = new System.Windows.Forms.TextBox();
      this._txtF1_Soll = new System.Windows.Forms.TextBox();
      this._txtF2_Ist = new System.Windows.Forms.TextBox();
      this._txtF1_Ist = new System.Windows.Forms.TextBox();
      this._chkPresOff = new System.Windows.Forms.CheckBox();
      this._lblF2 = new System.Windows.Forms.Label();
      this._lblF1 = new System.Windows.Forms.Label();
      this._chkT4Off = new System.Windows.Forms.CheckBox();
      this._chkT3Off = new System.Windows.Forms.CheckBox();
      this._chkT2Off = new System.Windows.Forms.CheckBox();
      this._chkT1Off = new System.Windows.Forms.CheckBox();
      this._nudT4_Soll = new System.Windows.Forms.NumericUpDown();
      this._nudT3_Soll = new System.Windows.Forms.NumericUpDown();
      this._nudT2_Soll = new System.Windows.Forms.NumericUpDown();
      this._nudT1_Soll = new System.Windows.Forms.NumericUpDown();
      this._txtPres = new System.Windows.Forms.TextBox();
      this._txtT4_Ist = new System.Windows.Forms.TextBox();
      this._txtT2_Ist = new System.Windows.Forms.TextBox();
      this._txtT3_Ist = new System.Windows.Forms.TextBox();
      this._txtT1_Ist = new System.Windows.Forms.TextBox();
      this._lblSoll = new System.Windows.Forms.Label();
      this._lblIst = new System.Windows.Forms.Label();
      this._lblPres = new System.Windows.Forms.Label();
      this._lblT4 = new System.Windows.Forms.Label();
      this._lblT3 = new System.Windows.Forms.Label();
      this._lblT2 = new System.Windows.Forms.Label();
      this._lblT1 = new System.Windows.Forms.Label();
      this.tpAnalysis = new System.Windows.Forms.TabPage();
      this._lblScanOffset_Max = new System.Windows.Forms.Label();
      this._lblScanOffset_Min = new System.Windows.Forms.Label();
      this._gbPA = new System.Windows.Forms.GroupBox();
      this._cbPAMode = new System.Windows.Forms.ComboBox();
      this._lblPAMode = new System.Windows.Forms.Label();
      this._txtPeakAreaDetNsig = new System.Windows.Forms.TextBox();
      this._lblPeakAreaDetNsig = new System.Windows.Forms.Label();
      this._gbPS = new System.Windows.Forms.GroupBox();
      this._cbPSMode = new System.Windows.Forms.ComboBox();
      this._lblPSMode = new System.Windows.Forms.Label();
      this._txtPeakSearchNsig = new System.Windows.Forms.TextBox();
      this._lblPeakSearchNsig = new System.Windows.Forms.Label();
      this._gbSmoothing = new System.Windows.Forms.GroupBox();
      this._txtSmoothing_SG_width = new System.Windows.Forms.TextBox();
      this._lblSmoothing_SG_width = new System.Windows.Forms.Label();
      this._txtSmoothing_MA_width = new System.Windows.Forms.TextBox();
      this._lblSmoothing_MA_width = new System.Windows.Forms.Label();
      this._rbSmoothing_SG = new System.Windows.Forms.RadioButton();
      this._rbSmoothing_MA = new System.Windows.Forms.RadioButton();
      this._lblSmoothing_Type = new System.Windows.Forms.Label();
      this._txtStatScanOffset = new System.Windows.Forms.TextBox();
      this._lblStatScanOffset = new System.Windows.Forms.Label();
      this._txtScanOffset_Max = new System.Windows.Forms.TextBox();
      this._txtScanOffset_Min = new System.Windows.Forms.TextBox();
      this._lblScanOffset = new System.Windows.Forms.Label();
      this._lblGainFactor = new System.Windows.Forms.Label();
      this._gbLoad = new System.Windows.Forms.GroupBox();
      this._txtCurSvcFile = new System.Windows.Forms.TextBox();
      this._lblCurSvcFile = new System.Windows.Forms.Label();
      this._btnLoad = new System.Windows.Forms.Button();
      this._lbSvcFiles = new System.Windows.Forms.ListBox();
      this._gbError = new System.Windows.Forms.GroupBox();
      this._txtError = new System.Windows.Forms.TextBox();
      this._gbComment = new System.Windows.Forms.GroupBox();
      this._txtAvCmtSize = new System.Windows.Forms.TextBox();
      this._lblAvCmtSize = new System.Windows.Forms.Label();
      this._txtComment = new System.Windows.Forms.TextBox();
      this._btnExtended = new System.Windows.Forms.Button();
      this._chkPeriodicUpdate = new System.Windows.Forms.CheckBox();
      this._pgbTransfer = new System.Windows.Forms.ProgressBar();
      this._btnCancel = new System.Windows.Forms.Button();
      this._btnWrite = new System.Windows.Forms.Button();
      this._btnRead = new System.Windows.Forms.Button();
      this._Timer = new System.Windows.Forms.Timer(this.components);
      this._TimerPeriodicUpdate = new System.Windows.Forms.Timer(this.components);
      this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
      this._cbGainFactor = new System.Windows.Forms.ComboBox();
      this.tabControl1.SuspendLayout();
      this.tpDevice.SuspendLayout();
      this._gbPumpOnTimes.SuspendLayout();
      this._gbLogo.SuspendLayout();
      this.tpDateTime.SuspendLayout();
      this.tpSensor.SuspendLayout();
      this._gbSensor_PF.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this._nudP3)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._nudP2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._nudP1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._nudT4_Soll)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._nudT3_Soll)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._nudT2_Soll)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._nudT1_Soll)).BeginInit();
      this.tpAnalysis.SuspendLayout();
      this._gbPA.SuspendLayout();
      this._gbPS.SuspendLayout();
      this._gbSmoothing.SuspendLayout();
      this._gbLoad.SuspendLayout();
      this._gbError.SuspendLayout();
      this._gbComment.SuspendLayout();
      this.SuspendLayout();
      // 
      // tabControl1
      // 
      this.tabControl1.Controls.Add(this.tpDevice);
      this.tabControl1.Controls.Add(this.tpDateTime);
      this.tabControl1.Controls.Add(this.tpSensor);
      this.tabControl1.Controls.Add(this.tpAnalysis);
      this.tabControl1.Location = new System.Drawing.Point(12, 12);
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedIndex = 0;
      this.tabControl1.Size = new System.Drawing.Size(389, 417);
      this.tabControl1.TabIndex = 0;
      // 
      // tpDevice
      // 
      this.tpDevice.Controls.Add(this._txtPW);
      this.tpDevice.Controls.Add(this._lblPW);
      this.tpDevice.Controls.Add(this._chkPWOnOff);
      this.tpDevice.Controls.Add(this._chkErrConf);
      this.tpDevice.Controls.Add(this._txtSelfCheck);
      this.tpDevice.Controls.Add(this._lblSelfCheck);
      this.tpDevice.Controls.Add(this._chkDontChangePIDlamp);
      this.tpDevice.Controls.Add(this._gbPumpOnTimes);
      this.tpDevice.Controls.Add(this._gbLogo);
      this.tpDevice.Controls.Add(this._chkAccuDisplay);
      this.tpDevice.Controls.Add(this._chkResetPIDlamp);
      this.tpDevice.Controls.Add(this._txtPIDlamp);
      this.tpDevice.Controls.Add(this._lblPIDlamp);
      this.tpDevice.Controls.Add(this._txtCellNo);
      this.tpDevice.Controls.Add(this._txtDeviceNo);
      this.tpDevice.Controls.Add(this._lblCellNo);
      this.tpDevice.Controls.Add(this._lblDeviceNo);
      this.tpDevice.Location = new System.Drawing.Point(4, 22);
      this.tpDevice.Name = "tpDevice";
      this.tpDevice.Padding = new System.Windows.Forms.Padding(3);
      this.tpDevice.Size = new System.Drawing.Size(381, 391);
      this.tpDevice.TabIndex = 0;
      this.tpDevice.Text = "Device";
      this.tpDevice.UseVisualStyleBackColor = true;
      // 
      // _txtPW
      // 
      this._txtPW.Location = new System.Drawing.Point(313, 212);
      this._txtPW.Name = "_txtPW";
      this._txtPW.Size = new System.Drawing.Size(51, 20);
      this._txtPW.TabIndex = 16;
      this._txtPW.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblPW
      // 
      this._lblPW.Location = new System.Drawing.Point(215, 215);
      this._lblPW.Name = "_lblPW";
      this._lblPW.Size = new System.Drawing.Size(79, 18);
      this._lblPW.TabIndex = 15;
      this._lblPW.Text = "Password:";
      // 
      // _chkPWOnOff
      // 
      this._chkPWOnOff.Location = new System.Drawing.Point(12, 215);
      this._chkPWOnOff.Name = "_chkPWOnOff";
      this._chkPWOnOff.Size = new System.Drawing.Size(154, 18);
      this._chkPWOnOff.TabIndex = 14;
      this._chkPWOnOff.Text = "Password on / off";
      this._chkPWOnOff.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _chkErrConf
      // 
      this._chkErrConf.Location = new System.Drawing.Point(12, 196);
      this._chkErrConf.Name = "_chkErrConf";
      this._chkErrConf.Size = new System.Drawing.Size(212, 18);
      this._chkErrConf.TabIndex = 13;
      this._chkErrConf.Text = "Error confirmation on / off";
      this._chkErrConf.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtSelfCheck
      // 
      this._txtSelfCheck.Location = new System.Drawing.Point(332, 172);
      this._txtSelfCheck.Name = "_txtSelfCheck";
      this._txtSelfCheck.Size = new System.Drawing.Size(32, 20);
      this._txtSelfCheck.TabIndex = 12;
      this._txtSelfCheck.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblSelfCheck
      // 
      this._lblSelfCheck.Location = new System.Drawing.Point(12, 172);
      this._lblSelfCheck.Name = "_lblSelfCheck";
      this._lblSelfCheck.Size = new System.Drawing.Size(316, 20);
      this._lblSelfCheck.TabIndex = 11;
      this._lblSelfCheck.Text = "Min. duration of self-check OK state (in [1,24] sec):";
      // 
      // _chkDontChangePIDlamp
      // 
      this._chkDontChangePIDlamp.Location = new System.Drawing.Point(252, 51);
      this._chkDontChangePIDlamp.Name = "_chkDontChangePIDlamp";
      this._chkDontChangePIDlamp.Size = new System.Drawing.Size(96, 18);
      this._chkDontChangePIDlamp.TabIndex = 7;
      this._chkDontChangePIDlamp.Text = "Don\'t change";
      this._chkDontChangePIDlamp.CheckedChanged += new System.EventHandler(this._chkDontChangeOnTimes_CheckedChanged);
      this._chkDontChangePIDlamp.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _gbPumpOnTimes
      // 
      this._gbPumpOnTimes.Controls.Add(this._chkDontChangeP3On);
      this._gbPumpOnTimes.Controls.Add(this._chkDontChangeP2On);
      this._gbPumpOnTimes.Controls.Add(this._chkDontChangeP1On);
      this._gbPumpOnTimes.Controls.Add(this._chkResetP3On);
      this._gbPumpOnTimes.Controls.Add(this._chkResetP2On);
      this._gbPumpOnTimes.Controls.Add(this._chkResetP1On);
      this._gbPumpOnTimes.Controls.Add(this._txtP3On);
      this._gbPumpOnTimes.Controls.Add(this._lblP3On);
      this._gbPumpOnTimes.Controls.Add(this._txtP2On);
      this._gbPumpOnTimes.Controls.Add(this._lblP2On);
      this._gbPumpOnTimes.Controls.Add(this._txtP1On);
      this._gbPumpOnTimes.Controls.Add(this._lblP1On);
      this._gbPumpOnTimes.Location = new System.Drawing.Point(79, 78);
      this._gbPumpOnTimes.Name = "_gbPumpOnTimes";
      this._gbPumpOnTimes.Size = new System.Drawing.Size(290, 82);
      this._gbPumpOnTimes.TabIndex = 10;
      this._gbPumpOnTimes.TabStop = false;
      this._gbPumpOnTimes.Text = "Pump On times (in h)";
      // 
      // _chkDontChangeP3On
      // 
      this._chkDontChangeP3On.Location = new System.Drawing.Point(191, 60);
      this._chkDontChangeP3On.Name = "_chkDontChangeP3On";
      this._chkDontChangeP3On.Size = new System.Drawing.Size(96, 18);
      this._chkDontChangeP3On.TabIndex = 11;
      this._chkDontChangeP3On.Text = "Nicht ändern";
      this._chkDontChangeP3On.CheckedChanged += new System.EventHandler(this._chkDontChangeOnTimes_CheckedChanged);
      this._chkDontChangeP3On.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _chkDontChangeP2On
      // 
      this._chkDontChangeP2On.Location = new System.Drawing.Point(191, 40);
      this._chkDontChangeP2On.Name = "_chkDontChangeP2On";
      this._chkDontChangeP2On.Size = new System.Drawing.Size(96, 18);
      this._chkDontChangeP2On.TabIndex = 7;
      this._chkDontChangeP2On.Text = "Nicht ändern";
      this._chkDontChangeP2On.CheckedChanged += new System.EventHandler(this._chkDontChangeOnTimes_CheckedChanged);
      this._chkDontChangeP2On.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _chkDontChangeP1On
      // 
      this._chkDontChangeP1On.Location = new System.Drawing.Point(191, 20);
      this._chkDontChangeP1On.Name = "_chkDontChangeP1On";
      this._chkDontChangeP1On.Size = new System.Drawing.Size(96, 18);
      this._chkDontChangeP1On.TabIndex = 3;
      this._chkDontChangeP1On.Text = "Don\'t change";
      this._chkDontChangeP1On.CheckedChanged += new System.EventHandler(this._chkDontChangeOnTimes_CheckedChanged);
      this._chkDontChangeP1On.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _chkResetP3On
      // 
      this._chkResetP3On.Location = new System.Drawing.Point(118, 60);
      this._chkResetP3On.Name = "_chkResetP3On";
      this._chkResetP3On.Size = new System.Drawing.Size(68, 18);
      this._chkResetP3On.TabIndex = 10;
      this._chkResetP3On.Text = "Zurücks.";
      this._chkResetP3On.CheckedChanged += new System.EventHandler(this._chkResetOnTimes_CheckedChanged);
      this._chkResetP3On.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _chkResetP2On
      // 
      this._chkResetP2On.Location = new System.Drawing.Point(118, 40);
      this._chkResetP2On.Name = "_chkResetP2On";
      this._chkResetP2On.Size = new System.Drawing.Size(68, 18);
      this._chkResetP2On.TabIndex = 6;
      this._chkResetP2On.Text = "Zurücks.";
      this._chkResetP2On.CheckedChanged += new System.EventHandler(this._chkResetOnTimes_CheckedChanged);
      this._chkResetP2On.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _chkResetP1On
      // 
      this._chkResetP1On.Location = new System.Drawing.Point(118, 20);
      this._chkResetP1On.Name = "_chkResetP1On";
      this._chkResetP1On.Size = new System.Drawing.Size(68, 18);
      this._chkResetP1On.TabIndex = 2;
      this._chkResetP1On.Text = "Zurücks.";
      this._chkResetP1On.CheckedChanged += new System.EventHandler(this._chkResetOnTimes_CheckedChanged);
      this._chkResetP1On.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtP3On
      // 
      this._txtP3On.Location = new System.Drawing.Point(64, 58);
      this._txtP3On.Name = "_txtP3On";
      this._txtP3On.ReadOnly = true;
      this._txtP3On.Size = new System.Drawing.Size(49, 20);
      this._txtP3On.TabIndex = 9;
      this._txtP3On.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblP3On
      // 
      this._lblP3On.Location = new System.Drawing.Point(8, 60);
      this._lblP3On.Name = "_lblP3On";
      this._lblP3On.Size = new System.Drawing.Size(54, 18);
      this._lblP3On.TabIndex = 8;
      this._lblP3On.Text = "Pump3:";
      // 
      // _txtP2On
      // 
      this._txtP2On.Location = new System.Drawing.Point(64, 38);
      this._txtP2On.Name = "_txtP2On";
      this._txtP2On.ReadOnly = true;
      this._txtP2On.Size = new System.Drawing.Size(49, 20);
      this._txtP2On.TabIndex = 5;
      this._txtP2On.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblP2On
      // 
      this._lblP2On.Location = new System.Drawing.Point(8, 40);
      this._lblP2On.Name = "_lblP2On";
      this._lblP2On.Size = new System.Drawing.Size(54, 18);
      this._lblP2On.TabIndex = 4;
      this._lblP2On.Text = "Pump2:";
      // 
      // _txtP1On
      // 
      this._txtP1On.Location = new System.Drawing.Point(64, 18);
      this._txtP1On.Name = "_txtP1On";
      this._txtP1On.ReadOnly = true;
      this._txtP1On.Size = new System.Drawing.Size(49, 20);
      this._txtP1On.TabIndex = 1;
      this._txtP1On.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblP1On
      // 
      this._lblP1On.Location = new System.Drawing.Point(8, 20);
      this._lblP1On.Name = "_lblP1On";
      this._lblP1On.Size = new System.Drawing.Size(54, 18);
      this._lblP1On.TabIndex = 0;
      this._lblP1On.Text = "Pump1:";
      // 
      // _gbLogo
      // 
      this._gbLogo.Controls.Add(this._rbLogoENVI);
      this._gbLogo.Controls.Add(this._rbLogoENIT);
      this._gbLogo.Controls.Add(this._rbLogoIUT);
      this._gbLogo.Location = new System.Drawing.Point(12, 78);
      this._gbLogo.Name = "_gbLogo";
      this._gbLogo.Size = new System.Drawing.Size(62, 82);
      this._gbLogo.TabIndex = 9;
      this._gbLogo.TabStop = false;
      this._gbLogo.Text = "Logo";
      // 
      // _rbLogoENVI
      // 
      this._rbLogoENVI.Location = new System.Drawing.Point(8, 60);
      this._rbLogoENVI.Name = "_rbLogoENVI";
      this._rbLogoENVI.Size = new System.Drawing.Size(50, 18);
      this._rbLogoENVI.TabIndex = 2;
      this._rbLogoENVI.TabStop = true;
      this._rbLogoENVI.Text = "ENVI";
      this._rbLogoENVI.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _rbLogoENIT
      // 
      this._rbLogoENIT.Location = new System.Drawing.Point(8, 40);
      this._rbLogoENIT.Name = "_rbLogoENIT";
      this._rbLogoENIT.Size = new System.Drawing.Size(50, 18);
      this._rbLogoENIT.TabIndex = 1;
      this._rbLogoENIT.TabStop = true;
      this._rbLogoENIT.Text = "ENIT";
      this._rbLogoENIT.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _rbLogoIUT
      // 
      this._rbLogoIUT.Location = new System.Drawing.Point(8, 20);
      this._rbLogoIUT.Name = "_rbLogoIUT";
      this._rbLogoIUT.Size = new System.Drawing.Size(50, 18);
      this._rbLogoIUT.TabIndex = 0;
      this._rbLogoIUT.TabStop = true;
      this._rbLogoIUT.Text = "IUT";
      this._rbLogoIUT.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _chkAccuDisplay
      // 
      this._chkAccuDisplay.Location = new System.Drawing.Point(12, 56);
      this._chkAccuDisplay.Name = "_chkAccuDisplay";
      this._chkAccuDisplay.Size = new System.Drawing.Size(156, 18);
      this._chkAccuDisplay.TabIndex = 8;
      this._chkAccuDisplay.Text = "Accu-Anzeige ein / aus";
      this._chkAccuDisplay.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _chkResetPIDlamp
      // 
      this._chkResetPIDlamp.Location = new System.Drawing.Point(252, 34);
      this._chkResetPIDlamp.Name = "_chkResetPIDlamp";
      this._chkResetPIDlamp.Size = new System.Drawing.Size(72, 18);
      this._chkResetPIDlamp.TabIndex = 6;
      this._chkResetPIDlamp.Text = "Zurücks.";
      this._chkResetPIDlamp.CheckedChanged += new System.EventHandler(this._chkResetOnTimes_CheckedChanged);
      this._chkResetPIDlamp.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtPIDlamp
      // 
      this._txtPIDlamp.Location = new System.Drawing.Point(172, 32);
      this._txtPIDlamp.Name = "_txtPIDlamp";
      this._txtPIDlamp.ReadOnly = true;
      this._txtPIDlamp.Size = new System.Drawing.Size(76, 20);
      this._txtPIDlamp.TabIndex = 5;
      this._txtPIDlamp.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblPIDlamp
      // 
      this._lblPIDlamp.Location = new System.Drawing.Point(172, 12);
      this._lblPIDlamp.Name = "_lblPIDlamp";
      this._lblPIDlamp.Size = new System.Drawing.Size(152, 18);
      this._lblPIDlamp.TabIndex = 4;
      this._lblPIDlamp.Text = "PID lamp time (in h):";
      // 
      // _txtCellNo
      // 
      this._txtCellNo.Location = new System.Drawing.Point(92, 32);
      this._txtCellNo.Name = "_txtCellNo";
      this._txtCellNo.Size = new System.Drawing.Size(76, 20);
      this._txtCellNo.TabIndex = 3;
      this._txtCellNo.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtDeviceNo
      // 
      this._txtDeviceNo.Location = new System.Drawing.Point(12, 32);
      this._txtDeviceNo.Name = "_txtDeviceNo";
      this._txtDeviceNo.Size = new System.Drawing.Size(76, 20);
      this._txtDeviceNo.TabIndex = 1;
      this._txtDeviceNo.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblCellNo
      // 
      this._lblCellNo.Location = new System.Drawing.Point(92, 12);
      this._lblCellNo.Name = "_lblCellNo";
      this._lblCellNo.Size = new System.Drawing.Size(76, 18);
      this._lblCellNo.TabIndex = 2;
      this._lblCellNo.Text = "Cell number:";
      // 
      // _lblDeviceNo
      // 
      this._lblDeviceNo.Location = new System.Drawing.Point(12, 12);
      this._lblDeviceNo.Name = "_lblDeviceNo";
      this._lblDeviceNo.Size = new System.Drawing.Size(76, 18);
      this._lblDeviceNo.TabIndex = 0;
      this._lblDeviceNo.Text = "Device no.:";
      // 
      // tpDateTime
      // 
      this.tpDateTime.Controls.Add(this._dtpSvcDate);
      this.tpDateTime.Controls.Add(this._lblSvcDate);
      this.tpDateTime.Controls.Add(this._dtpTime);
      this.tpDateTime.Controls.Add(this._dtpDate);
      this.tpDateTime.Controls.Add(this._lblTime);
      this.tpDateTime.Controls.Add(this._lblDate);
      this.tpDateTime.Location = new System.Drawing.Point(4, 22);
      this.tpDateTime.Name = "tpDateTime";
      this.tpDateTime.Padding = new System.Windows.Forms.Padding(3);
      this.tpDateTime.Size = new System.Drawing.Size(381, 391);
      this.tpDateTime.TabIndex = 1;
      this.tpDateTime.Text = "Datetime";
      this.tpDateTime.UseVisualStyleBackColor = true;
      // 
      // _dtpSvcDate
      // 
      this._dtpSvcDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
      this._dtpSvcDate.Location = new System.Drawing.Point(220, 32);
      this._dtpSvcDate.Name = "_dtpSvcDate";
      this._dtpSvcDate.Size = new System.Drawing.Size(100, 20);
      this._dtpSvcDate.TabIndex = 5;
      this._dtpSvcDate.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblSvcDate
      // 
      this._lblSvcDate.Location = new System.Drawing.Point(220, 12);
      this._lblSvcDate.Name = "_lblSvcDate";
      this._lblSvcDate.Size = new System.Drawing.Size(100, 18);
      this._lblSvcDate.TabIndex = 4;
      this._lblSvcDate.Text = "Service Date:";
      // 
      // _dtpTime
      // 
      this._dtpTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
      this._dtpTime.Location = new System.Drawing.Point(116, 32);
      this._dtpTime.Name = "_dtpTime";
      this._dtpTime.ShowUpDown = true;
      this._dtpTime.Size = new System.Drawing.Size(100, 20);
      this._dtpTime.TabIndex = 3;
      this._dtpTime.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _dtpDate
      // 
      this._dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
      this._dtpDate.Location = new System.Drawing.Point(12, 32);
      this._dtpDate.Name = "_dtpDate";
      this._dtpDate.Size = new System.Drawing.Size(100, 20);
      this._dtpDate.TabIndex = 1;
      this._dtpDate.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblTime
      // 
      this._lblTime.Location = new System.Drawing.Point(116, 12);
      this._lblTime.Name = "_lblTime";
      this._lblTime.Size = new System.Drawing.Size(100, 18);
      this._lblTime.TabIndex = 2;
      this._lblTime.Text = "Time:";
      // 
      // _lblDate
      // 
      this._lblDate.Location = new System.Drawing.Point(12, 12);
      this._lblDate.Name = "_lblDate";
      this._lblDate.Size = new System.Drawing.Size(100, 18);
      this._lblDate.TabIndex = 0;
      this._lblDate.Text = "Date:";
      // 
      // tpSensor
      // 
      this.tpSensor.Controls.Add(this._txtMFCcap);
      this.tpSensor.Controls.Add(this._lblMFCcap);
      this.tpSensor.Controls.Add(this._lblpm_3);
      this.tpSensor.Controls.Add(this._txtF3_Soll_TolAbs);
      this.tpSensor.Controls.Add(this._txtF3_Soll);
      this.tpSensor.Controls.Add(this._lblF3);
      this.tpSensor.Controls.Add(this._txtF3_Ist);
      this.tpSensor.Controls.Add(this._lblTolAbs);
      this.tpSensor.Controls.Add(this._lblSoll_1);
      this.tpSensor.Controls.Add(this._lblIst_1);
      this.tpSensor.Controls.Add(this._gbSensor_PF);
      this.tpSensor.Controls.Add(this._txtKP4);
      this.tpSensor.Controls.Add(this._txtKP3);
      this.tpSensor.Controls.Add(this._txtKP2);
      this.tpSensor.Controls.Add(this._txtKP1);
      this.tpSensor.Controls.Add(this._lblKP);
      this.tpSensor.Controls.Add(this._lblpm_1);
      this.tpSensor.Controls.Add(this._lblpm);
      this.tpSensor.Controls.Add(this._txtF2_Soll_TolAbs);
      this.tpSensor.Controls.Add(this._txtF1_Soll_TolAbs);
      this.tpSensor.Controls.Add(this._txtF2_Soll);
      this.tpSensor.Controls.Add(this._txtF1_Soll);
      this.tpSensor.Controls.Add(this._txtF2_Ist);
      this.tpSensor.Controls.Add(this._txtF1_Ist);
      this.tpSensor.Controls.Add(this._chkPresOff);
      this.tpSensor.Controls.Add(this._lblF2);
      this.tpSensor.Controls.Add(this._lblF1);
      this.tpSensor.Controls.Add(this._chkT4Off);
      this.tpSensor.Controls.Add(this._chkT3Off);
      this.tpSensor.Controls.Add(this._chkT2Off);
      this.tpSensor.Controls.Add(this._chkT1Off);
      this.tpSensor.Controls.Add(this._nudT4_Soll);
      this.tpSensor.Controls.Add(this._nudT3_Soll);
      this.tpSensor.Controls.Add(this._nudT2_Soll);
      this.tpSensor.Controls.Add(this._nudT1_Soll);
      this.tpSensor.Controls.Add(this._txtPres);
      this.tpSensor.Controls.Add(this._txtT4_Ist);
      this.tpSensor.Controls.Add(this._txtT2_Ist);
      this.tpSensor.Controls.Add(this._txtT3_Ist);
      this.tpSensor.Controls.Add(this._txtT1_Ist);
      this.tpSensor.Controls.Add(this._lblSoll);
      this.tpSensor.Controls.Add(this._lblIst);
      this.tpSensor.Controls.Add(this._lblPres);
      this.tpSensor.Controls.Add(this._lblT4);
      this.tpSensor.Controls.Add(this._lblT3);
      this.tpSensor.Controls.Add(this._lblT2);
      this.tpSensor.Controls.Add(this._lblT1);
      this.tpSensor.Location = new System.Drawing.Point(4, 22);
      this.tpSensor.Name = "tpSensor";
      this.tpSensor.Size = new System.Drawing.Size(381, 391);
      this.tpSensor.TabIndex = 2;
      this.tpSensor.Text = "Sensor";
      this.tpSensor.UseVisualStyleBackColor = true;
      // 
      // _txtMFCcap
      // 
      this._txtMFCcap.Location = new System.Drawing.Point(176, 328);
      this._txtMFCcap.Name = "_txtMFCcap";
      this._txtMFCcap.ReadOnly = true;
      this._txtMFCcap.Size = new System.Drawing.Size(43, 20);
      this._txtMFCcap.TabIndex = 38;
      this._txtMFCcap.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblMFCcap
      // 
      this._lblMFCcap.Location = new System.Drawing.Point(173, 284);
      this._lblMFCcap.Name = "_lblMFCcap";
      this._lblMFCcap.Size = new System.Drawing.Size(47, 28);
      this._lblMFCcap.TabIndex = 28;
      this._lblMFCcap.Text = "MFC-Kapazität:";
      // 
      // _lblpm_3
      // 
      this._lblpm_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._lblpm_3.Location = new System.Drawing.Point(283, 350);
      this._lblpm_3.Name = "_lblpm_3";
      this._lblpm_3.Size = new System.Drawing.Size(32, 18);
      this._lblpm_3.TabIndex = 45;
      this._lblpm_3.Text = "+/-";
      // 
      // _txtF3_Soll_TolAbs
      // 
      this._txtF3_Soll_TolAbs.Location = new System.Drawing.Point(317, 350);
      this._txtF3_Soll_TolAbs.Name = "_txtF3_Soll_TolAbs";
      this._txtF3_Soll_TolAbs.Size = new System.Drawing.Size(52, 20);
      this._txtF3_Soll_TolAbs.TabIndex = 46;
      this._txtF3_Soll_TolAbs.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF3_Soll
      // 
      this._txtF3_Soll.Location = new System.Drawing.Point(223, 350);
      this._txtF3_Soll.Name = "_txtF3_Soll";
      this._txtF3_Soll.Size = new System.Drawing.Size(56, 20);
      this._txtF3_Soll.TabIndex = 44;
      this._txtF3_Soll.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblF3
      // 
      this._lblF3.Location = new System.Drawing.Point(12, 350);
      this._lblF3.Name = "_lblF3";
      this._lblF3.Size = new System.Drawing.Size(100, 18);
      this._lblF3.TabIndex = 42;
      this._lblF3.Text = "Flow3 (ml/min):";
      // 
      // _txtF3_Ist
      // 
      this._txtF3_Ist.Location = new System.Drawing.Point(116, 350);
      this._txtF3_Ist.Name = "_txtF3_Ist";
      this._txtF3_Ist.ReadOnly = true;
      this._txtF3_Ist.Size = new System.Drawing.Size(56, 20);
      this._txtF3_Ist.TabIndex = 43;
      this._txtF3_Ist.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblTolAbs
      // 
      this._lblTolAbs.Location = new System.Drawing.Point(319, 284);
      this._lblTolAbs.Name = "_lblTolAbs";
      this._lblTolAbs.Size = new System.Drawing.Size(52, 18);
      this._lblTolAbs.TabIndex = 30;
      this._lblTolAbs.Text = "Tol.:";
      // 
      // _lblSoll_1
      // 
      this._lblSoll_1.Location = new System.Drawing.Point(225, 284);
      this._lblSoll_1.Name = "_lblSoll_1";
      this._lblSoll_1.Size = new System.Drawing.Size(56, 18);
      this._lblSoll_1.TabIndex = 29;
      this._lblSoll_1.Text = "Soll:";
      // 
      // _lblIst_1
      // 
      this._lblIst_1.Location = new System.Drawing.Point(118, 284);
      this._lblIst_1.Name = "_lblIst_1";
      this._lblIst_1.Size = new System.Drawing.Size(56, 18);
      this._lblIst_1.TabIndex = 27;
      this._lblIst_1.Text = "Ist:";
      // 
      // _gbSensor_PF
      // 
      this._gbSensor_PF.Controls.Add(this._lblPump);
      this._gbSensor_PF.Controls.Add(this._lblFlow);
      this._gbSensor_PF.Controls.Add(this._lblPFHandling);
      this._gbSensor_PF.Controls.Add(this._chkPF3);
      this._gbSensor_PF.Controls.Add(this._chkPF2);
      this._gbSensor_PF.Controls.Add(this._chkPF1);
      this._gbSensor_PF.Controls.Add(this._nudP3);
      this._gbSensor_PF.Controls.Add(this._nudP2);
      this._gbSensor_PF.Controls.Add(this._nudP1);
      this._gbSensor_PF.Controls.Add(this._lblP3);
      this._gbSensor_PF.Controls.Add(this._lblP2);
      this._gbSensor_PF.Controls.Add(this._lblP1);
      this._gbSensor_PF.Controls.Add(this._txtF3);
      this._gbSensor_PF.Controls.Add(this._txtF2);
      this._gbSensor_PF.Controls.Add(this._txtF1);
      this._gbSensor_PF.Location = new System.Drawing.Point(8, 150);
      this._gbSensor_PF.Name = "_gbSensor_PF";
      this._gbSensor_PF.Size = new System.Drawing.Size(320, 126);
      this._gbSensor_PF.TabIndex = 26;
      this._gbSensor_PF.TabStop = false;
      this._gbSensor_PF.Text = "Pump - Flow handling";
      // 
      // _lblPump
      // 
      this._lblPump.Location = new System.Drawing.Point(168, 22);
      this._lblPump.Name = "_lblPump";
      this._lblPump.Size = new System.Drawing.Size(50, 20);
      this._lblPump.TabIndex = 1;
      this._lblPump.Text = "Pump";
      // 
      // _lblFlow
      // 
      this._lblFlow.Location = new System.Drawing.Point(84, 22);
      this._lblFlow.Name = "_lblFlow";
      this._lblFlow.Size = new System.Drawing.Size(80, 20);
      this._lblFlow.TabIndex = 0;
      this._lblFlow.Text = "Flow";
      // 
      // _lblPFHandling
      // 
      this._lblPFHandling.Location = new System.Drawing.Point(228, 22);
      this._lblPFHandling.Name = "_lblPFHandling";
      this._lblPFHandling.Size = new System.Drawing.Size(86, 28);
      this._lblPFHandling.TabIndex = 2;
      this._lblPFHandling.Text = "P-F Handhabung:";
      // 
      // _chkPF3
      // 
      this._chkPF3.Location = new System.Drawing.Point(262, 98);
      this._chkPF3.Name = "_chkPF3";
      this._chkPF3.Size = new System.Drawing.Size(52, 18);
      this._chkPF3.TabIndex = 14;
      this._chkPF3.Text = "P3";
      this._chkPF3.CheckedChanged += new System.EventHandler(this._chkPF_CheckedChanged);
      this._chkPF3.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _chkPF2
      // 
      this._chkPF2.Location = new System.Drawing.Point(262, 76);
      this._chkPF2.Name = "_chkPF2";
      this._chkPF2.Size = new System.Drawing.Size(52, 18);
      this._chkPF2.TabIndex = 10;
      this._chkPF2.Text = "P2";
      this._chkPF2.CheckedChanged += new System.EventHandler(this._chkPF_CheckedChanged);
      this._chkPF2.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _chkPF1
      // 
      this._chkPF1.Location = new System.Drawing.Point(262, 54);
      this._chkPF1.Name = "_chkPF1";
      this._chkPF1.Size = new System.Drawing.Size(52, 18);
      this._chkPF1.TabIndex = 6;
      this._chkPF1.Text = "P1";
      this._chkPF1.CheckedChanged += new System.EventHandler(this._chkPF_CheckedChanged);
      this._chkPF1.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _nudP3
      // 
      this._nudP3.Location = new System.Drawing.Point(166, 98);
      this._nudP3.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this._nudP3.Name = "_nudP3";
      this._nudP3.Size = new System.Drawing.Size(80, 20);
      this._nudP3.TabIndex = 13;
      this._nudP3.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _nudP2
      // 
      this._nudP2.Location = new System.Drawing.Point(166, 76);
      this._nudP2.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this._nudP2.Name = "_nudP2";
      this._nudP2.Size = new System.Drawing.Size(80, 20);
      this._nudP2.TabIndex = 9;
      this._nudP2.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _nudP1
      // 
      this._nudP1.Location = new System.Drawing.Point(166, 54);
      this._nudP1.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this._nudP1.Name = "_nudP1";
      this._nudP1.Size = new System.Drawing.Size(80, 20);
      this._nudP1.TabIndex = 5;
      this._nudP1.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblP3
      // 
      this._lblP3.Location = new System.Drawing.Point(8, 98);
      this._lblP3.Name = "_lblP3";
      this._lblP3.Size = new System.Drawing.Size(70, 18);
      this._lblP3.TabIndex = 11;
      this._lblP3.Text = "Pump3:";
      // 
      // _lblP2
      // 
      this._lblP2.Location = new System.Drawing.Point(8, 76);
      this._lblP2.Name = "_lblP2";
      this._lblP2.Size = new System.Drawing.Size(70, 18);
      this._lblP2.TabIndex = 7;
      this._lblP2.Text = "Pump2:";
      // 
      // _lblP1
      // 
      this._lblP1.Location = new System.Drawing.Point(8, 54);
      this._lblP1.Name = "_lblP1";
      this._lblP1.Size = new System.Drawing.Size(70, 18);
      this._lblP1.TabIndex = 3;
      this._lblP1.Text = "Pump1:";
      // 
      // _txtF3
      // 
      this._txtF3.Location = new System.Drawing.Point(82, 98);
      this._txtF3.Name = "_txtF3";
      this._txtF3.Size = new System.Drawing.Size(80, 20);
      this._txtF3.TabIndex = 12;
      this._txtF3.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF2
      // 
      this._txtF2.Location = new System.Drawing.Point(82, 76);
      this._txtF2.Name = "_txtF2";
      this._txtF2.Size = new System.Drawing.Size(80, 20);
      this._txtF2.TabIndex = 8;
      this._txtF2.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF1
      // 
      this._txtF1.Location = new System.Drawing.Point(82, 54);
      this._txtF1.Name = "_txtF1";
      this._txtF1.Size = new System.Drawing.Size(80, 20);
      this._txtF1.TabIndex = 4;
      this._txtF1.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtKP4
      // 
      this._txtKP4.Location = new System.Drawing.Point(284, 100);
      this._txtKP4.Name = "_txtKP4";
      this._txtKP4.Size = new System.Drawing.Size(38, 20);
      this._txtKP4.TabIndex = 22;
      this._txtKP4.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtKP3
      // 
      this._txtKP3.Location = new System.Drawing.Point(284, 78);
      this._txtKP3.Name = "_txtKP3";
      this._txtKP3.Size = new System.Drawing.Size(38, 20);
      this._txtKP3.TabIndex = 17;
      this._txtKP3.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtKP2
      // 
      this._txtKP2.Location = new System.Drawing.Point(284, 56);
      this._txtKP2.Name = "_txtKP2";
      this._txtKP2.Size = new System.Drawing.Size(38, 20);
      this._txtKP2.TabIndex = 12;
      this._txtKP2.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtKP1
      // 
      this._txtKP1.Location = new System.Drawing.Point(284, 34);
      this._txtKP1.Name = "_txtKP1";
      this._txtKP1.Size = new System.Drawing.Size(38, 20);
      this._txtKP1.TabIndex = 7;
      this._txtKP1.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblKP
      // 
      this._lblKP.Location = new System.Drawing.Point(266, 12);
      this._lblKP.Name = "_lblKP";
      this._lblKP.Size = new System.Drawing.Size(58, 18);
      this._lblKP.TabIndex = 2;
      this._lblKP.Text = "P-Faktor:";
      // 
      // _lblpm_1
      // 
      this._lblpm_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._lblpm_1.Location = new System.Drawing.Point(283, 328);
      this._lblpm_1.Name = "_lblpm_1";
      this._lblpm_1.Size = new System.Drawing.Size(32, 18);
      this._lblpm_1.TabIndex = 40;
      this._lblpm_1.Text = "+/-";
      // 
      // _lblpm
      // 
      this._lblpm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._lblpm.Location = new System.Drawing.Point(283, 306);
      this._lblpm.Name = "_lblpm";
      this._lblpm.Size = new System.Drawing.Size(32, 18);
      this._lblpm.TabIndex = 34;
      this._lblpm.Text = "+/-";
      // 
      // _txtF2_Soll_TolAbs
      // 
      this._txtF2_Soll_TolAbs.Location = new System.Drawing.Point(317, 328);
      this._txtF2_Soll_TolAbs.Name = "_txtF2_Soll_TolAbs";
      this._txtF2_Soll_TolAbs.Size = new System.Drawing.Size(52, 20);
      this._txtF2_Soll_TolAbs.TabIndex = 41;
      this._txtF2_Soll_TolAbs.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF1_Soll_TolAbs
      // 
      this._txtF1_Soll_TolAbs.Location = new System.Drawing.Point(317, 306);
      this._txtF1_Soll_TolAbs.Name = "_txtF1_Soll_TolAbs";
      this._txtF1_Soll_TolAbs.Size = new System.Drawing.Size(52, 20);
      this._txtF1_Soll_TolAbs.TabIndex = 35;
      this._txtF1_Soll_TolAbs.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF2_Soll
      // 
      this._txtF2_Soll.Location = new System.Drawing.Point(223, 328);
      this._txtF2_Soll.Name = "_txtF2_Soll";
      this._txtF2_Soll.Size = new System.Drawing.Size(56, 20);
      this._txtF2_Soll.TabIndex = 39;
      this._txtF2_Soll.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF1_Soll
      // 
      this._txtF1_Soll.Location = new System.Drawing.Point(223, 306);
      this._txtF1_Soll.Name = "_txtF1_Soll";
      this._txtF1_Soll.Size = new System.Drawing.Size(56, 20);
      this._txtF1_Soll.TabIndex = 33;
      this._txtF1_Soll.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF2_Ist
      // 
      this._txtF2_Ist.Location = new System.Drawing.Point(116, 328);
      this._txtF2_Ist.Name = "_txtF2_Ist";
      this._txtF2_Ist.ReadOnly = true;
      this._txtF2_Ist.Size = new System.Drawing.Size(56, 20);
      this._txtF2_Ist.TabIndex = 37;
      this._txtF2_Ist.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF1_Ist
      // 
      this._txtF1_Ist.Location = new System.Drawing.Point(116, 306);
      this._txtF1_Ist.Name = "_txtF1_Ist";
      this._txtF1_Ist.ReadOnly = true;
      this._txtF1_Ist.Size = new System.Drawing.Size(56, 20);
      this._txtF1_Ist.TabIndex = 32;
      this._txtF1_Ist.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _chkPresOff
      // 
      this._chkPresOff.Location = new System.Drawing.Point(220, 122);
      this._chkPresOff.Name = "_chkPresOff";
      this._chkPresOff.Size = new System.Drawing.Size(102, 18);
      this._chkPresOff.TabIndex = 25;
      this._chkPresOff.Text = "Display off";
      this._chkPresOff.CheckedChanged += new System.EventHandler(this._chkPresOff_CheckedChanged);
      this._chkPresOff.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblF2
      // 
      this._lblF2.Location = new System.Drawing.Point(12, 328);
      this._lblF2.Name = "_lblF2";
      this._lblF2.Size = new System.Drawing.Size(100, 18);
      this._lblF2.TabIndex = 36;
      this._lblF2.Text = "Flow2 (ml/min):";
      // 
      // _lblF1
      // 
      this._lblF1.Location = new System.Drawing.Point(12, 306);
      this._lblF1.Name = "_lblF1";
      this._lblF1.Size = new System.Drawing.Size(100, 18);
      this._lblF1.TabIndex = 31;
      this._lblF1.Text = "Flow1 (ml/min):";
      // 
      // _chkT4Off
      // 
      this._chkT4Off.Location = new System.Drawing.Point(236, 100);
      this._chkT4Off.Name = "_chkT4Off";
      this._chkT4Off.Size = new System.Drawing.Size(44, 20);
      this._chkT4Off.TabIndex = 21;
      this._chkT4Off.Text = "Off";
      this._chkT4Off.CheckedChanged += new System.EventHandler(this._chkTOff_CheckedChanged);
      this._chkT4Off.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _chkT3Off
      // 
      this._chkT3Off.Location = new System.Drawing.Point(236, 78);
      this._chkT3Off.Name = "_chkT3Off";
      this._chkT3Off.Size = new System.Drawing.Size(44, 20);
      this._chkT3Off.TabIndex = 16;
      this._chkT3Off.Text = "Off";
      this._chkT3Off.CheckedChanged += new System.EventHandler(this._chkTOff_CheckedChanged);
      this._chkT3Off.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _chkT2Off
      // 
      this._chkT2Off.Location = new System.Drawing.Point(236, 56);
      this._chkT2Off.Name = "_chkT2Off";
      this._chkT2Off.Size = new System.Drawing.Size(44, 20);
      this._chkT2Off.TabIndex = 11;
      this._chkT2Off.Text = "Off";
      this._chkT2Off.CheckedChanged += new System.EventHandler(this._chkTOff_CheckedChanged);
      this._chkT2Off.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _chkT1Off
      // 
      this._chkT1Off.Location = new System.Drawing.Point(236, 34);
      this._chkT1Off.Name = "_chkT1Off";
      this._chkT1Off.Size = new System.Drawing.Size(44, 20);
      this._chkT1Off.TabIndex = 6;
      this._chkT1Off.Text = "Off";
      this._chkT1Off.CheckedChanged += new System.EventHandler(this._chkTOff_CheckedChanged);
      this._chkT1Off.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _nudT4_Soll
      // 
      this._nudT4_Soll.Location = new System.Drawing.Point(180, 100);
      this._nudT4_Soll.Maximum = new decimal(new int[] {
            250,
            0,
            0,
            0});
      this._nudT4_Soll.Minimum = new decimal(new int[] {
            40,
            0,
            0,
            0});
      this._nudT4_Soll.Name = "_nudT4_Soll";
      this._nudT4_Soll.Size = new System.Drawing.Size(52, 20);
      this._nudT4_Soll.TabIndex = 20;
      this._nudT4_Soll.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
      this._nudT4_Soll.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _nudT3_Soll
      // 
      this._nudT3_Soll.Location = new System.Drawing.Point(180, 78);
      this._nudT3_Soll.Maximum = new decimal(new int[] {
            250,
            0,
            0,
            0});
      this._nudT3_Soll.Minimum = new decimal(new int[] {
            40,
            0,
            0,
            0});
      this._nudT3_Soll.Name = "_nudT3_Soll";
      this._nudT3_Soll.Size = new System.Drawing.Size(52, 20);
      this._nudT3_Soll.TabIndex = 15;
      this._nudT3_Soll.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
      this._nudT3_Soll.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _nudT2_Soll
      // 
      this._nudT2_Soll.Location = new System.Drawing.Point(180, 56);
      this._nudT2_Soll.Maximum = new decimal(new int[] {
            250,
            0,
            0,
            0});
      this._nudT2_Soll.Minimum = new decimal(new int[] {
            40,
            0,
            0,
            0});
      this._nudT2_Soll.Name = "_nudT2_Soll";
      this._nudT2_Soll.Size = new System.Drawing.Size(52, 20);
      this._nudT2_Soll.TabIndex = 10;
      this._nudT2_Soll.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
      this._nudT2_Soll.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _nudT1_Soll
      // 
      this._nudT1_Soll.Location = new System.Drawing.Point(180, 34);
      this._nudT1_Soll.Maximum = new decimal(new int[] {
            250,
            0,
            0,
            0});
      this._nudT1_Soll.Minimum = new decimal(new int[] {
            40,
            0,
            0,
            0});
      this._nudT1_Soll.Name = "_nudT1_Soll";
      this._nudT1_Soll.Size = new System.Drawing.Size(52, 20);
      this._nudT1_Soll.TabIndex = 5;
      this._nudT1_Soll.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
      this._nudT1_Soll.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtPres
      // 
      this._txtPres.Location = new System.Drawing.Point(116, 122);
      this._txtPres.Name = "_txtPres";
      this._txtPres.ReadOnly = true;
      this._txtPres.Size = new System.Drawing.Size(100, 20);
      this._txtPres.TabIndex = 24;
      this._txtPres.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtT4_Ist
      // 
      this._txtT4_Ist.Location = new System.Drawing.Point(116, 100);
      this._txtT4_Ist.Name = "_txtT4_Ist";
      this._txtT4_Ist.ReadOnly = true;
      this._txtT4_Ist.Size = new System.Drawing.Size(60, 20);
      this._txtT4_Ist.TabIndex = 19;
      this._txtT4_Ist.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtT2_Ist
      // 
      this._txtT2_Ist.Location = new System.Drawing.Point(116, 56);
      this._txtT2_Ist.Name = "_txtT2_Ist";
      this._txtT2_Ist.ReadOnly = true;
      this._txtT2_Ist.Size = new System.Drawing.Size(60, 20);
      this._txtT2_Ist.TabIndex = 9;
      this._txtT2_Ist.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtT3_Ist
      // 
      this._txtT3_Ist.Location = new System.Drawing.Point(116, 78);
      this._txtT3_Ist.Name = "_txtT3_Ist";
      this._txtT3_Ist.ReadOnly = true;
      this._txtT3_Ist.Size = new System.Drawing.Size(60, 20);
      this._txtT3_Ist.TabIndex = 14;
      this._txtT3_Ist.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtT1_Ist
      // 
      this._txtT1_Ist.Location = new System.Drawing.Point(116, 34);
      this._txtT1_Ist.Name = "_txtT1_Ist";
      this._txtT1_Ist.ReadOnly = true;
      this._txtT1_Ist.Size = new System.Drawing.Size(60, 20);
      this._txtT1_Ist.TabIndex = 4;
      this._txtT1_Ist.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblSoll
      // 
      this._lblSoll.Location = new System.Drawing.Point(180, 12);
      this._lblSoll.Name = "_lblSoll";
      this._lblSoll.Size = new System.Drawing.Size(80, 18);
      this._lblSoll.TabIndex = 1;
      this._lblSoll.Text = "Soll:";
      // 
      // _lblIst
      // 
      this._lblIst.Location = new System.Drawing.Point(116, 12);
      this._lblIst.Name = "_lblIst";
      this._lblIst.Size = new System.Drawing.Size(60, 18);
      this._lblIst.TabIndex = 0;
      this._lblIst.Text = "Ist:";
      // 
      // _lblPres
      // 
      this._lblPres.Location = new System.Drawing.Point(12, 122);
      this._lblPres.Name = "_lblPres";
      this._lblPres.Size = new System.Drawing.Size(100, 18);
      this._lblPres.TabIndex = 23;
      this._lblPres.Text = "Pressure (kPa):";
      // 
      // _lblT4
      // 
      this._lblT4.Location = new System.Drawing.Point(12, 100);
      this._lblT4.Name = "_lblT4";
      this._lblT4.Size = new System.Drawing.Size(100, 18);
      this._lblT4.TabIndex = 18;
      this._lblT4.Text = "Temp4 (°C):";
      // 
      // _lblT3
      // 
      this._lblT3.Location = new System.Drawing.Point(12, 78);
      this._lblT3.Name = "_lblT3";
      this._lblT3.Size = new System.Drawing.Size(100, 18);
      this._lblT3.TabIndex = 13;
      this._lblT3.Text = "Temp3 (°C):";
      // 
      // _lblT2
      // 
      this._lblT2.Location = new System.Drawing.Point(12, 56);
      this._lblT2.Name = "_lblT2";
      this._lblT2.Size = new System.Drawing.Size(100, 18);
      this._lblT2.TabIndex = 8;
      this._lblT2.Text = "Temp2 (°C):";
      // 
      // _lblT1
      // 
      this._lblT1.Location = new System.Drawing.Point(12, 34);
      this._lblT1.Name = "_lblT1";
      this._lblT1.Size = new System.Drawing.Size(100, 18);
      this._lblT1.TabIndex = 3;
      this._lblT1.Text = "Temp1 (°C):";
      // 
      // tpAnalysis
      // 
      this.tpAnalysis.Controls.Add(this._cbGainFactor);
      this.tpAnalysis.Controls.Add(this._lblScanOffset_Max);
      this.tpAnalysis.Controls.Add(this._lblScanOffset_Min);
      this.tpAnalysis.Controls.Add(this._gbPA);
      this.tpAnalysis.Controls.Add(this._gbPS);
      this.tpAnalysis.Controls.Add(this._gbSmoothing);
      this.tpAnalysis.Controls.Add(this._txtStatScanOffset);
      this.tpAnalysis.Controls.Add(this._lblStatScanOffset);
      this.tpAnalysis.Controls.Add(this._txtScanOffset_Max);
      this.tpAnalysis.Controls.Add(this._txtScanOffset_Min);
      this.tpAnalysis.Controls.Add(this._lblScanOffset);
      this.tpAnalysis.Controls.Add(this._lblGainFactor);
      this.tpAnalysis.Location = new System.Drawing.Point(4, 22);
      this.tpAnalysis.Name = "tpAnalysis";
      this.tpAnalysis.Size = new System.Drawing.Size(381, 391);
      this.tpAnalysis.TabIndex = 3;
      this.tpAnalysis.Text = "Analysis";
      this.tpAnalysis.UseVisualStyleBackColor = true;
      // 
      // _lblScanOffset_Max
      // 
      this._lblScanOffset_Max.Location = new System.Drawing.Point(297, 12);
      this._lblScanOffset_Max.Name = "_lblScanOffset_Max";
      this._lblScanOffset_Max.Size = new System.Drawing.Size(64, 16);
      this._lblScanOffset_Max.TabIndex = 11;
      this._lblScanOffset_Max.Text = "Max.";
      this._lblScanOffset_Max.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // _lblScanOffset_Min
      // 
      this._lblScanOffset_Min.Location = new System.Drawing.Point(229, 12);
      this._lblScanOffset_Min.Name = "_lblScanOffset_Min";
      this._lblScanOffset_Min.Size = new System.Drawing.Size(64, 16);
      this._lblScanOffset_Min.TabIndex = 10;
      this._lblScanOffset_Min.Text = "Min.";
      this._lblScanOffset_Min.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // _gbPA
      // 
      this._gbPA.Controls.Add(this._cbPAMode);
      this._gbPA.Controls.Add(this._lblPAMode);
      this._gbPA.Controls.Add(this._txtPeakAreaDetNsig);
      this._gbPA.Controls.Add(this._lblPeakAreaDetNsig);
      this._gbPA.Location = new System.Drawing.Point(188, 59);
      this._gbPA.Name = "_gbPA";
      this._gbPA.Size = new System.Drawing.Size(176, 80);
      this._gbPA.TabIndex = 4;
      this._gbPA.TabStop = false;
      this._gbPA.Text = "Peak area determination";
      // 
      // _cbPAMode
      // 
      this._cbPAMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this._cbPAMode.Enabled = false;
      this._cbPAMode.Location = new System.Drawing.Point(64, 52);
      this._cbPAMode.Name = "_cbPAMode";
      this._cbPAMode.Size = new System.Drawing.Size(92, 21);
      this._cbPAMode.TabIndex = 3;
      this._cbPAMode.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblPAMode
      // 
      this._lblPAMode.Location = new System.Drawing.Point(8, 52);
      this._lblPAMode.Name = "_lblPAMode";
      this._lblPAMode.Size = new System.Drawing.Size(48, 23);
      this._lblPAMode.TabIndex = 2;
      this._lblPAMode.Text = "Mode:";
      // 
      // _txtPeakAreaDetNsig
      // 
      this._txtPeakAreaDetNsig.Location = new System.Drawing.Point(108, 24);
      this._txtPeakAreaDetNsig.MaxLength = 2;
      this._txtPeakAreaDetNsig.Name = "_txtPeakAreaDetNsig";
      this._txtPeakAreaDetNsig.Size = new System.Drawing.Size(44, 20);
      this._txtPeakAreaDetNsig.TabIndex = 1;
      this._txtPeakAreaDetNsig.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblPeakAreaDetNsig
      // 
      this._lblPeakAreaDetNsig.Location = new System.Drawing.Point(8, 20);
      this._lblPeakAreaDetNsig.Name = "_lblPeakAreaDetNsig";
      this._lblPeakAreaDetNsig.Size = new System.Drawing.Size(84, 28);
      this._lblPeakAreaDetNsig.TabIndex = 0;
      this._lblPeakAreaDetNsig.Text = "\'n\' sigma factor (>= 0, def. 3):";
      // 
      // _gbPS
      // 
      this._gbPS.Controls.Add(this._cbPSMode);
      this._gbPS.Controls.Add(this._lblPSMode);
      this._gbPS.Controls.Add(this._txtPeakSearchNsig);
      this._gbPS.Controls.Add(this._lblPeakSearchNsig);
      this._gbPS.Location = new System.Drawing.Point(12, 59);
      this._gbPS.Name = "_gbPS";
      this._gbPS.Size = new System.Drawing.Size(170, 80);
      this._gbPS.TabIndex = 3;
      this._gbPS.TabStop = false;
      this._gbPS.Text = "Peak search";
      // 
      // _cbPSMode
      // 
      this._cbPSMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this._cbPSMode.Enabled = false;
      this._cbPSMode.Location = new System.Drawing.Point(64, 52);
      this._cbPSMode.Name = "_cbPSMode";
      this._cbPSMode.Size = new System.Drawing.Size(92, 21);
      this._cbPSMode.TabIndex = 3;
      this._cbPSMode.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblPSMode
      // 
      this._lblPSMode.Location = new System.Drawing.Point(8, 52);
      this._lblPSMode.Name = "_lblPSMode";
      this._lblPSMode.Size = new System.Drawing.Size(48, 23);
      this._lblPSMode.TabIndex = 2;
      this._lblPSMode.Text = "Mode:";
      // 
      // _txtPeakSearchNsig
      // 
      this._txtPeakSearchNsig.Location = new System.Drawing.Point(108, 24);
      this._txtPeakSearchNsig.MaxLength = 3;
      this._txtPeakSearchNsig.Name = "_txtPeakSearchNsig";
      this._txtPeakSearchNsig.Size = new System.Drawing.Size(44, 20);
      this._txtPeakSearchNsig.TabIndex = 1;
      this._txtPeakSearchNsig.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblPeakSearchNsig
      // 
      this._lblPeakSearchNsig.Location = new System.Drawing.Point(8, 20);
      this._lblPeakSearchNsig.Name = "_lblPeakSearchNsig";
      this._lblPeakSearchNsig.Size = new System.Drawing.Size(84, 28);
      this._lblPeakSearchNsig.TabIndex = 0;
      this._lblPeakSearchNsig.Text = "\'n\' sigma factor (>= 0, def. 6):";
      // 
      // _gbSmoothing
      // 
      this._gbSmoothing.Controls.Add(this._txtSmoothing_SG_width);
      this._gbSmoothing.Controls.Add(this._lblSmoothing_SG_width);
      this._gbSmoothing.Controls.Add(this._txtSmoothing_MA_width);
      this._gbSmoothing.Controls.Add(this._lblSmoothing_MA_width);
      this._gbSmoothing.Controls.Add(this._rbSmoothing_SG);
      this._gbSmoothing.Controls.Add(this._rbSmoothing_MA);
      this._gbSmoothing.Controls.Add(this._lblSmoothing_Type);
      this._gbSmoothing.Location = new System.Drawing.Point(12, 165);
      this._gbSmoothing.Name = "_gbSmoothing";
      this._gbSmoothing.Size = new System.Drawing.Size(352, 86);
      this._gbSmoothing.TabIndex = 7;
      this._gbSmoothing.TabStop = false;
      this._gbSmoothing.Text = "Smoothing";
      // 
      // _txtSmoothing_SG_width
      // 
      this._txtSmoothing_SG_width.Location = new System.Drawing.Point(298, 64);
      this._txtSmoothing_SG_width.MaxLength = 3;
      this._txtSmoothing_SG_width.Name = "_txtSmoothing_SG_width";
      this._txtSmoothing_SG_width.Size = new System.Drawing.Size(46, 20);
      this._txtSmoothing_SG_width.TabIndex = 6;
      this._txtSmoothing_SG_width.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblSmoothing_SG_width
      // 
      this._lblSmoothing_SG_width.Location = new System.Drawing.Point(8, 64);
      this._lblSmoothing_SG_width.Name = "_lblSmoothing_SG_width";
      this._lblSmoothing_SG_width.Size = new System.Drawing.Size(286, 18);
      this._lblSmoothing_SG_width.TabIndex = 5;
      this._lblSmoothing_SG_width.Text = "SG: Filter width (in [3,5,...,41]):";
      // 
      // _txtSmoothing_MA_width
      // 
      this._txtSmoothing_MA_width.Location = new System.Drawing.Point(298, 42);
      this._txtSmoothing_MA_width.MaxLength = 3;
      this._txtSmoothing_MA_width.Name = "_txtSmoothing_MA_width";
      this._txtSmoothing_MA_width.Size = new System.Drawing.Size(46, 20);
      this._txtSmoothing_MA_width.TabIndex = 4;
      this._txtSmoothing_MA_width.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblSmoothing_MA_width
      // 
      this._lblSmoothing_MA_width.Location = new System.Drawing.Point(8, 42);
      this._lblSmoothing_MA_width.Name = "_lblSmoothing_MA_width";
      this._lblSmoothing_MA_width.Size = new System.Drawing.Size(286, 18);
      this._lblSmoothing_MA_width.TabIndex = 3;
      this._lblSmoothing_MA_width.Text = "MA: Filter width (in [3,5,...,11]):";
      // 
      // _rbSmoothing_SG
      // 
      this._rbSmoothing_SG.Location = new System.Drawing.Point(224, 20);
      this._rbSmoothing_SG.Name = "_rbSmoothing_SG";
      this._rbSmoothing_SG.Size = new System.Drawing.Size(120, 18);
      this._rbSmoothing_SG.TabIndex = 2;
      this._rbSmoothing_SG.Text = "Savitzky/Golay";
      this._rbSmoothing_SG.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _rbSmoothing_MA
      // 
      this._rbSmoothing_MA.Location = new System.Drawing.Point(100, 20);
      this._rbSmoothing_MA.Name = "_rbSmoothing_MA";
      this._rbSmoothing_MA.Size = new System.Drawing.Size(120, 18);
      this._rbSmoothing_MA.TabIndex = 1;
      this._rbSmoothing_MA.Text = "Moving Average";
      this._rbSmoothing_MA.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblSmoothing_Type
      // 
      this._lblSmoothing_Type.Location = new System.Drawing.Point(8, 20);
      this._lblSmoothing_Type.Name = "_lblSmoothing_Type";
      this._lblSmoothing_Type.Size = new System.Drawing.Size(88, 18);
      this._lblSmoothing_Type.TabIndex = 0;
      this._lblSmoothing_Type.Text = "Filter type:";
      this._lblSmoothing_Type.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // _txtStatScanOffset
      // 
      this._txtStatScanOffset.Location = new System.Drawing.Point(298, 255);
      this._txtStatScanOffset.MaxLength = 2;
      this._txtStatScanOffset.Name = "_txtStatScanOffset";
      this._txtStatScanOffset.ReadOnly = true;
      this._txtStatScanOffset.Size = new System.Drawing.Size(64, 20);
      this._txtStatScanOffset.TabIndex = 9;
      this._txtStatScanOffset.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblStatScanOffset
      // 
      this._lblStatScanOffset.Location = new System.Drawing.Point(12, 255);
      this._lblStatScanOffset.Name = "_lblStatScanOffset";
      this._lblStatScanOffset.Size = new System.Drawing.Size(282, 18);
      this._lblStatScanOffset.TabIndex = 8;
      this._lblStatScanOffset.Text = "Statischer Scanoffset ( adc):";
      // 
      // _txtScanOffset_Max
      // 
      this._txtScanOffset_Max.Location = new System.Drawing.Point(298, 35);
      this._txtScanOffset_Max.MaxLength = 5;
      this._txtScanOffset_Max.Name = "_txtScanOffset_Max";
      this._txtScanOffset_Max.Size = new System.Drawing.Size(64, 20);
      this._txtScanOffset_Max.TabIndex = 2;
      this._txtScanOffset_Max.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtScanOffset_Min
      // 
      this._txtScanOffset_Min.Location = new System.Drawing.Point(230, 35);
      this._txtScanOffset_Min.MaxLength = 5;
      this._txtScanOffset_Min.Name = "_txtScanOffset_Min";
      this._txtScanOffset_Min.Size = new System.Drawing.Size(64, 20);
      this._txtScanOffset_Min.TabIndex = 1;
      this._txtScanOffset_Min.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblScanOffset
      // 
      this._lblScanOffset.Location = new System.Drawing.Point(12, 35);
      this._lblScanOffset.Name = "_lblScanOffset";
      this._lblScanOffset.Size = new System.Drawing.Size(214, 20);
      this._lblScanOffset.TabIndex = 0;
      this._lblScanOffset.Text = "Scan offset (in [0, 32767]):";
      // 
      // _lblGainFactor
      // 
      this._lblGainFactor.Location = new System.Drawing.Point(12, 143);
      this._lblGainFactor.Name = "_lblGainFactor";
      this._lblGainFactor.Size = new System.Drawing.Size(280, 18);
      this._lblGainFactor.TabIndex = 5;
      this._lblGainFactor.Text = "Gainfaktor:";
      // 
      // _gbLoad
      // 
      this._gbLoad.Controls.Add(this._txtCurSvcFile);
      this._gbLoad.Controls.Add(this._lblCurSvcFile);
      this._gbLoad.Controls.Add(this._btnLoad);
      this._gbLoad.Controls.Add(this._lbSvcFiles);
      this._gbLoad.Location = new System.Drawing.Point(410, 184);
      this._gbLoad.Name = "_gbLoad";
      this._gbLoad.Size = new System.Drawing.Size(368, 104);
      this._gbLoad.TabIndex = 3;
      this._gbLoad.TabStop = false;
      this._gbLoad.Text = "Load service file";
      // 
      // _txtCurSvcFile
      // 
      this._txtCurSvcFile.Location = new System.Drawing.Point(114, 78);
      this._txtCurSvcFile.Name = "_txtCurSvcFile";
      this._txtCurSvcFile.ReadOnly = true;
      this._txtCurSvcFile.Size = new System.Drawing.Size(246, 20);
      this._txtCurSvcFile.TabIndex = 3;
      this._txtCurSvcFile.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblCurSvcFile
      // 
      this._lblCurSvcFile.Location = new System.Drawing.Point(10, 80);
      this._lblCurSvcFile.Name = "_lblCurSvcFile";
      this._lblCurSvcFile.Size = new System.Drawing.Size(100, 18);
      this._lblCurSvcFile.TabIndex = 2;
      this._lblCurSvcFile.Text = "Geladene Datei:";
      // 
      // _btnLoad
      // 
      this._btnLoad.Location = new System.Drawing.Point(276, 32);
      this._btnLoad.Name = "_btnLoad";
      this._btnLoad.Size = new System.Drawing.Size(84, 23);
      this._btnLoad.TabIndex = 1;
      this._btnLoad.Text = "Load";
      this._btnLoad.Click += new System.EventHandler(this._btnLoad_Click);
      this._btnLoad.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lbSvcFiles
      // 
      this._lbSvcFiles.Location = new System.Drawing.Point(8, 20);
      this._lbSvcFiles.Name = "_lbSvcFiles";
      this._lbSvcFiles.Size = new System.Drawing.Size(260, 56);
      this._lbSvcFiles.Sorted = true;
      this._lbSvcFiles.TabIndex = 0;
      this._lbSvcFiles.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _gbError
      // 
      this._gbError.Controls.Add(this._txtError);
      this._gbError.Location = new System.Drawing.Point(410, 112);
      this._gbError.Name = "_gbError";
      this._gbError.Size = new System.Drawing.Size(368, 68);
      this._gbError.TabIndex = 2;
      this._gbError.TabStop = false;
      this._gbError.Text = "Error display";
      // 
      // _txtError
      // 
      this._txtError.Location = new System.Drawing.Point(8, 20);
      this._txtError.Multiline = true;
      this._txtError.Name = "_txtError";
      this._txtError.ReadOnly = true;
      this._txtError.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this._txtError.Size = new System.Drawing.Size(352, 44);
      this._txtError.TabIndex = 0;
      this._txtError.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _gbComment
      // 
      this._gbComment.Controls.Add(this._txtAvCmtSize);
      this._gbComment.Controls.Add(this._lblAvCmtSize);
      this._gbComment.Controls.Add(this._txtComment);
      this._gbComment.Location = new System.Drawing.Point(410, 26);
      this._gbComment.Name = "_gbComment";
      this._gbComment.Size = new System.Drawing.Size(368, 82);
      this._gbComment.TabIndex = 1;
      this._gbComment.TabStop = false;
      this._gbComment.Text = "Comment";
      // 
      // _txtAvCmtSize
      // 
      this._txtAvCmtSize.Location = new System.Drawing.Point(154, 55);
      this._txtAvCmtSize.Name = "_txtAvCmtSize";
      this._txtAvCmtSize.ReadOnly = true;
      this._txtAvCmtSize.Size = new System.Drawing.Size(68, 20);
      this._txtAvCmtSize.TabIndex = 2;
      this._txtAvCmtSize.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblAvCmtSize
      // 
      this._lblAvCmtSize.Location = new System.Drawing.Point(8, 55);
      this._lblAvCmtSize.Name = "_lblAvCmtSize";
      this._lblAvCmtSize.Size = new System.Drawing.Size(142, 18);
      this._lblAvCmtSize.TabIndex = 1;
      this._lblAvCmtSize.Text = "Available size (B):";
      // 
      // _txtComment
      // 
      this._txtComment.Location = new System.Drawing.Point(8, 17);
      this._txtComment.Multiline = true;
      this._txtComment.Name = "_txtComment";
      this._txtComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this._txtComment.Size = new System.Drawing.Size(352, 34);
      this._txtComment.TabIndex = 0;
      this._txtComment.TextChanged += new System.EventHandler(this._txtComment_TextChanged);
      this._txtComment.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnExtended
      // 
      this._btnExtended.Location = new System.Drawing.Point(625, 471);
      this._btnExtended.Name = "_btnExtended";
      this._btnExtended.Size = new System.Drawing.Size(96, 23);
      this._btnExtended.TabIndex = 9;
      this._btnExtended.Text = "Extended ...";
      this._btnExtended.Click += new System.EventHandler(this._btnExtended_Click);
      this._btnExtended.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _chkPeriodicUpdate
      // 
      this._chkPeriodicUpdate.Location = new System.Drawing.Point(69, 471);
      this._chkPeriodicUpdate.Name = "_chkPeriodicUpdate";
      this._chkPeriodicUpdate.Size = new System.Drawing.Size(160, 24);
      this._chkPeriodicUpdate.TabIndex = 5;
      this._chkPeriodicUpdate.Text = "Periodische Aktualisierung";
      this._chkPeriodicUpdate.CheckedChanged += new System.EventHandler(this._chkPeriodicUpdate_CheckedChanged);
      this._chkPeriodicUpdate.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _pgbTransfer
      // 
      this._pgbTransfer.Location = new System.Drawing.Point(234, 443);
      this._pgbTransfer.Name = "_pgbTransfer";
      this._pgbTransfer.Size = new System.Drawing.Size(327, 20);
      this._pgbTransfer.TabIndex = 4;
      // 
      // _btnCancel
      // 
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point(466, 471);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.Size = new System.Drawing.Size(96, 23);
      this._btnCancel.TabIndex = 8;
      this._btnCancel.Text = "Cancel";
      // 
      // _btnWrite
      // 
      this._btnWrite.Location = new System.Drawing.Point(350, 471);
      this._btnWrite.Name = "_btnWrite";
      this._btnWrite.Size = new System.Drawing.Size(96, 23);
      this._btnWrite.TabIndex = 7;
      this._btnWrite.Text = "Daten schreiben";
      this._btnWrite.Click += new System.EventHandler(this._btnWrite_Click);
      this._btnWrite.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnRead
      // 
      this._btnRead.Location = new System.Drawing.Point(234, 471);
      this._btnRead.Name = "_btnRead";
      this._btnRead.Size = new System.Drawing.Size(96, 23);
      this._btnRead.TabIndex = 6;
      this._btnRead.Text = "Read Data";
      this._btnRead.Click += new System.EventHandler(this._btnRead_Click);
      this._btnRead.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _Timer
      // 
      this._Timer.Interval = 1000;
      this._Timer.Tick += new System.EventHandler(this._Timer_Tick);
      // 
      // _TimerPeriodicUpdate
      // 
      this._TimerPeriodicUpdate.Interval = 5000;
      this._TimerPeriodicUpdate.Tick += new System.EventHandler(this._TimerPeriodicUpdate_Tick);
      // 
      // _ToolTip
      // 
      this._ToolTip.AutoPopDelay = 10000;
      this._ToolTip.InitialDelay = 500;
      this._ToolTip.ReshowDelay = 100;
      // 
      // _cbGainFactor
      // 
      this._cbGainFactor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this._cbGainFactor.FormattingEnabled = true;
      this._cbGainFactor.Location = new System.Drawing.Point(298, 143);
      this._cbGainFactor.Name = "_cbGainFactor";
      this._cbGainFactor.Size = new System.Drawing.Size(64, 21);
      this._cbGainFactor.TabIndex = 6;
      this._cbGainFactor.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // ServieNewForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(790, 505);
      this.Controls.Add(this._btnExtended);
      this.Controls.Add(this._chkPeriodicUpdate);
      this.Controls.Add(this._pgbTransfer);
      this.Controls.Add(this._btnCancel);
      this.Controls.Add(this._btnWrite);
      this.Controls.Add(this._btnRead);
      this.Controls.Add(this._gbLoad);
      this.Controls.Add(this._gbError);
      this.Controls.Add(this._gbComment);
      this.Controls.Add(this.tabControl1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.HelpButton = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ServieNewForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Service Control";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ServieNewForm_FormClosed);
      this.Load += new System.EventHandler(this.ServiceForm_Load);
      this.tabControl1.ResumeLayout(false);
      this.tpDevice.ResumeLayout(false);
      this.tpDevice.PerformLayout();
      this._gbPumpOnTimes.ResumeLayout(false);
      this._gbPumpOnTimes.PerformLayout();
      this._gbLogo.ResumeLayout(false);
      this.tpDateTime.ResumeLayout(false);
      this.tpSensor.ResumeLayout(false);
      this.tpSensor.PerformLayout();
      this._gbSensor_PF.ResumeLayout(false);
      this._gbSensor_PF.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this._nudP3)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._nudP2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._nudP1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._nudT4_Soll)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._nudT3_Soll)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._nudT2_Soll)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._nudT1_Soll)).EndInit();
      this.tpAnalysis.ResumeLayout(false);
      this.tpAnalysis.PerformLayout();
      this._gbPA.ResumeLayout(false);
      this._gbPA.PerformLayout();
      this._gbPS.ResumeLayout(false);
      this._gbPS.PerformLayout();
      this._gbSmoothing.ResumeLayout(false);
      this._gbSmoothing.PerformLayout();
      this._gbLoad.ResumeLayout(false);
      this._gbLoad.PerformLayout();
      this._gbError.ResumeLayout(false);
      this._gbError.PerformLayout();
      this._gbComment.ResumeLayout(false);
      this._gbComment.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tpDevice;
    private System.Windows.Forms.TabPage tpDateTime;
    private System.Windows.Forms.TabPage tpSensor;
    private System.Windows.Forms.TabPage tpAnalysis;
    private System.Windows.Forms.CheckBox _chkDontChangePIDlamp;
    private System.Windows.Forms.GroupBox _gbPumpOnTimes;
    private System.Windows.Forms.CheckBox _chkDontChangeP3On;
    private System.Windows.Forms.CheckBox _chkDontChangeP2On;
    private System.Windows.Forms.CheckBox _chkDontChangeP1On;
    private System.Windows.Forms.CheckBox _chkResetP3On;
    private System.Windows.Forms.CheckBox _chkResetP2On;
    private System.Windows.Forms.CheckBox _chkResetP1On;
    private System.Windows.Forms.TextBox _txtP3On;
    private System.Windows.Forms.Label _lblP3On;
    private System.Windows.Forms.TextBox _txtP2On;
    private System.Windows.Forms.Label _lblP2On;
    private System.Windows.Forms.TextBox _txtP1On;
    private System.Windows.Forms.Label _lblP1On;
    private System.Windows.Forms.GroupBox _gbLogo;
    private System.Windows.Forms.RadioButton _rbLogoENVI;
    private System.Windows.Forms.RadioButton _rbLogoENIT;
    private System.Windows.Forms.RadioButton _rbLogoIUT;
    private System.Windows.Forms.CheckBox _chkAccuDisplay;
    private System.Windows.Forms.CheckBox _chkResetPIDlamp;
    private System.Windows.Forms.TextBox _txtPIDlamp;
    private System.Windows.Forms.Label _lblPIDlamp;
    private System.Windows.Forms.TextBox _txtCellNo;
    private System.Windows.Forms.TextBox _txtDeviceNo;
    private System.Windows.Forms.Label _lblCellNo;
    private System.Windows.Forms.Label _lblDeviceNo;
    private System.Windows.Forms.TextBox _txtPW;
    private System.Windows.Forms.Label _lblPW;
    private System.Windows.Forms.CheckBox _chkPWOnOff;
    private System.Windows.Forms.CheckBox _chkErrConf;
    private System.Windows.Forms.TextBox _txtSelfCheck;
    private System.Windows.Forms.Label _lblSelfCheck;
    private System.Windows.Forms.DateTimePicker _dtpSvcDate;
    private System.Windows.Forms.Label _lblSvcDate;
    private System.Windows.Forms.DateTimePicker _dtpTime;
    private System.Windows.Forms.DateTimePicker _dtpDate;
    private System.Windows.Forms.Label _lblTime;
    private System.Windows.Forms.Label _lblDate;
    private System.Windows.Forms.TextBox _txtMFCcap;
    private System.Windows.Forms.Label _lblMFCcap;
    private System.Windows.Forms.Label _lblpm_3;
    private System.Windows.Forms.TextBox _txtF3_Soll_TolAbs;
    private System.Windows.Forms.TextBox _txtF3_Soll;
    private System.Windows.Forms.Label _lblF3;
    private System.Windows.Forms.TextBox _txtF3_Ist;
    private System.Windows.Forms.Label _lblTolAbs;
    private System.Windows.Forms.Label _lblSoll_1;
    private System.Windows.Forms.Label _lblIst_1;
    private System.Windows.Forms.GroupBox _gbSensor_PF;
    private System.Windows.Forms.Label _lblPump;
    private System.Windows.Forms.Label _lblFlow;
    private System.Windows.Forms.Label _lblPFHandling;
    private System.Windows.Forms.CheckBox _chkPF3;
    private System.Windows.Forms.CheckBox _chkPF2;
    private System.Windows.Forms.CheckBox _chkPF1;
    private System.Windows.Forms.NumericUpDown _nudP3;
    private System.Windows.Forms.NumericUpDown _nudP2;
    private System.Windows.Forms.NumericUpDown _nudP1;
    private System.Windows.Forms.Label _lblP3;
    private System.Windows.Forms.Label _lblP2;
    private System.Windows.Forms.Label _lblP1;
    private System.Windows.Forms.TextBox _txtF3;
    private System.Windows.Forms.TextBox _txtF2;
    private System.Windows.Forms.TextBox _txtF1;
    private System.Windows.Forms.TextBox _txtKP4;
    private System.Windows.Forms.TextBox _txtKP3;
    private System.Windows.Forms.TextBox _txtKP2;
    private System.Windows.Forms.TextBox _txtKP1;
    private System.Windows.Forms.Label _lblKP;
    private System.Windows.Forms.Label _lblpm_1;
    private System.Windows.Forms.Label _lblpm;
    private System.Windows.Forms.TextBox _txtF2_Soll_TolAbs;
    private System.Windows.Forms.TextBox _txtF1_Soll_TolAbs;
    private System.Windows.Forms.TextBox _txtF2_Soll;
    private System.Windows.Forms.TextBox _txtF1_Soll;
    private System.Windows.Forms.TextBox _txtF2_Ist;
    private System.Windows.Forms.TextBox _txtF1_Ist;
    private System.Windows.Forms.CheckBox _chkPresOff;
    private System.Windows.Forms.Label _lblF2;
    private System.Windows.Forms.Label _lblF1;
    private System.Windows.Forms.CheckBox _chkT4Off;
    private System.Windows.Forms.CheckBox _chkT3Off;
    private System.Windows.Forms.CheckBox _chkT2Off;
    private System.Windows.Forms.CheckBox _chkT1Off;
    private System.Windows.Forms.NumericUpDown _nudT4_Soll;
    private System.Windows.Forms.NumericUpDown _nudT3_Soll;
    private System.Windows.Forms.NumericUpDown _nudT2_Soll;
    private System.Windows.Forms.NumericUpDown _nudT1_Soll;
    private System.Windows.Forms.TextBox _txtPres;
    private System.Windows.Forms.TextBox _txtT4_Ist;
    private System.Windows.Forms.TextBox _txtT2_Ist;
    private System.Windows.Forms.TextBox _txtT3_Ist;
    private System.Windows.Forms.TextBox _txtT1_Ist;
    private System.Windows.Forms.Label _lblSoll;
    private System.Windows.Forms.Label _lblIst;
    private System.Windows.Forms.Label _lblPres;
    private System.Windows.Forms.Label _lblT4;
    private System.Windows.Forms.Label _lblT3;
    private System.Windows.Forms.Label _lblT2;
    private System.Windows.Forms.Label _lblT1;
    private System.Windows.Forms.GroupBox _gbPA;
    private System.Windows.Forms.ComboBox _cbPAMode;
    private System.Windows.Forms.Label _lblPAMode;
    private System.Windows.Forms.TextBox _txtPeakAreaDetNsig;
    private System.Windows.Forms.Label _lblPeakAreaDetNsig;
    private System.Windows.Forms.GroupBox _gbPS;
    private System.Windows.Forms.ComboBox _cbPSMode;
    private System.Windows.Forms.Label _lblPSMode;
    private System.Windows.Forms.TextBox _txtPeakSearchNsig;
    private System.Windows.Forms.Label _lblPeakSearchNsig;
    private System.Windows.Forms.GroupBox _gbSmoothing;
    private System.Windows.Forms.TextBox _txtSmoothing_SG_width;
    private System.Windows.Forms.Label _lblSmoothing_SG_width;
    private System.Windows.Forms.TextBox _txtSmoothing_MA_width;
    private System.Windows.Forms.Label _lblSmoothing_MA_width;
    private System.Windows.Forms.RadioButton _rbSmoothing_SG;
    private System.Windows.Forms.RadioButton _rbSmoothing_MA;
    private System.Windows.Forms.Label _lblSmoothing_Type;
    private System.Windows.Forms.TextBox _txtStatScanOffset;
    private System.Windows.Forms.Label _lblStatScanOffset;
    private System.Windows.Forms.TextBox _txtScanOffset_Max;
    private System.Windows.Forms.TextBox _txtScanOffset_Min;
    private System.Windows.Forms.Label _lblScanOffset;
    private System.Windows.Forms.Label _lblGainFactor;
    private System.Windows.Forms.GroupBox _gbLoad;
    private System.Windows.Forms.TextBox _txtCurSvcFile;
    private System.Windows.Forms.Label _lblCurSvcFile;
    private System.Windows.Forms.Button _btnLoad;
    private System.Windows.Forms.ListBox _lbSvcFiles;
    private System.Windows.Forms.GroupBox _gbError;
    private System.Windows.Forms.TextBox _txtError;
    private System.Windows.Forms.GroupBox _gbComment;
    private System.Windows.Forms.TextBox _txtAvCmtSize;
    private System.Windows.Forms.Label _lblAvCmtSize;
    private System.Windows.Forms.TextBox _txtComment;
    private System.Windows.Forms.Button _btnExtended;
    private System.Windows.Forms.CheckBox _chkPeriodicUpdate;
    private System.Windows.Forms.ProgressBar _pgbTransfer;
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.Button _btnWrite;
    private System.Windows.Forms.Button _btnRead;
    private System.Windows.Forms.Timer _Timer;
    private System.Windows.Forms.Timer _TimerPeriodicUpdate;
    private System.Windows.Forms.ToolTip _ToolTip;
    private System.Windows.Forms.Label _lblScanOffset_Max;
    private System.Windows.Forms.Label _lblScanOffset_Min;
    private System.Windows.Forms.ComboBox _cbGainFactor;
  }
}