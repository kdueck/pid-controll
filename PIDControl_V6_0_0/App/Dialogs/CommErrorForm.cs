using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using CommRS232;

namespace App
{
	/// <summary>
	/// Class CommErrorForm:
	/// Communication error dialog
	/// </summary>
	public class CommErrorForm : System.Windows.Forms.Form
	{
    private System.Windows.Forms.TextBox _txtCommError;
    private System.Windows.Forms.Button _btnOK;
    private System.Windows.Forms.PictureBox _PictureBox;
		
    /// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="fOwner">The owner window</param>
    /// <param name="msg">The communication message</param>
    public CommErrorForm (Form fOwner, CommMessage msg)
    {
      InitializeComponent();
      // Init.
      _fOwner = fOwner;
      _msg = msg;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="fOwner">The owner window</param>
    public CommErrorForm (Form fOwner)
    {
      InitializeComponent();
      // Init.
      _fOwner = fOwner;
      _msg = null;
    }
    
    /// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this._PictureBox = new System.Windows.Forms.PictureBox();
      this._txtCommError = new System.Windows.Forms.TextBox();
      this._btnOK = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // _PictureBox
      // 
      this._PictureBox.Location = new System.Drawing.Point(8, 8);
      this._PictureBox.Name = "_PictureBox";
      this._PictureBox.Size = new System.Drawing.Size(32, 32);
      this._PictureBox.TabIndex = 0;
      this._PictureBox.TabStop = false;
      this._PictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this._PictureBox_Paint);
      // 
      // _txtCommError
      // 
      this._txtCommError.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtCommError.Location = new System.Drawing.Point(48, 8);
      this._txtCommError.Multiline = true;
      this._txtCommError.Name = "_txtCommError";
      this._txtCommError.ReadOnly = true;
      this._txtCommError.Size = new System.Drawing.Size(312, 100);
      this._txtCommError.TabIndex = 1;
      this._txtCommError.Text = "";
      // 
      // _btnOK
      // 
      this._btnOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnOK.Location = new System.Drawing.Point(148, 116);
      this._btnOK.Name = "_btnOK";
      this._btnOK.TabIndex = 0;
      this._btnOK.Text = "OK";
      // 
      // CommErrorForm
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.ClientSize = new System.Drawing.Size(366, 148);
      this.Controls.Add(this._btnOK);
      this.Controls.Add(this._txtCommError);
      this.Controls.Add(this._PictureBox);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "CommErrorForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "RS232 connection";
      this.TopMost = true;
      this.Load += new System.EventHandler(this.CommErrorForm_Load);
      this.ResumeLayout(false);

    }

		#endregion
	
    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void CommErrorForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Message text
      string sMsg;
      switch (_msg.ResponseID)
      {
        case AppComm.CMID_STRLENERR:        // String length error
          // Error message:
          //    "Der String\r\n\r\n'{0}'\r\n\r\nist zu lang f�r eine �bertragung.
          //     Bitte beheben Sie den Fehler und starten Sie die �bertragung neu."
          string sFmt = r.GetString ("CommError_Err_StrLen");
          sMsg = string.Format (sFmt, _msg.ToString ());
          break;
        default:                            // all others
          // Def. error message:
          //    "Bei der seriellen Kommunikation ist ein Fehler aufgetreten.
          //     Bitte starten Sie die �bertragung neu."
          sMsg = r.GetString ("CommError_Err_Def");
          break;
      }

      // Resources
      this.Text = r.GetString ( "CommError_Title" );         // "RS232-Verbindung"
      this._btnOK.Text = r.GetString ( "CommError_btnOK" );  // "OK"
      this._txtCommError.Text = sMsg;

      // Icon: Stop
      _icon = SystemIcons.Hand;
      this._PictureBox.Invalidate ();

      // Center
      Common.CenterWindow (this, _fOwner);
    }

    /// <summary>
    /// 'Paint' event of the _PictureBox control
    /// </summary>
    private void _PictureBox_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
    {
      e.Graphics.DrawIcon ( _icon, this._PictureBox.ClientRectangle );
    }
 
    #endregion event handling

    #region private members

    /// <summary>
    /// The communication message
    /// </summary>
    CommMessage _msg;
    /// <summary>
    /// The dialog icon
    /// </summary>
    Icon _icon = null; 
    /// <summary>
    ///  The owner form
    /// </summary>
    Form _fOwner = null;

    #endregion // private members

    #region properties
    #endregion  // properties

  }
}
