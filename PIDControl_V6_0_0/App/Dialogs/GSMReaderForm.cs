// If defined, the 'Clear' action is processed in the devices 'Script transfer' mode.
// If not defined, the devices 'Script transfer' mode will not be used.
#define GSMREADER_CLEAR_WITH_INTERRUPTION

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Text;

using CommRS232;

namespace App
{
	/// <summary>
	/// Class GSMReaderForm:
	/// GSMReader implementation for Eeprom storage (for IUT-built devices) 
	/// </summary>
	/// <remarks>
  /// 1. Conc'ing the clearing of the EEprom result region:
  ///    Before a new measurement should be started, the EEprom result region should be cleared.
	/// </remarks>
  public class GSMReaderForm : System.Windows.Forms.Form
  {
    private System.Windows.Forms.Button _btnEeprom_Data;
    private System.Windows.Forms.ProgressBar _pgbTransfer;
    private System.Windows.Forms.Button _btnEeprom_ScriptInfo;
    private System.Windows.Forms.ToolTip _ToolTip;
    private System.Windows.Forms.Button _btnEeprom_Data_Clear;
    private System.Windows.Forms.Button _btnFile_EE_Data;
    private System.Windows.Forms.TextBox _txtFile_EE_Data;
    private System.Windows.Forms.TextBox _txtFile_EE_ScriptInfo;
    private System.Windows.Forms.Button _btnFile_EE_ScriptInfo;
 
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Constructor
    /// </summary>
    public GSMReaderForm()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(GSMReaderForm));
      this._btnEeprom_Data = new System.Windows.Forms.Button();
      this._pgbTransfer = new System.Windows.Forms.ProgressBar();
      this._btnFile_EE_Data = new System.Windows.Forms.Button();
      this._txtFile_EE_Data = new System.Windows.Forms.TextBox();
      this._txtFile_EE_ScriptInfo = new System.Windows.Forms.TextBox();
      this._btnFile_EE_ScriptInfo = new System.Windows.Forms.Button();
      this._btnEeprom_Data_Clear = new System.Windows.Forms.Button();
      this._btnEeprom_ScriptInfo = new System.Windows.Forms.Button();
      this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
      this.SuspendLayout();
      // 
      // _btnEeprom_Data
      // 
      this._btnEeprom_Data.Location = new System.Drawing.Point(8, 8);
      this._btnEeprom_Data.Name = "_btnEeprom_Data";
      this._btnEeprom_Data.Size = new System.Drawing.Size(124, 23);
      this._btnEeprom_Data.TabIndex = 0;
      this._btnEeprom_Data.Text = "Transfer results";
      this._btnEeprom_Data.Click += new System.EventHandler(this._btnEeprom_Data_Click);
      this._btnEeprom_Data.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _pgbTransfer
      // 
      this._pgbTransfer.Location = new System.Drawing.Point(8, 118);
      this._pgbTransfer.Name = "_pgbTransfer";
      this._pgbTransfer.Size = new System.Drawing.Size(444, 20);
      this._pgbTransfer.TabIndex = 7;
      // 
      // _btnFile_EE_Data
      // 
      this._btnFile_EE_Data.Image = ((System.Drawing.Image)(resources.GetObject("_btnFile_EE_Data.Image")));
      this._btnFile_EE_Data.Location = new System.Drawing.Point(428, 8);
      this._btnFile_EE_Data.Name = "_btnFile_EE_Data";
      this._btnFile_EE_Data.Size = new System.Drawing.Size(22, 23);
      this._btnFile_EE_Data.TabIndex = 2;
      this._btnFile_EE_Data.Click += new System.EventHandler(this._btnFile_Click);
      this._btnFile_EE_Data.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtFile_EE_Data
      // 
      this._txtFile_EE_Data.Location = new System.Drawing.Point(146, 12);
      this._txtFile_EE_Data.Name = "_txtFile_EE_Data";
      this._txtFile_EE_Data.ReadOnly = true;
      this._txtFile_EE_Data.Size = new System.Drawing.Size(280, 20);
      this._txtFile_EE_Data.TabIndex = 1;
      this._txtFile_EE_Data.Text = "";
      // 
      // _txtFile_EE_ScriptInfo
      // 
      this._txtFile_EE_ScriptInfo.Location = new System.Drawing.Point(146, 84);
      this._txtFile_EE_ScriptInfo.Name = "_txtFile_EE_ScriptInfo";
      this._txtFile_EE_ScriptInfo.ReadOnly = true;
      this._txtFile_EE_ScriptInfo.Size = new System.Drawing.Size(280, 20);
      this._txtFile_EE_ScriptInfo.TabIndex = 5;
      this._txtFile_EE_ScriptInfo.Text = "";
      // 
      // _btnFile_EE_ScriptInfo
      // 
      this._btnFile_EE_ScriptInfo.Image = ((System.Drawing.Image)(resources.GetObject("_btnFile_EE_ScriptInfo.Image")));
      this._btnFile_EE_ScriptInfo.Location = new System.Drawing.Point(428, 80);
      this._btnFile_EE_ScriptInfo.Name = "_btnFile_EE_ScriptInfo";
      this._btnFile_EE_ScriptInfo.Size = new System.Drawing.Size(22, 23);
      this._btnFile_EE_ScriptInfo.TabIndex = 6;
      this._btnFile_EE_ScriptInfo.Click += new System.EventHandler(this._btnFile_Click);
      this._btnFile_EE_ScriptInfo.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnEeprom_Data_Clear
      // 
      this._btnEeprom_Data_Clear.Location = new System.Drawing.Point(8, 44);
      this._btnEeprom_Data_Clear.Name = "_btnEeprom_Data_Clear";
      this._btnEeprom_Data_Clear.Size = new System.Drawing.Size(124, 23);
      this._btnEeprom_Data_Clear.TabIndex = 3;
      this._btnEeprom_Data_Clear.Text = "Clear results";
      this._btnEeprom_Data_Clear.Click += new System.EventHandler(this._btnEeprom_Data_Clear_Click);
      this._btnEeprom_Data_Clear.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnEeprom_ScriptInfo
      // 
      this._btnEeprom_ScriptInfo.Location = new System.Drawing.Point(8, 80);
      this._btnEeprom_ScriptInfo.Name = "_btnEeprom_ScriptInfo";
      this._btnEeprom_ScriptInfo.Size = new System.Drawing.Size(124, 23);
      this._btnEeprom_ScriptInfo.TabIndex = 4;
      this._btnEeprom_ScriptInfo.Text = "Transfer ScriptInfo";
      this._btnEeprom_ScriptInfo.Click += new System.EventHandler(this._btnEeprom_ScriptInfo_Click);
      this._btnEeprom_ScriptInfo.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _ToolTip
      // 
      this._ToolTip.AutoPopDelay = 10000;
      this._ToolTip.InitialDelay = 500;
      this._ToolTip.ReshowDelay = 100;
      // 
      // GSMReaderForm
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.ClientSize = new System.Drawing.Size(460, 150);
      this.Controls.Add(this._pgbTransfer);
      this.Controls.Add(this._btnFile_EE_Data);
      this.Controls.Add(this._btnEeprom_Data_Clear);
      this.Controls.Add(this._btnFile_EE_ScriptInfo);
      this.Controls.Add(this._btnEeprom_Data);
      this.Controls.Add(this._txtFile_EE_ScriptInfo);
      this.Controls.Add(this._txtFile_EE_Data);
      this.Controls.Add(this._btnEeprom_ScriptInfo);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.HelpButton = true;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "GSMReaderForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Read Eeprom";
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GSMReaderForm_KeyPress);
      this.Load += new System.EventHandler(this.GSMReaderForm_Load);
      this.Closed += new System.EventHandler(this.GSMReaderForm_Closed);
      this.ResumeLayout(false);

    }

    #endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void GSMReaderForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Resources
      this.Text                       = r.GetString ( "GSMReader_Title" );                // "Lesen/L�schen des Eeproms"

      this._btnEeprom_Data.Text       = r.GetString ( "GSMReader_btnEeprom_Data" );       // "Resultate �bertragen"
      this._btnEeprom_Data_Clear.Text = r.GetString ( "GSMReader_btnEeprom_Data_Clear" ); // "Resultate l�schen"
      this._btnEeprom_ScriptInfo.Text = r.GetString ( "GSMReader_btnEeprom_ScriptInfo" ); // "ScriptInfo �bertragen"
      
      // Display filepaths for EEprom data & scriptinfo storage
      this._txtFile_EE_Data.Text        = app.Data.Common.sEepromDataFile;
      this._txtFile_EE_ScriptInfo.Text  = app.Data.Common.sEepromScriptFile;
      
      // Update the form
      UpdateData ();
    }

    /// <summary>
    /// 'Closed' event of the form
    /// </summary>
    private void GSMReaderForm_Closed(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      
      // Stop timer

      // Write app data: EEprom data & scriptinfo filepaths
      app.Data.Common.sEepromDataFile   = this._txtFile_EE_Data.Text;
      app.Data.Common.sEepromScriptFile = this._txtFile_EE_ScriptInfo.Text;
      app.Data.Write ();

      // Check: Is a Transfer process still in progress?
      if ( _bTransferInProgress ) 
      {
        // Yes:
        // Check: Did we already get the name of the script, that was lastly running?
        if ( _sLastScriptName.Length > 0 ) 
        {
          // Yes:
          // Finish the transfer
          _EndTransfer ();
        }
      }
    }

    /// <summary>
    /// 'KeyPress' event of the form
    /// </summary>
    private void GSMReaderForm_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
    {
      // Close the form on pressing 'Escape'
      if ( e.KeyChar == (char) Keys.Escape )
        this.Close ();
    }
    
  
    /// <summary>
    /// 'Click' event of the '_btnFile' Button control
    /// </summary>
    private void _btnFile_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Get the current path of the file, the transferred data should be written into 
      // ( CD: Which Browse button was pressed? )
      string sFileName;
      if      ( sender  == this._btnFile_EE_Data )      sFileName = this._txtFile_EE_Data.Text;
      else if ( sender  == this._btnFile_EE_ScriptInfo) sFileName = this._txtFile_EE_ScriptInfo.Text;
      else                                              return; // F.a.F. ...
            
      // Get the full path of the file, the transferred data should be written into:

      // Prepare 'Save file' dialog
      //    Initial dir.
      string sInitialDir = @"C:\";                        // Default dir.
      int nPos = sFileName.LastIndexOf ( '\\' );          // If the file path is already known: dir. of the file
      if ( -1 != nPos )
      {
        string sDir = sFileName.Substring ( 0, nPos );
        if (Directory.Exists(sDir))
          sInitialDir = sDir;
      }
      Directory.SetCurrentDirectory ( sInitialDir );
      //    Filter
      string sFilter = "All files (*.*)|*.*";
      //    Def. ext.
      string sDefaultExt = "txt";
      //    Title
      string sTitle = r.GetString ( "GSMReader_OFN_Save" );    // "Datendatei speichern"
      
      // 'Save file' dialog
      SaveFileDialog sfd = new SaveFileDialog ();
      sfd.Title = sTitle;
      sfd.InitialDirectory = sInitialDir;
      sfd.FileName = sFileName;
      sfd.Filter = sFilter;
      sfd.DefaultExt = sDefaultExt;
      sfd.AddExtension = true;
            
      DialogResult dr = sfd.ShowDialog();
      if ( dr == DialogResult.OK )
      {
        // Overtake the file path corr'ing to the current transfer type ( CD: Which Browse button was pressed? )
        sFileName = sfd.FileName;
        // Display the file path
        if      (sender  == this._btnFile_EE_Data)        this._txtFile_EE_Data.Text        = sFileName;
        else if (sender  == this._btnFile_EE_ScriptInfo)  this._txtFile_EE_ScriptInfo.Text  = sFileName;
      }  
      sfd.Dispose ();
            
      // Update the form, if req.
      if ( dr == DialogResult.OK )
      {
        UpdateData ();
      }
    }

    /// <summary>
    /// 'Click' event of the '_btnEeprom_Data' Button control
    /// </summary>
    private void _btnEeprom_Data_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;

      // Check: Communication channel open?
      if ( !comm.IsOpen () )
        return; // No

      // Assign the name of the datafile, corr'ing to the current transfer
      _sFileName = this._txtFile_EE_Data.Text;
      if ( _sFileName.Trim ().Length == 0 ) 
        return;

      // Empty result list 
      _results.RemoveAll ();

      // Indicate that the Transfer process has begun.
      _bTransferInProgress = true;

      // TX: Script Current
      //    Parameter: none
      //    Device: Action - nothing; 
      //            Returns - Name of the script currently running on the PID device
      CommMessage msg = new CommMessage ( comm.msgScriptCurrent );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      // TX: Transfer Start
      //    Parameter: The transfer task to be performed
      //    Device: Action - Indicate, that the Eeprom data transfer to PC has begun; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgTransferStart );
      msg.Parameter = TransferTask.Eeprom_Data.ToString ( "D" );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );
      // TX: Store Eeprom-Data Count
      //    Parameter: none
      //    Device: Action - nothing; 
      //            Returns - # of result items (of type STORERESULTITEM) stored
      //                      in the result storage region of the Eeprom residing on the PID device
      msg = new CommMessage ( comm.msgStoreEDCount );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg );
    }

    /// <summary>
    /// 'Click' event of the '_btnEeprom_Data_Clear' Button control
    /// </summary>
    private void _btnEeprom_Data_Clear_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // Check: Communication channel open?
      if ( !comm.IsOpen () )
        return; // No

      // Safety request: Should the data really be cleared?
      string sMsg = r.GetString ("GSMReader_Que_ClearData");    // "Sollen die Daten wirklich gel�scht werden?"
      string sCaption = r.GetString ("Form_Common_TextSafReq"); // "Sicherheitsabfrage"
      DialogResult dr = MessageBox.Show (this, sMsg, sCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
      if ( dr == DialogResult.No )
        return; // No.

      // Yes:
#if GSMREADER_CLEAR_WITH_INTERRUPTION      
      // Indicate that the Transfer process has begun.
      _bTransferInProgress = true;

      // TX: Script Current
      //    Parameter: none
      //    Device: Action - nothing; 
      //            Returns - Name of the script currently running on the PID device
      CommMessage msg = new CommMessage ( comm.msgScriptCurrent );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg );
      // TX: Transfer Start
      //    Parameter: The transfer task to be performed
      //    Device: Action - Indicate, that the Eeprom data clear (transfer) has begun; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgTransferStart );
      msg.Parameter = TransferTask.Eeprom_Data_Clear.ToString ( "D" );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );
#endif
      
      // TX: Store Eeprom-Data Clear
      //    Parameter: none
      //    Device: Action - Clears the result storage region on the Eeprom; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgStoreEDClear );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
    }

    /// <summary>
    /// 'Click' event of the '_btnEeprom_ScriptInfo' Button control
    /// </summary>
    private void _btnEeprom_ScriptInfo_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // Check: Communication channel open?
      if ( !comm.IsOpen () )
        return; // No

      // Assign the name of the datafile, corr'ing to the current transfer
      _sFileName = this._txtFile_EE_ScriptInfo.Text;
      if ( _sFileName.Trim ().Length == 0 ) 
        return;

      // Reset the script collection object
      _sc.Reset ();

      // Instead (simpler): Empty ScriptInfo list
      _alScriptInfo.Clear ();

      // Get the # of scripts installed on the PID device
      _nScripts = app.Doc.arsDeviceScripts.Count;
      
      // Perform the ScriptInfo transfer, if installed scripts are present
      if (_nScripts > 0)
      {      
        // Indicate that the Transfer process has begun.
        _bTransferInProgress = true;

        // TX: Script Current
        //    Parameter: none
        //    Device: Action - nothing; 
        //            Returns - Name of the script currently running on the PID device
        CommMessage msg = new CommMessage ( comm.msgScriptCurrent );
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
        // TX: Transfer Start
        //    Parameter: The transfer task to be performed
        //    Device: Action - Indicate, that the Eeprom ScriptInfo transfer to PC has begun; 
        //            Returns - OK
        msg = new CommMessage ( comm.msgTransferStart );
        msg.Parameter = TransferTask.Eeprom_ScriptInfo.ToString ( "D" );
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage ( msg );
        // Reset the script counter
        _nCurrScriptIdx = 0;
      
        // Initiate: Progressbar control
        this._pgbTransfer.Minimum = 0;
        this._pgbTransfer.Maximum = _nScripts;
        this._pgbTransfer.Value   = _nCurrScriptIdx;
      
        // TX: Store Eeprom-ScriptInfo Open
        //    Parameter: Index of the script to be loaded
        //    Device: Action - Loades the script with the given idx from Eeprom; 
        //            Returns - Name of the addressed script
        msg = new CommMessage ( comm.msgStoreESOpen );
        msg.Parameter = _nCurrScriptIdx.ToString ();
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage ( msg );
      }
      else
      {
        string sMsg =  r.GetString ( "GSMReader_Info_Eeprom_NoScripts" ); // "Auf dem Eeprom des Ger�tes sind keine Scriptinformationen gespeichert."
        string sCap = r.GetString ( "Form_Common_TextInfo" );             // "Information"
        MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information ); 
      }
    }
   
    /// <summary>
    /// 'HelpRequested' event of some controls
    /// </summary>
    /// <remarks>
    /// </remarks>
    private void _ctl_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Help requesting control
      Control requestingControl = (Control)sender;
      // Assign help text
      string s = "";
      //    Select file Buttons
      if      ((requestingControl == this._btnFile_EE_Data) || (requestingControl == this._btnFile_EE_ScriptInfo))
        s = r.GetString ("GSMReader_Help_btnFile");               // "Auswahl der Datei zum Speichern der �bertragenen Daten."
        //    Eeprom Action Buttons
      else if (requestingControl == this._btnEeprom_Data)
        s = r.GetString ("GSMReader_Help_btnEeprom_Data");        // "Startet die �bertragung der Resultate vom Eeprom des Ger�ts."
      else if (requestingControl == this._btnEeprom_Data_Clear)
        s = r.GetString ("GSMReader_Help_btnEeprom_Data_Clear");  // "L�scht den Resultatsspeicher auf dem Eeprom des Ger�ts."
      else if (requestingControl == this._btnEeprom_ScriptInfo)
        s = r.GetString ("GSMReader_Help_btnEeprom_ScriptInfo");  // "Startet die �bertragung der Scriptinformationen vom Eeprom des Ger�ts."
      
      // Show help
      this._ToolTip.SetToolTip (requestingControl, s);
      hlpevent.Handled=true;
    }

    #endregion // event handling

    #region constants and enums

    /// <summary>
    /// The TransferType enumeration ( CD: Which Browse button was pressed? )
    /// </summary>
    enum TransferType 
    {
      Eeprom_Data,
      Eeprom_ScriptInfo
    };

    #endregion // constants and enums

    #region members
    
    /// <summary>
    /// True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    bool _bShowWait = false;

    /// <summary>
    /// Indicates, whether the stored data Transfer process is curr'ly in progress ( true ) or not ( false )
    /// </summary>
    bool _bTransferInProgress = false;
    
    /// <summary>
    /// Name of the script, that was lastly running on the PID device
    /// </summary>
    string _sLastScriptName = string.Empty;

    /// <summary>
    /// Path of the datafile, the transferred data should be written into
    /// </summary>
    string _sFileName = string.Empty;     

    /// <summary>
    /// The stored results list
    /// </summary>
    CStoreResults _results = new CStoreResults ();
    
    /// <summary>
    /// The script collection object, the ScriptInfo should be stored into
    /// </summary>
    ScriptCollection _sc = new ScriptCollection ();  
    /// <summary>
    /// The script, the ScriptInfo is stored within
    /// </summary>
    Script _scr = null; 
    
    /// <summary>
    /// Instead (simpler): The list for ScriptInfo storage ( ScriptInfo list )
    /// </summary>
    ArrayList _alScriptInfo = new ArrayList ();
    /// <summary>
    /// Indicates, whether on storing the ScriptInfo the configuration object (True) or the
    /// list (False) should be used
    /// </summary>
    Boolean _bSIStorage_UseConfig = true;
    
    //----------------------------
    // Data storage: Eeprom-Data

    /// <summary>
    /// # of result items stored on the PID device
    /// </summary>
    int _nStoreCount = 0;
    /// <summary>
    /// Transferred result items counter
    /// </summary>
    int _nItemCount = 0;
    /// <summary>
    /// From this: Valid result items
    /// </summary>
    int _nValidItemCount = 0;
    
    /// <summary>
    /// The # of different script indices in a result list
    /// </summary>
    int _nScriptIndices = 0;
    /// <summary>
    /// The current idx within the ScriptIndices list
    /// </summary>
    int _nCurrIdx = 0;
    
    //----------------------------
    // Data storage: Eeprom-ScriptInfo

    /// <summary>
    /// # of scripts installed on the PID device
    /// </summary>
    int _nScripts = 0;
    /// <summary>
    /// Idx. of the script curr'ly transferred
    /// </summary>
    int _nCurrScriptIdx = 0;

    /// <summary>
    /// Script curr'ly transferred: # of windows
    /// </summary>
    int _nWindows = 0;
    /// <summary>
    /// Script curr'ly transferred: Idx. of the window curr'ly transferred
    /// </summary>
    int _nCurrWindowIdx = 0;

    /// <summary>
    /// Script curr'ly transferred: # of events
    /// </summary>
    int _nEvents = 0;
    /// <summary>
    /// Script curr'ly transferred: Idx. of the event curr'ly transferred
    /// </summary>
    int _nCurrEventIdx = 0;
    
    #endregion // members

    #region methods

    /// <summary>
    /// Updates the form
    /// </summary>
    void UpdateData () 
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;

      // Select file Buttons
      Boolean bEn = ( doc.sVersion.Length > 0 ) && ( _bTransferInProgress == false );
      this._btnFile_EE_Data.Enabled       = bEn;      
      this._btnFile_EE_ScriptInfo.Enabled = bEn;
      // Eeprom Transfer Buttons
      bEn = ( doc.sVersion.Length > 0 ) && ( _bTransferInProgress == false ) && comm.IsOpen ();
      Boolean bEn1 = bEn && ( this._txtFile_EE_Data.Text.Length > 0 );
      this._btnEeprom_Data.Enabled        = bEn1; 
      this._btnEeprom_Data_Clear.Enabled  = bEn; 
      bEn1 = bEn && ( this._txtFile_EE_ScriptInfo.Text.Length > 0 );
      this._btnEeprom_ScriptInfo.Enabled  = bEn1;       
      // Progres bar
      bEn = doc.sVersion.Length > 0;
      this._pgbTransfer.Enabled           = bEn;    
    }
    
    /// <summary>
    /// Stores the result list to file (en bloque)
    /// </summary>
    void _WriteData ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      try 
      {
        // Wait cursor
        Cursor.Current = Cursors.WaitCursor;
        // Build the 'valitity array':
        // Check, whether the substance windows contain valid stored results or not.
        _results.FindActive ();
        // Write the result list to the specified file
        if ( !_results.FileWrite ( _sFileName ) ) 
        {
          throw new ApplicationException ();
        }   
      }
      catch 
      {
        // Error:

        // Show corr'ing message
        string fmt = r.GetString ( "GSMReader_Error_DataStorage" );   // "Fehler beim Speichern der Daten in Datei\r\n   '{0}'"
        string sMsg = string.Format ( fmt, _sFileName );
        string sCap = r.GetString ( "Form_Common_TextError" );        // "Fehler"
        MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error );
      }
      finally 
      {
        // Normal cursor
        Cursor.Current = Cursors.Default;
      }
    }

    /// <summary>
    /// Stores the ScriptInfo list to file (en bloque)
    /// </summary>
    void _WriteScriptInfo ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      StreamWriter file = null;
      try 
      {
        // Wait cursor
        Cursor.Current = Cursors.WaitCursor;
        
        // Creates the file for writing UTF-8 encoded text
        file = File.CreateText ( _sFileName );

        // Write the ScriptInfo list to the specified file
        foreach ( string s in this._alScriptInfo )
        {
          file.WriteLine (s);
        }
      }
      catch 
      {
        // Error:

        // Show corr'ing message
        string fmt = r.GetString ( "GSMReader_Error_ScriptInfoStorage" ); // "Fehler beim Speichern der Scriptinformation in Datei\r\n   '{0}'."
        string sMsg = string.Format ( fmt, _sFileName );
        string sCap = r.GetString ( "Form_Common_TextError" );            // "Fehler"
        MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error );
      }
      finally 
      {
        // Close the file
        if ( null != file ) 
          file.Close ();
        // Normal cursor
        Cursor.Current = Cursors.Default;
      }
    }

    /// <summary>
    /// Finishes the transfer
    /// </summary>
    void _EndTransfer ()
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Doc doc = app.Doc;

      // TX: Script Select
      // (Device: Action - Selects the script to be executed (here: script last used); TX - OK)
      CommMessage msg = new CommMessage ( comm.msgScriptSelect );
      msg.Parameter = doc.ScriptNameToIdx ( _sLastScriptName ).ToString ();
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );
      // TX: Transfer Complete
      //    Parameter: none
      //    Device: Action - Indicate, that the transfer to PC has completed; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgTransferComplete );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);

      // Indicate that the Transfer process has finished.
      _bTransferInProgress = false;

      // Reset: Value (position) of the Progressbar control
      this._pgbTransfer.Value = this._pgbTransfer.Minimum;
    }

    /// <summary>
    /// Performs actions in reaction of the receipt of a comm. message  
    /// </summary>
    /// <param name="msgRX">The comm. message</param>
    public void AfterRXCompleted(CommMessage msgRX)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // CD: Message response ID
      if ((msgRX.ResponseID == AppComm.CMID_TIMEOUT) || (msgRX.ResponseID == AppComm.CMID_STRLENERR))
      {
        // A comm. error (Timeout, ...) occurred:
      
        // Update the form
        UpdateData ();
      }
      
      else if (msgRX.ResponseID == AppComm.CMID_ANSWER)
      {
        // The message response is present:
        string sCmd = msgRX.CommandResponse;
        byte[] arbyPar = msgRX.ParameterResponse;

        // Get the parameter string from the parameter Byte array
        string sPar = Encoding.ASCII.GetString (arbyPar);
        // CD according to the message command
        switch ( sCmd ) 
        {
        
            //----------------------------------------------------------
            // Common Transfer messages
            //----------------------------------------------------------

          case "SCRCURRENT":
            // ScriptCurrent
            // RX: Name of the script currently running on the device   
            try
            {
              // Get the name of the script, that was lastly running on the IMS device
              _sLastScriptName = sPar;
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptCurrent->function failed" );
            }
            break;

          case "SCRSELECT":
            // ScriptSelect
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptSelect->function failed" );
            }
            break;
          
          case "TRNSTART":
            // Transfer Start
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgTransferStart->function failed" );
            }
            break;

          case "TRNCOMPLETE":
            // Transfer Complete
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgTransferComplete->function failed" );
            }
            break;

            //----------------------------------------------------------
            // Eeprom-Data
            //----------------------------------------------------------

          case "STREDCOUNT":
            // Store Eeprom-Data Count
            // RX: # of result items (of type STORERESULTITEM) stored
            //     in the result storage region of the Eeprom residing on the IMS device
            try
            {
              // Get the # of result items (of type STORERESULTITEM) stored 
              // in the result storage region of the Eeprom residing on the IMS device
              _nStoreCount = int.Parse ( sPar );
              if (0 == _nStoreCount)
              {
                // No result items stored:

                // Finish the transfer
                _EndTransfer ();
              
                // Display: # of items transferred
                string fmt = r.GetString ( "GSMReader_Info_ED_SD_count" );      // "'{0}' Items wurden �bertragen, davon sind '{1}' g�ltig."
                string sMsg = string.Format ( fmt, _nStoreCount, _nStoreCount );
                string sCap = r.GetString ( "Form_Common_TextInfo" );           // "Information"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
                break;
              }
            
              // There are result items stored:              
            
              // Initiate: Transferred result items counter, From this: Valid result items 
              _nItemCount = 0;
              _nValidItemCount = 0;

              // Initiate: Progressbar control
              this._pgbTransfer.Minimum = 0;
              this._pgbTransfer.Maximum = _nStoreCount;
              this._pgbTransfer.Value   = _nItemCount;
          
              // TX: Store Eeprom-Data Item ( initial )
              //    Parameter: Index of the result item to be retrieved
              //    Device: Action - nothing; 
              //            Returns - Stored result item with the given index
              CommMessage msg = new CommMessage ( comm.msgStoreEDItem );
              msg.Parameter = _nItemCount.ToString ();
              msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
              comm.WriteMessage ( msg );
            }
            catch
            {
              Debug.WriteLine ( "OnMsgStoreEDCount->function failed" );
            }
            break;

          case "STREDITEM":
            // Store Eeprom-Data Item
            // RX: Stored result item with the given index
            try
            {
              // Update result list
              try 
              {
                // Build a CStoreResultItem object from the parameter string
                CStoreResultItem item = new CStoreResultItem ( sPar );
                // Assign its position within the stored results
                item.dwPosition = (uint) _nItemCount;
                // Add it to the result list 
                _results.Add ( item );
                // Update: # of valid result items
                _nValidItemCount++;
              }
              catch 
              {
              }

              // Update: Transferred result items counter
              _nItemCount++;
              // Update: Value (position) of the Progressbar control
              this._pgbTransfer.Value = _nItemCount;

              // Check: Did we read all the result items stored on the IMS device? 
              if ( _nItemCount < _nStoreCount ) 
              {
                // No:
              
                // TX: Store Eeprom-Data Item ( subsequent )
                //    Parameter: Index of the result item to be retrieved
                //    Device: Action - nothing; 
                //            Returns - Stored result item with the given index
                CommMessage msg = new CommMessage ( comm.msgStoreEDItem );
                msg.Parameter = _nItemCount.ToString ();
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage ( msg );
              }
              else
              {
                // Yes:
              
                // Analyse result list: Get the different script indices
                _results.Analyze ();
              
                // Initiate Header info query
                _nScriptIndices = _results.ScriptIndices.Count;   // # of different script indices
                _nCurrIdx = 0;                                    // Current idx within the ScriptIndices list
                _results.HeaderInfo.Clear ();                     // Clear the HeaderInfo list

                // TX: Store Eeprom-Data Header ( initial )
                //    Parameter: Index of the 1st script present in the result list 
                //    Device: Action - nothing; 
                //            Returns - Name of the script with the given index & names of its substance windows
                CommMessage msg = new CommMessage ( comm.msgStoreEDHeader );
                msg.Parameter = _results.ScriptIndices[_nCurrIdx].ToString ();
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage ( msg );
              }
            }
            catch
            {
              Debug.WriteLine ( "OnMsgStoreEDItem->function failed" );
            }
            break;

          case "STREDHEADER":
            // Store Eeprom-Data Header
            // RX: Name of the script with the given index & names of its substance windows
            try
            {
              // Add header information to the HeaderInfo list 
              _results.HeaderInfo.Add (sPar);
              // Next idx within the ScriptIndices list
              _nCurrIdx++;
              // Check: Did we obtain all the required header information?
              if (_nCurrIdx < _nScriptIndices)
              {
                // No:
              
                // TX: Store Eeprom-Data Header ( subsequent )
                //    Parameter: Index of the next script present in the result list
                //    Device: Action - nothing; 
                //            Returns - Name of the script with the given index & names of its substance windows
                CommMessage msg = new CommMessage ( comm.msgStoreEDHeader );
                msg.Parameter = _results.ScriptIndices[_nCurrIdx].ToString ();
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage ( msg );
              }
              else
              {
                // Yes:
              
                // Finish the transfer
                _EndTransfer ( );
              
                // Store the result list to file (en bloque)
                _WriteData ();
              
                // Display: # of items transferred
                string fmt = r.GetString ( "GSMReader_Info_ED_SD_count" );  // "'{0}' Items wurden �bertragen, davon sind '{1}' g�ltig."
                string sMsg = string.Format ( fmt, _nItemCount, _nValidItemCount );
                string sCap = r.GetString ( "Form_Common_TextInfo" );       // "Information"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
              }
            }
            catch
            {
              Debug.WriteLine ( "OnMsgStoreEDHeader->function failed" );
            }
            break;

          case "STREDCLEAR":
            // Store Eeprom-Data Clear
            try
            {

#if GSMREADER_CLEAR_WITH_INTERRUPTION      
              // Finish the transfer
              _EndTransfer ( );
#endif
         
              // Success?
              if ( sPar.ToUpper () == "OK" ) 
              {
                // Yes:
                // Display: Success message
                string sMsg = r.GetString ( "GSMReader_Info_StoreEDclear" );  // "Die Daten wurden erfolgreich gel�scht."
                string sCap = r.GetString ( "Form_Common_TextInfo" );         // "Information"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
              }
            }
            catch
            {
              Debug.WriteLine ( "OnMsgStoreEDClear->function failed" );
            }
            break;

            //----------------------------------------------------------
            // Eeprom-ScriptInfo
            //----------------------------------------------------------

          case "STRESOPEN":
            // Store Eeprom-ScriptInfo Open
            // RX: Name of the addressed script
            try
            {
              // Check, whether on storing the ScriptInfo the script collection object or the
              // list should be used
              if ( _bSIStorage_UseConfig )
              {
                // The script collection object:
                // Get the addressed script
                _scr = _sc.Scripts[_nCurrScriptIdx];
                // Assign its name
                _scr.Name = sPar;
              }
              else
              {
                // The list:
                // Add the Name of the addressed script to the ScriptInfo list
                this._alScriptInfo.Add ( sPar );
              }

              // TX: Store Eeprom-ScriptInfo Parameter
              //    Parameter: none
              //    Device: Action - nothing; 
              //            Returns - Parameter string of the addressed script
              CommMessage msg = new CommMessage (comm.msgStoreESParameter);
              msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
              comm.WriteMessage (msg);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgStoreESOpen->function failed" );
            }
            break;
          
          case "STRESPARAM":
            // Store Eeprom-ScriptInfo Parameter
            // RX: Parameter string of the addressed script:
            //    "<MeasCycle>,<SigProcInt>,<PCCommInt>,<MeasScript>,<OffsetCorr>,<ScriptID>,
            //     <SpanConcS_0>,...,<SpanConcS_Nm1>,
            //     <AutoSpanMon_0.Enabled>,<AutoSpanMon_0.LowLimit>,<AutoSpanMon_0.UppLimit>,<AutoSpanMon_0.NoErrMeas>,
            //     ...,
            //     <AutoSpanMon_Nm1.Enabled>,<AutoSpanMon_Nm1.LowLimit>,<AutoSpanMon_Nm1.UppLimit>,<AutoSpanMon_Nm1.NoErrMeas>,
            //     <Total>",
            //    with: N = GSM_MAX_WINDOWS (Max. number of script windows)
            //    (# of parameters: 3 + 3 + GSM_MAX_WINDOWS + Script.MAX_SCRIPT_WINDOWS*4 + 1)
            try
            {
              // Check, whether on storing the ScriptInfo the script collection object or the
              // list should be used
              if ( _bSIStorage_UseConfig )
              {
                // The script collection object:
                // Addressed script: Update parameter section
                string[] ars = sPar.Split ( ',' );
                if ( ars.Length == 3 + 3 + Script.MAX_SCRIPT_WINDOWS + Script.MAX_SCRIPT_WINDOWS*4 + 1 )
                {
                  int i, j=0;
                  // Meas. cycle time
                  _scr.Parameter.dwCycleTime = UInt32.Parse (ars[j++]);
                  // Signal processing interval
                  _scr.Parameter.dwSigProcInt = UInt32.Parse (ars[j++]);
                  // PC comm. interval
                  _scr.Parameter.dwPCCommInt = UInt32.Parse (ars[j++]);
                  // Meas. script ident.
                  _scr.Parameter.byMeasScript = byte.Parse (ars[j++]);
                  // Static scan offset correction
                  _scr.Parameter.byOffsetCorr = byte.Parse (ars[j++]);
                  // Script ID
                  _scr.Parameter.dwScriptID = UInt32.Parse (ars[j++]);
                  // Span concentrations
                  System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
                  for (i=0; i < Script.MAX_SCRIPT_WINDOWS; i++)
                    _scr.Parameter.arfSpanConc[i] = float.Parse (ars[j++], nfi);
                  // Automated span monitoring
                  for (i=0; i < Script.MAX_SCRIPT_WINDOWS; i++)
                  {
                    int n = int.Parse (ars[j++]);
                    bool bEnabled = (n == 1) ? true : false;
                    string s = string.Format ("{0},{1},{2}", ars[j++], ars[j++], ars[j++]);
                    ScriptParameter.AutoSpanMon asm = new ScriptParameter.AutoSpanMon (bEnabled, s);
                    asm.CopyTo (_scr.Parameter.arAutoSpanMon[i]);
                  }
                  // Summary signal identifier
                  _scr.Parameter.sTotal = ars[j];
                }
              }
              else 
              {
                // The list:
                // Add the Parameter string of the addressed script to the ScriptInfo list
                this._alScriptInfo.Add ( sPar );
              }

              // TX: Store Eeprom-ScriptInfo #ofWindows
              //    Parameter: none
              //    Device: Action - nothing; 
              //            Returns - #ofWindows string of the addressed script
              CommMessage msg = new CommMessage (comm.msgStoreESWindowCount);
              msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
              comm.WriteMessage (msg);
            }
            catch (Exception exc)
            {
              string s = exc.Message;
              Debug.WriteLine ( "OnMsgStoreESParameter->function failed" );
            }
            break;
          
          case "STRESWINCNT":
            // Store Eeprom-ScriptInfo #ofWindows
            // RX: #ofWindows string of the addressed script
            try
            {
              // Get the #ofWindows of the addressed script
              _nWindows = int.Parse ( sPar );

              // Check, whether on storing the ScriptInfo the script collection object or the
              // list should be used
              if ( _bSIStorage_UseConfig )
              {
                // The script collection object:
                // Addressed script: Update # of script windows
                _scr.NumWindows = _nWindows;
              }

              // Initiate: Idx. of the window curr'ly transferred
              _nCurrWindowIdx = 0;
            
              // Next TX depending on the #ofWindows
              if (_nWindows > 0)
              {
                // TX: Store Eeprom-ScriptInfo Window
                //    Param: Index of the window whose information should be retrieved
                //    Device: Action - nothing; 
                //            Returns - String representation for the window with the given idx
                CommMessage msg = new CommMessage ( comm.msgStoreESWindow );
                msg.Parameter = _nCurrWindowIdx.ToString ();
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage ( msg );
              }
              else
              {
                // TX: Store Eeprom-ScriptInfo #ofEvents
                //    Parameter: none
                //    Device: Action - nothing; 
                //            Returns - #ofEvents string of the addressed script
                CommMessage msg = new CommMessage ( comm.msgStoreESEventCount );
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage (msg);
              }
            }
            catch
            {
              Debug.WriteLine ( "OnMsgStoreESWindowCount->function failed" );
            }
            break;
          
          case "STRESWIN":
            // Store Eeprom-ScriptInfo Window
            // RX: String representation for the window with the given idx of the addressed script
            try
            {
              // Check, whether on storing the ScriptInfo the script collection object or the
              // list should be used
              if ( _bSIStorage_UseConfig )
              {
                // The script collection object:
                // Addressed script: Update windows section
                ScriptWindow sw = _scr.Windows[_nCurrWindowIdx];
                string sKeyVal = ScriptCollection.FormatItemString (sPar);
                sw.Load (sKeyVal);
              }
              else 
              {
                // The list:
                // Add the current Window string of the addressed script to the ScriptInfo list
                this._alScriptInfo.Add ( sPar );
              }

              // Update: Idx. of the window curr'ly transferred
              _nCurrWindowIdx++;
            
              // Next TX depending on current window idx
              if (_nCurrWindowIdx < _nWindows)
              {
                // TX: Store Eeprom-ScriptInfo Window
                //    Param: Index of the window whose information should be retrieved
                //    Device: Action - nothing; 
                //            Returns - String representation for the window with the given idx
                CommMessage msg = new CommMessage ( comm.msgStoreESWindow );
                msg.Parameter = _nCurrWindowIdx.ToString ();
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage ( msg );
              }
              else
              {
                // TX: Store Eeprom-ScriptInfo #ofEvents
                //    Parameter: none
                //    Device: Action - nothing; 
                //            Returns - #ofEvents string of the addressed script
                CommMessage msg = new CommMessage ( comm.msgStoreESEventCount );
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage (msg);
              }
            }
            catch
            {
              Debug.WriteLine ( "OnMsgStoreESWindow->function failed" );
            }
            break;
          
          case "STRESEVTCNT":
            // Store Eeprom-ScriptInfo #ofEvents
            // RX: #ofEvents string of the addressed script
            try
            {
              // Get the #ofEvents of the addressed script
              _nEvents = int.Parse ( sPar );
            
              // Check, whether on storing the ScriptInfo the script collection object or the
              // list should be used
              if ( _bSIStorage_UseConfig )
              {
                // The script collection object:
                // Addressed script: Update # of script commands
                _scr.NumCommands = _nEvents;
              }
            
              // Initiate: Idx. of the event curr'ly transferred
              _nCurrEventIdx = 0;
            
              // Next TX depending on the #ofEvents
              if (_nEvents > 0)
              {
                // TX: Store Eeprom-ScriptInfo Event
                //    Parameter: Index of the event whose information should be retrieved
                //    Device: Action - nothing; 
                //            Returns - String representation for the event with the given idx
                CommMessage msg = new CommMessage ( comm.msgStoreESEvent );
                msg.Parameter = _nCurrEventIdx.ToString ();
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage ( msg );
              }
              else 
              {
                // No events available: Branch to 'Next ScriptInfo transfer'
                goto case "STRESEVT";
              }
            }
            catch
            {
              Debug.WriteLine ( "OnMsgStoreESEventCount->function failed" );
            }
            break;
          
          case "STRESEVT":
            // Store Eeprom-ScriptInfo Event
            // RX: String representation for the event with the given idx of the addressed script
            try
            {
              // Check: Events available?
              if (_nEvents > 0)
              {
                // Yes:
              
                // Check, whether on storing the ScriptInfo the script collection object or the
                // list should be used
                if ( _bSIStorage_UseConfig )
                {
                  // The script collection object:
                  // Addressed script: Update commands section
                  ScriptEvent se = _scr.Cmds[_nCurrEventIdx];
                  string sKeyVal = ScriptCollection.FormatItemString (sPar);
                  se.Load (sKeyVal);
                }
                else 
                {
                  // The list:
                  // Add the current Event string of the addressed script to the ScriptInfo list
                  this._alScriptInfo.Add ( sPar );
                }

                // Update: Idx. of the event curr'ly transferred
                _nCurrEventIdx++;
              
                // Next TX depending on current event idx
                if (_nCurrEventIdx < _nEvents)
                {
                  // TX: Store Eeprom-ScriptInfo Event
                  //    Parameter: Index of the event whose information should be retrieved
                  //    Device: Action - nothing; 
                  //            Returns - String representation for the event with the given idx
                  CommMessage msg = new CommMessage ( comm.msgStoreESEvent );
                  msg.Parameter = _nCurrEventIdx.ToString ();
                  msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                  comm.WriteMessage ( msg );
                  break;
                }
              }
            
              // No ( ... more Events available ):
              // Prepare: Next ScriptInfo transfer
              _nCurrScriptIdx++;
              
              // Update: Value (position) of the Progressbar control
              this._pgbTransfer.Value = _nCurrScriptIdx;

              // Check: ScriptInfo transfer completed?
              if (_nCurrScriptIdx < _nScripts)
              {
                // No:

                // TX: Store Eeprom-ScriptInfo Open
                //    Parameter: Index of the script to be loaded
                //    Device: Action - Loades the script with the given idx from Eeprom; 
                //            Returns - Name of the addressed script
                CommMessage msg = new CommMessage ( comm.msgStoreESOpen );
                msg.Parameter = _nCurrScriptIdx.ToString ();
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage ( msg );
              }
              else
              {
                // Yes:
                // ==> We read all the ScriptInfo storage on the IMS device: 
                string fmt, sMsg, sCap;

                // Finish the transfer
                _EndTransfer ( );

                // Check, whether on storing the ScriptInfo the script collection object or the
                // list should be used
                if ( _bSIStorage_UseConfig )
                {
                  // The script collection object:
                  // Assign the # of scripts
                  _sc.NumScripts = _nScripts;
                  // Store the script information (the script collection object) to file
                  try 
                  {
                    _sc.Write ( _sFileName );
                  }
                  catch 
                  {
                    fmt = r.GetString ("GSMReader_Error_ScriptInfoStorage");  // "Fehler beim Speichern der Scriptinformation in Datei\r\n'{0}'."
                    sMsg = string.Format (fmt, _sFileName);
                    sCap = r.GetString ("Form_Common_TextError");             // "Fehler"
                    MessageBox.Show (this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
                  }
                }
                else 
                {
                  // The list:
                  // Store the script information (the ScriptInfo list) to file (en bloque)
                  _WriteScriptInfo ();
                }
              
                // Display: # of ScriptInfo's (scripts) transferred
                fmt = r.GetString ( "GSMReader_Info_ES_SS_count" ); // '{0}' scripts were transferred.
                sMsg = string.Format ( fmt, _nScripts );
                sCap = r.GetString ( "Form_Common_TextInfo" );      // "Information"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
              }
            }
            catch
            {
              Debug.WriteLine ( "OnMsgStoreESEvent->function failed" );
            }
            break;

        } // E - switch ( id )
    
        // Update the form
        UpdateData ();
      }//E - if (e.Message.Id = AppComm.CMID_ANSWER)

    }

    #endregion // methods

    #region properties

    /// <summary>
    ///  True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    public bool ShowWait
    {
      get { return _bShowWait; }
      set { _bShowWait = value; }
    }

    #endregion properties

  }
}
