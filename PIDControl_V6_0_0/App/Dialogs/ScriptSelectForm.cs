using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;

namespace App
{
	/// <summary>
	/// Class ScriptSelectForm:
	/// Script selection dialog
	/// </summary>
  public class ScriptSelectForm : System.Windows.Forms.Form
  {
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.TextBox _txtIntro;
    private System.Windows.Forms.Button _btnOk;
    private System.Windows.Forms.ListBox _ListBox;
    private System.ComponentModel.Container components = null;

    /// <summary>
    /// Constructor
    /// </summary>
    public ScriptSelectForm()
    {
      InitializeComponent();

    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this._btnCancel = new System.Windows.Forms.Button();
      this._txtIntro = new System.Windows.Forms.TextBox();
      this._btnOk = new System.Windows.Forms.Button();
      this._ListBox = new System.Windows.Forms.ListBox();
      this.SuspendLayout();
      // 
      // _btnCancel
      // 
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point(252, 40);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.TabIndex = 3;
      this._btnCancel.Text = "Cancel";
      // 
      // _txtIntro
      // 
      this._txtIntro.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtIntro.Location = new System.Drawing.Point(8, 164);
      this._txtIntro.Multiline = true;
      this._txtIntro.Name = "_txtIntro";
      this._txtIntro.ReadOnly = true;
      this._txtIntro.Size = new System.Drawing.Size(236, 48);
      this._txtIntro.TabIndex = 1;
      this._txtIntro.Text = "";
      // 
      // _btnOk
      // 
      this._btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
      this._btnOk.Location = new System.Drawing.Point(252, 8);
      this._btnOk.Name = "_btnOk";
      this._btnOk.TabIndex = 2;
      this._btnOk.Text = "OK";
      // 
      // _ListBox
      // 
      this._ListBox.Location = new System.Drawing.Point(8, 8);
      this._ListBox.Name = "_ListBox";
      this._ListBox.Size = new System.Drawing.Size(236, 147);
      this._ListBox.TabIndex = 0;
      // 
      // ScriptSelectForm
      // 
      this.AcceptButton = this._btnOk;
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.CancelButton = this._btnCancel;
      this.ClientSize = new System.Drawing.Size(334, 220);
      this.Controls.Add(this._ListBox);
      this.Controls.Add(this._btnOk);
      this.Controls.Add(this._txtIntro);
      this.Controls.Add(this._btnCancel);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ScriptSelectForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Script selection";
      this.Load += new System.EventHandler(this.ScriptSelectForm_Load);
      this.Closed += new System.EventHandler(this.ScriptSelectForm_Closed);
      this.ResumeLayout(false);

    }

    #endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void ScriptSelectForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      Ressources r = app.Ressources;
      
      try
      {
        // Resources
        this.Text             = r.GetString ( "ScriptSelect_Title" );      // "Scriptauswahl"
        this._btnCancel.Text  = r.GetString ( "ScriptSelect_btnCancel" );  // "Abbruch"
        this._btnOk.Text      = r.GetString ( "ScriptSelect_btnOk" );      // "OK"
        this._txtIntro.Text   = r.GetString ( "ScriptSelect_txtIntro" );   // "Bitte w�hlen Sie das auszuf�hrende Script aus.\r\nW�hlen Sie dann 'Script ausf�hren', um die Messung zu starten."

        // Fill the _ListBox control with the available scripts
        this._ListBox.Items.Clear ();
        for ( int i=0; i < doc.arsDeviceScripts.Count; i++ )
          this._ListBox.Items.Add ( doc.arsDeviceScripts[i] );

        // Highlight the script last used, if any
        _sCurSelScript = doc.sCurrentScript;
        this._ListBox.SelectedItem = _sCurSelScript;
      }
      catch
      {
      }
    }

    /// <summary>
    /// 'Closed' event of the form
    /// </summary>
    private void ScriptSelectForm_Closed(object sender, System.EventArgs e)
    {
      if ( this.DialogResult == DialogResult.OK ) 
      {
        try 
        {  
          App app = App.Instance;
          Doc doc = app.Doc;

          // Update the documents 'sCurrentScript' member: The script current selected
          _sCurSelScript = this._ListBox.SelectedItem.ToString ();
          doc.sCurrentScript = _sCurSelScript;
        }
        catch 
        {
        }
      }
    }
    
    #endregion event handling

    #region members
    
    /// <summary>
    /// The name of the script curr'ly selected
    /// </summary>
    string  _sCurSelScript = string.Empty;

    #endregion members
 
  }
}
