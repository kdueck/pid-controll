using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using System.Text;

using CommRS232;

namespace App
{
	/// <summary>
	/// Class ConnectForm:
	/// Connect dialog
	/// </summary>
  public class ConnectForm : System.Windows.Forms.Form
  {
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.TextBox _txtIntro;
    private System.Windows.Forms.TextBox _txtMsg;
    private System.Windows.Forms.Timer _Timer;
    private System.ComponentModel.IContainer components;

    /// <summary>
    /// Constructor
    /// </summary>
    public ConnectForm()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this._btnCancel = new System.Windows.Forms.Button();
      this._txtIntro = new System.Windows.Forms.TextBox();
      this._txtMsg = new System.Windows.Forms.TextBox();
      this._Timer = new System.Windows.Forms.Timer(this.components);
      this.SuspendLayout();
      // 
      // _btnCancel
      // 
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point(252, 8);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.TabIndex = 0;
      this._btnCancel.Text = "Cancel";
      // 
      // _txtIntro
      // 
      this._txtIntro.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtIntro.Location = new System.Drawing.Point(8, 8);
      this._txtIntro.Multiline = true;
      this._txtIntro.Name = "_txtIntro";
      this._txtIntro.ReadOnly = true;
      this._txtIntro.Size = new System.Drawing.Size(236, 48);
      this._txtIntro.TabIndex = 1;
      this._txtIntro.Text = "";
      // 
      // _txtMsg
      // 
      this._txtMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtMsg.Location = new System.Drawing.Point(8, 64);
      this._txtMsg.Name = "_txtMsg";
      this._txtMsg.ReadOnly = true;
      this._txtMsg.Size = new System.Drawing.Size(236, 13);
      this._txtMsg.TabIndex = 2;
      this._txtMsg.Text = "";
      // 
      // _Timer
      // 
      this._Timer.Interval = 1000;
      this._Timer.Tick += new System.EventHandler(this._Timer_Tick);
      // 
      // ConnectForm
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.CancelButton = this._btnCancel;
      this.ClientSize = new System.Drawing.Size(334, 92);
      this.Controls.Add(this._txtMsg);
      this.Controls.Add(this._txtIntro);
      this.Controls.Add(this._btnCancel);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ConnectForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Establish connection";
      this.Load += new System.EventHandler(this.ConnectForm_Load);
      this.Closed += new System.EventHandler(this.ConnectForm_Closed);
      this.ResumeLayout(false);

    }

    #endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void ConnectForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // Resources
      this.Text             = r.GetString ( "Connect_Title" );                // "Verbindung herstellen"
      this._btnCancel.Text  = r.GetString ( "Connect_btnCancel" );            // "Abbruch"

      // Check: Communication channel open?
      if ( !comm.IsOpen () )
      {
        // No:

        // Show corr'ing intro text
        this._txtIntro.Text = r.GetString ( "Connect_CommNotAccessible" );    // "Communication channel not accessible."
      }
      else
      {
        // Yes:

        // Init. Message text
        this._txtIntro.Text = r.GetString ( "Connect_ReadVersion" );            // "Versuche die Verbindung herzustellen und die Version zu lesen ..."
        
        // TX: Version
        app.Doc.sVersion = "";
        CommMessage msg = new CommMessage (comm.msgVersion);
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      
        // TX: DateTime
        msg = new CommMessage (comm.msgDateTime);
        msg.Parameter = DateTime.Now.ToString ( "ddMMyyHHmmss" );      
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      
        // TX: Script list
        msg = new CommMessage (comm.msgScriptList);
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      
        // TX: Read service data (Device section)
        app.Doc.sDevNo = "";
        msg = new CommMessage (comm.msgServiceReadData);
        msg.Parameter = "0";      
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      
        // TX: Language synchronisation, if req.
        // Notes:
        //    - En: 0
        //    - De: 1
        if (app.Data.Language.bLangSyn)
        {
          msg = new CommMessage (comm.msgLanguageSynchro);
          int nLang;
          if ( app.Data.Language.eLanguage == AppData.LanguageData.Languages.en ) nLang=0;
          else                                                                    nLang=1;
          msg.Parameter = nLang.ToString ();
          msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
          comm.WriteMessage (msg);
        }

        // TX: CTS Read
        msg = new CommMessage (comm.msgCTSRead);
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);

        // Enable timer
        _Timer.Enabled = true;
      }
    }
	
    /// <summary>
    /// 'Closed' event of the form
    /// </summary>
    private void ConnectForm_Closed(object sender, System.EventArgs e)
    {
      // Disable timer
      _Timer.Enabled = false;
    }

    /// <summary>
    /// 'Tick' event of the _Timer control (each 1 sec)
    /// </summary>
    private void _Timer_Tick(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // Check: Communication channel open?
      if ( !comm.IsOpen () ) 
      {
        return; // No
      }

      // Check: Communication idle?
      bool bCommIdle = comm.IsIdle ();
      if ( bCommIdle )
      {
        // Yes ( d.h.: unbeschäftigt ):

        // Incr. 'Check' counter
        _nCheckCount++;
      
        // CD: WarmUp running?
        if (doc.bWarmUpRunning)
        {
          // Yes
          switch ( _nCheckCount )
          {
            case 1:                 // Init'ly: Update intro text corr'ly
              this._txtIntro.Text = r.GetString ( "Connect_WarmUp" );             // "The device is in WarmUp state.\r\nA connection cannot be established."
              break;

            case _2nd_nCheckCount:  // 2. check point: Close dialog with Cancel
              this.DialogResult = DialogResult.Cancel;
              this.Close ();
              break;
          }
        }
        else
        {
          // No
          switch ( _nCheckCount )
          {
            case 1:                 // Init'ly: Update intro text corr'ly
              this._txtIntro.Text = r.GetString ( "Connect_CheckVersion" );       // "Checking the version ..."
              break;

            case _1st_nCheckCount:  // 1. check point: Check version number and show check result
              if ( doc.CheckVersion() ) 
                this._txtMsg.Text = r.GetString ( "Connect_CheckVersionOK" );     // "Correct version number."
              else 
                this._txtMsg.Text = r.GetString ( "Connect_CheckVersionFailed" ); // "Wrong version number."
              break;

            case _2nd_nCheckCount:  // 2. check point: Close dialog with OK/Cancel on correct/incorrect version
              if ( doc.CheckVersion() )  
                this.DialogResult = DialogResult.OK;
              else 
                this.DialogResult = DialogResult.Cancel;
              this.Close ();
              break;
          }
        }
      }
      else
      {
        // No ( d.h.: beschäftigt ):

        // Incr. 'Read' counter
        _nReadCount++;
      
        // Update intro text (Append a point each sec)
        // Notes:
        //  2 variants are presented here, both with equal validity. We choose the "correct" one.
        // (1. This is the normal way in case of a 1 sec timer, as used here)
        //        string s = r.GetString ( "Connect_ReadVersion" );                     // "Trying to establish connection and reading the version ..."
        //        for (int i=0; i < _nReadCount; i++)
        //          s += ".";
        //        this._txtIntro.Text = s;
        // (2. This is the correct way in case of a timer with an arbitrary interval)
        int nmsec = _nReadCount * _Timer.Interval;
        int nWholeSec = nmsec / 1000;
        int nFracMsec = nmsec % 1000;
        if (nFracMsec < _Timer.Interval/2)
        {
          string s = r.GetString ( "Connect_ReadVersion" );                     // "Trying to establish connection and reading the version ..."
          for (int i=0; i < nWholeSec; i++)
            s += ".";
          this._txtIntro.Text = s;
        }
      }
    }

    #endregion event handling

    #region constants
     
    // 1. check point (in sec):
    // If the connection is established, and this duration has elapsed afterwards, the version check is performed.
    const int _1st_nCheckCount = 2;
    // 2. check point (in sec):
    // If the connection is established, and this duration has elapsed afterwards, the dialog is closed 
    // with 'OK' indication on correct version OR with 'Cancel' indication on incorrect version.
    const int _2nd_nCheckCount = 5;     
    
    #endregion constants
    
    #region members
    
    /// <summary>
    /// True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    bool _bShowWait = false;
    
    /// <summary>
    /// Version 'Check' counter (incr'd each sec)
    /// </summary>
    int _nCheckCount = 0;

    /// <summary>
    /// Version 'Read' counter (incr'd each sec)
    /// </summary>
    int _nReadCount = 0;
    
    #endregion members

    #region methods
    
    /// <summary>
    /// Performs actions in reaction of the receipt of a comm. message  
    /// </summary>
    /// <param name="msgRX">The comm. message</param>
    public void AfterRXCompleted(CommMessage msgRX)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      MainForm f = app.MainForm;

      // CD: Message response ID
      if ((msgRX.ResponseID == AppComm.CMID_TIMEOUT) || (msgRX.ResponseID == AppComm.CMID_STRLENERR))
      {
        // A comm. error (Timeout, ...) occurred:

        // Close the ConnectForm
        Close ();
      }
      
      else if (msgRX.ResponseID == AppComm.CMID_ANSWER)
      {
        // The message response is present:
        string sCmd = msgRX.CommandResponse;
        byte[] arbyPar = msgRX.ParameterResponse;

        // Get the parameter string from the parameter Byte array
        string sPar = Encoding.ASCII.GetString (arbyPar);
        // CD according to the message command
        switch ( sCmd ) 
        {
            //----------------------------------------------------------
            // General messages
            //----------------------------------------------------------
          
          case "VER":
            // Version
            try
            {
              // Update document members
              string[] ars = sPar.Split( '\t' );
              doc.sVersion = ars[0];                // device version
              doc.nMPChan = int.Parse(ars[1]);      // The # of MP channels (0, if MP not connected)
            }
            catch
            {
              Debug.WriteLine ( "OnMsgVersion->function failed" );
            }
            break;

          case "LANGSYN":
            // Language synchronisation
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgLanguageSynchro->function failed" );
            }
            break;

          case "DATI":
            // Set the DateTime on the device
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgDateTime->function failed" );
            }
            break;
            
            //----------------------------------------------------------
            // Script messages
            //----------------------------------------------------------
        
          case "SCRLIST":
            // ScriptList
            try
            {
              string sMsg = string.Format ("Connect: SCRLIST: {0}", sPar); 
              Debug.WriteLine ( sMsg );  
              
              // Update the Script list (= documents 'arsDeviceScripts' member)
              doc.UpdateScriptList (sPar);
              // Update view
              doc.UpdateAllViews ();
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptList->function failed" );
            }
            break;
        
            //----------------------------------------------------------
            // Service-Read
            //----------------------------------------------------------

          case "SVCREADDATA":
            // Service Read Data
            // RX: The service data from the device (Device section)
            try
            {
              string[] ars = sPar.Split ( '\t' );
              // The 1. string in the array is the Device No.            
              doc.sDevNo = ars[0];                
              // Update the 'Device information / Device No.' label
              f.lblDeviceNo_Val.Text = doc.sDevNo;
            }
            catch
            {
              Debug.WriteLine ( "OnMsgServiceReadData->function failed" );
            }
            break;
        
            //----------------------------------------------------------
            // CTS-Read
            //----------------------------------------------------------

          case "CTSREAD":
            // CTS Read
            // RX: The CTS data from the device
            try
            {
              string[] ars = sPar.Split ( '\t' );
              // The 1. string in the array is the 'CTS handling enabled?' indication           
              doc.bCTSRunning = (ars[0].Trim() == "1") ? true : false;
            }
            catch 
            {
              Debug.WriteLine ( "OnMsgCTSReadData->function failed" );
            }
            break;

        }//E - switch
      }//E - if (e.Message.Id = AppComm.CMID_ANSWER)

    }

    #endregion methods

    #region properties

    /// <summary>
    ///  True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    public bool ShowWait
    {
      get { return _bShowWait; }
      set { _bShowWait = value; }
    }

    #endregion properties

  }
}
