using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;

namespace App
{
	/// <summary>
	/// Class CommentForm:
	/// Comment dialog
	/// </summary>
  public class CommentForm : System.Windows.Forms.Form
  {
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.Button _btnOk;
    private System.Windows.Forms.Label _lblInfo;
    private System.Windows.Forms.TextBox _txtComment;
    private System.ComponentModel.Container components = null;

    /// <summary>
    /// Constructor
    /// </summary>
    public CommentForm()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this._btnCancel = new System.Windows.Forms.Button();
      this._btnOk = new System.Windows.Forms.Button();
      this._lblInfo = new System.Windows.Forms.Label();
      this._txtComment = new System.Windows.Forms.TextBox();
      this.SuspendLayout();
      // 
      // _btnCancel
      // 
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point(136, 128);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.TabIndex = 3;
      this._btnCancel.Text = "Cancel";
      // 
      // _btnOk
      // 
      this._btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
      this._btnOk.Location = new System.Drawing.Point(40, 128);
      this._btnOk.Name = "_btnOk";
      this._btnOk.TabIndex = 2;
      this._btnOk.Text = "OK";
      // 
      // _lblInfo
      // 
      this._lblInfo.Location = new System.Drawing.Point(8, 8);
      this._lblInfo.Name = "_lblInfo";
      this._lblInfo.Size = new System.Drawing.Size(232, 36);
      this._lblInfo.TabIndex = 0;
      this._lblInfo.Text = "Please enter your comment (max. 100 characters):";
      this._lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // _txtComment
      // 
      this._txtComment.Location = new System.Drawing.Point(8, 48);
      this._txtComment.MaxLength = 100;
      this._txtComment.Multiline = true;
      this._txtComment.Name = "_txtComment";
      this._txtComment.Size = new System.Drawing.Size(232, 68);
      this._txtComment.TabIndex = 1;
      this._txtComment.Text = "";
      // 
      // CommentForm
      // 
      this.AcceptButton = this._btnOk;
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.CancelButton = this._btnCancel;
      this.ClientSize = new System.Drawing.Size(250, 160);
      this.Controls.Add(this._txtComment);
      this.Controls.Add(this._lblInfo);
      this.Controls.Add(this._btnOk);
      this.Controls.Add(this._btnCancel);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "CommentForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Enter comment";
      this.Load += new System.EventHandler(this.CommentForm_Load);
      this.Closed += new System.EventHandler(this.CommentForm_Closed);
      this.ResumeLayout(false);

    }

    #endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void CommentForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      try
      {
        // Resources
        this.Text             = r.GetString ( "Comment_Title" );      // "Kommentar"
        this._btnCancel.Text  = r.GetString ( "Comment_btnCancel" );  // "Abbruch"
        this._btnOk.Text      = r.GetString ( "Comment_btnOK" );      // "OK"
        this._lblInfo.Text    = r.GetString ( "Comment_lblInfo" );    // "Bitte geben Sie Ihren Kommentar ein (max. 100 Zeichen):"
      }
      catch
      {
      }
    }

    /// <summary>
    /// 'Closed' event of the form
    /// </summary>
    private void CommentForm_Closed(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      // Overtake the comment inout
      if ( this.DialogResult == DialogResult.OK ) 
        doc.sComment = _txtComment.Text.Trim();
      else
        doc.sComment = "";
    }
    
    #endregion event handling

    #region members
    #endregion members
  
  }
}
