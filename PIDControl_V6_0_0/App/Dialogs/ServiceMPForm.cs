using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using System.Diagnostics;

using CommRS232;

namespace App
{
	/// <summary>
	/// Class ServiceMPForm:
	/// Service Control MultiPlexer (MP)
	/// </summary>
	/// <remarks>
	/// </remarks>
	public class ServiceMPForm : System.Windows.Forms.Form
	{
    private System.Windows.Forms.Button _btnRead;
    private System.Windows.Forms.Button _btnWrite;
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.ProgressBar _pgbTransfer;
    private System.Windows.Forms.ToolTip _ToolTip;
    private System.Windows.Forms.Timer _Timer;
    private System.Windows.Forms.CheckBox _chkPeriodicUpdate;
    private System.Windows.Forms.GroupBox _gbFlow;
    private System.Windows.Forms.Label _lblSoll;
    private System.Windows.Forms.TextBox _txtF1s;
    private System.Windows.Forms.TextBox _txtF3s;
    private System.Windows.Forms.TextBox _txtF2s;
    private System.Windows.Forms.TextBox _txtF4s;
    private System.Windows.Forms.TextBox _txtF5s;
    private System.Windows.Forms.TextBox _txtF6s;
    private System.Windows.Forms.TextBox _txtF7s;
    private System.Windows.Forms.TextBox _txtF8s;
    private System.Windows.Forms.TextBox _txtF9s;
    private System.Windows.Forms.TextBox _txtF10s;
    private System.Windows.Forms.TextBox _txtF11s;
    private System.Windows.Forms.TextBox _txtF12s;
    private System.Windows.Forms.TextBox _txtF13s;
    private System.Windows.Forms.TextBox _txtF14s;
    private System.Windows.Forms.TextBox _txtF15s;
    private System.Windows.Forms.TextBox _txtF16s;
    private System.Windows.Forms.TextBox _txtF16i;
    private System.Windows.Forms.TextBox _txtF15i;
    private System.Windows.Forms.TextBox _txtF14i;
    private System.Windows.Forms.TextBox _txtF13i;
    private System.Windows.Forms.TextBox _txtF12i;
    private System.Windows.Forms.TextBox _txtF11i;
    private System.Windows.Forms.TextBox _txtF10i;
    private System.Windows.Forms.TextBox _txtF9i;
    private System.Windows.Forms.TextBox _txtF8i;
    private System.Windows.Forms.TextBox _txtF7i;
    private System.Windows.Forms.TextBox _txtF6i;
    private System.Windows.Forms.TextBox _txtF5i;
    private System.Windows.Forms.TextBox _txtF4i;
    private System.Windows.Forms.TextBox _txtF2i;
    private System.Windows.Forms.TextBox _txtF3i;
    private System.Windows.Forms.TextBox _txtF1i;
    private System.Windows.Forms.Label _lblIst;
    private System.ComponentModel.IContainer components;

		/// <summary>
		/// Constructor
		/// </summary>
    public ServiceMPForm()
		{
			InitializeComponent();
      
      // Init.
      Init ();
    }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		
    /// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.components = new System.ComponentModel.Container();
      this._gbFlow = new System.Windows.Forms.GroupBox();
      this._txtF16i = new System.Windows.Forms.TextBox();
      this._txtF15i = new System.Windows.Forms.TextBox();
      this._txtF14i = new System.Windows.Forms.TextBox();
      this._txtF13i = new System.Windows.Forms.TextBox();
      this._txtF12i = new System.Windows.Forms.TextBox();
      this._txtF11i = new System.Windows.Forms.TextBox();
      this._txtF10i = new System.Windows.Forms.TextBox();
      this._txtF9i = new System.Windows.Forms.TextBox();
      this._txtF8i = new System.Windows.Forms.TextBox();
      this._txtF7i = new System.Windows.Forms.TextBox();
      this._txtF6i = new System.Windows.Forms.TextBox();
      this._txtF5i = new System.Windows.Forms.TextBox();
      this._txtF4i = new System.Windows.Forms.TextBox();
      this._txtF2i = new System.Windows.Forms.TextBox();
      this._txtF3i = new System.Windows.Forms.TextBox();
      this._txtF1i = new System.Windows.Forms.TextBox();
      this._lblIst = new System.Windows.Forms.Label();
      this._txtF16s = new System.Windows.Forms.TextBox();
      this._txtF15s = new System.Windows.Forms.TextBox();
      this._txtF14s = new System.Windows.Forms.TextBox();
      this._txtF13s = new System.Windows.Forms.TextBox();
      this._txtF12s = new System.Windows.Forms.TextBox();
      this._txtF11s = new System.Windows.Forms.TextBox();
      this._txtF10s = new System.Windows.Forms.TextBox();
      this._txtF9s = new System.Windows.Forms.TextBox();
      this._txtF8s = new System.Windows.Forms.TextBox();
      this._txtF7s = new System.Windows.Forms.TextBox();
      this._txtF6s = new System.Windows.Forms.TextBox();
      this._txtF5s = new System.Windows.Forms.TextBox();
      this._txtF4s = new System.Windows.Forms.TextBox();
      this._txtF2s = new System.Windows.Forms.TextBox();
      this._txtF3s = new System.Windows.Forms.TextBox();
      this._txtF1s = new System.Windows.Forms.TextBox();
      this._lblSoll = new System.Windows.Forms.Label();
      this._btnRead = new System.Windows.Forms.Button();
      this._btnWrite = new System.Windows.Forms.Button();
      this._btnCancel = new System.Windows.Forms.Button();
      this._pgbTransfer = new System.Windows.Forms.ProgressBar();
      this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
      this._Timer = new System.Windows.Forms.Timer(this.components);
      this._chkPeriodicUpdate = new System.Windows.Forms.CheckBox();
      this._gbFlow.SuspendLayout();
      this.SuspendLayout();
      // 
      // _gbFlow
      // 
      this._gbFlow.Controls.Add(this._txtF16i);
      this._gbFlow.Controls.Add(this._txtF15i);
      this._gbFlow.Controls.Add(this._txtF14i);
      this._gbFlow.Controls.Add(this._txtF13i);
      this._gbFlow.Controls.Add(this._txtF12i);
      this._gbFlow.Controls.Add(this._txtF11i);
      this._gbFlow.Controls.Add(this._txtF10i);
      this._gbFlow.Controls.Add(this._txtF9i);
      this._gbFlow.Controls.Add(this._txtF8i);
      this._gbFlow.Controls.Add(this._txtF7i);
      this._gbFlow.Controls.Add(this._txtF6i);
      this._gbFlow.Controls.Add(this._txtF5i);
      this._gbFlow.Controls.Add(this._txtF4i);
      this._gbFlow.Controls.Add(this._txtF2i);
      this._gbFlow.Controls.Add(this._txtF3i);
      this._gbFlow.Controls.Add(this._txtF1i);
      this._gbFlow.Controls.Add(this._lblIst);
      this._gbFlow.Controls.Add(this._txtF16s);
      this._gbFlow.Controls.Add(this._txtF15s);
      this._gbFlow.Controls.Add(this._txtF14s);
      this._gbFlow.Controls.Add(this._txtF13s);
      this._gbFlow.Controls.Add(this._txtF12s);
      this._gbFlow.Controls.Add(this._txtF11s);
      this._gbFlow.Controls.Add(this._txtF10s);
      this._gbFlow.Controls.Add(this._txtF9s);
      this._gbFlow.Controls.Add(this._txtF8s);
      this._gbFlow.Controls.Add(this._txtF7s);
      this._gbFlow.Controls.Add(this._txtF6s);
      this._gbFlow.Controls.Add(this._txtF5s);
      this._gbFlow.Controls.Add(this._txtF4s);
      this._gbFlow.Controls.Add(this._txtF2s);
      this._gbFlow.Controls.Add(this._txtF3s);
      this._gbFlow.Controls.Add(this._txtF1s);
      this._gbFlow.Controls.Add(this._lblSoll);
      this._gbFlow.Location = new System.Drawing.Point(8, 8);
      this._gbFlow.Name = "_gbFlow";
      this._gbFlow.Size = new System.Drawing.Size(724, 92);
      this._gbFlow.TabIndex = 0;
      this._gbFlow.TabStop = false;
      this._gbFlow.Text = "Flow";
      // 
      // _txtF16i
      // 
      this._txtF16i.Location = new System.Drawing.Point(664, 56);
      this._txtF16i.Name = "_txtF16i";
      this._txtF16i.ReadOnly = true;
      this._txtF16i.Size = new System.Drawing.Size(36, 20);
      this._txtF16i.TabIndex = 33;
      this._txtF16i.Text = "";
      this._txtF16i.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF16i.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF15i
      // 
      this._txtF15i.Location = new System.Drawing.Point(624, 56);
      this._txtF15i.Name = "_txtF15i";
      this._txtF15i.ReadOnly = true;
      this._txtF15i.Size = new System.Drawing.Size(36, 20);
      this._txtF15i.TabIndex = 32;
      this._txtF15i.Text = "";
      this._txtF15i.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF15i.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF14i
      // 
      this._txtF14i.Location = new System.Drawing.Point(584, 56);
      this._txtF14i.Name = "_txtF14i";
      this._txtF14i.ReadOnly = true;
      this._txtF14i.Size = new System.Drawing.Size(36, 20);
      this._txtF14i.TabIndex = 31;
      this._txtF14i.Text = "";
      this._txtF14i.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF14i.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF13i
      // 
      this._txtF13i.Location = new System.Drawing.Point(544, 56);
      this._txtF13i.Name = "_txtF13i";
      this._txtF13i.ReadOnly = true;
      this._txtF13i.Size = new System.Drawing.Size(36, 20);
      this._txtF13i.TabIndex = 30;
      this._txtF13i.Text = "";
      this._txtF13i.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF13i.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF12i
      // 
      this._txtF12i.Location = new System.Drawing.Point(504, 56);
      this._txtF12i.Name = "_txtF12i";
      this._txtF12i.ReadOnly = true;
      this._txtF12i.Size = new System.Drawing.Size(36, 20);
      this._txtF12i.TabIndex = 29;
      this._txtF12i.Text = "";
      this._txtF12i.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF12i.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF11i
      // 
      this._txtF11i.Location = new System.Drawing.Point(464, 56);
      this._txtF11i.Name = "_txtF11i";
      this._txtF11i.ReadOnly = true;
      this._txtF11i.Size = new System.Drawing.Size(36, 20);
      this._txtF11i.TabIndex = 28;
      this._txtF11i.Text = "";
      this._txtF11i.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF11i.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF10i
      // 
      this._txtF10i.Location = new System.Drawing.Point(424, 56);
      this._txtF10i.Name = "_txtF10i";
      this._txtF10i.ReadOnly = true;
      this._txtF10i.Size = new System.Drawing.Size(36, 20);
      this._txtF10i.TabIndex = 27;
      this._txtF10i.Text = "";
      this._txtF10i.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF10i.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF9i
      // 
      this._txtF9i.Location = new System.Drawing.Point(384, 56);
      this._txtF9i.Name = "_txtF9i";
      this._txtF9i.ReadOnly = true;
      this._txtF9i.Size = new System.Drawing.Size(36, 20);
      this._txtF9i.TabIndex = 26;
      this._txtF9i.Text = "";
      this._txtF9i.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF9i.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF8i
      // 
      this._txtF8i.Location = new System.Drawing.Point(344, 56);
      this._txtF8i.Name = "_txtF8i";
      this._txtF8i.ReadOnly = true;
      this._txtF8i.Size = new System.Drawing.Size(36, 20);
      this._txtF8i.TabIndex = 25;
      this._txtF8i.Text = "";
      this._txtF8i.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF8i.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF7i
      // 
      this._txtF7i.Location = new System.Drawing.Point(304, 56);
      this._txtF7i.Name = "_txtF7i";
      this._txtF7i.ReadOnly = true;
      this._txtF7i.Size = new System.Drawing.Size(36, 20);
      this._txtF7i.TabIndex = 24;
      this._txtF7i.Text = "";
      this._txtF7i.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF7i.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF6i
      // 
      this._txtF6i.Location = new System.Drawing.Point(264, 56);
      this._txtF6i.Name = "_txtF6i";
      this._txtF6i.ReadOnly = true;
      this._txtF6i.Size = new System.Drawing.Size(36, 20);
      this._txtF6i.TabIndex = 23;
      this._txtF6i.Text = "";
      this._txtF6i.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF6i.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF5i
      // 
      this._txtF5i.Location = new System.Drawing.Point(224, 56);
      this._txtF5i.Name = "_txtF5i";
      this._txtF5i.ReadOnly = true;
      this._txtF5i.Size = new System.Drawing.Size(36, 20);
      this._txtF5i.TabIndex = 22;
      this._txtF5i.Text = "";
      this._txtF5i.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF5i.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF4i
      // 
      this._txtF4i.Location = new System.Drawing.Point(184, 56);
      this._txtF4i.Name = "_txtF4i";
      this._txtF4i.ReadOnly = true;
      this._txtF4i.Size = new System.Drawing.Size(36, 20);
      this._txtF4i.TabIndex = 21;
      this._txtF4i.Text = "";
      this._txtF4i.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF4i.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF2i
      // 
      this._txtF2i.Location = new System.Drawing.Point(104, 56);
      this._txtF2i.Name = "_txtF2i";
      this._txtF2i.ReadOnly = true;
      this._txtF2i.Size = new System.Drawing.Size(36, 20);
      this._txtF2i.TabIndex = 19;
      this._txtF2i.Text = "";
      this._txtF2i.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF2i.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF3i
      // 
      this._txtF3i.Location = new System.Drawing.Point(144, 56);
      this._txtF3i.Name = "_txtF3i";
      this._txtF3i.ReadOnly = true;
      this._txtF3i.Size = new System.Drawing.Size(36, 20);
      this._txtF3i.TabIndex = 20;
      this._txtF3i.Text = "";
      this._txtF3i.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF3i.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF1i
      // 
      this._txtF1i.Location = new System.Drawing.Point(64, 56);
      this._txtF1i.Name = "_txtF1i";
      this._txtF1i.ReadOnly = true;
      this._txtF1i.Size = new System.Drawing.Size(36, 20);
      this._txtF1i.TabIndex = 18;
      this._txtF1i.Text = "";
      this._txtF1i.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF1i.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblIst
      // 
      this._lblIst.Location = new System.Drawing.Point(12, 56);
      this._lblIst.Name = "_lblIst";
      this._lblIst.Size = new System.Drawing.Size(40, 23);
      this._lblIst.TabIndex = 17;
      this._lblIst.Text = "Ist:";
      // 
      // _txtF16s
      // 
      this._txtF16s.Location = new System.Drawing.Point(664, 24);
      this._txtF16s.Name = "_txtF16s";
      this._txtF16s.Size = new System.Drawing.Size(36, 20);
      this._txtF16s.TabIndex = 16;
      this._txtF16s.Text = "";
      this._txtF16s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF16s.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF15s
      // 
      this._txtF15s.Location = new System.Drawing.Point(624, 24);
      this._txtF15s.Name = "_txtF15s";
      this._txtF15s.Size = new System.Drawing.Size(36, 20);
      this._txtF15s.TabIndex = 15;
      this._txtF15s.Text = "";
      this._txtF15s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF15s.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF14s
      // 
      this._txtF14s.Location = new System.Drawing.Point(584, 24);
      this._txtF14s.Name = "_txtF14s";
      this._txtF14s.Size = new System.Drawing.Size(36, 20);
      this._txtF14s.TabIndex = 14;
      this._txtF14s.Text = "";
      this._txtF14s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF14s.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF13s
      // 
      this._txtF13s.Location = new System.Drawing.Point(544, 24);
      this._txtF13s.Name = "_txtF13s";
      this._txtF13s.Size = new System.Drawing.Size(36, 20);
      this._txtF13s.TabIndex = 13;
      this._txtF13s.Text = "";
      this._txtF13s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF13s.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF12s
      // 
      this._txtF12s.Location = new System.Drawing.Point(504, 24);
      this._txtF12s.Name = "_txtF12s";
      this._txtF12s.Size = new System.Drawing.Size(36, 20);
      this._txtF12s.TabIndex = 12;
      this._txtF12s.Text = "";
      this._txtF12s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF12s.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF11s
      // 
      this._txtF11s.Location = new System.Drawing.Point(464, 24);
      this._txtF11s.Name = "_txtF11s";
      this._txtF11s.Size = new System.Drawing.Size(36, 20);
      this._txtF11s.TabIndex = 11;
      this._txtF11s.Text = "";
      this._txtF11s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF11s.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF10s
      // 
      this._txtF10s.Location = new System.Drawing.Point(424, 24);
      this._txtF10s.Name = "_txtF10s";
      this._txtF10s.Size = new System.Drawing.Size(36, 20);
      this._txtF10s.TabIndex = 10;
      this._txtF10s.Text = "";
      this._txtF10s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF10s.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF9s
      // 
      this._txtF9s.Location = new System.Drawing.Point(384, 24);
      this._txtF9s.Name = "_txtF9s";
      this._txtF9s.Size = new System.Drawing.Size(36, 20);
      this._txtF9s.TabIndex = 9;
      this._txtF9s.Text = "";
      this._txtF9s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF9s.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF8s
      // 
      this._txtF8s.Location = new System.Drawing.Point(344, 24);
      this._txtF8s.Name = "_txtF8s";
      this._txtF8s.Size = new System.Drawing.Size(36, 20);
      this._txtF8s.TabIndex = 8;
      this._txtF8s.Text = "";
      this._txtF8s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF8s.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF7s
      // 
      this._txtF7s.Location = new System.Drawing.Point(304, 24);
      this._txtF7s.Name = "_txtF7s";
      this._txtF7s.Size = new System.Drawing.Size(36, 20);
      this._txtF7s.TabIndex = 7;
      this._txtF7s.Text = "";
      this._txtF7s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF7s.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF6s
      // 
      this._txtF6s.Location = new System.Drawing.Point(264, 24);
      this._txtF6s.Name = "_txtF6s";
      this._txtF6s.Size = new System.Drawing.Size(36, 20);
      this._txtF6s.TabIndex = 6;
      this._txtF6s.Text = "";
      this._txtF6s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF6s.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF5s
      // 
      this._txtF5s.Location = new System.Drawing.Point(224, 24);
      this._txtF5s.Name = "_txtF5s";
      this._txtF5s.Size = new System.Drawing.Size(36, 20);
      this._txtF5s.TabIndex = 5;
      this._txtF5s.Text = "";
      this._txtF5s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF5s.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF4s
      // 
      this._txtF4s.Location = new System.Drawing.Point(184, 24);
      this._txtF4s.Name = "_txtF4s";
      this._txtF4s.Size = new System.Drawing.Size(36, 20);
      this._txtF4s.TabIndex = 4;
      this._txtF4s.Text = "";
      this._txtF4s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF4s.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF2s
      // 
      this._txtF2s.Location = new System.Drawing.Point(104, 24);
      this._txtF2s.Name = "_txtF2s";
      this._txtF2s.Size = new System.Drawing.Size(36, 20);
      this._txtF2s.TabIndex = 2;
      this._txtF2s.Text = "";
      this._txtF2s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF2s.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF3s
      // 
      this._txtF3s.Location = new System.Drawing.Point(144, 24);
      this._txtF3s.Name = "_txtF3s";
      this._txtF3s.Size = new System.Drawing.Size(36, 20);
      this._txtF3s.TabIndex = 3;
      this._txtF3s.Text = "";
      this._txtF3s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF3s.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtF1s
      // 
      this._txtF1s.Location = new System.Drawing.Point(64, 24);
      this._txtF1s.Name = "_txtF1s";
      this._txtF1s.Size = new System.Drawing.Size(36, 20);
      this._txtF1s.TabIndex = 1;
      this._txtF1s.Text = "";
      this._txtF1s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this._txtF1s.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblSoll
      // 
      this._lblSoll.Location = new System.Drawing.Point(12, 24);
      this._lblSoll.Name = "_lblSoll";
      this._lblSoll.Size = new System.Drawing.Size(40, 23);
      this._lblSoll.TabIndex = 0;
      this._lblSoll.Text = "Soll:";
      // 
      // _btnRead
      // 
      this._btnRead.Location = new System.Drawing.Point(212, 144);
      this._btnRead.Name = "_btnRead";
      this._btnRead.Size = new System.Drawing.Size(96, 23);
      this._btnRead.TabIndex = 3;
      this._btnRead.Text = "Read Data";
      this._btnRead.Click += new System.EventHandler(this._btnRead_Click);
      this._btnRead.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnWrite
      // 
      this._btnWrite.Location = new System.Drawing.Point(328, 144);
      this._btnWrite.Name = "_btnWrite";
      this._btnWrite.Size = new System.Drawing.Size(96, 23);
      this._btnWrite.TabIndex = 4;
      this._btnWrite.Text = "Daten schreiben";
      this._btnWrite.Click += new System.EventHandler(this._btnWrite_Click);
      this._btnWrite.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnCancel
      // 
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point(444, 144);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.Size = new System.Drawing.Size(96, 23);
      this._btnCancel.TabIndex = 5;
      this._btnCancel.Text = "Cancel";
      // 
      // _pgbTransfer
      // 
      this._pgbTransfer.Location = new System.Drawing.Point(212, 112);
      this._pgbTransfer.Name = "_pgbTransfer";
      this._pgbTransfer.Size = new System.Drawing.Size(327, 20);
      this._pgbTransfer.TabIndex = 1;
      // 
      // _ToolTip
      // 
      this._ToolTip.AutoPopDelay = 10000;
      this._ToolTip.InitialDelay = 500;
      this._ToolTip.ReshowDelay = 100;
      // 
      // _Timer
      // 
      this._Timer.Interval = 5000;
      this._Timer.Tick += new System.EventHandler(this._Timer_Tick);
      // 
      // _chkPeriodicUpdate
      // 
      this._chkPeriodicUpdate.Location = new System.Drawing.Point(16, 144);
      this._chkPeriodicUpdate.Name = "_chkPeriodicUpdate";
      this._chkPeriodicUpdate.Size = new System.Drawing.Size(160, 24);
      this._chkPeriodicUpdate.TabIndex = 2;
      this._chkPeriodicUpdate.Text = "Periodische Aktualisierung";
      this._chkPeriodicUpdate.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      this._chkPeriodicUpdate.CheckedChanged += new System.EventHandler(this._chkPeriodicUpdate_CheckedChanged);
      // 
      // ServiceMPForm
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.CancelButton = this._btnCancel;
      this.ClientSize = new System.Drawing.Size(742, 180);
      this.Controls.Add(this._chkPeriodicUpdate);
      this.Controls.Add(this._pgbTransfer);
      this.Controls.Add(this._btnCancel);
      this.Controls.Add(this._btnWrite);
      this.Controls.Add(this._btnRead);
      this.Controls.Add(this._gbFlow);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.HelpButton = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ServiceMPForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Service Control MP";
      this.Load += new System.EventHandler(this.ServiceForm_Load);
      this.Closed += new System.EventHandler(this.ServiceForm_Closed);
      this._gbFlow.ResumeLayout(false);
      this.ResumeLayout(false);

    }
		
    #endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void ServiceForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Resources
      this.Text                   = r.GetString ( "ServiceMP_Title" );          // "�bertragung der Multiplexer-Daten"
      
      this._gbFlow.Text           = r.GetString ( "ServiceMP_gbFlow" );         // "Flow data"
      this._lblSoll.Text          = r.GetString ( "ServiceMP_lblSoll" );        // "Soll:"
      this._lblIst.Text           = r.GetString ( "ServiceMP_lblIst" );         // "Ist:"

      this._chkPeriodicUpdate.Text= r.GetString ( "Service_chkPeriodicUpdate" );// "Periodische Aktualisierung"
      this._btnRead.Text          = r.GetString ( "Service_btnRead" );          // "Daten lesen"
      this._btnWrite.Text         = r.GetString ( "Service_btnWrite" );         // "Daten schreiben"
      this._btnCancel.Text        = r.GetString ( "Service_btnCancel" );        // "Abbruch"

      // Update the form
      UpdateData ();

      // Read in initially the service data
      _btnRead_Click (null, new System.EventArgs ());
    }

    /// <summary>
    /// 'Closed' event of the form
    /// </summary>
    private void ServiceForm_Closed(object sender, System.EventArgs e)
    {

      // Stop timer
      _Timer.Enabled = false;

      // Check: Is a Transfer process still in progress?
      if ( _bTransferInProgress ) 
      {
        // Yes:
        // Check: Did we already get the name of the script, that was lastly running?
        if ( _sLastScriptName.Length > 0 ) 
        {
          // Yes:

          // Finish the transfer
          _EndTransfer ();
        }
      }
    }
    
    /// <summary>
    /// 'Click' event of the '_btnRead' Button control
    /// </summary>
    /// <remarks>
    /// 1.
    /// Reads in (i.e. transfers from device to PC) the Multiplexer data and updates the control
    /// contents corr'ly
    /// 2.
    /// The mechanism of reading data from the device is the "chain" mechanism:
    /// The kick-off of a subsequent command takes place after the current command has completed.
    /// Updating a progress bar is done after each step.
    /// </remarks>
    private void _btnRead_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;

      // Check: Is a Transfer process still in progress?
      if ( _bTransferInProgress ) 
        return; // Yes

      // Check: Communication channel open?
      if ( !comm.IsOpen () )
        return; // No

      // Indicate that the Transfer process has begun.
      _bTransferInProgress = true;
        
      // TX: Script Current
      //    Parameter: none
      //    Device: Action - nothing; 
      //            Returns - Name of the script currently running on the IMS device
      CommMessage msg = new CommMessage (comm.msgScriptCurrent);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      
      // TX: Transfer Start
      //    Parameter: The transfer task to be performed
      //    Device: Action - Indicate, that the MP-Read transfer has begun; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgTransferStart );
      msg.Parameter = TransferTask.MP_Read.ToString ( "D" );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );
      
      // Init. progress bar
      this._pgbTransfer.Minimum = 0;
      this._pgbTransfer.Maximum = _anzDataStrings;
      this._pgbTransfer.Value = 0;
      _currDataType=0;

      // TX: Service MP Read (Initial)
      //    Parameter: none
      //    Device: Action - nothing
      //            Returns - The service data string for the given service data type from the device
      msg = new CommMessage (comm.msgServiceMPRead);
      msg.Parameter = _currDataType.ToString (); 
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
    }

    /// <summary>
    /// 'Click' event of the '_btnWrite' Button control
    /// </summary>
    /// <remarks>
    /// 1.
    /// Writes (i.e. transfers from PC to device) the Multiplexer data
    /// 2.
    /// There are 2 mechanisms of writing data to the device:
    /// a) The "chain" mechanism (as used here): The kick-off of a subsequent command 
    ///   takes place after the current command has answered. Updating a progress bar
    ///   is done after each step.
    /// b) The sending of all commands one after another (as done by the script transfer): 
    ///   Then by means of a timer the current message queue count can be revised and  
    ///   a progress bar can be updated corr'ly.
    /// </remarks>
    private void _btnWrite_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;

      // Check: Is a Transfer process still in progress?
      if ( _bTransferInProgress ) 
        return; // Yes

      // Check: Communication channel open?
      if ( !comm.IsOpen () )
        return; // No

      // Check the validity of the control contents
      if ( !_CheckControls() )
        return;

      // Update the ServiceData members
      _UpdateServiceData ( true );
        
      // Indicate that the Transfer process has begun.
      _bTransferInProgress = true;

      // TX: Script Current
      //    Parameter: none
      //    Device: Action - nothing; 
      //            Returns - Name of the script currently running on the IMS device
      CommMessage msg = new CommMessage (comm.msgScriptCurrent);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      
      // TX: Transfer Start
      //    Parameter: The transfer task to be performed
      //    Device: Action - Indicate, that the Service-Write transfer has begun; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgTransferStart );
      msg.Parameter = TransferTask.MP_Write.ToString ( "D" );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );
      
      // Init. progress bar
      this._pgbTransfer.Minimum = 0;
      this._pgbTransfer.Maximum = _anzDataStrings;
      this._pgbTransfer.Value = 0;
      _currDataType=0;

      // TX: Service MP Write (Initial)
      //    Parameter: none
      //    Device: Action - Actualizes the service data on the device 
      //            Returns - OK
      msg = new CommMessage (comm.msgServiceMPWrite);
      msg.Parameter = string.Format ( "{0},{1}", _currDataType, _arsServiceData[_currDataType] );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
    }

    /// <summary>
    /// 'HelpRequested' event of some controls
    /// </summary>
    /// <remarks>
    /// </remarks>
    private void _ctl_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Help requesting control
      Control requestingControl = (Control)sender;
      
      // Assign help text
      string s = "";
      
      // ---------------------------
      // Form level
      
      //   Button Read
      if      (requestingControl == this._btnRead)
        s = r.GetString ("Service_Help_btnRead");       // "Liest die Service-Daten vom Eeprom des Ger�ts."
        //   Button Write
      else if (requestingControl == this._btnWrite)
        s = r.GetString ("Service_Help_btnWrite");      // "Schreibt die Service-Daten auf den Eeprom des Ger�ts."
        //  CheckBox Periodic Update
      else if (requestingControl == this._chkPeriodicUpdate)
        s = r.GetString ("Service_Help_chkPeriodicUpdate"); // "Wenn markiert, wird ein Timer-gest�tztes periodisches Monitoring (Lesen) initialisiert."
      
      // ---------------------------
      // Flows

        // Flow - Soll values
      else if ( (requestingControl == this._txtF1s) ||
                (requestingControl == this._txtF2s) ||
                (requestingControl == this._txtF3s) ||
                (requestingControl == this._txtF4s) ||
                (requestingControl == this._txtF5s) ||
                (requestingControl == this._txtF6s) ||
                (requestingControl == this._txtF7s) ||
                (requestingControl == this._txtF8s) ||
                (requestingControl == this._txtF9s) ||
                (requestingControl == this._txtF10s) ||
                (requestingControl == this._txtF11s) ||
                (requestingControl == this._txtF12s) ||
                (requestingControl == this._txtF13s) ||
                (requestingControl == this._txtF14s) ||
                (requestingControl == this._txtF15s) ||
                (requestingControl == this._txtF16s))
        s = r.GetString ("ServiceMP_Help_txtFSoll");    // "Der Fluss-Sollwert f�r den entsprechenden Kanal (in [0, 1023])."
        // Flow - Ist values
      else if ( (requestingControl == this._txtF1i) ||
        (requestingControl == this._txtF2i) ||
        (requestingControl == this._txtF3i) ||
        (requestingControl == this._txtF4i) ||
        (requestingControl == this._txtF5i) ||
        (requestingControl == this._txtF6i) ||
        (requestingControl == this._txtF7i) ||
        (requestingControl == this._txtF8i) ||
        (requestingControl == this._txtF9i) ||
        (requestingControl == this._txtF10i) ||
        (requestingControl == this._txtF11i) ||
        (requestingControl == this._txtF12i) ||
        (requestingControl == this._txtF13i) ||
        (requestingControl == this._txtF14i) ||
        (requestingControl == this._txtF15i) ||
        (requestingControl == this._txtF16i))
        s = r.GetString ("ServiceMP_Help_txtFIst");     // "Der Fluss-Istwert f�r den entsprechenden Kanal (in [0, 1023])."

      // Show help
      this._ToolTip.SetToolTip (requestingControl, s);
      hlpevent.Handled=true;
    }
    
    /// <summary>
    /// 'CheckedChanged' event of the '_chkPeriodic' CheckBox control:
    /// If checked, a Timer-based periodic monitoring (Read) is initiated.
    /// </summary>
    private void _chkPeriodicUpdate_CheckedChanged(object sender, System.EventArgs e)
    {
      CheckBox chk = (CheckBox)sender;
      if ( chk.Checked )
      {
        // Start Timer-based periodic monitoring (Read)
        _Timer.Enabled = true;                        // Start
      }
      else
      {
        // Stop Timer-based periodic monitoring (Read)
        _Timer.Enabled = false;                       // Stop
      }
      
      // Update the form
      UpdateData ();
    }

    /// <summary>
    /// 'Tick' event of the '_Timer' control
    /// </summary>
    private void _Timer_Tick(object sender, System.EventArgs e)
    {
      // Check: Is a Transfer process still in progress?
      if ( _bTransferInProgress ) 
        return; // Yes
      
      // Read in the service data
      _btnRead_Click (null, new System.EventArgs ());
    }

    #endregion event handling

    #region members
    
    /// <summary>
    /// True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    bool _bShowWait = false;

    /// <summary>
    /// The Service Data string array
    /// </summary>
    string[] _arsServiceData = new string[_NROFSVCDATASTRINGS];  
    /// <summary>
    /// The number of strings in the Service Data string array 
    /// </summary>
    int _anzDataStrings = _NROFSVCDATASTRINGS;
    /// <summary>
    /// The Service Data type ( representing the indizes of the Service Data string array )
    /// </summary>
    int _currDataType = 0;

    /// <summary>
    /// Indicates, whether a Transfer process is curr'ly in progress ( true ) or not ( false )
    /// </summary>
    bool _bTransferInProgress = false;

    /// <summary>
    /// Name of the script, that was lastly running on the device
    /// </summary>
    string _sLastScriptName = string.Empty;
    
    #endregion members

    #region constants & enums

    // # of Service Data types (= # of different Service Data strings)
    const int _NROFSVCDATASTRINGS = 1;

    #endregion constants & enums

    #region methods
    
    /// <summary>
    /// Performs initialisation tasks
    /// </summary>
    private void Init ()
    {
      App app = App.Instance;
      Doc doc = app.Doc;

      // Init's
      // Flow controls
      if (doc.nMPChan < 16)
      {
        this._txtF9s.Enabled = false; this._txtF9s.Text = "0"; this._txtF9i.Text = "0";
        this._txtF10s.Enabled = false; this._txtF10s.Text = "0"; this._txtF10i.Text = "0";
        this._txtF11s.Enabled = false; this._txtF11s.Text = "0"; this._txtF11i.Text = "0";
        this._txtF12s.Enabled = false; this._txtF12s.Text = "0"; this._txtF12i.Text = "0";
        this._txtF13s.Enabled = false; this._txtF13s.Text = "0"; this._txtF13i.Text = "0";
        this._txtF14s.Enabled = false; this._txtF14s.Text = "0"; this._txtF14i.Text = "0";
        this._txtF15s.Enabled = false; this._txtF15s.Text = "0"; this._txtF15i.Text = "0";
        this._txtF16s.Enabled = false; this._txtF16s.Text = "0"; this._txtF16i.Text = "0";
      }
    }

    /// <summary>
    /// Ckecks the validity of the control contents
    /// </summary>
    /// <returns>True, if the control contents are valid; false otherwise</returns>
    Boolean _CheckControls()
    {
      object o;

      //-----------------------------------
      // Section: Flow data

      if ( !Check.Execute ( this._txtF1s, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o) )
        return false;
      if ( !Check.Execute ( this._txtF2s, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o) )
        return false;
      if ( !Check.Execute ( this._txtF3s, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o) )
        return false;
      if ( !Check.Execute ( this._txtF4s, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o) )
        return false;
      if ( !Check.Execute ( this._txtF5s, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o) )
        return false;
      if ( !Check.Execute ( this._txtF6s, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o) )
        return false;
      if ( !Check.Execute ( this._txtF7s, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o) )
        return false;
      if ( !Check.Execute ( this._txtF8s, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o) )
        return false;
      
      // Notes.
      //  Can be checked even if 'doc.nMPChan' = 8: These controls are init'ly disabled and set to 0.
      if ( !Check.Execute ( this._txtF9s, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o) )
        return false;
      if ( !Check.Execute ( this._txtF10s, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o) )
        return false;
      if ( !Check.Execute ( this._txtF11s, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o) )
        return false;
      if ( !Check.Execute ( this._txtF12s, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o) )
        return false;
      if ( !Check.Execute ( this._txtF13s, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o) )
        return false;
      if ( !Check.Execute ( this._txtF14s, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o) )
        return false;
      if ( !Check.Execute ( this._txtF15s, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o) )
        return false;
      if ( !Check.Execute ( this._txtF16s, CheckType.Int, CheckRelation.IN, 0, 1023, true, out o) )
        return false;
      
      // ready
      return true;
    }

    /// <summary>
    /// Updates the form
    /// </summary>
    void UpdateData () 
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;

      Boolean bEn = 
        ( doc.sVersion.Length > 0 ) && comm.IsOpen () && 
        ( _bTransferInProgress == false ) && ( this._chkPeriodicUpdate.Checked == false );

      // Button 'Read'
      this._btnRead.Enabled   = bEn; 
      // Button 'Write'
      this._btnWrite.Enabled  = bEn;   
      
      // Progress bar
      this._pgbTransfer.Enabled = doc.sVersion.Length > 0;
    }

    /// <summary>
    /// Updates the ServiceData string based on the control contents (DDX = true) or viceversa (DDX = false). 
    /// </summary>
    /// <param name="bDDX">The DDX direction</param>
    void _UpdateServiceData (Boolean bDDX)
    {
      App app = App.Instance;
      Doc doc = app.Doc;

      if ( bDDX )
      {
        // Controls -> Members ( WRITE (to device)):

        // Build the ServiceData string array
        // Notes:
        //  1.
        //  The Servicedata string has depending on the Servicedata type the following format:
        //
        //  Servicedata type        Servicedata string
        // ------------------------------------------------------------------------------------------------------------
        //  0 (flow data: 16)       "<F1s>TAB<F2s>TAB ... <F16s>TAB"
        //
        //  2.
        //  a) 
        //  The length of a whole parameter string should not exceed 'MAX_PAR_LEN' bytes. Because of the 
        //  'SvcDataType' token the Servicedata string should not exceed a length of ('MAX_PAR_LEN'-2) B.
        //  b)
        //  The Servicedata string should not exceed a length of 127 B (see device SW, Notes for 'sf_MPwrite()').
        //
        //  With respect to a) & b): length of the Servicedata string < Min ('MAX_PAR_LEN'-2, 127) -> This must be proofed for!
        //        
        //  Here we have appr. as an upper margin the following Byte #:
        //
        //  Servicedata type    Servicedata string length (B)
        // ------------------------------------------------------------------------------------------------------------
        //  0                   16 x (4+1)                  = 80

        
        // Section: Flow data
        string s = string.Format ("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t",
          this._txtF1s.Text,
          this._txtF2s.Text,
          this._txtF3s.Text,
          this._txtF4s.Text,
          this._txtF5s.Text,
          this._txtF6s.Text,
          this._txtF7s.Text,
          this._txtF8s.Text
        );
        _arsServiceData[0] = s;
        
        if (doc.nMPChan > 8)
        {
          s = string.Format ("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t",
            this._txtF9s.Text,
            this._txtF10s.Text,
            this._txtF11s.Text,
            this._txtF12s.Text,
            this._txtF13s.Text,
            this._txtF14s.Text,
            this._txtF15s.Text,
            this._txtF16s.Text
            );
          _arsServiceData[0] += s;
        }

      }
      else
      {
        // Members -> Controls ( READ (from device)):

        // Parse the ServiceData string array and Update the control contents
        // Notes:
        //  1.
        //  The Servicedata string has depending on the Servicedata type the following format:
        //
        //  Servicedata type        Servicedata string
        // ------------------------------------------------------------------------------------------------------------
        //  0 (Flow data: 16)       "<F1i>TAB<F2i>TAB ... <F16i>TAB"
        //
        //  2.
        //  Concerning the string length:
        //  Here we have appr. as an upper margin the following Byte #:
        //
        //  Servicedata type    Servicedata string length (B)
        // ------------------------------------------------------------------------------------------------------------
        //  0                   16 x (4+1)                  = 80

        // Section: Flow data
        string[] ars = _arsServiceData[0].Split ( '\t' );
        try 
        {
          this._txtF1i.Text  = ars[0];
          this._txtF2i.Text  = ars[1];
          this._txtF3i.Text  = ars[2];
          this._txtF4i.Text  = ars[3];
          this._txtF5i.Text  = ars[4];
          this._txtF6i.Text  = ars[5];
          this._txtF7i.Text  = ars[6];
          this._txtF8i.Text  = ars[7];
          
          if (doc.nMPChan > 8)
          {
            this._txtF9i.Text  = ars[8];
            this._txtF10i.Text  = ars[9];
            this._txtF11i.Text  = ars[10];
            this._txtF12i.Text  = ars[11];
            this._txtF13i.Text  = ars[12];
            this._txtF14i.Text  = ars[13];
            this._txtF15i.Text  = ars[14];
            this._txtF16i.Text  = ars[15];
          }
        }
        catch
        {
        }
      
      }
    }

    /// <summary>
    /// Finishes the transfer
    /// </summary>
    void _EndTransfer ( )
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Doc doc = app.Doc;

      // TX: Script Select
      //    Parameter: The name of the script to be executed
      //    Device: Action - Selects the script to be executed (here: script last used); 
      //            Returns - OK
      CommMessage msg = new CommMessage ( comm.msgScriptSelect );
      msg.Parameter = doc.ScriptNameToIdx ( _sLastScriptName ).ToString ();
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );

      // TX: Transfer Complete
      //    Parameter: none
      //    Device: Action - Indicate, that the transfer has completed; 
      //            Returns - OK
      msg = new CommMessage (comm.msgTransferComplete);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);

      // Indicate that the Transfer process has finished.
      _bTransferInProgress = false;

      // Reset: Value (position) of the Progressbar control
      this._pgbTransfer.Value = 0;
    }

    /// <summary>
    /// Performs actions in reaction of the receipt of a comm. message  
    /// </summary>
    /// <param name="msgRX">The comm. message</param>
    public void AfterRXCompleted(CommMessage msgRX)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // CD: Message response ID
      if ((msgRX.ResponseID == AppComm.CMID_TIMEOUT) || (msgRX.ResponseID == AppComm.CMID_STRLENERR))
      {
        // A comm. error (Timeout, ...) occurred:
      
        // Update the form
        UpdateData ();
      }

      else if (msgRX.ResponseID == AppComm.CMID_ANSWER)
      {
        // The message response is present:
        string sCmd = msgRX.CommandResponse;
        byte[] arbyPar = msgRX.ParameterResponse;

        // Get the parameter string from the parameter Byte array
        string sPar = Encoding.ASCII.GetString (arbyPar);
        // CD according to the message command
        switch ( sCmd ) 
        {
        
            //----------------------------------------------------------
            // Common Transfer messages
            //----------------------------------------------------------

          case "SCRCURRENT":
            // ScriptCurrent
            // RX: Name of the script currently running on the device   
            try
            {
              // Get the name of the script, that was lastly running on the IMS device
              _sLastScriptName = sPar;
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptCurrent->function failed" );
            }
            break;

          case "SCRSELECT":
            // ScriptSelect
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptSelect->function failed" );
            }
            break;
          
          case "TRNSTART":
            // Transfer Start
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgTransferStart->function failed" );
            }
            break;

          case "TRNCOMPLETE":
            // Transfer Complete
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgTransferComplete->function failed" );
            }
            break;

            //----------------------------------------------------------
            // Service-Read
            //----------------------------------------------------------

          case "MPREAD":
            // Service MP Read
            // RX: The MP data
            try
            {
              // Update the MP Service Data string array with the current service data string
              _arsServiceData[_currDataType] = sPar;
              // Update progress bar
              this._pgbTransfer.Value++;

              // Check, whether all service data strings have been read
              if ( _currDataType == _anzDataStrings - 1 )
              {
                // Yes:
                // Update the ServiceData controls
                _UpdateServiceData (false);
            
                // Finish the transfer
                _EndTransfer ();   
                break;
              }
            
              // No:
              // Update the Service Data type
              _currDataType++;
            
              // TX: Service MP Read ( Subsequent )
              //    Parameter: The Service Data type for the service data to be read
              //    Device: Action - nothing; 
              //            Returns - The service data string for the given service data type from the device
              CommMessage msg = new CommMessage ( comm.msgServiceMPRead );
              msg.Parameter = _currDataType.ToString ();
              msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
              comm.WriteMessage ( msg );
            }
            catch
            {
              Debug.WriteLine ( "OnMPRead->function failed" );
            }
            break;
        
            //----------------------------------------------------------
            // Service-Write
            //----------------------------------------------------------

          case "MPWRITE":
            // Service MP Write
            // RX: OK
            try
            {
              CommMessage msg;

              // Update progress bar
              this._pgbTransfer.Value++;

              // Check, whether all Service Data strings have been written
              if ( _currDataType == _anzDataStrings - 1 )
              {
                // Yes:
                // Finish the transfer
                _EndTransfer ();
                break; 
              }
            
              // No:
              // Update the Service Data type
              _currDataType++;

              // TX: Service MP Write ( Subsequent )
              //    Parameter: The Service Data type and the corr'ing Service Data string
              //    Device: Action - Actualizes the service data on the device 
              //            Returns - OK
              msg = new CommMessage ( comm.msgServiceMPWrite );
              msg.Parameter = string.Format ( "{0},{1}", _currDataType, _arsServiceData[_currDataType] );
              msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
              comm.WriteMessage ( msg );
            }
            catch
            {
              Debug.WriteLine ( "OnMPWrite->function failed" );
            }
            break;


        } // E - switch ( id )
    
        // Update the form
        UpdateData ();
      }//E - if (e.Message.Id = AppComm.CMID_ANSWER

    }
    
    #endregion methods

    #region properties

    /// <summary>
    ///  True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    public bool ShowWait
    {
      get { return _bShowWait; }
      set { _bShowWait = value; }
    }

    #endregion properties

  }
}
