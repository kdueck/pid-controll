using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace App
{
	/// <summary>
	/// Class PropertiesForm:
	/// Properties dialog
	/// </summary>
	public class PropertiesForm : System.Windows.Forms.Form
	{
    private System.Windows.Forms.TabControl _TabControl;
    private System.Windows.Forms.TabPage _TabPage_RS232;
    private System.Windows.Forms.TabPage _TabPage_Datafolder;
    private System.Windows.Forms.TabPage _TabPage_Program;
    private System.Windows.Forms.Button _btnOK;
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.Label _lblPort;
    private System.Windows.Forms.ComboBox _cbPorts;
    private System.Windows.Forms.Label _lblCommInfo;
    private System.Windows.Forms.Label _lblDatafolderInfo;
    private System.Windows.Forms.Label _lblFolder;
    private System.Windows.Forms.Button _btnSelectFolder;
    private System.Windows.Forms.Label _lblProgramInfo;
    private System.Windows.Forms.PictureBox _pbRS232;
    private System.Windows.Forms.PictureBox _pbDatafolder;
    private System.Windows.Forms.PictureBox _pbProgram;
    private System.Windows.Forms.TextBox _txtFolder;
    private System.Windows.Forms.TabPage _TabPage_Language;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.RadioButton _rbEn;
    private System.Windows.Forms.RadioButton _rbDe;
    private System.Windows.Forms.Label _lblLanguageInfo;
    private System.Windows.Forms.Label _lblLanguage;
    private System.Windows.Forms.Label _lblYUnit;
    private System.Windows.Forms.Panel _pnlYUnit;
    private System.Windows.Forms.RadioButton _rbAdc;
    private System.Windows.Forms.RadioButton _rbPercent;
    private System.Windows.Forms.Panel _pnlXUnit;
    private System.Windows.Forms.RadioButton _rbSeconds;
    private System.Windows.Forms.RadioButton _rbPoints;
    private System.Windows.Forms.Label _lblXUnit;
    private System.Windows.Forms.RadioButton _rbmV;
    private System.Windows.Forms.CheckBox _chkLangSyn;
    private System.Windows.Forms.ComboBox _cbBD;
    private System.Windows.Forms.Label _lblBD;
    private System.Windows.Forms.CheckBox _chkSDcardReader;
    private CheckBox _chkService;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Constructor
		/// </summary>
    public PropertiesForm()
		{
			InitializeComponent();
      // Init's
      Init ();
    }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PropertiesForm));
      this._TabControl = new System.Windows.Forms.TabControl();
      this._TabPage_RS232 = new System.Windows.Forms.TabPage();
      this._cbBD = new System.Windows.Forms.ComboBox();
      this._lblBD = new System.Windows.Forms.Label();
      this._pbRS232 = new System.Windows.Forms.PictureBox();
      this._lblCommInfo = new System.Windows.Forms.Label();
      this._cbPorts = new System.Windows.Forms.ComboBox();
      this._lblPort = new System.Windows.Forms.Label();
      this._TabPage_Program = new System.Windows.Forms.TabPage();
      this._chkService = new System.Windows.Forms.CheckBox();
      this._chkSDcardReader = new System.Windows.Forms.CheckBox();
      this._pnlXUnit = new System.Windows.Forms.Panel();
      this._rbSeconds = new System.Windows.Forms.RadioButton();
      this._rbPoints = new System.Windows.Forms.RadioButton();
      this._lblXUnit = new System.Windows.Forms.Label();
      this._pnlYUnit = new System.Windows.Forms.Panel();
      this._rbmV = new System.Windows.Forms.RadioButton();
      this._rbAdc = new System.Windows.Forms.RadioButton();
      this._rbPercent = new System.Windows.Forms.RadioButton();
      this._lblYUnit = new System.Windows.Forms.Label();
      this._pbProgram = new System.Windows.Forms.PictureBox();
      this._lblProgramInfo = new System.Windows.Forms.Label();
      this._TabPage_Datafolder = new System.Windows.Forms.TabPage();
      this._pbDatafolder = new System.Windows.Forms.PictureBox();
      this._btnSelectFolder = new System.Windows.Forms.Button();
      this._txtFolder = new System.Windows.Forms.TextBox();
      this._lblDatafolderInfo = new System.Windows.Forms.Label();
      this._lblFolder = new System.Windows.Forms.Label();
      this._TabPage_Language = new System.Windows.Forms.TabPage();
      this._chkLangSyn = new System.Windows.Forms.CheckBox();
      this.panel2 = new System.Windows.Forms.Panel();
      this._rbEn = new System.Windows.Forms.RadioButton();
      this._rbDe = new System.Windows.Forms.RadioButton();
      this._lblLanguageInfo = new System.Windows.Forms.Label();
      this._lblLanguage = new System.Windows.Forms.Label();
      this._btnOK = new System.Windows.Forms.Button();
      this._btnCancel = new System.Windows.Forms.Button();
      this._TabControl.SuspendLayout();
      this._TabPage_RS232.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this._pbRS232)).BeginInit();
      this._TabPage_Program.SuspendLayout();
      this._pnlXUnit.SuspendLayout();
      this._pnlYUnit.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this._pbProgram)).BeginInit();
      this._TabPage_Datafolder.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this._pbDatafolder)).BeginInit();
      this._TabPage_Language.SuspendLayout();
      this.panel2.SuspendLayout();
      this.SuspendLayout();
      // 
      // _TabControl
      // 
      this._TabControl.Controls.Add(this._TabPage_RS232);
      this._TabControl.Controls.Add(this._TabPage_Program);
      this._TabControl.Controls.Add(this._TabPage_Datafolder);
      this._TabControl.Controls.Add(this._TabPage_Language);
      this._TabControl.Location = new System.Drawing.Point(8, 8);
      this._TabControl.Name = "_TabControl";
      this._TabControl.SelectedIndex = 0;
      this._TabControl.Size = new System.Drawing.Size(372, 192);
      this._TabControl.TabIndex = 0;
      // 
      // _TabPage_RS232
      // 
      this._TabPage_RS232.Controls.Add(this._cbBD);
      this._TabPage_RS232.Controls.Add(this._lblBD);
      this._TabPage_RS232.Controls.Add(this._pbRS232);
      this._TabPage_RS232.Controls.Add(this._lblCommInfo);
      this._TabPage_RS232.Controls.Add(this._cbPorts);
      this._TabPage_RS232.Controls.Add(this._lblPort);
      this._TabPage_RS232.Location = new System.Drawing.Point(4, 22);
      this._TabPage_RS232.Name = "_TabPage_RS232";
      this._TabPage_RS232.Size = new System.Drawing.Size(364, 166);
      this._TabPage_RS232.TabIndex = 0;
      this._TabPage_RS232.Text = "RS232";
      // 
      // _cbBD
      // 
      this._cbBD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this._cbBD.Location = new System.Drawing.Point(120, 96);
      this._cbBD.Name = "_cbBD";
      this._cbBD.Size = new System.Drawing.Size(121, 21);
      this._cbBD.TabIndex = 4;
      // 
      // _lblBD
      // 
      this._lblBD.Location = new System.Drawing.Point(12, 96);
      this._lblBD.Name = "_lblBD";
      this._lblBD.Size = new System.Drawing.Size(100, 20);
      this._lblBD.TabIndex = 3;
      this._lblBD.Text = "Baudrate:";
      // 
      // _pbRS232
      // 
      this._pbRS232.Image = ((System.Drawing.Image)(resources.GetObject("_pbRS232.Image")));
      this._pbRS232.Location = new System.Drawing.Point(296, 8);
      this._pbRS232.Name = "_pbRS232";
      this._pbRS232.Size = new System.Drawing.Size(61, 115);
      this._pbRS232.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this._pbRS232.TabIndex = 3;
      this._pbRS232.TabStop = false;
      // 
      // _lblCommInfo
      // 
      this._lblCommInfo.Location = new System.Drawing.Point(8, 8);
      this._lblCommInfo.Name = "_lblCommInfo";
      this._lblCommInfo.Size = new System.Drawing.Size(280, 52);
      this._lblCommInfo.TabIndex = 0;
      this._lblCommInfo.Text = "Please select the communication properties. These properties will be activated af" +
    "ter restarting the communication.";
      // 
      // _cbPorts
      // 
      this._cbPorts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this._cbPorts.Location = new System.Drawing.Point(120, 68);
      this._cbPorts.Name = "_cbPorts";
      this._cbPorts.Size = new System.Drawing.Size(121, 21);
      this._cbPorts.TabIndex = 2;
      // 
      // _lblPort
      // 
      this._lblPort.Location = new System.Drawing.Point(12, 68);
      this._lblPort.Name = "_lblPort";
      this._lblPort.Size = new System.Drawing.Size(100, 20);
      this._lblPort.TabIndex = 1;
      this._lblPort.Text = "Port:";
      // 
      // _TabPage_Program
      // 
      this._TabPage_Program.Controls.Add(this._chkService);
      this._TabPage_Program.Controls.Add(this._chkSDcardReader);
      this._TabPage_Program.Controls.Add(this._pnlXUnit);
      this._TabPage_Program.Controls.Add(this._lblXUnit);
      this._TabPage_Program.Controls.Add(this._pnlYUnit);
      this._TabPage_Program.Controls.Add(this._lblYUnit);
      this._TabPage_Program.Controls.Add(this._pbProgram);
      this._TabPage_Program.Controls.Add(this._lblProgramInfo);
      this._TabPage_Program.Location = new System.Drawing.Point(4, 22);
      this._TabPage_Program.Name = "_TabPage_Program";
      this._TabPage_Program.Size = new System.Drawing.Size(364, 166);
      this._TabPage_Program.TabIndex = 2;
      this._TabPage_Program.Text = "Program";
      // 
      // _chkService
      // 
      this._chkService.Location = new System.Drawing.Point(10, 132);
      this._chkService.Name = "_chkService";
      this._chkService.Size = new System.Drawing.Size(124, 30);
      this._chkService.TabIndex = 5;
      this._chkService.Text = "New Service dialog style?";
      // 
      // _chkSDcardReader
      // 
      this._chkSDcardReader.Location = new System.Drawing.Point(165, 132);
      this._chkSDcardReader.Name = "_chkSDcardReader";
      this._chkSDcardReader.Size = new System.Drawing.Size(124, 30);
      this._chkSDcardReader.TabIndex = 6;
      this._chkSDcardReader.Text = "New SDcard reader style?";
      // 
      // _pnlXUnit
      // 
      this._pnlXUnit.Controls.Add(this._rbSeconds);
      this._pnlXUnit.Controls.Add(this._rbPoints);
      this._pnlXUnit.Location = new System.Drawing.Point(164, 92);
      this._pnlXUnit.Name = "_pnlXUnit";
      this._pnlXUnit.Size = new System.Drawing.Size(124, 32);
      this._pnlXUnit.TabIndex = 4;
      // 
      // _rbSeconds
      // 
      this._rbSeconds.Location = new System.Drawing.Point(62, 6);
      this._rbSeconds.Name = "_rbSeconds";
      this._rbSeconds.Size = new System.Drawing.Size(52, 20);
      this._rbSeconds.TabIndex = 1;
      this._rbSeconds.Text = "sec";
      // 
      // _rbPoints
      // 
      this._rbPoints.Location = new System.Drawing.Point(4, 6);
      this._rbPoints.Name = "_rbPoints";
      this._rbPoints.Size = new System.Drawing.Size(52, 20);
      this._rbPoints.TabIndex = 0;
      this._rbPoints.Text = "pts";
      // 
      // _lblXUnit
      // 
      this._lblXUnit.Location = new System.Drawing.Point(8, 96);
      this._lblXUnit.Name = "_lblXUnit";
      this._lblXUnit.Size = new System.Drawing.Size(152, 32);
      this._lblXUnit.TabIndex = 3;
      this._lblXUnit.Text = "Einheit der Abszissenachse f�r die Scananzeige:";
      // 
      // _pnlYUnit
      // 
      this._pnlYUnit.Controls.Add(this._rbmV);
      this._pnlYUnit.Controls.Add(this._rbAdc);
      this._pnlYUnit.Controls.Add(this._rbPercent);
      this._pnlYUnit.Location = new System.Drawing.Point(164, 40);
      this._pnlYUnit.Name = "_pnlYUnit";
      this._pnlYUnit.Size = new System.Drawing.Size(124, 48);
      this._pnlYUnit.TabIndex = 2;
      // 
      // _rbmV
      // 
      this._rbmV.Location = new System.Drawing.Point(4, 26);
      this._rbmV.Name = "_rbmV";
      this._rbmV.Size = new System.Drawing.Size(52, 20);
      this._rbmV.TabIndex = 2;
      this._rbmV.Text = "mV";
      // 
      // _rbAdc
      // 
      this._rbAdc.Location = new System.Drawing.Point(62, 4);
      this._rbAdc.Name = "_rbAdc";
      this._rbAdc.Size = new System.Drawing.Size(52, 20);
      this._rbAdc.TabIndex = 1;
      this._rbAdc.Text = "adc";
      // 
      // _rbPercent
      // 
      this._rbPercent.Location = new System.Drawing.Point(4, 4);
      this._rbPercent.Name = "_rbPercent";
      this._rbPercent.Size = new System.Drawing.Size(52, 20);
      this._rbPercent.TabIndex = 0;
      this._rbPercent.Text = "%";
      // 
      // _lblYUnit
      // 
      this._lblYUnit.Location = new System.Drawing.Point(8, 44);
      this._lblYUnit.Name = "_lblYUnit";
      this._lblYUnit.Size = new System.Drawing.Size(152, 32);
      this._lblYUnit.TabIndex = 1;
      this._lblYUnit.Text = "Einheit der Ordinatenachse f�r die Scananzeige:";
      // 
      // _pbProgram
      // 
      this._pbProgram.Image = ((System.Drawing.Image)(resources.GetObject("_pbProgram.Image")));
      this._pbProgram.Location = new System.Drawing.Point(296, 8);
      this._pbProgram.Name = "_pbProgram";
      this._pbProgram.Size = new System.Drawing.Size(61, 115);
      this._pbProgram.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this._pbProgram.TabIndex = 6;
      this._pbProgram.TabStop = false;
      // 
      // _lblProgramInfo
      // 
      this._lblProgramInfo.Location = new System.Drawing.Point(8, 8);
      this._lblProgramInfo.Name = "_lblProgramInfo";
      this._lblProgramInfo.Size = new System.Drawing.Size(280, 32);
      this._lblProgramInfo.TabIndex = 0;
      this._lblProgramInfo.Text = "Please select the properties for the representation in the program.";
      // 
      // _TabPage_Datafolder
      // 
      this._TabPage_Datafolder.Controls.Add(this._pbDatafolder);
      this._TabPage_Datafolder.Controls.Add(this._btnSelectFolder);
      this._TabPage_Datafolder.Controls.Add(this._txtFolder);
      this._TabPage_Datafolder.Controls.Add(this._lblDatafolderInfo);
      this._TabPage_Datafolder.Controls.Add(this._lblFolder);
      this._TabPage_Datafolder.Location = new System.Drawing.Point(4, 22);
      this._TabPage_Datafolder.Name = "_TabPage_Datafolder";
      this._TabPage_Datafolder.Size = new System.Drawing.Size(364, 166);
      this._TabPage_Datafolder.TabIndex = 1;
      this._TabPage_Datafolder.Text = "Datafolder";
      // 
      // _pbDatafolder
      // 
      this._pbDatafolder.Image = ((System.Drawing.Image)(resources.GetObject("_pbDatafolder.Image")));
      this._pbDatafolder.Location = new System.Drawing.Point(296, 8);
      this._pbDatafolder.Name = "_pbDatafolder";
      this._pbDatafolder.Size = new System.Drawing.Size(61, 115);
      this._pbDatafolder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this._pbDatafolder.TabIndex = 8;
      this._pbDatafolder.TabStop = false;
      // 
      // _btnSelectFolder
      // 
      this._btnSelectFolder.Image = ((System.Drawing.Image)(resources.GetObject("_btnSelectFolder.Image")));
      this._btnSelectFolder.Location = new System.Drawing.Point(266, 68);
      this._btnSelectFolder.Name = "_btnSelectFolder";
      this._btnSelectFolder.Size = new System.Drawing.Size(22, 18);
      this._btnSelectFolder.TabIndex = 2;
      this._btnSelectFolder.Click += new System.EventHandler(this._btnSelectFolder_Click);
      // 
      // _txtFolder
      // 
      this._txtFolder.Location = new System.Drawing.Point(8, 88);
      this._txtFolder.Name = "_txtFolder";
      this._txtFolder.ReadOnly = true;
      this._txtFolder.Size = new System.Drawing.Size(280, 20);
      this._txtFolder.TabIndex = 3;
      // 
      // _lblDatafolderInfo
      // 
      this._lblDatafolderInfo.Location = new System.Drawing.Point(8, 8);
      this._lblDatafolderInfo.Name = "_lblDatafolderInfo";
      this._lblDatafolderInfo.Size = new System.Drawing.Size(280, 52);
      this._lblDatafolderInfo.TabIndex = 0;
      this._lblDatafolderInfo.Text = "Please select the properties regarding the data access.";
      // 
      // _lblFolder
      // 
      this._lblFolder.Location = new System.Drawing.Point(8, 68);
      this._lblFolder.Name = "_lblFolder";
      this._lblFolder.Size = new System.Drawing.Size(100, 20);
      this._lblFolder.TabIndex = 1;
      this._lblFolder.Text = "Datafolder:";
      // 
      // _TabPage_Language
      // 
      this._TabPage_Language.Controls.Add(this._chkLangSyn);
      this._TabPage_Language.Controls.Add(this.panel2);
      this._TabPage_Language.Controls.Add(this._lblLanguageInfo);
      this._TabPage_Language.Controls.Add(this._lblLanguage);
      this._TabPage_Language.Location = new System.Drawing.Point(4, 22);
      this._TabPage_Language.Name = "_TabPage_Language";
      this._TabPage_Language.Size = new System.Drawing.Size(364, 166);
      this._TabPage_Language.TabIndex = 3;
      this._TabPage_Language.Text = "Language";
      // 
      // _chkLangSyn
      // 
      this._chkLangSyn.Location = new System.Drawing.Point(8, 92);
      this._chkLangSyn.Name = "_chkLangSyn";
      this._chkLangSyn.Size = new System.Drawing.Size(280, 24);
      this._chkLangSyn.TabIndex = 3;
      this._chkLangSyn.Text = "Automatische Sprachsynchronisation";
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this._rbEn);
      this.panel2.Controls.Add(this._rbDe);
      this.panel2.Location = new System.Drawing.Point(116, 60);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(172, 28);
      this.panel2.TabIndex = 2;
      // 
      // _rbEn
      // 
      this._rbEn.Location = new System.Drawing.Point(4, 4);
      this._rbEn.Name = "_rbEn";
      this._rbEn.Size = new System.Drawing.Size(76, 24);
      this._rbEn.TabIndex = 1;
      this._rbEn.Text = "English";
      // 
      // _rbDe
      // 
      this._rbDe.Location = new System.Drawing.Point(92, 4);
      this._rbDe.Name = "_rbDe";
      this._rbDe.Size = new System.Drawing.Size(76, 24);
      this._rbDe.TabIndex = 0;
      this._rbDe.Text = "German";
      // 
      // _lblLanguageInfo
      // 
      this._lblLanguageInfo.Location = new System.Drawing.Point(8, 8);
      this._lblLanguageInfo.Name = "_lblLanguageInfo";
      this._lblLanguageInfo.Size = new System.Drawing.Size(280, 52);
      this._lblLanguageInfo.TabIndex = 0;
      this._lblLanguageInfo.Text = "Please select the properties regarding the language.";
      // 
      // _lblLanguage
      // 
      this._lblLanguage.Location = new System.Drawing.Point(8, 68);
      this._lblLanguage.Name = "_lblLanguage";
      this._lblLanguage.Size = new System.Drawing.Size(100, 20);
      this._lblLanguage.TabIndex = 1;
      this._lblLanguage.Text = "Language:";
      // 
      // _btnOK
      // 
      this._btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
      this._btnOK.Location = new System.Drawing.Point(106, 208);
      this._btnOK.Name = "_btnOK";
      this._btnOK.Size = new System.Drawing.Size(75, 23);
      this._btnOK.TabIndex = 1;
      this._btnOK.Text = "OK";
      // 
      // _btnCancel
      // 
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point(210, 208);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.Size = new System.Drawing.Size(75, 23);
      this._btnCancel.TabIndex = 2;
      this._btnCancel.Text = "Cancel";
      // 
      // PropertiesForm
      // 
      this.AcceptButton = this._btnOK;
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.CancelButton = this._btnCancel;
      this.ClientSize = new System.Drawing.Size(390, 240);
      this.Controls.Add(this._btnCancel);
      this.Controls.Add(this._btnOK);
      this.Controls.Add(this._TabControl);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "PropertiesForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Properties";
      this.Closed += new System.EventHandler(this.PropertiesForm_Closed);
      this.Load += new System.EventHandler(this.PropertiesForm_Load);
      this._TabControl.ResumeLayout(false);
      this._TabPage_RS232.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this._pbRS232)).EndInit();
      this._TabPage_Program.ResumeLayout(false);
      this._pnlXUnit.ResumeLayout(false);
      this._pnlYUnit.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this._pbProgram)).EndInit();
      this._TabPage_Datafolder.ResumeLayout(false);
      this._TabPage_Datafolder.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this._pbDatafolder)).EndInit();
      this._TabPage_Language.ResumeLayout(false);
      this.panel2.ResumeLayout(false);
      this.ResumeLayout(false);

    }

		#endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void PropertiesForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
     
      // Resources
      this.Text                     = r.GetString ( "Properties_Title" );              // "Eigenschaften"
      this._btnOK.Text              = r.GetString ( "Properties_btnOK" );              // "OK"
      this._btnCancel.Text          = r.GetString ( "Properties_btnCancel" );          // "Abbruch"
      
      this._TabPage_RS232.Text      = r.GetString ( "Properties_TabPage_RS232" );      // "RS232-Schnittstelle"
      this._lblCommInfo.Text        = r.GetString ( "Properties_lblCommInfo" );        // "Bitte w�hlen Sie die Eigenschaften der Daten�bertragung aus. Diese Eigenschaften werden erst nach einem Neustart der Daten�bertragung aktiviert."
      this._lblPort.Text            = r.GetString ( "Properties_lblPort" );            // "Anschlu�:"
      this._lblBD.Text              = r.GetString ( "Properties_lblBD" );              // "Baudrate:"

      this._TabPage_Datafolder.Text = r.GetString ( "Properties_TabPage_Datafolder" ); // "Datenspeicherung"
      this._lblDatafolderInfo.Text  = r.GetString ( "Properties_lblDatafolderInfo" );  // "Bitte w�hlen Sie die Eigenschaften f�r den Zugriff auf Ihre Dateien aus."
      this._lblFolder.Text          = r.GetString ( "Properties_lblFolder" );          // "Datenverzeichnis:"
      
      this._TabPage_Program.Text    = r.GetString ( "Properties_TabPage_Program" );    // "Programm"
      this._lblProgramInfo.Text     = r.GetString ( "Properties_lblProgramInfo" );     // "Bitte w�hlen Sie die Eigenschaften f�r Darstellungen im Programm aus."
      this._lblYUnit.Text           = r.GetString ( "Properties_lblYUnit" );           // "Einheit der Ordinatenachse f�r die Scananzeige:"
      this._rbPercent.Text          = r.GetString ( "Properties_rbPercent" );          // "%"
      this._rbAdc.Text              = r.GetString ( "Properties_rbAdc" );              // "adc"
      this._rbmV.Text               = r.GetString ( "Properties_rbmV" );               // "mV"
      this._lblXUnit.Text           = r.GetString ( "Properties_lblXUnit" );           // "Einheit der Abszissenachse f�r die Scananzeige:"
      this._rbPoints.Text           = r.GetString ( "Properties_rbPoints" );           // "pts"
      this._rbSeconds.Text          = r.GetString ( "Properties_rbSeconds" );          // "sec"
      this._chkService.Text         = r.GetString ("Properties_chkService");           // "New Service dialog style?"
      this._chkSDcardReader.Text    = r.GetString ("Properties_chkSDcardReader");      // "New SDcard reader style?"
    
      this._TabPage_Language.Text   = r.GetString ( "Properties_TabPage_Language" );   // "Sprache"
      this._rbEn.Text               = r.GetString ( "Properties_rbEn" );               // "Englisch"
      this._rbDe.Text               = r.GetString ( "Properties_rbDe" );               // "Deutsch"
      this._lblLanguageInfo.Text    = r.GetString ( "Properties_lblLanguageInfo" );    // "Bitte w�hlen Sie die Spracheigenschaften aus."
      this._lblLanguage.Text        = r.GetString ( "Properties_lblLanguage" );        // "Sprache:"
      this._chkLangSyn.Text         = r.GetString ( "Properties_chkLangSyn" );         // "Automatische Sprachsynchronisation"
 

      //-------------------------------------------------------------
      // Tab RS232
      //-------------------------------------------------------------

      // The RS232 Tab is shown ONLY, if the device is NOT connected and NO script is running 
      m_bTabRS232Visible = !comm.IsOpen() &&  !doc.bRunning; 
      // Check: Show RS232 Tab?
      if (m_bTabRS232Visible)
      {
        // Yes:

        // Enumerate available communication ports
        ArrayList al = AppComm.GetAvailableComPorts ();
        _cbPorts.DataSource = al;

        // Check communication channel last used, if available
        if ( _cbPorts.Items.Count > 0 ) 
        {
          _cbPorts.SelectedIndex = 0;
          if ( app.Data.Communication.sCommChannel.Length > 0 ) 
          {
            if ( _cbPorts.Items.Contains ( app.Data.Communication.sCommChannel ) )
            {
              _cbPorts.SelectedItem = app.Data.Communication.sCommChannel;
            }
          }
        }

        // Baudrate
        this._cbBD.SelectedItem = app.Data.Communication.dwBaudRate;
      }
      else
      {
        // No:
        // Remove the Tab from the TabControls 'TabPages' collection (and hence hide it)
        this._TabControl.TabPages.Remove (this._TabPage_RS232);
      }

      //-------------------------------------------------------------
      // Tab Datafolder
      //-------------------------------------------------------------

      // Display data folder last used, if available
      if ( app.Data.Common.sDataFolder.Length > 0 ) 
      {
        this._txtFolder.Text = app.Data.Common.sDataFolder;
      }

      //-------------------------------------------------------------
      // Tab Program
      //-------------------------------------------------------------

      // Check YUnit last used
      if      ( app.Data.Program.eYUnit == AppData.ProgramData.YUnits.Percent )
        this._rbPercent.Checked = true;
      else if ( app.Data.Program.eYUnit == AppData.ProgramData.YUnits.Adc )
        this._rbAdc.Checked = true;
      else
        this._rbmV.Checked = true;

      // Check XUnit last used
      if ( app.Data.Program.eXUnit == AppData.ProgramData.XUnits.Points )
        this._rbPoints.Checked = true;
      else
        this._rbSeconds.Checked = true;

      // New Service dialog style?
      this._chkService.Checked = app.Data.Program.bServiceNew;
      // New SDcard reader style?
      this._chkSDcardReader.Checked = app.Data.Program.bSDcardReaderNew;
      
      //-------------------------------------------------------------
      // Tab Language
      //-------------------------------------------------------------

      // Check Language last used
      if ( app.Data.Language.eLanguage == AppData.LanguageData.Languages.en )
        this._rbEn.Checked = true;
      else
        this._rbDe.Checked = true;
      // Check language synchronisation
      this._chkLangSyn.Checked = app.Data.Language.bLangSyn;
    
    }

    /// <summary>
    /// 'Closed' event of the form
    /// </summary>
    private void PropertiesForm_Closed(object sender, System.EventArgs e)
    {
      if ( this.DialogResult == DialogResult.OK ) 
      {
        App app = App.Instance;

        //-------------------------------------------------------------
        // Tab RS232
        //-------------------------------------------------------------
        
        // Check: Is the RS232 Tab shown?
        if (m_bTabRS232Visible)
        {
          // Yes:
          // Selected communication port
          if ( null != this._cbPorts.SelectedItem )
            app.Data.Communication.sCommChannel = this._cbPorts.SelectedItem.ToString();

          // Baudrate
          if ( null != this._cbBD.SelectedItem )
            app.Data.Communication.dwBaudRate = (UInt32) this._cbBD.SelectedItem;
        }

        //-------------------------------------------------------------
        // Tab Datafolder
        //-------------------------------------------------------------

        // Selected data folder
        app.Data.Common.sDataFolder = this._txtFolder.Text;
        
        //-------------------------------------------------------------
        // Tab Program
        //-------------------------------------------------------------
       
        // Selected YUnit
        if ( this._rbPercent.Checked == true )
          app.Data.Program.eYUnit = AppData.ProgramData.YUnits.Percent;
        else if ( this._rbAdc.Checked == true )
          app.Data.Program.eYUnit = AppData.ProgramData.YUnits.Adc;
        else
          app.Data.Program.eYUnit = AppData.ProgramData.YUnits.mV;
 
        // Selected XUnit
        if ( this._rbPoints.Checked == true )
          app.Data.Program.eXUnit = AppData.ProgramData.XUnits.Points;
        else
          app.Data.Program.eXUnit = AppData.ProgramData.XUnits.Seconds;

        // New Service dialog style?
        app.Data.Program.bServiceNew = this._chkService.Checked;
        // New SDcard reader style?
        app.Data.Program.bSDcardReaderNew = this._chkSDcardReader.Checked;
        
        //-------------------------------------------------------------
        // Tab Language
        //-------------------------------------------------------------
       
        // Selected Language
        if ( this._rbEn.Checked == true )
          app.Data.Language.eLanguage = AppData.LanguageData.Languages.en;
        else
          app.Data.Language.eLanguage = AppData.LanguageData.Languages.de;
        // Language synchronisation
        app.Data.Language.bLangSyn = this._chkLangSyn.Checked;
     
        
        // Write app data: comm. port, data folder, display unit, language
        app.Data.Write ();
      }
    }

    /// <summary>
    /// 'Click' event of the _btnSelectFolder control
    /// </summary>
    private void _btnSelectFolder_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // FolderBrowser dialog
      FolderBrowserDialog fbd = new FolderBrowserDialog ();

      fbd.Description = r.GetString ( "Properties_Txt_SelectDataFolder" ); // "W�hlen Sie den Ordner zur Speicherung der Daten aus"
      fbd.ShowNewFolderButton = true;
      fbd.SelectedPath = this._txtFolder.Text;

      DialogResult result = fbd.ShowDialog();
      if( result == DialogResult.OK )
      {
        // Overtake the data folder
        this._txtFolder.Text = fbd.SelectedPath;
      }
      fbd.Dispose ();
    }

    #endregion // event handling

    #region members
    
    /// <summary>
    /// Indicates, whether the RS232 Tab is shown (true) or not (false)
    /// </summary>
    bool m_bTabRS232Visible = false;      

    #endregion // members

    #region methods
 
    /// <summary>
    /// Init's the form
    /// </summary>
    void Init ()
    {
      App app = App.Instance;

      //-------------------------------------------------------------
      // Tab RS232
      //-------------------------------------------------------------

      // Baudrate CB
      //  Items
      this._cbBD.Items.AddRange ( new object[] { 
                                                 AppData.CommunicationData.CBR_115200, 
                                                 AppData.CommunicationData.CBR_57600 
                                               } 
        );
      //  Visibility: Hide it (Reason: We work here with the 'one & only' BR 115200)
      this._cbBD.Visible = false;
      this._lblBD.Visible = false;

      //-------------------------------------------------------------
      // Tab Program
      //-------------------------------------------------------------

      // Ordinate scaling 'mV':
      // This radiobutton must be hidden due to unclear conversion into the mV scale. 
      this._rbmV.Visible = false;    
    }
    
    #endregion // methods
 
  }
}
