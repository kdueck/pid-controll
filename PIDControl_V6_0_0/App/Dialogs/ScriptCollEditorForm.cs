using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace App
{
	/// <summary>
	/// Class ScriptCollEditorForm:
	/// Script collection editor dialog
	/// </summary>
	/// <remarks>
  /// The following size/length definitions are used in this code (GSM) and in the device code:
  /// 
  /// Description                               GSM code                            Device code
  /// --------------------------------------------------------------------------------------------------
  /// (Scripts)
  /// Max. # of scripts per config. file        MAX_SCRIPT_NUMBER=10                MAX_NUM_SCRIPTS=10
  /// Max. length of a script name              MAX_SCRIPT_NAMELENGTH=29            MAX_SCRIPT_NAMELENGTH=29
  /// 
  /// (Script window section)
  /// Max. # of substance windows per script    MAX_SCRIPT_WINDOWS=16               GSM_MAX_WINDOWS=16
  /// Max. length of a substance name           MAX_SCRIPT_WINDOWSUBSTANCENAME=15   MAX_SCRIPT_WINDOWSUBSTANCENAME=15
  /// 
  /// (Script command section)
  /// Max. # of script cmd's per script         MAX_SCRIPT_EVENTS=64                GSM_MAX_NUM_EVENTS=64
  /// Max. length of a script cmd identifier    MAX_SCRIPT_EVENTCMDSIZE=15          (not required)
  /// Max. # of script cmd parameters           MAX_SCREVT_PARAMS=5                 (N.N.)
  /// --------------------------------------------------------------------------------------------------
  /// </remarks>
  public class ScriptCollEditorForm : System.Windows.Forms.Form
  {
    private System.Windows.Forms.GroupBox _gbScript;
    private System.Windows.Forms.ColumnHeader colName;
    private System.Windows.Forms.Button _btnSaveEnc;
    private System.Windows.Forms.Button _btnLoadEnc;
    private System.Windows.Forms.Button _btnScript_Remove;
    private System.Windows.Forms.Button _btnScript_Add;
    private System.Windows.Forms.ToolTip _ToolTip;
    private System.Windows.Forms.ListView _lvScripts;
    private System.Windows.Forms.GroupBox _gbComments;
    private System.Windows.Forms.Label _lblCmt_ScrColl;
    private System.Windows.Forms.TextBox _txtCmt_ScrColl;
    private System.Windows.Forms.GroupBox _gbScrBinding;
    private System.Windows.Forms.TextBox _txtScrBinding;
    private System.Windows.Forms.Button _btnScript_Edit;
    private System.Windows.Forms.Timer _Timer;
    private System.Windows.Forms.Label _lblFilename;
    private System.Windows.Forms.TextBox _txtFilename;
    private System.Windows.Forms.Button _btnNew;
    private System.Windows.Forms.Button _btnDelete;
    private GroupBox _gbEncrypted;
    private GroupBox _gbPlainText;
    private Button _btnLoadPlainText;
    private Button _btnSavePlainText;
    private System.ComponentModel.IContainer components;

    /// <summary>
    /// Constructor
    /// </summary>
    public ScriptCollEditorForm()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this._btnSaveEnc = new System.Windows.Forms.Button();
      this._gbScript = new System.Windows.Forms.GroupBox();
      this._btnScript_Edit = new System.Windows.Forms.Button();
      this._lvScripts = new System.Windows.Forms.ListView();
      this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this._btnScript_Add = new System.Windows.Forms.Button();
      this._btnScript_Remove = new System.Windows.Forms.Button();
      this._btnLoadEnc = new System.Windows.Forms.Button();
      this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
      this._gbComments = new System.Windows.Forms.GroupBox();
      this._txtCmt_ScrColl = new System.Windows.Forms.TextBox();
      this._lblCmt_ScrColl = new System.Windows.Forms.Label();
      this._gbScrBinding = new System.Windows.Forms.GroupBox();
      this._txtScrBinding = new System.Windows.Forms.TextBox();
      this._Timer = new System.Windows.Forms.Timer(this.components);
      this._lblFilename = new System.Windows.Forms.Label();
      this._txtFilename = new System.Windows.Forms.TextBox();
      this._btnNew = new System.Windows.Forms.Button();
      this._btnDelete = new System.Windows.Forms.Button();
      this._gbEncrypted = new System.Windows.Forms.GroupBox();
      this._gbPlainText = new System.Windows.Forms.GroupBox();
      this._btnLoadPlainText = new System.Windows.Forms.Button();
      this._btnSavePlainText = new System.Windows.Forms.Button();
      this._gbScript.SuspendLayout();
      this._gbComments.SuspendLayout();
      this._gbScrBinding.SuspendLayout();
      this._gbEncrypted.SuspendLayout();
      this._gbPlainText.SuspendLayout();
      this.SuspendLayout();
      // 
      // _btnSaveEnc
      // 
      this._btnSaveEnc.Location = new System.Drawing.Point(98, 19);
      this._btnSaveEnc.Name = "_btnSaveEnc";
      this._btnSaveEnc.Size = new System.Drawing.Size(80, 23);
      this._btnSaveEnc.TabIndex = 1;
      this._btnSaveEnc.Text = "Speichern ...";
      this._btnSaveEnc.Click += new System.EventHandler(this._btnSaveEnc_Click);
      this._btnSaveEnc.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _gbScript
      // 
      this._gbScript.Controls.Add(this._btnScript_Edit);
      this._gbScript.Controls.Add(this._lvScripts);
      this._gbScript.Controls.Add(this._btnScript_Add);
      this._gbScript.Controls.Add(this._btnScript_Remove);
      this._gbScript.Location = new System.Drawing.Point(8, 46);
      this._gbScript.Name = "_gbScript";
      this._gbScript.Size = new System.Drawing.Size(352, 196);
      this._gbScript.TabIndex = 3;
      this._gbScript.TabStop = false;
      this._gbScript.Text = "Sc&ripts (Script collection)";
      // 
      // _btnScript_Edit
      // 
      this._btnScript_Edit.Location = new System.Drawing.Point(264, 72);
      this._btnScript_Edit.Name = "_btnScript_Edit";
      this._btnScript_Edit.Size = new System.Drawing.Size(76, 23);
      this._btnScript_Edit.TabIndex = 2;
      this._btnScript_Edit.Text = "Bearbeiten";
      this._btnScript_Edit.Click += new System.EventHandler(this._btnScript_Edit_Click);
      this._btnScript_Edit.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lvScripts
      // 
      this._lvScripts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colName});
      this._lvScripts.FullRowSelect = true;
      this._lvScripts.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
      this._lvScripts.HideSelection = false;
      this._lvScripts.Location = new System.Drawing.Point(8, 20);
      this._lvScripts.MultiSelect = false;
      this._lvScripts.Name = "_lvScripts";
      this._lvScripts.Size = new System.Drawing.Size(248, 168);
      this._lvScripts.TabIndex = 0;
      this._lvScripts.UseCompatibleStateImageBehavior = false;
      this._lvScripts.View = System.Windows.Forms.View.Details;
      this._lvScripts.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      this._lvScripts.DoubleClick += new System.EventHandler(this._lvScripts_DoubleClick);
      // 
      // colName
      // 
      this.colName.Text = "Name";
      this.colName.Width = 300;
      // 
      // _btnScript_Add
      // 
      this._btnScript_Add.Location = new System.Drawing.Point(264, 40);
      this._btnScript_Add.Name = "_btnScript_Add";
      this._btnScript_Add.Size = new System.Drawing.Size(76, 23);
      this._btnScript_Add.TabIndex = 1;
      this._btnScript_Add.Text = "Hinzufügen";
      this._btnScript_Add.Click += new System.EventHandler(this._btnScript_Add_Click);
      this._btnScript_Add.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnScript_Remove
      // 
      this._btnScript_Remove.Location = new System.Drawing.Point(264, 104);
      this._btnScript_Remove.Name = "_btnScript_Remove";
      this._btnScript_Remove.Size = new System.Drawing.Size(76, 23);
      this._btnScript_Remove.TabIndex = 3;
      this._btnScript_Remove.Text = "Remove";
      this._btnScript_Remove.Click += new System.EventHandler(this._btnScript_Remove_Click);
      this._btnScript_Remove.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnLoadEnc
      // 
      this._btnLoadEnc.Location = new System.Drawing.Point(8, 19);
      this._btnLoadEnc.Name = "_btnLoadEnc";
      this._btnLoadEnc.Size = new System.Drawing.Size(80, 23);
      this._btnLoadEnc.TabIndex = 0;
      this._btnLoadEnc.Text = "Load ...";
      this._btnLoadEnc.Click += new System.EventHandler(this._btnLoadEnc_Click);
      this._btnLoadEnc.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _ToolTip
      // 
      this._ToolTip.AutoPopDelay = 10000;
      this._ToolTip.InitialDelay = 500;
      this._ToolTip.ReshowDelay = 100;
      // 
      // _gbComments
      // 
      this._gbComments.Controls.Add(this._txtCmt_ScrColl);
      this._gbComments.Controls.Add(this._lblCmt_ScrColl);
      this._gbComments.Location = new System.Drawing.Point(368, 98);
      this._gbComments.Name = "_gbComments";
      this._gbComments.Size = new System.Drawing.Size(208, 144);
      this._gbComments.TabIndex = 5;
      this._gbComments.TabStop = false;
      this._gbComments.Text = "&Comments";
      // 
      // _txtCmt_ScrColl
      // 
      this._txtCmt_ScrColl.Location = new System.Drawing.Point(8, 44);
      this._txtCmt_ScrColl.Multiline = true;
      this._txtCmt_ScrColl.Name = "_txtCmt_ScrColl";
      this._txtCmt_ScrColl.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this._txtCmt_ScrColl.Size = new System.Drawing.Size(192, 92);
      this._txtCmt_ScrColl.TabIndex = 1;
      this._txtCmt_ScrColl.WordWrap = false;
      this._txtCmt_ScrColl.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblCmt_ScrColl
      // 
      this._lblCmt_ScrColl.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblCmt_ScrColl.Location = new System.Drawing.Point(8, 20);
      this._lblCmt_ScrColl.Name = "_lblCmt_ScrColl";
      this._lblCmt_ScrColl.Size = new System.Drawing.Size(192, 23);
      this._lblCmt_ScrColl.TabIndex = 0;
      this._lblCmt_ScrColl.Text = "Script file:";
      // 
      // _gbScrBinding
      // 
      this._gbScrBinding.Controls.Add(this._txtScrBinding);
      this._gbScrBinding.Location = new System.Drawing.Point(368, 46);
      this._gbScrBinding.Name = "_gbScrBinding";
      this._gbScrBinding.Size = new System.Drawing.Size(208, 48);
      this._gbScrBinding.TabIndex = 4;
      this._gbScrBinding.TabStop = false;
      this._gbScrBinding.Text = "Script file &binding to device no.";
      // 
      // _txtScrBinding
      // 
      this._txtScrBinding.Location = new System.Drawing.Point(8, 20);
      this._txtScrBinding.Name = "_txtScrBinding";
      this._txtScrBinding.Size = new System.Drawing.Size(192, 20);
      this._txtScrBinding.TabIndex = 0;
      this._txtScrBinding.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _Timer
      // 
      this._Timer.Interval = 200;
      this._Timer.Tick += new System.EventHandler(this._Timer_Tick);
      // 
      // _lblFilename
      // 
      this._lblFilename.Location = new System.Drawing.Point(8, 8);
      this._lblFilename.Name = "_lblFilename";
      this._lblFilename.Size = new System.Drawing.Size(72, 18);
      this._lblFilename.TabIndex = 1;
      this._lblFilename.Text = "Script&datei:";
      // 
      // _txtFilename
      // 
      this._txtFilename.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtFilename.Location = new System.Drawing.Point(84, 8);
      this._txtFilename.Multiline = true;
      this._txtFilename.Name = "_txtFilename";
      this._txtFilename.ReadOnly = true;
      this._txtFilename.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this._txtFilename.Size = new System.Drawing.Size(484, 32);
      this._txtFilename.TabIndex = 2;
      this._txtFilename.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnNew
      // 
      this._btnNew.Location = new System.Drawing.Point(404, 269);
      this._btnNew.Name = "_btnNew";
      this._btnNew.Size = new System.Drawing.Size(80, 23);
      this._btnNew.TabIndex = 7;
      this._btnNew.Text = "&New";
      this._btnNew.Click += new System.EventHandler(this._btnNew_Click);
      this._btnNew.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnDelete
      // 
      this._btnDelete.Location = new System.Drawing.Point(494, 269);
      this._btnDelete.Name = "_btnDelete";
      this._btnDelete.Size = new System.Drawing.Size(80, 23);
      this._btnDelete.TabIndex = 8;
      this._btnDelete.Text = "D&elete";
      this._btnDelete.Click += new System.EventHandler(this._btnDelete_Click);
      this._btnDelete.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _gbEncrypted
      // 
      this._gbEncrypted.Controls.Add(this._btnLoadEnc);
      this._gbEncrypted.Controls.Add(this._btnSaveEnc);
      this._gbEncrypted.Location = new System.Drawing.Point(204, 250);
      this._gbEncrypted.Name = "_gbEncrypted";
      this._gbEncrypted.Size = new System.Drawing.Size(186, 50);
      this._gbEncrypted.TabIndex = 6;
      this._gbEncrypted.TabStop = false;
      this._gbEncrypted.Text = "&Encrypted data";
      // 
      // _gbPlainText
      // 
      this._gbPlainText.Controls.Add(this._btnLoadPlainText);
      this._gbPlainText.Controls.Add(this._btnSavePlainText);
      this._gbPlainText.Location = new System.Drawing.Point(8, 250);
      this._gbPlainText.Name = "_gbPlainText";
      this._gbPlainText.Size = new System.Drawing.Size(186, 50);
      this._gbPlainText.TabIndex = 0;
      this._gbPlainText.TabStop = false;
      this._gbPlainText.Text = "Plain &text data";
      // 
      // _btnLoadPlainText
      // 
      this._btnLoadPlainText.Location = new System.Drawing.Point(8, 19);
      this._btnLoadPlainText.Name = "_btnLoadPlainText";
      this._btnLoadPlainText.Size = new System.Drawing.Size(80, 23);
      this._btnLoadPlainText.TabIndex = 0;
      this._btnLoadPlainText.Text = "Load ...";
      this._btnLoadPlainText.Click += new System.EventHandler(this._btnLoadPlainText_Click);
      this._btnLoadPlainText.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnSavePlainText
      // 
      this._btnSavePlainText.Location = new System.Drawing.Point(98, 19);
      this._btnSavePlainText.Name = "_btnSavePlainText";
      this._btnSavePlainText.Size = new System.Drawing.Size(80, 23);
      this._btnSavePlainText.TabIndex = 1;
      this._btnSavePlainText.Text = "Speichern ...";
      this._btnSavePlainText.Click += new System.EventHandler(this._btnSavePlainText_Click);
      this._btnSavePlainText.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // ScriptCollEditorForm
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.ClientSize = new System.Drawing.Size(582, 310);
      this.Controls.Add(this._gbPlainText);
      this.Controls.Add(this._gbEncrypted);
      this.Controls.Add(this._btnDelete);
      this.Controls.Add(this._btnNew);
      this.Controls.Add(this._txtFilename);
      this.Controls.Add(this._lblFilename);
      this.Controls.Add(this._gbScrBinding);
      this.Controls.Add(this._gbComments);
      this.Controls.Add(this._gbScript);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.HelpButton = true;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ScriptCollEditorForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Script  editor";
      this.Closed += new System.EventHandler(this.ScriptCollEditorForm_Closed);
      this.Load += new System.EventHandler(this.ScriptCollEditorForm_Load);
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ServiceExtForm_KeyPress);
      this._gbScript.ResumeLayout(false);
      this._gbComments.ResumeLayout(false);
      this._gbComments.PerformLayout();
      this._gbScrBinding.ResumeLayout(false);
      this._gbScrBinding.PerformLayout();
      this._gbEncrypted.ResumeLayout(false);
      this._gbPlainText.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void ScriptCollEditorForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      try
      {
        // Resources
        this.Text                   = r.GetString ( "ScriptCollEditor_Title" );         // "Script editor"   

        this._gbPlainText.Text      = r.GetString ( "ScriptCollEditor_gbPlainText");    // "Plain &text data"
        this._btnLoadPlainText.Text = r.GetString ( "ScriptCollEditor_btnLoad");        // "Laden ..."
        this._btnSavePlainText.Text = r.GetString ( "ScriptCollEditor_btnSave");        // "Speichern ..."    
        this._gbEncrypted.Text      = r.GetString ( "ScriptCollEditor_gbEncrypted");    // "Encr&ypted data"
        this._btnLoadEnc.Text       = r.GetString ( "ScriptCollEditor_btnLoad");        // "Laden ..."
        this._btnSaveEnc.Text       = r.GetString ( "ScriptCollEditor_btnSave" );       // "Speichern ..."    
        this._btnNew.Text           = r.GetString ( "ScriptCollEditor_btnNew" );        // "&Neu"
        this._btnDelete.Text        = r.GetString ( "ScriptCollEditor_btnDelete" );     // "Lösch&en"
       
        this._lblFilename.Text      = r.GetString ( "ScriptCollEditor_lblFilename" );   // "Script&datei"

        this._gbScript.Text         = r.GetString ( "ScriptCollEditor_gbScript" );      // "S&cripte in Datei (Scriptkollektion)"
        this.colName.Text           = r.GetString ( "ScriptCollEditor_colName" );       // "Name"
        this._btnScript_Add.Text    = r.GetString ( "ScriptCollEditor_btnScript_Add" ); // "Hinzufügen"  
        this._btnScript_Edit.Text   = r.GetString ( "ScriptCollEditor_btnScript_Edit" );// "Bearbeiten"  
        this._btnScript_Remove.Text = r.GetString ( "ScriptCollEditor_btnScript_Remove" );  // "Entfernen" 
      
        this._gbScrBinding.Text     = r.GetString ( "ScriptCollEditor_gbScrBinding" );  // "Script file &binding to device no."

        this._gbComments.Text       = r.GetString ( "ScriptCollEditor_gbComments" );    // "&Kommentare"
        this._lblCmt_ScrColl.Text   = r.GetString ( "ScriptCollEditor_lblCmt_ScrColl" );// "Scriptkollektion:"
      
      }
      catch
      {
      }

      // Control  initialisation
      _InitControls ();

      // Timer
      _Timer.Enabled = true;
    }

    /// <summary>
    /// 'Closed' event of the form
    /// </summary>
    private void ScriptCollEditorForm_Closed(object sender, System.EventArgs e)
    {
      // Timer
      _Timer.Enabled = false;
    }
    
    /// <summary>
    /// 'KeyPress' event of the form
    /// </summary>
    /// <remarks>
    /// In order to close the form by means of ESC key pressing, the form property 'KeyPreview' must be set
    /// to 'True'.
    /// </remarks>
    private void ServiceExtForm_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
    {
      // Close the form on pressing 'Escape'
      if ( e.KeyChar == (char) Keys.Escape )
        this.Close ();
    }
    
    /// <summary>
    /// 'Click' event of the 'Add (Script)' button control
    /// </summary>
    private void _btnScript_Add_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      // Check: At most MAX_SCRIPT_NUMBER scripts should be edited!
      if (_lvScripts.Items.Count >= ScriptCollection.MAX_SCRIPT_NUMBER)
      {
        // Show message
        string fmt = r.GetString ("ScriptCollEditor_btnScript_Add_Info_MaxScriptNumber");   // "At most {0} scripts can be edited.\r\nThis number is already reached, thats why a further edition is impossible."
        string sMsg = string.Format (fmt, ScriptCollection.MAX_SCRIPT_NUMBER);
        string sCap = r.GetString ("Form_Common_TextInfo");                                 // "Information"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
        return;
      }
      // Add a script to the script collection:
      // Open the ScriptEditorWindow dialog (Add), handing over initialisation info
      ScriptEditorForm f = new ScriptEditorForm ();
      f.ScrColl = _sc;
      f.Index = -1;
      DialogResult dr = f.ShowDialog ();
      if (dr == DialogResult.OK) 
      {
        // Overtake
        Script scr = f.Script;
        // Add the script to the script collection
        _sc.AddScript (scr);
        // Create the new 'Scripts' ListView item
        ListViewItem lvi = new ListViewItem ();
        lvi.Text = scr.Name;
        // Add the item to the 'Scripts' ListView
        _lvScripts.Items.Add (lvi);
        // Adjust the selection of the 'Scripts' ListView and ensure its visibility
        _lvScripts.Focus ();
        _lvScripts.Items[_lvScripts.Items.Count-1].Selected = true;
        _lvScripts.Items[_lvScripts.Items.Count-1].EnsureVisible ();    
      }        
      f.Dispose ();
    }

    /// <summary>
    /// 'Click' event of the 'Edit (Script)' button control
    /// </summary>
    private void _btnScript_Edit_Click(object sender, System.EventArgs e)
    {
      // Check, whether a script name is selected or not
      int n = _lvScripts.SelectedItems.Count;
      if (0 == n) 
        return; // No.

      // Yes:
      // Get the idx of the selected entry
      int idx = _lvScripts.SelectedIndices[0];
      // Edit a script of the script collection:
      // Open the ScriptEditorWindow dialog (Edit), handing over initialisation info
      ScriptEditorForm f = new ScriptEditorForm ();
      f.ScrColl = _sc;
      f.Index = idx;
      DialogResult dr = f.ShowDialog ();
      if (dr == DialogResult.OK) 
      {
        // Overtake
        Script scr = f.Script;
        // Insert the script into the script collection
        _sc.UpdateScript (idx, scr);
        // Create the corr'ing 'Scripts' ListView item
        ListViewItem lvi = new ListViewItem ();
        lvi.Text = scr.Name;
        // Insert the item into the 'Scripts' ListView (remove the up to now item first)
        _lvScripts.Items.RemoveAt (idx);
        _lvScripts.Items.Insert (idx, lvi);
        // Adjust the selection of the 'Scripts' ListView and ensure its visibility
        _lvScripts.Focus ();
        _lvScripts.Items[idx].Selected = true;
        _lvScripts.Items[idx].EnsureVisible ();
      }        
      f.Dispose ();
    }
    
    /// <summary>
    /// 'Click' event of the 'Remove (Script)' button control
    /// </summary>
    private void _btnScript_Remove_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Check, whether a script name is selected or not
      int n = _lvScripts.SelectedItems.Count;
      if (0 == n) 
        return; // No.
      
      // Yes:
      // Safety request
      ListViewItem lviSel = _lvScripts.SelectedItems[0];
      string s = "";
      for (int i=0; i < Math.Min (3, lviSel.SubItems.Count) ;i++)
      {
        s += string.Format (" {0}", lviSel.SubItems[i].Text);
      }
      if (lviSel.SubItems.Count > 3) s += " ...";
      string fmt = r.GetString ("ScriptCollEditor_btnScript_Remove_Que_Delete");  // "Should the entry\r\n'{0}'\r\nreally be deleted?"
      string sMsg = string.Format (fmt, s);
      string sCap = r.GetString ("Form_Common_TextSafReq");                       // "Sicherheitsabfrage"
      DialogResult dr = MessageBox.Show (sMsg, sCap, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
      if (dr == DialogResult.No)
        return; // No.
      
      // Yes:
      // Remove the script name from the 'Scripts' ListView 
      // & Remove the corr'ing script from the script collection
      int idx = _lvScripts.SelectedIndices[0];
      string sScrName = _lvScripts.SelectedItems[0].Text;
      _lvScripts.Items.RemoveAt (idx);
      _sc.RemoveScript (sScrName);

      // Adjust the selection of the 'Scripts' ListView and ensure its visibility, if possible
      n = _lvScripts.Items.Count;
      if (n > 0)
      {
        if (idx < n-1) 
        {
          _lvScripts.Items[idx].Selected = true;
          _lvScripts.Items[idx].EnsureVisible ();
        }
        else         
        {
          _lvScripts.Items[n-1].Selected = true;
          _lvScripts.Items[n-1].EnsureVisible ();
        }
      }
    }

    /// <summary>
    /// 'DoubleClick' event of the 'Scripts' ListView control
    /// </summary>
    private void _lvScripts_DoubleClick(object sender, System.EventArgs e)
    {
      // Edit a script of the script collection
      _btnScript_Edit_Click(null, new System.EventArgs());
    }

    /// <summary>
    /// 'Click' event of the 'New' button control
    /// </summary>
    private void _btnNew_Click(object sender, System.EventArgs e)
    {
      // Initializes the controls
      _InitControls ();
    }
    
    /// <summary>
    /// 'Click' event of the 'Delete' button control
    /// </summary>
    private void _btnDelete_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      // Check, whether a script file is in processing or not
      string sFilepath = this._txtFilename.Text.Trim ();
      if (sFilepath.Length == 0) return;    // No
      // Yes:
      // Safety request
      string fmt = r.GetString ("ScriptCollEditor_btnDelete_Que_Delete");   // "Should the script file\r\n'{0}'\r\nreally be deleted?"
      string sMsg = string.Format (fmt, Path.GetFileName (sFilepath));
      string sCap = r.GetString ("Form_Common_TextSafReq");                 // "Sicherheitsabfrage"
      DialogResult dr = MessageBox.Show (sMsg, sCap, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
      if (dr == DialogResult.No)
        return;                             // No.
      // Yes:
      // Delete the script file
      File.Delete (sFilepath);
      // Initialize the controls
      _InitControls ();
    }

    /// <summary>
    /// 'Click' event of the 'Load' (plain text data) button control
    /// </summary>
    private void _btnLoadPlainText_Click(object sender, EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Load a script collection from file ((plain text data)
      string sFilename = string.Empty;
      string sInfo = string.Empty;
      try
      {
        // Note: 
        //  We use here the initialisation via 'app.Data.Common.sScriptFileName', because this
        //  member is updated after each Open/Save file dialog, and the dialogs are initiated
        //  therefore with the file previously used.  
        sFilename = app.Data.Common.sScriptFileName;
        // Open file dialog: Select plain text script file
        Boolean bLoad = app.Doc.GetFileName(ref sFilename, true, Doc.CryptMode.Decrypted);
        if (bLoad)
        {
          // OK
          Cursor.Current = Cursors.WaitCursor;
          // Load script file
          _sc.Load(sFilename, out sInfo);
          if (sInfo.Length > 0)
          {
            // Info: Zulässige max. Scriptanzahl OR Fensterzahl OR Befehlsanzahl überschritten
            string sMsg = string.Format("{0} '{1}':\r\n", r.GetString("cDoc_LoadScript_File"), sFilename); // "Datei '<filename>':\r\n"
            sMsg += sInfo;
            string sCap = r.GetString("Form_Common_TextInfo");           // "Info"
            MessageBox.Show(sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
          }
          // Display filename
          this._txtFilename.Text = sFilename;
        }
      }
      catch (System.Exception exc)
      {
        Debug.WriteLine("Failed: ScriptCollEditorForm_btnLoadPlainText_Click()\n");

        string fmt = r.GetString("ScriptCollEditor_btnLoad_Err_Load"); // "Error on loading script file\r\n'{0}'."
        string sMsg = string.Format(fmt, sFilename);
        sMsg += "\r\n\r\n" + exc.Message;
        MessageBox.Show(sMsg);

        _InitControls();
        return;
      }
      finally
      {
        Cursor.Current = Cursors.Default;
      }

      // Update the control contents based on the script collection
      _ScriptCollToContents(_sc);
    }

    /// <summary>
    /// 'Click' event of the 'Save' (plain text data) button control
    /// </summary>
    private void _btnSavePlainText_Click(object sender, EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Check, whether the script collection contains any scripts or not
      if (_sc.NumScripts == 0)
      {
        // No:
        string sMsg = r.GetString("ScriptCollEditor_btnSave_Info_NoScripts");  // "No scripts available.\r\nUnable to save an empty script collection."
        string sCap = r.GetString("Form_Common_TextInfo");                     // "Information"
        MessageBox.Show(sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
        return;
      }
      // Yes:
      // Update the comments (at script collection level)
      TBToComment(this._txtCmt_ScrColl, _sc.arsComment);
      // Update the 'Device no.' Top line of the script collection
      _sc.sDevNo = this._txtScrBinding.Text;
      // Update the 'UserID' Top line of the script collection
      _sc.sUserID = Encoding.ASCII.GetString(EnkyLC.hwinfo.serialNumber);

      // Save the script collection to file (plain text data)
      string sFilename = string.Empty;
      try
      {
        // Note: 
        //  We use here the initialisation via 'app.Data.Common.sScriptFileName', because this
        //  member is updated after each Open/Save file dialog, and the dialogs are initiated
        //  therefore with the file previously used.  
        sFilename = app.Data.Common.sScriptFileName;
        // Save file dialog
        Boolean bStore = app.Doc.GetFileName(ref sFilename, false, Doc.CryptMode.Decrypted);
        if (bStore)
        {
          // OK
          // Store plain text data into the script file
          _sc.Write(sFilename);
          // Display filename
          this._txtFilename.Text = sFilename;
        }
      }
      catch (System.Exception exc)
      {
        string sMsg = exc.Message;
        string sCap = r.GetString("Form_Common_TextError");  // "Fehler"
        MessageBox.Show(sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }
    
    /// <summary>
    /// 'Click' event of the 'Load' (encrypted data) button control
    /// </summary>
    private void _btnLoadEnc_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      Ressources r = app.Ressources;

      // Load a script collection from file (encrypted data)
      string sFilename = string.Empty;
      string sTempFilename = string.Empty;
      string sInfo = string.Empty;
      try
      {
        // Note: 
        //  We use here the initialisation via 'app.Data.Common.sScriptFileName', because this
        //  member is updated after each Open/Save file dialog, and the dialogs are initiated
        //  therefore with the file previously used.  
        sFilename = app.Data.Common.sScriptFileName;
        // Open file dialog: Select encrypted script file
        Boolean bLoad = app.Doc.GetFileName(ref sFilename, true, Doc.CryptMode.Encrypted);
        if (bLoad)
        {
          // OK
          Cursor.Current = Cursors.WaitCursor;
          // Read encrypted script file into byte array
          FileStream fs = File.OpenRead(sFilename);
          int nBytes = (int)fs.Length;
          byte[] arby = new byte[nBytes];
          int nBytesRead = fs.Read(arby, 0, nBytes);
          fs.Close();
          // Decrypt byte array
          string sDecrypted = Crypt.AESDecrypt(arby, AppData.CommonData.AES_KEY, AppData.CommonData.AES_IV);
          // Store decrypted data into a temporary script file
          sTempFilename = doc.FxGetTempFilename();
          StreamWriter fout = File.CreateText(sTempFilename);
          fout.Write(sDecrypted);
          fout.Close();
          // Load temporary script file
          _sc.Load(sTempFilename, out sInfo);
          if (sInfo.Length > 0)
          {
            // Info: Zulässige max. Scriptanzahl OR Fensterzahl OR Befehlsanzahl überschritten
            string sMsg = string.Format("{0} '{1}':\r\n", r.GetString("cDoc_LoadScript_File"), sFilename); // "Datei '<filename>':\r\n"
            sMsg += sInfo;
            string sCap = r.GetString("Form_Common_TextInfo");           // "Info"
            MessageBox.Show(sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
          }
          // Display filename
          this._txtFilename.Text = sFilename;
        }
      }
      catch (System.Exception exc)
      {
        Debug.WriteLine("Failed: ScriptCollEditorForm_btnLoadEnc_Click()\n");

        string fmt = r.GetString("ScriptCollEditor_btnLoad_Err_Load"); // "Error on loading script file\r\n'{0}'."
        string sMsg = string.Format(fmt, sFilename);
        sMsg += "\r\n\r\n" + exc.Message;
        MessageBox.Show(sMsg);

        _InitControls();
        return;
      }
      finally
      {
        Cursor.Current = Cursors.Default;
        // Delete temporary script file, if req.
        if (sTempFilename.Length > 0)
          File.Delete(sTempFilename);
      }

      // Update the control contents based on the script collection
      _ScriptCollToContents(_sc);
    }

    /// <summary>
    /// 'Click' event of the 'Save' (encrypted data) button control
    /// </summary>
    private void _btnSaveEnc_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      Ressources r = app.Ressources;

      // Check, whether the script collection contains any scripts or not
      if (_sc.NumScripts == 0)
      {
        // No:
        string sMsg = r.GetString("ScriptCollEditor_btnSave_Info_NoScripts");  // "No scripts available.\r\nUnable to save an empty script collection."
        string sCap = r.GetString("Form_Common_TextInfo");                     // "Information"
        MessageBox.Show(sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
        return;
      }
      // Yes:
      // Update the comments (at script collection level)
      TBToComment(this._txtCmt_ScrColl, _sc.arsComment);
      // Update the 'Device no.' Top line of the script collection
      _sc.sDevNo = this._txtScrBinding.Text;
      // Update the 'UserID' Top line of the script collection
      _sc.sUserID = Encoding.ASCII.GetString(EnkyLC.hwinfo.serialNumber);

      // Save the script collection to file (encrypted data)
      string sFilename = string.Empty;
      string sTempFilename = string.Empty;
      try
      {
        // Note: 
        //  We use here the initialisation via 'app.Data.Common.sScriptFileName', because this
        //  member is updated after each Open/Save file dialog, and the dialogs are initiated
        //  therefore with the file previously used.  
        sFilename = app.Data.Common.sScriptFileName;
        // Save file dialog
        Boolean bStore = app.Doc.GetFileName(ref sFilename, false, Doc.CryptMode.Encrypted);
        if (bStore)
        {
          // OK
          // Store decrypted data into a temporary script file
          sTempFilename = doc.FxGetTempFilename();
          _sc.Write(sTempFilename);
          // Read temporary script file into string
          StreamReader sr = File.OpenText(sTempFilename);
          string sDecrypted = sr.ReadToEnd();
          sr.Close();
          // Encrypt string
          byte[] arby = Crypt.AESEncrypt(sDecrypted, AppData.CommonData.AES_KEY, AppData.CommonData.AES_IV);
          // Store encrypted data into script file
          FileStream fs = new FileStream(sFilename, FileMode.Create);
          BinaryWriter w = new BinaryWriter(fs);
          for (int i = 0; i < arby.Length; i++)
            w.Write(arby[i]);
          w.Close();
          fs.Close();
          // Display filename
          this._txtFilename.Text = sFilename;
        }
      }
      catch (System.Exception exc)
      {
        string sMsg = exc.Message;
        string sCap = r.GetString("Form_Common_TextError");  // "Fehler"
        MessageBox.Show(sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        // Delete temporary script file, if req.
        if (sTempFilename.Length > 0)
          File.Delete(sTempFilename);
      }
    }

    /// <summary>
    /// 'HelpRequested' event of some controls
    /// </summary>
    private void _ctl_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Help requesting control
      Control requestingControl = (Control)sender;
      // Assign help text
      string s = "";
    
      // Filename
      if (requestingControl == this._txtFilename)
        s = r.GetString ("ScriptCollEditor_Hlp_txtFilename");         // "The script file, that contains the current script collection.";
      
      // Scripts
      else if (requestingControl == _lvScripts)
        s = r.GetString ("ScriptCollEditor_Hlp_lvScripts");           // "Contains the names of the scripts that are present in the current script collection.";
      else if (requestingControl == _btnScript_Add)
        s = r.GetString ("ScriptCollEditor_Hlp_btnScript_Add");       // "Adds a script to the current script collection.";
      else if (requestingControl == _btnScript_Edit)
        s = r.GetString ("ScriptCollEditor_Hlp_btnScript_Edit");      // "Updates a script in the current script collection.";
      else if (requestingControl == _btnScript_Remove)
        s = r.GetString ("ScriptCollEditor_Hlp_btnScript_Remove");    // "Removes a script from the current script collection.";
      
        // Script file binding to device no.
      else if (requestingControl == this._txtScrBinding)
        s = r.GetString ("ScriptCollEditor_Hlp_txtScrBinding");       // "The device no. the script file is connected with\r\n(Empty in case of no binding).";

        // Comments
      else if (requestingControl == this._txtCmt_ScrColl)
        s = r.GetString ("ScriptCollEditor_Hlp_txtCmt_ScrColl");      // "The comment, that appears at the top of the script file."

        // Generally
      else if (requestingControl == this._btnLoadPlainText)
        s = r.GetString("ScriptCollEditor_Hlp_btnLoadPlainText");     // "Loads a script collection from a configuration file (plain text data).";
      else if (requestingControl == _btnSavePlainText)
        s = r.GetString("ScriptCollEditor_Hlp_btnSavePlainText");     // "Saves a script collection to a configuration file (plain text data).";
      else if (requestingControl == _btnLoadEnc)
        s = r.GetString ("ScriptCollEditor_Hlp_btnLoadEnc");          // "Loads a script collection from a configuration file (encrypted data).";
      else if (requestingControl == _btnSaveEnc)
        s = r.GetString("ScriptCollEditor_Hlp_btnSaveEnc");           // "Saves a script collection to a configuration file (encrypted data).";
      else if (requestingControl == _btnNew)
        s = r.GetString ("ScriptCollEditor_Hlp_btnNew");              // "Initiates a new script collection.";
      else if (requestingControl == _btnDelete)
        s = r.GetString ("ScriptCollEditor_Hlp_btnDelete");           // "Deletes the specified script file.";
      
      // Show help
      this._ToolTip.SetToolTip (requestingControl, s);
      hlpevent.Handled=true;
    }

    /// <summary>
    /// 'Tick' event of the '_Timer' control
    /// </summary>
    private void _Timer_Tick(object sender, System.EventArgs e)
    {
      Boolean b;

      //-------------------------------
      // Section 'Scripts'

      // Check: At most MAX_SCRIPT_NUMBER scripts may be contained in a script collection
      b = (_sc.NumScripts < ScriptCollection.MAX_SCRIPT_NUMBER);
      this._btnScript_Add.Enabled = b;

      // Check, whether a script name is selected (True) or not (False)
      b = (_lvScripts.SelectedItems.Count > 0);
      this._btnScript_Edit.Enabled = b;
      this._btnScript_Remove.Enabled = b;

      //-------------------------------
      // Section 'Generally'

      // Subsection 'Plain text data': Visible, if EnkyLC is adjusted corr'ly
      this._gbPlainText.Visible = EnkyLC.bEditScripts;
      //    Button 'Load': Enabled always
      //    Button 'Save': Enabled, if the script collection contains any scripts
      b = (_sc.NumScripts > 0);
      this._btnSavePlainText.Enabled = b;

      // Subsection 'Encrypted data': Visible always
      //    Button 'Load': Enabled always
      //    Button 'Save': Enabled, if the script collection contains any scripts and EnkyLC is adjusted corr'ly
      b = (_sc.NumScripts > 0) && EnkyLC.bEditScripts;
      this._btnSaveEnc.Enabled = b;

      // Button 'New': Enabled always
      
      // Button 'Delete': Enabled, if a script file is in processing
      b = (this._txtFilename.Text.Trim().Length > 0);
      this._btnDelete.Enabled = b;
    }

    #endregion event handling

    #region members

    /// <summary>
    /// The script collection
    /// </summary>
    ScriptCollection _sc = new ScriptCollection ();

    #endregion members
 
    #region constants
    #endregion constants
  
    #region methods

    /// <summary>
    /// Updates the control contents based on a script collection.
    /// </summary>
    /// <param name="sc">The script collection</param>
    void _ScriptCollToContents (ScriptCollection sc)
    {
      // Clear the 'Scripts' ListViev 
      _lvScripts.Items.Clear ();
      // Fill the 'Scripts' ListView 
      for (int i=0; i < sc.NumScripts; i++)
      {
        // The current script
        Script scr = sc.Scripts[i];
        // Update the 'Scripts' ListView
        _lvScripts.Items.Add (scr.Name);
      }
      // Adjust the selection of the 'Scripts' ListView and ensure its visibility
      if (_lvScripts.Items.Count > 0)
      {
        _lvScripts.Focus ();
        _lvScripts.Items[0].Selected = true;
        _lvScripts.Items[0].EnsureVisible ();
      }
      // Update the TextBox, corr'ing with the script collection level comments
      CommentToTB (this._txtCmt_ScrColl, sc.arsComment);
      // Update the 'Script file binding to device no.' TB 
      this._txtScrBinding.Text = sc.sDevNo;
    }

    /// <summary>
    /// Updates a 'Comments' TextBox control based on the corr'ing comments, 
    /// available in the script collection.
    /// </summary>
    /// <param name="t">The 'Comments' TextBox</param>
    /// <param name="ar">The comments</param>
    void CommentToTB (TextBox t, StringArray ar)
    {
      // Build the TextBox text (lines, separated by NL; avoid empty lines)
      string sCmt = string.Empty;
      for (int i=0; i < ar.Count; i++)
        if (ar[i].Trim().Length > 0) sCmt += ar[i] + "\r\n";
      // Update the TextBox text corr'ly
      t.Text = sCmt;
    }

    /// <summary>
    /// Updates the comments, available in a script collection, based on the corr'ing
    /// 'Comments' TextBox control.
    /// </summary>
    /// <param name="t">The 'Comments' TextBox</param>
    /// <param name="ar">The comments</param>
    void TBToComment (TextBox t, StringArray ar)
    {
      // Extract all strings of text from the TextBox control 
      string[] arCmt = new string [t.Lines.Length];
      arCmt = t.Lines;
      // Update the comments corr'ly (avoid empty lines)
      ar.Clear ();
      for (int i=0; i < arCmt.Length; i++)
        if (arCmt[i].Trim().Length > 0) ar.Add (arCmt[i]);
    }

    /// <summary>
    /// Initializes the controls.
    /// </summary>
    void _InitControls ()
    {
      // Filename
      this._txtFilename.Text = "";
      // Scripts
      _lvScripts.Items.Clear ();
      // Script file binding
      this._txtScrBinding.Text = "";
      // Comments
      this._txtCmt_ScrColl.Text = "";
    
      // Reset the script collection
      _sc.Reset ();
    }

    #endregion methods

  }
}
