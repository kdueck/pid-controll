using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace App
{
	/// <summary>
	/// Class TitleForm:
	/// Entrance window
	/// </summary>
	public class TitleForm : System.Windows.Forms.Form
	{
    private System.Windows.Forms.Timer _Timer;
    private System.Windows.Forms.PictureBox _PictureBox;
    private System.ComponentModel.IContainer components;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="f">The parent window</param>
    public TitleForm ( Form f )
		{
			InitializeComponent();

      // Overgive
      _f = f;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.components = new System.ComponentModel.Container();
      this._Timer = new System.Windows.Forms.Timer(this.components);
      this._PictureBox = new System.Windows.Forms.PictureBox();
      this.SuspendLayout();
      // 
      // _Timer
      // 
      this._Timer.Interval = 2500;
      this._Timer.Tick += new System.EventHandler(this._Timer_Tick);
      // 
      // _PictureBox
      // 
      this._PictureBox.Location = new System.Drawing.Point(0, 0);
      this._PictureBox.Name = "_PictureBox";
      this._PictureBox.Size = new System.Drawing.Size(520, 317);
      this._PictureBox.TabIndex = 0;
      this._PictureBox.TabStop = false;
      // 
      // TitleForm
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.ClientSize = new System.Drawing.Size(520, 317);
      this.ControlBox = false;
      this.Controls.Add(this._PictureBox);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.Name = "TitleForm";
      this.ShowInTaskbar = false;
      this.Load += new System.EventHandler(this.TitleForm_Load);
      this.Closed += new System.EventHandler(this.TitleForm_Closed);
      this.ResumeLayout(false);

    }

		#endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void TitleForm_Load(object sender, System.EventArgs e)
    {
      // PictureBox ( depending on the Language last used )
      App app = App.Instance;
      string path = "App.Images.Dialogs.Title.";
      if ( app.Data.Language.eLanguage == AppData.LanguageData.Languages.en )
        path += "AppTitle.bmp";
      else 
        path += "AppTitle_de.bmp";
      this._PictureBox.Image = Common.LoadBitmap ( path );

      // Center form
      if ( null != _f )
        Common.CenterWindow ( this, _f );

      // Enable timer
      _Timer.Enabled = true;
    }

    /// <summary>
    /// 'Closed' event of the form
    /// </summary>
    private void TitleForm_Closed(object sender, System.EventArgs e)
    {
      // Disable timer
      _Timer.Enabled = false;
    }

    /// <summary>
    /// 'Tick' event of the _Timer control (each 5 sec)
    /// </summary>
    private void _Timer_Tick(object sender, System.EventArgs e)
    {
      // Close the form
      Close ();
    }
	
    #endregion event handling

    #region members

    /// <summary>
    /// The parent window 
    /// </summary>
    Form _f = null; 

    #endregion  // members

  }
}
