// If defined, the 'Clear' action is processed in the devices 'Script transfer' mode.
// If not defined, the devices 'Script transfer' mode will not be used.
#define SERVICEEXT_CLEAR_WITH_INTERRUPTION

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Text;
using System.IO;
using System.Diagnostics;

using CommRS232;

namespace App
{
	/// <summary>
	/// Class ServiceExtForm
	/// </summary>
	/// <remarks>
  /// The RDTFI (Read Text file contents) cmd is implemented here as follows:
  /// First ALL the paths of the text files to be read from device are put into a transfer list.
  /// Then each file of the transfer list is treated sequentielly: 
  /// The 1st file path in the transfer list is put into the TX message list, after it the corr'ing file 
  /// contents are received, they are processed (storage to HD), and the corr'ing entry is removed from 
  /// the transfer list. This procedure is repeated until the transfer list was empty.
	/// </remarks>
  public class ServiceExtForm : System.Windows.Forms.Form
  {
    private System.Windows.Forms.GroupBox gpSDcard;
    private System.Windows.Forms.GroupBox gpFlow;
    private System.Windows.Forms.Button btnFlowFiles_Select;
    private System.Windows.Forms.Button btnFlowFiles_Read;
    private System.Windows.Forms.ListBox lbFlowFiles;
    private System.Windows.Forms.Label lblFlowFiles;
    private System.Windows.Forms.GroupBox gpClear;
    private System.Windows.Forms.Button btnClear;
    private System.Windows.Forms.RadioButton rbFlowLog;
    private System.Windows.Forms.ProgressBar pgbClear;
    private System.Windows.Forms.GroupBox gpStorageFolder;
    private System.Windows.Forms.Button btnStorageFolder;
    private System.Windows.Forms.Label lblStorageFolder;
    private System.Windows.Forms.GroupBox gpTransfer;
    private System.Windows.Forms.Label lblTransferRate;
    private System.Windows.Forms.TextBox txtFileContents;
    private System.Windows.Forms.Label lblFileContents;
    private System.Windows.Forms.ListBox lbAddedFiles;
    private System.Windows.Forms.Label lblAddedFiles;
    private System.Windows.Forms.CheckBox chkFilePreview;
    private System.Windows.Forms.Button btnAdd;
    private System.Windows.Forms.Button btnRemove;
    private System.Windows.Forms.Button btnRemoveAll;
    private System.Windows.Forms.Button btnTransfer;
    private System.Windows.Forms.ProgressBar pgbTransfer;
    private System.Windows.Forms.GroupBox gpErrHan;
    private System.Windows.Forms.ListView _lvPercent;
    private System.Windows.Forms.ColumnHeader colEHPercentage;
    private System.Windows.Forms.Label lblEH_Hint;
    private System.Windows.Forms.ColumnHeader colEHID;
    private System.Windows.Forms.Button _btnEHWrite;
    private System.Windows.Forms.Button _btnEHRead;
    private System.Windows.Forms.Label lblEH_Success;
    private System.Windows.Forms.ToolTip _ToolTip;
    private GroupBox gpUserID;
    private ListView _lvUserIDs;
    private ColumnHeader colUID_No;
    private ColumnHeader colUID_DT;
    private ColumnHeader colUID_ID;
    private GroupBox gpSDreinit;
    private Button btnSDreinitReset;
    private Button btnSDreinitRead;
    private Label lblSDreinit_val;
    private Label lblSDreinit;
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Constructor
    /// </summary>
    public ServiceExtForm()
    {
      InitializeComponent();
      
      // Init.
      Init ();
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if (components != null) 
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container ();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager (typeof (ServiceExtForm));
      this.gpSDcard = new System.Windows.Forms.GroupBox ();
      this.gpFlow = new System.Windows.Forms.GroupBox ();
      this.btnFlowFiles_Select = new System.Windows.Forms.Button ();
      this.btnFlowFiles_Read = new System.Windows.Forms.Button ();
      this.lbFlowFiles = new System.Windows.Forms.ListBox ();
      this.lblFlowFiles = new System.Windows.Forms.Label ();
      this.gpClear = new System.Windows.Forms.GroupBox ();
      this.btnClear = new System.Windows.Forms.Button ();
      this.rbFlowLog = new System.Windows.Forms.RadioButton ();
      this.pgbClear = new System.Windows.Forms.ProgressBar ();
      this.gpStorageFolder = new System.Windows.Forms.GroupBox ();
      this.btnStorageFolder = new System.Windows.Forms.Button ();
      this.lblStorageFolder = new System.Windows.Forms.Label ();
      this.gpTransfer = new System.Windows.Forms.GroupBox ();
      this.lblTransferRate = new System.Windows.Forms.Label ();
      this.txtFileContents = new System.Windows.Forms.TextBox ();
      this.lblFileContents = new System.Windows.Forms.Label ();
      this.lbAddedFiles = new System.Windows.Forms.ListBox ();
      this.lblAddedFiles = new System.Windows.Forms.Label ();
      this.chkFilePreview = new System.Windows.Forms.CheckBox ();
      this.btnAdd = new System.Windows.Forms.Button ();
      this.btnRemove = new System.Windows.Forms.Button ();
      this.btnRemoveAll = new System.Windows.Forms.Button ();
      this.btnTransfer = new System.Windows.Forms.Button ();
      this.pgbTransfer = new System.Windows.Forms.ProgressBar ();
      this.gpErrHan = new System.Windows.Forms.GroupBox ();
      this.lblEH_Success = new System.Windows.Forms.Label ();
      this._btnEHWrite = new System.Windows.Forms.Button ();
      this._btnEHRead = new System.Windows.Forms.Button ();
      this.lblEH_Hint = new System.Windows.Forms.Label ();
      this._lvPercent = new System.Windows.Forms.ListView ();
      this.colEHID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader ()));
      this.colEHPercentage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader ()));
      this._ToolTip = new System.Windows.Forms.ToolTip (this.components);
      this.gpUserID = new System.Windows.Forms.GroupBox ();
      this._lvUserIDs = new System.Windows.Forms.ListView ();
      this.colUID_No = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader ()));
      this.colUID_DT = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader ()));
      this.colUID_ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader ()));
      this.gpSDreinit = new System.Windows.Forms.GroupBox ();
      this.btnSDreinitReset = new System.Windows.Forms.Button ();
      this.btnSDreinitRead = new System.Windows.Forms.Button ();
      this.lblSDreinit_val = new System.Windows.Forms.Label ();
      this.lblSDreinit = new System.Windows.Forms.Label ();
      this.gpSDcard.SuspendLayout ();
      this.gpFlow.SuspendLayout ();
      this.gpClear.SuspendLayout ();
      this.gpStorageFolder.SuspendLayout ();
      this.gpTransfer.SuspendLayout ();
      this.gpErrHan.SuspendLayout ();
      this.gpUserID.SuspendLayout ();
      this.gpSDreinit.SuspendLayout ();
      this.SuspendLayout ();
      // 
      // gpSDcard
      // 
      this.gpSDcard.Controls.Add (this.gpSDreinit);
      this.gpSDcard.Controls.Add (this.gpFlow);
      this.gpSDcard.Controls.Add (this.gpClear);
      this.gpSDcard.Controls.Add (this.gpStorageFolder);
      this.gpSDcard.Controls.Add (this.gpTransfer);
      this.gpSDcard.Location = new System.Drawing.Point (8, 4);
      this.gpSDcard.Name = "gpSDcard";
      this.gpSDcard.Size = new System.Drawing.Size (644, 394);
      this.gpSDcard.TabIndex = 0;
      this.gpSDcard.TabStop = false;
      this.gpSDcard.Text = "SD card";
      // 
      // gpFlow
      // 
      this.gpFlow.Controls.Add (this.btnFlowFiles_Select);
      this.gpFlow.Controls.Add (this.btnFlowFiles_Read);
      this.gpFlow.Controls.Add (this.lbFlowFiles);
      this.gpFlow.Controls.Add (this.lblFlowFiles);
      this.gpFlow.Location = new System.Drawing.Point (8, 20);
      this.gpFlow.Name = "gpFlow";
      this.gpFlow.Size = new System.Drawing.Size (176, 156);
      this.gpFlow.TabIndex = 0;
      this.gpFlow.TabStop = false;
      this.gpFlow.Text = "Flow files on device";
      // 
      // btnFlowFiles_Select
      // 
      this.btnFlowFiles_Select.Location = new System.Drawing.Point (92, 128);
      this.btnFlowFiles_Select.Name = "btnFlowFiles_Select";
      this.btnFlowFiles_Select.Size = new System.Drawing.Size (76, 23);
      this.btnFlowFiles_Select.TabIndex = 3;
      this.btnFlowFiles_Select.Text = "Select all";
      this.btnFlowFiles_Select.Click += new System.EventHandler (this.btnSelect_Click);
      this.btnFlowFiles_Select.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnFlowFiles_Read
      // 
      this.btnFlowFiles_Read.Location = new System.Drawing.Point (8, 128);
      this.btnFlowFiles_Read.Name = "btnFlowFiles_Read";
      this.btnFlowFiles_Read.Size = new System.Drawing.Size (76, 23);
      this.btnFlowFiles_Read.TabIndex = 2;
      this.btnFlowFiles_Read.Text = "Read";
      this.btnFlowFiles_Read.Click += new System.EventHandler (this.btnRead_Click);
      this.btnFlowFiles_Read.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lbFlowFiles
      // 
      this.lbFlowFiles.Location = new System.Drawing.Point (8, 56);
      this.lbFlowFiles.Name = "lbFlowFiles";
      this.lbFlowFiles.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.lbFlowFiles.Size = new System.Drawing.Size (160, 69);
      this.lbFlowFiles.Sorted = true;
      this.lbFlowFiles.TabIndex = 1;
      this.lbFlowFiles.SelectedIndexChanged += new System.EventHandler (this.lbFiles_SelectedIndexChanged);
      this.lbFlowFiles.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lblFlowFiles
      // 
      this.lblFlowFiles.Location = new System.Drawing.Point (8, 20);
      this.lblFlowFiles.Name = "lblFlowFiles";
      this.lblFlowFiles.Size = new System.Drawing.Size (160, 36);
      this.lblFlowFiles.TabIndex = 0;
      this.lblFlowFiles.Text = "Available files: ";
      this.lblFlowFiles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // gpClear
      // 
      this.gpClear.Controls.Add (this.btnClear);
      this.gpClear.Controls.Add (this.rbFlowLog);
      this.gpClear.Controls.Add (this.pgbClear);
      this.gpClear.Location = new System.Drawing.Point (8, 177);
      this.gpClear.Name = "gpClear";
      this.gpClear.Size = new System.Drawing.Size (176, 92);
      this.gpClear.TabIndex = 3;
      this.gpClear.TabStop = false;
      this.gpClear.Text = "Clear  device storage";
      // 
      // btnClear
      // 
      this.btnClear.Location = new System.Drawing.Point (22, 42);
      this.btnClear.Name = "btnClear";
      this.btnClear.Size = new System.Drawing.Size (136, 23);
      this.btnClear.TabIndex = 1;
      this.btnClear.Text = "Clear";
      this.btnClear.Click += new System.EventHandler (this.btnClear_Click);
      this.btnClear.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // rbFlowLog
      // 
      this.rbFlowLog.Location = new System.Drawing.Point (8, 18);
      this.rbFlowLog.Name = "rbFlowLog";
      this.rbFlowLog.Size = new System.Drawing.Size (80, 20);
      this.rbFlowLog.TabIndex = 0;
      this.rbFlowLog.TabStop = true;
      this.rbFlowLog.Text = "Flow";
      this.rbFlowLog.CheckedChanged += new System.EventHandler (this.rbClear_CheckedChanged);
      this.rbFlowLog.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // pgbClear
      // 
      this.pgbClear.Location = new System.Drawing.Point (20, 66);
      this.pgbClear.Name = "pgbClear";
      this.pgbClear.Size = new System.Drawing.Size (136, 20);
      this.pgbClear.TabIndex = 2;
      // 
      // gpStorageFolder
      // 
      this.gpStorageFolder.Controls.Add (this.btnStorageFolder);
      this.gpStorageFolder.Controls.Add (this.lblStorageFolder);
      this.gpStorageFolder.Location = new System.Drawing.Point (192, 20);
      this.gpStorageFolder.Name = "gpStorageFolder";
      this.gpStorageFolder.Size = new System.Drawing.Size (444, 44);
      this.gpStorageFolder.TabIndex = 1;
      this.gpStorageFolder.TabStop = false;
      this.gpStorageFolder.Text = "Storage folder";
      // 
      // btnStorageFolder
      // 
      this.btnStorageFolder.Image = ((System.Drawing.Image)(resources.GetObject ("btnStorageFolder.Image")));
      this.btnStorageFolder.Location = new System.Drawing.Point (412, 18);
      this.btnStorageFolder.Name = "btnStorageFolder";
      this.btnStorageFolder.Size = new System.Drawing.Size (22, 23);
      this.btnStorageFolder.TabIndex = 1;
      this.btnStorageFolder.Click += new System.EventHandler (this.btnStorageFolder_Click);
      this.btnStorageFolder.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lblStorageFolder
      // 
      this.lblStorageFolder.Location = new System.Drawing.Point (8, 20);
      this.lblStorageFolder.Name = "lblStorageFolder";
      this.lblStorageFolder.Size = new System.Drawing.Size (396, 18);
      this.lblStorageFolder.TabIndex = 0;
      this.lblStorageFolder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // gpTransfer
      // 
      this.gpTransfer.Controls.Add (this.lblTransferRate);
      this.gpTransfer.Controls.Add (this.txtFileContents);
      this.gpTransfer.Controls.Add (this.lblFileContents);
      this.gpTransfer.Controls.Add (this.lbAddedFiles);
      this.gpTransfer.Controls.Add (this.lblAddedFiles);
      this.gpTransfer.Controls.Add (this.chkFilePreview);
      this.gpTransfer.Controls.Add (this.btnAdd);
      this.gpTransfer.Controls.Add (this.btnRemove);
      this.gpTransfer.Controls.Add (this.btnRemoveAll);
      this.gpTransfer.Controls.Add (this.btnTransfer);
      this.gpTransfer.Controls.Add (this.pgbTransfer);
      this.gpTransfer.Location = new System.Drawing.Point (192, 68);
      this.gpTransfer.Name = "gpTransfer";
      this.gpTransfer.Size = new System.Drawing.Size (444, 317);
      this.gpTransfer.TabIndex = 2;
      this.gpTransfer.TabStop = false;
      this.gpTransfer.Text = "File transfer from device to PC";
      // 
      // lblTransferRate
      // 
      this.lblTransferRate.Location = new System.Drawing.Point (384, 138);
      this.lblTransferRate.Name = "lblTransferRate";
      this.lblTransferRate.Size = new System.Drawing.Size (48, 23);
      this.lblTransferRate.TabIndex = 8;
      this.lblTransferRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txtFileContents
      // 
      this.txtFileContents.Location = new System.Drawing.Point (8, 186);
      this.txtFileContents.Multiline = true;
      this.txtFileContents.Name = "txtFileContents";
      this.txtFileContents.ReadOnly = true;
      this.txtFileContents.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this.txtFileContents.Size = new System.Drawing.Size (424, 120);
      this.txtFileContents.TabIndex = 10;
      this.txtFileContents.WordWrap = false;
      this.txtFileContents.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lblFileContents
      // 
      this.lblFileContents.Location = new System.Drawing.Point (12, 166);
      this.lblFileContents.Name = "lblFileContents";
      this.lblFileContents.Size = new System.Drawing.Size (420, 18);
      this.lblFileContents.TabIndex = 9;
      this.lblFileContents.Text = "File contents:";
      this.lblFileContents.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbAddedFiles
      // 
      this.lbAddedFiles.Location = new System.Drawing.Point (8, 40);
      this.lbAddedFiles.Name = "lbAddedFiles";
      this.lbAddedFiles.Size = new System.Drawing.Size (424, 69);
      this.lbAddedFiles.TabIndex = 1;
      this.lbAddedFiles.SelectedIndexChanged += new System.EventHandler (this.lbAddedFiles_SelectedIndexChanged);
      this.lbAddedFiles.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lblAddedFiles
      // 
      this.lblAddedFiles.Location = new System.Drawing.Point (8, 20);
      this.lblAddedFiles.Name = "lblAddedFiles";
      this.lblAddedFiles.Size = new System.Drawing.Size (276, 18);
      this.lblAddedFiles.TabIndex = 0;
      this.lblAddedFiles.Text = "Device files to be transferred:";
      this.lblAddedFiles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // chkFilePreview
      // 
      this.chkFilePreview.Location = new System.Drawing.Point (296, 20);
      this.chkFilePreview.Name = "chkFilePreview";
      this.chkFilePreview.Size = new System.Drawing.Size (136, 18);
      this.chkFilePreview.TabIndex = 2;
      this.chkFilePreview.Text = "File preview";
      this.chkFilePreview.CheckedChanged += new System.EventHandler (this.chkFilePreview_CheckedChanged);
      this.chkFilePreview.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnAdd
      // 
      this.btnAdd.Font = new System.Drawing.Font ("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnAdd.Location = new System.Drawing.Point (8, 112);
      this.btnAdd.Name = "btnAdd";
      this.btnAdd.Size = new System.Drawing.Size (136, 24);
      this.btnAdd.TabIndex = 3;
      this.btnAdd.Text = "Add selected files";
      this.btnAdd.Click += new System.EventHandler (this.btnAdd_Click);
      this.btnAdd.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnRemove
      // 
      this.btnRemove.Font = new System.Drawing.Font ("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnRemove.Location = new System.Drawing.Point (152, 112);
      this.btnRemove.Name = "btnRemove";
      this.btnRemove.Size = new System.Drawing.Size (136, 24);
      this.btnRemove.TabIndex = 4;
      this.btnRemove.Text = "Remove selected file";
      this.btnRemove.Click += new System.EventHandler (this.btnRemove_Click);
      this.btnRemove.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnRemoveAll
      // 
      this.btnRemoveAll.Font = new System.Drawing.Font ("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnRemoveAll.Location = new System.Drawing.Point (296, 112);
      this.btnRemoveAll.Name = "btnRemoveAll";
      this.btnRemoveAll.Size = new System.Drawing.Size (136, 24);
      this.btnRemoveAll.TabIndex = 5;
      this.btnRemoveAll.Text = "Remove all files";
      this.btnRemoveAll.Click += new System.EventHandler (this.btnRemoveAll_Click);
      this.btnRemoveAll.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnTransfer
      // 
      this.btnTransfer.BackColor = System.Drawing.SystemColors.Control;
      this.btnTransfer.ForeColor = System.Drawing.SystemColors.ControlText;
      this.btnTransfer.Location = new System.Drawing.Point (8, 138);
      this.btnTransfer.Name = "btnTransfer";
      this.btnTransfer.Size = new System.Drawing.Size (136, 24);
      this.btnTransfer.TabIndex = 6;
      this.btnTransfer.Text = "Transfer";
      this.btnTransfer.UseVisualStyleBackColor = false;
      this.btnTransfer.Click += new System.EventHandler (this.btnTransfer_Click);
      this.btnTransfer.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // pgbTransfer
      // 
      this.pgbTransfer.Location = new System.Drawing.Point (152, 138);
      this.pgbTransfer.Name = "pgbTransfer";
      this.pgbTransfer.Size = new System.Drawing.Size (220, 23);
      this.pgbTransfer.TabIndex = 7;
      // 
      // gpErrHan
      // 
      this.gpErrHan.Controls.Add (this.lblEH_Success);
      this.gpErrHan.Controls.Add (this._btnEHWrite);
      this.gpErrHan.Controls.Add (this._btnEHRead);
      this.gpErrHan.Controls.Add (this.lblEH_Hint);
      this.gpErrHan.Controls.Add (this._lvPercent);
      this.gpErrHan.Location = new System.Drawing.Point (8, 399);
      this.gpErrHan.Name = "gpErrHan";
      this.gpErrHan.Size = new System.Drawing.Size (280, 206);
      this.gpErrHan.TabIndex = 1;
      this.gpErrHan.TabStop = false;
      this.gpErrHan.Text = "Error handling";
      // 
      // lblEH_Success
      // 
      this.lblEH_Success.Location = new System.Drawing.Point (175, 96);
      this.lblEH_Success.Name = "lblEH_Success";
      this.lblEH_Success.Size = new System.Drawing.Size (96, 20);
      this.lblEH_Success.TabIndex = 3;
      this.lblEH_Success.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // _btnEHWrite
      // 
      this._btnEHWrite.Location = new System.Drawing.Point (175, 64);
      this._btnEHWrite.Name = "_btnEHWrite";
      this._btnEHWrite.Size = new System.Drawing.Size (96, 23);
      this._btnEHWrite.TabIndex = 2;
      this._btnEHWrite.Text = "Write data";
      this._btnEHWrite.Click += new System.EventHandler (this._btnEHWrite_Click);
      this._btnEHWrite.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // _btnEHRead
      // 
      this._btnEHRead.Location = new System.Drawing.Point (175, 32);
      this._btnEHRead.Name = "_btnEHRead";
      this._btnEHRead.Size = new System.Drawing.Size (96, 23);
      this._btnEHRead.TabIndex = 1;
      this._btnEHRead.Text = "Read data";
      this._btnEHRead.Click += new System.EventHandler (this._btnEHRead_Click);
      this._btnEHRead.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lblEH_Hint
      // 
      this.lblEH_Hint.Location = new System.Drawing.Point (9, 129);
      this.lblEH_Hint.Name = "lblEH_Hint";
      this.lblEH_Hint.Size = new System.Drawing.Size (262, 69);
      this.lblEH_Hint.TabIndex = 4;
      this.lblEH_Hint.Text = "Das Feld \'Percentage\' gibt die Mindestanzahl der fehlerfreien Durchl�ufe an, dami" +
          "t der zugeordnete Fehler NICHT ausgel�st wird. Zum Editieren bitte doppelklicken" +
          ".";
      // 
      // _lvPercent
      // 
      this._lvPercent.Columns.AddRange (new System.Windows.Forms.ColumnHeader[] {
            this.colEHID,
            this.colEHPercentage});
      this._lvPercent.FullRowSelect = true;
      this._lvPercent.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
      this._lvPercent.HideSelection = false;
      this._lvPercent.Location = new System.Drawing.Point (8, 20);
      this._lvPercent.MultiSelect = false;
      this._lvPercent.Name = "_lvPercent";
      this._lvPercent.Size = new System.Drawing.Size (160, 104);
      this._lvPercent.TabIndex = 0;
      this._lvPercent.UseCompatibleStateImageBehavior = false;
      this._lvPercent.View = System.Windows.Forms.View.Details;
      this._lvPercent.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      this._lvPercent.DoubleClick += new System.EventHandler (this._lvPercent_DoubleClick);
      // 
      // colEHID
      // 
      this.colEHID.Text = "ID";
      // 
      // colEHPercentage
      // 
      this.colEHPercentage.Text = "Percentage";
      this.colEHPercentage.Width = 80;
      // 
      // _ToolTip
      // 
      this._ToolTip.AutoPopDelay = 10000;
      this._ToolTip.InitialDelay = 500;
      this._ToolTip.ReshowDelay = 100;
      // 
      // gpUserID
      // 
      this.gpUserID.Controls.Add (this._lvUserIDs);
      this.gpUserID.Location = new System.Drawing.Point (298, 399);
      this.gpUserID.Name = "gpUserID";
      this.gpUserID.Size = new System.Drawing.Size (354, 205);
      this.gpUserID.TabIndex = 2;
      this.gpUserID.TabStop = false;
      this.gpUserID.Text = "User ID\'s on device EEPROM";
      // 
      // _lvUserIDs
      // 
      this._lvUserIDs.Columns.AddRange (new System.Windows.Forms.ColumnHeader[] {
            this.colUID_No,
            this.colUID_DT,
            this.colUID_ID});
      this._lvUserIDs.FullRowSelect = true;
      this._lvUserIDs.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
      this._lvUserIDs.HideSelection = false;
      this._lvUserIDs.Location = new System.Drawing.Point (11, 23);
      this._lvUserIDs.MultiSelect = false;
      this._lvUserIDs.Name = "_lvUserIDs";
      this._lvUserIDs.Size = new System.Drawing.Size (323, 175);
      this._lvUserIDs.TabIndex = 0;
      this._lvUserIDs.UseCompatibleStateImageBehavior = false;
      this._lvUserIDs.View = System.Windows.Forms.View.Details;
      this._lvUserIDs.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // colUID_No
      // 
      this.colUID_No.Text = "No";
      this.colUID_No.Width = 40;
      // 
      // colUID_DT
      // 
      this.colUID_DT.Text = "Timestamp";
      this.colUID_DT.Width = 140;
      // 
      // colUID_ID
      // 
      this.colUID_ID.Text = "User ID";
      this.colUID_ID.Width = 100;
      // 
      // gpSDreinit
      // 
      this.gpSDreinit.Controls.Add (this.btnSDreinitReset);
      this.gpSDreinit.Controls.Add (this.btnSDreinitRead);
      this.gpSDreinit.Controls.Add (this.lblSDreinit_val);
      this.gpSDreinit.Controls.Add (this.lblSDreinit);
      this.gpSDreinit.Location = new System.Drawing.Point (8, 270);
      this.gpSDreinit.Name = "gpSDreinit";
      this.gpSDreinit.Size = new System.Drawing.Size (176, 116);
      this.gpSDreinit.TabIndex = 4;
      this.gpSDreinit.TabStop = false;
      this.gpSDreinit.Text = "SDcard reinitialisation";
      // 
      // btnSDreinitReset
      // 
      this.btnSDreinitReset.Location = new System.Drawing.Point (92, 84);
      this.btnSDreinitReset.Name = "btnSDreinitReset";
      this.btnSDreinitReset.Size = new System.Drawing.Size (76, 23);
      this.btnSDreinitReset.TabIndex = 3;
      this.btnSDreinitReset.Text = "Reset";
      this.btnSDreinitReset.Click += new System.EventHandler (this.btnSDreinitReset_Click);
      this.btnSDreinitReset.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // btnSDreinitRead
      // 
      this.btnSDreinitRead.Location = new System.Drawing.Point (8, 84);
      this.btnSDreinitRead.Name = "btnSDreinitRead";
      this.btnSDreinitRead.Size = new System.Drawing.Size (76, 23);
      this.btnSDreinitRead.TabIndex = 2;
      this.btnSDreinitRead.Text = "Read";
      this.btnSDreinitRead.Click += new System.EventHandler (this.btnSDreinitRead_Click);
      this.btnSDreinitRead.HelpRequested += new System.Windows.Forms.HelpEventHandler (this._ctl_HelpRequested);
      // 
      // lblSDreinit_val
      // 
      this.lblSDreinit_val.Location = new System.Drawing.Point (8, 56);
      this.lblSDreinit_val.Name = "lblSDreinit_val";
      this.lblSDreinit_val.Size = new System.Drawing.Size (160, 20);
      this.lblSDreinit_val.TabIndex = 1;
      this.lblSDreinit_val.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblSDreinit
      // 
      this.lblSDreinit.Location = new System.Drawing.Point (8, 24);
      this.lblSDreinit.Name = "lblSDreinit";
      this.lblSDreinit.Size = new System.Drawing.Size (160, 28);
      this.lblSDreinit.TabIndex = 0;
      this.lblSDreinit.Text = "Number od SD card reinitialisations:";
      this.lblSDreinit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // ServiceExtForm
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size (5, 13);
      this.ClientSize = new System.Drawing.Size (662, 611);
      this.Controls.Add (this.gpUserID);
      this.Controls.Add (this.gpErrHan);
      this.Controls.Add (this.gpSDcard);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.HelpButton = true;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ServiceExtForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Extended functions";
      this.Closed += new System.EventHandler (this.ServiceExtForm_Closed);
      this.Load += new System.EventHandler (this.ServiceExtForm_Load);
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler (this.ServiceExtForm_KeyPress);
      this.gpSDcard.ResumeLayout (false);
      this.gpFlow.ResumeLayout (false);
      this.gpClear.ResumeLayout (false);
      this.gpStorageFolder.ResumeLayout (false);
      this.gpTransfer.ResumeLayout (false);
      this.gpTransfer.PerformLayout ();
      this.gpErrHan.ResumeLayout (false);
      this.gpUserID.ResumeLayout (false);
      this.gpSDreinit.ResumeLayout (false);
      this.ResumeLayout (false);

    }

    #endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form 
    /// </summary>
    private void ServiceExtForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Resources
      //    Initial Label strings
      _sInitialLblFilesTxt            = r.GetString ("SDcardReader_Txt_AvailFiles");    // "Available files:";
      _sInitiallblFileContentsTxt     = r.GetString ("SDcardReader_Txt_FileCont");      // "File contents:";
      //    Select Buttons      
      _sBTN_TEXT_SELECT_ALL           = r.GetString ("SDcardReader_Txt_SelectAll");     // "Select all"
      _sBTN_TEXT_UNSELECT_ALL         = r.GetString ("SDcardReader_Txt_UnselectAll");   // "Unselect all"
      //    Controls
      this.Text                       = r.GetString ( "ServiceExt_Title" );             // "Extended functions"
      
      this.gpSDcard.Text              = r.GetString ( "ServiceExt_gpSDcard" );          // "SD card"

      this.gpFlow.Text                = r.GetString ( "ServiceExt_gpFlow" );            // "Flow files on device"
      this.lblFlowFiles.Text          = _sInitialLblFilesTxt;                           // "Available files:";
      this.btnFlowFiles_Read.Text     = r.GetString ( "SDcardReader_btnRead" );         // "Read"
      this.btnFlowFiles_Select.Text   = _sBTN_TEXT_SELECT_ALL;                          // "Select all"
      
      this.gpStorageFolder.Text       = r.GetString ( "SDcardReader_gpStorageFolder" ); // "Storage folder"  

      this.gpTransfer.Text            = r.GetString ( "SDcardReader_gpTransfer" );      // "File transfer from device to PC"
      this.lblAddedFiles.Text         = r.GetString ( "SDcardReader_lblAddedFiles" );   // "Device files to be transferred"
      this.chkFilePreview.Text        = r.GetString ( "SDcardReader_chkFilePreview" );  // "File preview"
      this.btnAdd.Text                = r.GetString ( "SDcardReader_btnAdd" );          // "Add selected files"
      this.btnRemove.Text             = r.GetString ( "SDcardReader_btnRemove" );       // "Remove selected file"
      this.btnRemoveAll.Text          = r.GetString ( "SDcardReader_btnRemoveAll" );    // "Remove all files"
      this.btnTransfer.Text           = r.GetString ( "SDcardReader_btnTransfer" );     // "Transfer"
      this.lblFileContents.Text       = _sInitiallblFileContentsTxt;                    // "File contents:";
      
      this.gpClear.Text               = r.GetString ( "SDcardReader_gpClear" );         // "Clear device storage"
      this.rbFlowLog.Text             = r.GetString ( "ServiceExt_rbFlowLog" );         // "Flow"
      this.btnClear.Text              = r.GetString ( "SDcardReader_btnClear" );        // "Clear"

      this.gpSDreinit.Text            = r.GetString ("ServiceExt_gpSDreinit");          // "SDcard reinitialisations"

      this.lblSDreinit.Text           = r.GetString ("ServiceExt_lblSDreinit");         // "No of SDcard reinitialisations:"
      this.btnSDreinitRead.Text       = r.GetString ("ServiceExt_btnSDreinitRead");     // "Read"
      this.btnSDreinitReset.Text      = r.GetString ("ServiceExt_btnSDreinitReset");    // "Reset"

      this.gpErrHan.Text              = r.GetString ("ServiceExt_gpErrHan");            // "Error handling"

      this.colEHID.Text               = r.GetString ( "ServiceExt_colEHID");            // "ID"
      this.colEHPercentage.Text       = r.GetString ( "ServiceExt_colEHPercentage");    // "Percentage"
      this.lblEH_Hint.Text            = r.GetString ( "ServiceExt_lblEH_Hint");         // "Das Feld 'Percentage' gibt die Mindestanzahl der fehlerfreien Durchl�ufe an, damit der zugeordnete Fehler NICHT ausgel�st wird. Zum Editieren bitte doppelklicken."

      this._btnEHRead.Text            = r.GetString ( "ServiceExt_btnEHRead");          // "Read data"
      this._btnEHWrite.Text           = r.GetString ( "ServiceExt_btnEHWrite");         // "Write data"

      this.gpUserID.Text              = r.GetString ("ServiceExt_gpUserID");            // "User ID's on device EEPROM"
      this.colUID_No.Text             = r.GetString ("ServiceExt_colUID_No");           // " No."
      this.colUID_DT.Text             = r.GetString ("ServiceExt_colUID_DT");           // " Timestamp"
      this.colUID_ID.Text             = r.GetString ("ServiceExt_colUID_ID");           // " User ID"
      
      // Display SDcard data storage folder, if available
      if ( app.Data.Common.sSDcardFolder.Length > 0 ) 
      {
        this.lblStorageFolder.Text = app.Data.Common.sSDcardFolder;
      }
      
      // Update the form
      UpdateData ();

      // Request the device root folder paths
      RequestDeviceRootFolders ();

      // Read in initially the SDcard reinitialisations data
      btnSDreinitRead_Click (null, new System.EventArgs ());

      // Read in initially the  error handling data
      _btnEHRead_Click (null, new System.EventArgs ());

      // Read User ID's from device Eeprom
      ReadUserIDs();
    }

    /// <summary>
    /// 'Closed' event of the form 
    /// </summary>
    private void ServiceExtForm_Closed(object sender, System.EventArgs e)
    {
      App app = App.Instance;

      // Write app data: SDcard data storage folder
      app.Data.Common.sSDcardFolder = this.lblStorageFolder.Text;
      app.Data.Write ();
      
      // Check: Is a Transfer process still in progress?
      if ( _bTransferInProgress ) 
      {
        // Yes:
        // Check: Did we already get the name of the script, that was lastly running?
        if ( _sLastScriptName.Length > 0 ) 
        {
          // Yes:
          // Finish the transfer
          _EndTransfer ( );
        }
      }
    }
  
    /// <summary>
    /// 'KeyPress' event of the form
    /// </summary>
    private void ServiceExtForm_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
    {
      // Close the form on pressing 'Escape'
      if ( e.KeyChar == (char) Keys.Escape )
        this.Close ();
    }
    
    /// <summary>
    /// 'Click' event of the 'Storage folder' Button control
    /// </summary>
    private void btnStorageFolder_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // FolderBrowser dialog
      FolderBrowserDialog dlg = new FolderBrowserDialog ();
      
      dlg.Description  = r.GetString ("SDcardReader_Txt_SelectStorageFolder"); // "Bitte w�hlen Sie den Ordner zur Speicherung der SD-Kartendaten"
      dlg.ShowNewFolderButton = true;
      dlg.SelectedPath = this.lblStorageFolder.Text;

      if (DialogResult.OK == dlg.ShowDialog ())
      {
        // Overtake the storage folder
        this.lblStorageFolder.Text = dlg.SelectedPath;
      }
      dlg.Dispose ();
    }

    /// <summary>
    /// 'Click' event of the 'Flow/Error - Read' Button controls
    /// </summary>
    private void btnRead_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;

      // Build the message

      // --------------------------------------
      // Files

      if (sender == this.btnFlowFiles_Read)   
      {
        // The 'Flow files - Read' Button was pressed:
        // Read Flow files: "RDFLWFI\r"
        CommMessage msg = new CommMessage (comm.msgReadFlowFiles);
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
    }

    /// <summary>
    /// 'Click' event of the 'Flow/Error - Select' Button controls
    /// </summary>
    private void btnSelect_Click(object sender, System.EventArgs e)
    {
      // Get the ListBox corr'ing to the Select Button control
      ListBox lb = null;
      if      (sender == this.btnFlowFiles_Select)  lb=this.lbFlowFiles;
      // React on non-assigned or empty ListBox controls
      if (null == lb)
        return;
      if (lb.Items.Count == 0)
        return;
      // Depending on the current Button text:
      // - If 'Select all':   Select all ListBox items,   Button text: 'Unselect all'
      // - If 'Unselect all': Unselect all ListBox items, Button text: 'Select all'
      Button btn = (Button) sender;
      bool bSelect = (btn.Text == _sBTN_TEXT_SELECT_ALL) ? true : false;
      string sText = (btn.Text == _sBTN_TEXT_SELECT_ALL) ? _sBTN_TEXT_UNSELECT_ALL : _sBTN_TEXT_SELECT_ALL;
      //    Select resp. Unselect all ListBox items
      for (int i=0; i < lb.Items.Count ;i++)
        lb.SetSelected (i, bSelect);
      //    Update the Button text corr'ly
      btn.Text = sText;
    }

    /// <summary>
    /// 'SelectedIndexChanged' event of the 'Flow/Error' ListBox controls
    /// </summary>
    private void lbFiles_SelectedIndexChanged(object sender, System.EventArgs e)
    {
      // Get the Select Button corr'ing to the ListBox control  
      ListBox lb = (ListBox) sender;
      Button btn = null;
      if      (lb == this.lbFlowFiles)  btn = this.btnFlowFiles_Select;
      // React on non-assigned Button controls
      if (null == btn)
        return;
      // Get: All / Selected ListBox items
      int nAll = lb.Items.Count;
      int nSel = lb.SelectedItems.Count;
      // Update the Button text corr'ly:
      // - If all items are selected: Button text: 'Unselect all'
      // - If no items are selected:  Button text: 'Select all'
      if      (nSel == nAll)  btn.Text = _sBTN_TEXT_UNSELECT_ALL;
      else if (nSel == 0)     btn.Text = _sBTN_TEXT_SELECT_ALL;
    }
    
    /// <summary>
    /// 'CheckedChanged' event of the 'File preview' CheckBox control 
    /// </summary>
    private void chkFilePreview_CheckedChanged(object sender, System.EventArgs e)
    {
      if (!this.chkFilePreview.Checked)
      {
        // Reset transfer related controls: 'File contents'
        this.lblFileContents.Text = _sInitiallblFileContentsTxt;
        this.txtFileContents.Text = "";
      }
    }
    
    /// <summary>
    /// 'Click' event of the 'Add selected files' Button control 
    /// </summary>
    private void btnAdd_Click(object sender, System.EventArgs e)
    {
      // Add selected Flow files
      foreach (object o in this.lbFlowFiles.SelectedItems)
      {
        // Compose the device Flow file path
        string sFileName = (string) o;
        string sFilePath = Path.Combine (_sFlowFolderPath, sFileName);
        // Add the path to the ListBox
        if (-1 == this.lbAddedFiles.FindStringExact (sFilePath))
          this.lbAddedFiles.Items.Add (sFilePath);
      }
    }
    
    /// <summary>
    /// 'Click' event of the 'Remove selected file' Button control 
    /// </summary>
    private void btnRemove_Click(object sender, System.EventArgs e)
    {
      // Check: Item selected?
      int idx = this.lbAddedFiles.SelectedIndex;
      if (-1 == idx)
        return;   // No
      // Yes:
      // Remove the selected file
      this.lbAddedFiles.Items.RemoveAt (idx);
      // Reset transfer related controls: 'File contents'
      this.lblFileContents.Text = _sInitiallblFileContentsTxt;
      this.txtFileContents.Text = "";
    }
    
    
    /// <summary>
    /// 'Click' event of the 'Remove all files' Button control 
    /// </summary>
    private void btnRemoveAll_Click(object sender, System.EventArgs e)
    {
      // Remove all files
      this.lbAddedFiles.Items.Clear ();
      // Reset transfer related controls: 'File contents'
      this.lblFileContents.Text = _sInitiallblFileContentsTxt;
      this.txtFileContents.Text = "";
    }
    
    /// <summary>
    /// 'SelectedIndexChanged' event of the 'Added files' ListBox control 
    /// </summary>
    private void lbAddedFiles_SelectedIndexChanged(object sender, System.EventArgs e)
    {
      // Check: Item selected?
      int idx = this.lbAddedFiles.SelectedIndex;
      if (-1 == idx)
        return;   // No
      // Yes:
      // Check: File preview desired?
      if (this.chkFilePreview.Checked)
      {
        // Yes:
        // Indicate, that the file should NOT be stored after transmission
        _bWriteFile = false;
        // Initiate Transfer progress bar
        this.pgbTransfer.Minimum  = 0;
        this.pgbTransfer.Maximum  = 1;
        this.pgbTransfer.Value    = this.pgbTransfer.Minimum;
        // Initiate 'Transfer rate' Label
        this.lblTransferRate.Text = string.Format ("{0}/{1}", this.pgbTransfer.Value, this.pgbTransfer.Maximum); 
        
        // Indicate that the Transfer process has begun.
        _bTransferInProgress = true;
        // TX: Script Current
        //    Parameter: none
        //    Device: Action - nothing; 
        //            Returns - Name of the script currently running on the IMS device
        App app = App.Instance;
        AppComm comm = app.Comm;
        CommMessage msg = new CommMessage ( comm.msgScriptCurrent );
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
        // TX: Transfer Start
        //    Parameter: The transfer task to be performed
        //    Device: Action - Indicate, that the SD card data transfer to PC has begun; 
        //            Returns - OK
        msg = new CommMessage ( comm.msgTransferStart );
        msg.Parameter = TransferTask.SDcard_Data.ToString ( "D" );
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage ( msg );
      
        // Fill the Transfer list with the path of the file to be transferred
        this._alTrans.Clear ();
        this._alTrans.Add (this.lbAddedFiles.SelectedItem.ToString ());

        // Initiate the transfer 
        // TX: Read Text file contents: "RDTFI " + sFilePath + "\r"
        msg = new CommMessage (comm.msgReadTextFileContents);
        msg.Parameter = this._alTrans[0].ToString ();
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
    }
    
    /// <summary>
    /// 'Click' event of the 'Transfer' Button control 
    /// </summary>
    private void btnTransfer_Click(object sender, System.EventArgs e)
    {
      // Check: Are there any files to be transferred?
      if (this.lbAddedFiles.Items.Count > 0)
      {
        // Yes:
        // Indicate, that the files should be stored after transmission
        _bWriteFile = true;
        // Initiate Transfer progress bar
        this.pgbTransfer.Minimum  = 0;
        this.pgbTransfer.Maximum  = this.lbAddedFiles.Items.Count;
        this.pgbTransfer.Value    = this.pgbTransfer.Minimum;
        // Initiate 'Transfer rate' Label
        this.lblTransferRate.Text = string.Format ("{0}/{1}", this.pgbTransfer.Value, this.pgbTransfer.Maximum); 
        
        // Indicate that the Transfer process has begun.
        _bTransferInProgress = true;
        // TX: Script Current
        //    Parameter: none
        //    Device: Action - nothing; 
        //            Returns - Name of the script currently running on the IMS device
        App app = App.Instance;
        AppComm comm = app.Comm;
        CommMessage msg = new CommMessage ( comm.msgScriptCurrent );
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
        // TX: Transfer Start
        //    Parameter: The transfer task to be performed
        //    Device: Action - Indicate, that the SD card data transfer to PC has begun; 
        //            Returns - OK
        msg = new CommMessage ( comm.msgTransferStart );
        msg.Parameter = TransferTask.SDcard_Data.ToString ( "D" );
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage ( msg );
        
        // Fill the Transfer list with the paths of the files to be transferred
        this._alTrans.Clear ();
        foreach (object o in this.lbAddedFiles.Items)
        {
          this._alTrans.Add (o);
        }

        // Initiate the transfer 
        // TX: Read Text file contents: "RDTFI " + sFilePath + "\r"
        msg = new CommMessage (comm.msgReadTextFileContents);
        msg.Parameter = this._alTrans[0].ToString ();
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
    }

    /// <summary>
    /// 'CheckedChanged' event of the 'Clear' RadioButton controls 
    /// </summary>
    private void rbClear_CheckedChanged(object sender, System.EventArgs e)
    {
      RadioButton rb = (RadioButton)sender;
      if (rb.Checked)
      {
        // Set the device folder type
        if      (rb == this.rbFlowLog)     this._eDeviceFolderType = DeviceFolderType.Flow;
        // Dis-/Enable the 'Clear' button
        SetClearEnable ();
      }
    }
    
    /// <summary>
    /// 'Click' event of the 'Clear' Button control 
    /// </summary>
    private void btnClear_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;
      
      CommMessage msg;

      // Safety request: Should the root folder really be emptied?
      //    Preparation (CD regarding the sender)
      string sFmt = "";
      int nItem = 0;
      switch (this._eDeviceFolderType)
      {
        case DeviceFolderType.Flow:   sFmt = r.GetString ("ServiceExt_Que_ClearFlow"); nItem = this.lbFlowFiles.Items.Count; break;   // "Should the {0} Flow files really be cleared?"
      }
      //    Ensure, that an empty process will be performed ONLY when subfolders are present (in the sense: enumerated in the corr'ing ListBox)
      if (0 == nItem)
        return;
      //    Request
      string sMsg = string.Format (sFmt, nItem);
      string sCap = r.GetString ("Form_Common_TextSafReq");     // "Safety request"
      DialogResult dr = MessageBox.Show (this, sMsg, sCap, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
      if ( dr == DialogResult.No )
        return; // No.

      // Yes:

#if SERVICEEXT_CLEAR_WITH_INTERRUPTION      
      // Indicate that the Transfer process has begun.
      _bTransferInProgress = true;

      // TX: Script Current
      //    Parameter: none
      //    Device: Action - nothing; 
      //            Returns - Name of the script currently running on the IMS device
      msg = new CommMessage (comm.msgScriptCurrent);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      // TX: Transfer Start
      //    Parameter: The transfer task to be performed
      //    Device: Action - Indicate, that the SDcard data clear (transfer) has begun; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgTransferStart );
      msg.Parameter = TransferTask.SDcard_Data_Clear.ToString ( "D" );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );
#endif

      // Clear the subfolders on device
      //    Preparation (CD regarding the sender)
      string sRoot = "";
      switch (this._eDeviceFolderType)
      {
        case DeviceFolderType.Flow:   sRoot = _sFlowFolderPath; break;
      }
      //    Clearing
      if (sRoot.Length > 0)
      {
        // CD: The 'Flow' root folder is emptied by means of the corr'ing 'Empty' routine.
        //     Reason: See Message description in AppComm.cs.

        // Initiate Clear progress bar
        this.pgbClear.Minimum  = 0;
        this.pgbClear.Maximum  = 1;
        this.pgbClear.Value    = this.pgbClear.Minimum;
        // Disable the form controls
        UpdateData (false);
        // Empty the folder on device ("EYFLWFO"):
        // TX: Empty the Flow folder
        //    Parameter: none
        //    Device: Action - Empties the Flow folder
        //            Returns - OK
        switch (this._eDeviceFolderType)
        {
          case DeviceFolderType.Flow:   msg = new CommMessage ( comm.msgEmptyFlowFolder ); break;
        }
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
    
    }


    /// <summary>
    /// 'DoubleClick' event of the 'Percent' ListView control
    /// </summary>
    private void _lvPercent_DoubleClick(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Check, whether a row is selected or not
      int n = _lvPercent.SelectedItems.Count;
      if (0 == n) 
        return; // No.
      // Yes:
      // Get the idx of the selected entry
      ListViewItem lviSel = _lvPercent.SelectedItems[0];
      int idx = _lvPercent.SelectedIndices[0];
      // Open the ServiceExtEHForm dialog, handing over initialisation info
      ServiceExtEHForm f = new ServiceExtEHForm ();
      string sID = lviSel.Text;
      f.ID = int.Parse (sID);                           
      f.Percentage = int.Parse (lviSel.SubItems[1].Text); 
      //    Open dialog
      DialogResult dr = f.ShowDialog ();
      if (dr == DialogResult.OK) 
      {
        // Overtake
        ListViewItem lvi = new ListViewItem ();
        lvi.Text = f.ID.ToString ();                  // 'ID' is therewith the 1. subitem (index 0) of the SubItemCollection
        lvi.SubItems.Add ( f.Percentage.ToString() ); // 'Percentage' is therewith the 2. subitem (index 1) of the SubItemCollection
        // Remove the (old) entry
        _lvPercent.Items.RemoveAt (idx);
        // Insert the edited (new) entry instead
        _lvPercent.Items.Insert (idx, lvi);
        // Adjust the selection of the 'Windows' ListView and ensure its visibility
        _lvPercent.Focus ();
        _lvPercent.Items[idx].Selected = true;
        _lvPercent.Items[idx].EnsureVisible ();
      }
      f.Dispose ();
    }
    
    /// <summary>
    /// 'Click' event of the '_btnEHRead' Button control
    /// </summary>
    private void _btnEHRead_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;

      // Check: Is a Transfer process still in progress?
      if ( _bTransferInProgress ) 
        return; // Yes

      // Check: Communication channel open?
      if ( !comm.IsOpen () )
        return; // No

      // Indicate that the Transfer process has begun.
      _bTransferInProgress = true;
        
      // TX: Script Current
      //    Parameter: none
      //    Device: Action - nothing; 
      //            Returns - Name of the script currently running on the IMS device
      CommMessage msg = new CommMessage (comm.msgScriptCurrent);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      
      // TX: Transfer Start
      //    Parameter: The transfer task to be performed
      //    Device: Action - Indicate, that the Service-Read transfer has begun; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgTransferStart );
      msg.Parameter = TransferTask.Service_Read.ToString ( "D" );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );
      
      // TX: EHRead
      //    Parameter: none
      //    Device: Action - Reads the error handling data from device Eeprom;  
      //            Returns - the error handling data
      msg = new CommMessage (comm.msgEHRead);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
    
      // Clear 'Success' label
      lblEH_Success.Text = "";
    }

    /// <summary>
    /// 'Click' event of the '_btnEHWrite' Button control
    /// </summary>
    private void _btnEHWrite_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;

      // Check: Is a Transfer process still in progress?
      if ( _bTransferInProgress ) 
        return; // Yes

      // Check: Communication channel open?
      if ( !comm.IsOpen () )
        return; // No

      // Update the ServiceData members
      _UpdateEHData ( true );
        
      // Indicate that the Transfer process has begun.
      _bTransferInProgress = true;

      // TX: Script Current
      //    Parameter: none
      //    Device: Action - nothing; 
      //            Returns - Name of the script currently running on the IMS device
      CommMessage msg = new CommMessage (comm.msgScriptCurrent);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      
      // TX: Transfer Start
      //    Parameter: The transfer task to be performed
      //    Device: Action - Indicate, that the Service-Write transfer has begun; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgTransferStart );
      msg.Parameter = TransferTask.Service_Write.ToString ( "D" );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );
      
      // Init. the EH part counter
      _currEHpart = 0;

      // TX: EHWrite (Part 0)
      //    Parameter: the error handling data
      //    Device: Action - Writes the error handling data to device Eeprom; 
      //            Returns - OK
      msg = new CommMessage (comm.msgEHWrite);
      msg.Parameter = string.Format ( "{0},{1}", _currEHpart, _arsEHData[_currEHpart] );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);

      // Clear 'Success' label
      lblEH_Success.Text = "";
    }

    /// <summary>
    /// 'Click' event of the 'btnSDreinitRead' Button control
    /// </summary>
    private void btnSDreinitRead_Click (object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;

      // TX: SDGETRIN
      //    Parameter: none
      //    Device: Action - Gets the # of SDcard reinitialisations;  
      //            Returns - the # of SDcard reinitialisations
      CommMessage msg = new CommMessage (comm.msgSDGetReinitNo);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
    }

    /// <summary>
    /// 'Click' event of the 'btnSDreinitReset' Button control
    /// </summary>
    private void btnSDreinitReset_Click (object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;

      // TX: SDRESETRIN
      //    Parameter: none
      //    Device: Action - Resets the # of SDcard reinitialisations;  
      //            Returns - nothing
      CommMessage msg = new CommMessage (comm.msgSDResetReinitNo);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      // Read in the SDcard reinitialisations data
      btnSDreinitRead_Click (null, new System.EventArgs ());
    }
    
    /// <summary>
    /// 'HelpRequested' event of some controls
    /// </summary>
    /// <remarks>
    /// </remarks>
    private void _ctl_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Help requesting control
      Control requestingControl = (Control)sender;
      
      // Assign help text
      string s = "";
     
      // ---------------------------
      // SDcard section
    
      //  LB Flow files
      if (requestingControl == this.lbFlowFiles)  
        s = r.GetString ("ServiceExt_Help_lbFlowFiles");          // "Zeigt die Dateien an, die sich im Flow-Ordner des Ger�ts befinden."
        // Btn. Read Flow files
      else if (requestingControl == this.btnFlowFiles_Read)  
        s = r.GetString ("ServiceExt_Help_btnFlowFiles_Read");    // "Liest die Namen der Dateien, die sich im Flow-Ordner des Ger�ts befinden."
        // Btn. Select Flow files
      else if (requestingControl == this.btnFlowFiles_Select)  
        s = r.GetString ("ServiceExt_Help_btnFlowFiles_Select");  // "W�hlt Flow-Dateien zur �bertragung aus."
        // Btn. Storage folder
      else if (requestingControl == this.btnStorageFolder)
        s = r.GetString ("ServiceExt_Help_btnStorageFolder");     // "W�hlt den Ordner auf dem PC aus, in dem die �bertragenen Dateien gespeichert werden."
        // ChB File preview
      else if (requestingControl == this.chkFilePreview)
        s = r.GetString ("ServiceExt_Help_chkFilePreview");       // "Wenn markiert, wird der Inhalt der �bertragenen Datei angezeigt."
        // LB Added files
      else if (requestingControl == this.lbAddedFiles)
        s = r.GetString ("ServiceExt_Help_lbAddedFiles");         // "Listet die zum PC zu �bertragenden Dateien auf (Transferliste)."
        // Btn. Add
      else if (requestingControl == this.btnAdd)
        s = r.GetString ("ServiceExt_Help_btnAdd");               // "F�gt die ausgew�hlten Dateien zur Liste der zu �bertragenden Dateien hinzu."
        // Btn. Remove
      else if (requestingControl == this.btnRemove)
        s = r.GetString ("ServiceExt_Help_btnRemove");            // "Entfernt die ausgew�hlten Dateien aus der Liste der zu �bertragenden Dateien."
        // Btn. Remove all
      else if (requestingControl == this.btnRemoveAll)
        s = r.GetString ("ServiceExt_Help_btnRemoveAll");         // "Entfernt alle Dateien aus der Liste der zu �bertragenden Dateien."
        // Btn. Transfer
      else if (requestingControl == this.btnTransfer)
        s = r.GetString ("ServiceExt_Help_btnTransfer");          // "�bertr�gt die in der Transferliste enthaltenen Dateien zum PC."
        // TB File contents
      else if (requestingControl == this.txtFileContents)
        s = r.GetString ("ServiceExt_Help_txtFileContents");      // "Zeigt den Inhalt der �bertragenen Datei an."
        // Btn. Clear
      else if (requestingControl == this.btnClear)
        s = r.GetString ("ServiceExt_Help_btnClear");             // "Leert den angegebenen Ger�teordner."

        // Sdcard reinitialisations

        // Btn. Read
      else if (requestingControl == this.btnSDreinitRead)
        s = r.GetString ("ServiceExt_Help_btnSDreinitRead");      // "Gets the cumulated # of SDcard reinitialisations."
      // Btn. Reset
      else if (requestingControl == this.btnSDreinitReset)
        s = r.GetString ("ServiceExt_Help_btnSDreinitReset");     // "Resets the cumulated # of SDcard reinitialisations."
      
        // ---------------------------
        // ErrorHandling section

        // LV Percentage
      else if (requestingControl == this._lvPercent)
        s = r.GetString ("ServiceExt_Help_lvPercent");            // "Parametriert die Schwellenwerte der Ger�tefehler."
        // Btn. Read
      else if (requestingControl == this._btnEHRead)
        s = r.GetString ("ServiceExt_Help_btnEHRead");            // "Liest die Schwellenwerte der Ger�tefehler vom Ger�t."
        // Btn. Write
      else if (requestingControl == this._btnEHWrite)
        s = r.GetString ("ServiceExt_Help_btnEHWrite");           // "Schreibt die Schwellenwerte der Ger�tefehler zum Ger�t."
        
        // ---------------------------
        //  ... El resto del mundo ...

      else
        s = r.GetString ("ServiceExt_Help_Default");              // "(Kein Kommentar verf�gbar)"

      // Show help
      this._ToolTip.SetToolTip (requestingControl, s);
      hlpevent.Handled=true;
    }
    
    #endregion event handling

    #region constants & enums
 
    /// <summary>
    /// Device Folder type enumeration
    /// </summary>
    enum DeviceFolderType
    {
      None,
      Flow,     // The 'Flow' root folder
    };
    
    #endregion constants & enums

    #region members

    /// <summary>
    /// True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    bool _bShowWait = false;

    /// <summary>
    /// Indicates, whether the stored data Transfer process is curr'ly in progress ( true ) or not ( false )
    /// </summary>
    bool _bTransferInProgress = false;
    
    /// <summary>
    /// Name of the script, that was lastly running on the PID device
    /// </summary>
    string _sLastScriptName = string.Empty;

    // Device folder strings
    
    /// <summary>
    /// Path of the device Flow root folder
    /// </summary>
    string _sFlowFolderPath  = "";             
    
    // File storage

    /// <summary>
    /// Indicates, whether a transferred file should be stored (True) or not (False)
    /// </summary>
    bool _bWriteFile = false;  

    // Initial Label strings
    string _sInitialLblFilesTxt         = "Available files:";
    string _sInitiallblFileContentsTxt  = "File contents:";

    // Select Buttons
    string _sBTN_TEXT_SELECT_ALL       = "Select all";   // (Un)Select Buttons: Select all
    string _sBTN_TEXT_UNSELECT_ALL     = "Unselect all"; // (Un)Select Buttons: Unselect all

    /// <summary>
    /// The device folder type
    /// </summary>
    DeviceFolderType _eDeviceFolderType = DeviceFolderType.None;

    /// <summary>
    /// The Transfer list, which contains the paths of the files to be transferred (read)
    /// </summary>
    ArrayList _alTrans = new ArrayList ();

    // ---------------------------
    // Error handling section

    /// <summary>
    /// Error handling (EH) data string - Read
    /// </summary>
    string _sEHData = "";
    
    /// <summary>
    /// The EH data string array - Write
    /// </summary>
    string[] _arsEHData = new string[2];  
    
    /// <summary>
    /// The EH part counter - Write
    /// </summary>
    int _currEHpart = 0;

    #endregion members

    #region methods

    /// <summary>
    /// Performs initialisation tasks
    /// </summary>
    private void Init ()
    {
      // Reset all controls
      ResetAll ();
      // Initiate 'Clear' RadioButton
      this.rbFlowLog.Checked = true;
      // Assign the 'Sorted' style to the Flow files LB:
      // These files should be shown in the temporal sequence as they were stored on SDcard.
      // (Done in the UI)
    }

    /// <summary>
    /// Resets all controls
    /// </summary>
    void ResetAll ()
    {
         
      // ---------------------------
      // SD card section
   
      // Section 'Flow'
      ResetFlow ();
     
      // Section 'File transfer'
      //    Files to be transferred
      this.lbAddedFiles.Items.Clear ();
      //    Transfer cues
      this.pgbTransfer.Value = this.pgbTransfer.Minimum;
      this.lblTransferRate.Text = ""; 
      //    File contents
      this.lblFileContents.Text = _sInitiallblFileContentsTxt;
      this.txtFileContents.Text = "";
 
      // Section 'Clear'
      this.pgbClear.Value = this.pgbClear.Minimum;
  
      // ---------------------------
      // Error handling section

      this._lvPercent.Items.Clear ();
    }
 
    /// <summary>
    /// Resets the 'Flow' transfer related controls
    /// </summary>
    void ResetFlow ()
    {
      // Section 'Flow files on device'
      //    Flow files
      this.lblFlowFiles.Text = _sInitialLblFilesTxt;
      this.lbFlowFiles.Items.Clear ();
      this.btnFlowFiles_Select.Text = _sBTN_TEXT_SELECT_ALL;
 
      // Delete all entries that refer to the Flow root folder from the 'lbAddedFiles' ListBox  
      DeleteAddedEntries (this._sFlowFolderPath);    
    }
    
    /// <summary>
    /// Deletes all entries that refer to a given root folder from the 'lbAddedFiles' ListBox.
    /// </summary>
    /// <param name="sRoot">The name of a root folder</param>
    void DeleteAddedEntries (string sRoot)
    {
      if (sRoot.Length == 0)
        return;
      bool bCont=true;
      while (bCont)
      {
        bCont=false;
        foreach (object o in this.lbAddedFiles.Items)
        {
          string sFilepath = (string) o;
          if (sFilepath.StartsWith (sRoot))
          {
            this.lbAddedFiles.Items.Remove (o);
            bCont=true;
            break;
          }
        }
      }
    }
    
    /// <summary>
    /// Dis-/Enables the 'Clear' button
    /// </summary>
    void SetClearEnable ()
    {
      ListBox lb = null;
      switch (this._eDeviceFolderType)
      {
        case DeviceFolderType.Flow:   lb = this.lbFlowFiles; break;
      }
      if (null != lb)
      {
        this.btnClear.Enabled = lb.Items.Count > 0;
      }
    }
    
    /// <summary>
    /// Stores file contents to file
    /// </summary>
    /// <param name="sDeviceFilePath">The device file path</param>
    /// <param name="sFileContents">The file contents string</param>
    void WriteFile (string sDeviceFilePath, string sFileContents)
    {
      // The StreamWriter object, used for file writing
      StreamWriter file = null;
      // Build the full file path for storage on HD: = Concatenation Storage folder / Device file path
      string sFilePath = sDeviceFilePath;
      if (sFilePath.StartsWith (@"\"))  sFilePath = sFilePath.Substring (1);  // Skip leading '\' due to 'Combine' requirements
      sFilePath = Path.Combine (this.lblStorageFolder.Text, sFilePath);
      // If req., create the directory the file should be stored within
      string sDir = Path.GetDirectoryName(sFilePath);
      if (!Directory.Exists (sDir))
        Directory.CreateDirectory (sDir);
      // Store
      try 
      {
        // Wait cursor
        Cursor.Current = Cursors.WaitCursor;
        // Creates the file for writing UTF-8 encoded text
        file = File.CreateText (sFilePath);
        // Write the file contents to the specified file
        file.Write (sFileContents);
      }
      catch (System.Exception exc)
      {
        // Error:
        App app = App.Instance;
        Ressources r = app.Ressources;

        // Show corr'ing message
        string sFmt = r.GetString ("SDcardReader_Err_FileSave");      // "Fehler beim Speichern in Datei\r\n'{0}'\r\n(Detail:\r\n{1})";
        string sMsg = string.Format (sFmt, sFilePath, exc.Message);
        string sCap = r.GetString ("Form_Common_TextError");          // "Fehler";
        MessageBox.Show (this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally 
      {
        // Close the file
        if ( null != file ) 
          file.Close ();
        // Normal cursor
        Cursor.Current = Cursors.Default;
      }
    }

    /// <summary>
    /// Finishes the transfer
    /// </summary>
    void _EndTransfer ()
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Doc doc = app.Doc;

      // TX: Script Select
      // (Device: Action - Selects the script to be executed (here: script last used); TX - OK)
      CommMessage msg = new CommMessage ( comm.msgScriptSelect );
      msg.Parameter = doc.ScriptNameToIdx ( _sLastScriptName ).ToString ();
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );
      // TX: Transfer Complete
      //    Parameter: none
      //    Device: Action - Indicate, that the transfer to PC has completed; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgTransferComplete );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);

      // Indicate that the Transfer process has finished.
      _bTransferInProgress = false;

      // Reset: Transfer cues
      //    Value (position) of the Progressbar control
      this.pgbTransfer.Value = this.pgbTransfer.Minimum;
      //    'Transfer rate' Label
      this.lblTransferRate.Text = ""; 
    }

    /// <summary>
    /// Updates the form.
    /// </summary>
    void UpdateData () 
    {
      UpdateData (true);
    }
    
    /// <summary>
    /// Updates the form.
    /// </summary>
    /// <param name="bEnable">Adjusting parameter: True - Controls are enabled programmatically; 
    /// False - Controls are forced to disabled state</param>
    void UpdateData (bool bEnable) 
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Doc doc = app.Doc;

      // Primary enable, masked by the adjusting parameter
      bool bEn = ( doc.sVersion.Length > 0 ) && ( _bTransferInProgress == false ) && comm.IsOpen ();
      bEn &= bEnable;
      
      // ---------------------------
      // SD card section

      // Storage folder section
      this.gpStorageFolder.Enabled = bEn;
      
      // Text (Flow) file sections 
      bool bEnDSS = bEn && (_sFlowFolderPath.Length > 0);
      this.gpFlow.Enabled   = bEnDSS;
      if (bEnDSS)
      {
        // Special case: Dis-/Enable the Select buttons
        this.btnFlowFiles_Select.Enabled = (this.lbFlowFiles.Items.Count > 0);
      }
      
      // Transfer section
      bool bEnT = bEn && (this.lblStorageFolder.Text.Length > 0);
      this.gpTransfer.Enabled = bEnT;
      
      // Clear section
      this.gpClear.Enabled = bEnDSS;
      if (bEnDSS)
      {
        // Special case: Dis-/Enable the Clear button
        SetClearEnable ();
      }

      // SDcard reinitialisations section
      this.gpSDreinit.Enabled = bEn;
      if (this.gpSDreinit.Enabled)
      {
        if (this.lblSDreinit_val.Text.Length > 0)
        {
          int no = int.Parse (this.lblSDreinit_val.Text);
          this.btnSDreinitReset.Enabled = (no > 0) ? true : false;
        }
      }
      
      // ---------------------------
      // Error handling section

      bool bEnEH = bEn && (_sEHData.Length > 0);
      this.gpErrHan.Enabled = bEnEH;
    }

    /// <summary>
    /// Requests the device root folder paths
    /// </summary>
    void RequestDeviceRootFolders ()
    {
      App app = App.Instance;
      AppComm comm = app.Comm;

      // Read Flow root folder path: "RDFLWFO\r"
      CommMessage msg = new CommMessage (comm.msgReadFlowFolder);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
    }

    /// <summary>
    /// Updates the EH data string based on the control contents (DDX = true) or viceversa (DDX = false). 
    /// </summary>
    /// <param name="bDDX">The DDX direction</param>
    void _UpdateEHData (Boolean bDDX)
    {
      if ( bDDX )
      {
        // Controls -> Members ( WRITE (to device)):

        // Build the EH data string array
        // Notes:
        //  The EH data string array has the following format:
        //    part 0: "<id_1st>,<per_1st>,...,<id_16>,<per_16>"
        //    part 1: "<id_17>,<per_17>,...,<id_last>,<per_last>"
        string s = "";
        int currEHpart = 0;
        int nAnz = this._lvPercent.Items.Count; 
        for (int i=0; i < nAnz; i++)
        {
          ListViewItem lvi = this._lvPercent.Items[i];
          int ID = int.Parse (lvi.Text);
          int Percentage = int.Parse (lvi.SubItems[1].Text);
          if (s.Length > 0) s += ",";
          s += string.Format ("{0},{1}", ID, Percentage);
          // Check: Part 0 ready?
          if ((currEHpart == 0) && (ID >= 16))
          {
            // Yes:
            _arsEHData[currEHpart] = s;     // Write EH data string array member 0 
            currEHpart++;                   // prepare next part (Part 1)
            s = "";                          
          }
        }
        // Part 1
        _arsEHData[currEHpart] = s;         // Write EH data string array member 1    
      }
      else
      {
        // Members -> Controls ( READ (from device)):

        // Parse the EH data string and Update the control contents
        // Notes:
        //  The EH data string has the following format:
        //    "<id_1st>,<per_1st>,...,<id_last>,<per_last>"
        this._lvPercent.Items.Clear ();
        string[] ars = _sEHData.Split( ',');
        int nAnz = ars.Length;
        for (int i=0; i < nAnz; i += 2)
        {
          ListViewItem lvi = new ListViewItem ();
          lvi.Text = ars[i];
          lvi.SubItems.Add ( ars[i+1] );
          this._lvPercent.Items.Add (lvi);
        }
      }
    }

    /// <summary>
    /// Decodes the message response into the EH data string.
    /// </summary>
    /// <param name="arby">The message response</param>
    /// <returns>The EH data string</returns>
    /// <remarks>
    /// Syntax of the encoded message response (data byte array): 
    ///   '(id_1st)(per_1st)...(id_last)(per_last)', with B7 set
    /// Syntax of the decoded EH data string:     
    ///   "(id_1st),(per_1st),...,(id_last),(per_last)", with B7 reset
    /// </remarks>
    internal static string _EHStringFromBytearray (byte[] arby)
    {
      int n=arby.Length;
      string s="";
      for (int i=0; i < n; i++)
      {
        byte by = arby[i];
        by &= 0x7F;
        if (s.Length > 0) s += ",";
        s += string.Format ("{0}", by);
      }
      return s;
    }

    /// <summary>
    /// Reads the User ID's from device Eeprom.
    /// </summary>
    void ReadUserIDs()
    {
      App app = App.Instance;
      AppComm comm = app.Comm;

      // Get User ID's: "GETUSERIDS\r"
      CommMessage msg = new CommMessage(comm.msgGetUserIDs);
      msg.WindowInfo = new WndInfo(this.Handle, _bShowWait);
      comm.WriteMessage(msg);
    }
    
    /// <summary>
    /// Performs actions in reaction of the receipt of a comm. message  
    /// </summary>
    /// <param name="msgRX">The comm. message</param>
    /// <remarks>
    /// The TX/RX messages have the following formats:
    /// 
    /// In addition to the SDcard storage messages, described in 'SDcardReaderForm', the following 
    /// SDcard storage messages are used:
    /// 
    /// --------------------------
    /// Flow
    /// --------------------------
    /// 
    /// 2.1. Flow folder path request
    ///   TX
    ///     syntax:        "RDFLWFO\r"
    ///   RX
    ///     syntax:        "RDFLWFO (folder_path)\r"
    ///     example:       "RDFLWFO \Flow\r"
    ///      
    /// 2.2. Flow files request
    ///   TX
    ///     syntax:        "RDFLWCFI\r"
    ///   RX
    ///     syntax:        "RDFLWFI (file_name_1),...,(file_name_N)\r"
    ///     example:       "RDFLWFI F0000001.txt\r"
    ///     
    /// 2.3. Empty Flow folder
    ///   TX
    ///     syntax:        "EYFLWFO\r"
    ///   RX
    ///     syntax:        "EYFLWFO OK\r"
    ///     
    /// </remarks>
    public void AfterRXCompleted(CommMessage msgRX)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // CD: Message response ID
      if ((msgRX.ResponseID == AppComm.CMID_TIMEOUT) || (msgRX.ResponseID == AppComm.CMID_STRLENERR))
      {
        // A comm. error (Timeout, ...) occurred:

        // Update the form
        UpdateData ();
      }

      else if (msgRX.ResponseID == AppComm.CMID_ANSWER)
      {
        // The message response is present:
        string sCmd = msgRX.CommandResponse;
        byte[] arbyPar = msgRX.ParameterResponse;

        string sMsg = "", sCap;

        // Get the parameter string from the parameter Byte array
        string sPar = Encoding.ASCII.GetString (arbyPar);
        // CD according to the message command
        switch (sCmd) 
        {
        
            //----------------------------------------------------------
            // Common Transfer messages
            //----------------------------------------------------------

          case "SCRCURRENT":
            // ScriptCurrent
            // RX: Name of the script currently running on the device   
            try
            {
              // Get the name of the script, that was lastly running on the IMS device
              _sLastScriptName = sPar;
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptCurrent->function failed" );
            }
            break;

          case "SCRSELECT":
            // ScriptSelect
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptSelect->function failed" );
            }
            break;
        
          case "TRNSTART":
            // Transfer Start
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgTransferStart->function failed" );
            }
            break;

          case "TRNCOMPLETE":
            // Transfer Complete
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgTransferComplete->function failed" );
            }
            break;

            //----------------------------------------------------------
            // SDcard
            //----------------------------------------------------------

            // Storage messages

          case "RDFLWFO":
            // Get the path of the device Flow root folder
            // RX: The path of the device Flow root folder
            try 
            {
              // Overgive the path of the device Flow root folder
              _sFlowFolderPath = sPar;
              // Update 'Flow files' label
              string sFmt = r.GetString ("SDcardReader_Txt_AvailFiles_1");    // "Files available in '{0}':"
              this.lblFlowFiles.Text = string.Format (sFmt, _sFlowFolderPath);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadFlowFolder->function failed" );
            }
            break;  

          case "RDFLWFI":
            // Get the files of the device Flow folder
            // RX: The files of the device Flow folder
            try 
            {
              // Add the names of the files to the 'Flow files' ListBox
              string[] ars=sPar.Split (',');
              this.lbFlowFiles.Items.Clear ();
              foreach (string s in ars)
              {
                if (s.Length > 0) this.lbFlowFiles.Items.Add (s);
              }
              // Set the text of the 'Flow files - Select' Button corr'ly
              this.btnFlowFiles_Select.Text = _sBTN_TEXT_SELECT_ALL;
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadFlowFiles->function failed" );
            }
            break;  

          case "EYFLWFO":            
            // Empty Flow folder
            // RX: "111...OK" or "111...Error", where a "1" stands for each deleted item (file/(sub)folder) in the Flow root folder
            try
            {
              // Update Clear progress bar
              this.pgbClear.Value++;
              if (pgbClear.Value == this.pgbClear.Maximum)
              {

#if SERVICEEXT_CLEAR_WITH_INTERRUPTION      
                // Finish the transfer
                _EndTransfer ( );
#endif
                
                // Update affected controls
                ResetFlow ();

                // Reset Clear progress bar
                System.Threading.Thread.Sleep (1000);                     // in order to see the progress bar filling
                this.pgbClear.Value = this.pgbClear.Minimum;              // Reset
                
                // Display: Success message
                sMsg = r.GetString ( "ServiceExt_Info_FlowEmptied" );     // "Der Flow-Ordner wurde geleert."
                sCap = r.GetString ( "Form_Common_TextInfo" );            // "Information"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
              }
            }
            catch
            {
              Debug.WriteLine ( "OnMsgEmptyFlowFolder->function failed" );
            }
            break;
         
          case "RDTFI":
            // Get the file contents of a device text file
            // RX: The file contents string
            try 
            {
              // Update Transfer progress bar
              this.pgbTransfer.Value++;
              // Update 'Transfer rate' Label
              this.lblTransferRate.Text = string.Format ("{0}/{1}", this.pgbTransfer.Value, this.pgbTransfer.Maximum); 
              // Get the file path from the message lastly sent
            
              string sCurrCmd = msgRX.Command;
              string sFilePath = msgRX.Parameter;
          
#if DEBUG            
              // Testing only: Show the path of the file curr'ly received in Debug window
              string sCom = string.Format("{0}.: {1}, {2}", this.pgbTransfer.Value, sCurrCmd, sFilePath);
              Debug.WriteLine ( sCom );
#endif            
            
              // Show the file contents, if req.
              if (this.chkFilePreview.Checked)
              {
                // Update the 'File contents' Label
                string sFmt = r.GetString ("SDcardReader_Txt_FileCont_1");   // "Contents of file '{0}':"
                this.lblFileContents.Text = string.Format (sFmt, sFilePath);
                // Update the 'File contents' TextBox
                this.txtFileContents.Text = sPar;
              }
              // Store the file contents to HD, if req.
              if (_bWriteFile)
              {
                WriteFile (sFilePath, sPar);
              }         

              // Remove the path of the file curr'ly received from the Transfer list
              this._alTrans.RemoveAt (0);
              // Check: Did we read all files?
              if (this._alTrans.Count > 0)
              {
                // No:
                // Continue the transfer 
                // TX: Read Text file contents: "RDTFI " + sFilePath + "\r"
                CommMessage msg = new CommMessage (comm.msgReadTextFileContents);
                msg.Parameter = this._alTrans[0].ToString ();
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage (msg);
              }
              else
              {
                // Yes:
                int nFilesRead = this.pgbTransfer.Value;
              
                // Finish the transfer
                _EndTransfer ( );
                            
                // Display: Files succ'ly read
                string sFmt = r.GetString ( "SDcardReader_Info_FilesRead" );  // "{0} files were successfully read."
                sMsg = string.Format (sFmt, nFilesRead);
                sCap = r.GetString ( "Form_Common_TextInfo" );                // "Information"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
              }
            }
            catch (System.Exception exc)
            {
              Debug.WriteLine ( "OnMsgReadTextFileContents->function failed: {0}", exc.Message );
            }
            break;

            // SDcard reinitialisations

          case "SDGETRIN":
            // Get the # of SDcard reinitialisations
            // RX: the # of SDcard reinitialisations
            try
            {
              // Update the '# of SDcard reinitialisations' label
              this.lblSDreinit_val.Text = sPar;
            }
            catch
            {
              Debug.WriteLine ("OnMsgSDGetReinitNo->function failed");
            }
            break;

          case "SDRESETRIN":
            // Reset the # of SDcard reinitialisations
            // RX: nothing
            try
            {
              sMsg = string.Format ("SDRESETRIN: {0}", sPar);
              Debug.WriteLine (sMsg);
            }
            catch
            {
              Debug.WriteLine ("OnMsgSDResetReinitNo->function failed");
            }
            break;
          
            //----------------------------------------------------------
            // Error handling messages
            //----------------------------------------------------------
      
          case "EHREAD":
            // Read Error handling  data
            // RX: The encoded message response (data byte array)
            try 
            {
              // Decode the message response into the EH data string
              _sEHData = _EHStringFromBytearray (arbyPar);
              // Update the EH controls
              _UpdateEHData (false);
              // Set 'Success' label
              lblEH_Success.Text = r.GetString ( "ServiceExt_EHRead_Success");        // "Read: OK"

              // Finish the transfer
              _EndTransfer ();   
            }
            catch
            {
              Debug.WriteLine ( "OnMsgEHRead->function failed" );
            }
            break;  
        
          case "EHWRITE":
            // Write Error handling  data
            // RX: OK
            try 
            {
              // Check: EH data array completely TX'd?
              if (_currEHpart == 1)
              {
                // Yes:
                // Set 'Success' label
                lblEH_Success.Text = r.GetString ( "ServiceExt_EHWrite_Success");     // "Write: OK"
                
                // Finish the transfer
                _EndTransfer ();   
                break;
              }
              // No: Update EH part counter
              _currEHpart++;
              // TX: EHWrite (Part 1)
              CommMessage msg = new CommMessage (comm.msgEHWrite);
              msg.Parameter = string.Format ( "{0},{1}", _currEHpart, _arsEHData[_currEHpart] );
              msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
              comm.WriteMessage (msg);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgEHWrite->function failed" );
            }
            break;

          //----------------------------------------------------------
          // Get User ID's on device Eeprom
          //----------------------------------------------------------

          case "GETUSERIDS":
            // Get the User ID's from device Eeprom
            // RX: The User ID's with timestamp (string, TAB-separated items) with the following format:
            //        "<timestamp_1>,<id_1>(TAB) ... (TAB)<timestamp_N>,<id_N>"
            //     with: N - # of User ID's
            try
            {
              // Clear the 'User ID' LV
              this._lvUserIDs.Items.Clear();
              // Parse the parameter string
              string[] ars = sPar.Split('\t');
              // Fill the 'User ID' LV
              int nUserID = 0;
              foreach (string s in ars)
              {
                if (s.Length > 0)
                {
                  nUserID++;
                  string[] ars1 = s.Split(',');
                  ListViewItem lvi = new ListViewItem();
                  lvi.Text = nUserID.ToString();  // 'No.' is therewith the 1. subitem (index 0) of the SubItemCollection
                  lvi.SubItems.Add(ars1[0]);      // 'Timestamp' is therewith the 2. subitem (index 1) of the SubItemCollection
                  lvi.SubItems.Add(ars1[1]);      // 'User ID' is therewith the 3. subitem (index 2) of the SubItemCollection
                  this._lvUserIDs.Items.Add(lvi);
                }
              }
            }
            catch
            {
              Debug.WriteLine("OnMsgGetUserIDs->function failed");
            }
            break;

        } // E - switch ( sCmd )
    
        // Update the form
        UpdateData ();
      }//E - if (e.Message.Id = AppComm.CMID_ANSWER)

    }

    #endregion methods

    #region properties

    /// <summary>
    ///  True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    public bool ShowWait
    {
      get { return _bShowWait; }
      set { _bShowWait = value; }
    }

    #endregion properties

  }
}
