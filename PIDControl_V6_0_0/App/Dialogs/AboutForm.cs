using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace App
{
	/// <summary>
	/// Class AboutForm:
	/// Product information
	/// </summary>
	public class AboutForm : System.Windows.Forms.Form
	{
    private System.Windows.Forms.PictureBox _pbGermany;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.LinkLabel _LinkLabel;
    private System.Windows.Forms.TextBox txtVersionDev;
    private System.Windows.Forms.TextBox txtVersionPC;
    private System.Windows.Forms.Label lblVersionPC;
    private System.Windows.Forms.Label lblVersionDev;
		
    /// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

    /// <summary>
    /// Constructor
    /// </summary>
    public AboutForm()
		{
			InitializeComponent();
    }

    /// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

    /// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager (typeof (AboutForm));
      this._pbGermany = new System.Windows.Forms.PictureBox ();
      this.label1 = new System.Windows.Forms.Label ();
      this.label2 = new System.Windows.Forms.Label ();
      this.label3 = new System.Windows.Forms.Label ();
      this.label4 = new System.Windows.Forms.Label ();
      this.label5 = new System.Windows.Forms.Label ();
      this.label6 = new System.Windows.Forms.Label ();
      this._LinkLabel = new System.Windows.Forms.LinkLabel ();
      this.lblVersionPC = new System.Windows.Forms.Label ();
      this.lblVersionDev = new System.Windows.Forms.Label ();
      this.txtVersionDev = new System.Windows.Forms.TextBox ();
      this.txtVersionPC = new System.Windows.Forms.TextBox ();
      ((System.ComponentModel.ISupportInitialize)(this._pbGermany)).BeginInit ();
      this.SuspendLayout ();
      // 
      // _pbGermany
      // 
      this._pbGermany.Image = ((System.Drawing.Image)(resources.GetObject ("_pbGermany.Image")));
      this._pbGermany.Location = new System.Drawing.Point (8, 8);
      this._pbGermany.Name = "_pbGermany";
      this._pbGermany.Size = new System.Drawing.Size (86, 113);
      this._pbGermany.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this._pbGermany.TabIndex = 0;
      this._pbGermany.TabStop = false;
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point (96, 48);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size (260, 20);
      this.label1.TabIndex = 5;
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point (96, 68);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size (260, 20);
      this.label2.TabIndex = 6;
      this.label2.Text = "IUT Medical GmbH";
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point (96, 88);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size (260, 20);
      this.label3.TabIndex = 7;
      this.label3.Text = "Volmerstra�e 7B";
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point (96, 108);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size (260, 20);
      this.label4.TabIndex = 8;
      this.label4.Text = "D-12489 Berlin";
      // 
      // label5
      // 
      this.label5.Location = new System.Drawing.Point (96, 128);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size (260, 20);
      this.label5.TabIndex = 9;
      this.label5.Text = "Tel.: +49 (0) 30 201433000";
      // 
      // label6
      // 
      this.label6.Location = new System.Drawing.Point (96, 148);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size (260, 20);
      this.label6.TabIndex = 10;
      this.label6.Text = "Fax: +49 (0) 30 201433009";
      // 
      // _LinkLabel
      // 
      this._LinkLabel.Location = new System.Drawing.Point (96, 168);
      this._LinkLabel.Name = "_LinkLabel";
      this._LinkLabel.Size = new System.Drawing.Size (260, 23);
      this._LinkLabel.TabIndex = 0;
      this._LinkLabel.TabStop = true;
      this._LinkLabel.Text = "http://www.iut-medical.com";
      this._LinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler (this._LinkLabel_LinkClicked);
      // 
      // lblVersionPC
      // 
      this.lblVersionPC.Location = new System.Drawing.Point (96, 8);
      this.lblVersionPC.Name = "lblVersionPC";
      this.lblVersionPC.Size = new System.Drawing.Size (160, 20);
      this.lblVersionPC.TabIndex = 1;
      this.lblVersionPC.Text = "Version (PC):";
      // 
      // lblVersionDev
      // 
      this.lblVersionDev.Location = new System.Drawing.Point (96, 28);
      this.lblVersionDev.Name = "lblVersionDev";
      this.lblVersionDev.Size = new System.Drawing.Size (160, 20);
      this.lblVersionDev.TabIndex = 3;
      this.lblVersionDev.Text = "Version (Compatible device):";
      // 
      // txtVersionDev
      // 
      this.txtVersionDev.Location = new System.Drawing.Point (260, 28);
      this.txtVersionDev.Name = "txtVersionDev";
      this.txtVersionDev.ReadOnly = true;
      this.txtVersionDev.Size = new System.Drawing.Size (96, 20);
      this.txtVersionDev.TabIndex = 4;
      this.txtVersionDev.Text = "<version_dev>";
      // 
      // txtVersionPC
      // 
      this.txtVersionPC.Location = new System.Drawing.Point (260, 8);
      this.txtVersionPC.Name = "txtVersionPC";
      this.txtVersionPC.ReadOnly = true;
      this.txtVersionPC.Size = new System.Drawing.Size (96, 20);
      this.txtVersionPC.TabIndex = 2;
      this.txtVersionPC.Text = "<version_PC>";
      // 
      // AboutForm
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size (5, 13);
      this.ClientSize = new System.Drawing.Size (362, 192);
      this.Controls.Add (this.txtVersionPC);
      this.Controls.Add (this.txtVersionDev);
      this.Controls.Add (this.lblVersionDev);
      this.Controls.Add (this.lblVersionPC);
      this.Controls.Add (this._LinkLabel);
      this.Controls.Add (this.label6);
      this.Controls.Add (this.label5);
      this.Controls.Add (this.label4);
      this.Controls.Add (this.label3);
      this.Controls.Add (this.label2);
      this.Controls.Add (this.label1);
      this.Controls.Add (this._pbGermany);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "AboutForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "About";
      this.Load += new System.EventHandler (this.AboutForm_Load);
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler (this.AboutForm_KeyPress);
      ((System.ComponentModel.ISupportInitialize)(this._pbGermany)).EndInit ();
      this.ResumeLayout (false);
      this.PerformLayout ();

    }
		
    #endregion

    #region events

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void AboutForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      Ressources r = app.Ressources;
      
      // Resources
      this.Text                = r.GetString ( "About_Title" );              // "Produktinformation"
      this.lblVersionPC.Text   = r.GetString ( "About_lblVersionPCKey" );    // "Version (PC):"
      this.lblVersionDev.Text  = r.GetString ( "About_lblVersionDevKey" );   // "Version (kompatibles Ger�t):"
      
      // Version - PC SW
      // Notes: 
      //  The corr'ing AssemblyInfo entries are used.
      string sPCVersion = Application.ProductVersion;
      //  Get the Major, Minor & Build numbers of the product version
      string[] ars = sPCVersion.Split ('.');
      try 
      {
        sPCVersion = string.Format ("{0}.{1}.{2}", 
          int.Parse(ars[0]), 
          int.Parse(ars[1]), 
          int.Parse(ars[2])
          );
      }
      catch {}
      //  Version info
      this.txtVersionPC.Text = sPCVersion;
      
      // Version - device SW
      // Notes:
      //  The string array 'VERSIONSTRING' is used.
      string sDevVersion = "";
      string s1;
      foreach (string s in doc.VERSIONSTRING)
      {
        ars = s.Split ('.');
        try 
        {
          s1 = string.Format ("{0}.{1}.{2}", 
            int.Parse(ars[0]), 
            int.Parse(ars[1]), 
            int.Parse(ars[2])
            );
        }
        catch 
        {
          s1 = s; 
        }
        sDevVersion += s1 + ", ";
      }
      //  Version info (without trailing comma)
      sDevVersion = sDevVersion.Trim();
      this.txtVersionDev.Text = sDevVersion.Substring (0, sDevVersion.Length-1);
    }

    /// <summary>
    /// 'KeyPress' event of the form
    /// </summary>
    private void AboutForm_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
    {
      // Close the form on pressing 'Enter' or 'Escape'
      if ( e.KeyChar == (char) Keys.Enter || e.KeyChar == (char) Keys.Escape )
        this.Close ();
    }
	
    /// <summary>
    /// 'LinkClicked' event of the _LinkLabel control
    /// </summary>
    private void _LinkLabel_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
    {
      // Get the target ( here: the ENIT-URL )
      string target = _LinkLabel.Text;

      // Navigate to it
      try
      {
        System.Diagnostics.Process.Start( target );
      }
      catch
      {    
        Win.MessageBeep ( Win.BeepType.IconHand );
      }
    
    }

    #endregion // events

    #region members
    #endregion // members

   }
}
