// If defined, a script sequence entry is "correct", if it is unique.
// If not defined, a script sequence entry is always "correct".
// #define CTS_UNIQUE_ENTRY

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using System.Diagnostics;
using System.Threading;

using CommRS232;

namespace App
{
	/// <summary>
	/// Class CTSForm:
	/// Clock-timed scripts Control
	/// </summary>
	/// <remarks>
	/// </remarks>
	public class CTSForm : System.Windows.Forms.Form
	{
    private System.Windows.Forms.Button _btnRead;
    private System.Windows.Forms.Button _btnWrite;
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.ProgressBar _pgbTransfer;
    private System.Windows.Forms.ToolTip _ToolTip;
    private System.Windows.Forms.Timer _Timer;
    private System.Windows.Forms.CheckBox _chkCTSEnable;
    private System.Windows.Forms.Label _lblStartInterval;
    private System.Windows.Forms.TextBox _txtStartInterval;
    private System.Windows.Forms.Label _lblStartUnit;
    private System.Windows.Forms.Button _btnRemove;
    private System.Windows.Forms.Button _btnEdit;
    private System.Windows.Forms.Button _btnAdd;
    private System.Windows.Forms.ListView _lvScripts;
    private System.Windows.Forms.ColumnHeader colScript;
    private System.Windows.Forms.ColumnHeader colUnit;
    private System.Windows.Forms.ComboBox _cbStartUnit;
    private System.Windows.Forms.GroupBox _gbCTSHandling;
    private System.Windows.Forms.GroupBox _gbCTSSettings;
    private System.Windows.Forms.ColumnHeader colRunningPeriod;
    private System.Windows.Forms.Label _lblStart;
    private System.Windows.Forms.Button _btnUp;
    private System.Windows.Forms.Button _btnDown;
    private System.ComponentModel.IContainer components;

		/// <summary>
		/// Constructor
		/// </summary>
    public CTSForm()
		{
			InitializeComponent();
      
      // Init
      Init ();
    }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		
    /// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.components = new System.ComponentModel.Container();
      System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(CTSForm));
      this._btnRead = new System.Windows.Forms.Button();
      this._btnWrite = new System.Windows.Forms.Button();
      this._btnCancel = new System.Windows.Forms.Button();
      this._pgbTransfer = new System.Windows.Forms.ProgressBar();
      this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
      this._Timer = new System.Windows.Forms.Timer(this.components);
      this._gbCTSHandling = new System.Windows.Forms.GroupBox();
      this._lblStart = new System.Windows.Forms.Label();
      this._cbStartUnit = new System.Windows.Forms.ComboBox();
      this._lblStartUnit = new System.Windows.Forms.Label();
      this._txtStartInterval = new System.Windows.Forms.TextBox();
      this._lblStartInterval = new System.Windows.Forms.Label();
      this._chkCTSEnable = new System.Windows.Forms.CheckBox();
      this._gbCTSSettings = new System.Windows.Forms.GroupBox();
      this._btnDown = new System.Windows.Forms.Button();
      this._btnUp = new System.Windows.Forms.Button();
      this._btnRemove = new System.Windows.Forms.Button();
      this._btnEdit = new System.Windows.Forms.Button();
      this._btnAdd = new System.Windows.Forms.Button();
      this._lvScripts = new System.Windows.Forms.ListView();
      this.colScript = new System.Windows.Forms.ColumnHeader();
      this.colRunningPeriod = new System.Windows.Forms.ColumnHeader();
      this.colUnit = new System.Windows.Forms.ColumnHeader();
      this._gbCTSHandling.SuspendLayout();
      this._gbCTSSettings.SuspendLayout();
      this.SuspendLayout();
      // 
      // _btnRead
      // 
      this._btnRead.Location = new System.Drawing.Point(85, 304);
      this._btnRead.Name = "_btnRead";
      this._btnRead.Size = new System.Drawing.Size(96, 23);
      this._btnRead.TabIndex = 3;
      this._btnRead.Text = "Read Data";
      this._btnRead.Click += new System.EventHandler(this._btnRead_Click);
      this._btnRead.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnWrite
      // 
      this._btnWrite.Location = new System.Drawing.Point(201, 304);
      this._btnWrite.Name = "_btnWrite";
      this._btnWrite.Size = new System.Drawing.Size(96, 23);
      this._btnWrite.TabIndex = 4;
      this._btnWrite.Text = "Daten schreiben";
      this._btnWrite.Click += new System.EventHandler(this._btnWrite_Click);
      this._btnWrite.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnCancel
      // 
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point(317, 304);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.Size = new System.Drawing.Size(96, 23);
      this._btnCancel.TabIndex = 5;
      this._btnCancel.Text = "Cancel";
      // 
      // _pgbTransfer
      // 
      this._pgbTransfer.Location = new System.Drawing.Point(86, 272);
      this._pgbTransfer.Name = "_pgbTransfer";
      this._pgbTransfer.Size = new System.Drawing.Size(327, 20);
      this._pgbTransfer.TabIndex = 2;
      // 
      // _ToolTip
      // 
      this._ToolTip.AutoPopDelay = 10000;
      this._ToolTip.InitialDelay = 500;
      this._ToolTip.ReshowDelay = 100;
      // 
      // _Timer
      // 
      this._Timer.Interval = 500;
      this._Timer.Tick += new System.EventHandler(this._Timer_Tick);
      // 
      // _gbCTSHandling
      // 
      this._gbCTSHandling.Controls.Add(this._lblStart);
      this._gbCTSHandling.Controls.Add(this._cbStartUnit);
      this._gbCTSHandling.Controls.Add(this._lblStartUnit);
      this._gbCTSHandling.Controls.Add(this._txtStartInterval);
      this._gbCTSHandling.Controls.Add(this._lblStartInterval);
      this._gbCTSHandling.Controls.Add(this._chkCTSEnable);
      this._gbCTSHandling.Location = new System.Drawing.Point(8, 176);
      this._gbCTSHandling.Name = "_gbCTSHandling";
      this._gbCTSHandling.Size = new System.Drawing.Size(480, 84);
      this._gbCTSHandling.TabIndex = 1;
      this._gbCTSHandling.TabStop = false;
      this._gbCTSHandling.Text = "Aktivierung";
      // 
      // _lblStart
      // 
      this._lblStart.Location = new System.Drawing.Point(180, 20);
      this._lblStart.Name = "_lblStart";
      this._lblStart.Size = new System.Drawing.Size(288, 28);
      this._lblStart.TabIndex = 1;
      this._lblStart.Text = "Zeitinterval bis zum Start des 1. Scripts:";
      this._lblStart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // _cbStartUnit
      // 
      this._cbStartUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this._cbStartUnit.Location = new System.Drawing.Point(388, 52);
      this._cbStartUnit.Name = "_cbStartUnit";
      this._cbStartUnit.Size = new System.Drawing.Size(84, 21);
      this._cbStartUnit.TabIndex = 5;
      this._cbStartUnit.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblStartUnit
      // 
      this._lblStartUnit.Location = new System.Drawing.Point(324, 52);
      this._lblStartUnit.Name = "_lblStartUnit";
      this._lblStartUnit.Size = new System.Drawing.Size(60, 23);
      this._lblStartUnit.TabIndex = 4;
      this._lblStartUnit.Text = "Einheit:";
      this._lblStartUnit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // _txtStartInterval
      // 
      this._txtStartInterval.Location = new System.Drawing.Point(244, 52);
      this._txtStartInterval.Name = "_txtStartInterval";
      this._txtStartInterval.Size = new System.Drawing.Size(64, 20);
      this._txtStartInterval.TabIndex = 3;
      this._txtStartInterval.Text = "";
      this._txtStartInterval.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblStartInterval
      // 
      this._lblStartInterval.Location = new System.Drawing.Point(180, 52);
      this._lblStartInterval.Name = "_lblStartInterval";
      this._lblStartInterval.Size = new System.Drawing.Size(60, 23);
      this._lblStartInterval.TabIndex = 2;
      this._lblStartInterval.Text = "Wert:";
      this._lblStartInterval.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // _chkCTSEnable
      // 
      this._chkCTSEnable.Location = new System.Drawing.Point(8, 20);
      this._chkCTSEnable.Name = "_chkCTSEnable";
      this._chkCTSEnable.Size = new System.Drawing.Size(164, 28);
      this._chkCTSEnable.TabIndex = 0;
      this._chkCTSEnable.Text = "Enable clock-timed scripts handling";
      this._chkCTSEnable.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _gbCTSSettings
      // 
      this._gbCTSSettings.Controls.Add(this._btnDown);
      this._gbCTSSettings.Controls.Add(this._btnUp);
      this._gbCTSSettings.Controls.Add(this._btnRemove);
      this._gbCTSSettings.Controls.Add(this._btnEdit);
      this._gbCTSSettings.Controls.Add(this._btnAdd);
      this._gbCTSSettings.Controls.Add(this._lvScripts);
      this._gbCTSSettings.Location = new System.Drawing.Point(8, 8);
      this._gbCTSSettings.Name = "_gbCTSSettings";
      this._gbCTSSettings.Size = new System.Drawing.Size(480, 164);
      this._gbCTSSettings.TabIndex = 0;
      this._gbCTSSettings.TabStop = false;
      this._gbCTSSettings.Text = "Script sequence";
      // 
      // _btnDown
      // 
      this._btnDown.Image = ((System.Drawing.Image)(resources.GetObject("_btnDown.Image")));
      this._btnDown.Location = new System.Drawing.Point(420, 124);
      this._btnDown.Name = "_btnDown";
      this._btnDown.Size = new System.Drawing.Size(20, 20);
      this._btnDown.TabIndex = 5;
      this._btnDown.Click += new System.EventHandler(this._btnUpDown_Click);
      this._btnDown.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnUp
      // 
      this._btnUp.Image = ((System.Drawing.Image)(resources.GetObject("_btnUp.Image")));
      this._btnUp.Location = new System.Drawing.Point(388, 124);
      this._btnUp.Name = "_btnUp";
      this._btnUp.Size = new System.Drawing.Size(20, 20);
      this._btnUp.TabIndex = 4;
      this._btnUp.Click += new System.EventHandler(this._btnUpDown_Click);
      this._btnUp.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnRemove
      // 
      this._btnRemove.Location = new System.Drawing.Point(368, 88);
      this._btnRemove.Name = "_btnRemove";
      this._btnRemove.Size = new System.Drawing.Size(96, 23);
      this._btnRemove.TabIndex = 3;
      this._btnRemove.Text = "Remove";
      this._btnRemove.Click += new System.EventHandler(this._btnRemove_Click);
      this._btnRemove.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnEdit
      // 
      this._btnEdit.Location = new System.Drawing.Point(368, 56);
      this._btnEdit.Name = "_btnEdit";
      this._btnEdit.Size = new System.Drawing.Size(96, 23);
      this._btnEdit.TabIndex = 2;
      this._btnEdit.Text = "Edit";
      this._btnEdit.Click += new System.EventHandler(this._btnEdit_Click);
      this._btnEdit.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnAdd
      // 
      this._btnAdd.Location = new System.Drawing.Point(368, 24);
      this._btnAdd.Name = "_btnAdd";
      this._btnAdd.Size = new System.Drawing.Size(96, 23);
      this._btnAdd.TabIndex = 1;
      this._btnAdd.Text = "Add";
      this._btnAdd.Click += new System.EventHandler(this._btnAdd_Click);
      this._btnAdd.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lvScripts
      // 
      this._lvScripts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
                                                                                 this.colScript,
                                                                                 this.colRunningPeriod,
                                                                                 this.colUnit});
      this._lvScripts.FullRowSelect = true;
      this._lvScripts.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
      this._lvScripts.HideSelection = false;
      this._lvScripts.Location = new System.Drawing.Point(8, 24);
      this._lvScripts.MultiSelect = false;
      this._lvScripts.Name = "_lvScripts";
      this._lvScripts.Size = new System.Drawing.Size(348, 132);
      this._lvScripts.TabIndex = 0;
      this._lvScripts.View = System.Windows.Forms.View.Details;
      this._lvScripts.DoubleClick += new System.EventHandler(this._lvScripts_DoubleClick);
      this._lvScripts.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // colScript
      // 
      this.colScript.Text = "Script";
      this.colScript.Width = 180;
      // 
      // colRunningPeriod
      // 
      this.colRunningPeriod.Text = "Laufzeit";
      this.colRunningPeriod.Width = 100;
      // 
      // colUnit
      // 
      this.colUnit.Text = "Unit";
      // 
      // CTSForm
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.CancelButton = this._btnCancel;
      this.ClientSize = new System.Drawing.Size(498, 340);
      this.Controls.Add(this._gbCTSSettings);
      this.Controls.Add(this._gbCTSHandling);
      this.Controls.Add(this._pgbTransfer);
      this.Controls.Add(this._btnCancel);
      this.Controls.Add(this._btnWrite);
      this.Controls.Add(this._btnRead);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.HelpButton = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "CTSForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Clock-timed scripts Control";
      this.Load += new System.EventHandler(this.CTSForm_Load);
      this.Closed += new System.EventHandler(this.CTSForm_Closed);
      this._gbCTSHandling.ResumeLayout(false);
      this._gbCTSSettings.ResumeLayout(false);
      this.ResumeLayout(false);

    }
		
    #endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void CTSForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Resources
      this.Text                   = r.GetString ( "CTS_Title" );                // "Clock-timed scripts control"
      
      this._gbCTSSettings.Text    = r.GetString ( "CTS_gbCTSSettings" );        // "Script sequence"
      this.colScript.Text         = r.GetString ( "CTS_colScript" );            // "Script"
      this.colRunningPeriod.Text  = r.GetString ( "CTS_colRunningPeriod" );     // "Laufzeit"
      this.colUnit.Text           = r.GetString ( "CTS_colUnit" );              // "Unit"
      this._btnAdd.Text           = r.GetString ( "CTS_btnAdd" );               // "Add ..."
      this._btnEdit.Text          = r.GetString ( "CTS_btnEdit" );              // "Edit ..."
      this._btnRemove.Text        = r.GetString ( "CTS_btnRemove" );            // "Remove"
      
      this._gbCTSHandling.Text    = r.GetString ( "CTS_gbCTSHandling" );        // "Aktivierung"
      this._chkCTSEnable.Text     = r.GetString ( "CTS_chkCTSEnable" );         // "Enable clock-timed scripts handling"
      this._lblStart.Text         = r.GetString ( "CTS_lblStart" );             // Zeitinterval bis zum Start des 1. Scripts:
      this._lblStartInterval.Text = r.GetString ( "CTS_lblStartInterval" );     // "Wert:"
      this._lblStartUnit.Text     = r.GetString ( "CTS_lblStartUnit" );         // "Einheit:"

      this._btnRead.Text          = r.GetString ( "Service_btnRead" );          // "Daten lesen"
      this._btnWrite.Text         = r.GetString ( "Service_btnWrite" );         // "Daten schreiben"
      this._btnCancel.Text        = r.GetString ( "Service_btnCancel" );        // "Abbruch"
      
      // Read in initially the CTS data
      _btnRead_Click (null, new System.EventArgs ());

      // Timer enable
      _Timer.Enabled = true;
    }

    
    /// <summary>
    /// 'Closed' event of the form
    /// </summary>
    private void CTSForm_Closed(object sender, System.EventArgs e)
    {
      // Stop timer
      _Timer.Enabled = false;

      // Check: Is a Transfer process still in progress?
      if ( _bTransferInProgress ) 
      {
        // Yes:
        // Check: Did we already get the name of the script, that was lastly running?
        if ( _sLastScriptName.Length > 0 ) 
        {
          // Yes:

          // Finish the transfer
          _EndTransfer ();
        }
      }
    }
    
 
    /// <summary>
    /// 'Click' event of the 'Add' button control
    /// </summary>
    private void _btnAdd_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Check: At most MAX_CLOCKTIMEDSCRIPTS rows should be edited!
      if (_lvScripts.Items.Count >= Doc.MAX_CLOCKTIMEDSCRIPTS)
      {
        // Show message
        string fmt = r.GetString ("CTS_btnAdd_Info_MaxClockTimedScripts");  // "At most {0} rows can be edited.\r\nThis number is already reached, thats why a further edition is impossible."
        string sMsg = string.Format (fmt, Doc.MAX_CLOCKTIMEDSCRIPTS);
        string sCap = r.GetString ("Form_Common_TextInfo");                 // "Information"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
        return;
      }
      // Add
      CTSElementForm f = new CTSElementForm ();
      DialogResult dr = f.ShowDialog ();
      if (dr == DialogResult.OK) 
      {
        // Overtake
        ListViewItem lvi = new ListViewItem ();
        lvi.Text = f.Script;                              // 'Script' is therewith the 1. subitem (index 0) of the SubItemCollection
        lvi.SubItems.Add ( f.RunningPeriod.ToString() );  // 'RunningPeriod' is therewith the 2. subitem (index 1) of the SubItemCollection
        lvi.SubItems.Add ( f.Unit );                      // ...
        
        // Check for entry correctness:
        // Assume, that the entry is correct
        bool bCorrectEntry = true;
#if CTS_UNIQUE_ENTRY
        // Check: Unique entry?
        // Note: Two entries are considered as non-unique, if their 'Script' members coincide.
        for (int i=0; i < _lvScripts.Items.Count ;i++) 
        {
          ListViewItem lvi1 = _lvScripts.Items[i];
          if (lvi1.Text.ToUpper() == lvi.Text.ToUpper())
          {
            // No: 
            // Show message
            string fmt = r.GetString ("ScriptEditor_btnWindow_Add_Error_EntryExists");  // "An entry for '{0}' does already exist.\r\nThe new entry will not be added."
            string sMsg = string.Format (fmt, lvi.Text);
            string sCap = r.GetString ("Form_Common_TextError");                        // "Fehler"
            MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
            // Indicate, that the entry already exists (and is therefore not correct)  
            bCorrectEntry = false;
            break;
          }
        }
#endif        
        if (bCorrectEntry) 
        {
          // Correct entry:
          // Add it to the list
          _lvScripts.Items.Add (lvi);
          // Adjust the selection of the ListView and ensure its visibility
          _lvScripts.Focus ();
          _lvScripts.Items[_lvScripts.Items.Count-1].Selected = true;
          _lvScripts.Items[_lvScripts.Items.Count-1].EnsureVisible ();
        }
      }
      f.Dispose ();
    }

    /// <summary>
    /// 'Click' event of the 'Edit' button control
    /// </summary>
    private void _btnEdit_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Check, whether a Script row is selected or not
      int n = _lvScripts.SelectedItems.Count;
      if (0 == n) 
        return; // No.
      // Yes:
      // Get the idx of the selected entry
      ListViewItem lviSel = _lvScripts.SelectedItems[0];
      int idx = _lvScripts.SelectedIndices[0];
      // Open the CTSElementForm dialog, handing over initialisation info
      CTSElementForm f = new CTSElementForm ();
      f.Script        = lviSel.Text;                          // 'Script' is at the same time the 1. subitem (index 0) of the SubItemCollection
      f.RunningPeriod = int.Parse (lviSel.SubItems[1].Text);  // 'RunningPeriod' is the 2. subitem (index 1) of the SubItemCollection
      f.Unit          = lviSel.SubItems[2].Text;              // ...
      DialogResult dr = f.ShowDialog ();
      if (dr == DialogResult.OK) 
      {
        // Overtake
        ListViewItem lvi = new ListViewItem ();
        lvi.Text = f.Script;
        lvi.SubItems.Add ( f.RunningPeriod.ToString() );
        lvi.SubItems.Add ( f.Unit );
        
        // Check for entry correctness:
        // Assume, that the entry is correct
        bool bCorrectEntry = true;
#if CTS_UNIQUE_ENTRY
        // Check: Unique entry?
        // Note: Two entries are considered as non-unique, if their 'Script' members coincide.
        for (int i=0; i < _lvScripts.Items.Count ;i++) 
        {
          if (i != idx) // Do not consider the selected entry itself 
          {
            ListViewItem lvi1 = _lvScripts.Items[i];
            if (lvi1.Text.ToUpper() == lvi.Text.ToUpper())
            {
              // No: 
              // Show message
              string fmt = r.GetString ("ScriptEditor_btnWindow_Edit_Error_EntryExists"); // "An entry for '{0}' does already exist.\r\nThe edited entry will be rejected."
              string sMsg = string.Format (fmt, lvi.Text);
              string sCap = r.GetString ("Form_Common_TextError");                        // "Fehler"
              MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
              // Indicate, that the entry already exists (and is therefore not correct)  
              bCorrectEntry = false;
              break;
            }
          }
        }
#endif 
        if (bCorrectEntry) 
        {
          // Correct entry:
          // Remove the (old) entry
          _lvScripts.Items.RemoveAt (idx);
          // Insert the edited (new) entry instead
          _lvScripts.Items.Insert (idx, lvi);
          // Adjust the selection of the ListView and ensure its visibility
          _lvScripts.Focus ();
          _lvScripts.Items[idx].Selected = true;
          _lvScripts.Items[idx].EnsureVisible ();
        }
      }
      f.Dispose ();
    }

    /// <summary>
    /// 'DoubleClick' event of the ListView control
    /// </summary>
    private void _lvScripts_DoubleClick(object sender, System.EventArgs e)
    {
      // Start the 'Edit' action
      _btnEdit_Click(null, new System.EventArgs ());
    }
   
    /// <summary>
    /// 'Click' event of the 'Remove' button control
    /// </summary>
    private void _btnRemove_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      // Check, whether a row is selected or not
      int n = _lvScripts.SelectedItems.Count;
      if (0 == n) 
        return; // No.
      // Yes:
      // Safety request
      ListViewItem lviSel = _lvScripts.SelectedItems[0];
      string s = "";
      for (int i=0; i < Math.Min (3, lviSel.SubItems.Count) ;i++)
      {
        s += string.Format (" {0}", lviSel.SubItems[i].Text);
      }
      if (lviSel.SubItems.Count > 3) s += " ...";
      string fmt = r.GetString ("ScriptEditor_btnRemove_Que_Delete");  // "Should the entry\r\n'{0}'\r\nreally be deleted?"
      string sMsg = string.Format (fmt, s);
      string sCap = r.GetString ("Form_Common_TextSafReq");            // "Sicherheitsabfrage"
      DialogResult dr = MessageBox.Show (sMsg, sCap, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
      if (dr == DialogResult.No)
        return; // No.
      // Yes:
      // Remove the entry from the ListView
      int idx = _lvScripts.SelectedIndices[0];
      _lvScripts.Items.RemoveAt (idx);
      // Select the next entry
      n = _lvScripts.Items.Count;
      if (n > 0)
      {
        _lvScripts.Focus ();
        if (idx < n-1) _lvScripts.Items[idx].Selected = true;
        else           _lvScripts.Items[n-1].Selected = true;
      }
    }

    /// <summary>
    /// 'Click' event of the Up/Down Button controls
    /// </summary>
    private void _btnUpDown_Click(object sender, System.EventArgs e)
    {
      try
      {
        Button btn = (Button)sender;
        ListViewItem lviSel = _lvScripts.SelectedItems[0];
        int idx = _lvScripts.SelectedIndices[0];
        if (btn == this._btnUp)
        {
          // Up
          if (idx > 0)
          {
            _lvScripts.Items.RemoveAt (idx);
            idx--;
            _lvScripts.Items.Insert (idx, lviSel);
            _lvScripts.Focus ();
            _lvScripts.Items[idx].Selected = true;
          }
        }
        else if (btn == this._btnDown)
        {
          // Down
          if (idx < _lvScripts.Items.Count-1)
          {
            _lvScripts.Items.RemoveAt (idx);
            idx++;
            _lvScripts.Items.Insert (idx, lviSel);
            _lvScripts.Focus ();
            _lvScripts.Items[idx].Selected = true;
          }
        }
      }
      catch
      {}
    }
    
    /// <summary>
    /// 'Click' event of the '_btnRead' Button control
    /// </summary>
    /// <remarks>
    /// Reads in (i.e. transfers from device to PC) the CTS data and updates the control
    /// contents corr'ly
    /// </remarks>
    private void _btnRead_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;

      // Check: Is a Transfer process still in progress?
      if ( _bTransferInProgress ) 
        return; // Yes

      // Check: Communication channel open?
      if ( !comm.IsOpen () )
        return; // No

      // Indicate that the Transfer process has begun.
      _bTransferInProgress = true;
        
      // TX: Script Current
      //    Parameter: none
      //    Device: Action - nothing; 
      //            Returns - Name of the script currently running on the IMS device
      CommMessage msg = new CommMessage (comm.msgScriptCurrent);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      
      // TX: Transfer Start
      //    Parameter: The transfer task to be performed
      //    Device: Action - Indicate, that the CTS-Read transfer has begun; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgTransferStart );
      msg.Parameter = TransferTask.CTS_Read.ToString ( "D" );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );
      
      // TX: CTS Read
      //    Parameter: none
      //    Device: Action - nothing; 
      //            Returns - the clock-timed script data from the device
      msg = new CommMessage (comm.msgCTSRead);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);

      // Init. progress bar
      this._pgbTransfer.Minimum = 0;
      this._pgbTransfer.Maximum = 1;
      this._pgbTransfer.Value = 0;
    }

    /// <summary>
    /// 'Click' event of the '_btnWrite' Button control
    /// </summary>
    /// <remarks>
    /// Writes (i.e. transfers from PC to device) the CTS data
    /// </remarks>
    private void _btnWrite_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;

      // Check: Is a Transfer process still in progress?
      if ( _bTransferInProgress ) 
        return; // Yes

      // Check: Communication channel open?
      if ( !comm.IsOpen () )
        return; // No

      // Check the validity of the control contents
      if ( !_CheckControls() )
        return;
      
      // Update the CTS data
      _UpdateCTSData ( true );
        
      // Indicate that the Transfer process has begun.
      _bTransferInProgress = true;

      // TX: Script Current
      //    Parameter: none
      //    Device: Action - nothing; 
      //            Returns - Name of the script currently running on the IMS device
      CommMessage msg = new CommMessage (comm.msgScriptCurrent);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      
      // TX: Transfer Start
      //    Parameter: The transfer task to be performed
      //    Device: Action - Indicate, that the CTS-Write transfer has begun; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgTransferStart );
      msg.Parameter = TransferTask.CTS_Write.ToString ( "D" );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );
      
      // TX: CTS Write
      //    Parameter: The data string
      //    Device: Action - Actualizes the clock-timed script data on the device 
      //            Returns - OK
      msg = new CommMessage (comm.msgCTSWrite);
      msg.Parameter = _sCTSdata;
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
 
      // Init. progress bar
      this._pgbTransfer.Minimum = 0;
      this._pgbTransfer.Maximum = 1;
      this._pgbTransfer.Value = 0;
    }

    /// <summary>
    /// 'HelpRequested' event of some controls
    /// </summary>
    /// <remarks>
    /// </remarks>
    private void _ctl_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Help requesting control
      Control requestingControl = (Control)sender;
      
      // Assign help text
      string s = "";
      
      // ---------------------------
      // Form level

      // Button Read
      if      (requestingControl == this._btnRead)
        s = r.GetString ("CTS_Help_btnRead");           // "Liest die Daten vom Eeprom des Ger�ts."
        // Button Write
      else if (requestingControl == this._btnWrite)
        s = r.GetString ("CTS_Help_btnWrite");          // "Schreibt die Daten auf den Eeprom des Ger�ts."

      // -----------------------------------------
      // 'Script sequence' section
      
        // LV Scripts
      else if (requestingControl == this._lvScripts)
        s = r.GetString ("CTS_Help_lvScripts");          // "Verwaltet die Liste der zeitgebergest�tzten Scripts."
        // Button Add
      else if (requestingControl == this._btnAdd)
        s = r.GetString ("CTS_Help_btnAdd");             // "F�gt einen Script zur Liste hinzu."
        // Button Edit
      else if (requestingControl == this._btnEdit)
        s = r.GetString ("CTS_Help_btnEdit");            // "Editiert den ausgew�hlten Script."
        // Button Remove
      else if (requestingControl == this._btnRemove)
        s = r.GetString ("CTS_Help_btnRemove");          // "L�scht den ausgew�hlten Script von der Liste."
        // Button Up
      else if (requestingControl == this._btnUp)
        s = r.GetString ("CTS_Help_btnUp");              // "Verschiebt den ausgew�hlten Script aufw�rts in der Liste."
        // Button Down
      else if (requestingControl == this._btnDown)
        s = r.GetString ("CTS_Help_btnDown");            // "Verschiebt den ausgew�hlten Script abw�rts in der Liste."
      
      // -----------------------------------------
      // 'Activation' section
      
        // ChB Enable CTS handling
      else if (requestingControl == this._chkCTSEnable)
        s = r.GetString ("CTS_Help_chkCTSEnable");       // "Wenn markiert/nicht markiert, ist das zeitgebergest�tzte Scripthandling aktiviert/deaktiviert."
        // TB Value
      else if (requestingControl == this._txtStartInterval)
        s = r.GetString ("CTS_Help_txtStartInterval");   // "Zeitintervall bis zum Start des 1. Scripts (Wert)."
        // CB Unit
      else if (requestingControl == this._cbStartUnit)
        s = r.GetString ("CTS_Help_cbStartUnit");        // "Zeitintervall bis zum Start des 1. Scripts (Einheit)."
       
      // Show help
      this._ToolTip.SetToolTip (requestingControl, s);
      hlpevent.Handled=true;
    }

    /// <summary>
    /// 'Tick' event of the '_Timer' timer control
    /// </summary>
    private void _Timer_Tick(object sender, System.EventArgs e)
    {
      // Update the form
      UpdateData ();
    }
    
    #endregion event handling

    #region members
   
    /// <summary>
    /// True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    bool _bShowWait = false;

    /// <summary>
    /// The CTS data string
    /// </summary>
    string _sCTSdata = "";
  
    /// <summary>
    /// Indicates, whether a Transfer process is curr'ly in progress ( true ) or not ( false )
    /// </summary>
    bool _bTransferInProgress = false;

    /// <summary>
    /// Name of the script, that was lastly running on the IMS device
    /// </summary>
    string _sLastScriptName = string.Empty;
    
    #endregion members

    #region constants & enums
    #endregion constants & enums

    #region methods
    
    /// <summary>
    /// Init's the form.
    /// </summary>
    void Init ()
    {

      // Units CB
      this._cbStartUnit.Items.Clear ();
      this._cbStartUnit.Items.AddRange (new object[] { "min", "h" });
    }

    /// <summary>
    /// Ckecks the validity of the control contents
    /// </summary>
    /// <returns>True, if the control contents are valid; false otherwise</returns>
    Boolean _CheckControls()
    {
      object o;
      App app = App.Instance;
      Ressources r = app.Ressources;

      // -----------------------
      // CTS settings
      // No need to check for validity
      
      // -----------------------
      // CTS handling
      
      // CTS may be enabled ONLY in presence of clock-timed scripts
      if (this._chkCTSEnable.Checked)
      {
        if (this._lvScripts.Items.Count == 0)
        {
          // Error:
          // Show message
          string sMsg = r.GetString ("CTS_Check_Err1");         // "CTS handling may be enabled only if scripts are assigned."
          string sCap = r.GetString ("Form_Common_TextError");  // "Error"
          MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
          return false;
        }
      }
      
      // Start interval (> 0) and Unit (No need to check for validity, because the CB has the DropDownList style)
      // Notes:
      //  The maximum permitted start interval is (2^16-1) min. This must be proofed for.
      if ( !Check.Execute ( this._txtStartInterval, CheckType.Int, CheckRelation.GT, 0, 0, true, out o) )
        return false;
      int nStartInterval=(int) o;
      string sUnit = this._cbStartUnit.SelectedItem.ToString ();
      //  Check for validity
      switch (sUnit)
      {
        case "min": break;
        case "h":   nStartInterval *= 60; break;
      }
      if (nStartInterval > UInt16.MaxValue)
      {
        // Error:
        // Show message
        string fmt = r.GetString ("CTS_Check_Err2");          // "The maximum permitted start interval is {0} min.\r\nPlease correct."
        string sMsg = string.Format (fmt, UInt16.MaxValue);
        string sCap = r.GetString ("Form_Common_TextError");  // "Error"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        // Cursor
        this._txtStartInterval.Focus ();
        return false;
      }

      // ready
      return true;
    }

    /// <summary>
    /// Updates the form
    /// </summary>
    void UpdateData () 
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;

      // Read/Write buttons
      Boolean bEn = ( doc.sVersion.Length > 0 ) && comm.IsOpen () && ( _bTransferInProgress == false );
      this._btnRead.Enabled   = bEn; 
      this._btnWrite.Enabled  = bEn;   
      
      // GB 'Activation'
      this._gbCTSHandling.Enabled = bEn;

      // Progress bar
      bEn = (doc.sVersion.Length > 0);
      this._pgbTransfer.Enabled = bEn;

      // Add/Edit/Remove/UpDown buttons
      bEn = (this._lvScripts.Items.Count < Doc.MAX_CLOCKTIMEDSCRIPTS);
      this._btnAdd.Enabled = bEn;
      
      bEn = (_lvScripts.SelectedItems.Count > 0);
      this._btnEdit.Enabled = bEn;
      this._btnRemove.Enabled = bEn;
      this._btnUp.Enabled = bEn;
      this._btnDown.Enabled = bEn;
      if (bEn)
      {
        int n = _lvScripts.Items.Count;
        int idx = _lvScripts.SelectedIndices[0];
        if      ( idx == 0)     this._btnUp.Enabled = false;
        else if ( idx == n-1 )  this._btnDown.Enabled = false;
      }
    }

    /// <summary>
    /// Updates the CTS data string based on the control contents (DDX = true) or viceversa (DDX = false). 
    /// </summary>
    /// <param name="bDDX">The DDX direction</param>
    void _UpdateCTSData (Boolean bDDX)
    {
      App app = App.Instance;
      Doc doc = app.Doc;

      if ( bDDX )
      {
        // Controls -> Members ( WRITE (to device)):

        // Build the CTS data string
        // Notes:
        //  1.
        //  The data string has the following format:
        //    "<CTS_Enabled>TAB<Start_Interval>TAB<ScriptIdx_0>TAB<RunningPeriod_0>TAB ... <ScriptIdx_Nm1>TAB<RunningPeriod_Nm1>TAB"
        //  with: N - # of clock-timed scripts
        //  2.
        //  a) 
        //  The length of a whole parameter string should not exceed 'MAX_PAR_LEN' bytes. Because the 
        //  parameter string & the data string are identical here, the data string should not exceed a length 
        //  of 'MAX_PAR_LEN' B.
        //  b) 
        //  The data string should not exceed a length of 127 B (see device SW, Notes for 'sf_ctswrite()').
        //  
        //  With respect to a) & b): length < Min ('MAX_PAR_LEN', 127) -> This must be proofed for!
        //
        //  Here we have appr. as an upper margin the following Byte #:
        //    (1+1) + (5+1) + 10 x ((2+1)+(5+1)) = 2+6+90 = 98
        
        // Part 'Activation':
        //  Calc. the start interval
        int nInterval = int.Parse (this._txtStartInterval.Text);
        string sUnit = this._cbStartUnit.SelectedItem.ToString ();
        switch (sUnit)
        {
          case "min": break;                      // min: do nothing
          case "h":   nInterval *= 60; break;     // h: transform h into min
        }
        //  Initiate the TX string      
        _sCTSdata = string.Format ("{0}\t{1}\t", 
          this._chkCTSEnable.Checked ? "1" : "0",
          nInterval
        );

        // Part 'Script sequence'
        foreach (ListViewItem lvi in this._lvScripts.Items)
        {
          // Current script: Calc. the running period
          nInterval = int.Parse (lvi.SubItems[1].Text);
          sUnit = lvi.SubItems[2].Text;
          switch (sUnit)
          {
            case "min": break;                      // min: do nothing
            case "h":   nInterval *= 60; break;     // h: transform h into min
          }
          // Build the TX string
          _sCTSdata += string.Format ("{0}\t{1}\t",
            doc.ScriptNameToIdx ( lvi.Text ),
            nInterval
            );
        }
      }
      else
      {
        // Members -> Controls ( READ (from device)):

        // Parse the CTS data string array and Update the control contents
        // Notes:
        //  1.
        //  The data string has the following format:
        //    "<CTS_Enabled>TAB<Start_Interval>TAB<ScriptIdx_0>TAB<RunningPeriod_0>TAB ... <ScriptIdx_Nm1>TAB<RunningPeriod_Nm1>TAB"
        //  with: N - # of clock-timed scripts
        //  2.
        //  Concerning the string length:
        //  Here we have appr. as an upper margin the following Byte #:
        //    (1+1) + (5+1) + 10 x ((2+1)+(5+1)) = 2+6+90 = 98
        //  3.
        //  Because a clock-timed script Settings structure contains 2 elements, the value 'nCount' 
        //  must be a true multiple of 2 - this must be proofed for. 
        string[] ars = _sCTSdata.Split ( '\t' );
        int nCount = ars.Length - 2;
        if (nCount%2 > 0) nCount = (nCount/2)*2;
        
        // Part 'Activation':
        //  CTS handling enabled?
        this._chkCTSEnable.Checked = (ars[0].Trim() == "1") ? true : false;
        //  Start interval & unit
        int nInterval = int.Parse (ars[1]);   // Start interval in min
        string sUnit;
        if ((nInterval % 60) == 0)
        {
          // Representation in h
          nInterval /= 60;
          sUnit = "h";
        }
        else
        {
          // Representation in min
          sUnit = "min";
        }
        string sInterval = nInterval.ToString ();
        this._txtStartInterval.Text = sInterval;
        this._cbStartUnit.SelectedItem = sUnit;
        
        // Part 'Script sequence'
        this._lvScripts.Items.Clear ();
        for (int i=0; i < nCount; i+=2)
        {
          ListViewItem lvi = new ListViewItem ();
          // Current script: Idx -> Name
          int idx = int.Parse(ars[i+2]);                    // The script idx ...
          if (idx > doc.arsDeviceScripts.Count-1) idx = 0;  // ... in a safe manner
          lvi.Text = doc.arsDeviceScripts[idx];
          // Current script: running period
          nInterval = int.Parse (ars[i+3]);     // Running period in min
          if ((nInterval % 60) == 0)
          {
            // Representation in h
            nInterval /= 60;
            sUnit = "h";
          }
          else
          {
            // Representation in min
            sUnit = "min";
          }
          sInterval = nInterval.ToString ();
          lvi.SubItems.Add (sInterval);
          lvi.SubItems.Add (sUnit);
          // Add item
          this._lvScripts.Items.Add (lvi);
        }
      }

    }

    /// <summary>
    /// Finishes the transfer
    /// </summary>
    void _EndTransfer ( )
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Doc doc = app.Doc;

      // TX: Script Select
      //    Parameter: The name of the script to be executed
      //    Device: Action - Selects the script to be executed (here: script last used); 
      //            Returns - OK
      CommMessage msg = new CommMessage ( comm.msgScriptSelect );
      msg.Parameter = doc.ScriptNameToIdx ( _sLastScriptName ).ToString ();
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );

      // TX: Transfer Complete
      //    Parameter: none
      //    Device: Action - Indicate, that the transfer has completed; 
      //            Returns - OK
      msg = new CommMessage (comm.msgTransferComplete);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);

      // Indicate that the Transfer process has finished.
      _bTransferInProgress = false;

      // Reset: Value (position) of the Progressbar control
      this._pgbTransfer.Value = 0;
    }
 
    /// <summary>
    /// Performs actions in reaction of the receipt of a comm. message  
    /// </summary>
    /// <param name="msgRX">The comm. message</param>
    public void AfterRXCompleted(CommMessage msgRX)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // CD: Message response ID
      if ((msgRX.ResponseID == AppComm.CMID_TIMEOUT) || (msgRX.ResponseID == AppComm.CMID_STRLENERR))
      {
        // A comm. error (Timeout, ...) occurred:

        // Update the form
        UpdateData ();
      }

      else if (msgRX.ResponseID == AppComm.CMID_ANSWER)
      {
        // The message response is present:
        string sCmd = msgRX.CommandResponse;
        byte[] arbyPar = msgRX.ParameterResponse;

        // Get the parameter string from the parameter Byte array
        string sPar = Encoding.ASCII.GetString (arbyPar);
        // CD according to the message command
        switch ( sCmd ) 
        {
        
            //----------------------------------------------------------
            // Common Transfer messages
            //----------------------------------------------------------

          case "SCRCURRENT":
            // ScriptCurrent
            // RX: Name of the script currently running on the device   
            try
            {
              // Get the name of the script, that was lastly running on the IMS device
              _sLastScriptName = sPar;
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptCurrent->function failed" );
            }
            break;

          case "SCRSELECT":
            // ScriptSelect
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptSelect->function failed" );
            }
            break;
          
          case "TRNSTART":
            // Transfer Start
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgTransferStart->function failed" );
            }
            break;

          case "TRNCOMPLETE":
            // Transfer Complete
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgTransferComplete->function failed" );
            }
            break;

            //----------------------------------------------------------
            // CTS-Read
            //----------------------------------------------------------

          case "CTSREAD":
            // CTS Read
            // RX: The CTS data from the device
            try
            {
              // Update the CTS data string
              _sCTSdata = sPar;
              // Update progress bar
              this._pgbTransfer.Value++;

              // Update the controls
              _UpdateCTSData (false);
            
              // Wait a little in order to see the progress bar flashing
              Thread.Sleep (100);
              
              // Finish the transfer
              _EndTransfer ();   
            }
            catch 
            {
              Debug.WriteLine ( "OnCTSRead->function failed" );
            }
            break;
        
            //----------------------------------------------------------
            // CTS-Write
            //----------------------------------------------------------

          case "CTSWRITE":
            // CTS Write
            // RX: 'CTS handling enabled?' indication
            try
            {
              // Update the 'CTS handling enabled?' indication
              doc.bCTSRunning = (sPar.Trim() == "1") ? true : false;
              
              // Update progress bar
              this._pgbTransfer.Value++;
              
              // Wait a little in order to see the progress bar flashing
              Thread.Sleep (100);

              // Finish the transfer
              _EndTransfer ();
            }
            catch
            {
              Debug.WriteLine ( "OnCTSWrite->function failed" );
            }
            break;

        } // E - switch ( id )
    
      }//E - if (e.Message.Id = AppComm.CMID_ANSWER

    }
    
    #endregion methods

    #region properties

    /// <summary>
    ///  True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    public bool ShowWait
    {
      get { return _bShowWait; }
      set { _bShowWait = value; }
    }

    #endregion properties

  }
}
