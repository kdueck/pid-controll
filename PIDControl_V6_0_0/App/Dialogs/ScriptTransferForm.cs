using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using System.Text;

using CommRS232;

namespace App
{
	/// <summary>
	/// Class ScriptTransferForm:
	/// Script transfer dialog ( transfer to the PID device )
	/// </summary>
  public class ScriptTransferForm : System.Windows.Forms.Form
  {
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.Timer _Timer;
    private System.Windows.Forms.TextBox _txtMsg;
    private System.Windows.Forms.ProgressBar _ProgressBar;
    private System.ComponentModel.IContainer components;

    /// <summary>
    /// Constructor
    /// </summary>
    public ScriptTransferForm()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container ();
      this._btnCancel = new System.Windows.Forms.Button ();
      this._txtMsg = new System.Windows.Forms.TextBox ();
      this._Timer = new System.Windows.Forms.Timer (this.components);
      this._ProgressBar = new System.Windows.Forms.ProgressBar ();
      this.SuspendLayout ();
      // 
      // _btnCancel
      // 
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point (252, 112);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.Size = new System.Drawing.Size (75, 23);
      this._btnCancel.TabIndex = 0;
      this._btnCancel.Text = "Cancel";
      // 
      // _txtMsg
      // 
      this._txtMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtMsg.Location = new System.Drawing.Point (8, 8);
      this._txtMsg.Multiline = true;
      this._txtMsg.Name = "_txtMsg";
      this._txtMsg.ReadOnly = true;
      this._txtMsg.Size = new System.Drawing.Size (318, 94);
      this._txtMsg.TabIndex = 1;
      // 
      // _Timer
      // 
      this._Timer.Interval = 50;
      this._Timer.Tick += new System.EventHandler (this._Timer_Tick);
      // 
      // _ProgressBar
      // 
      this._ProgressBar.Location = new System.Drawing.Point (8, 112);
      this._ProgressBar.Name = "_ProgressBar";
      this._ProgressBar.Size = new System.Drawing.Size (236, 23);
      this._ProgressBar.TabIndex = 2;
      // 
      // ScriptTransferForm
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size (5, 13);
      this.CancelButton = this._btnCancel;
      this.ClientSize = new System.Drawing.Size (334, 142);
      this.Controls.Add (this._ProgressBar);
      this.Controls.Add (this._txtMsg);
      this.Controls.Add (this._btnCancel);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ScriptTransferForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Transmit script";
      this.Closed += new System.EventHandler (this.ScriptTransferForm_Closed);
      this.Load += new System.EventHandler (this.ScriptTransferForm_Load);
      this.ResumeLayout (false);
      this.PerformLayout ();

    }
    
    #endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void ScriptTransferForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      Ressources r = app.Ressources;

      // Resources
      this.Text             = r.GetString ( "ScriptTransfer_Title" );      // "�bertragung der Scriptdaten"
      this._btnCancel.Text  = r.GetString ( "ScriptTransfer_btnCancel" );  // "Abbruch"
      string msg            = r.GetString ( "ScriptTransfer_txtMsg" );     // "Die Scriptdaten von Datei\r\n   {0}\r\nwerden �bertragen ..."
      this._txtMsg.Text = string.Format ( msg, doc.sScriptFilename );
  
      try
      {
        // Enable timer
        _Timer.Interval = 50;
        _Timer.Enabled = true;
    
        // Check: Communication channel open?
        if ( !app.Comm.IsOpen () )
        {
          // No: Close dialog with Cancel
          Debug.WriteLine ( "ScriptTransferForm.Load -> Comm is not open\n" );
          throw new ApplicationException ();
        }
    
        // Yes:
        // Send (transmit ) script to device 
        _SendScript();
      }
      catch
      {
        this.DialogResult = DialogResult.Cancel;
        Close ();
      }

      // Init. progres bar
      _nTotalNoOfMessages = app.Comm.MessageCount;  // The # of messages to be transferred
      this._ProgressBar.Minimum = 0;
      this._ProgressBar.Maximum = _nTotalNoOfMessages;
      this._ProgressBar.Value = 0;
    }

    /// <summary>
    /// 'Closed' event of the form
    /// </summary>
    private void ScriptTransferForm_Closed(object sender, System.EventArgs e)
    {
      // Disable timer
      _Timer.Enabled = false;
    }

    /// <summary>
    /// 'Tick' event of the _Timer control (each 50 ms)
    /// </summary>
    private void _Timer_Tick(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      
      // Check: Communication channel open?
      if ( !comm.IsOpen() ) 
      {
        // No: Close dialog with OK
        this.DialogResult = DialogResult.OK;
        Close ();
      }
  
      // Update progres bar
      int nNoOfMessages = comm.MessageCount;
      this._ProgressBar.Value = _nTotalNoOfMessages - nNoOfMessages;

      // Wait until communication is idle, afterwards wait a little (here: 1000 ms) so that all comm. processes 
      // (RX evaluation) could finish, and after that close the script transfer dialog  
      if (!_bIsIdle)
      {
        if ( comm.IsIdle() ) 
        {
          // Communication is idle: Start waiting a little 
          _bIsIdle = true;
          _nIdle = 0;
        }
      }
      else
      {
        _nIdle++;
        if (_nIdle == 20)
        {
          // Finish waiting: Close the dialog
          this.DialogResult = DialogResult.OK;
          Close ();
        }
      }
    }

    #endregion event handling

    #region members
    
    /// <summary>
    /// True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    bool _bShowWait = false;
    
    /// <summary>
    /// The # of messages to be transferred
    /// </summary>
    int _nTotalNoOfMessages;
    
    /// <summary>
    /// Waiting after communication is idle: Indicator
    /// </summary>
    bool _bIsIdle = false;
    /// <summary>
    /// Waiting after communication is idle: Counter
    /// </summary>
    int _nIdle = 0;

    #endregion members

    #region methods

    /// <summary>
    /// Sends a script to the device
    /// </summary>
    void _SendScript ()
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;
        
      // Check: Valid script configuration?
      if ( 0 == doc.arScript.NumScripts )
      {
        // No
        string sMsg = r.GetString ( "ScriptTransfer_Error_InvalidScriptfile" ); // "Die Scriptkonfigurationsdatei enth�lt entweder keine g�ltigen Scriptdefinitionen oder sie wurde nicht geladen."
        string sCap = r.GetString ("Form_Common_TextInfo");                     // "Information";
        MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
        return;
      }

      // TX: Script Clear
      CommMessage msg = new CommMessage (comm.msgScriptClear);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);

      // TX: Script UserID
      msg = new CommMessage(comm.msgScriptUserID);
      msg.Parameter = doc.arScript.sUserID;
      msg.WindowInfo = new WndInfo(this.Handle, _bShowWait);
      comm.WriteMessage(msg);

      // TX: Script Filename
      msg = new CommMessage (comm.msgScriptFilename);
      string s = System.IO.Path.GetFileName (doc.sScriptFilename);
      msg.Parameter = s;
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);

      try
      {
        // Loop over all scripts
        for ( int nScript=0; nScript < doc.arScript.NumScripts; nScript++ )
        {
          // Current script
          Script scr = doc.arScript.Scripts[nScript];
      
          // TX: Script Open
          msg = new CommMessage (comm.msgScriptOpen);
          msg.Parameter = scr.Name;
          msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
          comm.WriteMessage ( msg );
     
          // TX: Script Parameter ( Write the script parameter set )
          int nAnzParts = 2 + Script.MAX_SCRIPT_WINDOWS;    // # of script parameter (string) parts
          for (int j=0; j < nAnzParts; j++)
          {
            msg = new CommMessage (comm.msgScriptParameter);
            msg.Parameter = string.Format ("{0},{1}", j, scr.Parameter.ToSubString (j));
            msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
            comm.WriteMessage ( msg );
          }

          // TX: Script Window ( Write the script windows settings )
          for ( int i=0; i < scr.NumWindows; i++ )
          {
            for (int j=0; j < 2; j++)
            {
              msg = new CommMessage (comm.msgScriptWindow);
              msg.Parameter = string.Format ("{0},{1},{2}", i, j, scr.Windows[i].ToSubString (j));
              msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
              comm.WriteMessage ( msg );
            }
          }

          // TX: Script Event ( Write the script event settings )
          for ( int i=0; i < scr.NumCommands; i++ )
          {
            msg = new CommMessage (comm.msgScriptEvent);
            msg.Parameter = string.Format ("{0},{1}", i, scr.Cmds[i].ToString ());
            msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
            comm.WriteMessage ( msg );
          }

          // TX: Script Close
          msg = new CommMessage (comm.msgScriptClose);
          msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
          comm.WriteMessage (msg);
        }
    
        // TX: Script Complete
        msg = new CommMessage (comm.msgScriptComplete);
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
      catch
      {
        Debug.WriteLine ( "ScriptTransferForm.SendScript failed\n" );
        throw;
      }
    }

    /// <summary>
    /// Performs actions in reaction of the receipt of a comm. message  
    /// </summary>
    /// <param name="msgRX">The comm. message</param>
    public void AfterRXCompleted(CommMessage msgRX)
    {
      App app = App.Instance;
      Doc doc = app.Doc;

      // CD: Message response ID
      if ((msgRX.ResponseID == AppComm.CMID_TIMEOUT) || (msgRX.ResponseID == AppComm.CMID_STRLENERR))
      {
        // A comm. error (Timeout, ...) occurred:

        // do nothing
      }
      
      else if (msgRX.ResponseID == AppComm.CMID_ANSWER)
      {
        // The message response is present:
        string sCmd = msgRX.CommandResponse;
        byte[] arbyPar = msgRX.ParameterResponse;

        // Get the parameter string from the parameter Byte array
        string sPar = Encoding.ASCII.GetString (arbyPar);
        // CD according to the message command
        switch ( sCmd ) 
        {
            //----------------------------------------------------------
            // Script transfer messages
            //----------------------------------------------------------
        
          case "SCRCLEAR":
            // ScriptClear
            try
            {
              // Testing only
              string sMsg = string.Format ("SCRCLEAR: {0}", sPar); 
              Debug.WriteLine ( sMsg );  
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptClear->function failed" );
            }
            break;

          case "SCRUSERID":
            // User ID
            try
            {
              // Testing only
              string sMsg = string.Format("SCRUSERID: {0}", sPar);
              Debug.WriteLine(sMsg);
            }
            catch
            {
              Debug.WriteLine("OnMsgScriptUserID->function failed");
            }
            break;
          
          case "SCRFILENAME":
            // ScriptFilename
            try
            {
              // Testing only
              string sMsg = string.Format ("SCRFILENAME: {0}", sPar); 
              Debug.WriteLine ( sMsg );  
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptFilename->function failed" );
            }
            break;

          case "SCROPEN":
            // ScriptOpen
            try
            {
              // Testing only
              string sMsg = string.Format ("SCROPEN: {0}", sPar); 
              Debug.WriteLine ( sMsg );  
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptOpen->function failed" );
            }
            break;

          case "SCRCLOSE":
            // ScriptClose
            try
            {
              // Testing only
              string sMsg = string.Format ("SCRCLOSE: {0}", sPar); 
              Debug.WriteLine ( sMsg );  
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptClose->function failed" );
            }
            break;

          case "SCRPARAM":
            // ScriptParameter
            try
            {
              // Testing only
              string sMsg = string.Format ("SCRPARAM: {0}", sPar); 
              Debug.WriteLine ( sMsg );  
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptParam->function failed" );
            }
            break;

          case "SCRWINDOW":
            // ScriptWindow
            try
            {
              // Testing only
              string sMsg = string.Format ("SCRWINDOW: {0}", sPar); 
              Debug.WriteLine ( sMsg );  
            }
            catch 
            {
              Debug.WriteLine ( "OnMsgScriptWindow->function failed" );
            }
            break;

          case "SCREVENT":
            // ScriptEvent
            try
            {
              // Testing only
              string sMsg = string.Format ("SCREVENT: {0}", sPar); 
              Debug.WriteLine ( sMsg );  
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptEvent->function failed" );
            }
            break;

          case "SCRCOMPLETE":
            // ScriptComplete
            try
            {
              // Testing only
              string sMsg = string.Format ("SCRCOMPLETE: {0}", sPar); 
              Debug.WriteLine ( sMsg );  
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptComplete->function failed" );
            }
            break;

        }
      }//E - if (e.Message.Id = AppComm.CMID_ANSWER)

    }

    #endregion methods

    #region properties

    /// <summary>
    ///  True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    public bool ShowWait
    {
      get { return _bShowWait; }
      set { _bShowWait = value; }
    }

    #endregion properties

  }
}
