using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;

namespace App
{
	/// <summary>
	/// Class ScriptEditorForm:
	/// Script editor dialog
	/// </summary>
  public class ScriptEditorForm : System.Windows.Forms.Form
  {
    private System.Windows.Forms.GroupBox _gbParameter;
    private System.Windows.Forms.Label _lblMeasCycle;
    private System.Windows.Forms.GroupBox _gbWindow;
    private System.Windows.Forms.ColumnHeader colSubstance;
    private System.Windows.Forms.ColumnHeader colCenter;
    private System.Windows.Forms.ColumnHeader colWidth;
    private System.Windows.Forms.ColumnHeader colZeroVal;
    private System.Windows.Forms.ColumnHeader colDispLimit;
    private System.Windows.Forms.GroupBox _gpCmd;
    private System.Windows.Forms.ColumnHeader colTime;
    private System.Windows.Forms.ColumnHeader colCmd;
    private System.Windows.Forms.ColumnHeader colParameter;
    private System.Windows.Forms.Button _btnWindow_Remove;
    private System.Windows.Forms.Button _btnWindow_Edit;
    private System.Windows.Forms.Button _btnWindow_Add;
    private System.Windows.Forms.GroupBox _gbName;
    private System.Windows.Forms.TextBox _txtName;
    private System.Windows.Forms.Button _btnCmd_Remove;
    private System.Windows.Forms.Button _btnCmd_Edit;
    private System.Windows.Forms.Button _btnCmd_Add;
    private System.Windows.Forms.ToolTip _ToolTip;
    private System.Windows.Forms.ListView _lvWindows;
    private System.Windows.Forms.ListView _lvCommands;
    private System.Windows.Forms.Timer _Timer;
    private System.Windows.Forms.GroupBox _gbComments;
    private System.Windows.Forms.Label _lblCmt_Wnd;
    private System.Windows.Forms.Label _lblCmt_Cmd;
    private System.Windows.Forms.Label _lblCmt_Scr;
    private System.Windows.Forms.TextBox _txtCmt_Scr;
    private System.Windows.Forms.TextBox _txtCmt_Wnd;
    private System.Windows.Forms.TextBox _txtCmt_Cmd;
    private System.Windows.Forms.ColumnHeader colConcUnit;
    private System.Windows.Forms.TextBox _txtMeasCycle;
    private System.Windows.Forms.ColumnHeader colP0x;
    private System.Windows.Forms.ColumnHeader colP1x;
    private System.Windows.Forms.ColumnHeader colP2x;
    private System.Windows.Forms.ColumnHeader colP3x;
    private System.Windows.Forms.ColumnHeader colP4x;
    private System.Windows.Forms.ColumnHeader colP5x;
    private System.Windows.Forms.ColumnHeader colP6x;
    private System.Windows.Forms.ColumnHeader colP7x;
    private System.Windows.Forms.ColumnHeader colP0y;
    private System.Windows.Forms.ColumnHeader colP1y;
    private System.Windows.Forms.ColumnHeader colP2y;
    private System.Windows.Forms.ColumnHeader colP3y;
    private System.Windows.Forms.ColumnHeader colP4y;
    private System.Windows.Forms.ColumnHeader colP5y;
    private System.Windows.Forms.ColumnHeader colP6y;
    private System.Windows.Forms.ColumnHeader colP7y;
    private System.Windows.Forms.Label _lblSigProcInt;
    private System.Windows.Forms.Label _lblPCCommInt;
    private System.Windows.Forms.ComboBox _cbSigProcInt;
    private System.Windows.Forms.ComboBox _cbPCCommInt;
    private System.Windows.Forms.ColumnHeader colCalType;
    private System.Windows.Forms.ColumnHeader colUserVal;
    private System.Windows.Forms.ColumnHeader colA2;
    private System.Windows.Forms.ColumnHeader colA1;
    private System.Windows.Forms.TextBox _txtScriptID;
    private System.Windows.Forms.Label _lblScriptID;
    private System.Windows.Forms.ListView _lvSpanConc;
    private System.Windows.Forms.ColumnHeader colSCIdx;
    private System.Windows.Forms.ColumnHeader colSCVal;
    private System.Windows.Forms.Button _btnSpanConc_Edit;
    private System.Windows.Forms.Button _btnSpanConc_Remove;
    private System.Windows.Forms.Button _btnSpanConc_Add;
    private System.Windows.Forms.TextBox _txtCmt_Par;
    private System.Windows.Forms.Label _lblCmt_Par;
    private System.Windows.Forms.CheckBox _chkOffsetCorr;
    private System.Windows.Forms.Label _lblOffsetCorr;
    private System.Windows.Forms.ColumnHeader colSpanFac;
    private System.Windows.Forms.TextBox _txtTotal;
    private System.Windows.Forms.Label _lblTotal;
    private System.Windows.Forms.Button _btnAutoSpanMon_Edit;
    private System.Windows.Forms.Button _btnAutoSpanMon_Remove;
    private System.Windows.Forms.Button _btnAutoSpanMon_Add;
    private System.Windows.Forms.ListView _lvAutoSpanMon;
    private System.Windows.Forms.ColumnHeader colASMIdx;
    private System.Windows.Forms.ColumnHeader colASMLowLimit;
    private System.Windows.Forms.ColumnHeader colASMUppLimit;
    private System.Windows.Forms.ColumnHeader colASMNoErrMeas;
    private System.Windows.Forms.CheckBox _chkMeasScript;
    private System.Windows.Forms.Label _lblMeasScript;
    private System.Windows.Forms.Button _btnDown;
    private System.Windows.Forms.Button _btnUp;
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.Button _btnOK;
    private System.ComponentModel.IContainer components;

    /// <summary>
    /// Constructor
    /// </summary>
    public ScriptEditorForm()
    {
      InitializeComponent();
      // Initialize the form
      _Init ();
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ScriptEditorForm));
      this._btnCancel = new System.Windows.Forms.Button();
      this._btnOK = new System.Windows.Forms.Button();
      this._gbParameter = new System.Windows.Forms.GroupBox();
      this._chkMeasScript = new System.Windows.Forms.CheckBox();
      this._lblMeasScript = new System.Windows.Forms.Label();
      this._btnAutoSpanMon_Edit = new System.Windows.Forms.Button();
      this._btnAutoSpanMon_Remove = new System.Windows.Forms.Button();
      this._btnAutoSpanMon_Add = new System.Windows.Forms.Button();
      this._lvAutoSpanMon = new System.Windows.Forms.ListView();
      this.colASMIdx = new System.Windows.Forms.ColumnHeader();
      this.colASMLowLimit = new System.Windows.Forms.ColumnHeader();
      this.colASMUppLimit = new System.Windows.Forms.ColumnHeader();
      this.colASMNoErrMeas = new System.Windows.Forms.ColumnHeader();
      this._txtTotal = new System.Windows.Forms.TextBox();
      this._lblTotal = new System.Windows.Forms.Label();
      this._chkOffsetCorr = new System.Windows.Forms.CheckBox();
      this._lblOffsetCorr = new System.Windows.Forms.Label();
      this._btnSpanConc_Edit = new System.Windows.Forms.Button();
      this._btnSpanConc_Remove = new System.Windows.Forms.Button();
      this._btnSpanConc_Add = new System.Windows.Forms.Button();
      this._lvSpanConc = new System.Windows.Forms.ListView();
      this.colSCIdx = new System.Windows.Forms.ColumnHeader();
      this.colSCVal = new System.Windows.Forms.ColumnHeader();
      this._txtScriptID = new System.Windows.Forms.TextBox();
      this._lblScriptID = new System.Windows.Forms.Label();
      this._cbPCCommInt = new System.Windows.Forms.ComboBox();
      this._cbSigProcInt = new System.Windows.Forms.ComboBox();
      this._lblPCCommInt = new System.Windows.Forms.Label();
      this._lblSigProcInt = new System.Windows.Forms.Label();
      this._txtMeasCycle = new System.Windows.Forms.TextBox();
      this._lblMeasCycle = new System.Windows.Forms.Label();
      this._gbWindow = new System.Windows.Forms.GroupBox();
      this._lvWindows = new System.Windows.Forms.ListView();
      this.colSubstance = new System.Windows.Forms.ColumnHeader();
      this.colCenter = new System.Windows.Forms.ColumnHeader();
      this.colWidth = new System.Windows.Forms.ColumnHeader();
      this.colA1 = new System.Windows.Forms.ColumnHeader();
      this.colA2 = new System.Windows.Forms.ColumnHeader();
      this.colZeroVal = new System.Windows.Forms.ColumnHeader();
      this.colDispLimit = new System.Windows.Forms.ColumnHeader();
      this.colSpanFac = new System.Windows.Forms.ColumnHeader();
      this.colUserVal = new System.Windows.Forms.ColumnHeader();
      this.colCalType = new System.Windows.Forms.ColumnHeader();
      this.colP0x = new System.Windows.Forms.ColumnHeader();
      this.colP0y = new System.Windows.Forms.ColumnHeader();
      this.colP1x = new System.Windows.Forms.ColumnHeader();
      this.colP1y = new System.Windows.Forms.ColumnHeader();
      this.colP2x = new System.Windows.Forms.ColumnHeader();
      this.colP2y = new System.Windows.Forms.ColumnHeader();
      this.colP3x = new System.Windows.Forms.ColumnHeader();
      this.colP3y = new System.Windows.Forms.ColumnHeader();
      this.colP4x = new System.Windows.Forms.ColumnHeader();
      this.colP4y = new System.Windows.Forms.ColumnHeader();
      this.colP5x = new System.Windows.Forms.ColumnHeader();
      this.colP5y = new System.Windows.Forms.ColumnHeader();
      this.colP6x = new System.Windows.Forms.ColumnHeader();
      this.colP6y = new System.Windows.Forms.ColumnHeader();
      this.colP7x = new System.Windows.Forms.ColumnHeader();
      this.colP7y = new System.Windows.Forms.ColumnHeader();
      this.colConcUnit = new System.Windows.Forms.ColumnHeader();
      this._btnWindow_Edit = new System.Windows.Forms.Button();
      this._btnWindow_Remove = new System.Windows.Forms.Button();
      this._btnWindow_Add = new System.Windows.Forms.Button();
      this._gpCmd = new System.Windows.Forms.GroupBox();
      this._btnDown = new System.Windows.Forms.Button();
      this._btnUp = new System.Windows.Forms.Button();
      this._btnCmd_Add = new System.Windows.Forms.Button();
      this._btnCmd_Edit = new System.Windows.Forms.Button();
      this._btnCmd_Remove = new System.Windows.Forms.Button();
      this._lvCommands = new System.Windows.Forms.ListView();
      this.colTime = new System.Windows.Forms.ColumnHeader();
      this.colCmd = new System.Windows.Forms.ColumnHeader();
      this.colParameter = new System.Windows.Forms.ColumnHeader();
      this._gbName = new System.Windows.Forms.GroupBox();
      this._txtName = new System.Windows.Forms.TextBox();
      this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
      this._Timer = new System.Windows.Forms.Timer(this.components);
      this._gbComments = new System.Windows.Forms.GroupBox();
      this._txtCmt_Par = new System.Windows.Forms.TextBox();
      this._lblCmt_Par = new System.Windows.Forms.Label();
      this._txtCmt_Cmd = new System.Windows.Forms.TextBox();
      this._txtCmt_Wnd = new System.Windows.Forms.TextBox();
      this._txtCmt_Scr = new System.Windows.Forms.TextBox();
      this._lblCmt_Scr = new System.Windows.Forms.Label();
      this._lblCmt_Cmd = new System.Windows.Forms.Label();
      this._lblCmt_Wnd = new System.Windows.Forms.Label();
      this._gbParameter.SuspendLayout();
      this._gbWindow.SuspendLayout();
      this._gpCmd.SuspendLayout();
      this._gbName.SuspendLayout();
      this._gbComments.SuspendLayout();
      this.SuspendLayout();
      // 
      // _btnCancel
      // 
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point(756, 444);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.Size = new System.Drawing.Size(84, 23);
      this._btnCancel.TabIndex = 6;
      this._btnCancel.Text = "Cancel";
      // 
      // _btnOK
      // 
      this._btnOK.Location = new System.Drawing.Point(756, 412);
      this._btnOK.Name = "_btnOK";
      this._btnOK.Size = new System.Drawing.Size(84, 23);
      this._btnOK.TabIndex = 5;
      this._btnOK.Text = "OK";
      this._btnOK.Click += new System.EventHandler(this._btnOK_Click);
      this._btnOK.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _gbParameter
      // 
      this._gbParameter.Controls.Add(this._chkMeasScript);
      this._gbParameter.Controls.Add(this._lblMeasScript);
      this._gbParameter.Controls.Add(this._btnAutoSpanMon_Edit);
      this._gbParameter.Controls.Add(this._btnAutoSpanMon_Remove);
      this._gbParameter.Controls.Add(this._btnAutoSpanMon_Add);
      this._gbParameter.Controls.Add(this._lvAutoSpanMon);
      this._gbParameter.Controls.Add(this._txtTotal);
      this._gbParameter.Controls.Add(this._lblTotal);
      this._gbParameter.Controls.Add(this._chkOffsetCorr);
      this._gbParameter.Controls.Add(this._lblOffsetCorr);
      this._gbParameter.Controls.Add(this._btnSpanConc_Edit);
      this._gbParameter.Controls.Add(this._btnSpanConc_Remove);
      this._gbParameter.Controls.Add(this._btnSpanConc_Add);
      this._gbParameter.Controls.Add(this._lvSpanConc);
      this._gbParameter.Controls.Add(this._txtScriptID);
      this._gbParameter.Controls.Add(this._lblScriptID);
      this._gbParameter.Controls.Add(this._cbPCCommInt);
      this._gbParameter.Controls.Add(this._cbSigProcInt);
      this._gbParameter.Controls.Add(this._lblPCCommInt);
      this._gbParameter.Controls.Add(this._lblSigProcInt);
      this._gbParameter.Controls.Add(this._txtMeasCycle);
      this._gbParameter.Controls.Add(this._lblMeasCycle);
      this._gbParameter.Location = new System.Drawing.Point(288, 8);
      this._gbParameter.Name = "_gbParameter";
      this._gbParameter.Size = new System.Drawing.Size(556, 208);
      this._gbParameter.TabIndex = 1;
      this._gbParameter.TabStop = false;
      this._gbParameter.Text = "Script &parameter";
      // 
      // _chkMeasScript
      // 
      this._chkMeasScript.Location = new System.Drawing.Point(124, 46);
      this._chkMeasScript.Name = "_chkMeasScript";
      this._chkMeasScript.Size = new System.Drawing.Size(16, 24);
      this._chkMeasScript.TabIndex = 7;
      this._chkMeasScript.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblMeasScript
      // 
      this._lblMeasScript.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblMeasScript.Location = new System.Drawing.Point(8, 46);
      this._lblMeasScript.Name = "_lblMeasScript";
      this._lblMeasScript.Size = new System.Drawing.Size(112, 23);
      this._lblMeasScript.TabIndex = 6;
      this._lblMeasScript.Text = "MeasScript:";
      // 
      // _btnAutoSpanMon_Edit
      // 
      this._btnAutoSpanMon_Edit.Location = new System.Drawing.Point(372, 180);
      this._btnAutoSpanMon_Edit.Name = "_btnAutoSpanMon_Edit";
      this._btnAutoSpanMon_Edit.Size = new System.Drawing.Size(84, 23);
      this._btnAutoSpanMon_Edit.TabIndex = 18;
      this._btnAutoSpanMon_Edit.Text = "Bearbeiten ...";
      this._btnAutoSpanMon_Edit.Click += new System.EventHandler(this._btnAutoSpanMon_Edit_Click);
      this._btnAutoSpanMon_Edit.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnAutoSpanMon_Remove
      // 
      this._btnAutoSpanMon_Remove.Location = new System.Drawing.Point(460, 180);
      this._btnAutoSpanMon_Remove.Name = "_btnAutoSpanMon_Remove";
      this._btnAutoSpanMon_Remove.Size = new System.Drawing.Size(84, 23);
      this._btnAutoSpanMon_Remove.TabIndex = 19;
      this._btnAutoSpanMon_Remove.Text = "Remove";
      this._btnAutoSpanMon_Remove.Click += new System.EventHandler(this._btnRemove_Click);
      this._btnAutoSpanMon_Remove.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnAutoSpanMon_Add
      // 
      this._btnAutoSpanMon_Add.Location = new System.Drawing.Point(284, 180);
      this._btnAutoSpanMon_Add.Name = "_btnAutoSpanMon_Add";
      this._btnAutoSpanMon_Add.Size = new System.Drawing.Size(84, 23);
      this._btnAutoSpanMon_Add.TabIndex = 17;
      this._btnAutoSpanMon_Add.Text = "Hinzuf�gen ...";
      this._btnAutoSpanMon_Add.Click += new System.EventHandler(this._btnAutoSpanMon_Add_Click);
      this._btnAutoSpanMon_Add.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lvAutoSpanMon
      // 
      this._lvAutoSpanMon.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
                                                                                     this.colASMIdx,
                                                                                     this.colASMLowLimit,
                                                                                     this.colASMUppLimit,
                                                                                     this.colASMNoErrMeas});
      this._lvAutoSpanMon.FullRowSelect = true;
      this._lvAutoSpanMon.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
      this._lvAutoSpanMon.HideSelection = false;
      this._lvAutoSpanMon.Location = new System.Drawing.Point(280, 98);
      this._lvAutoSpanMon.MultiSelect = false;
      this._lvAutoSpanMon.Name = "_lvAutoSpanMon";
      this._lvAutoSpanMon.Size = new System.Drawing.Size(264, 76);
      this._lvAutoSpanMon.TabIndex = 16;
      this._lvAutoSpanMon.View = System.Windows.Forms.View.Details;
      this._lvAutoSpanMon.DoubleClick += new System.EventHandler(this._lv_DoubleClick);
      this._lvAutoSpanMon.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // colASMIdx
      // 
      this.colASMIdx.Text = "CTRLWND<x>";
      // 
      // colASMLowLimit
      // 
      this.colASMLowLimit.Text = "Lower Limit";
      // 
      // colASMUppLimit
      // 
      this.colASMUppLimit.Text = "Upper Limit";
      // 
      // colASMNoErrMeas
      // 
      this.colASMNoErrMeas.Text = "No. of err. meas.";
      // 
      // _txtTotal
      // 
      this._txtTotal.Location = new System.Drawing.Point(124, 72);
      this._txtTotal.Name = "_txtTotal";
      this._txtTotal.Size = new System.Drawing.Size(96, 20);
      this._txtTotal.TabIndex = 21;
      this._txtTotal.Text = "";
      this._txtTotal.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblTotal
      // 
      this._lblTotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblTotal.Location = new System.Drawing.Point(8, 72);
      this._lblTotal.Name = "_lblTotal";
      this._lblTotal.Size = new System.Drawing.Size(112, 23);
      this._lblTotal.TabIndex = 20;
      this._lblTotal.Text = "Total:";
      // 
      // _chkOffsetCorr
      // 
      this._chkOffsetCorr.Location = new System.Drawing.Point(292, 46);
      this._chkOffsetCorr.Name = "_chkOffsetCorr";
      this._chkOffsetCorr.Size = new System.Drawing.Size(16, 24);
      this._chkOffsetCorr.TabIndex = 9;
      this._chkOffsetCorr.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblOffsetCorr
      // 
      this._lblOffsetCorr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblOffsetCorr.Location = new System.Drawing.Point(176, 46);
      this._lblOffsetCorr.Name = "_lblOffsetCorr";
      this._lblOffsetCorr.Size = new System.Drawing.Size(112, 23);
      this._lblOffsetCorr.TabIndex = 8;
      this._lblOffsetCorr.Text = "OffsetCorr:";
      // 
      // _btnSpanConc_Edit
      // 
      this._btnSpanConc_Edit.Location = new System.Drawing.Point(100, 180);
      this._btnSpanConc_Edit.Name = "_btnSpanConc_Edit";
      this._btnSpanConc_Edit.Size = new System.Drawing.Size(84, 23);
      this._btnSpanConc_Edit.TabIndex = 14;
      this._btnSpanConc_Edit.Text = "Bearbeiten ...";
      this._btnSpanConc_Edit.Click += new System.EventHandler(this._btnSpanConc_Edit_Click);
      this._btnSpanConc_Edit.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnSpanConc_Remove
      // 
      this._btnSpanConc_Remove.Location = new System.Drawing.Point(188, 180);
      this._btnSpanConc_Remove.Name = "_btnSpanConc_Remove";
      this._btnSpanConc_Remove.Size = new System.Drawing.Size(84, 23);
      this._btnSpanConc_Remove.TabIndex = 15;
      this._btnSpanConc_Remove.Text = "Remove";
      this._btnSpanConc_Remove.Click += new System.EventHandler(this._btnRemove_Click);
      this._btnSpanConc_Remove.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnSpanConc_Add
      // 
      this._btnSpanConc_Add.Location = new System.Drawing.Point(12, 180);
      this._btnSpanConc_Add.Name = "_btnSpanConc_Add";
      this._btnSpanConc_Add.Size = new System.Drawing.Size(84, 23);
      this._btnSpanConc_Add.TabIndex = 13;
      this._btnSpanConc_Add.Text = "Hinzuf�gen ...";
      this._btnSpanConc_Add.Click += new System.EventHandler(this._btnSpanConc_Add_Click);
      this._btnSpanConc_Add.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lvSpanConc
      // 
      this._lvSpanConc.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
                                                                                  this.colSCIdx,
                                                                                  this.colSCVal});
      this._lvSpanConc.FullRowSelect = true;
      this._lvSpanConc.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
      this._lvSpanConc.HideSelection = false;
      this._lvSpanConc.Location = new System.Drawing.Point(8, 98);
      this._lvSpanConc.MultiSelect = false;
      this._lvSpanConc.Name = "_lvSpanConc";
      this._lvSpanConc.Size = new System.Drawing.Size(264, 76);
      this._lvSpanConc.TabIndex = 12;
      this._lvSpanConc.View = System.Windows.Forms.View.Details;
      this._lvSpanConc.DoubleClick += new System.EventHandler(this._lv_DoubleClick);
      this._lvSpanConc.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // colSCIdx
      // 
      this.colSCIdx.Text = "SPAN<x>";
      // 
      // colSCVal
      // 
      this.colSCVal.Text = "Value";
      // 
      // _txtScriptID
      // 
      this._txtScriptID.Location = new System.Drawing.Point(436, 46);
      this._txtScriptID.Name = "_txtScriptID";
      this._txtScriptID.Size = new System.Drawing.Size(96, 20);
      this._txtScriptID.TabIndex = 11;
      this._txtScriptID.Text = "";
      this._txtScriptID.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblScriptID
      // 
      this._lblScriptID.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblScriptID.Location = new System.Drawing.Point(356, 46);
      this._lblScriptID.Name = "_lblScriptID";
      this._lblScriptID.Size = new System.Drawing.Size(76, 23);
      this._lblScriptID.TabIndex = 10;
      this._lblScriptID.Text = "ScriptID:";
      // 
      // _cbPCCommInt
      // 
      this._cbPCCommInt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this._cbPCCommInt.Location = new System.Drawing.Point(472, 20);
      this._cbPCCommInt.Name = "_cbPCCommInt";
      this._cbPCCommInt.Size = new System.Drawing.Size(60, 21);
      this._cbPCCommInt.TabIndex = 5;
      this._cbPCCommInt.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _cbSigProcInt
      // 
      this._cbSigProcInt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this._cbSigProcInt.Location = new System.Drawing.Point(292, 20);
      this._cbSigProcInt.Name = "_cbSigProcInt";
      this._cbSigProcInt.Size = new System.Drawing.Size(60, 21);
      this._cbSigProcInt.TabIndex = 3;
      this._cbSigProcInt.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblPCCommInt
      // 
      this._lblPCCommInt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblPCCommInt.Location = new System.Drawing.Point(356, 20);
      this._lblPCCommInt.Name = "_lblPCCommInt";
      this._lblPCCommInt.Size = new System.Drawing.Size(112, 23);
      this._lblPCCommInt.TabIndex = 4;
      this._lblPCCommInt.Text = "PCCommInt [pts]:";
      // 
      // _lblSigProcInt
      // 
      this._lblSigProcInt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblSigProcInt.Location = new System.Drawing.Point(176, 20);
      this._lblSigProcInt.Name = "_lblSigProcInt";
      this._lblSigProcInt.Size = new System.Drawing.Size(112, 23);
      this._lblSigProcInt.TabIndex = 2;
      this._lblSigProcInt.Text = "SigProcInt [pts]:";
      // 
      // _txtMeasCycle
      // 
      this._txtMeasCycle.Location = new System.Drawing.Point(124, 20);
      this._txtMeasCycle.MaxLength = 10;
      this._txtMeasCycle.Name = "_txtMeasCycle";
      this._txtMeasCycle.Size = new System.Drawing.Size(44, 20);
      this._txtMeasCycle.TabIndex = 1;
      this._txtMeasCycle.Text = "";
      this._txtMeasCycle.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblMeasCycle
      // 
      this._lblMeasCycle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblMeasCycle.Location = new System.Drawing.Point(8, 20);
      this._lblMeasCycle.Name = "_lblMeasCycle";
      this._lblMeasCycle.Size = new System.Drawing.Size(112, 23);
      this._lblMeasCycle.TabIndex = 0;
      this._lblMeasCycle.Text = "MeasCycle [1/10s]:";
      // 
      // _gbWindow
      // 
      this._gbWindow.Controls.Add(this._lvWindows);
      this._gbWindow.Controls.Add(this._btnWindow_Edit);
      this._gbWindow.Controls.Add(this._btnWindow_Remove);
      this._gbWindow.Controls.Add(this._btnWindow_Add);
      this._gbWindow.Location = new System.Drawing.Point(8, 220);
      this._gbWindow.Name = "_gbWindow";
      this._gbWindow.Size = new System.Drawing.Size(836, 152);
      this._gbWindow.TabIndex = 2;
      this._gbWindow.TabStop = false;
      this._gbWindow.Text = "Script &windows";
      // 
      // _lvWindows
      // 
      this._lvWindows.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
                                                                                 this.colSubstance,
                                                                                 this.colCenter,
                                                                                 this.colWidth,
                                                                                 this.colA1,
                                                                                 this.colA2,
                                                                                 this.colZeroVal,
                                                                                 this.colDispLimit,
                                                                                 this.colSpanFac,
                                                                                 this.colUserVal,
                                                                                 this.colCalType,
                                                                                 this.colP0x,
                                                                                 this.colP0y,
                                                                                 this.colP1x,
                                                                                 this.colP1y,
                                                                                 this.colP2x,
                                                                                 this.colP2y,
                                                                                 this.colP3x,
                                                                                 this.colP3y,
                                                                                 this.colP4x,
                                                                                 this.colP4y,
                                                                                 this.colP5x,
                                                                                 this.colP5y,
                                                                                 this.colP6x,
                                                                                 this.colP6y,
                                                                                 this.colP7x,
                                                                                 this.colP7y,
                                                                                 this.colConcUnit});
      this._lvWindows.FullRowSelect = true;
      this._lvWindows.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
      this._lvWindows.HideSelection = false;
      this._lvWindows.Location = new System.Drawing.Point(8, 20);
      this._lvWindows.MultiSelect = false;
      this._lvWindows.Name = "_lvWindows";
      this._lvWindows.Size = new System.Drawing.Size(816, 97);
      this._lvWindows.TabIndex = 0;
      this._lvWindows.View = System.Windows.Forms.View.Details;
      this._lvWindows.DoubleClick += new System.EventHandler(this._lv_DoubleClick);
      this._lvWindows.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // colSubstance
      // 
      this.colSubstance.Text = "Substance";
      this.colSubstance.Width = 100;
      // 
      // colCenter
      // 
      this.colCenter.Text = "Center";
      // 
      // colWidth
      // 
      this.colWidth.Text = "Width";
      // 
      // colA1
      // 
      this.colA1.Text = "A1";
      // 
      // colA2
      // 
      this.colA2.Text = "A2";
      // 
      // colZeroVal
      // 
      this.colZeroVal.Text = "ZeroVal";
      // 
      // colDispLimit
      // 
      this.colDispLimit.Text = "DispLimit";
      // 
      // colSpanFac
      // 
      this.colSpanFac.Text = "Span factor";
      // 
      // colUserVal
      // 
      this.colUserVal.Text = "UserVal";
      // 
      // colCalType
      // 
      this.colCalType.Text = "CalType";
      // 
      // colP0x
      // 
      this.colP0x.Text = "P0x";
      // 
      // colP0y
      // 
      this.colP0y.Text = "P0y";
      // 
      // colP1x
      // 
      this.colP1x.Text = "P1x";
      // 
      // colP1y
      // 
      this.colP1y.Text = "P1y";
      // 
      // colP2x
      // 
      this.colP2x.Text = "P2x";
      // 
      // colP2y
      // 
      this.colP2y.Text = "P2y";
      // 
      // colP3x
      // 
      this.colP3x.Text = "P3x";
      // 
      // colP3y
      // 
      this.colP3y.Text = "P3y";
      // 
      // colP4x
      // 
      this.colP4x.Text = "P4x";
      // 
      // colP4y
      // 
      this.colP4y.Text = "P4y";
      // 
      // colP5x
      // 
      this.colP5x.Text = "P5x";
      // 
      // colP5y
      // 
      this.colP5y.Text = "P5y";
      // 
      // colP6x
      // 
      this.colP6x.Text = "P6x";
      // 
      // colP6y
      // 
      this.colP6y.Text = "P6y";
      // 
      // colP7x
      // 
      this.colP7x.Text = "P7x";
      // 
      // colP7y
      // 
      this.colP7y.Text = "P7y";
      // 
      // colConcUnit
      // 
      this.colConcUnit.Text = "ConcUnit";
      // 
      // _btnWindow_Edit
      // 
      this._btnWindow_Edit.Location = new System.Drawing.Point(374, 124);
      this._btnWindow_Edit.Name = "_btnWindow_Edit";
      this._btnWindow_Edit.Size = new System.Drawing.Size(84, 23);
      this._btnWindow_Edit.TabIndex = 2;
      this._btnWindow_Edit.Text = "Bearbeiten ...";
      this._btnWindow_Edit.Click += new System.EventHandler(this._btnWindow_Edit_Click);
      this._btnWindow_Edit.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnWindow_Remove
      // 
      this._btnWindow_Remove.Location = new System.Drawing.Point(464, 124);
      this._btnWindow_Remove.Name = "_btnWindow_Remove";
      this._btnWindow_Remove.Size = new System.Drawing.Size(84, 23);
      this._btnWindow_Remove.TabIndex = 3;
      this._btnWindow_Remove.Text = "Remove";
      this._btnWindow_Remove.Click += new System.EventHandler(this._btnRemove_Click);
      this._btnWindow_Remove.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnWindow_Add
      // 
      this._btnWindow_Add.Location = new System.Drawing.Point(284, 124);
      this._btnWindow_Add.Name = "_btnWindow_Add";
      this._btnWindow_Add.Size = new System.Drawing.Size(84, 23);
      this._btnWindow_Add.TabIndex = 1;
      this._btnWindow_Add.Text = "Hinzuf�gen ...";
      this._btnWindow_Add.Click += new System.EventHandler(this._btnWindow_Add_Click);
      this._btnWindow_Add.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _gpCmd
      // 
      this._gpCmd.Controls.Add(this._btnDown);
      this._gpCmd.Controls.Add(this._btnUp);
      this._gpCmd.Controls.Add(this._btnCmd_Add);
      this._gpCmd.Controls.Add(this._btnCmd_Edit);
      this._gpCmd.Controls.Add(this._btnCmd_Remove);
      this._gpCmd.Controls.Add(this._lvCommands);
      this._gpCmd.Location = new System.Drawing.Point(8, 60);
      this._gpCmd.Name = "_gpCmd";
      this._gpCmd.Size = new System.Drawing.Size(276, 156);
      this._gpCmd.TabIndex = 3;
      this._gpCmd.TabStop = false;
      this._gpCmd.Text = "Script &commands";
      // 
      // _btnDown
      // 
      this._btnDown.Image = ((System.Drawing.Image)(resources.GetObject("_btnDown.Image")));
      this._btnDown.Location = new System.Drawing.Point(12, 76);
      this._btnDown.Name = "_btnDown";
      this._btnDown.Size = new System.Drawing.Size(20, 20);
      this._btnDown.TabIndex = 5;
      this._btnDown.Click += new System.EventHandler(this._btnUpDown_Click);
      // 
      // _btnUp
      // 
      this._btnUp.Image = ((System.Drawing.Image)(resources.GetObject("_btnUp.Image")));
      this._btnUp.Location = new System.Drawing.Point(12, 48);
      this._btnUp.Name = "_btnUp";
      this._btnUp.Size = new System.Drawing.Size(20, 20);
      this._btnUp.TabIndex = 4;
      this._btnUp.Click += new System.EventHandler(this._btnUpDown_Click);
      this._btnUp.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnCmd_Add
      // 
      this._btnCmd_Add.Location = new System.Drawing.Point(10, 128);
      this._btnCmd_Add.Name = "_btnCmd_Add";
      this._btnCmd_Add.Size = new System.Drawing.Size(84, 23);
      this._btnCmd_Add.TabIndex = 1;
      this._btnCmd_Add.Text = "Add ...";
      this._btnCmd_Add.Click += new System.EventHandler(this._btnCmd_Add_Click);
      this._btnCmd_Add.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnCmd_Edit
      // 
      this._btnCmd_Edit.Location = new System.Drawing.Point(98, 128);
      this._btnCmd_Edit.Name = "_btnCmd_Edit";
      this._btnCmd_Edit.Size = new System.Drawing.Size(84, 23);
      this._btnCmd_Edit.TabIndex = 2;
      this._btnCmd_Edit.Text = "Edit ...";
      this._btnCmd_Edit.Click += new System.EventHandler(this._btnCmd_Edit_Click);
      this._btnCmd_Edit.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _btnCmd_Remove
      // 
      this._btnCmd_Remove.Location = new System.Drawing.Point(186, 128);
      this._btnCmd_Remove.Name = "_btnCmd_Remove";
      this._btnCmd_Remove.Size = new System.Drawing.Size(84, 23);
      this._btnCmd_Remove.TabIndex = 3;
      this._btnCmd_Remove.Text = "Remove";
      this._btnCmd_Remove.Click += new System.EventHandler(this._btnRemove_Click);
      this._btnCmd_Remove.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lvCommands
      // 
      this._lvCommands.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
                                                                                  this.colTime,
                                                                                  this.colCmd,
                                                                                  this.colParameter});
      this._lvCommands.FullRowSelect = true;
      this._lvCommands.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
      this._lvCommands.HideSelection = false;
      this._lvCommands.Location = new System.Drawing.Point(38, 20);
      this._lvCommands.MultiSelect = false;
      this._lvCommands.Name = "_lvCommands";
      this._lvCommands.Size = new System.Drawing.Size(230, 102);
      this._lvCommands.TabIndex = 0;
      this._lvCommands.View = System.Windows.Forms.View.Details;
      this._lvCommands.DoubleClick += new System.EventHandler(this._lv_DoubleClick);
      this._lvCommands.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // colTime
      // 
      this.colTime.Text = "Time";
      this.colTime.Width = 50;
      // 
      // colCmd
      // 
      this.colCmd.Text = "Command";
      this.colCmd.Width = 100;
      // 
      // colParameter
      // 
      this.colParameter.Text = "Parameter";
      this.colParameter.Width = 100;
      // 
      // _gbName
      // 
      this._gbName.Controls.Add(this._txtName);
      this._gbName.Location = new System.Drawing.Point(8, 8);
      this._gbName.Name = "_gbName";
      this._gbName.Size = new System.Drawing.Size(276, 48);
      this._gbName.TabIndex = 0;
      this._gbName.TabStop = false;
      this._gbName.Text = "Script &name";
      // 
      // _txtName
      // 
      this._txtName.Location = new System.Drawing.Point(8, 20);
      this._txtName.Name = "_txtName";
      this._txtName.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
      this._txtName.Size = new System.Drawing.Size(260, 20);
      this._txtName.TabIndex = 0;
      this._txtName.Text = "";
      this._txtName.WordWrap = false;
      this._txtName.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _ToolTip
      // 
      this._ToolTip.AutoPopDelay = 10000;
      this._ToolTip.InitialDelay = 500;
      this._ToolTip.ReshowDelay = 100;
      // 
      // _Timer
      // 
      this._Timer.Interval = 200;
      this._Timer.Tick += new System.EventHandler(this._Timer_Tick);
      // 
      // _gbComments
      // 
      this._gbComments.Controls.Add(this._txtCmt_Par);
      this._gbComments.Controls.Add(this._lblCmt_Par);
      this._gbComments.Controls.Add(this._txtCmt_Cmd);
      this._gbComments.Controls.Add(this._txtCmt_Wnd);
      this._gbComments.Controls.Add(this._txtCmt_Scr);
      this._gbComments.Controls.Add(this._lblCmt_Scr);
      this._gbComments.Controls.Add(this._lblCmt_Cmd);
      this._gbComments.Controls.Add(this._lblCmt_Wnd);
      this._gbComments.Location = new System.Drawing.Point(8, 376);
      this._gbComments.Name = "_gbComments";
      this._gbComments.Size = new System.Drawing.Size(740, 116);
      this._gbComments.TabIndex = 4;
      this._gbComments.TabStop = false;
      this._gbComments.Text = "C&omments";
      // 
      // _txtCmt_Par
      // 
      this._txtCmt_Par.Location = new System.Drawing.Point(190, 44);
      this._txtCmt_Par.Multiline = true;
      this._txtCmt_Par.Name = "_txtCmt_Par";
      this._txtCmt_Par.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this._txtCmt_Par.Size = new System.Drawing.Size(178, 64);
      this._txtCmt_Par.TabIndex = 3;
      this._txtCmt_Par.Text = "";
      this._txtCmt_Par.WordWrap = false;
      this._txtCmt_Par.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblCmt_Par
      // 
      this._lblCmt_Par.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblCmt_Par.Location = new System.Drawing.Point(190, 20);
      this._lblCmt_Par.Name = "_lblCmt_Par";
      this._lblCmt_Par.Size = new System.Drawing.Size(156, 23);
      this._lblCmt_Par.TabIndex = 2;
      this._lblCmt_Par.Text = "Parameter section:";
      // 
      // _txtCmt_Cmd
      // 
      this._txtCmt_Cmd.Location = new System.Drawing.Point(556, 44);
      this._txtCmt_Cmd.Multiline = true;
      this._txtCmt_Cmd.Name = "_txtCmt_Cmd";
      this._txtCmt_Cmd.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this._txtCmt_Cmd.Size = new System.Drawing.Size(178, 64);
      this._txtCmt_Cmd.TabIndex = 7;
      this._txtCmt_Cmd.Text = "";
      this._txtCmt_Cmd.WordWrap = false;
      this._txtCmt_Cmd.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtCmt_Wnd
      // 
      this._txtCmt_Wnd.Location = new System.Drawing.Point(372, 44);
      this._txtCmt_Wnd.Multiline = true;
      this._txtCmt_Wnd.Name = "_txtCmt_Wnd";
      this._txtCmt_Wnd.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this._txtCmt_Wnd.Size = new System.Drawing.Size(178, 64);
      this._txtCmt_Wnd.TabIndex = 5;
      this._txtCmt_Wnd.Text = "";
      this._txtCmt_Wnd.WordWrap = false;
      this._txtCmt_Wnd.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _txtCmt_Scr
      // 
      this._txtCmt_Scr.Location = new System.Drawing.Point(8, 44);
      this._txtCmt_Scr.Multiline = true;
      this._txtCmt_Scr.Name = "_txtCmt_Scr";
      this._txtCmt_Scr.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this._txtCmt_Scr.Size = new System.Drawing.Size(178, 64);
      this._txtCmt_Scr.TabIndex = 1;
      this._txtCmt_Scr.Text = "";
      this._txtCmt_Scr.WordWrap = false;
      this._txtCmt_Scr.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblCmt_Scr
      // 
      this._lblCmt_Scr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblCmt_Scr.Location = new System.Drawing.Point(8, 20);
      this._lblCmt_Scr.Name = "_lblCmt_Scr";
      this._lblCmt_Scr.Size = new System.Drawing.Size(156, 23);
      this._lblCmt_Scr.TabIndex = 0;
      this._lblCmt_Scr.Text = "Script:";
      // 
      // _lblCmt_Cmd
      // 
      this._lblCmt_Cmd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblCmt_Cmd.Location = new System.Drawing.Point(556, 20);
      this._lblCmt_Cmd.Name = "_lblCmt_Cmd";
      this._lblCmt_Cmd.Size = new System.Drawing.Size(156, 23);
      this._lblCmt_Cmd.TabIndex = 6;
      this._lblCmt_Cmd.Text = "Command section:";
      // 
      // _lblCmt_Wnd
      // 
      this._lblCmt_Wnd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblCmt_Wnd.Location = new System.Drawing.Point(372, 20);
      this._lblCmt_Wnd.Name = "_lblCmt_Wnd";
      this._lblCmt_Wnd.Size = new System.Drawing.Size(156, 23);
      this._lblCmt_Wnd.TabIndex = 4;
      this._lblCmt_Wnd.Text = "Windows section:";
      // 
      // ScriptEditorForm
      // 
      this.AcceptButton = this._btnOK;
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.CancelButton = this._btnCancel;
      this.ClientSize = new System.Drawing.Size(850, 500);
      this.Controls.Add(this._gbComments);
      this.Controls.Add(this._gbName);
      this.Controls.Add(this._gpCmd);
      this.Controls.Add(this._gbWindow);
      this.Controls.Add(this._gbParameter);
      this.Controls.Add(this._btnOK);
      this.Controls.Add(this._btnCancel);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.HelpButton = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ScriptEditorForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Script editor - Script";
      this.Load += new System.EventHandler(this.ScriptEditorForm_Load);
      this.Closed += new System.EventHandler(this.ScriptEditorForm_Closed);
      this._gbParameter.ResumeLayout(false);
      this._gbWindow.ResumeLayout(false);
      this._gpCmd.ResumeLayout(false);
      this._gbName.ResumeLayout(false);
      this._gbComments.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void ScriptEditorForm_Load(object sender, System.EventArgs e)
    {
      // The script editor form may be called only if a related script collection was present
      Debug.Assert (_sc != null);
      if (null == _sc)
      {
        this.Close ();
        return;
      }
      
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      try
      {
        // Resources
        this.Text                   = r.GetString ( "ScriptEditor_Title" );           // "Scripteditor - Script"    
        this._btnOK.Text            = r.GetString ( "ScriptEditor_btnOK" );           // "OK"
        this._btnCancel.Text        = r.GetString ( "ScriptEditor_btnCancel" );       // "Cancel"

        this._gbName.Text           = r.GetString ( "ScriptEditor_gbName" );          // "Script&name"
     
        this._gbParameter.Text      = r.GetString ( "ScriptEditor_gbParameter" );     // "Script&parameter" 
        this._lblMeasCycle.Text     = r.GetString ( "ScriptEditor_lblMeasCycle" );    // "Messzyklus [1/10s]:"
        this._lblMeasScript.Text    = r.GetString ( "ScriptEditor_lblMeasScript" );   // "MeasScript:" 
        this._lblOffsetCorr.Text    = r.GetString ( "ScriptEditor_lblOffsetCorr" );   // "OffsetCorr:" 
        this._lblScriptID.Text      = r.GetString ( "ScriptEditor_lblScriptID" );     // "Script ID:" 
        this.colSCIdx.Text          = r.GetString ( "ScriptEditor_colSCIdx");         // "SPAN<x>"
        this.colSCVal.Text          = r.GetString ( "ScriptEditor_colSCVal");         // "Value"
        this._btnSpanConc_Add.Text        = r.GetString ( "ScriptEditor_btnSpanConc_Add" );     // "Hinzuf�gen ..."  
        this._btnSpanConc_Edit.Text       = r.GetString ( "ScriptEditor_btnSpanConc_Edit" );    // "Bearbeiten ..."        
        this._btnSpanConc_Remove.Text     = r.GetString ( "ScriptEditor_btnSpanConc_Remove");   // "Entfernen"
        this.colASMIdx.Text               = r.GetString ( "ScriptEditor_colASMIdx");            // "CTRLWND<x>"
        this.colASMLowLimit.Text          = r.GetString ( "ScriptEditor_colASMLowLimit");       // "Lower limit"
        this.colASMUppLimit.Text          = r.GetString ( "ScriptEditor_colASMUppLimit");       // "Upper limit"
        this.colASMNoErrMeas.Text         = r.GetString ( "ScriptEditor_colASMNoErrMeas");      // "No. of err. meas."
        this._btnAutoSpanMon_Add.Text     = r.GetString ( "ScriptEditor_btnAutoSpanMon_Add" );  // "Hinzuf�gen ..."  
        this._btnAutoSpanMon_Edit.Text    = r.GetString ( "ScriptEditor_btnAutoSpanMon_Edit" ); // "Bearbeiten ..."        
        this._btnAutoSpanMon_Remove.Text  = r.GetString ( "ScriptEditor_btnAutoSpanMon_Remove");// "Entfernen"
        this._lblTotal.Text         = r.GetString ( "ScriptEditor_lblTotal" );        // "Total:" 
   
        this._gbWindow.Text         = r.GetString ( "ScriptEditor_gbWindow" );        // "Script&fenster"
        this.colSubstance.Text      = r.GetString ( "ScriptEditor_colSubstance");     // "Substanz"
        this.colCenter.Text         = r.GetString ( "ScriptEditor_colCenter");        // "Zentrum"
        this.colWidth.Text          = r.GetString ( "ScriptEditor_colWidth");         // "Breite"
        this.colA1.Text             = r.GetString ( "ScriptEditor_colA1");            // "LO-Alarm"
        this.colA2.Text             = r.GetString ( "ScriptEditor_colA2");            // "HI-Alarm"
        this.colZeroVal.Text        = r.GetString ( "ScriptEditor_colZeroVal");       // "Nullwert"
        this.colDispLimit.Text      = r.GetString ( "ScriptEditor_colDispLimit");     // "Displaygrenzwert"
        this.colSpanFac.Text        = r.GetString ( "ScriptEditor_colSpanfac");       // "Spanfaktor"
        this.colUserVal.Text        = r.GetString ( "ScriptEditor_colUserVal");       // "Nutzerwert"
        this.colConcUnit.Text       = r.GetString ( "ScriptEditor_colConcUnit");      // "Einheit"
        this._btnWindow_Add.Text    = r.GetString ( "ScriptEditor_btnWindow_Add" );   // "Hinzuf�gen ..."
        this._btnWindow_Edit.Text   = r.GetString ( "ScriptEditor_btnWindow_Edit" );  // "Bearbeiten ..."    
        this._btnWindow_Remove.Text = r.GetString ( "ScriptEditor_btnWindow_Remove" );// "Entfernen"
     
        this._gpCmd.Text            = r.GetString ( "ScriptEditor_gpCmd" );           // "Script&befehle"
        this.colTime.Text           = r.GetString ( "ScriptEditor_colTime" );         // "Zeit"
        this.colCmd.Text            = r.GetString ( "ScriptEditor_colCmd" );          // "Befehl"
        this.colParameter.Text      = r.GetString ( "ScriptEditor_colParameter" );    // "Parameter"
        this._btnCmd_Add.Text       = r.GetString ( "ScriptEditor_btnCmd_Add" );      // "Hinzuf�gen ..."
        this._btnCmd_Edit.Text      = r.GetString ( "ScriptEditor_btnCmd_Edit" );     // "Bearbeiten ..."  
        this._btnCmd_Remove.Text    = r.GetString ( "ScriptEditor_btnCmd_Remove" );   // "Entfernen"
      
        this._gbComments.Text       = r.GetString ( "ScriptEditor_gbComments" );      // "&Kommentare"
        this._lblCmt_Scr.Text       = r.GetString ( "ScriptEditor_lblCmt_Scr" );      // "Script:"
        this._lblCmt_Par.Text       = r.GetString ( "ScriptEditor_lblCmt_Par" );      // "Sektion 'Scriptparameter':"
        this._lblCmt_Wnd.Text       = r.GetString ( "ScriptEditor_lblCmt_Wnd" );      // "Sektion 'Scriptfenster':"
        this._lblCmt_Cmd.Text       = r.GetString ( "ScriptEditor_lblCmt_Cmd" );      // "Sektion 'Scriptbefehle':"
      }
      catch
      {
      }

      // Control  initialisation
      _InitControls ();
      if (-1 != _idx)
      {
        // Editing is requested:
        // The script to be edited
        Script scr = _sc.Scripts[_idx]; 
        // Update the control contents based on this script
        _ScriptToContents (scr);
        // Remember the original name of this script
        _sScrNameOrig=scr.Name;
      }

      // Timer
      _Timer.Enabled = true;
    }
    
    /// <summary>
    /// 'Closed' event of the form
    /// </summary>
    private void ScriptEditorForm_Closed(object sender, System.EventArgs e)
    {
      _Timer.Enabled = false;
    }

    /// <summary>
    /// 'Click' event of the 'Add (Span concentration)' button control
    /// </summary>
    private void _btnSpanConc_Add_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Check: At most MAX_SCRIPT_WINDOWS rows should be edited!
      if (_lvSpanConc.Items.Count >= Script.MAX_SCRIPT_WINDOWS)
      {
        // Show message
        string fmt = r.GetString ("ScriptEditor_btnSpanConc_Add_Info_MaxScriptWindows");  // "At most {0} Span concentration rows can be edited.\r\nThis number is already reached, thats why a further edition is impossible."
        string sMsg = string.Format (fmt, Script.MAX_SCRIPT_WINDOWS);
        string sCap = r.GetString ("Form_Common_TextInfo");                               // "Information"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
        return;
      }
      // Add
      ScriptEditorSpanConcForm f = new ScriptEditorSpanConcForm ();
      DialogResult dr = f.ShowDialog ();
      if (dr == DialogResult.OK) 
      {
        // Overtake
        System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
        ListViewItem lvi = new ListViewItem ();
        lvi.Text = ScriptCollection.SpanConc + f.Index.ToString (); // 'Span<Idx>' is therewith the 1. subitem (index 0) of the SubItemCollection
        lvi.SubItems.Add ( f.Value.ToString(nfi) );                 // 'Value' is therewith the 2. subitem (index 1) of the SubItemCollection
        // Check: Unique entry?
        // Note: Two entries are considered as non-unique, if their 'Index' members coincide.
        bool bEntryExists = false;
        for (int i=0; i < _lvSpanConc.Items.Count ;i++) 
        {
          ListViewItem lvi1 = _lvSpanConc.Items[i];
          if (lvi1.Text.ToUpper() == lvi.Text.ToUpper())
          {
            // No: 
            // Show message
            string fmt = r.GetString ("ScriptEditor_btnWindow_Add_Error_EntryExists");  // "An entry for '{0}' does already exist.\r\nThe new entry will not be added."
            string sMsg = string.Format (fmt, lvi.Text);
            string sCap = r.GetString ("Form_Common_TextError");                        // "Fehler"
            MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
            // Indicate, that the entry already exists 
            bEntryExists = true;
            break;
          }
        }
        if (!bEntryExists) 
        {
          // Correct entry:
          // Add it to the list
          _lvSpanConc.Items.Add (lvi);
          // Adjust the selection of the 'Span concentration' ListView and ensure its visibility
          _lvSpanConc.Focus ();
          _lvSpanConc.Items[_lvSpanConc.Items.Count-1].Selected = true;
          _lvSpanConc.Items[_lvSpanConc.Items.Count-1].EnsureVisible ();
        }
      }
      f.Dispose ();
    }

    /// <summary>
    /// 'Click' event of the 'Edit (Span concentration)' button control
    /// </summary>
    private void _btnSpanConc_Edit_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Check, whether a Span concentration row is selected or not
      int n = _lvSpanConc.SelectedItems.Count;
      if (0 == n) 
        return; // No.
      // Yes:
      // Get the idx of the selected entry
      ListViewItem lviSel = _lvSpanConc.SelectedItems[0];
      int idx = _lvSpanConc.SelectedIndices[0];
      // Open the ScriptEditorSpanConc dialog, handing over initialisation info
      ScriptEditorSpanConcForm f = new ScriptEditorSpanConcForm ();
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
      //    'Span<idx>' -> f.Index
      string sSpanIdx = lviSel.Text;
      int iSpanIdx = sSpanIdx.IndexOf (ScriptCollection.SpanConc);
      if (-1 == iSpanIdx)
      {
        return;        
      }
      string sIdx = sSpanIdx.Substring (ScriptCollection.SpanConc.Length);
      f.Index     = int.Parse (sIdx);                           
      //    Span conc. -> f.Value
      f.Value     = float.Parse (lviSel.SubItems[1].Text, nfi); 
      //    Open dialog
      DialogResult dr = f.ShowDialog ();
      if (dr == DialogResult.OK) 
      {
        // Overtake
        ListViewItem lvi = new ListViewItem ();
        lvi.Text = ScriptCollection.SpanConc + f.Index.ToString (); // 'Span<Idx>' is therewith the 1. subitem (index 0) of the SubItemCollection
        lvi.SubItems.Add ( f.Value.ToString(nfi) );                 // 'Value' is therewith the 2. subitem (index 1) of the SubItemCollection
        // Check: Unique entry?
        // Note: Two entries are considered as non-unique, if their 'Index' members coincide.
        bool bEntryExists = false;
        for (int i=0; i < _lvSpanConc.Items.Count ;i++) 
        {
          if (i != idx) // Do not consider the selected entry itself 
          {
            ListViewItem lvi1 = _lvSpanConc.Items[i];
            if (lvi1.Text.ToUpper() == lvi.Text.ToUpper())
            {
              // No: 
              // Show message
              string fmt = r.GetString ("ScriptEditor_btnWindow_Edit_Error_EntryExists"); // "An entry for '{0}' does already exist.\r\nThe edited entry will be rejected."
              string sMsg = string.Format (fmt, lvi.Text);
              string sCap = r.GetString ("Form_Common_TextError");                        // "Fehler"
              MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
              // Indicate, that the entry already exists 
              bEntryExists = true;
              break;
            }
          }
        }
        if (!bEntryExists) 
        {
          // Correct entry:
          // Remove the (old) entry
          _lvSpanConc.Items.RemoveAt (idx);
          // Insert the edited (new) entry instead
          _lvSpanConc.Items.Insert (idx, lvi);
          // Adjust the selection of the 'Windows' ListView and ensure its visibility
          _lvSpanConc.Focus ();
          _lvSpanConc.Items[idx].Selected = true;
          _lvSpanConc.Items[idx].EnsureVisible ();
        }
      }
      f.Dispose ();
    }
    
    /// <summary>
    /// 'Click' event of the 'Add (Automated span monitoring)' button control
    /// </summary>
    private void _btnAutoSpanMon_Add_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Check: At most MAX_SCRIPT_WINDOWS rows should be edited!
      if (this._lvAutoSpanMon.Items.Count >= Script.MAX_SCRIPT_WINDOWS)
      {
        // Show message
        string fmt = r.GetString ("ScriptEditor_btnAutoSpanMon_Add_Info_MaxScriptWindows"); // "At most {0} 'Automated span monitoring' rows can be edited.\r\nThis number is already reached, thats why a further edition is impossible."
        string sMsg = string.Format (fmt, Script.MAX_SCRIPT_WINDOWS);
        string sCap = r.GetString ("Form_Common_TextInfo");                                 // "Information"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
        return;
      }
      // Add
      ScriptEditorAutoSpanMonForm f = new ScriptEditorAutoSpanMonForm ();
      DialogResult dr = f.ShowDialog ();
      if (dr == DialogResult.OK) 
      {
        // Overtake
        System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
        ListViewItem lvi = new ListViewItem ();
        lvi.Text = ScriptCollection.AutoSpanMon + f.Index.ToString ();  // 'CtrlWnd<Idx>' is therewith the 1. subitem (index 0) of the SubItemCollection
        lvi.SubItems.Add ( f.LowLimit.ToString(nfi) );                  // 'Lower limit' is therewith the 2. subitem (index 1) of the SubItemCollection
        lvi.SubItems.Add ( f.UppLimit.ToString(nfi) );                  // ...
        lvi.SubItems.Add ( f.NoErrMeas.ToString() );      
        // Check: Unique entry?
        // Note: Two entries are considered as non-unique, if their 'Index' members coincide.
        bool bEntryExists = false;
        for (int i=0; i < _lvAutoSpanMon.Items.Count ;i++) 
        {
          ListViewItem lvi1 = _lvAutoSpanMon.Items[i];
          if (lvi1.Text.ToUpper() == lvi.Text.ToUpper())
          {
            // No: 
            // Show message
            string fmt = r.GetString ("ScriptEditor_btnWindow_Add_Error_EntryExists");  // "An entry for '{0}' does already exist.\r\nThe new entry will not be added."
            string sMsg = string.Format (fmt, lvi.Text);
            string sCap = r.GetString ("Form_Common_TextError");                        // "Fehler"
            MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
            // Indicate, that the entry already exists 
            bEntryExists = true;
            break;
          }
        }
        if (!bEntryExists) 
        {
          // Correct entry:
          // Add it to the list
          _lvAutoSpanMon.Items.Add (lvi);
          // Adjust the selection of the 'Automated span monitoring' ListView and ensure its visibility
          _lvAutoSpanMon.Focus ();
          _lvAutoSpanMon.Items[_lvAutoSpanMon.Items.Count-1].Selected = true;
          _lvAutoSpanMon.Items[_lvAutoSpanMon.Items.Count-1].EnsureVisible ();
        }
      }
      f.Dispose ();
    }

    /// <summary>
    /// 'Click' event of the 'Edit (Automated span monitoring)' button control
    /// </summary>
    private void _btnAutoSpanMon_Edit_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Check, whether an 'Automated span monitoring' row is selected or not
      int n = _lvAutoSpanMon.SelectedItems.Count;
      if (0 == n) 
        return; // No.
      // Yes:
      // Get the idx of the selected entry
      ListViewItem lviSel = _lvAutoSpanMon.SelectedItems[0];
      int idx = _lvAutoSpanMon.SelectedIndices[0];
      // Open the ScriptEditorAutoSpanMon dialog, handing over initialisation info
      ScriptEditorAutoSpanMonForm f = new ScriptEditorAutoSpanMonForm ();
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
      //    'CtrlWnd<idx>' -> f.Index
      string sASMIdx = lviSel.Text;
      int iASMIdx = sASMIdx.IndexOf (ScriptCollection.AutoSpanMon);
      if (-1 == iASMIdx)
      {
        return;        
      }
      string sIdx = sASMIdx.Substring (ScriptCollection.AutoSpanMon.Length);
      f.Index     = int.Parse (sIdx);                           
      //    Others
      f.LowLimit = float.Parse (lviSel.SubItems[1].Text, nfi); 
      f.UppLimit = float.Parse (lviSel.SubItems[2].Text, nfi); 
      f.NoErrMeas = int.Parse (lviSel.SubItems[3].Text); 
      //    Open dialog
      DialogResult dr = f.ShowDialog ();
      if (dr == DialogResult.OK) 
      {
        // Overtake
        ListViewItem lvi = new ListViewItem ();
        lvi.Text = ScriptCollection.AutoSpanMon + f.Index.ToString ();  // 'CtrlWnd<Idx>' is therewith the 1. subitem (index 0) of the SubItemCollection
        lvi.SubItems.Add ( f.LowLimit.ToString(nfi) );                  // 'Lower limit' is therewith the 2. subitem (index 1) of the SubItemCollection
        lvi.SubItems.Add ( f.UppLimit.ToString(nfi) );                  // ...
        lvi.SubItems.Add ( f.NoErrMeas.ToString() );      
        // Check: Unique entry?
        // Note: Two entries are considered as non-unique, if their 'Index' members coincide.
        bool bEntryExists = false;
        for (int i=0; i < _lvAutoSpanMon.Items.Count ;i++) 
        {
          if (i != idx) // Do not consider the selected entry itself 
          {
            ListViewItem lvi1 = _lvAutoSpanMon.Items[i];
            if (lvi1.Text.ToUpper() == lvi.Text.ToUpper())
            {
              // No: 
              // Show message
              string fmt = r.GetString ("ScriptEditor_btnWindow_Edit_Error_EntryExists"); // "An entry for '{0}' does already exist.\r\nThe edited entry will be rejected."
              string sMsg = string.Format (fmt, lvi.Text);
              string sCap = r.GetString ("Form_Common_TextError");                        // "Fehler"
              MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
              // Indicate, that the entry already exists 
              bEntryExists = true;
              break;
            }
          }
        }
        if (!bEntryExists) 
        {
          // Correct entry:
          // Remove the (old) entry
          _lvAutoSpanMon.Items.RemoveAt (idx);
          // Insert the edited (new) entry instead
          _lvAutoSpanMon.Items.Insert (idx, lvi);
          // Adjust the selection of the 'Windows' ListView and ensure its visibility
          _lvAutoSpanMon.Focus ();
          _lvAutoSpanMon.Items[idx].Selected = true;
          _lvAutoSpanMon.Items[idx].EnsureVisible ();
        }
      }
      f.Dispose ();
    }
    
    /// <summary>
    /// 'Click' event of the 'Add (Window)' button control
    /// </summary>
    private void _btnWindow_Add_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Check: At most MAX_SCRIPT_WINDOWS windows should be edited!
      if (_lvWindows.Items.Count >= Script.MAX_SCRIPT_WINDOWS)
      {
        // Show message
        string fmt = r.GetString ("ScriptEditor_btnWindow_Add_Info_MaxScriptWindows");  // "At most {0} substance windows can be edited.\r\nThis number is already reached, thats why a further edition is impossible."
        string sMsg = string.Format (fmt, Script.MAX_SCRIPT_WINDOWS);
        string sCap = r.GetString ("Form_Common_TextInfo");                             // "Information"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
        return;
      }
      // Add
      ScriptEditorWindowForm f = new ScriptEditorWindowForm ();
      DialogResult dr = f.ShowDialog ();
      if (dr == DialogResult.OK) 
      {
        // Overtake
        System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
        ListViewItem lvi = new ListViewItem ();
        
        lvi.Text = f.Substance;                   // 'Substance' is therewith the 1. subitem (index 0) of the SubItemCollection
        lvi.SubItems.Add ( f.Center.ToString() ); // 'Center' is therewith the 2. subitem (index 1) of the SubItemCollection
        lvi.SubItems.Add ( f.Width.ToString() );  // ...
        lvi.SubItems.Add ( f.A1.ToString(nfi) );
        lvi.SubItems.Add ( f.A2.ToString(nfi) );
        lvi.SubItems.Add ( f.ZeroVal.ToString(nfi) );
        lvi.SubItems.Add ( f.DispLimit.ToString(nfi) );
        lvi.SubItems.Add ( f.SpanFac.ToString(nfi) );
        lvi.SubItems.Add ( f.UserVal.ToString(nfi) );

        lvi.SubItems.Add ( f.CalType.ToString() );

        string s;
        s = (f.P0x == -1) ? "" : f.P0x.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P0y == -1) ? "" : f.P0y.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P1x == -1) ? "" : f.P1x.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P1y == -1) ? "" : f.P1y.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P2x == -1) ? "" : f.P2x.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P2y == -1) ? "" : f.P2y.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P3x == -1) ? "" : f.P3x.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P3y == -1) ? "" : f.P3y.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P4x == -1) ? "" : f.P4x.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P4y == -1) ? "" : f.P4y.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P5x == -1) ? "" : f.P5x.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P5y == -1) ? "" : f.P5y.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P6x == -1) ? "" : f.P6x.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P6y == -1) ? "" : f.P6y.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P7x == -1) ? "" : f.P7x.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P7y == -1) ? "" : f.P7y.ToString(nfi); lvi.SubItems.Add (s);
       
        lvi.SubItems.Add ( f.ConcUnit );
        // Check: Unique entry?
        // Note: Two entries are considered as non-unique, if their 'Substance' members coincide.
        bool bEntryExists = false;
        for (int i=0; i < _lvWindows.Items.Count ;i++) 
        {
          ListViewItem lvi1 = _lvWindows.Items[i];
          if (lvi1.Text.ToUpper() == lvi.Text.ToUpper())
          {
            // No: 
            // Show message
            string fmt = r.GetString ("ScriptEditor_btnWindow_Add_Error_EntryExists");  // "An entry for '{0}' does already exist.\r\nThe new entry will not be added."
            string sMsg = string.Format (fmt, lvi.Text);
            string sCap = r.GetString ("Form_Common_TextError");                        // "Fehler"
            MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
            // Indicate, that the entry already exists 
            bEntryExists = true;
            break;
          }
        }
        if (!bEntryExists) 
        {
          // Correct entry:
          // Add it to the list
          _lvWindows.Items.Add (lvi);
          // Adjust the selection of the 'Windows' ListView and ensure its visibility
          _lvWindows.Focus ();
          _lvWindows.Items[_lvWindows.Items.Count-1].Selected = true;
          _lvWindows.Items[_lvWindows.Items.Count-1].EnsureVisible ();
        }
      }
      f.Dispose ();
    }

    /// <summary>
    /// 'Click' event of the 'Edit (Window)' button control
    /// </summary>
    private void _btnWindow_Edit_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Check, whether a substance window is selected or not
      int n = _lvWindows.SelectedItems.Count;
      if (0 == n) 
        return; // No.
      // Yes:
      // Get the idx of the selected entry
      ListViewItem lviSel = _lvWindows.SelectedItems[0];
      int idx = _lvWindows.SelectedIndices[0];
      // Open the ScriptEditorWindow dialog, handing over initialisation info
      ScriptEditorWindowForm f = new ScriptEditorWindowForm ();
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
      
      f.Substance = lviSel.Text;                                // 'Substance' is at the same time the 1. subitem (index 0) of the SubItemCollection
      f.Center    = int.Parse (lviSel.SubItems[1].Text);        // 'Center' is the 2. subitem (index 1) of the SubItemCollection
      f.Width     = int.Parse (lviSel.SubItems[2].Text);        // ...
      f.A1        = float.Parse (lviSel.SubItems[3].Text, nfi);
      f.A2        = float.Parse (lviSel.SubItems[4].Text, nfi);
      f.ZeroVal   = float.Parse (lviSel.SubItems[5].Text, nfi);
      f.DispLimit = float.Parse (lviSel.SubItems[6].Text, nfi);
      f.SpanFac   = float.Parse (lviSel.SubItems[7].Text, nfi);
      f.UserVal   = float.Parse (lviSel.SubItems[8].Text, nfi);
      
      f.CalType   = int.Parse (lviSel.SubItems[9].Text);

      f.P0x = (lviSel.SubItems[10].Text.Trim().Length == 0) ? -1 : float.Parse (lviSel.SubItems[10].Text, nfi);
      f.P0y = (lviSel.SubItems[11].Text.Trim().Length == 0) ? -1 : float.Parse (lviSel.SubItems[11].Text, nfi);
      f.P1x = (lviSel.SubItems[12].Text.Trim().Length == 0) ? -1 : float.Parse (lviSel.SubItems[12].Text, nfi);
      f.P1y = (lviSel.SubItems[13].Text.Trim().Length == 0) ? -1 : float.Parse (lviSel.SubItems[13].Text, nfi);
      f.P2x = (lviSel.SubItems[14].Text.Trim().Length == 0) ? -1 : float.Parse (lviSel.SubItems[14].Text, nfi);
      f.P2y = (lviSel.SubItems[15].Text.Trim().Length == 0) ? -1 : float.Parse (lviSel.SubItems[15].Text, nfi);
      f.P3x = (lviSel.SubItems[16].Text.Trim().Length == 0) ? -1 : float.Parse (lviSel.SubItems[16].Text, nfi);
      f.P3y = (lviSel.SubItems[17].Text.Trim().Length == 0) ? -1 : float.Parse (lviSel.SubItems[17].Text, nfi);
      f.P4x = (lviSel.SubItems[18].Text.Trim().Length == 0) ? -1 : float.Parse (lviSel.SubItems[18].Text, nfi);
      f.P4y = (lviSel.SubItems[19].Text.Trim().Length == 0) ? -1 : float.Parse (lviSel.SubItems[19].Text, nfi);
      f.P5x = (lviSel.SubItems[20].Text.Trim().Length == 0) ? -1 : float.Parse (lviSel.SubItems[20].Text, nfi);
      f.P5y = (lviSel.SubItems[21].Text.Trim().Length == 0) ? -1 : float.Parse (lviSel.SubItems[21].Text, nfi);
      f.P6x = (lviSel.SubItems[22].Text.Trim().Length == 0) ? -1 : float.Parse (lviSel.SubItems[22].Text, nfi);
      f.P6y = (lviSel.SubItems[23].Text.Trim().Length == 0) ? -1 : float.Parse (lviSel.SubItems[23].Text, nfi);
      f.P7x = (lviSel.SubItems[24].Text.Trim().Length == 0) ? -1 : float.Parse (lviSel.SubItems[24].Text, nfi);
      f.P7y = (lviSel.SubItems[25].Text.Trim().Length == 0) ? -1 : float.Parse (lviSel.SubItems[25].Text, nfi);

      f.ConcUnit  = lviSel.SubItems[26].Text;
      
      DialogResult dr = f.ShowDialog ();
      if (dr == DialogResult.OK) 
      {
        // Overtake
        ListViewItem lvi = new ListViewItem ();
        
        lvi.Text = f.Substance;
        lvi.SubItems.Add ( f.Center.ToString() );
        lvi.SubItems.Add ( f.Width.ToString() );
        lvi.SubItems.Add ( f.A1.ToString(nfi) );
        lvi.SubItems.Add ( f.A2.ToString(nfi) );
        lvi.SubItems.Add ( f.ZeroVal.ToString(nfi) );
        lvi.SubItems.Add ( f.DispLimit.ToString(nfi) );
        lvi.SubItems.Add ( f.SpanFac.ToString(nfi) );
        lvi.SubItems.Add ( f.UserVal.ToString(nfi) );

        lvi.SubItems.Add ( f.CalType.ToString() );

        string s;
        s = (f.P0x == -1) ? "" : f.P0x.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P0y == -1) ? "" : f.P0y.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P1x == -1) ? "" : f.P1x.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P1y == -1) ? "" : f.P1y.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P2x == -1) ? "" : f.P2x.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P2y == -1) ? "" : f.P2y.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P3x == -1) ? "" : f.P3x.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P3y == -1) ? "" : f.P3y.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P4x == -1) ? "" : f.P4x.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P4y == -1) ? "" : f.P4y.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P5x == -1) ? "" : f.P5x.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P5y == -1) ? "" : f.P5y.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P6x == -1) ? "" : f.P6x.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P6y == -1) ? "" : f.P6y.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P7x == -1) ? "" : f.P7x.ToString(nfi); lvi.SubItems.Add (s);
        s = (f.P7y == -1) ? "" : f.P7y.ToString(nfi); lvi.SubItems.Add (s);
        
        lvi.SubItems.Add ( f.ConcUnit );
        // Check: Unique entry?
        // Note: Two entries are considered as non-unique, if their 'Substance' members coincide.
        bool bEntryExists = false;
        for (int i=0; i < _lvWindows.Items.Count ;i++) 
        {
          if (i != idx) // Do not consider the selected entry itself 
          {
            ListViewItem lvi1 = _lvWindows.Items[i];
            if (lvi1.Text.ToUpper() == lvi.Text.ToUpper())
            {
              // No: 
              // Show message
              string fmt = r.GetString ("ScriptEditor_btnWindow_Edit_Error_EntryExists"); // "An entry for '{0}' does already exist.\r\nThe edited entry will be rejected."
              string sMsg = string.Format (fmt, lvi.Text);
              string sCap = r.GetString ("Form_Common_TextError");                        // "Fehler"
              MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
              // Indicate, that the entry already exists 
              bEntryExists = true;
              break;
            }
          }
        }
        if (!bEntryExists) 
        {
          // Correct entry:
          // Remove the (old) entry
          _lvWindows.Items.RemoveAt (idx);
          // Insert the edited (new) entry instead
          _lvWindows.Items.Insert (idx, lvi);
          // Adjust the selection of the 'Windows' ListView and ensure its visibility
          _lvWindows.Focus ();
          _lvWindows.Items[idx].Selected = true;
          _lvWindows.Items[idx].EnsureVisible ();
        }
      }
      f.Dispose ();
    }

    /// <summary>
    /// 'Click' event of the 'Add (Cmd)' button control
    /// </summary>
    private void _btnCmd_Add_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Check: At most MAX_SCRIPT_EVENTS commands should be edited!
      if (_lvCommands.Items.Count >= Script.MAX_SCRIPT_EVENTS)
      {
        // Show message
        string fmt = r.GetString ("ScriptEditor_btnCmd_Add_Info_MaxScriptEvents");  // "At most {0} script commands can be edited.\r\nThis number is already reached, thats why a further edition is impossible."
        string sMsg = string.Format (fmt, Script.MAX_SCRIPT_EVENTS);
        string sCap = r.GetString ("Form_Common_TextInfo");                         // "Information"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
        return;
      }
      // Add
      ScriptEditorCmdForm f = new ScriptEditorCmdForm (Common.FormOpenMode.Add);
      DialogResult dr = f.ShowDialog ();
      if (dr == DialogResult.OK) 
      {
        // Overtake
        ListViewItem lvi = new ListViewItem ();
        lvi.Text = f.Time.ToString();     // 'Time' is therewith the 1. subitem (index 0) of the SubItemCollection
        lvi.SubItems.Add ( f.Cmd );       // 'Cmd' is therewith the 2. subitem (index 1) of the SubItemCollection
        lvi.SubItems.Add ( f.Parameter ); // ...
        // Check: Unique entry?
        // Note: Two entries are considered as non-unique, if all its members coincide.
        bool bEntryExists = false;
        for (int i=0; i < _lvCommands.Items.Count ;i++) 
        {
          ListViewItem lvi1 = _lvCommands.Items[i];
          if ( 
            (lvi1.Text.ToUpper() == lvi.Text.ToUpper()) &&
            (lvi1.SubItems[1].Text.ToUpper () == lvi.SubItems[1].Text.ToUpper ()) &&
            (lvi1.SubItems[2].Text.ToUpper () == lvi.SubItems[2].Text.ToUpper ()) 
            )
          {
            // No: 
            // Show message
            string sText = string.Format ("{0} {1} {2}", lvi.Text, lvi.SubItems[1].Text, lvi.SubItems[2].Text);
            string fmt = r.GetString ("ScriptEditor_btnCmd_Add_Error_EntryExists"); // "The entry '{0}' does already exist.\r\nThe new entry will not be added."
            string sMsg = string.Format (fmt, sText);
            string sCap = r.GetString ("Form_Common_TextError");                    // "Fehler"
            MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
            // Indicate, that the entry already exists 
            bEntryExists = true;
            break;
          }
        }
        if (!bEntryExists) 
        {
          // Correct entry:
          // Add it to the list at the appropriate Time position 
          Boolean bEntryAdded = false;
          for (int i=0; i < _lvCommands.Items.Count ;i++) 
          {
            ListViewItem lvi1 = _lvCommands.Items[i];
            if (int.Parse(lvi1.Text) >= int.Parse(lvi.Text))
            {
              // Insert the entry at the appropriate Time position
              _lvCommands.Items.Insert (i, lvi);
              // Adjust the selection of the 'Script commands' ListView and ensure its visibility
              _lvCommands.Focus ();
              _lvCommands.Items[i].Selected = true;
              _lvCommands.Items[i].EnsureVisible ();
              // Indicate, that the entry was added
              bEntryAdded = true;
              break;
            }
          }
          if (!bEntryAdded)
          {
            _lvCommands.Items.Add (lvi);
            // Adjust the selection of the 'Script commands' ListView and ensure its visibility
            _lvCommands.Focus ();
            _lvCommands.Items[_lvCommands.Items.Count-1].Selected = true;
            _lvCommands.Items[_lvCommands.Items.Count-1].EnsureVisible ();
          }
        }
      }
      f.Dispose ();
    }

    /// <summary>
    /// 'Click' event of the 'Edit (Cmd)' button control
    /// </summary>
    private void _btnCmd_Edit_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      // Check, whether a script command is selected or not
      int n = _lvCommands.SelectedItems.Count;
      if (0 == n) 
        return; // No.
      // Yes:
      // Get the idx of the selected entry
      ListViewItem lviSel = _lvCommands.SelectedItems[0];
      int idx = _lvCommands.SelectedIndices[0];
      // Open the ScriptEditorCmd dialog, handing over initialisation info
      ScriptEditorCmdForm f = new ScriptEditorCmdForm (Common.FormOpenMode.Edit);
      f.Time      = int.Parse (lviSel.Text);  // 'Time' is at the same time the 1. subitem (index 0) of the SubItemCollection
      f.Cmd       = lviSel.SubItems[1].Text;  // 'Cmd' is the 2. subitem (index 1) of the SubItemCollection
      f.Parameter = lviSel.SubItems[2].Text;  // ...
      DialogResult dr = f.ShowDialog ();
      if (dr == DialogResult.OK) 
      {
        // Overtake
        ListViewItem lvi = new ListViewItem ();
        lvi.Text = f.Time.ToString();     // 'Time' is therewith the 1. subitem (index 0) of the SubItemCollection
        lvi.SubItems.Add ( f.Cmd );       // 'Cmd' is therewith the 2. subitem (index 1) of the SubItemCollection
        lvi.SubItems.Add ( f.Parameter ); // ...
        // Check: Unique entry?
        // Note: Two entries are considered as non-unique, if all its members coincide.
        bool bEntryExists = false;
        for (int i=0; i < _lvCommands.Items.Count ;i++) 
        {
          if (i != idx) // Do not consider the selected entry itself 
          {
            ListViewItem lvi1 = _lvCommands.Items[i];
            if ( 
              (lvi1.Text.ToUpper() == lvi.Text.ToUpper()) &&
              (lvi1.SubItems[1].Text.ToUpper () == lvi.SubItems[1].Text.ToUpper ()) &&
              (lvi1.SubItems[2].Text.ToUpper () == lvi.SubItems[2].Text.ToUpper ()) 
              )
            {
              // No: 
              // Show message
              string sText = string.Format ("{0} {1} {2}", lvi.Text, lvi.SubItems[1].Text, lvi.SubItems[2].Text);
              string fmt = r.GetString ("ScriptEditor_btnCmd_Edit_Error_EntryExists");  // "The entry '{0}' does already exist.\r\nThe edited entry will be rejected."
              string sMsg = string.Format (fmt, sText);
              string sCap = r.GetString ("Form_Common_TextError");                      // "Fehler"
              MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
              // Indicate, that the entry already exists 
              bEntryExists = true;
              break;
            }
          }
        }
        if (!bEntryExists) 
        {
          // Correct entry:
          // Remove the (old) entry
          _lvCommands.Items.RemoveAt (idx);
          // Insert the edited (new) entry at the appropriate Time position instead
          Boolean bEntryInserted = false;
          for (int i=0; i < _lvCommands.Items.Count ;i++) 
          {
            ListViewItem lvi1 = _lvCommands.Items[i];
            if (int.Parse(lvi1.Text) >= int.Parse(lvi.Text))
            {
              // Insert the entry at the appropriate Time position
              _lvCommands.Items.Insert (i, lvi);
              // Adjust the selection of the 'Script commands' ListView and ensure its visibility
              _lvCommands.Focus ();
              _lvCommands.Items[i].Selected = true;
              _lvCommands.Items[i].EnsureVisible ();
              // Indicate, that the entry was added
              bEntryInserted = true;
              break;
            }
          }
          if (!bEntryInserted)
          {
            _lvCommands.Items.Add (lvi);
            // Adjust the selection of the 'Script commands' ListView and ensure its visibility
            _lvCommands.Focus ();
            _lvCommands.Items[_lvCommands.Items.Count-1].Selected = true;
            _lvCommands.Items[_lvCommands.Items.Count-1].EnsureVisible ();
          }
        }
      }
      f.Dispose ();
    }

    /// <summary>
    /// 'Click' event of the 'Up/Down Commands' button controls
    /// </summary>
    private void _btnUpDown_Click(object sender, System.EventArgs e)
    {
      try
      {
        ListView lv = this._lvCommands;
        Button btn = (Button)sender;
        ListViewItem lviSel = lv.SelectedItems[0];
        int idx = lv.SelectedIndices[0];
        if (btn == this._btnUp)
        {
          // Up
          if (idx > 0)
          {
            lv.Items.RemoveAt (idx);
            idx--;
            lv.Items.Insert (idx, lviSel);
            lv.Focus ();
            lv.Items[idx].Selected = true;
          }
        }
        else if (btn == this._btnDown)
        {
          // Down
          if (idx < lv.Items.Count-1)
          {
            lv.Items.RemoveAt (idx);
            idx++;
            lv.Items.Insert (idx, lviSel);
            lv.Focus ();
            lv.Items[idx].Selected = true;
          }
        }
      }
      catch
      {}
    }
    
    /// <summary>
    /// 'Click' event of the 'Remove SpanConc/Automated span monitoring/Windows/Commands' button controls
    /// </summary>
    private void _btnRemove_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      // ListView assignement
      ListView lv = null;
      if      (sender == this._btnSpanConc_Remove)    lv = this._lvSpanConc;
      else if (sender == this._btnAutoSpanMon_Remove) lv = this._lvAutoSpanMon;
      else if (sender == this._btnWindow_Remove)      lv = this._lvWindows;
      else if (sender == this._btnCmd_Remove)         lv = this._lvCommands;
      if (null == lv)
        return;

      // Check, whether a row is selected or not
      int n = lv.SelectedItems.Count;
      if (0 == n) 
        return; // No.
      
      // Yes:
      // Safety request
      ListViewItem lviSel = lv.SelectedItems[0];
      string s = "";
      for (int i=0; i < Math.Min (3, lviSel.SubItems.Count) ;i++)
      {
        s += string.Format (" {0}", lviSel.SubItems[i].Text);
      }
      if (lviSel.SubItems.Count > 3) s += " ...";
      string fmt = r.GetString ("ScriptEditor_btnRemove_Que_Delete");  // "Should the entry\r\n'{0}'\r\nreally be deleted?"
      string sMsg = string.Format (fmt, s);
      string sCap = r.GetString ("Form_Common_TextSafReq");            // "Sicherheitsabfrage"
      DialogResult dr = MessageBox.Show (sMsg, sCap, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
      if (dr == DialogResult.No)
        return; // No.
      
      // Yes:
      // Remove the entry from the ListView
      int idx = lv.SelectedIndices[0];
      lv.Items.RemoveAt (idx);
      
      // Select the next entry
      n = lv.Items.Count;
      if (n > 0)
      {
        if (idx < n-1) lv.Items[idx].Selected = true;
        else           lv.Items[n-1].Selected = true;
      }
    }

    /// <summary>
    /// 'DoubleClick' event of the 'SpanConc/Automated span monitoring/Windows/Commands' ListView controls
    /// </summary>
    private void _lv_DoubleClick(object sender, System.EventArgs e)
    {
      if      (sender == this._lvSpanConc)
        // Start the 'Edit (Span concentration)' action
        _btnSpanConc_Edit_Click(null, new System.EventArgs ());
      else if (sender == this._lvAutoSpanMon)
        // Start the 'Edit (Automated span monitoring)' action
        _btnAutoSpanMon_Edit_Click(null, new System.EventArgs ());
      else if (sender == this._lvWindows)
        // Start the 'Edit (Window)' action
        _btnWindow_Edit_Click(null, new System.EventArgs ());
      else if (sender == this._lvCommands)
        // Start the 'Edit (Cmd)' action
        _btnCmd_Edit_Click(null, new System.EventArgs ());
    }

    /// <summary>
    /// 'Click' event of the 'OK' button 
    /// </summary>
    private void _btnOK_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
    
      // Script check:
      //    Script name
      object o;
      if ( !Check.Execute ( this._txtName, CheckType.String, CheckRelation.IN, 1, Script.MAX_SCRIPT_NAMELENGTH, true, out o) )
        return;
      string sScrName=(string) o;
      //    Script parameter: Meas. cycle, (for the CB's no check is required.), ScriptID, Total
      if ( !Check.Execute ( this._txtMeasCycle, CheckType.Int, CheckRelation.GE, 100, 0, true, out o) )
        return;
      if ( !Check.Execute ( this._txtScriptID, CheckType.UInt32, CheckRelation.GE, 0, 0, true, out o) )
        return;
      if ( !Check.Execute ( this._txtTotal, CheckType.String, CheckRelation.IN, 0, ScriptParameter.MAXLENGTH_TOTAL, true, out o) )
        return;
      //    Script windows: No check required (Can be empty - a script without windows is valid)
      //    Script commands: Check required (Must not be empty) 
      if (_lvCommands.Items.Count == 0)
      {
        string sMsg = r.GetString ("ScriptEditor_btnOK_Error_NoCmds");          // "Keine Scriptbefehle vorhanden.\r\nEin Script ohne Befehle ist nicht zul�ssig."
        string sCap = r.GetString ("Form_Common_TextError");                    // "Fehler"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        _btnCmd_Add.Focus ();
        return;
      }

      // Script check OK:
      // Check: Does the script already exist in the script collection?
      int idx = _sc.FindScript (sScrName);
      Boolean bScrExist = (-1 == idx) ? false : true;
      if (bScrExist)
      {
        // The script already exists:
        // Check: If a script should be added, then an error is present at this point.
        //        OR
        //        If a script should be edited, and its name is already assigned (to another script), 
        //        then an error is present at this point.
        if ((_idx == -1) || ((_idx != -1) && (_idx != idx)))
        {
          // Error
          string sFmt = r.GetString ("ScriptEditor_btnOK_Err_ScrExists");   // "A script with name '{0}' does already exist in the configuration.\r\nPlease, choose another name."
          string sMsg = string.Format (sFmt, sScrName);
          string sCap = r.GetString ("Form_Common_TextError");              // "Error"
          DialogResult dr = MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
          // Reassign the original name of the script & focus it
          this._txtName.Text = _sScrNameOrig;
          this._txtName.Focus ();
          return;
        }
      }
      
      // Create a script object based on the control contents
      _scr = _ContentsToScript (sScrName);
      // Check the script for validity
      try
      {
        _scr.Check ();
      }
      catch (System.Exception exc)
      {
        string sMsg = exc.Message;
        string sCap = r.GetString ("Form_Common_TextError");                    // "Fehler"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }

      // OK:
      this.DialogResult = DialogResult.OK;
      this.Close ();
    }

    /// <summary>
    /// 'HelpRequested' event of some controls
    /// </summary>
    private void _ctl_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Help requesting control
      Control requestingControl = (Control)sender;
      // Assign help text
      string s = "";
      
      // Script name
      if      (requestingControl == this._txtName)
        s = r.GetString ("ScriptEditor_Hlp_txtName");             // "The name of the script.";
      
        // Script parameter
      else if (requestingControl == this._txtMeasCycle)
        s = r.GetString ("ScriptEditor_Hlp_txtMeasCycle");        // "The duration of a measurement cycle (in 1/10 s).";
      else if (requestingControl == this._cbSigProcInt)
        s = r.GetString ("ScriptEditor_Hlp_cbSigProcInt");        // "The signal processing interval (in pts).";
      else if (requestingControl == this._cbPCCommInt)
        s = r.GetString ("ScriptEditor_Hlp_cbPCCommInt");         // "The PC communication interval (in pts).";
 
      else if (requestingControl == this._chkMeasScript)
        s = r.GetString ("ScriptEditor_Hlp_chkMeasScript");       // "Indicates, whether the script is a meas. script (checked) or not (unchecked).";

      else if (requestingControl == this._chkOffsetCorr)
        s = r.GetString ("ScriptEditor_Hlp_chkOffsetCorr");       // "Indicates, whether a static scan offset correction should be performed (checked) or not (unchecked).";

      else if (requestingControl == this._txtScriptID)
        s = r.GetString ("ScriptEditor_Hlp_txtScriptID");         // "The script index (dimensionless)."
      else if (requestingControl == _lvSpanConc)
        s = r.GetString ("ScriptEditor_Hlp_lvSpanConc");          // "Contains the Span concentrations that are present in the 'Parameter' section of the script.";
      else if (requestingControl == _btnSpanConc_Add)
        s = r.GetString ("ScriptEditor_Hlp_btnSpanConc_Add");     // "Adds a Span concentration to the 'Parameter' section of the script.";
      else if (requestingControl == _btnSpanConc_Edit)
        s = r.GetString ("ScriptEditor_Hlp_btnSpanConc_Edit");    // "Edits a Span concentration from the 'Parameter' section of the script.";
      else if (requestingControl == _btnSpanConc_Remove)
        s = r.GetString ("ScriptEditor_Hlp_btnSpanConc_Remove");  // "Removes a Span concentration from the 'Parameter' section of the script.";
      else if (requestingControl == this._lvAutoSpanMon)
        s = r.GetString ("ScriptEditor_Hlp_lvAutoSpanMon");       // "Contains the 'Automated span monitoring' elements that are present in the 'Parameter' section of the script.";
      else if (requestingControl == _btnAutoSpanMon_Add)
        s = r.GetString ("ScriptEditor_Hlp_btnAutoSpanMon_Add");  // "Adds an 'Automated span monitoring' element to the 'Parameter' section of the script.";
      else if (requestingControl == _btnAutoSpanMon_Edit)
        s = r.GetString ("ScriptEditor_Hlp_btnAutoSpanMon_Edit"); // "Edits an 'Automated span monitoring' element from the 'Parameter' section of the script.";
      else if (requestingControl == _btnAutoSpanMon_Remove)
        s = r.GetString ("ScriptEditor_Hlp_btnAutoSpanMon_Remove");// "Removes an 'Automated span monitoring' element from the 'Parameter' section of the script.";
       
      else if (requestingControl == this._txtTotal)
        s = r.GetString ("ScriptEditor_Hlp_txtTotal");            // "The summary signal identifier."

        // Script windows
      else if (requestingControl == _lvWindows)
        s = r.GetString ("ScriptEditor_Hlp_lvWindows");           // "Contains the substance windows that are present in the 'Windows' section of the script.";
      else if (requestingControl == _btnWindow_Add)
        s = r.GetString ("ScriptEditor_Hlp_btnWindow_Add");       // "Adds a substance window to the 'Windows' section of the script.";
      else if (requestingControl == _btnWindow_Edit)
        s = r.GetString ("ScriptEditor_Hlp_btnWindow_Edit");      // "Edits a substance window from the 'Windows' section of the script.";
      else if (requestingControl == _btnWindow_Remove)
        s = r.GetString ("ScriptEditor_Hlp_btnWindow_Remove");    // "Removes a substance window from the 'Windows' section of the script.";
     
        // Script commands
      else if (requestingControl == _lvCommands)
        s = r.GetString ("ScriptEditor_Hlp_lvCommands");          // "Contains the script commands that are present in the 'Script' section of the script.";
      else if (requestingControl == _btnCmd_Add)
        s = r.GetString ("ScriptEditor_Hlp_btnCmd_Add");          // "Adds a script command to the 'Script' section of the script.";
      else if (requestingControl == _btnCmd_Edit)
        s = r.GetString ("ScriptEditor_Hlp_btnCmd_Edit");         // "Edits a script command from the 'Script' section of the script.";
      else if (requestingControl == _btnCmd_Remove)
        s = r.GetString ("ScriptEditor_Hlp_btnCmd_Remove");       // "Removes a script command from the 'Script' section of the script.";
      else if (requestingControl == this._btnUp)
        s = r.GetString ("ScriptEditor_Hlp_btnCmd_btnUp");        // "Verschiebt den ausgew�hlten Item aufw�rts in der Liste."
      else if (requestingControl == this._btnDown)
        s = r.GetString ("ScriptEditor_Hlp_btnCmd_btnDown");      // "Verschiebt den ausgew�hlten Item abw�rts in der Liste."
     
        // Comments
      else if (requestingControl == this._txtCmt_Scr)
        s = r.GetString ("ScriptEditor_Hlp_txtCmt");              // "The comment, that appears in the corresponding script section."
      else if (requestingControl == this._txtCmt_Par)
        s = r.GetString ("ScriptEditor_Hlp_txtCmt");              // "The comment, that appears in the corresponding script section."
      else if (requestingControl == this._txtCmt_Wnd)
        s = r.GetString ("ScriptEditor_Hlp_txtCmt");              // "The comment, that appears in the corresponding script section."
      else if (requestingControl == this._txtCmt_Cmd)
        s = r.GetString ("ScriptEditor_Hlp_txtCmt");              // "The comment, that appears in the corresponding script section."
        
      // Show help
      this._ToolTip.SetToolTip (requestingControl, s);
      hlpevent.Handled=true;
    }

    /// <summary>
    /// 'Tick' event of the '_Timer' control
    /// </summary>
    private void _Timer_Tick(object sender, System.EventArgs e)
    {
      //-------------------------------
      // Section 'Script parameter'
      
      // Check: At most MAX_SCRIPT_WINDOWS Span concentration rows may be edited
      Boolean b = (_lvSpanConc.Items.Count < Script.MAX_SCRIPT_WINDOWS);
      this._btnSpanConc_Add.Enabled = b;

      // Check, whether a Span concentration is selected or not
      b = (_lvSpanConc.SelectedItems.Count > 0);
      this._btnSpanConc_Edit.Enabled = b;
      this._btnSpanConc_Remove.Enabled = b;

      // Check: At most MAX_SCRIPT_WINDOWS 'Automated span monitoring' rows may be edited
      b = (this._lvAutoSpanMon.Items.Count < Script.MAX_SCRIPT_WINDOWS);
      this._btnAutoSpanMon_Add.Enabled = b;

      // Check, whether an 'Automated span monitoring' element is selected or not
      b = (_lvAutoSpanMon.SelectedItems.Count > 0);
      this._btnAutoSpanMon_Edit.Enabled = b;
      this._btnAutoSpanMon_Remove.Enabled = b;
      
      //-------------------------------
      // Section 'Script windows'

      // Check: At most MAX_SCRIPT_WINDOWS script windows may be edited
      b = (_lvWindows.Items.Count < Script.MAX_SCRIPT_WINDOWS);
      this._btnWindow_Add.Enabled = b;

      // Check, whether a substance window is selected or not
      b = (_lvWindows.SelectedItems.Count > 0);
      this._btnWindow_Edit.Enabled = b;
      this._btnWindow_Remove.Enabled = b;

      //-------------------------------
      // Section 'Script commands'

      // Check: At most MAX_SCRIPT_EVENTS commands may be edited
      b = (_lvCommands.Items.Count < Script.MAX_SCRIPT_EVENTS);
      this._btnCmd_Add.Enabled = b;

      // Check, whether a script command is selected or not
      b = (_lvCommands.SelectedItems.Count > 0);
      this._btnCmd_Edit.Enabled = b;
      this._btnCmd_Remove.Enabled = b;
      this._btnUp.Enabled = b;
      this._btnDown.Enabled = b;
      if (b)
      {
        int n = _lvCommands.Items.Count;
        int idx = _lvCommands.SelectedIndices[0];
        if      ( idx == 0)     this._btnUp.Enabled = false;
        else if ( idx == n-1 )  this._btnDown.Enabled = false;
      }

    }
   
    #endregion event handling

    #region members

    /// <summary>
    /// The script collection
    /// </summary>
    ScriptCollection _sc = null;

    /// <summary>
    /// The idx of the script to be edited (-1 in case of adding a script)
    /// </summary>
    int _idx = -1;
    
    /// <summary>
    /// A script object
    /// </summary>
    Script _scr = new Script ();
    
    /// <summary>
    /// The original name of the script
    /// </summary>
    string _sScrNameOrig = "";
    
    #endregion members
 
    #region constants
    #endregion constants
  
    #region methods

    /// <summary>
    /// Initializes the form.
    /// </summary>
    void _Init ()
    {
      int i;

      // Fill the 'SigProcInt' CB
      for (i=ScriptParameter.SigProcInt_Min; i <= ScriptParameter.SigProcInt_Max; i *= ScriptParameter.SigProcInt_Fac)
        _cbSigProcInt.Items.Add (i);
      // Fill the 'PCCommInt' CB
      for (i=ScriptParameter.PCCommInt_Min; i <= ScriptParameter.PCCommInt_Max; i *= ScriptParameter.PCCommInt_Fac)
        _cbPCCommInt.Items.Add (i);
    }
    
    /// <summary>
    /// Initializes the controls.
    /// </summary>
    void _InitControls ()
    {
      // Script name
      _txtName.Text = "";
      
      // Script parameter 
      //    (Default values)
      this._txtMeasCycle.Text = "";
      this._cbSigProcInt.Text = ScriptParameter.SigProcInt_Def.ToString ();
      this._cbPCCommInt.Text = ScriptParameter.PCCommInt_Def.ToString ();
      //    Meas. script ident.    
      this._chkMeasScript.Checked = false;
      //    Offset correction
      this._chkOffsetCorr.Checked = false;
      //    ScriptID CD
      this._txtScriptID.Text = "0";
      //    Span conc's      
      _lvSpanConc.Items.Clear ();
      //    Automated span monitoring
      this._lvAutoSpanMon.Items.Clear ();
      //    Total
      this._txtTotal.Text = "";

      // Script windows
      _lvWindows.Items.Clear ();

      // Script commands
      _lvCommands.Items.Clear ();

      // Comments
      this._txtCmt_Scr.Text = "";
      this._txtCmt_Par.Text = "";
      this._txtCmt_Wnd.Text = "";
      this._txtCmt_Cmd.Text = "";
    }

    /// <summary>
    /// Updates the control contents based on a script object.
    /// </summary>
    /// <param name="scr">The script object</param>
    void _ScriptToContents (Script scr)
    {
      // TextBox: Script name
      _txtName.Text = scr.Name;
      
      // ------------------------
      // Section: Parameter
      ScriptParameter sp = scr.Parameter;
      //    Meas. cycle time
      _txtMeasCycle.Text = sp.dwCycleTime.ToString ();
      //    Signal processing interval
      this._cbSigProcInt.Text = sp.dwSigProcInt.ToString ();
      //    PC comm. interval
      this._cbPCCommInt.Text = sp.dwPCCommInt.ToString ();
      //    Meas. script ident.
      this._chkMeasScript.Checked = (sp.byMeasScript == 1) ? true : false;
      //    Static scan offset correction
      this._chkOffsetCorr.Checked = (sp.byOffsetCorr == 1) ? true : false;
      //    Script ID
      _txtScriptID.Text = sp.dwScriptID.ToString ();
      //    Span concentrations
      //    (Including: Adjust the selection of the 'Span concentration' ListView and ensure its visibility)
      _lvSpanConc.Items.Clear ();
      for (int i=0; i < sp.arfSpanConc.Length; i++)
      {
        if (sp.arfSpanConc[i] > 0)
        {
          string[] ars = new string [2];
          System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
          ars[0] = ScriptCollection.SpanConc + i.ToString ();
          ars[1] = sp.arfSpanConc[i].ToString (nfi);
          ListViewItem lvi = new ListViewItem (ars);
          _lvSpanConc.Items.Add (lvi); 
        }
      }
      if (_lvSpanConc.Items.Count > 0)
      {
        _lvSpanConc.Focus ();
        _lvSpanConc.Items[_lvSpanConc.Items.Count-1].Selected = true;
        _lvSpanConc.Items[_lvSpanConc.Items.Count-1].EnsureVisible ();
      }
      //    Automated span monitoring
      //    (Including: Adjust the selection of the 'Automated span monitoring' ListView and ensure its visibility)
      this._lvAutoSpanMon.Items.Clear ();
      for (int i=0; i < sp.arAutoSpanMon.Length; i++)
      {
        if (sp.arAutoSpanMon[i].Enabled)
        {
          string[] ars = new string [4];
          System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
          ars[0] = ScriptCollection.AutoSpanMon + i.ToString ();
          ars[1] = sp.arAutoSpanMon[i].LowLimit.ToString (nfi);
          ars[2] = sp.arAutoSpanMon[i].UppLimit.ToString (nfi);
          ars[3] = sp.arAutoSpanMon[i].NoErrMeas.ToString ();
          ListViewItem lvi = new ListViewItem (ars);
          _lvAutoSpanMon.Items.Add (lvi); 
        }
      }
      if (_lvAutoSpanMon.Items.Count > 0)
      {
        _lvAutoSpanMon.Focus ();
        _lvAutoSpanMon.Items[_lvAutoSpanMon.Items.Count-1].Selected = true;
        _lvAutoSpanMon.Items[_lvAutoSpanMon.Items.Count-1].EnsureVisible ();
      }
      //    Summary signal identifier
      this._txtTotal.Text = sp.sTotal;
      
      // ------------------------
      // Section: Windows
      _lvWindows.Items.Clear ();
      for (int i=0; i < scr.NumWindows; i++)
      {
        ScriptWindow sw = scr.Windows[i];
        string[] ars = sw.ToStringFull().Split (',');
        ListViewItem lvi = new ListViewItem (ars);
        _lvWindows.Items.Add (lvi); 
      }
      // Adjust the selection of the 'Windows' ListView and ensure its visibility
      if (_lvWindows.Items.Count > 0)
      {
        _lvWindows.Focus ();
        _lvWindows.Items[_lvWindows.Items.Count-1].Selected = true;
        _lvWindows.Items[_lvWindows.Items.Count-1].EnsureVisible ();
      }

      // ------------------------
      // Section: Commands
      _lvCommands.Items.Clear ();
      for (int i=0; i < scr.NumCommands; i++)
      {
        ScriptEvent se = scr.Cmds[i];
        string[] ars = se.ToString().Split (',');
        // Secure, that the LV item to be build contained exactly 3 subitems
        ars = _LimitCmdItemNo (ars);
        ListViewItem lvi = new ListViewItem (ars);
        _lvCommands.Items.Add (lvi); 
      }
      // Adjust the selection of the 'Script commands' ListView and ensure its visibility
      if (_lvCommands.Items.Count > 0)
      {
        _lvCommands.Focus ();
        _lvCommands.Items[_lvCommands.Items.Count-1].Selected = true;
        _lvCommands.Items[_lvCommands.Items.Count-1].EnsureVisible ();
      }

      // ------------------------
      // Update the TextBoxes, corr'ing with script level comments
      CommentToTB (this._txtCmt_Scr, scr.arsComment);
      CommentToTB (this._txtCmt_Par, scr.arsParComment);
      CommentToTB (this._txtCmt_Wnd, scr.arsWndComment);
      CommentToTB (this._txtCmt_Cmd, scr.arsCmdComment);
    }

    /// <summary>
    /// Reorganizes the input cmd item array in a form, that the corr'ing output cmd item array 
    /// contained exactly 3 members: time, cmd, parameters (comma separated, if > 1) 
    /// </summary>
    /// <param name="ars">the input cmd item array</param>
    /// <returns>the corr'ing output cmd item array</returns>
    string[] _LimitCmdItemNo (string[] ars)
    {
      int i, j;
      int n = Math.Min(3, ars.Length);
      string[] ars1 = new string[n];
      for (i=0; i < 2; i++) ars1[i]=ars[i];
      for (ars1[i]="", j=i; j < ars.Length; j++)
      {
        ars1[i] += ars[j];
        if (j < ars.Length-1) ars1[i] += ",";
      }
      return ars1;
    }

    /// <summary>
    /// Creates a script object based on the control contents.
    /// </summary>
    /// <param name="sCfgName">The name of the script</param>
    /// <returns>The script object</returns>
    Script _ContentsToScript (string sCfgName)
    {
      // Create a script object & assign its name
      Script scr = new Script();  
      scr.Name = sCfgName;
      
      // ------------------------
      // Section: Parameter
      ScriptParameter sp = scr.Parameter;
      //    Meas. cycle time
      sp.dwCycleTime = UInt32.Parse (_txtMeasCycle.Text.Trim());
      //    Signal processing interval
      sp.dwSigProcInt = UInt32.Parse (this._cbSigProcInt.Text.Trim());
      //    PC comm. interval
      sp.dwPCCommInt = UInt32.Parse (this._cbPCCommInt.Text.Trim());
      //    Meas. script ident.
      sp.byMeasScript = (byte) (this._chkMeasScript.Checked ? 1 : 0);
      //    Static scan offset correction
      sp.byOffsetCorr = (byte) (this._chkOffsetCorr.Checked ? 1 : 0);
      //    Script ID
      sp.dwScriptID = UInt32.Parse (_txtScriptID.Text);
      //    Span concentrations
      for (int j=0; j < _lvSpanConc.Items.Count; j++)
      {
        ListViewItem lvi = _lvSpanConc.Items[j];
        // Current Span concentration (string)
        string sLine = string.Empty;
        for (int i=0; i < lvi.SubItems.Count ;i++)
        {
          sLine += string.Format ("{0}", lvi.SubItems[i].Text);
          if (i < lvi.SubItems.Count-1) sLine += ",";
        }
        // Current Span concentration (object)
        string sKeyVal = ScriptCollection.FormatItemString(sLine);
        sp.Load (sKeyVal);
      }
      //    Automated span monitoring
      for (int j=0; j < this._lvAutoSpanMon.Items.Count; j++)
      {
        ListViewItem lvi = _lvAutoSpanMon.Items[j];
        // Current 'Automated span monitoring' member (string)
        string sLine = string.Empty;
        for (int i=0; i < lvi.SubItems.Count ;i++)
        {
          sLine += string.Format ("{0}", lvi.SubItems[i].Text);
          if (i < lvi.SubItems.Count-1) sLine += ",";
        }
        // Current 'Automated span monitoring' member (object)
        string sKeyVal = ScriptCollection.FormatItemString(sLine);
        sp.Load (sKeyVal);
      }
      //    Summary signal identifier
      sp.sTotal = this._txtTotal.Text;
      
      // ------------------------
      // Section: Windows
      for (int j=0; j < _lvWindows.Items.Count; j++)
      {
        ListViewItem lvi = _lvWindows.Items[j];
        ScriptWindow sw = scr.Windows[j];
        // Current window (string)
        string sLine = string.Empty;
        for (int i=0; i < lvi.SubItems.Count ;i++)
        {
          sLine += string.Format ("{0}", lvi.SubItems[i].Text);
          if (i < lvi.SubItems.Count-1) sLine += ",";
        }
        // Current window (object)
        string sKeyVal = ScriptCollection.FormatItemString(sLine);
        sw.LoadFull (sKeyVal);
      }
      scr.NumWindows = _lvWindows.Items.Count; 
      
      // ------------------------
      // Section: Commands
      for (int j=0; j < _lvCommands.Items.Count; j++)
      {
        ListViewItem lvi = _lvCommands.Items[j];
        ScriptEvent se = scr.Cmds[j];
        // Current command (string)
        string sLine = string.Empty;
        for (int i=0; i < lvi.SubItems.Count ;i++)
        {
          sLine += string.Format ("{0}", lvi.SubItems[i].Text);
          if (i < lvi.SubItems.Count-1) sLine += ",";
        }
        // Current command (object)
        string sKeyVal = ScriptCollection.FormatItemString(sLine);
        se.Load (sKeyVal);
      }
      scr.NumCommands = _lvCommands.Items.Count;

      // ------------------------
      // Update the comments (at script level)
      TBToComment (this._txtCmt_Scr, scr.arsComment);
      TBToComment (this._txtCmt_Par, scr.arsParComment);
      TBToComment (this._txtCmt_Wnd, scr.arsWndComment);
      TBToComment (this._txtCmt_Cmd, scr.arsCmdComment);

      // Done
      return scr;
    }
    
    /// <summary>
    /// Updates a 'Comments' TextBox control based on the corr'ing comments, 
    /// available in the script collection.
    /// </summary>
    /// <param name="t">The 'Comments' TextBox</param>
    /// <param name="ar">The comments</param>
    void CommentToTB (TextBox t, StringArray ar)
    {
      // Build the TextBox text (lines, separated by NL; avoid empty lines)
      string sCmt = string.Empty;
      for (int i=0; i < ar.Count; i++)
        if (ar[i].Trim().Length > 0) sCmt += ar[i] + "\r\n";
      // Update the TextBox text corr'ly
      t.Text = sCmt;
    }

    /// <summary>
    /// Updates the comments, available in a script collection, based on the corr'ing
    /// 'Comments' TextBox control.
    /// </summary>
    /// <param name="t">The 'Comments' TextBox</param>
    /// <param name="ar">The comments</param>
    void TBToComment (TextBox t, StringArray ar)
    {
      // Extract all strings of text from the TextBox control 
      string[] arCmt = new string [t.Lines.Length];
      arCmt = t.Lines;
      // Update the comments corr'ly (avoid empty lines)
      ar.Clear ();
      for (int i=0; i < arCmt.Length; i++)
        if (arCmt[i].Trim().Length > 0) ar.Add (arCmt[i]);
    }

    #endregion methods

    #region properties

    /// <summary>
    /// The script collection object
    /// </summary>
    public ScriptCollection ScrColl
    {
      set { _sc = value; }
    }
    
    /// <summary>
    /// The idx of the script to be edited (-1 in case of adding a script)
    /// </summary>
    public int Index
    {
      set { _idx = value; }
    }
    
    /// <summary>
    /// The script object
    /// </summary>
    public Script Script
    {
      get { return _scr; }
    }
    
    #endregion properties

  }
}
