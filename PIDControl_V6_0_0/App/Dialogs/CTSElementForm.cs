using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace App
{
  /// <summary>
  /// Class CTSElementForm:
  /// Clock-timed script
  /// </summary>
  public class CTSElementForm : System.Windows.Forms.Form
  {
    private System.Windows.Forms.Button _btnOK;
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.ToolTip _ToolTip;
    private System.Windows.Forms.Label _lblScript;
    private System.Windows.Forms.ComboBox _cbScript;
    private System.Windows.Forms.Label _lblUnit;
    private System.Windows.Forms.ComboBox _cbUnit;
    private System.Windows.Forms.Label _lblRunningPeriod;
    private System.Windows.Forms.TextBox _txtRunningPeriod;
    private System.ComponentModel.IContainer components;

    /// <summary>
    /// Constructor
    /// </summary>
    public CTSElementForm()
    {
      InitializeComponent();
      // Init.
      Init ();
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this._btnOK = new System.Windows.Forms.Button();
      this._btnCancel = new System.Windows.Forms.Button();
      this._lblRunningPeriod = new System.Windows.Forms.Label();
      this._txtRunningPeriod = new System.Windows.Forms.TextBox();
      this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
      this._lblScript = new System.Windows.Forms.Label();
      this._cbScript = new System.Windows.Forms.ComboBox();
      this._lblUnit = new System.Windows.Forms.Label();
      this._cbUnit = new System.Windows.Forms.ComboBox();
      this.SuspendLayout();
      // 
      // _btnOK
      // 
      this._btnOK.Location = new System.Drawing.Point(72, 72);
      this._btnOK.Name = "_btnOK";
      this._btnOK.TabIndex = 6;
      this._btnOK.Text = "OK";
      this._btnOK.Click += new System.EventHandler(this._btnOK_Click);
      // 
      // _btnCancel
      // 
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point(172, 72);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.TabIndex = 7;
      this._btnCancel.Text = "Cancel";
      // 
      // _lblRunningPeriod
      // 
      this._lblRunningPeriod.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblRunningPeriod.Location = new System.Drawing.Point(8, 36);
      this._lblRunningPeriod.Name = "_lblRunningPeriod";
      this._lblRunningPeriod.TabIndex = 2;
      this._lblRunningPeriod.Text = "Running period:";
      // 
      // _txtRunningPeriod
      // 
      this._txtRunningPeriod.Location = new System.Drawing.Point(112, 36);
      this._txtRunningPeriod.Name = "_txtRunningPeriod";
      this._txtRunningPeriod.Size = new System.Drawing.Size(68, 20);
      this._txtRunningPeriod.TabIndex = 3;
      this._txtRunningPeriod.Text = "";
      this._txtRunningPeriod.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _ToolTip
      // 
      this._ToolTip.AutoPopDelay = 10000;
      this._ToolTip.InitialDelay = 500;
      this._ToolTip.ReshowDelay = 100;
      // 
      // _lblScript
      // 
      this._lblScript.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblScript.Location = new System.Drawing.Point(8, 8);
      this._lblScript.Name = "_lblScript";
      this._lblScript.TabIndex = 0;
      this._lblScript.Text = "Script:";
      // 
      // _cbScript
      // 
      this._cbScript.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this._cbScript.Location = new System.Drawing.Point(112, 8);
      this._cbScript.Name = "_cbScript";
      this._cbScript.Size = new System.Drawing.Size(212, 21);
      this._cbScript.TabIndex = 1;
      this._cbScript.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblUnit
      // 
      this._lblUnit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblUnit.Location = new System.Drawing.Point(188, 36);
      this._lblUnit.Name = "_lblUnit";
      this._lblUnit.Size = new System.Drawing.Size(64, 23);
      this._lblUnit.TabIndex = 4;
      this._lblUnit.Text = "Unit:";
      // 
      // _cbUnit
      // 
      this._cbUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this._cbUnit.Location = new System.Drawing.Point(256, 36);
      this._cbUnit.Name = "_cbUnit";
      this._cbUnit.Size = new System.Drawing.Size(68, 21);
      this._cbUnit.TabIndex = 5;
      this._cbUnit.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // CTSElementForm
      // 
      this.AcceptButton = this._btnOK;
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.CancelButton = this._btnCancel;
      this.ClientSize = new System.Drawing.Size(330, 108);
      this.Controls.Add(this._cbUnit);
      this.Controls.Add(this._lblUnit);
      this.Controls.Add(this._cbScript);
      this.Controls.Add(this._lblScript);
      this.Controls.Add(this._lblRunningPeriod);
      this.Controls.Add(this._txtRunningPeriod);
      this.Controls.Add(this._btnCancel);
      this.Controls.Add(this._btnOK);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.HelpButton = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "CTSElementForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Clock-timed script";
      this.Load += new System.EventHandler(this.CTSElementForm_Load);
      this.ResumeLayout(false);

    }

    #endregion

    #region members

    /// <summary>
    /// The script name
    /// </summary>
    string _Script = "";
    
    /// <summary>
    /// The running period
    /// </summary>
    int _RunningPeriod = 60;

    /// <summary>
    /// The unit
    /// </summary>
    string _Unit = "min";

    #endregion members

    #region events

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void CTSElementForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      try
      {
        // Resources
        this.Text               = r.GetString ( "CTSElement_Title" );               // "Clock-timed script"
        this._btnCancel.Text    = r.GetString ( "ScriptEditorWindow_btnCancel" );   // "Abbruch"
        this._btnOK.Text        = r.GetString ( "ScriptEditorWindow_btnOK" );       // "OK"
        
        this._lblScript.Text    = r.GetString ( "CTSElement_lblScript" );           // "Script:"
        this._lblRunningPeriod.Text = r.GetString ( "CTSElement_lblRunningPeriod" );// "Laufzeit:"
        this._lblUnit.Text      = r.GetString ( "CTSElement_lblUnit" );             // "Unit:"
      }
      catch
      {
      }

      // Control initialisation
      this._cbScript.Text         = _Script;
      this._txtRunningPeriod.Text = _RunningPeriod.ToString ();
      this._cbUnit.Text           = _Unit;
    }

    /// <summary>
    /// 'Click' event of the 'OK' button
    /// </summary>
    private void _btnOK_Click(object sender, System.EventArgs e)
    {
      object o;
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Script (there must be a script selected)
      o = this._cbScript.SelectedItem;
      if (null == o)
      {
        // Error:
        // Show message
        string sMsg = r.GetString ("CTSElement_btnOK_Err_1"); // "No script selected."
        string sCap = r.GetString ("Form_Common_TextError");  // "Error"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        // Cursor
        this._cbScript.Focus ();
        return;
      }
      _Script = o.ToString ();

      // Running period (> 0)
      if ( !Check.Execute ( this._txtRunningPeriod, CheckType.Int, CheckRelation.GT, 0, 0, true, out o) )
        return;
      _RunningPeriod=(int) o;
      
      // Unit (there must be a unit selected)
      o = this._cbUnit.SelectedItem;
      if (null == o)
      {
        // Error:
        // Show message
        string sMsg = r.GetString ("CTSElement_btnOK_Err_2"); // "No unit selected."
        string sCap = r.GetString ("Form_Common_TextError");  // "Error"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        // Cursor
        this._cbUnit.Focus ();
        return;
      }
      _Unit = o.ToString ();
      
      // Notes:
      //  The maximum permitted running period is (2^16-1) min. This must be proofed for.
      int nRunningPeriod = 0;
      switch (_Unit)
      {
        case "min": nRunningPeriod = _RunningPeriod; break;
        case "h":   nRunningPeriod = _RunningPeriod * 60; break;
      }
      if (nRunningPeriod > UInt16.MaxValue)
      {
        // Show message
        string fmt = r.GetString ("CTSElement_btnOK_Err_3");  // "The maximum permitted running period is {0} min.\r\nPlease correct."
        string sMsg = string.Format (fmt, UInt16.MaxValue);
        string sCap = r.GetString ("Form_Common_TextError");  // "Error"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        // Cursor
        this._txtRunningPeriod.Focus ();
        return;
      }

      // OK
      this.DialogResult = DialogResult.OK;
      this.Close ();
    }
 
    /// <summary>
    /// 'HelpRequested' event of the controls
    /// </summary>
    private void _ctl_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Help requesting control
      Control requestingControl = (Control)sender;
      // Assign help text
      string s = "";
      if      (requestingControl == _cbScript)
        s = r.GetString ("CTSElement_Hlp_txtScript");         // "The name of the script";
      else if (requestingControl == _txtRunningPeriod)
        s = r.GetString ("CTSElement_Hlp_txtRunningPeriod");  // "The running period of the script";
      else if (requestingControl == _cbUnit)
        s = r.GetString ("CTSElement_Hlp_cbUnit");            // "The unit of the scripts running period";
     
      // Show help
      this._ToolTip.SetToolTip (requestingControl, s);
      hlpevent.Handled=true;
    }

    #endregion events

    #region methods
    
    /// <summary>
    /// Init's the form.
    /// </summary>
    void Init ()
    {
      App app = App.Instance;
      Doc doc = app.Doc;

      // Scripts CB
      this._cbScript.Items.Clear ();
      foreach (string s in doc.arsDeviceScripts)
      {
        this._cbScript.Items.Add (s);
      }
      // Units CB
      this._cbUnit.Items.Clear ();
      this._cbUnit.Items.AddRange (new object[] { "min", "h" });
    }

    #endregion methods

    #region properties

    /// <summary>
    /// The script name
    /// </summary>
    public string Script
    {
      get { return _Script; }
      set { _Script = value; }
    }

    /// <summary>
    /// The running period
    /// </summary>
    public int RunningPeriod
    {
      get { return _RunningPeriod; }
      set { _RunningPeriod = value; }
    }
 
    /// <summary>
    /// The unit
    /// </summary>
    public string Unit
    {
      get { return _Unit; }
      set { _Unit = value; }
    }

    # endregion properties

  }

}
