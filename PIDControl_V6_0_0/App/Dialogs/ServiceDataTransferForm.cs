using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using System.Text;

using CommRS232;

namespace App
{
	/// <summary>
  /// Class ServiceDataTransferForm:
	/// Service data transfer dialog ( transfer to the PID device )
	/// </summary>
  public class ServiceDataTransferForm : System.Windows.Forms.Form
  {
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.Timer _Timer;
    private System.Windows.Forms.TextBox _txtMsg;
    private System.Windows.Forms.ProgressBar _ProgressBar;
    private System.ComponentModel.IContainer components;

    /// <summary>
    /// Constructor
    /// </summary>
    public ServiceDataTransferForm()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container ();
      this._btnCancel = new System.Windows.Forms.Button ();
      this._txtMsg = new System.Windows.Forms.TextBox ();
      this._Timer = new System.Windows.Forms.Timer (this.components);
      this._ProgressBar = new System.Windows.Forms.ProgressBar ();
      this.SuspendLayout ();
      // 
      // _btnCancel
      // 
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point (252, 112);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.Size = new System.Drawing.Size (75, 23);
      this._btnCancel.TabIndex = 0;
      this._btnCancel.Text = "Cancel";
      // 
      // _txtMsg
      // 
      this._txtMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._txtMsg.Location = new System.Drawing.Point (8, 8);
      this._txtMsg.Multiline = true;
      this._txtMsg.Name = "_txtMsg";
      this._txtMsg.ReadOnly = true;
      this._txtMsg.Size = new System.Drawing.Size (318, 94);
      this._txtMsg.TabIndex = 1;
      // 
      // _Timer
      // 
      this._Timer.Interval = 50;
      this._Timer.Tick += new System.EventHandler (this._Timer_Tick);
      // 
      // _ProgressBar
      // 
      this._ProgressBar.Location = new System.Drawing.Point (8, 112);
      this._ProgressBar.Name = "_ProgressBar";
      this._ProgressBar.Size = new System.Drawing.Size (236, 23);
      this._ProgressBar.TabIndex = 2;
      // 
      // ServiceDataTransferForm
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size (5, 13);
      this.CancelButton = this._btnCancel;
      this.ClientSize = new System.Drawing.Size (334, 142);
      this.Controls.Add (this._ProgressBar);
      this.Controls.Add (this._txtMsg);
      this.Controls.Add (this._btnCancel);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ServiceDataTransferForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Transmit service data";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler (this.ServiceDataTransferForm_FormClosed);
      this.Load += new System.EventHandler (this.ServiceDataTransferForm_Load);
      this.ResumeLayout (false);
      this.PerformLayout ();

    }
    
    #endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void ServiceDataTransferForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      Ressources r = app.Ressources;

      // Resources
      this.Text = r.GetString("ServiceDataTransfer_Title");                 // "Übertragung der Servicedaten"
      this._btnCancel.Text = r.GetString("ServiceDataTransfer_btnCancel");  // "Abbruch"
      string msg = r.GetString("ServiceDataTransfer_txtMsg");               // "Die Servicedaten von Datei\r\n   {0}\r\nwerden übertragen ..."
      this._txtMsg.Text = string.Format(msg, app.Data.Common.sServiceFileName);

      // Timer interval
      _Timer.Interval = 50;

      try
      {
        // Check: Communication channel open?
        if (!app.Comm.IsOpen())
        {
          // No: Close dialog with Cancel
          Debug.WriteLine("ServiceDataTransferForm.Load -> Comm is not open\n");
          throw new ApplicationException();
        }

        // Yes:
        // Send (transmit ) service data to IMS device 
        _SendServiceData();
      }
      catch
      {
        this.DialogResult = DialogResult.Cancel;
        Close();
      }

      // Init. progres bar
      _nTotalNoOfMessages = app.Comm.MessageCount;
      this._ProgressBar.Minimum = 0;
      this._ProgressBar.Maximum = _nTotalNoOfMessages;
      this._ProgressBar.Value = 0;
    }

    /// <summary>
    /// 'FormClosed' event of the form
    /// </summary>
    private void ServiceDataTransferForm_FormClosed(object sender, FormClosedEventArgs e)
    {
      // Disable timer
      _Timer.Enabled = false;

      // Check: Is a Transfer process still in progress?
      if (_bTransferInProgress)
      {
        // Yes:
        // Check: Did we already get the name of the script, that was lastly running?
        if (_sLastScriptName.Length > 0)
        {
          // Yes:

          // Finish the transfer
          _EndTransfer ();
        }
      }
    }

    /// <summary>
    /// 'Tick' event of the _Timer control
    /// </summary>
    private void _Timer_Tick(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      
      // Check: Communication channel open?
      if ( !comm.IsOpen() ) 
      {
        // No: Close dialog with OK
        this.DialogResult = DialogResult.OK;
        Close ();
      }
  
      // Update progres bar
      int nNoOfMessages = comm.MessageCount;
      this._ProgressBar.Value = _nTotalNoOfMessages - nNoOfMessages;

      // Check: Transfer in progress? 
      // If so, we have not to proof for idle communication
      if (_bTransferInProgress)
        return;

      // Transfer finished:
      // Wait until communication is idle, afterwards wait a little (here: 1000 ms) so that all comm. processes 
      // (RX evaluation) could finish, and after that close the script transfer dialog  
      if (!_bIsIdle)
      {
        if ( comm.IsIdle() ) 
        {
          // Communication is idle: Start waiting a little 
          _bIsIdle = true;
          _nIdle = 0;
        }
      }
      else
      {
        _nIdle++;
        if (_nIdle == 20)
        {
          // Finish waiting: Close the dialog
          this.DialogResult = DialogResult.OK;
          Close ();
        }
      }
    }

    #endregion // event handling

    #region private members

    /// <summary>
    /// True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    bool _bShowWait = false;
   
    /// <summary>
    /// Total # of messages
    /// </summary>
    int _nTotalNoOfMessages;
    
    /// <summary>
    /// Waiting after communication is idle: Indicator
    /// </summary>
    bool _bIsIdle = false;
    /// <summary>
    /// Waiting after communication is idle: Counter
    /// </summary>
    int _nIdle = 0;

    /// <summary>
    /// Indicates, whether a Transfer process is curr'ly in progress ( true ) or not ( false )
    /// </summary>
    bool _bTransferInProgress = false;
    
    /// <summary>
    /// Name of the script, that was lastly running on the device
    /// </summary>
    string _sLastScriptName = string.Empty;
    
    /// <summary>
    /// The Service Data string array (to be transferred to the device)
    /// </summary>
    string[] _arsServiceData = null;
    /// <summary>
    /// The Service Data type ( representing the indizes of the Service Data string array )
    /// </summary>
    int _currDataType = 0;
    
    #endregion // private members

    #region methods

    /// <summary>
    /// Sends the service data to the device
    /// </summary>
    void _SendServiceData ()
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // Check: Valid service data file string?
      if (0 == doc.sServiceData.Length)
      {
        // No
        string sMsg = r.GetString("ServiceDataTransfer_Error_InvalidServiceDataFile");  // "Keine Servicedatendatei geladen."
        string sCap = r.GetString("Form_Common_TextInfo");                              // "Information";
        MessageBox.Show(this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
        throw new ApplicationException ();
      }
      
      try
      {
        CommMessage msg;
        ServiceDataFile sdf = new ServiceDataFile();
        // Update the DateTime item in the Service data file string
        // Notes:
        //  Because a service data file contains a DateTime which is usually not up to date,
        //  the current DateTime must be introduced here.
        string sTotal = doc.sServiceData;
        string sDT = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss");
        if (!sdf.SetValue(ref sTotal, "DateTime", sDT))
        {
          // Error
          string sMsg = r.GetString("ServiceDataTransfer_Error_DT");  // "Fehler beim Aktualisieren der Datum/Zeit-Daten."
          string sCap = r.GetString("Form_Common_Error");             // "Error";
          MessageBox.Show(this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
          throw new ApplicationException ();
        }
        // Conversion: Service data file string -> Service Data string array
        _arsServiceData = sdf.ServiceDataAsStringArray(sTotal);

        // Indicate that the Transfer process has begun.
        _bTransferInProgress = true;
        // Enable timer
        _Timer.Enabled = true;
        // TX: Script Current
        //    Parameter: none
        //    Device: Action - nothing; 
        //            Returns - Name of the script currently running on the IMS device
        msg = new CommMessage (comm.msgScriptCurrent);
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
        // TX: Transfer Start
        //    Parameter: The transfer task to be performed
        //    Device: Action - Indicate, that the Service-Write transfer has begun; 
        //            Returns - OK
        msg = new CommMessage (comm.msgTransferStart);
        msg.Parameter = TransferTask.Service_Write.ToString ("D");
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
        // TX: Service Write Data
        //    Parameter: The Service Data type and the corr'ing Service Data string
        //    Device: Action - Actualizes the service data on the device; 
        //            Returns - OK
        for (int i = 0; i < _arsServiceData.Length; i++)
        {
          msg = new CommMessage(comm.msgServiceWriteData);
          msg.Parameter = string.Format("{0},{1}", i, _arsServiceData[i]);
          msg.WindowInfo = new WndInfo(this.Handle, _bShowWait);
          comm.WriteMessage(msg);
        }
        // Notes:
        //  Because the end-transfer cmd SCRSELECT expects a meaningful information from the device (the name of the script lastly running),
        //  that is received by means of the SCRCURRENT cmd, we cannot call the end-transfer cmds (SCRSELECT, TRNCOMPLETE) in a straightforward manner
        //  subsequently from here. Instead we have to wait, until this information was received from device. Thats why we call 
        //  the end-transfer cmds only after receipt of the answer of the last WriteData cmd SVCWRITEDATA. 
      }
      catch
      {
        Debug.WriteLine("ServiceDataTransferForm._SendServiceData failed\n");
        throw;
      }
    }

    /// <summary>
    /// Finishes the transfer
    /// </summary>
    void _EndTransfer ()
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Doc doc = app.Doc;

      // TX: Script Select
      //    Parameter: The name of the script to be executed
      //    Device: Action - Selects the script to be executed (here: script last used); 
      //            Returns - OK
      CommMessage msg = new CommMessage (comm.msgScriptSelect);
      msg.Parameter = doc.ScriptNameToIdx (_sLastScriptName).ToString ();
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);

      // TX: Transfer Complete
      //    Parameter: none
      //    Device: Action - Indicate, that the transfer has completed; 
      //            Returns - OK
      msg = new CommMessage (comm.msgTransferComplete);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);

      // Indicate that the Transfer process has finished.
      _bTransferInProgress = false;
    }
    
    /// <summary>
    /// Performs actions in reaction of the receipt of a comm. message  
    /// </summary>
    /// <param name="msgRX">The comm. message</param>
    public void AfterRXCompleted(CommMessage msgRX)
    {
      App app = App.Instance;
      Doc doc = app.Doc;

      // CD: Message response ID
      if ((msgRX.ResponseID == AppComm.CMID_TIMEOUT) || (msgRX.ResponseID == AppComm.CMID_STRLENERR))
      {
        // A comm. error (Timeout, ...) occurred:

        // do nothing
      }
      
      else if (msgRX.ResponseID == AppComm.CMID_ANSWER)
      {
        // The message response is present:
        string sCmd = msgRX.CommandResponse;
        byte[] arbyPar = msgRX.ParameterResponse;

        // Get the parameter string from the parameter Byte array
        string sPar = Encoding.ASCII.GetString (arbyPar);
        // CD according to the message command
        switch ( sCmd ) 
        {

          //----------------------------------------------------------
          // Common Transfer messages
          //----------------------------------------------------------

          case "SCRCURRENT":
            // ScriptCurrent
            // RX: Name of the script currently running on the device   
            try
            {
              // Get the name of the script, that was lastly running on the IMS device
              _sLastScriptName = sPar;
              // Testing only
              string sMsg = string.Format ("SCRCURRENT: {0}", sPar);
              Debug.WriteLine (sMsg);
            }
            catch
            {
              Debug.WriteLine ("OnMsgScriptCurrent->function failed");
            }
            break;

          case "SCRSELECT":
            // ScriptSelect
            // RX: OK
            try
            {
              // Testing only
              string sMsg = string.Format ("SCRSELECT: {0}", sPar);
              Debug.WriteLine (sMsg);
            }
            catch
            {
              Debug.WriteLine ("OnMsgScriptSelect->function failed");
            }
            break;

          case "TRNSTART":
            // Transfer Start
            // RX: OK
            try
            {
              // Testing only
              string sMsg = string.Format ("TRNSTART: {0}", sPar);
              Debug.WriteLine (sMsg);
            }
            catch
            {
              Debug.WriteLine ("OnMsgTransferStart->function failed");
            }
            break;

          case "TRNCOMPLETE":
            // Transfer Complete
            // RX: OK
            try
            {
              // Testing only
              string sMsg = string.Format ("TRNCOMPLETE: {0}", sPar);
              Debug.WriteLine (sMsg);
            }
            catch
            {
              Debug.WriteLine ("OnMsgTransferComplete->function failed");
            }
            break;
          
          //----------------------------------------------------------
          // Service-Write
          //----------------------------------------------------------

          case "SVCWRITEDATA":
            // Service Write Data
            // RX: OK
            try
            {
              // Testing only
              string sMsg = string.Format("SVCWRITEDATA: {0}", sPar); 
              Debug.WriteLine ( sMsg );

              // After receipt of the answer of the last WriteData cmd SVCWRITEDATA, the transfer should be finished
              if (_currDataType++ == _arsServiceData.Length - 1)
              {
                // Finish the transfer
                _EndTransfer ();
              }
            }
            catch
            {
              Debug.WriteLine("OnServiceWriteData->function failed");
            }
            break;

        }
      }//E - if (e.Message.Id = AppComm.CMID_ANSWER)

    }
    
    #endregion // methods

    #region properties

    /// <summary>
    ///  True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    public bool ShowWait
    {
      get { return _bShowWait; }
      set { _bShowWait = value; }
    }

    #endregion properties

  }
}
