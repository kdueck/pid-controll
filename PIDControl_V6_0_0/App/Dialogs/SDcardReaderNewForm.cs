// If defined, the 'Clear' action is processed in the devices 'Script transfer' mode.
// If not defined, the devices 'Script transfer' mode will not be used.
#define SDCARDREADER_CLEAR_WITH_INTERRUPTION

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Text;
using System.IO;
using System.Diagnostics;

using CommRS232;

namespace App
{
	/// <summary>
	/// Class SDcardReaderNewForm:
	/// GSMReader implementation for SD card (for IUT-built devices) - new design
	/// </summary>
	/// <remarks>
	/// 1.
  /// The RDTFI (Read Text file contents) cmd is implemented here as follows:
  /// First ALL the paths of the text files to be read from device are put into a transfer list.
  /// Then each file of the transfer list is treated sequentielly: 
  /// The 1st file path in the transfer list is put into the TX message list, after it the corr'ing file 
  /// contents are received, they are processed (storage to HD), and the corr'ing entry is removed from 
  /// the transfer list. This procedure is repeated until the transfer list was empty.
  /// The advantage of this approach is clear:
  /// All the processes are synchronized - one thing happens after another has finished. So
  /// a 'non-syncronisation' should not happen, and all is OK.
  /// Thats why we decided TO USE this implementation.
  /// (See remarks for 'SDcardReaderForm_0'.)
  /// 2.
	/// In order to monitor a SD card reinitialisation process a timer component is used. The following
	/// circumstances have to be considered thereby:
	/// The (initially tried) usage of a System.Windows.Forms.Timer didn't work - its timer callback 
	/// was NOT activated by the system after performing the "INDSK_S" (start reinitialisation) command, 
	/// as desired.
	/// Thats why a System.Threading.Timer was used instead - all works fine.
	/// </remarks>
  public class SDcardReaderNewForm : System.Windows.Forms.Form
  {
    private System.Windows.Forms.MainMenu mnu;
    private System.Windows.Forms.MenuItem menuItem1;
    private System.Windows.Forms.MenuItem menuItem2;
    private System.Windows.Forms.GroupBox gpTransfer;
    private System.Windows.Forms.Label lblTransferRate;
    private System.Windows.Forms.TextBox txtFileContents;
    private System.Windows.Forms.Label lblFileContents;
    private System.Windows.Forms.CheckBox chkFilePreview;
    private System.Windows.Forms.ProgressBar pgbTransfer;
    private System.Windows.Forms.GroupBox gpDevice;
    private System.Windows.Forms.ListView lvFiles;
    private System.Windows.Forms.TreeView tvFolders;
    private System.Windows.Forms.Label lblFolders;
    private System.Windows.Forms.Label lblFiles;
    private System.Windows.Forms.GroupBox gpStorageFolder;
    private System.Windows.Forms.TextBox txtStorageFolder;
    private System.Windows.Forms.Button btnStorageFolder;
    private System.Windows.Forms.ToolTip _ToolTip;
    private System.Windows.Forms.ImageList _ImageList;
    private System.Windows.Forms.Timer _Timer;
    private System.Windows.Forms.StatusBar _StatusBar;
    private System.Windows.Forms.MenuItem miAction;
    private System.Windows.Forms.MenuItem miAction_CopySel;
    private System.Windows.Forms.MenuItem miAction_SDcardCopy;
    private System.Windows.Forms.MenuItem miAction_SDcardInit;
    private System.Windows.Forms.MenuItem miAction_MarkAll;
    private System.Windows.Forms.MenuItem miAction_ToggleMarking;
    private System.Windows.Forms.MenuItem miAction_ClearFolder;
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Constructor
    /// </summary>
    public SDcardReaderNewForm()
    {
      InitializeComponent();
      
      // Init.
      Init ();
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if (components != null) 
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SDcardReaderNewForm));
      this._Timer = new System.Windows.Forms.Timer(this.components);
      this.mnu = new System.Windows.Forms.MainMenu();
      this.miAction = new System.Windows.Forms.MenuItem();
      this.miAction_CopySel = new System.Windows.Forms.MenuItem();
      this.miAction_SDcardCopy = new System.Windows.Forms.MenuItem();
      this.menuItem1 = new System.Windows.Forms.MenuItem();
      this.miAction_ClearFolder = new System.Windows.Forms.MenuItem();
      this.miAction_SDcardInit = new System.Windows.Forms.MenuItem();
      this.menuItem2 = new System.Windows.Forms.MenuItem();
      this.miAction_MarkAll = new System.Windows.Forms.MenuItem();
      this.miAction_ToggleMarking = new System.Windows.Forms.MenuItem();
      this.gpTransfer = new System.Windows.Forms.GroupBox();
      this.lblTransferRate = new System.Windows.Forms.Label();
      this.txtFileContents = new System.Windows.Forms.TextBox();
      this.lblFileContents = new System.Windows.Forms.Label();
      this.chkFilePreview = new System.Windows.Forms.CheckBox();
      this.pgbTransfer = new System.Windows.Forms.ProgressBar();
      this.gpDevice = new System.Windows.Forms.GroupBox();
      this.lblFiles = new System.Windows.Forms.Label();
      this.lblFolders = new System.Windows.Forms.Label();
      this.lvFiles = new System.Windows.Forms.ListView();
      this.tvFolders = new System.Windows.Forms.TreeView();
      this._ImageList = new System.Windows.Forms.ImageList(this.components);
      this.gpStorageFolder = new System.Windows.Forms.GroupBox();
      this.txtStorageFolder = new System.Windows.Forms.TextBox();
      this.btnStorageFolder = new System.Windows.Forms.Button();
      this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
      this._StatusBar = new System.Windows.Forms.StatusBar();
      this.gpTransfer.SuspendLayout();
      this.gpDevice.SuspendLayout();
      this.gpStorageFolder.SuspendLayout();
      this.SuspendLayout();
      // 
      // _Timer
      // 
      this._Timer.Tick += new System.EventHandler(this._Timer_Tick);
      // 
      // mnu
      // 
      this.mnu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
                                                                        this.miAction});
      // 
      // miAction
      // 
      this.miAction.Index = 0;
      this.miAction.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
                                                                             this.miAction_CopySel,
                                                                             this.miAction_SDcardCopy,
                                                                             this.menuItem1,
                                                                             this.miAction_ClearFolder,
                                                                             this.miAction_SDcardInit,
                                                                             this.menuItem2,
                                                                             this.miAction_MarkAll,
                                                                             this.miAction_ToggleMarking});
      this.miAction.Text = "&Actions";
      this.miAction.Popup += new System.EventHandler(this.miPopup);
      // 
      // miAction_CopySel
      // 
      this.miAction_CopySel.Index = 0;
      this.miAction_CopySel.Text = "Auswahl &kopieren";
      this.miAction_CopySel.Click += new System.EventHandler(this.miClick);
      this.miAction_CopySel.Select += new System.EventHandler(this.miSelect);
      // 
      // miAction_SDcardCopy
      // 
      this.miAction_SDcardCopy.Index = 1;
      this.miAction_SDcardCopy.Text = "&SD-Karte kopieren";
      this.miAction_SDcardCopy.Click += new System.EventHandler(this.miClick);
      this.miAction_SDcardCopy.Select += new System.EventHandler(this.miSelect);
      // 
      // menuItem1
      // 
      this.menuItem1.Index = 2;
      this.menuItem1.Text = "-";
      this.menuItem1.Select += new System.EventHandler(this.miSelect);
      // 
      // miAction_ClearFolder
      // 
      this.miAction_ClearFolder.Index = 3;
      this.miAction_ClearFolder.Text = "&Ordner l�schen/leeren";
      this.miAction_ClearFolder.Click += new System.EventHandler(this.miClick);
      this.miAction_ClearFolder.Select += new System.EventHandler(this.miSelect);
      // 
      // miAction_SDcardInit
      // 
      this.miAction_SDcardInit.Index = 4;
      this.miAction_SDcardInit.Text = "SD-Karte &initialisieren";
      this.miAction_SDcardInit.Click += new System.EventHandler(this.miClick);
      this.miAction_SDcardInit.Select += new System.EventHandler(this.miSelect);
      // 
      // menuItem2
      // 
      this.menuItem2.Index = 5;
      this.menuItem2.Text = "-";
      this.menuItem2.Select += new System.EventHandler(this.miSelect);
      // 
      // miAction_MarkAll
      // 
      this.miAction_MarkAll.Index = 6;
      this.miAction_MarkAll.Text = "Alles &markieren";
      this.miAction_MarkAll.Click += new System.EventHandler(this.miClick);
      this.miAction_MarkAll.Select += new System.EventHandler(this.miSelect);
      // 
      // miAction_ToggleMarking
      // 
      this.miAction_ToggleMarking.Index = 7;
      this.miAction_ToggleMarking.Text = "Markierung &umkehren";
      this.miAction_ToggleMarking.Click += new System.EventHandler(this.miClick);
      this.miAction_ToggleMarking.Select += new System.EventHandler(this.miSelect);
      // 
      // gpTransfer
      // 
      this.gpTransfer.Controls.Add(this.lblTransferRate);
      this.gpTransfer.Controls.Add(this.txtFileContents);
      this.gpTransfer.Controls.Add(this.lblFileContents);
      this.gpTransfer.Controls.Add(this.chkFilePreview);
      this.gpTransfer.Controls.Add(this.pgbTransfer);
      this.gpTransfer.Location = new System.Drawing.Point(404, 56);
      this.gpTransfer.Name = "gpTransfer";
      this.gpTransfer.Size = new System.Drawing.Size(352, 244);
      this.gpTransfer.TabIndex = 2;
      this.gpTransfer.TabStop = false;
      this.gpTransfer.Text = "File transfer from device to PC";
      // 
      // lblTransferRate
      // 
      this.lblTransferRate.Location = new System.Drawing.Point(292, 218);
      this.lblTransferRate.Name = "lblTransferRate";
      this.lblTransferRate.Size = new System.Drawing.Size(56, 22);
      this.lblTransferRate.TabIndex = 4;
      this.lblTransferRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txtFileContents
      // 
      this.txtFileContents.Location = new System.Drawing.Point(8, 66);
      this.txtFileContents.Multiline = true;
      this.txtFileContents.Name = "txtFileContents";
      this.txtFileContents.ReadOnly = true;
      this.txtFileContents.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this.txtFileContents.Size = new System.Drawing.Size(338, 148);
      this.txtFileContents.TabIndex = 2;
      this.txtFileContents.Text = "";
      this.txtFileContents.WordWrap = false;
      this.txtFileContents.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // lblFileContents
      // 
      this.lblFileContents.Location = new System.Drawing.Point(8, 42);
      this.lblFileContents.Name = "lblFileContents";
      this.lblFileContents.Size = new System.Drawing.Size(338, 22);
      this.lblFileContents.TabIndex = 1;
      this.lblFileContents.Text = "File contents:";
      this.lblFileContents.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // chkFilePreview
      // 
      this.chkFilePreview.Location = new System.Drawing.Point(8, 18);
      this.chkFilePreview.Name = "chkFilePreview";
      this.chkFilePreview.Size = new System.Drawing.Size(338, 22);
      this.chkFilePreview.TabIndex = 0;
      this.chkFilePreview.Text = "File preview";
      this.chkFilePreview.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      this.chkFilePreview.CheckedChanged += new System.EventHandler(this.chkFilePreview_CheckedChanged);
      // 
      // pgbTransfer
      // 
      this.pgbTransfer.Location = new System.Drawing.Point(8, 216);
      this.pgbTransfer.Name = "pgbTransfer";
      this.pgbTransfer.Size = new System.Drawing.Size(280, 23);
      this.pgbTransfer.TabIndex = 3;
      // 
      // gpDevice
      // 
      this.gpDevice.Controls.Add(this.lblFiles);
      this.gpDevice.Controls.Add(this.lblFolders);
      this.gpDevice.Controls.Add(this.lvFiles);
      this.gpDevice.Controls.Add(this.tvFolders);
      this.gpDevice.Location = new System.Drawing.Point(4, 4);
      this.gpDevice.Name = "gpDevice";
      this.gpDevice.Size = new System.Drawing.Size(400, 296);
      this.gpDevice.TabIndex = 0;
      this.gpDevice.TabStop = false;
      this.gpDevice.Text = "Files on device";
      // 
      // lblFiles
      // 
      this.lblFiles.Location = new System.Drawing.Point(144, 20);
      this.lblFiles.Name = "lblFiles";
      this.lblFiles.Size = new System.Drawing.Size(252, 16);
      this.lblFiles.TabIndex = 2;
      this.lblFiles.Text = "Files";
      // 
      // lblFolders
      // 
      this.lblFolders.Location = new System.Drawing.Point(8, 20);
      this.lblFolders.Name = "lblFolders";
      this.lblFolders.Size = new System.Drawing.Size(134, 16);
      this.lblFolders.TabIndex = 0;
      this.lblFolders.Text = "Folders";
      // 
      // lvFiles
      // 
      this.lvFiles.HideSelection = false;
      this.lvFiles.Location = new System.Drawing.Point(144, 38);
      this.lvFiles.Name = "lvFiles";
      this.lvFiles.Size = new System.Drawing.Size(252, 254);
      this.lvFiles.TabIndex = 3;
      this.lvFiles.View = System.Windows.Forms.View.List;
      this.lvFiles.Click += new System.EventHandler(this.lvFiles_Click);
      this.lvFiles.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      this.lvFiles.SelectedIndexChanged += new System.EventHandler(this.lvFiles_SelectedIndexChanged);
      // 
      // tvFolders
      // 
      this.tvFolders.HideSelection = false;
      this.tvFolders.ImageList = this._ImageList;
      this.tvFolders.Location = new System.Drawing.Point(8, 38);
      this.tvFolders.Name = "tvFolders";
      this.tvFolders.SelectedImageIndex = 1;
      this.tvFolders.Size = new System.Drawing.Size(136, 254);
      this.tvFolders.TabIndex = 1;
      this.tvFolders.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvFolders_AfterSelect);
      this.tvFolders.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _ImageList
      // 
      this._ImageList.ImageSize = new System.Drawing.Size(16, 16);
      this._ImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_ImageList.ImageStream")));
      this._ImageList.TransparentColor = System.Drawing.Color.Magenta;
      // 
      // gpStorageFolder
      // 
      this.gpStorageFolder.Controls.Add(this.txtStorageFolder);
      this.gpStorageFolder.Controls.Add(this.btnStorageFolder);
      this.gpStorageFolder.Location = new System.Drawing.Point(404, 4);
      this.gpStorageFolder.Name = "gpStorageFolder";
      this.gpStorageFolder.Size = new System.Drawing.Size(352, 48);
      this.gpStorageFolder.TabIndex = 1;
      this.gpStorageFolder.TabStop = false;
      this.gpStorageFolder.Text = "Storage folder";
      // 
      // txtStorageFolder
      // 
      this.txtStorageFolder.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txtStorageFolder.Location = new System.Drawing.Point(35, 24);
      this.txtStorageFolder.Name = "txtStorageFolder";
      this.txtStorageFolder.ReadOnly = true;
      this.txtStorageFolder.Size = new System.Drawing.Size(310, 13);
      this.txtStorageFolder.TabIndex = 1;
      this.txtStorageFolder.Text = "";
      // 
      // btnStorageFolder
      // 
      this.btnStorageFolder.Image = ((System.Drawing.Image)(resources.GetObject("btnStorageFolder.Image")));
      this.btnStorageFolder.Location = new System.Drawing.Point(7, 20);
      this.btnStorageFolder.Name = "btnStorageFolder";
      this.btnStorageFolder.Size = new System.Drawing.Size(22, 23);
      this.btnStorageFolder.TabIndex = 0;
      this.btnStorageFolder.Click += new System.EventHandler(this.btnStorageFolder_Click);
      this.btnStorageFolder.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _ToolTip
      // 
      this._ToolTip.AutoPopDelay = 10000;
      this._ToolTip.InitialDelay = 500;
      this._ToolTip.ReshowDelay = 100;
      // 
      // _StatusBar
      // 
      this._StatusBar.Location = new System.Drawing.Point(0, 303);
      this._StatusBar.Name = "_StatusBar";
      this._StatusBar.Size = new System.Drawing.Size(762, 20);
      this._StatusBar.SizingGrip = false;
      this._StatusBar.TabIndex = 3;
      // 
      // SDcardReaderNewForm
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.ClientSize = new System.Drawing.Size(762, 323);
      this.Controls.Add(this._StatusBar);
      this.Controls.Add(this.gpStorageFolder);
      this.Controls.Add(this.gpDevice);
      this.Controls.Add(this.gpTransfer);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.HelpButton = true;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.Menu = this.mnu;
      this.MinimizeBox = false;
      this.Name = "SDcardReaderNewForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Read SD card";
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SDcardReaderNewForm_KeyPress);
      this.Load += new System.EventHandler(this.SDcardReaderNewForm_Load);
      this.Closed += new System.EventHandler(this.SDcardReaderNewForm_Closed);
      this.gpTransfer.ResumeLayout(false);
      this.gpDevice.ResumeLayout(false);
      this.gpStorageFolder.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form 
    /// </summary>
    private void SDcardReaderNewForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Resources
      //    Initial Label strings
      _sInitiallblFileContentsTxt     = r.GetString ( "SDcardReaderNew_Txt_FileCont" );     // "File contents:";
      //    Controls
      this.Text                       = r.GetString ( "SDcardReaderNew_Title" );            // "Read SDcard"

      this.gpDevice.Text              = r.GetString ( "SDcardReaderNew_gpDevice" );         // "Files on device"  
      this.lblFolders.Text            = r.GetString ( "SDcardReaderNew_lblFolders" );       // "Folders"  
      this.lblFiles.Text              = r.GetString ( "SDcardReaderNew_lblFiles" );         // "Files"  

      this.gpStorageFolder.Text       = r.GetString ( "SDcardReaderNew_gpStorageFolder" );  // "Storage folder"  

      this.gpTransfer.Text            = r.GetString ( "SDcardReaderNew_gpTransfer" );       // "File transfer from device to PC"
      this.chkFilePreview.Text        = r.GetString ( "SDcardReaderNew_chkFilePreview" );   // "File preview"
      this.lblFileContents.Text       = _sInitiallblFileContentsTxt;                        // "File contents:";
      //    Menu
      this.miAction.Text              = r.GetString ( "SDcardReaderNew_miAction" );               // "&Aktion"
      this.miAction_CopySel.Text      = r.GetString ( "SDcardReaderNew_miAction_CopySel" );       // "Auswahl &kopieren"
      this.miAction_SDcardCopy.Text   = r.GetString ( "SDcardReaderNew_miAction_SDcardCopy" );    // "&SD-Karte kopieren"
      this.miAction_ClearFolder.Text  = r.GetString ( "SDcardReaderNew_miAction_ClearFolder" );   // "&Ordner l�schen/leeren"
      this.miAction_SDcardInit.Text   = r.GetString ( "SDcardReaderNew_miAction_SDcardInit" );    // "SD-Karte &initialisieren"
      this.miAction_MarkAll.Text      = r.GetString ( "SDcardReaderNew_miAction_MarkAll" );       // "Alles &markieren"
      this.miAction_ToggleMarking.Text= r.GetString ( "SDcardReaderNew_miAction_ToggleMarking" ); // "Markierung &umkehren"

      // Display SDcard data storage folder, if available
      if ( app.Data.Common.sSDcardFolder.Length > 0 ) 
      {
        this.txtStorageFolder.Text = app.Data.Common.sSDcardFolder;
      }
      
      // Update the form
      UpdateData (true);

      // Start timer-based action: Determine SDcard folder structure
      _eAction = Action.SDcardDetFolderStructure;
      _nTimer = 0;

      // Enable timer
      this._Timer.Enabled = true;
    }
    
    /// <summary>
    /// 'Closed' event of the form 
    /// </summary>
    private void SDcardReaderNewForm_Closed(object sender, System.EventArgs e)
    {
      App app = App.Instance;

      // Stop timer
      this._Timer.Enabled = false;
      
      // Write app data: SDcard data storage folder
      app.Data.Common.sSDcardFolder = this.txtStorageFolder.Text;
      app.Data.Write ();
      
      // Check: Is a Transfer process still in progress?
      if ( _bTransferInProgress ) 
      {
        // Yes:
        // Check: Did we already get the name of the script, that was lastly running?
        if ( _sLastScriptName.Length > 0 ) 
        {
          // Yes:
          // Finish the transfer
          _EndTransfer ( );
        }
      }
    }
  
    /// <summary>
    /// 'KeyPress' event of the form
    /// </summary>
    private void SDcardReaderNewForm_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
    {
      // Close the form on pressing 'Escape'
      if ( e.KeyChar == (char) Keys.Escape )
        this.Close ();
    }
    
    /// <summary>
    /// 'Popup' event of the menu
    /// </summary>
    private void miPopup(object sender, System.EventArgs e)
    {
      bool b;

      // ------------------------------
      // 'Action' menu

      if (sender == this.miAction) 
      {
        // 'Copy selection'
        b = (this.lvFiles.SelectedItems.Count > 0);
        this.miAction_CopySel.Enabled = b;
        // 'Copy SD card'
        b=true;
        this.miAction_SDcardCopy.Enabled = b;
        // 'Remove/Empty folder'
        b = (this.lvFiles.Items.Count > 0);
        this.miAction_ClearFolder.Enabled = b;
        // 'Initialize SD card'
        b=true;
        this.miAction_SDcardInit.Enabled = b;
        // 'Mark All' 
        b = (this.lvFiles.Items.Count > 0);
        this.miAction_MarkAll.Enabled = b;
        // 'Toggle marking'
        b = (this.lvFiles.Items.Count > 0);
        this.miAction_ToggleMarking.Enabled = b;
      }    
    }
    
    /// <summary>
    /// 'Click' event of the menu
    /// </summary>
    private void miClick(object sender, System.EventArgs e)
    {
      // ------------------------------
      // 'Action' menu

      // 'Copy selection'
      if      (sender == this.miAction_CopySel)              
        OnCopySel ();     
        // 'Copy SD card'
      else if (sender == this.miAction_SDcardCopy)
        OnSDcardCopy ();     
        // 'Remove/Empty folder'
      else if (sender == this.miAction_ClearFolder)
        OnClearFolder ();
        // 'Initialize SD card'
      else if (sender == this.miAction_SDcardInit)
        OnSDcardInit ();
        // 'Mark All' 
      else if (sender == this.miAction_MarkAll)
        OnMarkAll ();
        // 'Toggle marking'
      else if (sender == this.miAction_ToggleMarking)
        OnToggleMarking ();
    }
   
    /// <summary>
    /// 'Select' event of the menu
    /// </summary>
    private void miSelect(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      // ------------------------------
      // 'Action' menu

      // 'Copy selection'
      if      (sender == this.miAction_CopySel) 
        this._StatusBar.Text = r.GetString ( "SDcardReaderNew_miAction_CopySel_Select" );       // "Copies the files of the selected folder from device to PC"
        // 'Copy SD card'
      else if (sender == this.miAction_SDcardCopy)
        this._StatusBar.Text = r.GetString ( "SDcardReaderNew_miAction_SDcardCopy_Select" );    // "Copies all files residing on the SD card from device to PC"
        // 'Remove/Empty folder'
      else if (sender == this.miAction_ClearFolder)
        this._StatusBar.Text = r.GetString ( "SDcardReaderNew_miAction_ClearFolder_Select" );   // "Removes the selected subfolder from the SD card, resp. empties the selected root folder"
        // 'Initialize SD card'
      else if (sender == this.miAction_SDcardInit)
        this._StatusBar.Text = r.GetString ( "SDcardReaderNew_miAction_SDcardInit_Select" );    // "Reinitializes the SD card"
        // 'Mark All' 
      else if (sender == this.miAction_MarkAll)
        this._StatusBar.Text = r.GetString ( "SDcardReaderNew_miAction_MarkAll_Select" );       // "Selects all files in the window"
        // 'Toggle marking'
      else if (sender == this.miAction_ToggleMarking)
        this._StatusBar.Text = r.GetString ( "SDcardReaderNew_miAction_ToggleMarking_Select" ); // "Toogles the selection"
        // Default
      else
        this._StatusBar.Text = "";
    }

    /// <summary>
    /// 'AfterSelect' event of the folders TreeView control
    /// </summary>
    private void tvFolders_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
    {
      CommMessage msg = null;

      App app = App.Instance;
      AppComm comm = app.Comm;

      // The selected node
      TreeNode tn = e.Node;
      // Check: End node?
      if (tn.Nodes.Count == 0)
      {
        // Yes:
        // Parent node of the selected node (for non-root nodes only), in order to get the root node
        // of the selected node
        TreeNode tn1 = (tn.Parent == null) ? tn : tn.Parent;
        // Choose a 'Read files' message corr'ing to the root node (= root folder) type
        if      (tn1.Text == _sDataFolderPath)     msg = new CommMessage (comm.msgReadDataFiles);
        else if (tn1.Text == _sAlarmFolderPath)    msg = new CommMessage (comm.msgReadAlarmFiles);
        else if (tn1.Text == _sScanFolderPath)     msg = new CommMessage (comm.msgReadScanFiles);
        else if (tn1.Text == _sScriptFolderPath)   msg = new CommMessage (comm.msgReadScriptFiles); 
        else if (tn1.Text == _sServiceFolderPath)  msg = new CommMessage (comm.msgReadServiceFiles);
        else if (tn1.Text == _sErrorFolderPath)    msg = new CommMessage (comm.msgReadErrorFiles); 
        if (null != msg)
        {
          msg.Parameter = (tn.Parent == null) ? "" : tn.Text;
          msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
          comm.WriteMessage (msg);
          // Clear the Statusbar text
          // Notes:
          //  Suppose, that the files LV contains selected files, and the Statusbar thus displays
          //  the # of selected files. If one activates a folder node in the folders TV, the files LV 
          //  is refilled again in unselected state. Without the following line a non-synchronisation
          //  between files LV selection state and Statusbar contents would appear.
          this._StatusBar.Text = "";
        }
      }
    }

    /// <summary>
    /// 'Click' event of the files ListView control 
    /// </summary>
    private void lvFiles_Click(object sender, System.EventArgs e)
    {
      // Display the number of selected files in the Statusbar
      DisplayNrOfFilesSelected ();
    }

    /// <summary>
    /// 'SelectedIndexChanged' event of the files ListView control 
    /// </summary>
    /// <remarks>
    /// The 'Click' event of the LV control is unfortunately NOT triggered, if one clicks on a client area 
    /// part, which is not occupied by a filename. But on the other hand all the possibly selected control items 
    /// are unselected after such click. In order to synchronize in such case the displayed number of selected files 
    /// in the Statusbar, we have to use this event.   
    /// </remarks>
    private void lvFiles_SelectedIndexChanged(object sender, System.EventArgs e)
    {
      if (this.lvFiles.SelectedIndices.Count == 0)
      {
        // Display the number of selected files in the Statusbar
        DisplayNrOfFilesSelected ();
      }
    }
    
    /// <summary>
    /// 'CheckedChanged' event of the 'File preview' CheckBox control 
    /// </summary>
    private void chkFilePreview_CheckedChanged(object sender, System.EventArgs e)
    {
      if (!this.chkFilePreview.Checked)
      {
        // Reset transfer related controls: 'File contents'
        this.lblFileContents.Text = _sInitiallblFileContentsTxt;
        this.txtFileContents.Text = "";
      }
    }
    
    /// <summary>
    /// 'Click' event of the 'Storage folder' Button control
    /// </summary>
    private void btnStorageFolder_Click(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // FolderBrowser dialog
      FolderBrowserDialog dlg = new FolderBrowserDialog ();
      
      dlg.Description  = r.GetString ("SDcardReaderNew_Txt_SelectStorageFolder");   // "Bitte w�hlen Sie den Ordner zur Speicherung der SD-Kartendaten"
      dlg.ShowNewFolderButton = true;
      dlg.SelectedPath = this.txtStorageFolder.Text;

      if (DialogResult.OK == dlg.ShowDialog ())
      {
        // Overtake the storage folder
        this.txtStorageFolder.Text = dlg.SelectedPath;
      }
      dlg.Dispose ();
    }
  
    /// <summary>
    /// 'HelpRequested' event of some controls
    /// </summary>
    /// <remarks>
    /// </remarks>
    private void _ctl_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Help requesting control
      Control requestingControl = (Control)sender;
      
      // Assign help text
      string s = "";
     
      // ---------------------------
      // section 'Files on device'
    
      // Folders TV
      if (requestingControl == this.tvFolders)
        s = r.GetString ("SDcardReaderNew_Help_tvFolders");         // "Zeigt die Ordnerstruktur auf der SD-Karte des Ger�tes an."
        // Files LV
      else if (requestingControl == this.lvFiles)
        s = r.GetString ("SDcardReaderNew_Help_lvFiles");           // "Zeigt die Dateien im gew�hlten Ger�teordner an."

        // ---------------------------
        // section 'Storage folder'

        // Btn. Storage folder
      else if (requestingControl == this.btnStorageFolder)
        s = r.GetString ("SDcardReaderNew_Help_btnStorageFolder");  // "W�hlt den Ordner auf dem PC aus, in dem die �bertragenen Dateien gespeichert werden."
        
        // ---------------------------
        // section 'File transfer from device to PC'

        // ChB File preview
      else if (requestingControl == this.chkFilePreview)
        s = r.GetString ("SDcardReaderNew_Help_chkFilePreview");    // "Wenn markiert, wird der Inhalt der �bertragenen Datei angezeigt."
        // TB File contents
      else if (requestingControl == this.txtFileContents)
        s = r.GetString ("SDcardReaderNew_Help_txtFileContents");   // "Zeigt den Inhalt der �bertragenen Datei an."

        // ---------------------------
        //  ... El resto del mundo ...

      else
        s = r.GetString ("SDcardReaderNew_Help_Default");           // "(Kein Kommentar verf�gbar)"

      // Show help
      this._ToolTip.SetToolTip (requestingControl, s);
      hlpevent.Handled=true;
    }
    
    /// <summary>
    /// 'Tick' event of the timer 
    /// (Interval: 100 msec)
    /// </summary>
    private void _Timer_Tick(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;
 
      // CD: Which action is activated
      switch (_eAction)
      {
        case Action.SDcardDetFolderStructure:
          // Action: SDcard folder structure determination

          // Check: 0,5 sec after activation elapsed?
          _nTimer++;
          if (_nTimer == 5)
          {
            // Yes:
            _eAction = Action.None;
            // Fill the folders TreeView control
            FillTreeView ();  
          }
          break;

        case Action.SDcardDetFolderStructure_OnEnd:
          // Action: SDcard folder structure determination - On finish

          // Check: Communication idle?
          if (comm.IsIdle ())
          {
            // Yes:
            _eAction = Action.None;
            // Announce, that the folders TreeView control filling has finished
            this._StatusBar.Text = r.GetString ( "SDcardReaderNew_Load_EndTVFill" );
          }
          break;
      
        case Action.WaitUntilCommIdle:
          // Action: Wait until communication is idle, afterwards wait a little (here: 0,5 sec) so that 
          //         all comm. processes (RX evaluation) could finish

          // Check: Communication idle?
          if ( comm.IsIdle() ) 
          {
            // Yes: Start waiting a little 
            _nTimer++;
            if (_nTimer == 5)
            {
              // Finish waiting
              _eAction = Action.None;
            }
          }
          break;
        
        case Action.SDcardInitMonitoring:
          // Action: SDcard initialisation monitoring

          // Check: 1 sec after activation elapsed?
          _nTimer++;
          if (_nTimer == 10)
          {
            // Yes:
            _nTimer = 0;
            // Update progress bar (in a circular manner)
            this.pgbTransfer.Value++;
            if (this.pgbTransfer.Value == this.pgbTransfer.Maximum)
            {
              this.pgbTransfer.Value = this.pgbTransfer.Minimum;
            }
          }
          break;
      }
    }

    #endregion event handling

    #region constants & enums
    
    /// <summary>
    /// Timer-based actions enumeration
    /// </summary>
    enum Action 
    {
      None,
      SDcardDetFolderStructure,         // SDcard folder structure determination
      SDcardDetFolderStructure_OnEnd,   // SDcard folder structure determination - On finish
      WaitUntilCommIdle,                // Wait until communication is idle
      SDcardInitMonitoring              // SDcard initialisation monitoring
    };
    
    #endregion constants & enums

    #region members

    /// <summary>
    /// True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    bool _bShowWait = false;

    /// <summary>
    /// Indicates, whether the stored data Transfer process is curr'ly in progress ( true ) or not ( false )
    /// </summary>
    bool _bTransferInProgress = false;
    
    /// <summary>
    /// Name of the script, that was lastly running on the PID device
    /// </summary>
    string _sLastScriptName = string.Empty;


    /// <summary>
    /// Path of the device Data root folder
    /// </summary>
    string _sDataFolderPath     = "";             
    
    /// <summary>
    /// Path of the device Alarm root folder
    /// </summary>
    string _sAlarmFolderPath    = "";             
    
    /// <summary>
    /// Path of the device Scan root folder
    /// </summary>
    string _sScanFolderPath     = "";             
     
    /// <summary>
    /// Path of the device Script root folder
    /// </summary>
    string _sScriptFolderPath   = "";             
    
    /// <summary>
    /// Path of the device Service root folder
    /// </summary>
    string _sServiceFolderPath  = "";             
    
    /// <summary>
    /// Path of the device Error root folder
    /// </summary>
    string _sErrorFolderPath  = "";             
   

    /// <summary>
    /// Indicates, whether a transferred file should be stored (True) or not (False)
    /// </summary>
    bool _bWriteFile = false;  

    // Initial Label strings
    string _sInitiallblFileContentsTxt  = "File contents:";
    
    /// <summary>
    /// The Transfer list, which contains the paths of the files to be transferred (read)
    /// </summary>
    ArrayList _alTrans = new ArrayList ();


    /// <summary>
    /// Timer-based action
    /// </summary>
    Action _eAction = Action.None;
    /// <summary>
    /// Timer counter
    /// </summary>
    int _nTimer = 0;

    #endregion members

    #region methods

    /// <summary>
    /// Performs initialisation tasks.
    /// </summary>
    private void Init ()
    {
      // Reset controls and members
      ResetAll ();
    }
    
    /// <summary>
    /// Resets controls and members.
    /// </summary>
    void ResetAll ()
    {
      // Section 'Files on device'
      this.tvFolders.Nodes.Clear ();
      this.lvFiles.Items.Clear ();
  
      // Section 'File transfer from device to PC'
      //    File contents
      this.lblFileContents.Text = _sInitiallblFileContentsTxt;
      this.txtFileContents.Text = "";
      //    Transfer cues
      this.pgbTransfer.Value = this.pgbTransfer.Minimum;
      this.lblTransferRate.Text = ""; 
 
      // Members: Paths of the device root folders
      _sDataFolderPath = "";
      _sAlarmFolderPath = "";
      _sScanFolderPath = "";
      _sScriptFolderPath = "";
      _sServiceFolderPath = "";
      _sErrorFolderPath = "";
    }
    
    /// <summary>
    /// Fills the folder TreeView control.
    /// </summary>
    void FillTreeView ()
    {
      CommMessage msgFolder=null, msgSubFolders=null;

      App app = App.Instance;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // Announce, that the TV control filling begins
      this._StatusBar.Text = r.GetString ( "SDcardReaderNew_Load_BeginTVFill" );
      // Clear the tree view object
      this.tvFolders.Nodes.Clear ();
      // Loop over all file types (i.e. DAT, AL, SCN, SCR, SVC, ERR)
      for (int i=0; i < 6; i++)
      {
        switch (i)
        {
          case 0:     // Data
            msgFolder = new CommMessage (comm.msgReadDataFolder);       // msg 'Read root folder' for Data files
            msgSubFolders = new CommMessage (comm.msgReadDataFolders);  // msg 'Read subfolders' for Data files
            break;
          case 1:     // Alarm
            msgFolder = new CommMessage (comm.msgReadAlarmFolder);
            msgSubFolders = new CommMessage (comm.msgReadAlarmFolders);
            break;
          case 2:     // Scan
            msgFolder = new CommMessage (comm.msgReadScanFolder);
            msgSubFolders = new CommMessage (comm.msgReadScanFolders);
            break;
          case 3:     // Script
            msgFolder = new CommMessage (comm.msgReadScriptFolder);
            msgSubFolders = new CommMessage (comm.msgReadScriptFolders);
            break;
          case 4:     // Service
            msgFolder = new CommMessage (comm.msgReadServiceFolder);
            msgSubFolders = null;
            break;
          case 5:     // Error
            msgFolder = new CommMessage (comm.msgReadErrorFolder);
            msgSubFolders = null;
            break;
        }
        // Read root folder path
        msgFolder.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msgFolder);
        // Read (sub)folders, if any
        if (null != msgSubFolders)
        {
          msgSubFolders.WindowInfo = new WndInfo (this.Handle, _bShowWait);
          comm.WriteMessage (msgSubFolders);
        }
      }
      // Start timer-based action: SDcard folder structure determination - On finish
      _eAction = Action.SDcardDetFolderStructure_OnEnd;
    }

    /// <summary>
    /// Copies the selected files of the selected folder from device to PC.
    /// </summary>
    void OnCopySel ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      // Clear the transfer list
      this._alTrans.Clear ();
      // Get the selected node (= selected folder)
      TreeNode tn = this.tvFolders.SelectedNode;
      // Fill the transfer list with the paths of the selected files
      foreach (ListViewItem lvi in this.lvFiles.SelectedItems)
      {
        string s = Path.Combine (tn.FullPath, lvi.Text);
        this._alTrans.Add (s);
      }
      // Info, if transfer list is empty
      if (this._alTrans.Count == 0)
      {
        string sMsg = r.GetString ( "SDcardReaderNew_Info_TransferListEmpty" );     // "Keine kopierbaren Dateien gefunden"
        string sCap = r.GetString ( "Form_Common_TextInfo" );                       // "Information"
        MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
        return;
      }
      // Transfer device -> PC
      _OnCopy ();
    }

    /// <summary>
    /// Copies all files residing on the SD card from device to PC.
    /// </summary>
    void OnSDcardCopy ()
    {
      CommMessage msg, msgFiles = null;
      string sFile, sFilePath;
      bool bSubFoldersUsed;
      int nFolder;
      TreeNode tnLastWithFiles = null;

      App app = App.Instance;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // Clear the transfer list
      this._alTrans.Clear ();
      // Loop over all root folder nodes
      for (int k=0; k < this.tvFolders.Nodes.Count; k++)
      {
        // The current root folder
        TreeNode tn = this.tvFolders.Nodes[k];
        // The corr'ing 'Read files' comm. message and the indication, whether (sub)folders are used
        // for data storage or not 
        if      (tn.Text == _sDataFolderPath)   { msgFiles = comm.msgReadDataFiles; bSubFoldersUsed = true; }    
        else if (tn.Text == _sAlarmFolderPath)  { msgFiles = comm.msgReadAlarmFiles; bSubFoldersUsed = true; }   
        else if (tn.Text == _sScanFolderPath)   { msgFiles = comm.msgReadScanFiles; bSubFoldersUsed = true; }  
        else if (tn.Text == _sScriptFolderPath) { msgFiles = comm.msgReadScriptFiles; bSubFoldersUsed = true; }
        else if (tn.Text == _sServiceFolderPath){ msgFiles = comm.msgReadServiceFiles; bSubFoldersUsed = false; }    
        else if (tn.Text == _sErrorFolderPath)  { msgFiles = comm.msgReadErrorFiles; bSubFoldersUsed = false; }   
        else                                    { continue; }
        // # of (sub)folders
        nFolder = bSubFoldersUsed ? tn.Nodes.Count : 1;
        // Loop over the (sub)folders in the corr'ing device root folder 
        for (int i=0; i < nFolder; i++)
        {
          // The current (sub)folder
          TreeNode tn1 = bSubFoldersUsed ? tn.Nodes[i] : tn;
          // TX: Read files: "RD<xxx>FI " + sSubFolder + "\r" (<xxx> = DAT, AL, SCN, SCR, SVC, ERR)
          msg = new CommMessage (msgFiles);
          msg.Parameter = bSubFoldersUsed ? tn1.Text : "";
          msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
          comm.WriteMessage (msg);
          // Wait, until the communication is in idle state 
          // --> after completion, the corr'ing files LV has been filled
          _WaitUntilCommIdle ();
          // Remember this (sub)folder in case that it contains files
          if (this.lvFiles.Items.Count > 0)
            tnLastWithFiles = tn1;
          // Fill the transfer list:
          // Loop over the files residing in the (sub)folder 
          for (int j=0; j < this.lvFiles.Items.Count; j++)
          {
            // The current file
            sFile = lvFiles.Items[j].Text;
            // The absolute file path
            sFilePath = Path.Combine (tn1.FullPath, sFile);
            // Add it to the transfer list
            this._alTrans.Add (sFilePath);
          }//E - for (j)
        }//E - for (i)
      }//E - for (k)
      // Info, if transfer list is empty
      if (this._alTrans.Count == 0)
      {
        string sMsg = r.GetString ( "SDcardReaderNew_Info_TransferListEmpty" );     // "Keine kopierbaren Dateien gefunden"
        string sCap = r.GetString ( "Form_Common_TextInfo" );                       // "Information"
        MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
        return;
      }
      // Synchronize folders TV and files LV
      if (null != tnLastWithFiles)
        this.tvFolders.SelectedNode = tnLastWithFiles;
      // Transfer device -> PC
      _OnCopy ();
    }

    /// <summary>
    /// Initiates the Copy process device -> PC for the files, that are contained in the transfer list.
    /// </summary>
    void _OnCopy ()
    {
      // Check: Are there any files to be transferred?
      if (this._alTrans.Count > 0)
      {
        // Yes:
        // Indicate, that the files should be stored after transmission
        _bWriteFile = true;
        // Initiate Transfer progress bar
        this.pgbTransfer.Minimum  = 0;
        this.pgbTransfer.Maximum  = this._alTrans.Count;
        this.pgbTransfer.Value    = this.pgbTransfer.Minimum;
        // Initiate 'Transfer rate' Label
        this.lblTransferRate.Text = string.Format ("{0}/{1}", this.pgbTransfer.Value, this.pgbTransfer.Maximum); 
        
        // Indicate that the Transfer process has begun.
        _bTransferInProgress = true;
        // TX: Script Current
        //    Parameter: none
        //    Device: Action - nothing; 
        //            Returns - Name of the script currently running on the IMS device
        App app = App.Instance;
        AppComm comm = app.Comm;
        CommMessage msg = new CommMessage ( comm.msgScriptCurrent );
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
        // TX: Transfer Start
        //    Parameter: The transfer task to be performed
        //    Device: Action - Indicate, that the SD card data transfer to PC has begun; 
        //            Returns - OK
        msg = new CommMessage ( comm.msgTransferStart );
        msg.Parameter = TransferTask.SDcard_Data.ToString ( "D" );
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage ( msg );

        // Initiate the transfer 
        // TX: Read Text file contents: "RDTFI " + sFilePath + "\r"
        msg = new CommMessage (comm.msgReadTextFileContents);
        msg.Parameter = this._alTrans[0].ToString ();
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
    }

    /// <summary>
    /// Removes the selected subfolder from the SD card, resp. empties the selected root folder. 
    /// </summary>
    void OnClearFolder ()
    {
      bool bSubFoldersUsed;
      string sMsg;
      CommMessage msg;

      App app = App.Instance;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // The selected node (= selected folder)
      TreeNode tn = this.tvFolders.SelectedNode;
      
      // Indication: Subfolder usage yes / no?
      bSubFoldersUsed = ( tn.FullPath.StartsWith (_sDataFolderPath) ||
        tn.FullPath.StartsWith (_sAlarmFolderPath)||
        tn.FullPath.StartsWith (_sScanFolderPath) ||
        tn.FullPath.StartsWith (_sScriptFolderPath) ) ? true : false;
      
      // Safety request
      //    Preparation (Message corr'ing to subfolder usage)
      if (bSubFoldersUsed)
        sMsg = r.GetString ("SDcardReaderNew_Que_ClearSubFolder");    // "Should the selected subfolder really be cleared?"
      else
        sMsg = r.GetString ("SDcardReaderNew_Que_EmptyRootFolder");   // "Should the selected root folder really be emptied?"
      //    Request
      string sCap = r.GetString ("Form_Common_TextSafReq");           // "Safety request"
      DialogResult dr = MessageBox.Show (this, sMsg, sCap, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
      if ( dr == DialogResult.No )
        return; // No.
      
#if SDCARDREADER_CLEAR_WITH_INTERRUPTION      
      // Indicate that the Transfer process has begun.
      _bTransferInProgress = true;

      // TX: Script Current
      //    Parameter: none
      //    Device: Action - nothing; 
      //            Returns - Name of the script currently running on the IMS device
      msg = new CommMessage (comm.msgScriptCurrent);
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      // TX: Transfer Start
      //    Parameter: The transfer task to be performed
      //    Device: Action - Indicate, that the SDcard data clear (transfer) has begun; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgTransferStart );
      msg.Parameter = TransferTask.SDcard_Data_Clear.ToString ( "D" );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );
#endif
      
      // Initiate progress bar
      this.pgbTransfer.Minimum  = 0;
      this.pgbTransfer.Maximum  = 1;
      this.pgbTransfer.Value    = this.pgbTransfer.Minimum;
      // Disable the form controls
      UpdateData (false);
      // CD: Subfolder usage yes / no?
      if (bSubFoldersUsed)
      {
        // Yes (Data/Alarm/Scan/Script files):

        // Clear the selected subfolder on device:
        // TX: Remove device folder
        //    Parameter: the device folder path
        //    Device: Action - Removes the folder
        //            Returns - OK
        msg = new CommMessage ( comm.msgRemoveFolder );
        msg.Parameter = tn.FullPath;
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
      else
      {
        // No (Service/Error files):

        // Empty the root folder on device:
        // TX: Empty the Service/Error folder
        //    Parameter: none
        //    Device: Action - Empties the Service/Error folder
        //            Returns - OK
        if      ( tn.FullPath.StartsWith (_sServiceFolderPath) )  msg = new CommMessage ( comm.msgEmptyServiceFolder );
        else if ( tn.FullPath.StartsWith (_sErrorFolderPath) )    msg = new CommMessage ( comm.msgEmptyErrorFolder );
        msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
        comm.WriteMessage (msg);
      }
    }

    /// <summary>
    /// Reinitializes the SD card.
    /// </summary>
    void OnSDcardInit ()
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;
      
      // Safety request: Should the disk really be reinitialized?
      string sMsg = r.GetString ("SDcardReaderNew_Que_InitDisk");   // "Should the SD card really be reinitialized?"
      string sCap = r.GetString ("Form_Common_TextSafReq");         // "Safety request"
      DialogResult dr = MessageBox.Show (this, sMsg, sCap, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
      if ( dr == DialogResult.No )
        return; // No.

      // Yes:
      // Indicate that the Transfer process has begun.
      _bTransferInProgress = true;

      // TX: Script Current
      //    Parameter: none
      //    Device: Action - nothing; 
      //            Returns - Name of the script currently running on the IMS device
      CommMessage msg = new CommMessage ( comm.msgScriptCurrent );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
      // TX: Transfer Start
      //    Parameter: The transfer task to be performed
      //    Device: Action - Indicate, that the SD card reinitialize (transfer) has begun; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgTransferStart );
      msg.Parameter = TransferTask.SDcard_InitDisk.ToString ( "D" );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );

      // TX: (Re)initialize disk - Start
      //    Parameter: none
      //    Device: Action - (Re)initializes the disk - Start
      //            Returns - OK
      msg = new CommMessage ( comm.msgInitDisk_Start );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);
    }

    /// <summary>
    /// Marks all files in the files LV control.
    /// </summary>
    void OnMarkAll ()
    {
      // Mark 
      this.lvFiles.Focus ();
      foreach (ListViewItem lvi in this.lvFiles.Items)
        lvi.Selected = true;
      // Display the number of selected files in the Statusbar
      DisplayNrOfFilesSelected ();
    }

    /// <summary>
    /// Toogles marking of all files in the files LV control.
    /// </summary>
    void OnToggleMarking ()
    {
      // Toogle marking
      this.lvFiles.Focus ();
      foreach (ListViewItem lvi in this.lvFiles.Items)
        lvi.Selected = !lvi.Selected;
      // Display the number of selected files in the Statusbar
      DisplayNrOfFilesSelected ();
    }

    /// <summary>
    /// Displays the number of selected files in the Statusbar.
    /// </summary>
    void DisplayNrOfFilesSelected ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      string sFmt = r.GetString ( "SDcardReaderNew_Mark_SelItems" );    // "{0} Files selected"
      this._StatusBar.Text = string.Format (sFmt, this.lvFiles.SelectedItems.Count);
    }

    /// <summary>
    /// Updates the form.
    /// </summary>
    /// <param name="bEnable">Adjusting parameter: True - Controls are enabled programmatically; 
    /// False - Controls are forced to disabled state</param>
    void UpdateData (bool bEnable) 
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Doc doc = app.Doc;

      // Primary enable, masked by the adjusting parameter
      bool bEn = ( doc.sVersion.Length > 0 ) && ( _bTransferInProgress == false ) && comm.IsOpen ();
      bEn &= bEnable;

      // Secondary enable: 
      // As long as the device folder structure is not completely read, the UI should be in disabled state
      bEn &= (_sDataFolderPath.Length > 0 && _sAlarmFolderPath.Length > 0 && 
        _sScanFolderPath.Length > 0 && _sScriptFolderPath.Length > 0 && 
        _sServiceFolderPath.Length > 0 && _sErrorFolderPath.Length > 0);

      // Menu
      this.miAction.Enabled = bEn;

      // Section 'Files on device'
      this.gpDevice.Enabled = bEn;

      // Section 'Storage folder'
      this.gpStorageFolder.Enabled = bEn;
      
      // Section 'File transfer from device to PC'
      bool bEnT = bEn && (this.txtStorageFolder.Text.Length > 0);
      this.gpTransfer.Enabled = bEnT;
    }

    /// <summary>
    /// Adds subfolder nodes to the root folder nodes of the folders TV control.
    /// </summary>
    /// <param name="sRootFolder">The root folder</param>
    /// <param name="sSubFolders">The subfolder names, separated by commas</param>
    void UpdateFolders (string sRootFolder, string sSubFolders)
    {
      int idx;

      // Find the root folder node
      for (idx=0; idx < this.tvFolders.Nodes.Count; idx++)
        if (this.tvFolders.Nodes[idx].Text == sRootFolder) break;
      if (idx < this.tvFolders.Nodes.Count)
      {
        // Node was found:
        // Add subfolder nodes to it
        string[] ars=sSubFolders.Split (',');
        foreach (string s in ars)
        {
          if (s.Length > 0)
          {
            TreeNode tn = new TreeNode (s); 
            this.tvFolders.Nodes[idx].Nodes.Add (tn);
          }
        }
      }
    }

    /// <summary>
    /// Adds file names to the files LV control.
    /// </summary>
    /// <param name="sFiles">The file names, separated by commas</param>
    void UpdateFiles (string sFiles)
    {
      // Clear the files LV
      this.lvFiles.Items.Clear ();
      // Add file names to it
      string[] ars=sFiles.Split (',');
      foreach (string s in ars)
      {
        if (s.Length > 0) 
        {
          ListViewItem lvi = new ListViewItem ();
          lvi.Text = s;
          this.lvFiles.Items.Add (lvi);
        }
      }
    }

    /// <summary>
    /// Stores file contents to file.
    /// </summary>
    /// <param name="sDeviceFilePath">The device file path</param>
    /// <param name="sFileContents">The file contents string</param>
    void WriteFile (string sDeviceFilePath, string sFileContents)
    {
      // The StreamWriter object, used for file writing
      StreamWriter file = null;
      // Build the full file path for storage on HD: = Concatenation Storage folder / Device file path
      string sFilePath = sDeviceFilePath;
      if (sFilePath.StartsWith (@"\"))  sFilePath = sFilePath.Substring (1);  // Skip leading '\' due to 'Combine' requirements
      sFilePath = Path.Combine (this.txtStorageFolder.Text, sFilePath);
      // If req., create the directory the file should be stored within
      string sDir = Path.GetDirectoryName(sFilePath);
      if (!Directory.Exists (sDir))
        Directory.CreateDirectory (sDir);
      // Store
      try 
      {
        // Wait cursor
        Cursor.Current = Cursors.WaitCursor;
        // Creates the file for writing UTF-8 encoded text
        file = File.CreateText (sFilePath);
        // Write the file contents to the specified file
        file.Write (sFileContents);
      }
      catch (System.Exception exc)
      {
        // Error:
        App app = App.Instance;
        Ressources r = app.Ressources;

        // Show corr'ing message
        string sFmt = r.GetString ("SDcardReaderNew_Err_FileSave");       // "Fehler beim Speichern in Datei\r\n'{0}'\r\n(Detail:\r\n{1})";
        string sMsg = string.Format (sFmt, sFilePath, exc.Message);
        string sCap = r.GetString ("Form_Common_TextError");              // "Fehler";
        MessageBox.Show (this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally 
      {
        // Close the file
        if ( null != file ) 
          file.Close ();
        // Normal cursor
        Cursor.Current = Cursors.Default;
      }
    }

    /// <summary>
    /// Finishes the transfer
    /// </summary>
    void _EndTransfer ()
    {
      App app = App.Instance;
      AppComm comm = app.Comm;
      Doc doc = app.Doc;

      // TX: Script Select
      // (Device: Action - Selects the script to be executed (here: script last used); TX - OK)
      CommMessage msg = new CommMessage ( comm.msgScriptSelect );
      msg.Parameter = doc.ScriptNameToIdx ( _sLastScriptName ).ToString ();
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage ( msg );
      // TX: Transfer Complete
      //    Parameter: none
      //    Device: Action - Indicate, that the transfer to PC has completed; 
      //            Returns - OK
      msg = new CommMessage ( comm.msgTransferComplete );
      msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
      comm.WriteMessage (msg);

      // Indicate that the Transfer process has finished.
      _bTransferInProgress = false;

      // Reset: Transfer cues
      //    Value (position) of the Progressbar control
      this.pgbTransfer.Value = this.pgbTransfer.Minimum;
      //    'Transfer rate' Label
      this.lblTransferRate.Text = ""; 
    }

    /// <summary>
    /// Waits, until the communication is in idle state.
    /// </summary>
    void _WaitUntilCommIdle ()
    {
      // Start timer-based action: Wait until communication is idle
      _eAction = Action.WaitUntilCommIdle;
      _nTimer = 0;
      // Do events during waiting
      while (_eAction == Action.WaitUntilCommIdle)
        Application.DoEvents ();
    }
   
    /// <summary>
    /// Performs actions in reaction of the receipt of a comm. message  
    /// </summary>
    /// <param name="msgRX">The comm. message</param>
    /// <remarks>
    /// The TX/RX messages have the following formats:
    /// 
    /// --------------------------
    /// Data
    /// --------------------------
    /// 
    /// 2.1. Data folder path request
    ///   TX
    ///     syntax:        "RDDATFO\r"
    ///   RX
    ///     syntax:        "RDDATFO (folder_path)\r"
    ///     example:       "RDDATFO \Data\r" 
    ///     
    /// 2.2. Data (sub)folders request
    ///   TX
    ///     syntax:        "RDDATFOS\r"
    ///   RX
    ///     syntax:        "RDDATFOS (folder_name_1),...,(folder_name_N)\r"
    ///     example:       "RDDATFOS 000001\r" 
    ///     
    /// 2.3. Data files request
    ///   TX
    ///     syntax:        "RDDATFI (folder_name)\r"
    ///     example:       "RDDATFI 000001\r"
    ///   RX
    ///     syntax:        "RDDATFI (file_name_1),...,(file_name_N)\r"
    ///     example:       "RDDATFI Res_000101000031.txt\r"
    ///     
    /// Notes concerning clearing device Data storage:
    ///  The 'Empty Data folder' cmd is NOT used from within this PC SW:
    ///   TX
    ///     syntax:        "EYDATFO\r"
    ///   RX
    ///     syntax:        "EYDATFO OK\r"
    ///  The 'REMFO' cmd is used instead.    
    ///     
    /// --------------------------
    /// Alarm
    /// --------------------------
    ///     
    /// 2.4. Alarm folder path request
    ///   TX
    ///     syntax:        "RDALFO\r"
    ///   RX
    ///     syntax:        "RDALFO (folder_path)\r"
    ///     example:       "RDALFO \Alarms\r" 
    ///     
    /// 2.5. Alarm (sub)folders request
    ///   TX
    ///     syntax:        "RDALFOS\r"
    ///   RX
    ///     syntax:        "RDALFOS (folder_name_1),...,(folder_name_N)\r"
    ///     example:       "RDALFOS 000001\r" 
    ///     
    /// 2.6. Alarm files request
    ///   TX
    ///     syntax:        "RDALFI (folder_name)\r"
    ///     example:       "RDALFI 000001\r"
    ///   RX
    ///     syntax:        "RDALFI (file_name_1),...,(file_name_N)\r"
    ///     example:       "RDALFI Al_000101000031.txt\r"
    ///     
    /// Notes concerning clearing device Alarm storage:
    ///  The 'Empty Alarm folder' cmd is NOT used from within this PC SW:
    ///   TX
    ///     syntax:        "EYALFO\r"
    ///   RX
    ///     syntax:        "EYALFO OK\r"
    ///  The 'REMFO' cmd is used instead.    
    ///  
    /// --------------------------
    /// Scan
    /// --------------------------
    /// 
    /// 2.7. Scan folder path request
    ///   TX
    ///     syntax:        "RDSCNFO\r"
    ///   RX
    ///     syntax:        "RDSCNFO (folder_path)\r"
    ///     example:       "RDSCNFO \Scans\r" 
    ///     
    /// 2.8. Scan (sub)folders request
    ///   TX
    ///     syntax:        "RDSCNFOS\r"
    ///   RX
    ///     syntax:        "RDSCNFOS (folder_name_1),...,(folder_name_N)\r"
    ///     example:       "RDSCNFOS 000001\r" 
    ///     
    /// 2.9. Scan files request
    ///   TX
    ///     syntax:        "RDSCNFI (folder_name)\r"
    ///     example:       "RDSCNFI 000001\r"
    ///   RX
    ///     syntax:        "RDSCNFI (file_name_1),...,(file_name_N)\r"
    ///     example:       "RDSCNFI Scn_000101000051.txt\r"
    ///     
    /// Notes concerning clearing device Scan storage:
    ///  The 'Empty Scan folder' cmd is NOT used from within this PC SW:
    ///   TX
    ///     syntax:        "EYSCNFO\r"
    ///   RX
    ///     syntax:        "EYSCNFO OK\r"
    ///  The 'REMFO' cmd is used instead.    
    ///     
    /// --------------------------
    /// Script
    /// --------------------------
    ///     
    /// 2.10. Script folder path request
    ///   TX
    ///     syntax:        "RDSCRFO\r"
    ///   RX
    ///     syntax:        "RDSCRFO (folder_path)\r"
    ///     example:       "RDSCRFO \Scripts\r" 
    ///     
    /// 2.11. Script (sub)folders request
    ///   TX
    ///     syntax:        "RDSCRFOS\r"
    ///   RX
    ///     syntax:        "RDSCRFOS (folder_name_1),...,(folder_name_N)\r"
    ///     example:       "RDSCRFOS 000001\r" 
    ///     
    /// 2.12. Script files request
    ///   TX
    ///     syntax:        "RDSCRFI (folder_name)\r"
    ///     example:       "RDSCRFI 000001\r"
    ///   RX
    ///     syntax:        "RDSCRFI (file_name_1),...,(file_name_N)\r"
    ///     example:       "RDSCRFI Scr_000101000056.txt\r"
    ///     
    /// Notes concerning clearing device Script storage:
    ///  The 'Empty Script folder' cmd is NOT used from within this PC SW:
    ///   TX
    ///     syntax:        "EYSCRFO\r"
    ///   RX
    ///     syntax:        "EYSCRFO OK\r"
    ///  The 'REMFO' cmd is used instead.    
    ///     
    /// --------------------------
    /// Service
    /// --------------------------
    ///     
    /// 2.13. Service folder path request
    ///   TX
    ///     syntax:        "RDSVCFO\r"
    ///   RX
    ///     syntax:        "RDSVCFO (folder_path)\r"
    ///     example:       "RDSVCFO \Service\r" 
    ///     
    /// 2.14. Service files request
    ///   TX
    ///     syntax:        "RDSVCFI\r"
    ///   RX
    ///     syntax:        "RDSVCFI (file_name_1),...,(file_name_N)\r"
    ///     example:       "RDSVCFI Svc_000101000056.txt\r"
    ///   
    /// 2.15. Empty Service folder
    ///   TX
    ///     syntax:        "EYSVCFO\r"
    ///   RX
    ///     syntax:        "EYSVCFO OK\r"
    ///   
    /// --------------------------
    /// Error
    /// --------------------------
    ///     
    /// 2.16. Error folder path request
    ///   TX
    ///     syntax:        "RDERRFO\r"
    ///   RX
    ///     syntax:        "RDERRFO (folder_path)\r"
    ///     example:       "RDERRFO \Error\r" 
    ///     
    /// 2.17. Error files request
    ///   TX
    ///     syntax:        "RDERRFI\r"
    ///   RX
    ///     syntax:        "RDERRFI (file_name_1),...,(file_name_N)\r"
    ///     example:       "RDERRFI E0000001.txt\r"
    ///     
    /// 2.18. Empty Error folder
    ///   TX
    ///     syntax:        "EYERRFO\r"
    ///   RX
    ///     syntax:        "EYERRFO OK\r"
    ///     
    /// --------------------------
    /// Others
    /// --------------------------
    ///     
    /// 2.19. File contents request (Text)
    ///   TX
    ///     syntax:        "RDTFI (file_path)\r"
    ///     examples:      "RDTFI \Data\000001\Res_000101000031.txt\r",
    ///                    "RDTFI \Scripts\000001\Scr_000101000056.txt\r",
    ///                    "RDTFI \Scans\000001\Scn_000101000051.txt\r",
    ///   RX
    ///     syntax:        "RDTFI (file_contents)\r"
    ///     example:       "RDTFI (NL)Script: ...\r"             (for Data example)
    ///   Notes:
    ///     Don't forget: In the (file_contents) buffer all occurrences of the 'END_CHAR' char are
    ///     replaced by the 'REPL_CHAR' char, that means, that on receiving this buffer on PC
    ///     a re-replacement 'REPL_CHAR' -> 'END_CHAR' must take place!
    ///     
    /// 2.20. File contents request (Binary)
    ///   TX
    ///     syntax:        "RDBFI (file_path)\r"
    ///     example:       "RDBFI \System\RefScans\Scn.dat\r",
    ///   RX
    ///     syntax:        "RDBFI (file_size(4B))(file_contents)"
    ///     example:       "RDBFI ..."
    ///     
    /// 2.21. Remove device folder cmd
    ///   TX
    ///     syntax:        "REMFO (folder path)\r"
    ///     example:       "REMFO \Data\D000001\r",
    ///   RX
    ///     syntax:        "REMFO OK\r"
    ///     
    /// 2.22. Reinit. device storage - Start/Perform
    ///   TX
    ///     syntax:        "INDSK_S\r" / "INDSK_P\r" 
    ///   RX
    ///     syntax:        "INDSK_S OK\r" / "INDSK_P OK\r"
    ///     
    /// </remarks>
    public void AfterRXCompleted(CommMessage msgRX)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      AppComm comm = app.Comm;
      Ressources r = app.Ressources;

      // CD: Message response ID
      if ((msgRX.ResponseID == AppComm.CMID_TIMEOUT) || (msgRX.ResponseID == AppComm.CMID_STRLENERR))
      {
        // A comm. error (Timeout, ...) occurred:
        // Update the form
        UpdateData (true);
      }

      else if (msgRX.ResponseID == AppComm.CMID_ANSWER)
      {
        // The message response is present:
        string sCmd = msgRX.CommandResponse;
        byte[] arbyPar = msgRX.ParameterResponse;

        string sMsg, sCap;

        // Get the parameter string from the parameter Byte array
        string sPar = Encoding.ASCII.GetString (arbyPar);
        // CD according to the message command
        switch (sCmd) 
        {
        
            //----------------------------------------------------------
            // Common Transfer messages
            //----------------------------------------------------------

          case "SCRCURRENT":
            // ScriptCurrent
            // RX: Name of the script currently running on the device   
            try
            {
              // Get the name of the script, that was lastly running on the IMS device
              _sLastScriptName = sPar;
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptCurrent->function failed" );
            }
            break;

          case "SCRSELECT":
            // ScriptSelect
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgScriptSelect->function failed" );
            }
            break;
        
          case "TRNSTART":
            // Transfer Start
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgTransferStart->function failed" );
            }
            break;

          case "TRNCOMPLETE":
            // Transfer Complete
            // RX: OK
            try
            {
            }
            catch
            {
              Debug.WriteLine ( "OnMsgTransferComplete->function failed" );
            }
            break;

            //----------------------------------------------------------
            // SDcard storage messages
            //----------------------------------------------------------
          
            // --------------------------
            // Data
            // --------------------------

          case "RDDATFO":
            // Get the path of the device Data root folder
            // RX: The path of the device Data root folder
            try 
            {
              // Overgive the path of the device Data root folder
              _sDataFolderPath = sPar;
              // Add the Data root node to the folders TV
              TreeNode tn = new TreeNode (sPar); 
              this.tvFolders.Nodes.Add (tn);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadDataFolder->function failed" );
            }
            break;  

          case "RDDATFOS":
            // Get the (sub)folders of the device Data folder
            // RX: The (sub)folders of the device Data folder
            try 
            {
              
              // Add the names of the subfolders to the Data root node of the folders TV
              UpdateFolders (_sDataFolderPath, sPar);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadDataFolders->function failed" );
            }
            break;  
          
          case "RDDATFI":
            // Get the files of the current device Data subfolder
            // RX: The files of the current device Data subfolder
            try 
            {
              // Add the names of the files to the files LV
              UpdateFiles (sPar);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadDataFiles->function failed" );
            }
            break;  
            
            // --------------------------
            // Alarm
            // --------------------------

          case "RDALFO":
            // Get the path of the device Alarm root folder
            // RX: The path of the device Alarm root folder
            try 
            {
              // Overgive the path of the device Alarm root folder
              _sAlarmFolderPath = sPar;
              // Add the Alarm root node to the folders TV
              TreeNode tn = new TreeNode (sPar); 
              this.tvFolders.Nodes.Add (tn);
            }
            catch (System.Exception exc)
            {
              Debug.WriteLine ( "OnMsgReadAlarmFolder->function failed:" );
              Debug.WriteLine ( exc.Message );
            }
            break;  

          case "RDALFOS":
            // Get the (sub)folders of the device Alarm folder
            // RX: The (sub)folders of the device Alarm folder
            try 
            {
              // Add the names of the subfolders to the Alarm root node of the folders TV
              UpdateFolders (_sAlarmFolderPath, sPar);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadAlarmFolders->function failed" );
            }
            break;  
          
          case "RDALFI":
            // Get the files of the current device Alarm subfolder
            // RX: The files of the current device Alarm subfolder
            try 
            {
              // Add the names of the files to the files LV
              UpdateFiles (sPar);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadAlarmFiles->function failed" );
            }
            break;  
            
            // --------------------------
            // Scan
            // --------------------------
          
          case "RDSCNFO":
            // Get the path of the device Scan root folder
            // RX: The path of the device Scan root folder
            try 
            {
              // Overgive the path of the device Scan root folder
              _sScanFolderPath = sPar;
              // Add the Scan root node to the folders TV
              TreeNode tn = new TreeNode (sPar); 
              this.tvFolders.Nodes.Add (tn);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadScanFolder->function failed" );
            }
            break;  
          
          case "RDSCNFOS":
            // Get the (sub)folders of the device Scan folder
            // RX: The (sub)folders of the device Scan folder
            try 
            {
              // Add the names of the subfolders to the Scan root node of the folders TV
              UpdateFolders (_sScanFolderPath, sPar);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadScanFolders->function failed" );
            }
            break;         
        
          case "RDSCNFI":
            // Get the files of the device Scan folder
            // RX: The files of the device Scan folder
            try 
            {
              // Add the names of the files to the files LV
              UpdateFiles (sPar);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadScanFiles->function failed" );
            }
            break;     
            
            // --------------------------
            // Script
            // --------------------------

          case "RDSCRFO":
            // Get the path of the device Script root folder
            // RX: The path of the device Script root folder
            try 
            {
              // Overgive the path of the device Script root folder
              _sScriptFolderPath = sPar;
              // Add the Script root node to the folders TV
              TreeNode tn = new TreeNode (sPar); 
              this.tvFolders.Nodes.Add (tn);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadScriptFolder->function failed" );
            }
            break;  

          case "RDSCRFOS":
            // Get the (sub)folders of the device Script folder
            // RX: The (sub)folders of the device Script folder
            try 
            {
              // Add the names of the subfolders to the Script root node of the folders TV
              UpdateFolders (_sScriptFolderPath, sPar);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadScriptFolders->function failed" );
            }
            break;         
        
          case "RDSCRFI":
            // Get the files of the device Script folder
            // RX: The files of the device Script folder
            try 
            {
              // Add the names of the files to the files LV
              UpdateFiles (sPar);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadScriptFiles->function failed" );
            }
            break;  
            
            // --------------------------
            // Service
            // --------------------------
          
          case "RDSVCFO":
            // Get the path of the device Service root folder
            // RX: The path of the device Service root folder
            try 
            {
              // Overgive the path of the device Service root folder
              _sServiceFolderPath = sPar;
              // Add the Service root node to the folders TV
              TreeNode tn = new TreeNode (sPar); 
              this.tvFolders.Nodes.Add (tn);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadServiceFolder->function failed" );
            }
            break;  

          case "RDSVCFI":
            // Get the files of the device Service folder
            // RX: The files of the device Service folder
            try 
            {
              // Add the names of the files to the files LV
              UpdateFiles (sPar);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadServiceFiles->function failed" );
            }
            break;  

          case "EYSVCFO":            
            // Empty Service folder
            // RX: "111...OK" or "111...Error", where a "1" stands for each deleted item (file/(sub)folder) in the Service root folder
            try
            {
              // Update progress bar
              this.pgbTransfer.Value++;
              if (pgbTransfer.Value == this.pgbTransfer.Maximum)
              {

#if SDCARDREADER_CLEAR_WITH_INTERRUPTION      
                // Finish the transfer
                _EndTransfer ( );
#endif

                // Clear the files LV
                this.lvFiles.Items.Clear ();
         
                // Reset progress bar
                System.Threading.Thread.Sleep (1000);                         // in order to see the progress bar filling
                this.pgbTransfer.Value = this.pgbTransfer.Minimum;            // Reset
                
                // Display: Success message
                sMsg = r.GetString ( "SDcardReaderNew_Info_ServiceEmptied" ); // "Der Service-Ordner wurde geleert."
                sCap = r.GetString ( "Form_Common_TextInfo" );                // "Information"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
              }
            }
            catch
            {
              Debug.WriteLine ( "OnMsgEmptyServiceFolder->function failed" );
            }
            break;
            
            // --------------------------
            // Error
            // --------------------------

          case "RDERRFO":
            // Get the path of the device Error root folder
            // RX: The path of the device Error root folder
            try 
            {
              // Overgive the path of the device Error root folder
              _sErrorFolderPath = sPar;
              // Add the Error root node to the folders TV
              TreeNode tn = new TreeNode (sPar); 
              this.tvFolders.Nodes.Add (tn);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadErrorFolder->function failed" );
            }
            break;  

          case "RDERRFI":
            // Get the files of the device Error folder
            // RX: The files of the device Error folder
            try 
            {
              // Add the names of the files to the files LV
              UpdateFiles (sPar);
            }
            catch
            {
              Debug.WriteLine ( "OnMsgReadErrorFiles->function failed" );
            }
            break;  

          case "EYERRFO":            
            // Empty Error folder
            // RX: "111...OK" or "111...Error", where a "1" stands for each deleted item (file/(sub)folder) in the Error root folder
            try
            {
              // Update progress bar
              this.pgbTransfer.Value++;
              if (pgbTransfer.Value == this.pgbTransfer.Maximum)
              {

#if SDCARDREADER_CLEAR_WITH_INTERRUPTION      
                // Finish the transfer
                _EndTransfer ( );
#endif
                
                // Clear the files LV
                this.lvFiles.Items.Clear ();

                // Reset progress bar
                System.Threading.Thread.Sleep (1000);                         // in order to see the progress bar filling
                this.pgbTransfer.Value = this.pgbTransfer.Minimum;            // Reset
                
                // Display: Success message
                sMsg = r.GetString ( "SDcardReaderNew_Info_ErrorEmptied" );   // "Der Fehler-Ordner wurde geleert."
                sCap = r.GetString ( "Form_Common_TextInfo" );                // "Information"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
              }
            }
            catch
            {
              Debug.WriteLine ( "OnMsgEmptyErrorFolder->function failed" );
            }
            break;

            // --------------------------
            // Others
            // --------------------------

          case "RDTFI":
            // Get the file contents of a device text file
            // RX: The file contents string
            try 
            {
              // Update Transfer progress bar
              this.pgbTransfer.Value++;
              // Update 'Transfer rate' Label
              this.lblTransferRate.Text = string.Format ("{0}/{1}", this.pgbTransfer.Value, this.pgbTransfer.Maximum); 
              // Get the file path
              string sCurrCmd = msgRX.Command;
              string sFilePath = msgRX.Parameter;
          
#if DEBUG            
              // Testing only: Show the path of the file curr'ly received in Debug window
              string sCom = string.Format("{0}.: {1}, {2}", this.pgbTransfer.Value, sCurrCmd, sFilePath);
              Debug.WriteLine ( sCom );
#endif            
            
              // Show the file contents, if req.
              if (this.chkFilePreview.Checked)
              {
                // Update the 'File contents' Label
                string sFmt = r.GetString ("SDcardReaderNew_Txt_FileCont_1");     // "Contents of file '{0}':"
                this.lblFileContents.Text = string.Format (sFmt, sFilePath);
                // Update the 'File contents' TextBox
                this.txtFileContents.Text = sPar;
              }
              // Store the file contents to HD, if req.
              if (_bWriteFile)
              {
                WriteFile (sFilePath, sPar);
              }         

              // Remove the path of the file curr'ly received from the Transfer list
              this._alTrans.RemoveAt (0);
              // Check: Did we read all files?
              if (this._alTrans.Count > 0)
              {
                // No:
                // Continue the transfer 
                // TX: Read Text file contents: "RDTFI " + sFilePath + "\r"
                CommMessage msg = new CommMessage (comm.msgReadTextFileContents);
                msg.Parameter = this._alTrans[0].ToString ();
                msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
                comm.WriteMessage (msg);
              }
              else
              {
                // Yes:
                int nFilesRead = this.pgbTransfer.Value;
              
                // Finish the transfer
                _EndTransfer ( );
                            
                // Display: Files succ'ly read
                string sFmt = r.GetString ( "SDcardReaderNew_Info_FilesRead" );   // "{0} files were successfully read."
                sMsg = string.Format (sFmt, nFilesRead);
                sCap = r.GetString ( "Form_Common_TextInfo" );                    // "Information"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
              }
            }
            catch (System.Exception exc)
            {
              Debug.WriteLine ( "OnMsgReadTextFileContents->function failed: {0}", exc.Message );
            }
            break;  

          case "REMFO":            
            // Remove a device folder
            // RX: "111...OK" or "111...Error", where a "1" stands for each deleted file/(sub)folder in the root folder
            try
            {
              // Update progress bar
              this.pgbTransfer.Value++;
              if (pgbTransfer.Value == this.pgbTransfer.Maximum)
              {
              
#if SDCARDREADER_CLEAR_WITH_INTERRUPTION      
                // Finish the transfer
                _EndTransfer ( );
#endif
                // Update affected controls
                this.tvFolders.SelectedNode.Remove ();
                this.lvFiles.Items.Clear ();
                
                // Reset progress bar
                System.Threading.Thread.Sleep (1000);                     // in order to see the progress bar filling
                this.pgbTransfer.Value = this.pgbTransfer.Minimum;        // Reset
                              
                // Display: Success message
                sMsg = r.GetString ( "SDcardReaderNew_Info_Cleared" );    // "Der Unterordner wurde gel�scht."
                sCap = r.GetString ( "Form_Common_TextInfo" );            // "Information"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
              }
            }
            catch
            {
              Debug.WriteLine ( "OnMsgRemoveFolder->function failed" );
            }
            break;
              
          case "INDSK_S":               
            // Reinitialize the disk - Start initialisation
            // RX: "OK"
            // At this point the reinitialisation process was started.
            // Notes:
            //  The formatting of a 1GB disk takes about 20 sec. This must be considered on adjusting
            //  the progress bar.
            try
            {
              // Message: Continue the process?
              sMsg = r.GetString ("SDcardReaderNew_SafReq_InitDisk");   // "The reinitialisation process may take a long time.\r\nContinue?"
              sCap = r.GetString ( "Form_Common_TextSafReq" );          // "Sicherheitsabfrage"
              DialogResult dr = MessageBox.Show (this, sMsg, sCap, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
              if ( dr == DialogResult.No )
              {
                // No:
                // Finish the transfer
                _EndTransfer ( );
                return;
              }
              
              // Yes:
              // Initiate progress bar: 
              // Filled during 1 min 
              // (Because the corr'ing code in the timer routine where updating
              //  takes place is performed each sec, the max. of the bar is set to 60 (sec))
              this.pgbTransfer.Minimum  = 0;
              this.pgbTransfer.Maximum  = 60;
              this.pgbTransfer.Value    = this.pgbTransfer.Minimum;
              // Disable the form controls
              UpdateData (false);
              // Push the initialisation process 
              // Notes:
              //  Formating the disk is a time-consuming process. During this process a feedback from the
              //  device is not possible. In order to avoid a RX Timeout, the RX Timeout handling is  
              //  suspended during execution of the command.
              CommMessage msg = new CommMessage (comm.msgInitDisk_Perform);
              msg.SuspendRXTimeoutHandling = true;
              msg.WindowInfo = new WndInfo (this.Handle, _bShowWait);
              comm.WriteMessage ( msg );
              // Start timer-based action: SDcard initialisation monitoring
              _eAction = Action.SDcardInitMonitoring;
              _nTimer=0;
            }
            catch
            {
              Debug.WriteLine ( "OnMsgInitDiskStart->function failed" );
            }
            break;

          case "INDSK_P":          
            // Reinitialize the disk - Perform initialisation
            // RX: "OK" or "Error"
            // At this point the reinitialisation process was finished.
            try
            {
              // Stop timer-based action: SDcard initialisation monitoring
              _eAction = Action.None;
              // Finish the transfer
              _EndTransfer ( );
              // Success?
              if ( sPar.ToUpper ().EndsWith ("OK") ) 
              {
                // Yes:
                // Reset controls and members
                ResetAll ();
                // Display: Success message
                sMsg = r.GetString ( "SDcardReaderNew_Info_InitDisk" );     // "The disk was reinitialized successfully";
                sCap = r.GetString ( "Form_Common_TextInfo" );              // "Information"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information );
                // Start timer-based action: SDcard folder structure determination
                _eAction = Action.SDcardDetFolderStructure;
                _nTimer = 0;
              }
              else
              {
                // No:
                // Display: Error message
                sMsg = r.GetString ( "SDcardReaderNew_Error_InitDisk" );    // "An error occurred on reinitializing the disk";
                sCap = r.GetString ( "Form_Common_TextError" );             // "Fehler"
                MessageBox.Show ( this, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error );
              }
            }
            catch
            {
              Debug.WriteLine ( "OnMsgInitDiskPerform->function failed" );
            }
            break;
      
        } // E - switch ( id )
    
        // Update the form
        UpdateData (true);
      }//E - if (e.Message.Id = AppComm.CMID_ANSWER)

    }

    #endregion methods

    #region properties

    /// <summary>
    ///  True, if a Wait cursor should be shown during transmission; False otherwise
    /// </summary>
    public bool ShowWait
    {
      get { return _bShowWait; }
      set { _bShowWait = value; }
    }

    #endregion properties

  }
}
