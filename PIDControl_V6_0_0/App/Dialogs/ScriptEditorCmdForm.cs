using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace App
{
	/// <summary>
	/// Class ScriptEditorCmdForm:
	/// Script editor: Processing of a script command entry
	/// </summary>
	public class ScriptEditorCmdForm : System.Windows.Forms.Form
	{
    private System.Windows.Forms.Button _btnOK;
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.TextBox _txtTime;
    private System.Windows.Forms.Label _lblTime;
    private System.Windows.Forms.Label _lblCmd;
    private System.Windows.Forms.Label _lblParameter;
    private System.Windows.Forms.ComboBox _cbCmd;
    private System.Windows.Forms.TextBox _txtParameter;
    private System.Windows.Forms.ComboBox _cbParameter;
    private System.Windows.Forms.ToolTip _ToolTip;
    private System.ComponentModel.IContainer components;
    
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="fom">The form open mode</param>
    public ScriptEditorCmdForm(Common.FormOpenMode fom)
    {
      InitializeComponent();
      // Init
      _fom=fom;
    }
    
    /// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.components = new System.ComponentModel.Container();
      this._txtTime = new System.Windows.Forms.TextBox();
      this._lblTime = new System.Windows.Forms.Label();
      this._lblCmd = new System.Windows.Forms.Label();
      this._btnOK = new System.Windows.Forms.Button();
      this._btnCancel = new System.Windows.Forms.Button();
      this._lblParameter = new System.Windows.Forms.Label();
      this._txtParameter = new System.Windows.Forms.TextBox();
      this._cbCmd = new System.Windows.Forms.ComboBox();
      this._cbParameter = new System.Windows.Forms.ComboBox();
      this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
      this.SuspendLayout();
      // 
      // _txtTime
      // 
      this._txtTime.Location = new System.Drawing.Point(112, 8);
      this._txtTime.Name = "_txtTime";
      this._txtTime.TabIndex = 1;
      this._txtTime.Text = "";
      this._txtTime.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _lblTime
      // 
      this._lblTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblTime.Location = new System.Drawing.Point(8, 8);
      this._lblTime.Name = "_lblTime";
      this._lblTime.TabIndex = 0;
      this._lblTime.Text = "&Time:";
      // 
      // _lblCmd
      // 
      this._lblCmd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblCmd.Location = new System.Drawing.Point(8, 36);
      this._lblCmd.Name = "_lblCmd";
      this._lblCmd.TabIndex = 2;
      this._lblCmd.Text = "&Command:";
      // 
      // _btnOK
      // 
      this._btnOK.Location = new System.Drawing.Point(24, 100);
      this._btnOK.Name = "_btnOK";
      this._btnOK.TabIndex = 7;
      this._btnOK.Text = "OK";
      this._btnOK.Click += new System.EventHandler(this._btnOK_Click);
      // 
      // _btnCancel
      // 
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point(124, 100);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.TabIndex = 8;
      this._btnCancel.Text = "Cancel";
      // 
      // _lblParameter
      // 
      this._lblParameter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._lblParameter.Location = new System.Drawing.Point(8, 64);
      this._lblParameter.Name = "_lblParameter";
      this._lblParameter.TabIndex = 4;
      this._lblParameter.Text = "&Parameter:";
      // 
      // _txtParameter
      // 
      this._txtParameter.Location = new System.Drawing.Point(112, 64);
      this._txtParameter.Name = "_txtParameter";
      this._txtParameter.TabIndex = 5;
      this._txtParameter.Text = "";
      this._txtParameter.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _cbCmd
      // 
      this._cbCmd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this._cbCmd.Location = new System.Drawing.Point(112, 36);
      this._cbCmd.Name = "_cbCmd";
      this._cbCmd.Size = new System.Drawing.Size(100, 21);
      this._cbCmd.TabIndex = 3;
      this._cbCmd.SelectedIndexChanged += new System.EventHandler(this._cbCmd_SelectedIndexChanged);
      this._cbCmd.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _cbParameter
      // 
      this._cbParameter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this._cbParameter.Location = new System.Drawing.Point(112, 64);
      this._cbParameter.Name = "_cbParameter";
      this._cbParameter.Size = new System.Drawing.Size(100, 21);
      this._cbParameter.TabIndex = 6;
      this._cbParameter.HelpRequested += new System.Windows.Forms.HelpEventHandler(this._ctl_HelpRequested);
      // 
      // _ToolTip
      // 
      this._ToolTip.AutoPopDelay = 10000;
      this._ToolTip.InitialDelay = 500;
      this._ToolTip.ReshowDelay = 100;
      // 
      // ScriptEditorCmdForm
      // 
      this.AcceptButton = this._btnOK;
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.CancelButton = this._btnCancel;
      this.ClientSize = new System.Drawing.Size(222, 136);
      this.Controls.Add(this._cbParameter);
      this.Controls.Add(this._cbCmd);
      this.Controls.Add(this._lblParameter);
      this.Controls.Add(this._txtParameter);
      this.Controls.Add(this._txtTime);
      this.Controls.Add(this._btnCancel);
      this.Controls.Add(this._btnOK);
      this.Controls.Add(this._lblCmd);
      this.Controls.Add(this._lblTime);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.HelpButton = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ScriptEditorCmdForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Script editor - Command";
      this.Load += new System.EventHandler(this.ScriptEditorCmdForm_Load);
      this.ResumeLayout(false);

    }

		#endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void ScriptEditorCmdForm_Load(object sender, System.EventArgs e)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      try
      {
        // Resources
        this.Text               = r.GetString ( "ScriptEditorCmd_Title" );        // "Scripteditor - Befehl"
        this._btnCancel.Text    = r.GetString ( "ScriptEditorCmd_btnCancel" );    // "Abbruch"
        this._btnOK.Text        = r.GetString ( "ScriptEditorCmd_btnOK" );        // "OK"
        this._lblTime.Text      = r.GetString ( "ScriptEditorCmd_lblTime" );      // "&Zeit:"
        this._lblCmd.Text       = r.GetString ( "ScriptEditorCmd_lblCmd" );       // "&Befehl:"
        this._lblParameter.Text = r.GetString ( "ScriptEditorCmd_lblParameter" ); // "&Parameter:"
      }
      catch
      {
      }

      // Fill Cmd CB
      _cbCmd.DataSource = ScriptCommands.Commands;

      // Control initialisation
      _txtTime.Text       = _Time.ToString();
      if (_fom == Common.FormOpenMode.Add)
      {
        _cbCmd.SelectedItem = _cbCmd.Items[0];
      }
      else if (_fom == Common.FormOpenMode.Edit)
      {
        _cbCmd.SelectedItem = ScriptCommands.FromCommand (_Cmd);
        // Parameter input: Via CB or TB?
        Boolean bTB = ScriptCommands.ParameterViaTB (_Cmd);
        if (bTB)
        {
          // Via TB
          _txtParameter.Text = _Parameter;
        }
        else 
        {
          // Via CB
          _cbParameter.Text = _Parameter;
        }
      }
    }

    /// <summary>
    /// 'Click' event of the 'OK' button
    /// </summary>
    private void _btnOK_Click(object sender, System.EventArgs e)
    {
//      int n;
      string s, sParToken;
      object o;
      
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Time
      if ( !Check.Execute ( this._txtTime, CheckType.Int, CheckRelation.GE, 0, 0, true, out o) )
        return;
      _Time=(int) o;
      // Cmd
      s=_cbCmd.SelectedItem.ToString ().Trim ();
      if (s.Length  > ScriptEvent.MAX_SCRIPT_EVENTCMDSIZE)
      {
        string sText = Common.GetAdjustedText (this, _cbCmd);
        string fmt = r.GetString ("ScriptEditorCmd_Error_InvalidValue");  // "Ungültiger Wert:\r\n'{0} = {1}'"
        string sMsg = string.Format (fmt, sText, s);
        string sCap = r.GetString ("Form_Common_TextError");              // "Fehler"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        _cbCmd.Focus ();
        return;
      }
      _Cmd=s;
      // Parameter
      //    Parameter input: Via CB or TB?
      Boolean bTB = ScriptCommands.ParameterViaTB (_Cmd);
      if (bTB)
      {
        // Via TB:

        // Get the parameter string
        s = _txtParameter.Text.Trim ();
        // Perform a parameter validity check
        int res = ScriptEvent.CheckParameters (_Cmd, s, out sParToken);
        if (res > 0)
        {
          // Error
          string sText = Common.GetAdjustedText (this, _txtParameter);
          string fmt = r.GetString ("ScriptEditorCmd_Error_InvalidValue");  // "Ungültiger Wert:\r\n'{0} = {1}'"
          string sValue = "";
          switch (res)
          {
            case 1:       // parameter number outside the defined margins
            case 2:       // parameter number doesn't correspond with the defined one
              sValue = s; break;

            case 3:       // parameter is NOT a non-negative integer
            case 4:       // parameter themselves outside the defined margins
              sValue = sParToken; break;
          }
          string sMsg = string.Format (fmt, sText, sValue);
          string sCap = r.GetString ("Form_Common_TextError");              // "Fehler"
          MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
          _txtParameter.Focus ();
          return;
        }

      }
      else 
      {
        // Via CB: No check required
        s = _cbParameter.Text.Trim ();
      }
      _Parameter=s;

      // OK
      this.DialogResult = DialogResult.OK;
      this.Close ();
    }

    /// <summary>
    /// 'SelectedIndexChanged' event of the 'Command' CB
    /// </summary>
    private void _cbCmd_SelectedIndexChanged(object sender, System.EventArgs e)
    {
      // Get the selected entry
      if (_cbCmd.SelectedIndex == -1)
        return;
      string sCmd = _cbCmd.SelectedItem.ToString ();
      // Parameter input: Via CB or TB?
      Boolean bTB = ScriptCommands.ParameterViaTB (sCmd);
      if (bTB) 
      {
        // Via TB: TB is shown, CB is hidden
        _txtParameter.Visible=true;
        _cbParameter.Visible=false;
      }
      else
      {
        // Via CB: TB is hidden, CB is shown
        _txtParameter.Visible=false;
        _cbParameter.Visible=true;
      }
      // Initial data 
      if (bTB)
      {
        // TB is shown:
        // Clear it
        _txtParameter.Text = "";
      }
      else 
      {
        // CB is shown:
        // Fill it corr'ing to the script command selected
        
        // Prepare the parameter ranges ( the CB items ) corr'ing to the script command selected
        int min1=0, max1=-1, min2=0, max2=-1; // 1. and 2. ranges
        ScriptCommands.ParamRangeOnCmd ( sCmd, ref min1, ref max1, ref min2, ref max2 );
        // Fill CB
        _cbParameter.Items.Clear ();
        for (int i=min1; i <= max1 ;i++)
          _cbParameter.Items.Add (i.ToString ());
        for (int i=min2; i <= max2 ;i++)
          _cbParameter.Items.Add (i.ToString ());
        // Update CB: Show the 1. list element
        _cbParameter.SelectedIndex = 0;
      }
    }

    /// <summary>
    /// 'HelpRequested' event of some controls
    /// </summary>
    private void _ctl_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Help requesting control
      Control requestingControl = (Control)sender;
      // Assign help text
      string s = "";
      if      (requestingControl == this._txtTime)
        s = r.GetString ("ScriptEditorCmd_Hlp_txtTime");            // "The time at which the command is executed (in 1/10 sec).";
      else if (requestingControl == this._cbCmd)
      {
        s = r.GetString ("ScriptEditorCmd_Hlp_cbCmd");              // "The script command to be executed:\r\n";
        // Get the help text for the selected script command 
        if (_cbCmd.SelectedIndex == -1)
          return;
        s += ScriptCommands.HelpOnCmd ( _cbCmd.SelectedItem.ToString () );
      }
      else if ((requestingControl == this._cbParameter) || (requestingControl == this._txtParameter))
      {
        s = r.GetString ("ScriptEditorCmd_Hlp_cbParameter");        // "The script command parameter:\r\n";
        // Get the help text for the parameters of the selected script command 
        if (_cbCmd.SelectedIndex == -1)
          return;
        s += ScriptCommands.HelpOnPar ( _cbCmd.SelectedItem.ToString () );
      }
      // Show help
      this._ToolTip.SetToolTip (requestingControl, s);
      hlpevent.Handled=true;
    }

    #endregion event handling

    #region members
   
    /// <summary>
    /// The form open mode
    /// </summary>
    Common.FormOpenMode _fom = Common.FormOpenMode.Add;

    /// <summary>
    /// The script command time (in 1/10 sec)
    /// </summary>
    int _Time = 0;

    /// <summary>
    /// The script command
    /// </summary>
    string _Cmd = "";
    
    /// <summary>
    /// The command parameters (min. 1, max. 5 parameters, separated by ',')
    /// </summary>
    string _Parameter = "";

    #endregion members

    #region properties

    /// <summary>
    /// The script command time (in 1/10 sec)
    /// </summary>
    public int Time
    {
      get { return _Time; }
      set { _Time = value; }
    }

    /// <summary>
    /// The script command
    /// </summary>
    public string Cmd
    {
      get { return _Cmd; }
      set { _Cmd = value; }
    }

    /// <summary>
    /// The command parameters (min. 1, max. 5 parameters, separated by ',')
    /// </summary>
    public string Parameter
    {
      get { return _Parameter; }
      set { _Parameter = value; }
    }

    #endregion properties

    #region methods
    #endregion methods

	}

}
