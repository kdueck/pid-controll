using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace App
{
	/// <summary>
	/// Class Test1Form:
  /// Used in class 'Test1'
  /// </summary>
	public class Test1Form : System.Windows.Forms.Form
	{
    private System.Windows.Forms.Label lblFilterWidth;
    private System.Windows.Forms.ComboBox cbFilterWidth;
    private System.Windows.Forms.Label lblNSigPD;
    private System.Windows.Forms.ComboBox cbNSigPD;
    private System.Windows.Forms.GroupBox gbMethod;
    private System.Windows.Forms.RadioButton rbSG4;
    private System.Windows.Forms.RadioButton rbSG3;
    private System.Windows.Forms.RadioButton rbSG2;
    private System.Windows.Forms.RadioButton rbSG1;
    private System.Windows.Forms.Label lblMA;
    private System.Windows.Forms.Label lblSG;
    private System.Windows.Forms.RadioButton rbMA;
    private System.Windows.Forms.CheckBox chkOverwrite;
    private System.Windows.Forms.ComboBox cbNSigPA;
    private System.Windows.Forms.Label lblNSigPA;
    private System.Windows.Forms.ComboBox cbModePD;
    private System.Windows.Forms.Label lblModePD;
    private System.Windows.Forms.ComboBox cbModePA;
    private System.Windows.Forms.Label lblModePA;
		
    /// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

    /// <summary>
    /// Constructor
    /// </summary>
    public Test1Form()
		{
			InitializeComponent();

      // Init.
      Init ();
    }

    /// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

    /// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.lblFilterWidth = new System.Windows.Forms.Label();
      this.chkOverwrite = new System.Windows.Forms.CheckBox();
      this.cbFilterWidth = new System.Windows.Forms.ComboBox();
      this.cbNSigPD = new System.Windows.Forms.ComboBox();
      this.lblNSigPD = new System.Windows.Forms.Label();
      this.gbMethod = new System.Windows.Forms.GroupBox();
      this.rbMA = new System.Windows.Forms.RadioButton();
      this.lblSG = new System.Windows.Forms.Label();
      this.lblMA = new System.Windows.Forms.Label();
      this.rbSG4 = new System.Windows.Forms.RadioButton();
      this.rbSG3 = new System.Windows.Forms.RadioButton();
      this.rbSG2 = new System.Windows.Forms.RadioButton();
      this.rbSG1 = new System.Windows.Forms.RadioButton();
      this.cbNSigPA = new System.Windows.Forms.ComboBox();
      this.lblNSigPA = new System.Windows.Forms.Label();
      this.cbModePD = new System.Windows.Forms.ComboBox();
      this.lblModePD = new System.Windows.Forms.Label();
      this.cbModePA = new System.Windows.Forms.ComboBox();
      this.lblModePA = new System.Windows.Forms.Label();
      this.gbMethod.SuspendLayout();
      this.SuspendLayout();
      // 
      // lblFilterWidth
      // 
      this.lblFilterWidth.Location = new System.Drawing.Point(16, 236);
      this.lblFilterWidth.Name = "lblFilterWidth";
      this.lblFilterWidth.Size = new System.Drawing.Size(175, 20);
      this.lblFilterWidth.TabIndex = 2;
      this.lblFilterWidth.Text = "Smoothing filter width:";
      // 
      // chkOverwrite
      // 
      this.chkOverwrite.Location = new System.Drawing.Point(8, 8);
      this.chkOverwrite.Name = "chkOverwrite";
      this.chkOverwrite.Size = new System.Drawing.Size(300, 24);
      this.chkOverwrite.TabIndex = 0;
      this.chkOverwrite.Text = "Overwrite ScanParams?";
      this.chkOverwrite.CheckedChanged += new System.EventHandler(this.chkOverwrite_CheckedChanged);
      // 
      // cbFilterWidth
      // 
      this.cbFilterWidth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbFilterWidth.Location = new System.Drawing.Point(200, 236);
      this.cbFilterWidth.Name = "cbFilterWidth";
      this.cbFilterWidth.Size = new System.Drawing.Size(72, 21);
      this.cbFilterWidth.TabIndex = 3;
      // 
      // cbNSigPD
      // 
      this.cbNSigPD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbNSigPD.Location = new System.Drawing.Point(200, 264);
      this.cbNSigPD.Name = "cbNSigPD";
      this.cbNSigPD.Size = new System.Drawing.Size(72, 21);
      this.cbNSigPD.TabIndex = 5;
      // 
      // lblNSigPD
      // 
      this.lblNSigPD.Location = new System.Drawing.Point(16, 264);
      this.lblNSigPD.Name = "lblNSigPD";
      this.lblNSigPD.Size = new System.Drawing.Size(175, 20);
      this.lblNSigPD.TabIndex = 4;
      this.lblNSigPD.Text = "N-sigma (Peak detection):";
      // 
      // gbMethod
      // 
      this.gbMethod.Controls.Add(this.rbMA);
      this.gbMethod.Controls.Add(this.lblSG);
      this.gbMethod.Controls.Add(this.lblMA);
      this.gbMethod.Controls.Add(this.rbSG4);
      this.gbMethod.Controls.Add(this.rbSG3);
      this.gbMethod.Controls.Add(this.rbSG2);
      this.gbMethod.Controls.Add(this.rbSG1);
      this.gbMethod.Location = new System.Drawing.Point(12, 40);
      this.gbMethod.Name = "gbMethod";
      this.gbMethod.Size = new System.Drawing.Size(296, 180);
      this.gbMethod.TabIndex = 1;
      this.gbMethod.TabStop = false;
      this.gbMethod.Text = "Smoothing method";
      // 
      // rbMA
      // 
      this.rbMA.Location = new System.Drawing.Point(52, 140);
      this.rbMA.Name = "rbMA";
      this.rbMA.Size = new System.Drawing.Size(228, 24);
      this.rbMA.TabIndex = 6;
      this.rbMA.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
      // 
      // lblSG
      // 
      this.lblSG.Location = new System.Drawing.Point(8, 24);
      this.lblSG.Name = "lblSG";
      this.lblSG.Size = new System.Drawing.Size(36, 20);
      this.lblSG.TabIndex = 0;
      this.lblSG.Text = "SG:";
      // 
      // lblMA
      // 
      this.lblMA.Location = new System.Drawing.Point(8, 140);
      this.lblMA.Name = "lblMA";
      this.lblMA.Size = new System.Drawing.Size(36, 20);
      this.lblMA.TabIndex = 5;
      this.lblMA.Text = "MA:";
      // 
      // rbSG4
      // 
      this.rbSG4.Location = new System.Drawing.Point(52, 108);
      this.rbSG4.Name = "rbSG4";
      this.rbSG4.Size = new System.Drawing.Size(228, 24);
      this.rbSG4.TabIndex = 4;
      this.rbSG4.Text = "Common case (Wrapping)";
      this.rbSG4.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
      // 
      // rbSG3
      // 
      this.rbSG3.Location = new System.Drawing.Point(52, 80);
      this.rbSG3.Name = "rbSG3";
      this.rbSG3.Size = new System.Drawing.Size(228, 24);
      this.rbSG3.TabIndex = 3;
      this.rbSG3.Text = "Fixed filter width: 41 (Wrapping)";
      this.rbSG3.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
      // 
      // rbSG2
      // 
      this.rbSG2.Location = new System.Drawing.Point(52, 52);
      this.rbSG2.Name = "rbSG2";
      this.rbSG2.Size = new System.Drawing.Size(228, 24);
      this.rbSG2.TabIndex = 2;
      this.rbSG2.Text = "Fixed filter width: 41 (No wrapping)";
      this.rbSG2.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
      // 
      // rbSG1
      // 
      this.rbSG1.Location = new System.Drawing.Point(52, 24);
      this.rbSG1.Name = "rbSG1";
      this.rbSG1.Size = new System.Drawing.Size(228, 24);
      this.rbSG1.TabIndex = 1;
      this.rbSG1.Text = "Common case (No wrapping)";
      this.rbSG1.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
      // 
      // cbNSigPA
      // 
      this.cbNSigPA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbNSigPA.Location = new System.Drawing.Point(200, 292);
      this.cbNSigPA.Name = "cbNSigPA";
      this.cbNSigPA.Size = new System.Drawing.Size(72, 21);
      this.cbNSigPA.TabIndex = 7;
      // 
      // lblNSigPA
      // 
      this.lblNSigPA.Location = new System.Drawing.Point(16, 292);
      this.lblNSigPA.Name = "lblNSigPA";
      this.lblNSigPA.Size = new System.Drawing.Size(175, 32);
      this.lblNSigPA.TabIndex = 6;
      this.lblNSigPA.Text = "N-sigma (Peak area determination):";
      // 
      // cbModePD
      // 
      this.cbModePD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbModePD.Location = new System.Drawing.Point(464, 264);
      this.cbModePD.Name = "cbModePD";
      this.cbModePD.Size = new System.Drawing.Size(72, 21);
      this.cbModePD.TabIndex = 9;
      // 
      // lblModePD
      // 
      this.lblModePD.Location = new System.Drawing.Point(280, 264);
      this.lblModePD.Name = "lblModePD";
      this.lblModePD.Size = new System.Drawing.Size(175, 20);
      this.lblModePD.TabIndex = 8;
      this.lblModePD.Text = "Mode (Peak detection):";
      // 
      // cbModePA
      // 
      this.cbModePA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbModePA.Location = new System.Drawing.Point(464, 292);
      this.cbModePA.Name = "cbModePA";
      this.cbModePA.Size = new System.Drawing.Size(72, 21);
      this.cbModePA.TabIndex = 11;
      // 
      // lblModePA
      // 
      this.lblModePA.Location = new System.Drawing.Point(280, 292);
      this.lblModePA.Name = "lblModePA";
      this.lblModePA.Size = new System.Drawing.Size(175, 32);
      this.lblModePA.TabIndex = 10;
      this.lblModePA.Text = "Mode (Peak area determination):";
      // 
      // Test1Form
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.ClientSize = new System.Drawing.Size(542, 332);
      this.Controls.Add(this.cbModePA);
      this.Controls.Add(this.lblModePA);
      this.Controls.Add(this.cbModePD);
      this.Controls.Add(this.lblModePD);
      this.Controls.Add(this.cbNSigPA);
      this.Controls.Add(this.lblNSigPA);
      this.Controls.Add(this.gbMethod);
      this.Controls.Add(this.cbNSigPD);
      this.Controls.Add(this.lblNSigPD);
      this.Controls.Add(this.cbFilterWidth);
      this.Controls.Add(this.chkOverwrite);
      this.Controls.Add(this.lblFilterWidth);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "Test1Form";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Test1 (Spectrum evaluation)";
      this.Closing += new System.ComponentModel.CancelEventHandler(this.TestForm_Closing);
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TestForm_KeyPress);
      this.Load += new System.EventHandler(this.TestForm_Load);
      this.gbMethod.ResumeLayout(false);
      this.ResumeLayout(false);

    }
		
    #endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void TestForm_Load(object sender, System.EventArgs e)
    {
    }

    /// <summary>
    /// 'Closing' event of the form
    /// </summary>
    private void TestForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      Overwrite = this.chkOverwrite.Checked; 
      
      if      (this.rbSG1.Checked) Method = 1;
      else if (this.rbSG2.Checked) Method = 2;
      else if (this.rbSG3.Checked) Method = 3;
      else if (this.rbSG4.Checked) Method = 4;
      else if (this.rbMA.Checked) Method = 5;
      FilterWidth = int.Parse (this.cbFilterWidth.Text);
      NSigmaPD = int.Parse(this.cbNSigPD.Text);
      switch (this.cbModePD.Text)
      {
        case PeakSearch.Modi.Unidirectional:  NModePD = 0; break;
        case PeakSearch.Modi.Bidirectional:   NModePD = 1; break;
      }
      NSigmaPA = int.Parse(this.cbNSigPA.Text);
      switch (this.cbModePA.Text)
      {
        case PeakAreaDetermination.Modi.Simple:      NModePA = 0; break;
        case PeakAreaDetermination.Modi.Extended_1:  NModePA = 1; break;
        case PeakAreaDetermination.Modi.Extended_2:  NModePA = 2; break;
      }

      if (Overwrite)
      {
        if (Method == 5 && FilterWidth > 11)
        {
          string sMsg = "Filterbreite f�r MA in [3,11]!";
          string sCap = "Error";
          MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
          e.Cancel = true;
        }
      }

    }

    /// <summary>
    /// 'KeyPress' event of the form
    /// </summary>
    private void TestForm_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
    {
      // Close the form on pressing 'Enter' or 'Escape'
      if ( e.KeyChar == (char) Keys.Enter || e.KeyChar == (char) Keys.Escape )
      {
        this.Close ();
      }
    }
	
    /// <summary>
    /// 'CheckedChanged' event of the 'Overwrite' CheckBox control
    /// </summary>
    private void chkOverwrite_CheckedChanged(object sender, System.EventArgs e)
    {
      // GB 'Smoothing method': 
      //  Enabled, if 'Overwrite' is requested.
      this.gbMethod.Enabled = this.chkOverwrite.Checked;

      // CB 'Filterwidth': 
      //  Enabled, if 'Overwrite' is requested AND the smoothing method No.1,4,5
      //  (SG: Common cases, MA) is selected; Disabled otherwise.
      bool b = this.chkOverwrite.Checked && (rbSG1.Checked || rbSG4.Checked || rbMA.Checked); 
      this.cbFilterWidth.Enabled = b;
  
      // CB 'N-sigma peak detection'
      //  Enabled, if 'Overwrite' is requested.
      this.cbNSigPD.Enabled = this.chkOverwrite.Checked;
    
      // CB 'Mode peak detection'
      //  Enabled, if 'Overwrite' is requested.
      this.cbModePD.Enabled = this.chkOverwrite.Checked;

      // CB 'N-sigma peak area determination'
      //  Enabled, if 'Overwrite' is requested.
      this.cbNSigPA.Enabled = this.chkOverwrite.Checked;
      
      // CB 'Mode peak area determination'
      //  Enabled, if 'Overwrite' is requested.
      this.cbModePA.Enabled = this.chkOverwrite.Checked;
    }

    /// <summary>
    /// 'CheckedChanged' event of the RB controls
    /// </summary>
    private void rb_CheckedChanged(object sender, System.EventArgs e)
    {
      // CB 'Filterwidth': 
      //  Enabled, if 'Overwrite' is requested AND the smoothing method No.1,4,5
      //  (SG: Common cases, MA) is selected; Disabled otherwise.
      bool b = this.chkOverwrite.Checked && (rbSG1.Checked || rbSG4.Checked || rbMA.Checked); 
      this.cbFilterWidth.Enabled = b;
    }
 
    #endregion event handling

    #region members
    
    /// <summary>
    /// Indicate, whether parameter overwriting is desired (True) or not (False)
    /// </summary>
    public bool Overwrite = false;
    
    /// <summary>
    /// The smoothing method: 
    /// 1-4: SG
    ///   1 - Commom case (No wrapping), 2 - Fixed filter width: 41 (No wrapping), 
    ///   3 - Fixed filter width: 41 (Wrapping), 4 - Commom case (Wrapping)
    /// 5: MA
    /// </summary>
    public int Method = 1;
    
    /// <summary>
    /// The width of the smoothing filter
    /// </summary>
    public int FilterWidth = 41;
    
    /// <summary>
    /// N-sigma factor - peak detection
    /// </summary>
    public int NSigmaPD = 6;

    /// <summary>
    /// Mode - peak detection:
    /// 0 - Unidirectional, 1 - Bidirectional
    /// </summary>
    public int NModePD = 0;
    
    /// <summary>
    /// N-sigma factor - peak area determination
    /// </summary>
    public int NSigmaPA = 3;
    
    /// <summary>
    /// Mode - peak area determination:
    /// 0 - Simple, 1 - Extended/level 1, 2 - Extended/level 2
    /// </summary>
    public int NModePA = 0;
    
    #endregion members

    #region methods

    /// <summary>
    /// Init's the form.
    /// </summary>
    void Init ()
    {
      // Init. ChB
      this.chkOverwrite.Checked = Overwrite;
      chkOverwrite_CheckedChanged (null, new System.EventArgs ());
      
      // Init. RB's 'Smoothing method'
      switch (Method)
      {
          // SG
        case 1: this.rbSG1.Checked = true; break; // Commom case (No wrapping)
        case 2: this.rbSG2.Checked = true; break; // Fixed filter width: 41 (No wrapping)
        case 3: this.rbSG3.Checked = true; break; // Fixed filter width: 41 (Wrapping)
        case 4: this.rbSG4.Checked = true; break; // Commom case (Wrapping)
          // MA
        case 5: this.rbMA.Checked = true; break;
      }
      
      // Fill & Init. CB 'Filter width'    
      for (int i=3; i <= 41; i+=2)
        this.cbFilterWidth.Items.Add (i);
      if (this.cbFilterWidth.Items.Count > 0)
        this.cbFilterWidth.SelectedItem = (object) FilterWidth;
      
      // Fill & Init. CB 'N-sigma peak detection'    
      this.cbNSigPD.Items.Clear();
      for (int i=0; i <= 20; i++)
        this.cbNSigPD.Items.Add (i);
      if (this.cbNSigPD.Items.Count > 0)
        this.cbNSigPD.SelectedItem = (object) NSigmaPD;
   
      // Fill & Init. CB 'Mode peak detection'    
      this.cbModePD.Items.Clear();
      this.cbModePD.Items.AddRange (new object[] {
                                                   PeakSearch.Modi.Unidirectional, 
                                                   PeakSearch.Modi.Bidirectional
                                                 });
      switch (NModePD)
      {
        case 0: this.cbModePD.SelectedItem = PeakSearch.Modi.Unidirectional; break;
        case 1: this.cbModePD.SelectedItem = PeakSearch.Modi.Bidirectional; break;
      }

      // Fill & Init. CB 'N-sigma peak area determination'    
      this.cbNSigPA.Items.Clear ();
      for (int i=0; i <= 20; i++)
        this.cbNSigPA.Items.Add (i);
      if (this.cbNSigPA.Items.Count > 0)
        this.cbNSigPA.SelectedItem = (object) NSigmaPA;
    
      // Fill & Init. CB 'Mode peak area determination'    
      this.cbModePA.Items.Clear();
      this.cbModePA.Items.AddRange (new object[] {
                                                   PeakAreaDetermination.Modi.Simple, 
                                                   PeakAreaDetermination.Modi.Extended_1, 
                                                   PeakAreaDetermination.Modi.Extended_2
                                                 });
      switch (NModePA)
      {
        case 0: this.cbModePA.SelectedItem = PeakAreaDetermination.Modi.Simple; break;
        case 1: this.cbModePA.SelectedItem = PeakAreaDetermination.Modi.Extended_1; break;
        case 2: this.cbModePA.SelectedItem = PeakAreaDetermination.Modi.Extended_2; break;
      }
    }

    #endregion methods

  }
}
