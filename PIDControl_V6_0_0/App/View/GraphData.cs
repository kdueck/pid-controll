using System;
using System.Drawing;
using System.Drawing.Drawing2D;

// NOTE:
// The 2nd draft of a graphics interface.
// Preferred variant.

namespace App
{
  /// <summary>
  /// Class DataRect:
  /// Rectangle-like Data structure
  /// </summary>
  public class DataRect
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public DataRect()
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="xmin">Left data point of the Rectangle-like Data structure</param>
    /// <param name="xmax">Right data point of the Rectangle-like Data structure</param>
    /// <param name="ymin">Bottom data point of the Rectangle-like Data structure</param>
    /// <param name="ymax">Top data point of the Rectangle-like Data structure</param>
    public DataRect(float xmin, float xmax, float ymin, float ymax)
    {
      if (xmin < xmax)  { _xmin = xmin; _xmax = xmax; }
      else              { _xmin = xmax; _xmax = xmin; }
      if (ymin < ymax)  { _ymin = ymin; _ymax = ymax; }
      else              { _ymin = ymax; _ymax = ymin; }
    }

    #endregion constructors

    #region constants & enums
    #endregion constants & enums

    #region members
    
    // The 4 data points (TL, RB) of a Rectangle-like Data structure
    float _xmin=0, _xmax=0, _ymin=0, _ymax=0;
    
    #endregion members

    #region methods
    
    /// <summary>
    /// Sets the 4 data points (TL, RB) of a Rectangle-like Data structure
    /// </summary>
    /// <param name="xmin">Left data point of the Rectangle-like Data structure</param>
    /// <param name="xmax">Right data point of the Rectangle-like Data structure</param>
    /// <param name="ymin">Bottom data point of the Rectangle-like Data structure</param>
    /// <param name="ymax">Top data point of the Rectangle-like Data structure</param>
    public void Set (float xmin, float xmax, float ymin, float ymax)
    {
      if (xmin < xmax)  { _xmin = xmin; _xmax = xmax; }
      else              { _xmin = xmax; _xmax = xmin; }
      if (ymin < ymax)  { _ymin = ymin; _ymax = ymax; }
      else              { _ymin = ymax; _ymax = ymin; }
    }

    /// <summary>
    /// Sets the 4 data points (TL, RB) of a Rectangle-like Data structure
    /// </summary>
    /// <param name="dr">The source Data structure</param>
    public void Set (DataRect dr)
    {
      if (dr.xMin < dr.xMax)  { _xmin = dr.xMin; _xmax = dr.xMax; }
      else                    { _xmin = dr.xMax; _xmax = dr.xMin; }
      if (dr.yMin < dr.yMax)  { _ymin = dr.yMin; _ymax = dr.yMax; }
      else                    { _ymin = dr.yMax; _ymax = dr.yMin; }
    }

    /// <summary>
    /// Gets a value indicating whether the Rectangle-like Data structure is empty.
    /// </summary>
    /// <returns>True, if the Data structure is empty; otherwise False</returns>
    public bool IsEmpty ()
    {
      bool bIsEmpty = (_xmin == 0) && (_xmax == 0) && (_ymin == 0) && (_ymax == 0);
      return bIsEmpty;
    }

    /// <summary>
    /// Gets a value indicating whether the Rectangle-like Data structure is valid.
    /// </summary>
    /// <returns>True, if the Data structure is valid; otherwise False</returns>
    /// <remarks>
    /// The Data structure is qualified as not valid, if either its width or its height
    /// or both equals to zero.
    /// </remarks>
    public bool IsValid ()
    {
      bool bIsNotValid = (Math.Abs(Width) < 1e-6) || (Math.Abs(Height) < 1e-6);
      return !bIsNotValid;
    }

    /// <summary>
    /// Gets a value indicating whether the Rectangle-like Data structure contains
    /// a given data point.
    /// </summary>
    /// <param name="x">The abscissa of the data point</param>
    /// <param name="y">The ordinate of the data point</param>
    /// <returns>True, if the Data structure contains the data point; otherwise False</returns>
    public bool Contains (float x, float y)
    {
      bool bContains = (x >= _xmin && x <= _xmax) && (y >= _ymin && y <= _ymax);
      return bContains;
    }

    #endregion methods    

    #region properties
    
    /// <summary>
    /// Left data point of the Rectangle-like Data structure
    /// </summary>
    public float xMin 
    { 
      get { return _xmin; } 
      set { _xmin = value; }
    }
    /// <summary>
    /// Right data point of the Rectangle-like Data structure
    /// </summary>
    public float xMax
    { 
      get { return _xmax; }
      set { _xmax = value; }
    }
    /// <summary>
    /// Bottom data point of the Rectangle-like Data structure
    /// </summary>
    public float yMin
    {
      get { return _ymin; } 
      set { _ymin = value; }
    }
    /// <summary>
    /// Top data point of the Rectangle-like Data structure
    /// </summary>
    public float yMax
    { 
      get { return _ymax; }
      set { _ymax = value; }
    }
    /// <summary>
    /// Width of the Rectangle-like Data structure
    /// </summary>
    public float Width { get { return _xmax - _xmin; } }
    /// <summary>
    /// Height of the Rectangle-like Data structure
    /// </summary>
    public float Height { get { return _ymax - _ymin; } }
    
    #endregion properties    

  }// E - DataRect
  
  
  /// <summary>
  /// Class Scale:
  /// Coordinate scaling
  /// </summary>
  public class Scale
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public Scale()
    {
    }

    #endregion constructors

    #region constants & enums
    #endregion constants & enums

    #region members
    #endregion members

    #region methods

    /// <summary>
    /// Scales a graphical coordinate (pixel) to a data value.
    /// </summary>
    /// <param name="p">The pixel, whose corr'ing data value is requested</param>
    /// <param name="xl">The left (lower) graphic area margin</param>
    /// <param name="xr">The right (upper) graphic area margin</param>
    /// <param name="xmin">The min. data margin</param>
    /// <param name="xmax">The max. data mergin</param>
    /// <returns>The data value</returns>
    /// <remarks>
    /// Relation:
    /// xmin      x        xmax     Data
    /// |---------|--------|
    /// xl        p        xr       Pels
    /// </remarks>
    public static float PelToData (int p, int xl, int xr, float xmin, float xmax)
    {
      float x = (xmax-xmin) * (p-xl) / (xr-xl) + xmin;
      return x;
    }
    
    /// <summary>
    ///  Scales a data value to a graphical coordinate (pixel).
    /// </summary>
    /// <param name="x">The data value, whose corr'ing graphical coordinate is requested</param>
    /// <param name="xmin">The min. data margin</param>
    /// <param name="xmax">The max. data mergin</param>
    /// <param name="xl">The left (lower) graphic area margin</param>
    /// <param name="xr">The right (upper) graphic area margin</param>
    /// <returns>The pixel</returns>
    /// <remarks>
    /// Relation:
    /// xmin      x        xmax     Data
    /// |---------|--------|
    /// xl        p        xr       Pels
    /// </remarks>
    public static int DataToPel (float x, float xmin, float xmax, int xl, int xr)
    {
      int p = (int) ((xr-xl) * (x-xmin) / (xmax-xmin) + xl);
      return p;
    }
    
    #endregion methods    

    #region properties
    #endregion properties    

  }// E - Scale
  

  /// <summary>
	/// Class GraphData:
	/// Graphical data handling & representation
	/// </summary>
  public class GraphData
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public GraphData()
    {
      // Allocate the data array
      for (int i=0; i < MAX_Y_DATA; i++)
        _arY[i] = new float[0];
    }

    #endregion constructors

    #region constants & enums
    
    /// <summary>
    /// The percentage value, a graphic is widened with in the Y coordinate
    /// </summary>
    const int PERCENT_Y_WIDENING = 10;
    
    /// <summary>
    /// The max. # of one-dimensional data arrays
    /// </summary>
    const int MAX_Y_DATA = 3;

    /// <summary>
    /// The max. # of marker windows
    /// (Should correspond here to the max. number of Scan results permitted 'MAX_SCAN_RESULT')
    /// </summary>
    const int MAX_MARKER_WINDOWS = 16;

    #endregion constants & enums

    #region members
    
    /// <summary>
    /// The abscissa array
    /// </summary>
    float[] _arX = new float[0];
    /// <summary>
    /// The data array
    /// </summary>
    float[][] _arY = new float[MAX_Y_DATA][];
    /// <summary>
    /// The # of elements in the data arrays
    /// </summary>
    int _nX = 0;
    /// <summary>
    /// The # of data arrays
    /// </summary>
    int _nY = 0;

    /// <summary>
    /// The bitmap, the graphic is drawn into
    /// </summary>
    Bitmap _bm = null;
    /// <summary>
    /// The data graph colors 
    /// </summary>
    Color[] _arLineColor = new Color[] { Color.DarkBlue, Color.Red, Color.Green };

    /// <summary>
    /// The actual scale settings
    /// </summary>
    DataRect _XYData = new DataRect ();
    /// <summary>
    /// The default scale settings
    /// </summary>
    DataRect _DefData = new DataRect (0, 100, 0, 100);

    /// <summary>
    /// The begins of the marker windows
    /// </summary>
    float[] _fXMarkBegin = new float[MAX_MARKER_WINDOWS];
    /// <summary>
    /// The widths of the marker windows
    /// </summary>
    float[] _fXMarkWidth = new float[MAX_MARKER_WINDOWS];

    /// <summary>
    /// Indicates, whether cursor drawing is enabled (True) or not (False)
    /// </summary>
    bool _bEnableCursor = true;
    /// <summary>
    /// The abscissa of the cursor
    /// </summary>
    float _fXCursor = 0;
    /// <summary>
    /// The ordinate of the cursor
    /// </summary>
    float _fYCursor = 0;

    /// <summary>
    /// The "Data to be drawn" array: Specifies the data graphs, that should be drawn
    /// </summary>
    bool[] _arDataToDraw = new bool[MAX_Y_DATA];
    
    #endregion members

    #region methods
    
    /// <summary>
    /// Attaches the data to be drawn.
    /// </summary>
    /// <param name="arY">The data array</param>
    /// <param name="xa"> The left abscissa margin</param>
    /// <param name="xe"> The right abscissa margin</param>
    /// <returns>True if OK, False on error</returns>
    /// <remarks>
    /// 1. [xa, xe]: 'nX' equally spaced points, with 'nX' = dimension of the data arrays 'arY[i]'
    /// 2. # of graphs = dimension of data array 'arY'
    /// </remarks>
    public bool Attach (float[][] arY, float xa, float xe)
    {
      int nx, ny, i, j;

      // Init.
      if (xa > xe) { float t = xa; xa = xe; xe = t; }

      // Array dimensions
      //    # of data arrays (in [1, MAX_Y_DATA])
      ny = arY.Length;                        // f.i. 1 
      //    limitation, if req.
      if (ny > MAX_Y_DATA) ny = MAX_Y_DATA;
      //    Condition: All data arrays of equal length!  
      for (i=1; i < ny; i++)
      {
        if (arY[i-1].Length != arY[i].Length)
          break;
      }
      if (i < ny) nx = 0;                     // Data arrays with non-equal length
      else        nx = arY[0].Length;         // Data arrays with equal length: f.i. 2048
      //    Overgive
      _nX = nx;
      _nY = ny;
      
      // Check: Do we have a valid graphical object?
      if (!HasData())
        return false;   // No

      // Yes:
      // Allocation (only, if req.)
      //    Abscissa array
      nx = _arX.Length;
      if (nx != _nX) 
        _arX = new float[_nX];
      //    Data arrays
      nx = _arY[0].Length;
      if (nx != _nX)
        for (i=0; i < _nY; i++)
          _arY[i] = new float[_nX];
      
      // Fill abscissa array
      float delta = (xe - xa)/(_nX - 1);      // Data point spacing
      for (i=0; i < _nX; i++)
        _arX[i] = xa + i*delta;

      // Fill data arrays
      for (j=0; j < _nY; j++)                 // Loop over all data arrays
        for (i=0; i < _nX; i++)               // Loop over all data points within a data array
          _arY[j][i] = arY[j][i]; 
      
      // Show all data graphs
      for (i=0; i < _arDataToDraw.Length; i++)
        _arDataToDraw[i] = true;        
      
      // Done
      return true;
    }

    /// <summary>
    ///  Widens the DataMinMax values in Y direction.
    /// </summary>
    /// <param name="ymin">The minimum Y value</param>
    /// <param name="ymax">The maximum Y value</param>
    void YWidening (ref float ymin, ref float ymax)
    {
      float delta = ((ymax-ymin)/100)*PERCENT_Y_WIDENING;
      ymin -= delta; ymin *= 100; ymin = (float) Math.Floor (ymin); ymin /= 100;    // due to: F2 representation of the axis labels
      ymax += delta; ymax *= 100; ymax = (float) Math.Ceiling (ymax); ymax /= 100;
      if (ymin == ymax)
        ymax = ymin + 0.01f;
    }

    /// <summary>
    /// Draws the graphics.
    /// </summary>
    /// <param name="size">The size of the graphics area</param>
    /// <returns>The drawn image (Null in case of error)</returns>
    public Bitmap Draw (Size size)
    {
      try 
      {
        // Create the image
        _bm = new Bitmap (size.Width, size.Height);
        // Get its underlaying Graphics object
        Graphics g = Graphics.FromImage (_bm);
        // Get its bounding Rectangle
        Rectangle r = new Rectangle (0, 0, _bm.Width, _bm.Height);
        // Draw Background
        Brush b = new SolidBrush (Color.White);
        g.FillRectangle (b, r);
        // Draw the data
        DrawData (g, r);
        // Draw the cursor
        DrawCursor (g, r);       
        // Ready
        return _bm;
      }
      catch
      {
        return null;
      }
    }

    /// <summary>
    /// Draws the data.
    /// </summary>
    /// <param name="g">The Graphics object</param>
    /// <param name="r">The bounding rectangle</param>
    public void DrawData (Graphics g, Rectangle r)
    {
      Point pt = new Point (0,0), ptOld = new Point (0,0);
      
      // Actual scale settings
      DataRect dr = _XYData;
      if (!dr.IsValid ())
        return;
      // Draw the marker windows
      for (int i=0; i < MAX_MARKER_WINDOWS; i++)
      {
        if (_fXMarkBegin[i] != -1)
        {
          int nLeft  = Scale.DataToPel (_fXMarkBegin[i], dr.xMin, dr.xMax, r.Left, r.Right);
          int nRight  = Scale.DataToPel (_fXMarkBegin[i]+_fXMarkWidth[i], dr.xMin, dr.xMax, r.Left, r.Right);
          Rectangle rMark = new Rectangle ( nLeft, r.Top, nRight-nLeft, r.Height );
          SolidBrush br = new SolidBrush ( Color.LightGray );
          g.FillRectangle ( br, rMark );
        }
      }
      // Draw the data graphs
      int ia = IdxFromVal (dr.xMin);
      int ie = IdxFromVal (dr.xMax);
      for (int j=0; j < _nY; j++)
      {
        // Line color & width
        Pen p = new Pen (_arLineColor[j%_arLineColor.Length], 2);
        // Check: Should the current data graph be drawn?
        if (!_arDataToDraw[j])
        {
          // No: make it transparent
          p = new Pen (Color.Transparent, 2);  
        }
        // Draw
        for (int i=ia; i <= ie; i++)
        {
          pt.X = Scale.DataToPel (_arX[i], dr.xMin, dr.xMax, r.Left, r.Right); 
          pt.Y = Scale.DataToPel (_arY[j][i], dr.yMin, dr.yMax, r.Bottom, r.Top);
          if (i > ia)
            g.DrawLine (p, ptOld, pt);
          ptOld = pt;
        }
      }
    }

    /// <summary>
    /// Draws the cursor.
    /// </summary>
    /// <param name="g">The Graphics object</param>
    /// <param name="r">The bounding rectangle</param>
    void DrawCursor (Graphics g, Rectangle r)
    {
      Point pt = new Point (0,0);

      // Actual scale settings
      DataRect dr = _XYData;
      if (!dr.IsValid ())
        return;
      // Draw the Cursor
      if (_bEnableCursor)
      {
        Pen p = new Pen (Color.DarkRed, 1);
        p.DashStyle = DashStyle.Dash; 
        pt.X = Scale.DataToPel (_fXCursor, dr.xMin, dr.xMax, r.Left, r.Right); 
        pt.Y = Scale.DataToPel (_fYCursor, dr.yMin, dr.yMax, r.Bottom, r.Top);
        g.DrawLine (p, r.Left, pt.Y, r.Right, pt.Y);
        g.DrawLine (p, pt.X, r.Top, pt.X, r.Bottom);
      }
    }

    /// <summary>
    /// Gets the index, corring to a given array value.
    /// </summary>
    /// <param name="x">The given array value</param>
    /// <returns>The corr'ing index</returns>
    public int IdxFromVal (float x)
    {
      int idx;

      for (idx=0; idx < _nX; idx++)
        if (_arX[idx] >= x) break;
      if (idx < 0)      idx = 0;
      if (idx > _nX-1)  idx = _nX-1;
      return idx;
    }

    /// <summary>
    /// Gets the value, corring to a given array index.
    /// </summary>
    /// <param name="idx">The given array index</param>
    /// <returns>The corr'ing value</returns>
    public float ValFromIdx (int idx)
    {
      if (idx < 0)      idx = 0;
      if (idx > _nX-1)  idx = _nX-1;
      return _arX[idx];
    }

    /// <summary>
    /// Checks, whether the graphical object is valid or not.
    /// </summary>
    /// <returns>True in case ov validity</returns>
    public bool HasData ()
    {
      // A graphical object is valid, if at least 1 data graph with 2 data points was present
      bool b = (_nX >= 2) && (_nY >= 1);
      return b;
    }
    
    /// <summary>
    /// Fits the scale to the X data.
    /// </summary>
    public void FitScaleToXData  ()
    {
      // Get the abscissa margins
      float xmin = ValFromIdx (0);
      float xmax = ValFromIdx (_arX.Length-1);
      // Update the actual scale settings
      _XYData.xMin = xmin;
      _XYData.xMax = xmax;
      // Adjust the cursor position, if required
      AdjustCursor ();
    }
    
    /// <summary>
    /// Fits the scale to the Y data.
    /// </summary>
    public void FitScaleToYData  ()
    {
      // Get the current abscissa bounds
      float xmin = _XYData.xMin;
      int idxa = IdxFromVal (xmin);
      float xmax = _XYData.xMax;
      int idxe = IdxFromVal (xmax);
      // Get the ordinate DataMinMax values within the current abscissa bounds
      float ymin = float.MaxValue;
      float ymax = float.MinValue;
      for (int j=0; j < _nY; j++)                 // Loop over all data arrays
      {
        for (int i=idxa; i <= idxe; i++)          // Loop over the affected data points within a data array
        {
          if (_arY[j][i] < ymin) ymin = _arY[j][i];
          if (_arY[j][i] > ymax) ymax = _arY[j][i];
        }
      }
      // Widening of the ordinate DataMinMax values
      YWidening (ref ymin, ref ymax);
      // Update the actual scale settings
      _XYData.yMin = ymin;
      _XYData.yMax = ymax;
      // Adjust the cursor position, if required
      AdjustCursor ();
    }
    
    /// <summary>
    /// Fits the scale to the X and Y data.
    /// </summary>
    public void FitScaleToXYData  ()
    {
      FitScaleToXData();
      FitScaleToYData();
    }

    /// <summary>
    /// Fits the scale to the default data (in both X and Y coord's).
    /// </summary>
    public void FitScaleToDefaultData  ()
    {
      // Update the actual scale settings by means of the default values
      _XYData.Set (_DefData);
      // Adjust the cursor position, if required
      AdjustCursor ();
    }

    /// <summary>
    /// Sets the XY scale.
    /// </summary>
    /// <param name="xmin">Left data point of the XY scale</param>
    /// <param name="xmax">Right data point of the XY scale</param>
    /// <param name="ymin">Bottom data point of the XY scale</param>
    /// <param name="ymax">Top data point of the XY scale</param>
    public void SetScale (float xmin, float xmax, float ymin, float ymax)
    {
      // Update the actual scale settings
      _XYData.Set (xmin, xmax, ymin, ymax);
      // Adjust the cursor position, if required
      AdjustCursor ();
    }

    /// <summary>
    ///  Sets the default scale.
    /// </summary>
    /// <param name="xmin">Left data point of the default scale</param>
    /// <param name="xmax">Right data point of the default scale</param>
    /// <param name="ymin">Bottom data point of the default scale</param>
    /// <param name="ymax">Top data point of the default scale</param>
    public void SetDefaultScale (float xmin, float xmax, float ymin, float ymax)
    {
      // Update the default scale settings
      _DefData.Set (xmin, xmax, ymin, ymax);
      // Adjust the cursor position, if required
      AdjustCursor ();
    }
    

    /// <summary>
    /// Adjusts the cursor position, if required
    /// </summary>
    public void AdjustCursor ()
    {
      if (!_XYData.Contains (_fXCursor, _fYCursor))
      {
        _fXCursor = (xMin+xMax)/2;
        _fYCursor = (yMin+yMax)/2;
      }
    }

    /// <summary>
    /// Selects the data arrays to be drawn.
    /// </summary>
    /// <param name="arScanToDraw">An array, that specifies, which data should be drawn</param>
    public void  SetDataToDraw (bool[] arDataToDraw)
    {
      // Hide all data graphs
      for (int i=0; i < _arDataToDraw.Length; i++)
        _arDataToDraw[i] = false;        
      // Specify the data graphs, that should be drawn
      int n = Math.Min (arDataToDraw.Length, _arDataToDraw.Length);
      Array.Copy (arDataToDraw, 0, _arDataToDraw, 0, n); 
    }

    #endregion methods

    #region properties

    /// <summary>
    /// The bitmap, the graphic is drawn into
    /// </summary>
    public Bitmap Graphic { get { return _bm; } }

    /// <summary>
    /// Begins of the marker windows
    /// </summary>
    public float[] XMarkBegin 
    { 
      get { return _fXMarkBegin; }
    }
    /// <summary>
    /// Widths of the marker windows
    /// </summary>
    public float[] XMarkWidth 
    { 
      get { return _fXMarkWidth; }
    }

    /// <summary>
    /// Indicates, whether cursor drawing is enabled (True) or not (False
    /// </summary>
    public bool EnableCursor 
    { 
      get { return _bEnableCursor; }
      set { _bEnableCursor = value; }
    }
    /// <summary>
    /// The abscissa of the cursor
    /// </summary>
    public float XCursor 
    { 
      get { return _fXCursor; }
      set { _fXCursor = value; }
    }
    /// <summary>
    /// The ordinate of the cursor
    /// </summary>
    public float YCursor 
    { 
      get { return _fYCursor; }
      set { _fYCursor = value; }
    }

    /// <summary>
    /// The left data point of the actual scale settings
    /// </summary>
    public float xMin { get { return _XYData.xMin; } }
    /// <summary>
    ///  The right data point of the actual scale settings 
    /// </summary>
    public float xMax { get { return _XYData.xMax; } }
    /// <summary>
    ///  The bottom data point of the actual scale settings 
    /// </summary>
    public float yMin { get { return _XYData.yMin; } }
    /// <summary>
    ///  The top data point of the actual scale settings
    /// </summary>
    public float yMax { get { return _XYData.yMax; } }

    #endregion properties

  }// E - GraphData

}
