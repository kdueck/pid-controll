using System;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;
using System.Drawing.Printing;

namespace App
{
	/// <summary>
	/// Class Vw:
	/// The view
	/// </summary>
	public class Vw
	{
		
    #region constants
    #endregion  // constants
    
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    internal Vw()
		{
    }
	
    #endregion // constructors
 
    #region members
  
    /// <summary>
    /// The max. number of scan results (calculated value on initial update)
    /// </summary>
    int m_nMaxScanResult = 0; 

    //---------------------------------------------------------------
    // Part: Graph panel
    
    /// <summary>
    /// The graphical data object
    /// </summary>
    GraphData _gd = new GraphData ();

    /// <summary>
    /// Zoom cursor
    /// </summary>
    Cursor _cuZoom = null;
    /// <summary>
    /// Normal ( current ) cursor
    /// </summary>
    Cursor _cuNormal = null;
    
    /// <summary>
    /// Zoom mode (On = True, Off = False)
    /// </summary>
    bool _bZoomMode = false;          
    /// <summary>
    /// Begin & End points of the zooming rectangle
    /// </summary>
    Point _ptZoomPosFirst, _ptZoomPosLast;               
    /// <summary>
    /// Min. zoom rectangle dim's (in pels)
    /// </summary>
    const int _nZoomMinDistance = 10;  
    /// <summary>
    /// Zooming bitmap
    /// </summary>
    Bitmap _bmZoom = null;   

    //---------------------------------------------------------------
    // Part: Result panel

    /// <summary>
    /// Orig. Font of a Result TB control (for "Conc. credible" case)
    /// </summary>
    Font _font_orig;

    #endregion // members
    
    #region properties
    
    //---------------------------------------------------------------
    // Part: Graph panel
    
    // Zooming rectangle
    private Rectangle _ZoomRectangle          
    {
      get
      {
        Rectangle rect = new Rectangle();
        if (_ptZoomPosFirst.X <= _ptZoomPosLast.X ) 
        {
          rect.X      = _ptZoomPosFirst.X;
          rect.Width  = _ptZoomPosLast.X - _ptZoomPosFirst.X;
        }
        else 
        {
          rect.X      = _ptZoomPosLast.X;
          rect.Width  = _ptZoomPosFirst.X - _ptZoomPosLast.X;

        }
        if (_ptZoomPosFirst.Y <= _ptZoomPosLast.Y ) 
        {
          rect.Y      = _ptZoomPosFirst.Y;
          rect.Height  = _ptZoomPosLast.Y - _ptZoomPosFirst.Y;
        }
        else 
        {
          rect.Y      = _ptZoomPosLast.Y;
          rect.Height  = _ptZoomPosFirst.Y - _ptZoomPosLast.Y;
        }
        return rect;
      }
    }

    public GraphData GraphData { get { return _gd; } }

    #endregion // properties
    
    #region methods
    
    //---------------------------------------------------------------
    // Common

    /// <summary>
    /// Initializes the view
    /// </summary>
    public void OnInitialUpdate() 
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      MainForm f = app.MainForm;

      // Get the control array lengths
      GetControlArrayLengths();

      /////////////////////
      // Part: Graph panel
      
      // Load zoom cursor
      _cuZoom = Common.LoadCursor ( "App.Images.Zooming.GraphicsZoom.cur" );

      // Update the grafics default scale
      // NOTE:
      //  Ordinate: Because a scan on receiving is scaled to % unit, the basic ordinate unit is %.
      //  Abscissa: Because a scan on receiving is given in pts unit, the basic abscissa unit is pts.
      float xmin = 0;                    // Left abscissa margin in pts
      float xmax = Doc.NSCANDATA - 1;    // Right abscissa margin in pts
      _gd.SetDefaultScale (xmin, xmax, -10, 110);  // Basic ordinate is in %: [ymin,ymax] = [0 - delta/10, 100 + delta/10], delta=(100 - 0)
      _gd.FitScaleToDefaultData ();

      // Update the graphics including axis and cursor labels    
      UpdateGraph ();

      // # of processed Scan points
      f.lblNoOfPts_Val.Text = "";

      // Hide 'Offline' information
      f.lblOffline.Visible = false;

      /////////////////////
      // Part: Result panel

      // Orig. Font of a Result TB control (for "Conc. credible" case)
      _font_orig = f.artxtRes[0].Font; 

      // Device info
      f.lblCurScr_Name.Text   = "";   // Script name
      f.lblDeviceNo_Val.Text  = "";   // Ger�te-Nr.
      f.lblTemp_Val.Text      = "";   // Temp.
      f.lblPres_Val.Text      = "";   // Pres.
      f.lblFlow_Val.Text      = "";   // Flow
      f.lblOffset_Val.Text    = "";   // Offset
      //    Errors
      f.lblErr_Val.BackColor    = SystemColors.Control;     // BC 
      f.lblErr_Val.ForeColor    = SystemColors.ControlText; // FC 
      f.lblErr_Val.Text         = "";                       // Text
      //    Warnings
      f.lblWar_Val.BackColor    = SystemColors.Control;     // BC 
      f.lblWar_Val.ForeColor    = SystemColors.ControlText; // FC 
      f.lblWar_Val.Text         = "";                       // Text
  
      // Result rows
      for ( int i=0; i < m_nMaxScanResult; i++ )
      {
        f.arlblRes[i].Text     = "Default";
        f.artxtRes[i].Text     = "100.0";
        f.arlblResUnit[i].Text = ConcUnits.ppm;
      }

      // Clear the 'PeakInfo' ListView items
      f.lvPeakInfo.Items.Clear ();
    }

    /// <summary>
    /// Gets the control array lengths & initializes the corresponding members
    /// </summary>
    void GetControlArrayLengths ()
    {
      App app = App.Instance;
      MainForm f = app.MainForm;
      
      // -----------------------------
      // Result display
      
      // Get the lengths of the MainForm result control arrays
      int n1 = f.arlblRes.Length;
      int n2 = f.artxtRes.Length;
      int n3 = f.arlblResUnit.Length;
      int n4 = f.archkRes.Length;
      // Compare them: 'bError' is True, if the result control arrays are of different length 
      bool bError = (n1 != n2) || (n2 != n3) || (n3 != n4);
      Debug.Assert ( false == bError );
      if (bError)
      {
        throw new System.ApplicationException ();
      }
      // Max. number of scan results
      m_nMaxScanResult = n1;
             
    }

    /// <summary>
    /// Updates the view
    /// </summary>
    public void OnUpdate() 
    {
      try 
      {
        /////////////////////
        // Part: Graph panel
        
        // Update the graphics including axis and cursor labels     
        UpdateGraph ();

        // # of processed Scan points
        App app = App.Instance;
        MainForm f = app.MainForm;
        f.lblNoOfPts_Val.Text = app.Doc.nPoints.ToString ();

        /////////////////////
        // Part: Result panel

        // Update result panel
        UpdateResultPanel ();
        // Update the peak information
        UpdatePeakInfo ();

      }
      catch (System.Exception exc)
      {
        Debug.WriteLine ("OnUpdate->" + exc.Message);
      }

    }

    //---------------------------------------------------------------
    // Graph panel

    
    /// <summary>
    /// Updates the graphics including axis and cursor labels
    /// </summary>
    public void UpdateGraph ()
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      MainForm f = app.MainForm;
     
      // Allocate & Fill the data array, based on the scan arrays to be drawn
      float[][] arY = new float [2][];        // 2 data arrays
      ScanData scn = doc.arScanData[0];
      arY[0] = scn.ToFloatArray ();           // The 1. data array: raw scan
      ScanData scn_sm = doc.arScanData[1];
      arY[1] = scn_sm.ToFloatArray ();        // The 2. data array: smoothed scan
      
      // Set the left & right abscissa margins
      // NOTE:
      //  The spacing interval (0.01 here) should be assigned as a constant in the document class. 
      float xa = 0;
      float xe = (arY[0].Length == 0) ? xa : (float)(arY[0].Length-1);
      // Attach the data
      if (_gd.Attach (arY, xa, xe))
      {
        // Attaching successful:

        // Indicate the windows to be marked
        for (int i=0; i < Doc.MAX_SCAN_RESULT; i++)
        {
          if (doc.nMarkedWindow[i])
          {
            try
            {
              // The window should be marked:
              // Get the scan result, corr'ing to the window
              ScanResult res = doc.arScanResult[i];
              // Update PlotData: Marked window begin & width
              // Notes: 
              //  The unit of the window begin & width members of a scan result is pts. For grafical
              //  representation they have NOT to be converted.
              _gd.XMarkBegin[i] = (float) res.dwBeginTime;
              _gd.XMarkWidth[i] = (float) res.dwWidthTime;
            }
            catch
            {
              // The window should NOT be marked:
              _gd.XMarkBegin[i] = -1;
              _gd.XMarkWidth[i] = -1;
            }
          }
          else
          {
            // The window should NOT be marked:
            _gd.XMarkBegin[i] = -1;
            _gd.XMarkWidth[i] = -1;
          }
        }
        
        // Select the data arrays to be drawn
        _gd.SetDataToDraw (doc.arScanToDraw);
        
        // Draw the scan
        Bitmap bm = _gd.Draw (f.pbDraw.Size);
        if (null != bm)
        {
          f.pbDraw.Image = bm;
        }
      }
      // Update axis and cursor labels
      //    X
      switch (app.Data.Program.eXUnit)
      {
        case AppData.ProgramData.XUnits.Points:   f.lblXunit.Text = "[pts]"; break;
        case AppData.ProgramData.XUnits.Seconds:  f.lblXunit.Text = "[s]"; break;
      }
      float fX = _ConvertAccordingToXUnit (_gd.xMin);
      f.lblXmin.Text = fX.ToString ("F2");
      fX = _ConvertAccordingToXUnit (_gd.xMax);
      f.lblXmax.Text = fX.ToString ("F2");
      fX = _ConvertAccordingToXUnit (_gd.XCursor);
      f.lblXcur.Text = fX.ToString ("F2");
      //    Y
      switch (app.Data.Program.eYUnit)
      {
        case AppData.ProgramData.YUnits.Percent:  f.lblYunit.Text = "[%]"; break;
        case AppData.ProgramData.YUnits.Adc:      f.lblYunit.Text = "[adc]"; break;
        case AppData.ProgramData.YUnits.mV:       f.lblYunit.Text = "[mV]"; break;
      }
      float fY = _ConvertAccordingToYUnit (_gd.yMin);
      f.lblYmin.Text = fY.ToString ("F2");
      fY = _ConvertAccordingToYUnit (_gd.yMax);
      f.lblYmax.Text = fY.ToString ("F2");
      fY = _ConvertAccordingToYUnit (_gd.YCursor);
      f.lblYcur.Text = fY.ToString ("F2");
    }

    /// <summary>
    /// Converts ordinate values according to the current Y unit selected.
    /// </summary>
    /// <param name="fVal">An ordinate value</param>
    /// <returns>The converted ordinate value</returns>
    /// <remarks>
    /// Because a scan on receiving is scaled to % unit, the basic ordinate unit is %.
    /// Thats why the ordinate value parameter is always based on the percent scale.
    /// </remarks>
    float _ConvertAccordingToYUnit (float fVal)
    {
      float fScal = 1.0f;

      App app = App.Instance;
      switch (app.Data.Program.eYUnit)
      {
        case AppData.ProgramData.YUnits.Percent:    // % units
          fScal = 1.0f;
          break;
        case AppData.ProgramData.YUnits.Adc:        // ADC units
          // Assumption: 
          // 2^15 (adc) entspr. 100 (%)
          // -> p(%) / 100(%) = x(adc) / 2^15(adc) 
          // -> x(adc) = (2^15/100) * p(%) = fScal * p(%)
          fScal = (float) Doc.ADC_RESOLUTION / 100;
          break;
        case AppData.ProgramData.YUnits.mV:         // mV units
          // TODO
          // (for that reason the 'mv' radiobutton in the properties dialog (tabpage 'Program', item Ordinate scaling) has been hidden ...) 
          break;
      }
      return fScal*fVal;
    }

    /// <summary>
    /// Converts abscissa values according to the current X unit selected.
    /// </summary>
    /// <param name="fVal">An abscissa value</param>
    /// <returns>The converted abscissa value</returns>
    /// <remarks>
    /// Because a scan on receiving is given in pts unit, the basic abscissa unit is pts.
    /// Thats why the abscissa value parameter is always based on the pts scale.
    /// 
    /// Relation between the 2 scales:
    ///   0           S             N                   [sec]
    ///   |-----------|-------------|
    ///   0           P             2047                [pts]
    ///   
    ///   S = ( N / 2047 ) * P = fScal * P
    ///   with: N - meas. cycle (in sec)
    ///         2047 - # of scan elements minus 1
    /// </remarks>
    float _ConvertAccordingToXUnit (float fVal)
    {
      float fScal = 1.0f;

      App app = App.Instance;
      Doc doc = app.Doc;
      switch (app.Data.Program.eXUnit)
      {
        case AppData.ProgramData.XUnits.Points:     // [pts] units
          fScal = 1.0f;
          break;
        case AppData.ProgramData.XUnits.Seconds:    // [s] units
          fScal = (float)((doc.ScanParams.wMeasCycle/10.0)/(Doc.NSCANDATA-1)); // Relation see above
          break;
      }
      return fScal*fVal;
    }

    /// <summary>
    /// Updates the peak information.
    /// </summary>
    void UpdatePeakInfo ()
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      MainForm f = app.MainForm;

      try
      {
        string sUnit = "";

        // Column header: Position
        switch (app.Data.Program.eXUnit)
        {
          case AppData.ProgramData.XUnits.Points:   sUnit = "[pts]"; break;
          case AppData.ProgramData.XUnits.Seconds:  sUnit = "[s]"; break;
        }
        string sText = f.colAbscissa.Text;
        int idx = sText.IndexOf (" ");
        if (-1 != idx) 
        {
          sText = sText.Substring (0, idx) + " " + sUnit;
          f.colAbscissa.Text = sText;
        }

        // Column header: Height
        switch (app.Data.Program.eYUnit)
        {
          case AppData.ProgramData.YUnits.Percent:  sUnit = "[%]"; break;
          case AppData.ProgramData.YUnits.Adc:      sUnit = "[adc]"; break;
          case AppData.ProgramData.YUnits.mV:       sUnit = "[mV]"; break;
        }
        sText = f.colHeight.Text;
        idx = sText.IndexOf (" ");
        if (-1 != idx) 
        {
          sText = sText.Substring (0, idx) + " " + sUnit;
          f.colHeight.Text = sText;
        }
        
        // Items:
        System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
        // The current peak information
        PeakInfo pi = doc.PeakInfo;
        
        // Clear the 'PeakInfo' ListView items
        f.lvPeakInfo.Items.Clear ();
        // Fill the 'PeakInfo' ListView
        foreach (PeakInfoItem item in pi.Items)
        {
          // Create a new ListViewItem
          ListViewItem lvi = new ListViewItem ();
          lvi.UseItemStyleForSubItems = false;
          
          // ------------------------------------
          // The info regarding the current peak:
          
          //    1. subitem: Position
          idx = item.Position;                        // The index of the current peak
          float fPos = _ConvertAccordingToXUnit (idx);
          lvi.Text = fPos.ToString ("F2");               
          
          //    2. subitem: Height
          float fHeight = item.Height;                // The height of the current peak (in %)
          fHeight = _ConvertAccordingToYUnit (fHeight);
          lvi.SubItems.Add (fHeight.ToString ("F2", nfi));   

          //    3. subitem: Area (unit: [adc*pts])
          string sArea;
          if (item.Invalid == 1)                      // The current peak is invalid: 
            sArea = "OVERRANGE";                      //    Overrange info
          else                                        // The current peak is OK:           
            sArea = item.Area.ToString ("F0", nfi);   //    The area of the current peak
          lvi.SubItems.Add (sArea);
          
          //       If the current peak is invalid, show the corr'ing info in bold           
          if (item.Invalid == 1)
          {
            lvi.SubItems[2].Font = new Font(lvi.SubItems[2].Font, lvi.SubItems[2].Font.Style | FontStyle.Bold);
          }

          // Add the ListViewItem to our ListView control
          f.lvPeakInfo.Items.Add (lvi);
        }
      }
      catch
      {
      }
    }


    //---------------------------------------------------------------
    // Result panel
    
    /// <summary>
    /// Updates the result panel
    /// </summary>
    public void UpdateResultPanel ()
    {
      UpdateResultData ();
    }
    
    /// <summary>
    /// Updates the result data
    /// </summary>
    /// <remarks>
    /// Concerning the result TextBox controls:
    /// In order to distinguish the different result forecolors, the following comb's were proofed:
    /// #   ReadOnly  Enabled   White BG via code   Remarks
    /// 
    /// 1   F         F         yes                 forecolors not distinguishable, white BG, edit impossible
    /// 2   T         T         yes                 forecolors distinguishable, white BG, edit impossible
    /// 3   T         F         yes                 forecolors not distinguishable, white BG, edit impossible
    /// 4   F         T         yes                 forecolors distinguishable, white BG, edit possible
    /// 
    /// 5   F         F         no                  forecolors not distinguishable, BG as panel, edit impossible
    /// 6   T         T         no                  forecolors not distinguishable, BG as panel, edit impossible
    /// 7   T         F         no                  forecolors not distinguishable, BG as panel, edit impossible
    /// 8   F         T         no                  forecolors distinguishable, white BG, edit possible
    /// 
    /// In evaluation var. 2 is choosen.
    /// </remarks>
    void UpdateResultData ()
    {
      try 
      {
        App app = App.Instance;
        Doc doc = app.Doc;
        MainForm f = app.MainForm;
        Ressources r = app.Ressources;

        // ------------------------------
        // Section 'Results'
        
        // Get the number of scan results
        int nMaxWnd = doc.arScanResult.Count;
        bool bAlarmBeep = false;

        // Bold font of a Result TB control (for "Conc. incredible" case) 
        Font font_bold = new Font (_font_orig, FontStyle.Bold);

        // Loop over the max. possible number of scan results
        for ( int i=0; i < m_nMaxScanResult; i++ )
        {
          // Do we need the current result row?
          if ( i < nMaxWnd )
          {
            // Yes, the current result row is required:

            // Show the result row
            f.arlblRes[i].Visible = true;
            f.artxtRes[i].Visible = true;
            f.arlblResUnit[i].Visible = true;
            f.archkRes[i].Visible = true;

            // Get the current SR
            ScanResult res = doc.arScanResult[i];

            // Display the substance name
            f.arlblRes[i].Text = res.sSubstance;
      
            // Display the conc. unit
            f.arlblResUnit[i].Text = res.sConcUnit;

            // Set the alarm switch in case of substance alarm
            // (Only, if a signal processing has been triggered)
            // Notes:
            //  In order to show the alarm button, the following conditions must be fulfilled:
            //  1. The substance result must exceed its HI alarm level AND
            //  2a. A signal processing must have been performed on the device over a scan with non-zero length OR
            //  2b. The scan must be complete
            bool bSigProcTriggered = (doc.ScanInfo.wCtrlIO & 0x4000 ) != 0;  
            bool bShowAlarm = false;
            if ( res.dResult >= res.dA2 )                                               // 1.
            {
              if      ((doc.nPoints > 0) && bSigProcTriggered)  bShowAlarm = true;      // 2a.
              else if (doc.nPoints == Doc.NSCANDATA)            bShowAlarm = true;      // 2b.
            }
            if (bShowAlarm)
            {
              doc.bResultAlarm = true;
              bAlarmBeep = true;
            }

            // B - Test
            System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
            string sTest = "Pts: " + doc.nPoints.ToString () + ", Trig: " + bSigProcTriggered.ToString (); 
            string sTestData = ", Res: " + res.dResult.ToString ("F3", nfi) + ", A2: " + res.dA2.ToString ("F3", nfi);
            sTest += sTestData;
            Debug.WriteLine (sTest);
            // E - Test

            // Choose the colors of the result TB, depending on the alarm case
            // CD: Is the substance in non- / LO / HI alarm condition?
            if      ( res.dResult < res.dA1 )
            {
              // No alarm (BG: lightgray, FG: black) 
              f.artxtRes[i].BackColor = Color.LightGray;    
              f.artxtRes[i].ForeColor = Color.Black;
            }
            else if ( res.dResult < res.dA2 )
            {
              // LO alarm (BG: yellow, FG: black) 
              f.artxtRes[i].BackColor = Color.Yellow;    
              f.artxtRes[i].ForeColor = Color.Black;
            }
            else
            {
              // HI alarm (BG: red, FG: white) 
              f.artxtRes[i].BackColor = Color.Red;    
              f.artxtRes[i].ForeColor = Color.White;
            }

            // Display the substance result
            // (Take into account the fact, that the concentration might be NOT credibly.)
            int nPostDigits = ConcUnits.GetDecimals (res.sConcUnit);
            string sResult = Common.ConvertToScience ( res.dResult, nPostDigits );
            if (res.byConcIncredible == 1) 
            {
              // Conc. incredible
              sResult = "> " + sResult;                               // Insert the > sign
              f.artxtRes[i].Font = font_bold;                         // Assign the bold font to the TB
            }
            else
            {
              // Conc. OK
              f.artxtRes[i].Font = _font_orig;                        // Assign the orig. font to the TB
            }
            f.artxtRes[i].Text = sResult;
          }
          else
          {
            // No, the current result row is NOT required:

            // Hide the result row
            f.arlblRes[i].Visible = false;
            f.artxtRes[i].Visible = false;
            f.arlblResUnit[i].Visible = false;
            f.archkRes[i].Visible = false;
          }
        }

        // ------------------------------
        // Section 'Device information'
        
        f.lblCurScr_Name.Text = doc.sCurrentScript;                               // Script name
        DeviceInfo inf = doc.DeviceInfo;
        f.lblTemp_Val.Text    = Common.ConvertToScience ( inf.dTemperature, 1 );  // Temp.
        if (inf.nPresDispOnOff == 1)                                              // Pressure display on:
          f.lblPres_Val.Text  = Common.ConvertToScience( inf.dPressure, 1);       //    Pres.
        else                                                                      // Pressure display off:
          f.lblPres_Val.Text  = r.GetString( "Div_NotConnected" );                //    n.c.
        string sFlow = string.Format ("{0}, {1}, {2}", inf.nFlow1, Common.ConvertToScience (inf.dFlow2, 1), inf.nFlow3);
        f.lblFlow_Val.Text    = sFlow;                                            // Flows
        f.lblOffset_Val.Text  = inf.nOffset.ToString ();                          // Offset
        UpdateDI_ErrWar ();                                                       // Error/Warning

        // Beep in case of substance alarm
        if ( bAlarmBeep ) Win.MessageBeep ( Win.BeepType.SimpleBeep );
      }
      catch (System.Exception exc)
      {
        string s = "UpdateResultData() failed: " + exc.Message;
        Debug.WriteLine (s);
      }
    }

    /// <summary>
    /// Clears the result panel
    /// </summary>
    public void ClearResultPanel ()
    {
      App app = App.Instance;
      MainForm f = app.MainForm;

      // Loop over the max. possible number of scan results
      for ( int i=0; i < m_nMaxScanResult; i++ )
      {
        // Hide the current result row
        f.arlblRes[i].Visible = false;
        f.artxtRes[i].Visible = false;
        f.arlblResUnit[i].Visible = false;
        f.archkRes[i].Visible = false;
        // Uncheck its 'Show substance region' CheckBox
        f.archkRes[i].Checked = false;
      }
    }

    /// <summary>
    /// Updates the result data - DeviceInfo / Error/Warning
    /// </summary>
    public void UpdateDI_ErrWar ()
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      MainForm f = app.MainForm;
      Ressources r = app.Ressources;

      // Fehler
      Color tc, bc;
      string sErrWar = doc.ScanInfo.ErrorAsString ();
      if (sErrWar.Length == 0)
      {
        tc = SystemColors.ControlText;
        bc = SystemColors.Control;
        sErrWar = r.GetString ( "cView_DeviceInfo_ErrWar_None" );               // "Keine"
      }
      else
      {
        tc = Color.White;
        bc = Color.Red;
      }
      f.lblErr_Val.BackColor    = bc;       // BC 
      f.lblErr_Val.ForeColor    = tc;       // FC 
      f.lblErr_Val.Text         = sErrWar;  // Text
      // Warnungen
      sErrWar = doc.ScanInfo.WarningAsString ();
      if (sErrWar.Length == 0)
      {
        tc = SystemColors.ControlText;
        bc = SystemColors.Control;
        sErrWar = r.GetString ( "cView_DeviceInfo_ErrWar_None" );               // "Keine"
      }
      else
      {
        tc = Color.Black;
        bc = Color.Yellow;
      }
      f.lblWar_Val.BackColor    = bc;       // BC 
      f.lblWar_Val.ForeColor    = tc;       // FC 
      f.lblWar_Val.Text         = sErrWar;  // Text
    }


    //---------------------------------------------------------------
    // Printing

    /// <summary>
    /// Draws the header to the page                                
    /// </summary>
    /// <param name="e">The PrintPage event arguments</param>
    /// <param name="rDraw">The drawing area</param>
    void _OnPrintHeader ( PrintPageEventArgs e, Rectangle rDraw )
    { 
      App app = App.Instance;
      Doc doc = app.Doc;
      Ressources res = app.Ressources;
      
      Graphics g = e.Graphics;
      
      // 1. part: Title
      Font fntTitle = new Font ( "Times New Roman", 28, FontStyle.Bold );  
      string s = res.GetString ( "cView_PrintHeader_Title" );     // "Photo Ionisation Detector PID"
      Size si = g.MeasureString ( s, fntTitle ).ToSize (); 
      si.Width += 2; si.Height += 2;
      
      Rectangle rCurr = rDraw;
      rCurr.Height = si.Height;

      int x = rCurr.Left + (rCurr.Width - si.Width)/2;
      int y = rCurr.Top;
      Rectangle r = new Rectangle ( x, y, si.Width, si.Height );
      Brush brush = new SolidBrush ( Color.Black );
      g.DrawString ( s, fntTitle, brush, r );

      // 2. part: Info: Datum, Version Ger�te-SW, Version PC-SW
      Font fntNormal = new Font ( "Courier", 10, FontStyle.Regular );  
      string fmt = res.GetString ( "cView_PrintHeader_Info" );    // "Datum: {0}, Version Ger�t: {1}, Version PC: {2}"
      //    Get the product version associated with this application
      //    Notes: 
      //      The corr'ing AssemblyInfo entries are used.
      string sVersion = Application.ProductVersion;
      //    Get the Major, Minor & Build numbers of the product version
      string[] ars = sVersion.Split ('.');
      try 
      {
        sVersion = string.Format ("{0}.{1}.{2}", ars[0], ars[1], ars[2]);
      }
      catch {}
      s = string.Format ( fmt, DateTime.Now.ToString ( "dd.MM.yyyy HH:mm" ), app.Doc.sVersion, sVersion);
      si = g.MeasureString ( s, fntNormal ).ToSize (); 
      si.Width += 2; si.Height += 2;
      
      rCurr.Offset (0, rCurr.Height);
      rCurr.Height = si.Height;
            
      x = rCurr.Left + (rCurr.Width - si.Width)/2;
      y = rCurr.Top; 
      r = new Rectangle ( x, y, si.Width, si.Height );
      g.DrawString ( s, fntNormal, brush, r );

      // 3. part: Comment
      fntNormal = new Font ( "Courier", 10, FontStyle.Regular );  
      s = "Comment: " + doc.sComment;
      si = g.MeasureString ( s, fntNormal ).ToSize (); 
      si.Width += 2; si.Height += 2;

      rCurr.Offset (0, rCurr.Height);     // y given
      rCurr.X += 20;
      rCurr.Width -= 2 * 20;
      rCurr.Height = rDraw.Height - Math.Abs(rDraw.Top - rCurr.Top);
      
      g.DrawString ( s, fntNormal, brush, rCurr );
    }

    /// <summary>
    ///  Draws the data to the page
    /// </summary>
    /// <param name="e">The PrintPage event arguments</param>
    /// <param name="rDraw">The drawing area</param>
    void _OnPrintData ( PrintPageEventArgs e, Rectangle rDraw )
    {
      Graphics g = e.Graphics;
      rDraw.Inflate( -5, -5 );

      // Get the data
      App app = App.Instance;
      Doc doc = app.Doc;

      double dXMin = _ConvertAccordingToXUnit (_gd.xMin);
      double dXMax = _ConvertAccordingToXUnit (_gd.xMax);
      double dYMin = _ConvertAccordingToYUnit (_gd.yMin);
      double dYMax = _ConvertAccordingToYUnit (_gd.yMax);
      string sXunit = ""; 
      switch (app.Data.Program.eXUnit)
      {
        case AppData.ProgramData.XUnits.Points:   sXunit = "[pts]"; break;
        case AppData.ProgramData.XUnits.Seconds:  sXunit = "[s]"; break;
      }
      string sYunit = ""; 
      switch (app.Data.Program.eYUnit)
      {
        case AppData.ProgramData.YUnits.Percent:  sYunit = "[%]"; break;
        case AppData.ProgramData.YUnits.Adc:      sYunit = "[adc]"; break;
        case AppData.ProgramData.YUnits.mV:       sYunit = "[mV]"; break;
      }

      // Set the font, brush & pen
      Font fntNormal = new Font ( "Courier", 10, FontStyle.Regular );  
      Brush brush = new SolidBrush ( Color.Black );
      Pen pen = new Pen ( Color.Black );
      
      // Triple point ( point of origin )
      //    yMax width
      Size si = g.MeasureString ( Common.ConvertToScience ( dYMax, 2 ), fntNormal ).ToSize ();
      int cw = si.Width;
      int ch = si.Height;
      //    yMin width
      si = g.MeasureString ( Common.ConvertToScience ( dYMin, 2 ), fntNormal ).ToSize ();
      if ( si.Width > cw ) cw = si.Width;
      if ( si.Height > ch ) ch = si.Height;
      //    yUnit width
      si = g.MeasureString (sYunit, fntNormal ).ToSize ();
      if ( si.Width > cw ) cw = si.Width;
      if ( si.Height > ch ) ch = si.Height;
      //    over-all
      cw += 2;
      ch += 2;

      // Spacing marker length
      int nMarkerLength = ch/2;

      // Coordinate area ( drawing area )
      int x0 = rDraw.Left + cw + nMarkerLength/2;
      int y0 = rDraw.Bottom - 2*ch;
      si = g.MeasureString ( Common.ConvertToScience ( dXMax, 2 ), fntNormal ).ToSize ();
      int x1 = rDraw.Right - (si.Width+2)/2;
      int y1 = rDraw.Top + ch/2;

      // Draw the axis
      g.DrawLine ( pen, x0, y0, x1, y0 ); // horizontal
      g.DrawLine ( pen, x0, y0, x0, y1 ); // vertical

      // Draw the data
      // Notes:
      //  Here clipping is used in order to realize drawing only within the provided area.
      Rectangle r = new Rectangle ( x0, y1, x1-x0, y0-y1 );
      Region regClipOrig = g.Clip;    // Remember the current clipping region of the Graphics object
      Rectangle rClip = r;            // Define the clipping rectangle for data drawing
      rClip.Inflate (-1, -1);
      g.Clip = new Region (rClip);    // Adjust the new clipping region for data drawing
      _gd.DrawData ( g, r );          // Draw the data
      g.Clip = regClipOrig;           // Restore the original clipping region of the Graphics object

      // Draw the axis segments ( spacing )
      double dXRange = dXMax - dXMin;                 // the x range to be represented
      double dYRange = dYMax - dYMin;                 // the y range to be represented
      int x1m = x1;                                   // Init.: value of the last x marker 
      int y1m = y1;                                   // Init.: value of the last y marker 
      double dXEnd = dXMax;                           // Init.: Value corr'ing to the last x marker 
      double dYEnd = dYMax;                           // Init.: Value corr'ing to the last y marker 
      if ( !((dXRange <= 0) || (dYRange <= 0)) )
      {
        // ----------------------------
        // Spacing in X

        // Determine the initial (power-of-10) segment that matches best the X range: 'dXUnit' 
        double dXExp   = Math.Log10 ( dXRange );                  
        int    nXSign  = (int) ( dXExp / Math.Abs ( dXExp ) );
        int    nXExp   = (int) ( Math.Abs ( dXExp ) + 0.5 );
        double dXUnit  = Math.Pow ( 10, nXExp*nXSign );           
        // Determine the final spacing segment for the X range: 'dXUnit'    
        double dXNum = dXRange / dXUnit;
        if      ( dXNum <=  1.0 ) dXUnit /= 20;
        else if ( dXNum <=  2.0 ) dXUnit /= 10;
        else if ( dXNum <=  5.0 ) dXUnit /= 5;
        else if ( dXNum <= 10.0 ) dXUnit /= 2;
        // Determine the 1st X value, where a marker should be drawn
        // (i.e. the value corr'ing to the 1st X marker): 'dXBegin' 
        double dXBegin = ((int)(dXMin/dXUnit))*dXUnit;
        bool bXAdd;  
        if (dXBegin == dXMin) bXAdd = true;   // Begin marker drawing with the next marker value 
        else
        {
          if (dXMin >= 0)     bXAdd = true;   // Begin marker drawing with the next marker value
          else                bXAdd = false;  // Begin marker drawing with this marker value
        }
        if (bXAdd) dXBegin += dXUnit;
        // Init.: Value corr'ing to the last X marker  
        dXEnd = dXBegin;   
        // Calculate the pels, corr'ing to the 1st X marker position: 'dXPoint'
        // Notes:
        //  r.Left              dXPoint            r.Right        pel
        //  |-------------------|------------------------|
        //  xMin                dXBegin               xMax        data                
        double dXPoint = r.Left + r.Width*(dXBegin-dXMin)/dXRange;
        // Calculate the pels delta between two successive X markers: 'dXStep'
        double dXStep  = r.Width*dXUnit/dXRange;
        // Draw the X markers
        while ( (int)dXPoint <= r.Right )
        {
          // Draw the current X marker
          g.DrawLine ( pen, (int)dXPoint, r.Bottom - nMarkerLength/2, (int)dXPoint, r.Bottom + nMarkerLength/2 ); 
          // Initiate the next X marker
          dXPoint += dXStep;
          dXEnd += dXUnit;
        }
        // Get the data, corr'ing to the lastly drawn X marker
        x1m = (int)(dXPoint - dXStep);  // Its position (pels)
        dXEnd -= dXUnit;                // Its corr'ing value

        // ----------------------------
        // Spacing in y

        // Determine the initial (power-of-10) segment that matches best the Y range: 'dYUnit' 
        double dYExp   = Math.Log10 ( dYRange );
        int    nYSign  = (int) ( dYExp / Math.Abs ( dYExp ) );
        int    nYExp   = (int) ( Math.Abs ( dYExp ) + 0.5 );
        double dYUnit  = Math.Pow ( 10, nYExp*nYSign );
        // Determine the final spacing segment for the Y range: 'dYUnit'    
        double dYNum = dYRange / dYUnit;
        if      ( dYNum <=  1.0 ) dYUnit /= 20;
        else if ( dYNum <=  2.0 ) dYUnit /= 10;
        else if ( dYNum <=  5.0 ) dYUnit /= 5;
        else if ( dYNum <= 10.0 ) dYUnit /= 2;
        // Determine the 1st Y value, where a marker should be drawn
        // (i.e. the value corr'ing to the 1st Y marker): 'dYBegin' 
        double dYBegin = ((int)(dYMin/dYUnit))*dYUnit;
        bool bYAdd;  
        if (dYBegin == dYMin) bYAdd = true;   // Begin marker drawing with the next marker value 
        else
        {
          if (dYMin >= 0)   bYAdd = true;     // Begin marker drawing with the next marker value 
          else              bYAdd = false;    // Begin marker drawing with this marker value
        }
        if (bYAdd) dYBegin += dYUnit;
        // Init.: Value corr'ing to the last Y marker  
        dYEnd = dYBegin;   
        // Calculate the pels, corr'ing to the 1st Y marker position: 'dYPoint'
        // Notes:
        //  r.Bottom            dYPoint              r.Top        pel
        //  |-------------------|------------------------|
        //  yMin                dYBegin               yMax        data                
        double dYPoint = r.Bottom - r.Height*(dYBegin-dYMin)/dYRange;
        // Calculate the pels delta between two successive Y markers: 'dYStep'
        double dYStep  = r.Height*dYUnit/dYRange;
        // Draw the Y markers
        while ( (int)dYPoint >= r.Top )
        {
          // Draw the current Y marker
          g.DrawLine ( pen, r.Left - nMarkerLength/2, (int)dYPoint, r.Left + nMarkerLength/2, (int)dYPoint ); 
          // Initiate the next Y marker
          dYPoint -= dYStep;
          dYEnd += dYUnit;
        }
        // Get the data, corr'ing to the lastly drawn Y marker
        y1m = (int)(dYPoint + dYStep);  // Its position (pels) 
        dYEnd -= dYUnit;                // Its corr'ing value
      }
      
      // Draw the axis values:
      
      // X: 
      //    xMin
      string sValue = Common.ConvertToScience ( dXMin, 2 );
      si = g.MeasureString ( sValue, fntNormal ).ToSize ();
      si.Width += 2;
      r = new Rectangle ( x0-si.Width/2, y0+ch, si.Width, ch );
      g.DrawString ( sValue, fntNormal, brush, r );
      //    xMax
      sValue = Common.ConvertToScience ( dXEnd, 2 );
      si = g.MeasureString ( sValue, fntNormal ).ToSize ();
      si.Width += 2;
      r = new Rectangle ( x1m-si.Width/2, y0+ch, si.Width, ch );
      g.DrawString ( sValue, fntNormal, brush, r );
      //    xUnit
      si = g.MeasureString ( sXunit, fntNormal ).ToSize ();
      si.Width += 2;
      r = new Rectangle ( (x0+x1m-si.Width)/2, y0+ch, si.Width, ch );
      g.DrawString ( sXunit, fntNormal, brush, r );

      // Y:
      //    Text alignment:  right align
      StringFormat fmt = new StringFormat ();
      fmt.Alignment = StringAlignment.Far;                     
      //    yMax
      sValue = Common.ConvertToScience ( dYEnd, 2 );    
      r = new Rectangle ( rDraw.Left, y1m-ch/2, cw, ch );
      g.DrawString ( sValue, fntNormal, brush, r, fmt );
      //    yMin
      sValue = Common.ConvertToScience ( dYMin, 2 );    
      r = new Rectangle ( rDraw.Left, y0-ch/2, cw, ch );
      g.DrawString ( sValue, fntNormal, brush, r, fmt );
      //    yUnit
      r = new Rectangle ( rDraw.Left, (y0+y1m-ch)/2, cw, ch );
      g.DrawString ( sYunit, fntNormal, brush, r, fmt );
    }  
    
    /// <summary>
    /// Draws the params to the page
    /// </summary>
    /// <param name="e">The PrintPage event arguments</param>
    /// <param name="rDraw">The drawing area</param>
    void _OnPrintParams ( PrintPageEventArgs e, Rectangle rDraw )
    {
      Graphics g = e.Graphics;
      rDraw.Inflate( -5, -5 );

      App app = App.Instance;
      Doc doc = app.Doc;
      Ressources r = app.Ressources;

      DeviceInfo       inf = doc.DeviceInfo;
      ScanResultArray  res = doc.arScanResult;

      Font fntNormal = new Font ( "Courier", 10, FontStyle.Regular );  
      Font fntBold = new Font ( "Courier", 10, FontStyle.Bold );  
      Brush brush = new SolidBrush ( Color.Black );
      Brush brushAlarm = new SolidBrush ( Color.Red );
      Pen pen = new Pen (Color.Black);
      
      // Init's
      //    Widths & heights
      //    NOTE:
      //    There is a Bug in the 'MeasureString' routine: 
      //    It reports a width of 4 (with the given font)for each Multi-Space string (i.e. a string
      //    of kind " ", "  ", "   ", "      ", ... ). Thats why: In order to get the width of 
      //    3 Spaces, we measure the width of 1 Space & multiply it by the required factor.
      Size si = g.MeasureString ("0", fntNormal).ToSize ();   // Height of a row
      int cy = si.Height;
      si = g.MeasureString (" ", fntNormal).ToSize ();        // Horiz. distance of the separator line from the margins 
      int cxSepMargin = 3*si.Width;
      //    Coord's
      int x = rDraw.Left;                                     // TL - x
      int y = rDraw.Top;                                      // TL - y 
      
      int xInfCol2 = x + (1*rDraw.Width)/2;                   // 2. column for Info output
      
      int xResCol2 = x + (1*rDraw.Width)/3;                   // 2. column for Results output
      int xResCol3 = x + (3*rDraw.Width)/6;                   // 3. column for Results output
      int xResCol4 = x + (4*rDraw.Width)/6;                   // 4. column for Results output
      int xResCol5 = x + (5*rDraw.Width)/6;                   // 5. column for Results output
      
      //-----------------------
      // Output

      // Part: DeviceInfo 

      // 1. line: "Device adjustments" (BOLD)
      string s = r.GetString ( "cView_PrintParams_DeviceInfo" );  // "Ger�teeinstellungen"
      g.DrawString ( s, fntBold, brush, x, y );
      // 2. line: Temperature & Script name      
      y += cy;
      s = string.Format ( "   {0}: {1}", 
        r.GetString ( "cView_PrintParams_DeviceInfo_Temp" ),      // "Temperatur [�C]"
        Common.ConvertToScience ( inf.dTemperature, 1 )
        );
      g.DrawString ( s, fntNormal, brush, x, y );
      s = string.Format ( "{0}: {1}",
        r.GetString ( "cView_PrintParams_DeviceInfo_Script" ),    // "Script"
        doc.sCurrentScript
        );
      g.DrawString ( s, fntNormal, brush, xInfCol2, y );
      // 3. line: Pressure & offset
      y += cy;
      string sPres = (inf.nPresDispOnOff == 1) ? Common.ConvertToScience( inf.dPressure, 1) : r.GetString( "Div_NotConnected" );
      s = string.Format ( "   {0}: {1}", 
        r.GetString ( "cView_PrintParams_DeviceInfo_Pres" ),      // "Pressure [kPa]"
        sPres
        );
      g.DrawString ( s, fntNormal, brush, x, y );
      s = string.Format ( "{0}: {1}",
        r.GetString ( "cView_PrintParams_DeviceInfo_Offset" ),    // "Offset [adc]"
        inf.nOffset.ToString ()
        );
      g.DrawString ( s, fntNormal, brush, xInfCol2, y );
      // 4. line: Flow & Battery
      y += cy;
      if (app.Data.Program.bDisplayFlow)
      {
        s = string.Format ( "   {0}: {1}, {2}, {3}", 
          r.GetString ( "cView_PrintParams_DeviceInfo_Flow" ),      // "Flow [ml/min]"
          inf.nFlow1, Common.ConvertToScience (inf.dFlow2, 1), inf.nFlow3
          );
        g.DrawString ( s, fntNormal, brush, x, y );
      }
      s = string.Format ( "{0}: {1}", 
        r.GetString ( "cView_PrintParams_DeviceInfo_Batt" ),      // "Batterie [%]"
        inf.BatteryState
        );
      g.DrawString ( s, fntNormal, brush, xInfCol2, y );

      // Part: Results
      
      // 5. line: "Results" (BOLD)
      y += cy;
      s = r.GetString ( "cView_PrintParams_Results" );            // "Messergebnisse"
      g.DrawString ( s, fntBold, brush, x, y );
      // 6. line: Meas. cycle ( in s ), AccuRate     
      y += cy;
      s = string.Format ( "   {0} = {1} s",
        r.GetString ( "cView_PrintParams_Results_MeasCycle" ),    // "Messzyklus"
        (int)doc.ScanParams.wMeasCycle/10
        );
      g.DrawString ( s, fntNormal, brush, x, y );
      s = string.Format ( "{0} = {1}",
        r.GetString ( "cView_PrintParams_Results_AccuRate" ),     // "Akku-Rate"
        (int)doc.ScanParams.wAccurate
        );
      g.DrawString ( s, fntNormal, brush, xInfCol2, y );
      // 7. line: SigProcInt, PCCommInt      
      y += cy;
      s = string.Format ( "   {0} = {1} pts",
        r.GetString ( "cView_PrintParams_Results_SigProcInt" ),    // "Signalverarbeitungsintervall"
        (int)doc.ScanParams.wSigProcInt
        );
      g.DrawString ( s, fntNormal, brush, x, y );
      s = string.Format ( "{0} = {1} pts",
        r.GetString ( "cView_PrintParams_Results_PCCommInt" ),     // "PC-Kommunikationsintervall"
        (int)doc.ScanParams.wPCCommInt
        );
      g.DrawString ( s, fntNormal, brush, xInfCol2, y );
      // 8. ... lines: Results
      //    Header
      y += cy;
      s = string.Format ( "   {0}", 
        r.GetString ( "cView_PrintParams_Results_Name" ) );       // "Substanz"
      g.DrawString ( s, fntNormal, brush, x, y );
      s = r.GetString ( "cView_PrintParams_Results_Result" );     // "Resultat"
      g.DrawString ( s, fntNormal, brush, xResCol2, y );
      s = r.GetString ( "cView_PrintParams_Results_SpanFac" );    // "Spanfaktor"
      g.DrawString ( s, fntNormal, brush, xResCol3, y );
      s = r.GetString ( "cView_PrintParams_Results_WinBeg" );     // "Beginn [pts]"
      g.DrawString ( s, fntNormal, brush, xResCol4, y );
      s = r.GetString ( "cView_PrintParams_Results_WinWid" );     // "Breite [pts]"
      g.DrawString ( s, fntNormal, brush, xResCol5, y );
      //    Horiz. separator line
      y += cy;
      g.DrawLine (pen, x+cxSepMargin, y+cy/2, x+rDraw.Width-cxSepMargin, y+cy/2);
      //    Data      
      for ( int i=0; i < res.Count; i++ )
      {
        y += cy;
        // Substance (ggf. with Alarm indicator)
        if (res[i].dResult >= res[i].dA2)         // alarm case
        {
          s = string.Format ( " * {0}", res[i].sSubstance );
          g.DrawString ( s, fntNormal, brushAlarm, x, y );
        }
        else                                      // non-alarm case
        {
          s = string.Format ( "   {0}", res[i].sSubstance );
          g.DrawString ( s, fntNormal, brush, x, y );
        }
        // Meas. result, including conc. incredible
        int nPostDigits = ConcUnits.GetDecimals (res[i].sConcUnit);
        s = string.Format ( "{0} {1}", Common.ConvertToScience ( res[i].dResult, nPostDigits ), res[i].sConcUnit );
        if (res[i].byConcIncredible == 1) s = "> " + s;           
        g.DrawString ( s, fntNormal, brush, xResCol2, y );
        // Span factor
        s = Common.ConvertToScience ( res[i].dSpanFac, 3 );
        g.DrawString ( s, fntNormal, brush, xResCol3, y );
        // Begin  [pts]
        s = res[i].dwBeginTime.ToString ();
        g.DrawString ( s, fntNormal, brush, xResCol4, y );
        // Width  [pts]
        s = res[i].dwWidthTime.ToString (); 
        g.DrawString ( s, fntNormal, brush, xResCol5, y );
      }
    
    }  

    #endregion // methods
  
    #region event handling

    //---------------------------------------------------------------
    // Graph panel

    /// <summary>
    /// MouseDown event handling: Graphics Picturebox
    /// </summary>
    public void OnMouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
    {
      // Left button: Position cursor
      if ( e.Button == MouseButtons.Left ) 
      {
        // Cursor: Remember normal cursor, set capture, clip cursor
        _cuNormal = ((Control) sender).Cursor;
        ((Control)sender).Capture = true;
        Rectangle rClip = ((Control) sender).RectangleToScreen ( ((Control) sender).ClientRectangle );
        Cursor.Clip = rClip;
        // Check: Continue?
        if (!_gd.HasData())
          return;   // No.
        // Calc. & set cursor data
        Rectangle r = ((Control) sender).ClientRectangle;
        Point pt = new Point ( e.X, e.Y );
        float fX = Scale.PelToData (pt.X, r.Left, r.Right, _gd.xMin, _gd.xMax); 
        float fY = Scale.PelToData (pt.Y, r.Bottom, r.Top, _gd.yMin, _gd.yMax); 
        _gd.XCursor = fX;
        _gd.YCursor = fY;
        // Update the graphics
        UpdateGraph ();
      }
    
      // Right button: Zoom
      if ( e.Button == MouseButtons.Right ) 
      {
        // Check: Continue?
        if (!_gd.HasData())
          return;   // No.
        try
        {
          // Cursor: Remember normal cursor, set capture, clip cursor
          _cuNormal = ((Control) sender).Cursor;
          ((Control)sender).Capture = true;
          Rectangle rClip = ((Control) sender).RectangleToScreen ( ((Control) sender).ClientRectangle );
          Cursor.Clip = rClip;
          // Initiate zoom positions
          Point ptCursorPos = new Point ( e.X, e.Y );
          _ptZoomPosFirst = ptCursorPos;
          _ptZoomPosLast = ptCursorPos;
          // Create zoom bitmap
          _bmZoom = new Bitmap ( _gd.Graphic );
          // Indicate: Zoom mode on
          _bZoomMode = true;
        }
        catch {}
      }
    }
    
    /// <summary>
    /// MouseMove event handling: Graphics Picturebox
    /// </summary>
    public void OnMouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
    {
      // Left button: Position cursor
      if ( e.Button == MouseButtons.Left ) 
      {
        // Cursor: Zoom cursor
        if ( null != _cuZoom ) 
          ((Control) sender).Cursor = _cuZoom;
        // Check: Continue?
        if (!_gd.HasData())
          return;   // No.
        // Calculate & set cursor data
        Rectangle r = ((Control) sender).ClientRectangle;
        Point pt = new Point ( e.X, e.Y );
        float fX = Scale.PelToData (pt.X, r.Left, r.Right, _gd.xMin, _gd.xMax); 
        float fY = Scale.PelToData (pt.Y, r.Bottom, r.Top, _gd.yMin, _gd.yMax); 
        _gd.XCursor = fX;
        _gd.YCursor = fY;
        // Update the graphics
        UpdateGraph ();
      }
    
      // Right button: Zoom
      if ( e.Button == MouseButtons.Right ) 
      {
        // Check: Continue?
        if (!_gd.HasData())
          return;   // No.
        try
        {
          // Check: Zoom mode on?
          if ( _bZoomMode == true ) 
          {
            // Yes:
            // Cursor: Zoom cursor
            if ( null != _cuZoom ) 
              ((Control) sender).Cursor = _cuZoom;
            // Update zoom positions
            Point ptCursorPos = new Point ( e.X, e.Y );
            _ptZoomPosLast = ptCursorPos;
            // Draw zoom bitmap
            Graphics g = Graphics.FromImage ( _bmZoom );
            g.DrawImage ( _gd.Graphic, 0, 0 );
            if ((_nZoomMinDistance < _ZoomRectangle.Width) && (_nZoomMinDistance < _ZoomRectangle.Height))
            {
              Pen pen = new Pen ( Color.Black );
              g.DrawRectangle ( pen, _ZoomRectangle );
            }
            // Assign zoom bitmap
            PictureBox pbGraph = (PictureBox) sender;
            pbGraph.Image = _bmZoom;
            pbGraph.Refresh ();
          }
        }
        catch {}      
      }
    }

    /// <summary>
    /// MouseUp event handling: Graphics Picturebox
    /// </summary>
    public void OnMouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
    {
      // Left button: Position cursor
      if ( e.Button == MouseButtons.Left ) 
      {
        // Cursor: Set normal cursor, release capture, reset cursor clipping
        if ( null != _cuNormal ) 
          ((Control) sender).Cursor = _cuNormal;
        ((Control)sender).Capture = false;
        Cursor.Clip = Screen.PrimaryScreen.Bounds;
      }
    
      // Right button: Zoom
      if ( e.Button == MouseButtons.Right ) 
      {
        // Check: Zoom mode on?
        if (_bZoomMode == true) 
        {
          // Yes:
          try
          {
            // Cursor: Set normal cursor, release capture, reset cursor clipping
            if ( null != _cuNormal ) 
              ((Control) sender).Cursor = _cuNormal;
            ((Control)sender).Capture = false;
            Cursor.Clip = Screen.PrimaryScreen.Bounds;
            // Update zoom positions
            Point ptCursorPos = new Point ( e.X, e.Y );
            _ptZoomPosLast = ptCursorPos;
            // Calculate & set new scale
            if ((_nZoomMinDistance < _ZoomRectangle.Width) && (_nZoomMinDistance < _ZoomRectangle.Height))
            {
              Rectangle r = ((Control) sender).ClientRectangle;
              float xmin = Scale.PelToData (_ZoomRectangle.Left, r.Left, r.Right, _gd.xMin, _gd.xMax);
              float ymax = Scale.PelToData (_ZoomRectangle.Top, r.Bottom, r.Top, _gd.yMin, _gd.yMax);
              float xmax = Scale.PelToData (_ZoomRectangle.Right, r.Left, r.Right, _gd.xMin, _gd.xMax);
              float ymin = Scale.PelToData (_ZoomRectangle.Bottom, r.Bottom, r.Top, _gd.yMin, _gd.yMax);
              _gd.SetScale (xmin, xmax, ymin, ymax);
            }                    
            // Update the graphics
            UpdateGraph ();
          }
          catch {}
          finally 
          {
            // Indicate: Zoom mode off
            _bZoomMode = false;
          }
        }
      }
    }

    //---------------------------------------------------------------
    // Printing

    /// <summary>
    /// BeginPrint event handling: PrintDocument
    /// </summary>
    public void OnBeginPrinting ( PrintEventArgs e ) 
    {
    }
    
    /// <summary>
    /// EndPrint event handling: PrintDocument
    /// </summary>
    public void OnEndPrinting ( PrintEventArgs e ) 
    {
    }

    /// <summary>
    /// PrintPage event handling: PrintDocument
    /// </summary>
    public void OnPrint ( PrintPageEventArgs e ) 
    {
      Graphics g = e.Graphics;
      
      // Set the drawing area
      Rectangle rPage = e.PageBounds;   // total area of the page
      Rectangle rDraw = e.MarginBounds; // the portion of the page inside the margins
      
      // Get the positions for header, data and params drawing area:
      // The relation regarding the area heights is H : D : P ( = 1 : 5 : 5 )
      int nVertUnitsH = 1;
      int nVertUnitsD = 3;
      int nVertUnitsP = 4;
      int nVertUnits = nVertUnitsH + nVertUnitsD + nVertUnitsP;  // summ. number of vertical units for area separation
      
      Rectangle rDrawHeader = rDraw;                 
      Rectangle rDrawData   = rDraw;
      Rectangle rDrawParams = rDraw;                 
            
      rDrawHeader.Height = nVertUnitsH * (rDraw.Height/nVertUnits);
            
      rDrawData.Y        = rDrawHeader.Bottom;
      rDrawData.Height   = nVertUnitsD * (rDraw.Height/nVertUnits);
            
      rDrawParams.Y      = rDrawData.Bottom;
      rDrawParams.Height = nVertUnitsP * (rDraw.Height/nVertUnits);
  
      // Draw a line for perforation purposes
      Pen pen = new Pen ( Color.Black );
      g.DrawLine ( pen, rPage.Left, rPage.Height/2, (rPage.Left+rDraw.Left)/2, rPage.Height/2 ); 
  
      // Draw a frame around the drawing area
      rDraw.Height -= 1; rDraw.Width -= 1;
      g.DrawRectangle ( pen, rDraw );
      rDraw.Inflate ( -rDraw.Width/200, -rDraw.Width/200 );
      g.DrawRectangle ( pen, rDraw );
  
      // Draw a line between header, data and params drawing area
      g.DrawLine ( pen, rDraw.Left, rDrawHeader.Bottom, rDraw.Right, rDrawHeader.Bottom ); 
      g.DrawLine ( pen, rDraw.Left, rDrawData.Bottom, rDraw.Right, rDrawData.Bottom ); 
  
      // Draw the header to the page                                
      _OnPrintHeader ( e, rDrawHeader );
  
      // Check: Data present (i.e. graphical object valid)?
      if (_gd.HasData ())
      {
        // Yes:
        // Draw the data to the page            
        _OnPrintData ( e, rDrawData );

        // Draw the params to the page            
        _OnPrintParams ( e, rDrawParams );  
      }
    }
 
    #endregion // event handling

  }
}
