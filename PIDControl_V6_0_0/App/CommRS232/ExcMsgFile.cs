using System;
using System.IO;
using System.Text;
using System.Reflection;

using App;

namespace CommRS232
{
  /// <summary>
  /// Append specification type def.
  /// </summary>
  public enum AppendSpec
  {
    OnEntrance,   // On entrance: before the data are TX'd to the device 
    OnSuccess,    // On success: after the data have been read succ'ly from the device
    OnError,      // On error: en error occurred on TX/RX to/from the device
 
    OnThreadProc_CMID_ANSWER,
    OnThreadProc_CMID_EMPTY,
    OnThreadProc_Catch,

    OnHandleRX_Entry,
    OnHandleRX_AfterInv,
    OnHandleRX_CommClose,
    OnHandleRX_WarmUp,
    OnHandleRX_NotWarmUp,
    
    OnAfterRXCompleted_CMID_EMPTY,
    OnAfterRXCompleted_CMID_TIMEOUT,
    OnAfterRXCompleted_CMID_ANSWER,
    OnAfterRXCompleted_SAD_NODATA
  };

  /// <summary>
  /// Exception message file type def.
  /// </summary>
  enum ExcMsgFileType
  {
    Current,      // current Exception message file
    First         // first Exception message file
  };

  /// <summary>
  /// Class ExcMsgFile:
  /// Exception message file handling
  /// </summary>
  /// <remarks>
  /// The PC communication data can be stored in a special folder, the so called 'PCcommListener' folder.
  /// A storage is performed in the case that the menuitem 'PC communication listener' is checked by the user. 
  /// If this happens, the 'PCcommListener' folder is created within the application folder, and 
  /// the data are stored in a couple of consecutively numbered files, the so called 
  /// 'Exception message files'. The indices of these files are managed by means of the co called
  /// 'Exception message index file'. 
  /// If PC communication data storage is not desired, the 'PCcommListener' folder is removed.
  /// </remarks>
  public class ExcMsgFile
  {
    
    #region constructors
    
    /// <summary>
    /// Constructor
    /// </summary>
    public ExcMsgFile ()
    {
      // Application folder
      Assembly ass = Assembly.GetExecutingAssembly ();
      string sAppFilepath = ass.Location;
      string sAppFolder = Path.GetDirectoryName ( sAppFilepath );
      // Path of the 'PCcommListener' folder as subfolder of the application folder
      m_sExcMsgFolder = Path.Combine ( sAppFolder, "PCcommListener" );
      // Required file paths within the 'PCcommListener' folder
      m_sFmtFilePath = Path.Combine ( m_sExcMsgFolder, "ExcMsg_{0}.txt" );  // Template path for Exception message files
      m_sIdxPath = Path.Combine ( m_sExcMsgFolder, "ExcMsgIdx.txt" );       // Exception message index file path
    }
   
    #endregion constructors

    #region constants

    /// <summary>
    ///  Max. # of files
    /// </summary>
    const int _MAXFILES = 2;
    /// <summary>
    /// Max. file size (in Bytes): 100 KB
    /// </summary>
    const int _MAXFILESIZE = 102400;

    #endregion constants

    #region members

    /// <summary>
    /// Indicates, whether the Exception message file handling is enabled or not
    /// </summary>
    bool m_bEnabled = true;

    /// <summary>
    /// The 'PCcommListener' folder
    /// </summary>
    string m_sExcMsgFolder = "";
    /// <summary>
    /// Path of the Exception message index file
    /// </summary>
    string m_sIdxPath = "";
    /// <summary>
    /// Template path for Exception message files
    /// </summary>
    string m_sFmtFilePath = "";

    /// <summary>
    /// The exception name (in case of exception)
    /// </summary>
    string m_ExcName = "";
    
    #endregion members
    
    #region methods

    /// <summary>
    /// Init's the Exception message file handling.
    /// </summary>
    public void Init ()
    {
      // Delete all files residing in the 'PCcommListener' folder (i.e. Exception message files and 
      // the Exception message index file), and the folder itself
      if (Directory.Exists (m_sExcMsgFolder))
        Directory.Delete (m_sExcMsgFolder, true);
    }

    /// <summary>
    /// Appends data to an Exception message file.
    /// </summary>
    /// <param name="msg">The comm. message</param>
    /// <param name="spec">The Append specification type</param>
    public void Append (CommMessage msg, AppendSpec spec)
    {
      // Exception message file handling enabled?
      if (!m_bEnabled)
        return;   // No
      // Yes:
      // Append - Kernel
      Append_Kernel ();
      // Append data
      AppendExcMsgFile (msg, spec);
    }

    /// <summary>
    /// Appends data to an Exception message file - special case.
    /// </summary>
    /// <param name="hWnd">The owner window of the message</param>
    /// <param name="pos">The # of messages with this owner window in the message list</param>
    public void AppendSpecial (IntPtr hWnd, int pos)
    {
      // Exception message file handling enabled?
      if (!m_bEnabled)
        return;   // No
      // Yes:
      // Append - Kernel
      Append_Kernel ();
      // Append special data
      AppendExcMsgFileSpecial (hWnd, pos);
    }


    /// <summary>
    /// Appends data to an Exception message file - Kernel routine.
    /// </summary>
    void Append_Kernel ()
    {
      // Get the path of the current Exception message file
      string sPath;
      GetExcMsgFilePath (ExcMsgFileType.Current, out sPath);
      // Check: Does the file exist?
      if (!File.Exists (sPath))
      {
        // No: Create a new file
        CreateExcMsgFile ();
      }
      else
      {
        // Yes:
        // Get the size of the file
        FileInfo fi = new FileInfo (sPath);
        // Check: Upper limit exceeded?
        if (fi.Length > _MAXFILESIZE)
        {
          // Yes: 
          // Get the indices of the current & the 1st Exception message files
          int firstIdx, currIdx;
          ReadIdxFile (out currIdx, out firstIdx);
          // Incr. the idx of the current Exception message file (-> this will be the idx of the next current Exception message file) 
          currIdx++;
          // Check: Is the max. permitted # of Exception message files already reached?
          if (currIdx == firstIdx + _MAXFILES)
          {
            // Yes:
            // Get the path of the 1st Exception message file
            GetExcMsgFilePath (ExcMsgFileType.First, out sPath);
            // Remove it
            if (File.Exists (sPath)) File.Delete (sPath);
            // Incr. the idx of the 1st Exception message file (-> this will be the idx of the next 1st Exception message file) 
            firstIdx++;
          }
          // Store the indices of the current & the 1st Exception message files
          WriteIdxFile (currIdx, firstIdx);
          // Create a new Exception message file
          CreateExcMsgFile ();
        }
      }

    }

    /// <summary>
    /// Gets the path of the current or the 1st Exception message file, depending on the given 
    /// Exception message file type.
    /// </summary>
    /// <param name="t">the Exception message file type</param>
    /// <param name="sPath">the corr'ing Exception message file path (out)</param>
    void GetExcMsgFilePath (ExcMsgFileType t, out string sPath)
    {
      int firstIdx, currIdx, idx;

      // Get the Exception message file indices
      ReadIdxFile (out currIdx, out firstIdx);
      // CD: Exception message file type
      if (t == ExcMsgFileType.Current)  idx=currIdx;       // current file
      else                              idx=firstIdx;      // 1st file
      // Build the corr'ing Exception message file path
      sPath = string.Format (m_sFmtFilePath, idx);
    }
    
    /// <summary>
    /// Creates an Exception message file.
    /// </summary>
    void CreateExcMsgFile ()
    {
      // Get the path of the current Exception message file
      string sPath;
      GetExcMsgFilePath (ExcMsgFileType.Current, out sPath);
      // Write to file: Header 
      using (StreamWriter sw = new StreamWriter(sPath)) 
      {
        sw.WriteLine ("{0}\t{1}\t{2}\t{3}\t{4}\t{5}", "Time", "Cmd-IN", "Par-IN", "Cmd-Out", "Par-Out", "Exc");
      }
    }
    
    /// <summary>
    /// Writes to an Exception message file:
    /// Appends text (time stamp & comm. data) to the file. Creates the file previously, if required.
    /// </summary>
    /// <param name="msg">The comm. message</param>
    /// <param name="spec">The Append specification type</param>
    void AppendExcMsgFile (CommMessage msg, AppendSpec spec)
    {
      // Get the path of the current Exception message file
      string sPath;
      GetExcMsgFilePath (ExcMsgFileType.Current, out sPath);
      // Build the data line to be stored, depending on the Append specification type
      string sLine = "";
      byte[] arbyPar;
      string sDT = DateTime.Now.ToString ("dd.MM.yy HH:mm:ss");
      switch (spec)
      {
          // -----------------------------------
          // ThreadProc - Main

        case AppendSpec.OnEntrance:
          sLine = string.Format ("{0}\t{1}\t{2}", sDT, msg.Command, msg.Parameter);
          break;

        case AppendSpec.OnSuccess:
          arbyPar = msg.ParameterResponse;
          sLine = string.Format ("{0}\t{1}\t{2}\t{3}\t{4}", sDT, msg.Command, msg.Parameter, msg.CommandResponse, Encoding.ASCII.GetString (arbyPar));
          break;

        case AppendSpec.OnError:
          arbyPar = msg.ParameterResponse;
          sLine = string.Format ("{0}\t{1}\t{2}\t{3}\t{4}\t{5}", sDT, msg.Command, msg.Parameter, msg.CommandResponse, Encoding.ASCII.GetString (arbyPar), msg.ExcMsg);
          break;
      
          // -----------------------------------
          // ThreadProc
      
        case AppendSpec.OnThreadProc_CMID_ANSWER:
          sLine = string.Format ("\t-> ThreadProc: at CMID_ANSWER");
          break;
        case AppendSpec.OnThreadProc_CMID_EMPTY:
          sLine = string.Format ("\t-> ThreadProc: at CMID_EMPTY");
          break;
        case AppendSpec.OnThreadProc_Catch:
          sLine = "\r\n";
          sLine += string.Format ("\t***********************\r\n");
          sLine += string.Format ("\t***********************\r\n");
          sLine += string.Format ("\t-> ThreadProc: at Catch ({0})\r\n", this.m_ExcName);
          sLine += string.Format ("\t***********************\r\n");
          sLine += string.Format ("\t***********************\r\n");
          break;

          // -----------------------------------
          // Main - HandleRX

        case AppendSpec.OnHandleRX_Entry:
          sLine = string.Format ("\t-> HandleRX: at Entry");
          break;
        case AppendSpec.OnHandleRX_AfterInv:
          sLine = string.Format ("\t-> HandleRX: after Invocation");
          break;
        case AppendSpec.OnHandleRX_CommClose:
          sLine = string.Format ("\t-> HandleRX: after Comm close");
          break;
        case AppendSpec.OnHandleRX_WarmUp:
          sLine = string.Format ("\t-> HandleRX: at WarmUp");
          break;
        case AppendSpec.OnHandleRX_NotWarmUp:
          sLine = string.Format ("\t-> HandleRX: at Form allocation");
          break;
      
          // -----------------------------------
          // Main - AfterRXCompleted
        
        case AppendSpec.OnAfterRXCompleted_CMID_EMPTY:
          sLine = string.Format ("\t-> AfterRXCompleted: at CMID_EMPTY");
          break;
        case AppendSpec.OnAfterRXCompleted_CMID_TIMEOUT:
          sLine = string.Format ("\t-> AfterRXCompleted: at CMID_TIMEOUT");
          break;
        case AppendSpec.OnAfterRXCompleted_CMID_ANSWER:
          sLine = string.Format ("\t-> AfterRXCompleted: at CMID_ANSWER");
          break;
        case AppendSpec.OnAfterRXCompleted_SAD_NODATA:
          sLine = string.Format ("\t-> AfterRXCompleted: at NODATA");
          break;
      
      }
      // Append the data line to the file
      StreamWriter sw = null;
      try
      {
        sw = File.AppendText (sPath);
        sw.WriteLine (sLine);
      }
      catch
      {
      }
      finally
      {
        if (null != sw) sw.Close ();
      }
    }  
    
    /// <summary>
    /// Writes to an Exception message file:
    /// Appends text (time stamp & comm. data) to the file. Creates the file previously, if required.
    /// </summary>
    /// <param name="msg">The comm. message</param>
    /// <param name="spec">The Append specification type</param>
    void AppendExcMsgFileSpecial (IntPtr hWnd, int pos)
    {
      // Get the path of the current Exception message file
      string sPath;
      GetExcMsgFilePath (ExcMsgFileType.Current, out sPath);
      // Build the data line to be stored, depending on the Append specification type
      string sLine = string.Format ("\t-> Wnd: {0}\tPos: {1}", hWnd.ToString (), pos);
      // Append the data line to the file
      StreamWriter sw = null;
      try
      {
        sw = File.AppendText (sPath);
        sw.WriteLine (sLine);
      }
      catch
      {
      }
      finally
      {
        if (null != sw) sw.Close ();
      }
    }  
    
    
    /// <summary>
    /// Writes the Exception message index file.
    /// </summary>
    /// <param name="currIdx">The idx of the current Exception message file</param>
    /// <param name="firstIdx">The idx of the 1st Exception message file</param>
    void WriteIdxFile (int currIdx, int firstIdx)
    {
      using (StreamWriter sw = new StreamWriter(m_sIdxPath)) 
      {
        sw.WriteLine ("{0},{1}", currIdx, firstIdx); 
      }
    }

    /// <summary>
    /// Reads the Exception message index file.
    /// </summary>
    /// <param name="firstIdx">The idx of the 1st Exception message file (out)</param>
    /// <param name="currIdx">The idx of the current Exception message file (out)</param>
    void ReadIdxFile (out int currIdx, out int firstIdx) 
    {
      // Check: Does the 'PCcommListener' folder exist?
      if (!Directory.Exists (m_sExcMsgFolder))
      {
        // No:
        // Create it
        Directory.CreateDirectory ( m_sExcMsgFolder );
        // Initiate the Exception message index file 
        // (= Reset the indices of the current & the 1st Exception message files)
        WriteIdxFile (0, 0); 
      }
      // Read the Exception message index file
      using (StreamReader sr = new StreamReader(m_sIdxPath)) 
      {
        string sLine = sr.ReadLine ();
        int idx = sLine.IndexOf (",");
        string sCurrIdx = sLine.Substring (0,idx);
        currIdx = int.Parse (sCurrIdx);
        string sFirstIdx = sLine.Substring (idx+1);
        firstIdx = int.Parse (sFirstIdx);
      }
    }

    #endregion methods
  
    #region properties
    
    /// <summary>
    /// Indicates, whether the Exception message file handling is enabled or not
    /// </summary>
    public bool Enabled
    {
      get { return m_bEnabled; }
      set { m_bEnabled = value; }
    }
    
    /// <summary>
    /// The exception name (in case of exception)
    /// </summary>
    public string ExcName
    {
      get { return m_ExcName; }
      set { m_ExcName = value; }
    }
 
    #endregion properties
  }

}





