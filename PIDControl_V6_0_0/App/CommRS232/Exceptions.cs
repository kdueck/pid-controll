using System;

namespace CommRS232
{

  /// <summary>
  /// Class CmdException:
  /// Exception thrown when the device received an erroneous cmd ( TX PC -> RX device )
  /// </summary>
  public class CmdException : System.ApplicationException
  {
    #region constructors
    
    /// <summary>
    /// Constructor
    /// </summary>
    public CmdException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public CmdException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      string sMsg = "Cmd failure";
      return sMsg;
    }

    #endregion methods
  
  }
  
  
  /// <summary>
  /// Class CmdParException:
  /// Exception thrown when the device received an erroneous cmd-par-string ( TX PC -> RX device )
  /// </summary>
  public class CmdParException : System.ApplicationException
  {
    #region constructors
    
    /// <summary>
    /// Constructor
    /// </summary>
    public CmdParException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public CmdParException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      string sMsg = "Cmd-Par failure";
      return sMsg;
    }

    #endregion methods
  
  }
  
  
  /// <summary>
  /// Class CRC16Exception:
  /// Exception thrown when a CRC16 error occurs
  /// </summary>
  public class CRC16Exception : System.ApplicationException
  {
    #region constructors
    
    /// <summary>
    /// Constructor
    /// </summary>
    public CRC16Exception () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public CRC16Exception (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      string sMsg = "CRC16 failure";
      return sMsg;
    }

    #endregion methods
  
  }


  /// <summary>
  /// Class InputBufferException:
  /// Exception thrown when an error on RX input buffer filling occurred ( TX PC -> RX device )
  /// </summary>
  public class InputBufferException : System.ApplicationException
  {
    #region constructors
    
    /// <summary>
    /// Constructor
    /// </summary>
    public InputBufferException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public InputBufferException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      string sMsg = "Input buffer failure";
      return sMsg;
    }

    #endregion methods
  
  }

  
  /// <summary>
  /// Class TimeoutException:
  /// Exception thrown when a Timeout occurs.
  /// </summary>
  public class TimeoutException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public TimeoutException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public TimeoutException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      string sMsg = "Timeout failure";
      return sMsg;
    }

    #endregion methods
  
  }

}





