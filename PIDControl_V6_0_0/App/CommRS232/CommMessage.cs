using System;
using System.Collections;
using System.Windows.Forms;
using System.Text;
using System.Diagnostics;

using App;

namespace CommRS232
{
  /// <summary>
  /// Class CommMessage:
  /// Communication message 
  /// </summary>
  public class CommMessage
  {

    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public CommMessage()
    {
      Empty ();
    }
	
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The communication message</param>
    public CommMessage ( CommMessage msg ) 
    {
      _sCmd = msg.Command;
      _sPar = msg.Parameter;
      _bBinaryCmd = msg.BinaryCommand;
      _bSuspendRXTimeoutHandling = msg.SuspendRXTimeoutHandling;

      _bPermanentyCalled = msg.PermanentyCalled;
      
      _sCmdAnswer = msg.CommandResponse;
      if (null != msg.ParameterResponse)
      {
        _arParAnswer = new byte[msg.ParameterResponse.Length];
        Array.Copy (msg.ParameterResponse, 0, _arParAnswer, 0, msg.ParameterResponse.Length);
      }
      if (null != msg.WindowInfo)
        _wi = new WndInfo (msg.WindowInfo);
      _nResponseID = msg.ResponseID;

      _sExcMsg = msg.ExcMsg;
    } 
  
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="sCmd">The message command</param>
    public CommMessage ( string sCmd ) 
    {
      Empty ();
      _sCmd = sCmd;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="sCmd">The message command</param>
    /// <param name="parameter">The message parameter</param>
    public CommMessage ( string sCmd, string parameter ) 
    {
      Empty ();
      _sCmd = sCmd;
      _sPar = parameter;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="sCmd">The message command</param>
    /// <param name="parameter">The message parameter</param>
    /// <param name="bBinaryCmd">Indicates, whether a message requests a binary response (True) or not (False)</param>
    public CommMessage ( string sCmd, string parameter, bool bBinaryCmd ) 
    {
      Empty ();
      _sCmd = sCmd;
      _sPar = parameter;
      _bBinaryCmd = bBinaryCmd;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="sCmd">The message command</param>
    /// <param name="parameter">The message parameter</param>
    /// <param name="bBinaryCmd">Indicates, whether a message requests a binary response (True) or not (False)</param>
    /// <param name="bSuspendRXTimeoutHandling">Indicates, whether RX Timeout handling is suspended (True) or not (False)</param>
    public CommMessage ( string sCmd, string parameter, bool bBinaryCmd, bool bSuspendRXTimeoutHandling ) 
    {
      Empty ();
      _sCmd = sCmd;
      _sPar = parameter;
      _bBinaryCmd = bBinaryCmd;
      _bSuspendRXTimeoutHandling = bSuspendRXTimeoutHandling;
    }
    
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="wi">The information regarding the form thats owns the message</param>
    /// <param name="sCmd">The message command</param>
    public CommMessage ( WndInfo wi, string sCmd ) 
    {
      Empty ();
      _wi = new WndInfo (wi);
      _sCmd = sCmd;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="wi">The information regarding the form thats owns the message</param>
    /// <param name="sCmd">The message command</param>
    /// <param name="parameter">The message parameter</param>
    public CommMessage ( WndInfo wi, string sCmd, string parameter ) 
    {
      Empty ();
      _wi = new WndInfo (wi);
      _sCmd = sCmd;
      _sPar = parameter;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="wi">The information regarding the form thats owns the message</param>
    /// <param name="sCmd">The message command</param>
    /// <param name="parameter">The message parameter</param>
    /// <param name="bBinaryCmd">Indicates, whether a message requests a binary response (True) or not (False)</param>
    public CommMessage ( WndInfo wi, string sCmd, string parameter, bool bBinaryCmd ) 
    {
      Empty ();
      _wi = new WndInfo (wi);
      _sCmd = sCmd;
      _sPar = parameter;
      _bBinaryCmd = bBinaryCmd;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="wi">The information regarding the form thats owns the message</param>
    /// <param name="sCmd">The message command</param>
    /// <param name="parameter">The message parameter</param>
    /// <param name="bBinaryCmd">Indicates, whether a message requests a binary response (True) or not (False)</param>
    /// <param name="bSuspendRXTimeoutHandling">Indicates, whether RX Timeout handling is suspended (True) or not (False)</param>
    public CommMessage ( WndInfo wi, string sCmd, string parameter, bool bBinaryCmd, bool bSuspendRXTimeoutHandling ) 
    {
      Empty ();
      _wi = new WndInfo (wi);
      _sCmd = sCmd;
      _sPar = parameter;
      _bBinaryCmd = bBinaryCmd;
      _bSuspendRXTimeoutHandling = bSuspendRXTimeoutHandling;
    }
    
    #endregion // constructors
  
    #region constants & enums

    // Communication error specifier
    // NOTE:  The names of these specifiers in the GSM SW MUST coincide with the corr'ing names
    //        in the device SW.
    /// <summary>
    /// Specifier: Device received an erroneous cmd ( TX PC -> RX device )
    /// </summary>
    const string SZCOMMERROR_COMMAND = "ERRCMD";
    /// <summary>
    /// Specifier: Device received an erroneous cmd-par-string ( TX PC -> RX device )
    /// </summary>
    const string SZCOMMERROR_CMDPARSTR = "ERRCMDPARSTR";
    /// <summary>
    /// Specifier: CRC error after receiving a cmd-par-string ( TX PC -> RX device )
    /// </summary>
    const string SZCOMMERROR_CRC = "ERRCRC";
    /// <summary>
    /// Specifier: Error on RX input buffer filling ( TX PC -> RX device )
    /// </summary>
    const string SZCOMMERROR_BUF = "ERRBUF";

    /// <summary>
    /// WarmUp specifier
    /// NOTE: The name of this specifier in the GSM SW MUST coincide with the corr'ing name
    ///       in the device SW.
    /// </summary>
    public const string SZWARMUP = "WARMUP";
    
    #endregion constants & enums

    #region private members
    
    /// <summary>
    /// The message command 
    /// </summary>
    string _sCmd = "";
    /// <summary>
    /// The message parameter
    /// </summary>
    string _sPar = "";
    /// <summary>
    /// Indicates, whether a message requests a binary response (True) or not (False)
    /// </summary>
    bool _bBinaryCmd = false;
    /// <summary>
    /// Indicates, whether RX Timeout handling is suspended (True) or not (False)
    /// </summary>
    bool _bSuspendRXTimeoutHandling = false;

    /// <summary>
    /// Indicates, whether a message is a permanenty called message (true) or not (false)
    /// </summary>
    bool _bPermanentyCalled = false;
    
    /// <summary>
    /// The message command ( response )
    /// </summary>
    string _sCmdAnswer = "";           
    /// <summary>
    /// The message parameter ( response )
    /// </summary>
    byte[] _arParAnswer = null;           
    /// <summary>
    /// The window information
    /// </summary>
    WndInfo _wi = null;   
    /// <summary>
    /// The message response ID
    /// </summary>
    int _nResponseID = 0;

    /// <summary>
    /// The exception message, if req.
    /// </summary>
    string _sExcMsg = "";

    #endregion  // private members 
    
    #region properties
    
    /// <summary>
    /// The message command
    /// </summary>
    public string Command 
    {
      get { return _sCmd; }
      set { _sCmd = value; }
    }
    /// <summary>
    /// The message parameter
    /// </summary>
    public string Parameter 
    {
      get { return _sPar; }
      set { _sPar = value; }
    }
    /// <summary>
    /// Indicates, whether a message requests a binary response (True) or not (False)
    /// </summary>
    public bool BinaryCommand
    {
      get { return _bBinaryCmd; }
      set { _bBinaryCmd = value; }
    }
    /// <summary>
    /// Indicates, whether RX Timeout handling is suspended (True) or not (False)
    /// </summary>
    public bool SuspendRXTimeoutHandling
    {
      get { return _bSuspendRXTimeoutHandling; }
      set { _bSuspendRXTimeoutHandling = value; }
    }

    /// <summary>
    /// Indicates, whether a message is a permanenty called message (true) or not (false)
    /// </summary>
    public bool PermanentyCalled
    {
      get { return _bPermanentyCalled; }
      set { _bPermanentyCalled = value; }
    }

    /// <summary>
    /// The message command (response)
    /// </summary>
    public string CommandResponse 
    {
      get { return _sCmdAnswer; }
      set { _sCmdAnswer = value; }
    }
    /// <summary>
    /// The message parameter (response)
    /// </summary>
    public byte[] ParameterResponse 
    {
      get { return _arParAnswer; }
      set { _arParAnswer = value; }
    }
    /// <summary>
    /// The window information
    /// </summary>
    public WndInfo WindowInfo
    {
      get { return _wi; }
      set { _wi = value; }
    }
    /// <summary>
    /// The message response ID
    /// </summary>
    public int ResponseID
    {
      get { return _nResponseID; }
      set { _nResponseID= value; }
    }

    /// <summary>
    /// The exception message, if req. (response)
    /// </summary>
    public string ExcMsg 
    {
      get { return _sExcMsg; }
      set { _sExcMsg = value; }
    }
    
    #endregion // properties

    #region methods
  
    /// <summary>
    /// Clears a message object.
    /// </summary>
    void Empty ()
    {
      _sCmd = string.Empty;
      _sPar= string.Empty;
      _bBinaryCmd = false;
      _bSuspendRXTimeoutHandling = false;
      
      _bPermanentyCalled = false;
      
      _sCmdAnswer = "";
      _arParAnswer = new byte[0];
      _wi = new WndInfo ();
      _nResponseID = 0;

      _sExcMsg = "";
    }

    /// <summary>
    /// Builds a message ( cmd-par ) string based on the message.
    /// </summary>
    /// <param name="s">The cmd-par string (output)</param>
    public void BuildString ( ref string s ) 
    {
      s = _sCmd;
      if ( _sPar.Length > 0 )
      {
        s += " ";
        s += _sPar;
      }
    }

    /// <summary>
    /// Builds a message based on a ( response ) byte array.
    /// </summary>
    /// <param name="arbyRead">The ( response ) byte array</param>
    public void ProcessAnswer ( byte[] arbyRead )
    {
      _sCmdAnswer = "";
      _arParAnswer = new byte[0];

      if ( arbyRead.Length == 0 ) return;

      ASCIIEncoding ae = new ASCIIEncoding ();
      string s = ae.GetString (arbyRead);
      int nPos = s.IndexOf ( ' ' );
      if ( -1 != nPos )
      {
        _sCmdAnswer = s.Substring ( 0, nPos );
        int nParAnswer = arbyRead.Length - (nPos+1);
        _arParAnswer = new byte[nParAnswer];
        Array.Copy(arbyRead, nPos+1, _arParAnswer, 0, nParAnswer);  
      }
      else
      {
        _sCmdAnswer = s;
        _arParAnswer = new byte[0];
      }
    }
    
    /// <summary>
    /// Gets the form, associated with the messages owner window handle.
    /// </summary>
    /// <returns>The associated form (null, if no form is associated)</returns>
    public Form GetOwnerForm ()
    {
      Form fOwner = null;
      IntPtr hWnd = this.WindowInfo.Owner;
      if (IntPtr.Zero != hWnd)
      {
        fOwner = (Form) Control.FromHandle (hWnd);
      }
      return fOwner;
    }

    /// <summary>
    /// Checks, whether an communication error occurred on device.
    /// </summary>
    public void CheckForCommError ()
    {
      System.Exception exc = null;
      
      // Check the message command response with regard to a communication error specifier
      string s = this.CommandResponse;
      if      (s.IndexOf (SZCOMMERROR_COMMAND) != -1) 
        // Device received an erroneous cmd ( TX PC -> RX device )
        exc = new CmdException ("Cmd failure - device");
      else if (s.IndexOf (SZCOMMERROR_CMDPARSTR) != -1) 
        // Device received an erroneous cmd-par-string ( TX PC -> RX device )
        exc = new CmdParException ("Cmd-Par failure - device");
      else if (s.IndexOf (SZCOMMERROR_CRC) != -1) 
        // CRC error (TX):  The CRC16, which was calculated on the PC, differs from the CRC16,
        //                  which was calculated on the device, after the device has received the request  
        exc = new CRC16Exception ("CRC16 failure - device");
      else if (s.IndexOf (SZCOMMERROR_BUF) != -1) 
      {
        // Error on RX input buffer filling ( TX PC -> RX device )
        exc = new InputBufferException ("Input buffer failure - device");
      }
 
      // React
      if (null != exc)
      {
        if (this.PermanentyCalled)
        {
          string sMsg = string.Format ("Not considered comm. error: ThreadProc() - {0}", exc.Message);
          Debug.WriteLine (sMsg);
        }
        else
          throw exc;
      }
    }
    
    /// <summary>
    /// Converts the value of this instance to its equivalent string representation.
    /// </summary>
    /// <returns>The string representation</returns>
    public override string ToString()
    {
      string s = "";
      this.BuildString (ref s);
      return s;
    }
    
    /// <summary>
    /// Indicates, whether the message is a 'Set' message or not:
    /// A 'Set' message returns no information from device, but only an 'OK' answer.
    /// </summary>
    /// <returns>True, if the message is a 'Set' message; False otherwise</returns>
    /// <remarks>
    /// Currently not needed.
    /// </remarks>
    public bool IsSetCmd ()
    {
      // Loop over all 'Set' messages 
      foreach (string s in AppComm.SetCmds)
      {
        // Check: Is the message command a 'Set' command?
        if (s == _sCmd) 
          return true;      // Yes
      }
      return false;         // No
    }
    
    #endregion // methods

  }//E - class CommMessage


  /// <summary>
  /// Class CommMessageList:
  /// Communication message list
  /// </summary>
  public class CommMessageList : IEnumerable
  {
    #region members & properties
    
    /// <summary>
    /// The list
    /// </summary>
    private ArrayList _Items = new ArrayList();

    #endregion  //members & properties

    #region indexer
    
    /// <summary>
    /// The indexer
    /// </summary>
    public CommMessage this[int index]
    {
      get { return (CommMessage)_Items[index]; }
    }

    #endregion  //indexer
    
    #region IEnumerable Members

    /// <summary>
    /// The enumerator
    /// </summary>
    /// <returns>The enumerator</returns>
    public IEnumerator GetEnumerator ()
    {
      return _Items.GetEnumerator ();
    }

    #endregion // IEnumerable Members
    
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public CommMessageList ()
    {
    }

    /// <summary>
    /// Destructor
    /// </summary>
    ~CommMessageList() 
    {
      Clear ();
    }
    
    #endregion // constructors

    #region handling

    /// <summary>
    /// Clears the list.
    /// </summary>
    public void Clear () 
    {
      lock (this) { _Items.Clear (); }
    }

    /// <summary>
    /// Adds a CommMessage object to the list.
    /// </summary>
    /// <param name="msg">The CommMessage object to be added</param>
    public void Add ( CommMessage msg ) 
    {
      lock (this) { _Items.Add ( msg ); }
    }
    
    /// <summary>
    /// Inserts a CommMessage object at the specified index into the list.
    /// </summary>
    /// <param name="idx">The zero-based index at which the item should be inserted</param>
    /// <param name="msg">The CommMessage object to be inserted</param>
    public void Insert (int idx, CommMessage msg )
    {
      lock (this) { _Items.Insert (idx, msg ); }
    }

    /// <summary>
    /// Removes the element at the specified index of the list.
    /// </summary>
    /// <param name="idx">The zero-based index at which the item should be removed</param>
    public void RemoveAt (int idx)
    {
      lock (this) { _Items.RemoveAt (idx); }
    }

    /// <summary>
    /// Gets the next message to be processed.
    /// </summary>
    /// <returns>The next message to be processed (null, if no such message)</returns>
    public CommMessage GetNext ()
    {
      CommMessage msg = null;

      lock ( this )
      {
        if ( this.Count > 0 )
        {
          msg = new CommMessage (this[0]);
          this.RemoveAt (0);
        }
      }

      return msg;
    }

    /// <summary>
    /// Gets the # of list elements.
    /// </summary>
    public int Count 
    {
      get { lock (this) { return _Items.Count; } }
    }

    /// <summary>
    /// Finds the first occurrence of a message with a given window owner.
    /// </summary>
    /// <param name="hWnd">The window handle</param>
    /// <returns>The position of this message in the list; -1, if no such message was found</returns>
    public int FindOwner ( IntPtr hWnd )
    {
      int pos = -1;

      lock ( this )
      {
        if ( this.Count > 0 )
        {
          // walk through the list
          for ( pos = 0; pos < this.Count; pos++ )
          {
            // get the message
            CommMessage msg = this[pos];
      
            // if the owner was found return the position
            if ( hWnd == msg.WindowInfo.Owner ) break;
          }
          if (pos == this.Count) pos = -1;
        }
      }

      return pos;
    }
    
    
    #endregion // handling

  }//E - class CommMessageList


  /// <summary>
  /// Class WndInfo:
  /// A Window information item
  /// </summary>
  public class WndInfo
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public WndInfo ()
    {
      _hOwner = IntPtr.Zero;
      _bShowWait = false;
    }
    
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="wi">The Window information object to be initialized with</param>
    public WndInfo (WndInfo wi)
    {
      _hOwner = wi._hOwner;
      _bShowWait = wi._bShowWait;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="hOwner">The owner window</param>
    /// <param name="bShowWait">True, if a Wait cursor should be shown during transmission; False otherwise</param>
    public WndInfo (IntPtr hOwner, bool bShowWait)
    {
      _hOwner = hOwner;
      _bShowWait = bShowWait;
    }

    #endregion constructors

    #region members

    /// <summary>
    /// The owner window
    /// </summary>
    IntPtr _hOwner = IntPtr.Zero;
    
    /// <summary>
    /// True, if a Wait cursor should be shown during transmission; False otherwise.
    /// </summary>
    bool _bShowWait = false;

    #endregion members

    #region properties

    /// <summary>
    /// The owner window
    /// </summary>
    public IntPtr Owner
    {
      get { return _hOwner; }
    }

    /// <summary>
    /// True, if a Wait cursor should be shown during transmission; False otherwise.
    /// </summary>
    public bool ShowWait
    {
      get { return _bShowWait; }
    }

    #endregion properties

  }//E - Class WndInfo
  
}
