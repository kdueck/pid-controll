using System;
using System.Threading;
using System.Collections;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;

using App;

namespace CommRS232
{
  /// <summary>
  /// CommRs232 Response Event handler declaration
  /// </summary>
  public delegate void CommRs232ResponseHandler (object sender, CommRs232ResponseEventArgs e);

  /// <summary>
  /// CommRs232 Response event args class
  /// </summary>
  public class CommRs232ResponseEventArgs : EventArgs
  {
    /// <summary>
    /// Constructor
    /// </summary>
    public CommRs232ResponseEventArgs (CommMessage msg)
    {
      m_msg = msg;
    }

    /// <summary>
    /// The response message
    /// </summary>
    CommMessage m_msg;

    /// <summary>
    /// The response message
    /// </summary>
    public CommMessage Message
    {
      get { return m_msg; }
    }
  }

  /// <summary>
  /// Class CommRS232:
  /// RS232 serial interface
  /// </summary>
  /// <remarks>
  /// TODO
  /// 1. Include: Binary command handling ('_bBinaryCmd' member of the 'CommMessage' class)
  /// </remarks>
  public abstract class CommRS232
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public CommRS232 ()
    {
    }

    #endregion constructors

    #region constants & enums

    /// <summary>
    /// The end char of a message string: 0x0D ( = '\r', = CR )
    /// </summary>
    const char ENDCHAR = '\r';
    /// <summary>
    /// The replacement char for the end char, as used in the device SW: 0x01
    /// (Means, That after receiving a byte array from the device, each replacement char encountered
    ///  must be re-replaced by the end char) 
    /// </summary>
    readonly char REPLCHAR = System.Convert.ToChar (0x01);

    /// <summary>
    /// Message response ID: Answer
    /// </summary>
    public const int CMID_ANSWER = 1;
    /// <summary>
    /// Message response ID: Empty
    /// </summary>
    public const int CMID_EMPTY = 2;
    /// <summary>
    /// Message response ID: Timeout
    /// </summary>
    public const int CMID_TIMEOUT = 3;
    /// <summary>
    /// Message response ID: String length error
    /// </summary>
    public const int CMID_STRLENERR = 4;

#if DEBUG
    /// <summary>
    /// Communication Timeout (unit: 10 ms): 5 sec
    /// </summary>
    const uint dwTIMEOUT = 500;
#else
    /// <summary>
    /// Communication Timeout (unit: 10 ms): 5 sec
    /// </summary>
    const uint dwTIMEOUT = 500;                 
#endif

    /// <summary>
    /// Length of the global RX buffer (in Bytes)
    /// </summary>
    const int RXBUFLEN = 16384;

    /// <summary>
    /// Length of the device RX buffer (in Byte)
    /// NOTE: This value must coincide with the RXBUFSIZE value in the device SW.
    /// </summary>
    public const int TXBUFSIZE = 160;
    /// <summary>
    /// Max. length of a parameter string
    /// Notes:
    ///   cmd-par-string syntax: 
    ///     "(cmd) (par)(crc,4B)\r"
    ///   with: 
    ///     - max. length of a cmd-par-string: 'TXBUFSIZE' B (defined above)
    ///     - max. length of a cmd:            15 B
    ///   we have: 
    ///     - max. length 'MAX_PAR_LEN' of a parameter string: 
    ///       TXBUFSIZE = 15 + 1 + MAX_PAR_LEN + 4 + 1 + 1 (string end recog.), 
    ///       MAX_PAR_LEN = TXBUFSIZE - 22
    ///   Ex.:
    ///     'TXBUFSIZE'=160 -> 'MAX_PAR_LEN'=138
    ///   
    ///   -> this value is merely symbolically, i.e. not used in the SW
    /// </summary>
    public const int MAX_PAR_LEN = TXBUFSIZE - 22;

    /// <summary>
    /// Max. number of comm. errors (CRC, Timeout) permitted 
    /// </summary>
    const int MAXCOMMERROR = 5;

    #endregion constants & enums

    #region members

    /// <summary>
    /// The serial port
    /// </summary>
    System.IO.Ports.SerialPort m_SerialPort = new System.IO.Ports.SerialPort ();

    /// <summary>
    /// The Thread object
    /// </summary>
    Thread m_pThread = null;

    /// <summary>
    /// Indicates, whether the thread should be aborted (True) or not (False)
    /// </summary>
    bool m_bAbort = false;

    /// <summary>
    /// Indicates, whether a transfer is in progres (True) or not (False)
    /// </summary>
    bool m_bTransferInProgress = false;

    /// <summary>
    /// Message handling: The currently processed message
    /// </summary>
    CommMessage m_msgCurrent = new CommMessage ();

    /// <summary>
    /// Message handling: The message list, which contains the messages to be transmitted
    /// </summary>
    CommMessageList m_lstTransmit = new CommMessageList ();

    /// <summary>
    /// The RX buffer
    /// </summary>
    byte[] m_RXBuffer = new byte[RXBUFLEN];

    /// <summary>
    /// The comm. error (exception) counter
    /// </summary>
    int m_nErr = 0;

    /// <summary>
    /// Indicates, whether the message list is locked for Write access (true) or not (false)
    /// </summary>
    bool m_bTransmitListLocked = false;

    /// <summary>
    /// The Exception message file 
    /// </summary>
    ExcMsgFile m_emf = new ExcMsgFile ();

    #endregion members

    #region methods

    //---------------------------------------------------------------
    // Information handling

    /// <summary>
    /// Checks, whether a communication channel is open or not.
    /// </summary>
    /// <returns>True, if open; False otherwise</returns>
    public bool IsOpen ()
    {
      bool bOpen = (null != m_pThread) && m_SerialPort.IsOpen;
      return bOpen;
    }

    /// <summary>
    /// Checks, whether a communication channel is closed or not.
    /// </summary>
    /// <returns>True, if closed; False otherwise</returns>
    public bool IsClosed ()
    {
      bool bClosed = (null == m_pThread) && !m_SerialPort.IsOpen;
      return bClosed;
    }

    /// <summary>
    /// Proofs whether the communication is idle ( im Leerlauf ) or not.
    /// </summary>
    /// <returns>True, if the communication is idle; otherwise false</returns>
    public bool IsIdle ()
    {
      // If a comm. channel doesn't exist, we are idle.
      if (!m_SerialPort.IsOpen)
        return true;
      // If a transfer is in progress (i.e. we are waiting for answer from the device), we are NOT idle.
      if (m_bTransferInProgress)
        return false;
      // If the message list is not empty, we are NOT idle.
      if (0 != MessageCount)
        return false;
      // Otherwise we are idle.
      return true;
    }

    //---------------------------------------------------------------
    // Thread: creation, destruction, procedure

    /// <summary>
    /// Creates the communication thread.
    /// </summary>
    /// <returns>True, if the thread was created successfully; False otherwise</returns>
    bool CreateThread ()
    {
      m_pThread = new Thread (new ThreadStart (ThreadProc));
      if (null == m_pThread)
      {
        Debug.Assert (false);
        return false;
      }
      m_pThread.Priority = ThreadPriority.Lowest;
      m_pThread.Start ();
      return true;
    }

    /// <summary>
    /// Delegate for the 'SetCursor' method invocation
    /// </summary>
    delegate void SetCursorDelegate (Form f, System.Windows.Forms.Cursor cursor);
    /// <summary>
    /// Sets a specified cursor shape with respect to a given form. 
    /// </summary>
    /// <param name="f">The form</param>
    /// <param name="cursor">The cursor shape</param>
    void SetCursor (Form f, System.Windows.Forms.Cursor cursor)
    {
      // Invocation required?
      if (f.InvokeRequired)
        // Yes: Invoke synchronously
        f.Invoke (new SetCursorDelegate (SetCursor), new object[] { f, cursor });
      else
        // No
        f.Cursor = cursor;
    }

    /// <summary>
    /// Communication thread procedure.
    /// </summary>
    void ThreadProc ()
    {
      CommMessage msg;
      string s = "";
      byte[] arby;
      byte[] arbyPreprocessed;

      // Endless loop
      while (!m_bAbort)
      {
        // Wait, until the communication channel has been opened
        if (!m_SerialPort.IsOpen)
        {
          Thread.Sleep (100);
          continue;
        }

        // Wait, until a message should be processed
        msg = m_lstTransmit.GetNext ();
        if (null == msg)
        {
          Thread.Sleep (100);
          continue;
        }
        // Remember this message as the currently processed message
        m_msgCurrent = msg;

        try
        {
          // Exception message file: Append data
          m_emf.Append (msg, AppendSpec.OnEntrance);

          // Build the message string
          msg.BuildString (ref s);

          // Indicate: A transfer is in progres (Cursor: Wait, if req.)
          m_bTransferInProgress = true;
          if (msg.WindowInfo.ShowWait)
          {
            Form fOwner = msg.GetOwnerForm ();
            if (null != fOwner)
              SetCursor (fOwner, Cursors.WaitCursor);
          }

          // Before TX: Build the CRC16 from the message string, and append it to the corr'ing byte array
          BuildCRC16 (s, out arby);

          // Write the byte array to the communication channel
          WriteString (arby);
          // Read the ( response ) byte array from the communication channel
          ReadBytes (out arby);

          // After RX: Calculate the CRC16 of the response byte array, and compare it with the transmitted one
          if (!CompareCRC16 (ref arby))
          {
            // CRC error (RX):  The CRC16, which was calculated on the device, differs from the CRC16,
            //                  which was calculated on the PC, after the PC has received the response  

            // Notes, JM, 3.12.13:
            // If this (PC-sided) comm. error is reported here, we do NOT know, whether the message
            // has been executed on device or nor - that means, the message should be repeated in any case
            // true to the motto "In case of doubt once again ...". Hence the following 'Set' message check
            // is incorrect and must not be performed. 

            // Check, whether the message is a 'Set' message or not
            // (A 'Set' message returns no information from device, but only an 'OK' answer)
            //            if (!msg.IsSetCmd ())  
            throw new CRC16Exception ("CRC failure - PC");    // No, it isn't: once again ...
          }

          // Indicate: A transfer is no longer in progres (Cursor: Default, if req.)
          m_bTransferInProgress = false;
          if (msg.WindowInfo.ShowWait)
          {
            Form fOwner = msg.GetOwnerForm ();
            if (null != fOwner)
              SetCursor (fOwner, Cursors.Default);
          }

          // Preprocess the ( response ) byte array
          PreprocessAnswer (arby, out arbyPreprocessed);

          // Fill the response members of the message, based on the preprocessed ( response ) byte array
          msg.ProcessAnswer (arbyPreprocessed);

          // Check, whether an comm. error occurred on device
          msg.CheckForCommError ();

          // Exception message file: Append data
          m_emf.Append (msg, AppendSpec.OnSuccess);

          // The owner window of the message
          IntPtr hWnd = msg.WindowInfo.Owner;
          // The # of messages with this owner window in the message list
          int pos = m_lstTransmit.FindOwner (hWnd);

          // Exception message file: Append special data
          m_emf.AppendSpecial (hWnd, pos);

          // Check: Does an owner window exist?
          if (IntPtr.Zero != hWnd)
          {
            // Yes: Send the message with the 'Answer' notification to this window
            msg.ResponseID = CMID_ANSWER;
            OnCommRs232ResponseHandler (msg);

            // Exception message file: Append helper stuff
            m_emf.Append (msg, AppendSpec.OnThreadProc_CMID_ANSWER);

            // Check: Are there more messages with this owner window in the message list?
            if (-1 == pos)
            {
              // No: Send the message with the 'Empty' notification   
              msg.ResponseID = CMID_EMPTY;
              OnCommRs232ResponseHandler (msg);

              // Exception message file: Append helper stuff
              m_emf.Append (msg, AppendSpec.OnThreadProc_CMID_EMPTY);
            }
          }

          // Reset comm. error counter
          m_nErr = 0;
        }
        catch (System.Exception exc)
        {
          // Error:
          string sMsg = string.Format ("Error: ThreadProc()\r\n{0}", exc.Message);
          Debug.WriteLine (sMsg);

          // CD: Exception type
          bool bSendErrNotif = true;
          bool bExc = exc is CmdException
            || exc is CmdParException
            || exc is CRC16Exception
            || exc is InputBufferException
            || exc is TimeoutException;
          if (bExc)
          {
            m_nErr++;                             // Incr. comm. error counter
            if (m_nErr < MAXCOMMERROR)
            {                                     // Error limit not yet reached:  
              bSendErrNotif = false;              //  Indicate, that a window error notification should be suppressed
              Thread.Sleep (100);               //  Wait a little
              RepeatLastMessage ();               //  Repeat the message lastly transmitted
            }
            else
            {                                     // Error limit reached:
              m_nErr = 0;                           //  Reset comm. error counter  
            }
          }

          // Exception message file: Append helper stuff
          m_emf.ExcName = exc.Message;
          m_emf.Append (msg, AppendSpec.OnThreadProc_Catch);

          // Check: Should a window error notification be transmitted?
          if (bSendErrNotif)
          {
            // Yes:

            // Indicate: A transfer is no longer in progres (Cursor: Default, if req.)
            m_bTransferInProgress = false;
            if (msg.WindowInfo.ShowWait)
            {
              Form fOwner = msg.GetOwnerForm ();
              if (null != fOwner)
                SetCursor (fOwner, Cursors.Default);
            }

            // Lock the message list for Write access & clear it
            m_bTransmitListLocked = true;
            m_lstTransmit.Clear ();

            // Fill the messages 'ExcMsg' member
            msg.ExcMsg = exc.ToString () + ": " + exc.Message;
            // Exception message file: Append data
            m_emf.Append (msg, AppendSpec.OnError);

            // Send the message with the 'Timeout' notification to the window
            IntPtr hWnd = msg.WindowInfo.Owner;
            if (IntPtr.Zero != hWnd)
            {
              msg.ResponseID = CMID_TIMEOUT;
              OnCommRs232ResponseHandler (msg);
            }
          }

        } //E - catch

      } //E - while ( !m_bAbort )

      m_bAbort = false;
      m_SerialPort.Close ();
      m_pThread = null;
    }



    //---------------------------------------------------------------
    // Message handling

    /// <summary>
    /// Transmits a message to the communication channel
    /// </summary>
    /// <param name="msg">The message</param>
    /// <returns>True, if the message was succ'ly added</returns>
    public void WriteMessage (CommMessage msg)
    {
      if (m_SerialPort.IsOpen)
      {
        // Check the message:
        //  If the message doesn't contain a command (Length = 0), or if the message string is too long 
        //  in order to be received well by the device UART (Length > TXBUFSIZE-6, '6': 1 char string 
        //  end recognition, 1 char End recognition for serial comm., 4 chars CRC16), 
        //  then don't add it to the message list & exit corr'ly.
        string sMsg = msg.ToString ();
        if ((sMsg.Length == 0) || (sMsg.Length > TXBUFSIZE - 6))
        {
          // String length error: Send the message with the 'String length error' notification   
          msg.ResponseID = CMID_STRLENERR;
          OnCommRs232ResponseHandler (msg);
        }
        else
        {
          // Add the message to the message list 
          // (only, if the message list is not locked for Write access)
          if (!m_bTransmitListLocked)
            m_lstTransmit.Add (msg);
        }
      }
    }

    /// <summary>
    /// Repeats the message lastly transmitted.
    /// </summary>
    public void RepeatLastMessage ()
    {
      if (m_SerialPort.IsOpen)
      {
        // Insert the message on top of the message list 
        // (only, if the message list is not locked for Write access)
        if (!m_bTransmitListLocked)
          m_lstTransmit.Insert (0, m_msgCurrent);
      }
    }

    /// <summary>
    /// Waits until the communication is empty.
    /// </summary>
    /// <param name="timeout">
    /// If 0, a wait dialog is included only when the communication is not idle.
    /// If greater than 0, a wait dialog is always included.
    /// </param>
    public void WaitForEmpty ()
    {
      // If the communication is already idle, we are done.
      if (IsIdle ())
        return;

      // Show the 'Communication wait' dialog
      // (This dialog waits until the communication is idle.)
      CommChannelWaitForm dlg = new CommChannelWaitForm (this);
      Form fOwner = null;
      try
      {
        fOwner = m_msgCurrent.GetOwnerForm ();
      }
      catch { }
      if (null != fOwner)
        dlg.ShowDialog (fOwner);  // Use this method in order to secure the centering of the dialog above its owner 
      else
        dlg.ShowDialog ();
      dlg.Dispose ();
    }


    //---------------------------------------------------------------
    // Communication handling

    /// <summary>
    /// Initializes and opens a serial communication channel
    /// </summary>
    /// <param name="szChannel"></param>
    /// <param name="dwBaudRate"></param>
    public void OpenChannel (string szChannel, UInt32 dwBaudRate)
    {
      try
      {
        // Configure & open the serial port
        m_SerialPort.BaudRate = (int)dwBaudRate;
        m_SerialPort.PortName = szChannel;
        m_SerialPort.Parity = System.IO.Ports.Parity.None;
        m_SerialPort.DataBits = 8;
        m_SerialPort.StopBits = System.IO.Ports.StopBits.One;
        m_SerialPort.Handshake = System.IO.Ports.Handshake.None;
        m_SerialPort.ReadTimeout = 500;  // earlier: to.ReadIntervalTimeout         = 0xFFFFFFFF;
        m_SerialPort.WriteTimeout = 500; // earlier: to.WriteTotalTimeoutConstant   = 5000;
        m_SerialPort.Open ();

        // Clear the transmit message list
        m_lstTransmit.Clear ();
        // Indicate, that the message list is not locked for Write access
        m_bTransmitListLocked = false;

        // Reset the abort condition
        m_bAbort = false;

        // Reset the comm. error counter
        m_nErr = 0;

        // Create the communication thread
        CreateThread ();
      }
      catch
      {
        // In case of error: Close the communication channel
        if (m_SerialPort.IsOpen)
          m_SerialPort.Close ();
        throw;
      }
    }

    /// <summary>
    /// Closes a serial communication channel
    /// </summary>
    public void CloseChannel ()
    {
      m_bAbort = true;
      m_lstTransmit.Clear ();

      // Show the 'Communication wait' dialog
      // (This dialog waits until the communication is idle.)
      try
      {
        CommChannelWaitForm dlg = new CommChannelWaitForm (this);
        Form fOwner = m_msgCurrent.GetOwnerForm ();
        if (null != fOwner)
          dlg.ShowDialog (fOwner);  // Use this method in order to secure the centering of the dialog above its owner 
        else
          dlg.ShowDialog ();
        dlg.Dispose ();
      }
      catch
      {
      }

      // Reset the comm. error counter
      m_nErr = 0;
    }


    //---------------------------------------------------------------
    // String handling

    /// <summary>
    /// Preprocesses the ( response ) byte array.
    /// </summary>
    /// <param name="arbyRead">The ( response ) byte array</param>
    /// <param name="arbyPreprocessed">The preprocessed ( response ) byte array</param>
    void PreprocessAnswer (byte[] arbyRead, out byte[] arbyPreprocessed)
    {
      // Init.
      if (arbyRead.Length == 0)
      {
        arbyPreprocessed = new byte[0];
        return;
      }
      int nPreprocessed = arbyRead.Length;

      // Check: Does the current message request a binary response or not?
      if (m_msgCurrent.BinaryCommand)
      {
        // The current message requests a binary response:
        // (Message string format: "<cmd>SPACE<parSize:4><par>")
        int i, idx1 = -1;

        // Remove the leading 4 Filesize bytes
        for (i = 0; i < arbyRead.Length; i++)
        {
          if (arbyRead[i] == 0x20) { idx1 = i; break; }
        }
        if (-1 != idx1)
        {
          for (i = idx1 + 1 + 4; i < arbyRead.Length; i++)
            arbyRead[i - 4] = arbyRead[i];
          nPreprocessed -= 4;
        }
      }
      else
      {
        // The current message does NOT request a binary response:
        // (Message string format: "<cmd>SPACE<par>\r")

        // Re-replacement 'REPL_CHAR' -> 'END_CHAR'
        // Notes:
        //  In the byte array, received from the device, all occurrences of the 'END_CHAR' char
        //  are replaced by the 'REPL_CHAR' char, that means, that afterwards on PC (namely HERE)
        //  a re-replacement 'REPL_CHAR' -> 'END_CHAR' must take place.
        byte endByte = Convert.ToByte (ENDCHAR);
        byte replByte = Convert.ToByte (REPLCHAR);
        for (int i = 0; i < arbyRead.Length; i++)
        {
          if (arbyRead[i] == replByte)
            arbyRead[i] = endByte;
        }
      }

      // Allocate & fill the output byte array
      arbyPreprocessed = new byte[nPreprocessed];
      Array.Copy (arbyRead, 0, arbyPreprocessed, 0, nPreprocessed);
    }

    /// <summary>
    /// Reads a byte array from a serial communication channel.
    /// </summary>
    /// <param name="arbyRead">The byte array read</param>
    void ReadBytes (out byte[] arbyRead)
    {
      try
      {
        // Initiate 
        UInt32 dwTimeCount = 0;       // The timeout counter
        bool bExit = false;           // 'Read completed' switch
        int nReadCount = 0;           // The # of bytes in the output byte array
        int nTotalBytes = 0;          // Binary response mode only: The total number of Bytes that must be received

        // Loop, until the output byte array was completely read
        while (!bExit)
        {
          // Check: Timeout?
          if (dwTIMEOUT < dwTimeCount)
            throw new TimeoutException ();  // Yes.

          // No:
          // Check: Are there any bytes received but not yet read?
          int dwByteToRead = m_SerialPort.BytesToRead;
          if (0 == dwByteToRead)
          {
            // No:
            // Incr. the Timeout counter, if req.
            if (this.m_msgCurrent.SuspendRXTimeoutHandling == false)
              dwTimeCount++;
            // Wait 10 ms ...
            Thread.Sleep (10);
            // ... and continue
            continue;
          }

          // Yes:
          // Read the received bytes
          byte[] byBuffer = new byte[dwByteToRead];
          m_SerialPort.Read (byBuffer, 0, dwByteToRead);

          // Reset Timeout counter
          dwTimeCount = 0;

          // Update the output byte array, quit the loop in case of end char 
          // Check: Does the current message request a binary response or not?
          if (m_msgCurrent.BinaryCommand)
          {
            // The current message requests a binary response:
            // (Message string format: "<cmd>SPACE<parSize:4><par>")

            // Get the # of transmitted bytes
            int nBuffer = byBuffer.Length;
            // Check: RX buffer completely filled?
            if (nReadCount + nBuffer > m_RXBuffer.Length)
            {
              // Yes: Reallocate it
              byte[] arbyReadNew = new byte[m_RXBuffer.Length + RXBUFLEN];
              Array.Copy (m_RXBuffer, 0, arbyReadNew, 0, nReadCount);
              m_RXBuffer = arbyReadNew;
            }
            // Update the RX buffer
            Array.Copy (byBuffer, 0, m_RXBuffer, nReadCount, nBuffer);
            nReadCount += nBuffer;
            // Check, whether the total number of Bytes, that must be received, is already available or not 
            if (nTotalBytes == 0)
            {
              // The total number of Bytes, that must be received, is NOT yet available:
              // Check, whether the cmd-par separator was already transmitted
              int i;
              for (i = 0; i < nReadCount; i++)
              {
                if (m_RXBuffer[i] == 0x20) break;
              }
              if (i < nReadCount)
              {
                // The cmd-par separator was already transmitted:
                // Adjust the end index of the Filesize bytes (4B)
                int nFilesizeEndIdx = i + 4;
                // Check, whether the Filesize bytes are already available
                if (nReadCount >= nFilesizeEndIdx + 1)
                {
                  // Yes, they are:
                  // Calculate the Filesize
                  int nFilesize = (int)m_RXBuffer[nFilesizeEndIdx];
                  for (int j = 0; j < 3; j++)
                    nFilesize = 256 * nFilesize + (int)m_RXBuffer[nFilesizeEndIdx - (j + 1)];
                  // Calculate the total number of Bytes, that must be received
                  nTotalBytes = (nFilesizeEndIdx + 1) + nFilesize;
                }
              }
            }
            // Check: All bytes received?
            if (nTotalBytes == nReadCount)
              bExit = true;         // Yes: Prepare the exit of the read loop
          }
          else
          {
            // The current message does NOT request a binary response:
            // (Message string format: "<cmd>SPACE<par>\r")

            // Check: End char received?
            int nBuffer = byBuffer.Length;
            char cEndChar = Convert.ToChar (byBuffer[nBuffer - 1]);
            if (ENDCHAR == cEndChar)
            {
              // Yes:
              // Notes:
              //  The end char will NOT be taken over into the RX buffer.
              nBuffer--;              // Skip the end char 
              bExit = true;           // Prepare the exit of the read loop
            }
            // Check: RX buffer completely filled?
            if (nReadCount + nBuffer > m_RXBuffer.Length)
            {
              // Yes: Reallocate it
              byte[] arbyReadNew = new byte[m_RXBuffer.Length + RXBUFLEN];
              Array.Copy (m_RXBuffer, 0, arbyReadNew, 0, nReadCount);
              m_RXBuffer = arbyReadNew;
            }
            // Update the RX buffer
            Array.Copy (byBuffer, 0, m_RXBuffer, nReadCount, nBuffer);
            nReadCount += nBuffer;
          }

        } // while ( !bExit )

        // Allocate & fill the output byte array
        arbyRead = new byte[nReadCount];
        Array.Copy (m_RXBuffer, 0, arbyRead, 0, nReadCount);
      }
      catch
      {
        arbyRead = null;
        throw;
      }
    }

    /// <summary>
    /// Writes a byte array to a serial communication channel.
    /// </summary>
    /// <param name="arbyWithCRC">The byte array to be transmitted</param>
    void WriteString (byte[] arbyWithCRC)
    {
      try
      {
        // Provide the byte array to be transmitted with the comm. end char
        byte[] byBuffer = new byte[arbyWithCRC.Length + 1];
        arbyWithCRC.CopyTo (byBuffer, 0);
        byBuffer[arbyWithCRC.Length] = (byte)ENDCHAR;
        // Write the byte array to the communication device
        m_SerialPort.Write (byBuffer, 0, byBuffer.Length);
      }
      catch
      {
        Debug.WriteLine ("WriteString () failed.");
        throw;
      }
    }

    //---------------------------------------------------------------
    // ComPort enumerating

    /// <summary>
    /// Gets the available communication ports of the PC.
    /// </summary>
    /// <returns>A list containing the available communication ports</returns>
    public static ArrayList GetAvailableComPorts ()
    {
      string[] ars = System.IO.Ports.SerialPort.GetPortNames ();
      ArrayList al = new ArrayList (ars);
      return al;
    }

    //---------------------------------------------------------------
    // CRC16 handling

    /// <summary>
    /// Builds the CRC16 from the given string, and appends it to the corr'ing byte array (sequence MSB to LSB).
    /// </summary>
    /// <param name="s">The string</param>
    /// <param name="arbyWithCRC">The corr'ing byte array (Out)</param>
    void BuildCRC16 (string s, out byte[] arbyWithCRC)
    {
      // String as byte array
      ASCIIEncoding ae = new ASCIIEncoding ();
      byte[] arby = ae.GetBytes (s);
      // Calc. its CRC16
      UInt16 crc = CRC16.crc16 (arby, CRC16.Types.OneProcess);
      // Append the CRC's byte array representation to the byte array
      byte[] arby_crc;
      CRC16_ToByteArray (crc, out arby_crc);
      arbyWithCRC = new byte[arby.Length + arby_crc.Length];
      Array.Copy (arby, 0, arbyWithCRC, 0, arby.Length);
      Array.Copy (arby_crc, 0, arbyWithCRC, arby.Length, arby_crc.Length);
    }

    /// <summary>
    /// Converts a CRC16 number into a byte array of length 4 (sequence MSB to LSB).
    /// </summary>
    /// <param name="crc">The CRC16 number</param>
    /// <param name="arby">The byte array (Out)</param>
    void CRC16_ToByteArray (UInt16 crc, out byte[] arby)
    {
      UInt16 div;
      byte cin, cout;
      int i;

      // Loop over all powers (from highest to lowest)
      arby = new byte[4];
      for (i = 0, div = 4096; i < 4; i++, div >>= 4)
      {
        cin = (byte)(crc / div);                 // current power
        if (cin < 10) cout = (byte)(cin + 48);    // numbers 0..9 -> ASCII chars '0'..'9'
        else cout = (byte)(cin + 55);    // numbers A..F -> ASCII chars 'A'..'F'
        arby[i] = cout;                         // fill byte array
        crc -= (UInt16)(cin * div);               // next power ...
      }
    }

    /// <summary>
    /// Calculates the CRC16 of the response byte array, and compares it with the transmitted one. 
    /// </summary>
    /// <param name="arby">The response byte array (In/Out)</param>
    /// <returns>True, if both CRC's coincide; False otherwise</returns>
    bool CompareCRC16 (ref byte[] arby)
    {
      // Get the target (Soll) CRC: Last 4 bytes of the response byte array
      byte[] arby_crc = new byte[4];
      Array.Copy (arby, arby.Length - 4, arby_crc, 0, 4);
      UInt16 crc_target = CRC16_FromByteArray (arby_crc);
      // Clean up the response byte array: Clear the target CRC bytes 
      byte[] arby_answer = new byte[arby.Length - 4];
      Array.Copy (arby, 0, arby_answer, 0, arby.Length - 4);
      // Calc. the actual (Ist) CRC 
      UInt16 crc_actual = CRC16.crc16 (arby_answer, CRC16.Types.OneProcess);
      // Compare both CRC's
      if (crc_target != crc_actual)
      {
        // CRC error
        return false;
      }
      // Reassign the response byte array after cleaning up
      arby = arby_answer;
      return true;
    }

    /// <summary>
    /// Converts a byte array of length 4 (sequence MSB to LSB) into a CRC16 number.
    /// </summary>
    /// <param name="arby">The byte array</param>
    /// <returns>The CRC16 number</returns>
    UInt16 CRC16_FromByteArray (byte[] arby)
    {
      UInt16 crc, fac;
      byte cin, cout;
      int i;

      // Loop over the byte array elements
      for (i = 0, crc = 0, fac = 4096; i < 4; i++, fac >>= 4)
      {
        cout = arby[i];                         // current element (ASCII char)
        if (cout < 65) cin = (byte)(cout - 48);  // ASCII chars '0'..'9' -> numbers 0..9
        else cin = (byte)(cout - 55);  // ASCII chars 'A'..'F' -> numbers A..F 
        crc += (UInt16)(cin * fac);               // Update the CRC16 number
      }
      return crc;
    }

    #endregion methods

    #region properties

    /// <summary>
    /// Gets the number of messages residing in the message list
    /// </summary>
    public int MessageCount
    {
      get { return this.m_lstTransmit.Count; }
    }

    /// <summary>
    /// The comm. error (exception) counter
    /// </summary>
    public int ErrorCount
    {
      get { return this.m_nErr; }
    }

    /// <summary>
    /// The Exception message file
    /// </summary>
    public ExcMsgFile ExcMsgFile
    {
      get { return m_emf; }
    }

    #endregion properties

    #region events

    /// <summary>
    /// CommRs232 Response event declaration
    /// </summary>
    public event CommRs232ResponseHandler CommRs232Response;

    /// <summary>
    /// Function called on CommRs232 Response event triggering
    /// </summary>
    /// <param name="msg">The response message</param>
    public void OnCommRs232ResponseHandler (CommMessage msg)
    {
      if (null != CommRs232Response)
      {
        CommRs232Response (this, new CommRs232ResponseEventArgs (msg));
      }
    }

    #endregion events

  }
}
