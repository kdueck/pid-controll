using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;

namespace CommRS232
{
	/// <summary>
	/// Class CommChannelWaitForm:
	/// Wait for Idle dialog
	/// </summary>
  public class CommChannelWaitForm : System.Windows.Forms.Form
  {
    private System.Windows.Forms.Timer _Timer;
    private System.Windows.Forms.PictureBox _PictureBox;
    private System.ComponentModel.IContainer components;

    /// <summary>
    /// Constructor
    /// </summary>
    public CommChannelWaitForm (CommRS232 comm)
    {
      InitializeComponent();

      // Init
      _comm = comm;                           // Communication channel
      _dwTimeout = 1000;                      // Timespan for Timeout ( 1 sec )
     
      _arsPath = new string[2];               // Array of paths to the bitmap ressources
      string path = "App.CommRS232.Images.";
      _arsPath[0] = path + "Comm_0.bmp";
      _arsPath[1] = path + "Comm_1.bmp";
      _nCurrIdx = 0;                          // Index of the current bitmap path
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this._Timer = new System.Windows.Forms.Timer(this.components);
      this._PictureBox = new System.Windows.Forms.PictureBox();
      this.SuspendLayout();
      // 
      // _Timer
      // 
      this._Timer.Interval = 500;
      this._Timer.Tick += new System.EventHandler(this._Timer_Tick);
      // 
      // _PictureBox
      // 
      this._PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
      this._PictureBox.Location = new System.Drawing.Point(0, 0);
      this._PictureBox.Name = "_PictureBox";
      this._PictureBox.Size = new System.Drawing.Size(258, 40);
      this._PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this._PictureBox.TabIndex = 2;
      this._PictureBox.TabStop = false;
      // 
      // CommChannelWaitForm
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.ClientSize = new System.Drawing.Size(258, 40);
      this.Controls.Add(this._PictureBox);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "CommChannelWaitForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CommChannelWaitForm_KeyPress);
      this.Load += new System.EventHandler(this.CommChannelWaitForm_Load);
      this.Closed += new System.EventHandler(this.CommChannelWaitForm_Closed);
      this.ResumeLayout(false);

    }

    #endregion

    #region event handling

    /// <summary>
    /// 'Load' event of the form
    /// </summary>
    private void CommChannelWaitForm_Load(object sender, System.EventArgs e)
    {
      Debug.Assert ( null != _comm );

      // Show 1. hourglass
      string path = _arsPath[_nCurrIdx];
      this._PictureBox.Image = LoadBitmap ( path );
      // Update the index of the current bitmap path 
      _nCurrIdx++;
      if (_nCurrIdx > _arsPath.Length-1) _nCurrIdx = 0; 

      // Set Timeout counter (for counting down)
      _dwTimeoutCount = (UInt32) (_dwTimeout / _Timer.Interval);
      
      // Enable timer
      _Timer.Enabled = true;
    }	

    /// <summary>
    /// 'Closed' event of the form
    /// </summary>
    private void CommChannelWaitForm_Closed(object sender, System.EventArgs e)
    {
      // Disable timer
      _Timer.Enabled = false;
    }

    /// <summary>
    /// 'KeyPress' event of the form
    /// </summary>
    private void CommChannelWaitForm_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
    {
      // Close the form on pressing 'Enter' or 'Escape'
      if ( e.KeyChar == (char) Keys.Enter || e.KeyChar == (char) Keys.Escape )
      {
        this.DialogResult = DialogResult.Cancel;
        this.Close ();
      }
    }
    
    /// <summary>
    /// 'Tick' event of the _Timer control
    /// </summary>
    private void _Timer_Tick(object sender, System.EventArgs e)
    {
      // Show hourglasses in a circular manner
      string path = _arsPath[_nCurrIdx];
      this._PictureBox.Image = LoadBitmap ( path );
      // Update the index of the current bitmap path 
      _nCurrIdx++;
      if (_nCurrIdx > _arsPath.Length-1) _nCurrIdx = 0; 
    
      // Timeout count down
      if ( 0 != _dwTimeoutCount )
      {
        _dwTimeoutCount--;
        return;
      }
      
      // Close the form, when the communication channel is idle
      if ( _comm.IsIdle () )
      {
        DialogResult = DialogResult.OK;
        Close ();
      }
    }

    #endregion event handling

    #region members
    
    /// <summary>
    /// The Index of the current bitmap path
    /// </summary>
    int _nCurrIdx             = 0;      
    
    /// <summary>
    /// The array of paths to the bitmap ressources
    /// </summary>
    string[] _arsPath         = null;   

    /// <summary>
    /// The communication object
    /// </summary>
    CommRS232 _comm           = null;  

    /// <summary>
    /// The timespan for Timeout (in msec)
    /// </summary>
    UInt32 _dwTimeout         = 0;      
    
    /// <summary>
    /// The timer counter
    /// </summary>
    UInt32 _dwTimeoutCount    = 0;      

    #endregion members

    #region methos

    /// <summary>
    /// Loads a bitmap from the embedded resource of the executed assembly.
    /// </summary>
    /// <param name="fullpath">Full path of the bitmap resource</param>
    /// <returns>The bitmap</returns>
    Bitmap LoadBitmap ( string fullpath )
    {
      Bitmap bmp = null;
      try
      {
        System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
        System.IO.Stream stream = asm.GetManifestResourceStream (fullpath);
        bmp = new Bitmap ( stream );
      }
      catch
      {
      }
      return bmp;
    }

    #endregion methos

  }
}
 