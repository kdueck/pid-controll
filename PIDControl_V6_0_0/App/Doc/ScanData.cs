using System;
using System.Collections;

namespace App
{
  /// <summary>
  /// Class ScanData:
  /// Scan data list
  /// </summary>
  public class ScanData : IEnumerable
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScanData ()
    {
    }

    #endregion constructors

    #region members
    
    /// <summary>
    /// The list
    /// </summary>
    private ArrayList _Items = new ArrayList();
 
    #endregion members

    #region methods

    /// <summary>
    /// Clears the list
    /// </summary>
    public void Clear () 
    {
      _Items.Clear ();
    }

    /// <summary>
    /// Adds a value to the list
    /// </summary>
    /// <param name="f">The value to be added</param>
    public void Add ( float f ) 
    {
      _Items.Add ( f );
    }
    
    /// <summary>
    /// Copies the elements of the list to a new array of the 'float' type.
    /// </summary>
    /// <returns>The array</returns>
    public float[] ToFloatArray ()
    {
      try
      {
        return (float[]) _Items.ToArray (typeof(float));
      }
      catch
      {
        return new float[0];
      }
    }
 
    #endregion methods

    #region properties
    
    /// <summary>
    /// Gets the size of the list
    /// </summary>
    public int Count 
    {
      get { return _Items.Count; }
    }

    #endregion properties

    #region indexer
    
    /// <summary>
    /// The indexer
    /// </summary>
    public float this[int index]
    {
      get { return (float)_Items[index]; }
    }

    #endregion indexer
    
    #region IEnumerable Members

    /// <summary>
    /// The enumerator
    /// </summary>
    /// <returns>The enumerator</returns>
    public IEnumerator GetEnumerator ()
    {
      return _Items.GetEnumerator ();
    }

    #endregion IEnumerable Members

  }

}
