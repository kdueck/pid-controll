using System;

namespace App
{
	
  /// <summary>
  /// Class ScriptCommand:
  /// Represents a script command
  /// </summary>
  public class ScriptCommand
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScriptCommand()
    {
      m_sCommand = "";
      m_bCBcmd = true;
      m_nParNumber = 1;
      m_nMin1=0;
      m_nMax1=0;
      m_nMin2=0;
      m_nMax2=0;
    }
    
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="sc">The ScriptCommand the instance should be initialized with</param>
    public ScriptCommand(ScriptCommand sc)
    {
      m_sCommand = sc.m_sCommand;
      m_bCBcmd = sc.m_bCBcmd;
      m_nParNumber = sc.m_nParNumber;
      m_nMin1=sc.m_nMin1;
      m_nMax1=sc.m_nMax1;
      m_nMin2=sc.m_nMin2;
      m_nMax2=sc.m_nMax2;
    }
    
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="sCommand">The script command name</param>
    /// <param name="bCBcmd">Indicates, whether its parameters should be input via a TextBox (false) or via a ComboBox (true)</param>
    /// <param name="nParNumber">The number of parameters</param>
    /// <param name="nMin1">The minimum of the 1. parameter range</param>
    /// <param name="nMax1">The maximum of the 1. parameter range</param>
    /// <param name="nMin2">The minimum of the 2. parameter range</param>
    /// <param name="nMax2">The maximum of the 2. parameter range</param>
    public ScriptCommand(string sCommand, bool bCBcmd, int nParNumber, int nMin1, int nMax1, int nMin2, int nMax2)
    {
      m_sCommand = sCommand;
      m_bCBcmd = bCBcmd;
      m_nParNumber = nParNumber;
      m_nMin1=nMin1;
      m_nMax1=nMax1;
      m_nMin2=nMin2;
      m_nMax2=nMax2;
    }
 
    #endregion constructors

    #region members

    /// <summary>
    ///  The script command name
    /// </summary>
    string m_sCommand; 
    
    /// <summary>
    /// Indicates, whether its parameters should be input via a TextBox (false) or via a ComboBox (true)
    /// </summary>
    bool m_bCBcmd ;     
    
    /// <summary>
    /// The number of parameters
    /// </summary>
    int m_nParNumber;   
    
    /// <summary>
    /// The minimum of the 1. parameter range
    /// </summary>
    int m_nMin1;        
    
    /// <summary>
    /// The maximum of the 1. parameter range
    /// </summary>
    int m_nMax1;       
    
    /// <summary>
    /// The minimum of the 2. parameter range
    /// </summary>
    int m_nMin2;       

    /// <summary>
    /// The maximum of the 2. parameter range
    /// </summary>
    int m_nMax2;       

    #endregion members

    #region properties

    /// <summary>
    /// The script command name
    /// </summary>
    public string Command { get { return m_sCommand; } }
    /// <summary>
    /// Indicates, whether its parameters should be input via a TextBox (false) or via a ComboBox (true)
    /// </summary>
    public bool CBcmd { get { return m_bCBcmd; } }
    /// <summary>
    /// The number of parameters
    /// </summary>
    public int ParNumber { get { return m_nParNumber; } }
    /// <summary>
    /// The minimum of the 1. parameter range
    /// </summary>
    public int Min1 { get { return m_nMin1; } }
    /// <summary>
    /// The maximum of the 1. parameter range
    /// </summary>
    public int Max1 { get { return m_nMax1; } }
    /// <summary>
    /// The minimum of the 2. parameter range
    /// </summary>
    public int Min2 { get { return m_nMin2; } }
    /// <summary>
    ///  The maximum of the 2. parameter range
    /// </summary>
    public int Max2 { get { return m_nMax2; } }

    #endregion properties

    #region methods

    /// <summary>
    /// Returns a String that represents the current Object.
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      return m_sCommand.Trim ();
    }

    #endregion methods
  }

  
  /// <summary>
	/// Class ScriptCommands:
	/// Management of the (available) script commands
	/// </summary>
	public class ScriptCommands
	{
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    private ScriptCommands()
    {
    }

    #endregion constructors
    
    #region members
    
    /// <summary>
    /// Available script commands
    /// </summary>
    static ScriptCommand[] arsc = new ScriptCommand[] 
    {  
      // PID Hardware Control Commands
      new ScriptCommand ("PUMP1", true, 1, 0, 100, 200, 200),                         // 0-100, 200=SpeedClean
      new ScriptCommand ("PUMP2", true, 1, 0, 100, 200, 200),                         // 0-100, 200=SpeedClean
      new ScriptCommand ("PUMP3", true, 1, 0, 100, 0, -1),                            // 0-100
      new ScriptCommand ("VALVE1", true, 1, 0, -1, 0, 1),                             // 0,1
      //    The script cmd VALVE2 is redundant (no such valve), but will be used in the device SW
      //    for acoustic alarm message to the AB
//      new ScriptCommand ("VALVE2", true, 1, 0, -1, 0, 1),                             // 0,1
      new ScriptCommand ("LAMP", true, 1, 0, 100, 0, -1),                             // 0-100
      new ScriptCommand ("CELL", true, 1, 0, -1, 0, 1),                               // 0,1
      new ScriptCommand ("TEMP1", true, 1, 0, 0, 40, 250),                            // 0, 40-250
      new ScriptCommand ("TEMP2", true, 1, 0, 0, 40, 250),                            // 0, 40-250
      new ScriptCommand ("TEMP3", true, 1,0, 0, 40, 2500),                            // 0, 40-250
      new ScriptCommand ("TEMP4", true, 1, 0, 0, 40, 250),                            // 0, 40-250
      new ScriptCommand ("TEMPALL", false, 3, 0, 0, 40, 250),                         // TB: 3 x {0, [40,250]}
      new ScriptCommand ("TEMPQUIT1", true, 1, 0, 0, 40, 250),                        // 0, 40-250
      new ScriptCommand ("TEMPQUIT2", true, 1, 0, 0, 40, 250),                        // 0, 40-250
      new ScriptCommand ("TEMPQUIT3", true, 1, 0, 0, 40, 2500),                       // 0, 40-250
      new ScriptCommand ("TEMPQUIT4", true, 1, 0, 0, 40, 250),                        // 0, 40-250
      new ScriptCommand ("TEMPQUITALL", false, 3, 0, 0, 40, 250),                     // TB: 3 x {0, [40,250]}
      new ScriptCommand ("MFC", false, 1, 0, -1, 0, int.MaxValue),                    // TB: [0, 2^31-1]
       
      // Spectrum Data Commands                                  
      new ScriptCommand ("GETSTATOFFSET", false, 1, 0, -1, 0, 65535),                 // TB: [0, 2^16-1]
      new ScriptCommand ("GETOFFSET", false, 1, 0, -1, 0, 65535),                     // TB: [0, 2^16-1]
      new ScriptCommand ("SCANSTART", true, 1, 0, -1, 0, 0),                          // 0
      new ScriptCommand ("SCANREADY", false, 1, 0, -1, 0, 65535),                     // TB: [0, 2^16-1]
      new ScriptCommand ("MATHROUT", true, 1, 0, -1, 0, 1),                           // 0,1
      new ScriptCommand ("SUMSIGNAL", false, 1, 0, -1, 0, int.MaxValue),              // [0, 2^31-1]
              
      // Program flow Commands                            
      new ScriptCommand ("GOTO", false, 1, 0, -1, 0, 65535),                          // TB: [0, 2^16-1]
      new ScriptCommand ("KEY", false, 1, 0, -1, 0, 65535),                           // TB: [0, 2^16-1]
      new ScriptCommand ("CNT", false, 2, 0, -1, 0, 65535),                           // TB: 2 x [0, 2^16-1]

      // User interface commands
      new ScriptCommand ("DISPMODE", true, 1, 0, -1, 0, 2),                           // 0-2
      new ScriptCommand ("DISPHIDES", true, 1, 0, -1, 0, 1),                          // 0-1
      new ScriptCommand ("DIMMING", true, 1, 0, -1, 0, 15),                           // 0-15

      // GSM reader commands
      new ScriptCommand ("STRCLEAR", true, 1, 0, -1, 0, 0),                           // 0
      new ScriptCommand ("STRSTORE", true, 1, 0, -1, 0, 0),                           // 0
    
      // Current loop adjustment commands
      new ScriptCommand ("AOUT0", false, 1, 0, 20000, 0, -1),                       // TB: 0 - 20000
      new ScriptCommand ("AOUT1", false, 1, 0, 20000, 0, -1),                       // TB: 0 - 20000
      new ScriptCommand ("AOUT2", false, 1, 0, 20000, 0, -1),                       // TB: 0 - 20000
      new ScriptCommand ("AOUT3", false, 1, 0, 20000, 0, -1),                       // TB: 0 - 20000
      new ScriptCommand ("AOUT4", false, 1, 0, 20000, 0, -1),                       // TB: 0 - 20000
      new ScriptCommand ("AOUT5", false, 1, 0, 20000, 0, -1),                       // TB: 0 - 20000
      new ScriptCommand ("AOUT6", false, 1, 0, 20000, 0, -1),                       // TB: 0 - 20000
      new ScriptCommand ("AOUT7", false, 1, 0, 20000, 0, -1),                       // TB: 0 - 20000
      new ScriptCommand ("AOUT8", false, 1, 0, 20000, 0, -1),                       // TB: 0 - 20000
      new ScriptCommand ("AOUT9", false, 1, 0, 20000, 0, -1),                       // TB: 0 - 20000
      new ScriptCommand ("AOUT10", false, 1, 0, 20000, 0, -1),                       // TB: 0 - 20000
      new ScriptCommand ("AOUT11", false, 1, 0, 20000, 0, -1),                       // TB: 0 - 20000
      new ScriptCommand ("AOUT12", false, 1, 0, 20000, 0, -1),                       // TB: 0 - 20000
      new ScriptCommand ("AOUT13", false, 1, 0, 20000, 0, -1),                       // TB: 0 - 20000
      new ScriptCommand ("AOUT14", false, 1, 0, 20000, 0, -1),                       // TB: 0 - 20000
      new ScriptCommand ("AOUT15", false, 1, 0, 20000, 0, -1),                       // TB: 0 - 20000

      // DOUT commands
      new ScriptCommand ("DOUT0", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT1", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT2", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT3", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT4", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT5", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT6", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT7", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT8", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT9", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT10", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT11", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT12", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT13", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT14", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT15", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT16", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT17", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT18", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT19", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT20", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT21", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT22", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT23", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT24", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT25", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT26", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT27", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT28", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT29", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT30", true, 1, 0, 1, 0, -1),                              // 0 - 1
      new ScriptCommand ("DOUT31", true, 1, 0, 1, 0, -1),                              // 0 - 1
      
      // MP channel commands
      new ScriptCommand ("MPSETCHAN0", true, 1, 0, 99, 0, -1),                         // 0 - 99
      new ScriptCommand ("MPSETCHAN1", true, 1, 0, 99, 0, -1),                         // 0 - 99
      new ScriptCommand ("MPSETCHAN2", true, 1, 0, 99, 0, -1),                         // 0 - 99
      new ScriptCommand ("MPSETCHAN3", true, 1, 0, 99, 0, -1),                         // 0 - 99
      new ScriptCommand ("MPSETCHAN4", true, 1, 0, 99, 0, -1),                         // 0 - 99
      new ScriptCommand ("MPSETCHAN5", true, 1, 0, 99, 0, -1),                         // 0 - 99
      new ScriptCommand ("MPSETCHAN6", true, 1, 0, 99, 0, -1),                         // 0 - 99
      new ScriptCommand ("MPSETCHAN7", true, 1, 0, 99, 0, -1),                         // 0 - 99
      new ScriptCommand ("MPSETCHAN8", true, 1, 0, 99, 0, -1),                         // 0 - 99
      new ScriptCommand ("MPSETCHAN9", true, 1, 0, 99, 0, -1),                         // 0 - 99
      new ScriptCommand ("MPSETCHAN10", true, 1, 0, 99, 0, -1),                         // 0 - 99
      new ScriptCommand ("MPSETCHAN11", true, 1, 0, 99, 0, -1),                         // 0 - 99
      new ScriptCommand ("MPSETCHAN12", true, 1, 0, 99, 0, -1),                         // 0 - 99
      new ScriptCommand ("MPSETCHAN13", true, 1, 0, 99, 0, -1),                         // 0 - 99
      new ScriptCommand ("MPSETCHAN14", true, 1, 0, 99, 0, -1),                         // 0 - 99
      new ScriptCommand ("MPSETCHAN15", true, 1, 0, 99, 0, -1),                         // 0 - 99
      new ScriptCommand ("MPCYCCNT", true, 1, 0, -1, 0, 0),                             // 0
    };
    
    #endregion members

    #region static methods

    /// <summary>
    /// Gets the ScriptCommand object based on a given command name. 
    /// </summary>
    /// <param name="sCmd">The command name</param>
    /// <returns>The ScriptCommand object (null, if not found)</returns>
    public static ScriptCommand FromCommand (string sCmd)
    {
      // Loop over the available script commands
      foreach (ScriptCommand cmd in arsc)
      {
        // Does the command name match?
        if (cmd.Command == sCmd.Trim())
          // Yes
          return cmd;
      }
      // No
      return null;
    }

    /// <summary>
    /// Checks, whether a given script command matches one of the available script commands.
    /// </summary>
    /// <param name="sCmd">The script command</param>
    /// <returns>True on match; false otherwise</returns>
    public static Boolean Check (string sCmd)
    {
      // Loop over the available script commands
      foreach (ScriptCommand cmd in arsc)
      {
        // Does the command name match?
        if (cmd.Command == sCmd.Trim())
          // Yes
          return true;
      }
      // No
      return false;
    }
    
    /// <summary>
    /// Gets the parameter ranges for a given script command.
    /// </summary>
    /// <param name="sCmd">The script command</param>
    /// <param name="min1">The minimum of the 1. range</param>
    /// <param name="max1">The maximum of the 1. range</param>
    /// <param name="min2">The minimum of the 2. range</param>
    /// <param name="max2">The maximum of the 2. range</param>
    /// <remarks>
    /// Releveant only for those script commands, whose parameters should be input via a ComboBox. 
    /// </remarks>
    public static void ParamRangeOnCmd ( string sCmd, ref int min1, ref int max1, ref int min2, ref int max2 )
    {
      // Loop over the available script commands
      foreach (ScriptCommand cmd in arsc)
      {
        // Does the command name match?
        if (cmd.Command == sCmd.Trim ())
        {
          // Yes: Get its parameter ranges
          min1=cmd.Min1; max1=cmd.Max1; min2=cmd.Min2; max2=cmd.Max2;
          return;
        }
      }
    }

    /// <summary>
    /// Gets the parameter number for a given script command.
    /// </summary>
    /// <param name="sCmd">The script command</param>
    /// <returns>The parameter number</returns>
    /// <remarks>
    /// Releveant only for those script commands, whose parameters should be input via a TextBox. 
    /// </remarks>
    public static int ParamNumberOnCmd ( string sCmd )
    {
      // Loop over the available script commands
      foreach (ScriptCommand cmd in arsc)
      {
        // Does the command name match?
        if (cmd.Command == sCmd.Trim ())
        {
          // Yes
          return cmd.ParNumber;   // Ex.: 1 parameter: "100 = GOTO,20", 3 parameters: "100 = TEMPALL,20,30,40"
        }
      }
      // No
      return -1;
    }

    /// <summary>
    /// Indicates, whether a given script command is a CB command (i.e. whose parameters should be 
    /// input via a ComboBox) or a TB command (i.e. whose parameters should be input via a TextBox).
    /// </summary>
    /// <param name="sCmd">The script command</param>
    /// <returns>True if it is a CB command, false if it is a TB command</returns>
    public static Boolean IsCBCmd (string sCmd)
    {
      // Loop over the available script commands
      foreach (ScriptCommand cmd in arsc)
      {
        // Does the command name match?
        if (cmd.Command == sCmd.Trim())
        {
          // Yes: Evaluate its 'CBcmd' member:
          //  Return true, if the cmd is a CB cmd.
          //  Return false, if the cmd is a TB cmd.
          return cmd.CBcmd;
        }
      }
      // Just in case ...
      return false;
    }

    /// <summary>
    /// Checks, whether the parameter input for the given script command is via TB (TextBox) 
    /// or CB (ComboBox).
    /// </summary>
    /// <param name="sCmd">The script command</param>
    /// <returns>True, if the parameter input for the given command is via TB; false otherwise</returns>
    public static Boolean ParameterViaTB ( string sCmd )
    {
      // Get the CB/TB indication: True on TB, false on CB
      bool bTB = !IsCBCmd (sCmd);
      return bTB;
    }

    /// <summary>
    /// Gets a help text for a given script command.
    /// </summary>
    /// <param name="sCmd">The script command</param>
    /// <returns>Its help text</returns>
    public static string HelpOnCmd ( string sCmd )
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      string s = "";
      switch ( sCmd )
      {
        case "PUMP1":
          s = r.GetString ("ScriptCommands_HlpOnCmd_PUMP1");        // "Adjust Pump1 to [0, 100]%; a value of 200 means SpeedClean.";
          break;
        case "PUMP2":
          s = r.GetString ("ScriptCommands_HlpOnCmd_PUMP2");        // "Adjust Pump2 to [0, 100]%; a value of 200 means SpeedClean.";
          break;
        case "PUMP3":
          s = r.GetString ("ScriptCommands_HlpOnCmd_PUMP3");        // "Adjust Pump3 to [0, 100]%.";
          break;
        case "VALVE1":
          s = r.GetString ("ScriptCommands_HlpOnCmd_VALVE1");       // "Switch Valve1 Off (0) / On (1).";
          break;
        case "VALVE2":
          s = r.GetString ("ScriptCommands_HlpOnCmd_VALVE2");       // "Switch Valve2 Off (0) / On (1).";
          break;
        case "LAMP":
          s = r.GetString ("ScriptCommands_HlpOnCmd_LAMP");         // "Switch PID lamp Off (0) / On (1-100).";
          break;
        case "CELL":
          s = r.GetString ("ScriptCommands_HlpOnCmd_CELL");         // "Switch to cell input 1 (0) / 2 (1).";
          break;
        case "TEMP1":
          s = r.GetString ("ScriptCommands_HlpOnCmd_TEMP1");        // "Adjust heater temperature 1 to [40, 250]�C\r\n" + "(script continues running).";
          break;
        case "TEMP2":
          s = r.GetString ("ScriptCommands_HlpOnCmd_TEMP2");        // "Adjust heater temperature 2 to [40, 250]�C\r\n" + "(script continues running).";
          break;
        case "TEMP3":
          s = r.GetString ("ScriptCommands_HlpOnCmd_TEMP3");        // "Adjust heater temperature 3 to [40, 250]�C\r\n" + "(script continues running).";
          break;
        case "TEMP4":
          s = r.GetString ("ScriptCommands_HlpOnCmd_TEMP4");        // "Adjust heater temperature 4 to [40, 250]�C\r\n" + "(script continues running).";
          break;
        case "TEMPALL":       // Par.: TB
          s = r.GetString ("ScriptCommands_HlpOnCmd_TEMPALL");      // "Adjust heater temperatures 1-3 to [40, 250]�C simultaneously\r\n" + "(script continues running).";
          break;
        case "TEMPQUIT1":
          s = r.GetString ("ScriptCommands_HlpOnCmd_TEMPQUIT1");    // "Adjust heater temperature 1 to [40, 250]�C\r\n" + "(script is stopped until the temperature is reached).";
          break;
        case "TEMPQUIT2":
          s = r.GetString ("ScriptCommands_HlpOnCmd_TEMPQUIT2");    // "Adjust heater temperature 2 to [40, 250]�C\r\n" + "(script is stopped until the temperature is reached).";
          break;
        case "TEMPQUIT3":
          s = r.GetString ("ScriptCommands_HlpOnCmd_TEMPQUIT3");    // "Adjust heater temperature 3 to [40, 250]�C\r\n" + "(script is stopped until the temperature is reached).";
          break;
        case "TEMPQUIT4":
          s = r.GetString ("ScriptCommands_HlpOnCmd_TEMPQUIT4");    // "Adjust heater temperature 4 to [40, 250]�C\r\n" + "(script is stopped until the temperature is reached).";
          break;
        case "TEMPQUITALL":   // Par.: TB
          s = r.GetString ("ScriptCommands_HlpOnCmd_TEMPQUITALL");  // "Adjust heater temperatures 1-3 to [40, 250]�C simultaneously\r\n" + "(script is stopped until the temperature is reached).";
          break;

        case "MFC":           // Par.: TB
          s = r.GetString ("ScriptCommands_HlpOnCmd_MFC");          // "Adjust MFC flow debit value (unit: �l/min).";
          break;
        
        case "GETSTATOFFSET":
          s = r.GetString ("ScriptCommands_HlpOnCmd_GETSTATOFFSET");// "Determine the static scan offset.";
          break;
        case "GETOFFSET":
          s = r.GetString ("ScriptCommands_HlpOnCmd_GETOFFSET");    // "Start the scan offset determination.";
          break;
        case "SCANSTART":
          s = r.GetString ("ScriptCommands_HlpOnCmd_SCANSTART");    // "Start the scan aquisition.";
          break;
        case "SCANREADY":
          s = r.GetString ("ScriptCommands_HlpOnCmd_SCANREADY");    // "Continue the program with the command at the defined time, if the scan aquisition has finished.";
          break;
        case "MATHROUT":
          s = r.GetString ("ScriptCommands_HlpOnCmd_MATHROUT");     // "Include (1) or Exclude (0) denoising algorithm in/from peak detection.";
          break;
        case "SUMSIGNAL":
          s = r.GetString ("ScriptCommands_HlpOnCmd_SUMSIGNAL");    // "Execute calculation of the summary signal.";
          break;

        case "GOTO":          // Par.: TB
          s = r.GetString ("ScriptCommands_HlpOnCmd_GOTO");         // "Continue the program execution with the command at the defined time.";
          break;
        case "KEY":           // Par.: TB
          s = r.GetString ("ScriptCommands_HlpOnCmd_KEY");          // "Continue the program execution with the command at the defined time, if a Key was pressed.";
          break;
        case "CNT":           // Par.: TB
          s = r.GetString ("ScriptCommands_HlpOnCmd_CNT");          // "Increments a global counter variable. After reaching the target count the Span calibration will be finished, and the program will be continued with the command at the defined time."
          break;

        case "DISPMODE":
          s = r.GetString ("ScriptCommands_HlpOnCmd_DISPMODE");     // "Adjust display mode:\r\n" + "Display all (0) / > DISPLIMIT value (1) / > HI-ALERT value (2)";
          break;
        case "DISPHIDES":
          s = r.GetString ("ScriptCommands_HlpOnCmd_DISPHIDES");    // "Show (0) or hide (1) the Scan display.";
          break;
        case "DIMMING":
          s = r.GetString ("ScriptCommands_HlpOnCmd_DIMMING");      // "Adjust dimming mode:\r\n" + "Minimum brightness (0) ... Full brightness (15)";
          break;

        case "STRCLEAR":
          s = r.GetString ("ScriptCommands_HlpOnCmd_STRCLEAR");     // "Delete the memory in the device (Eeprom).";
          break;
        case "STRSTORE":
          s = r.GetString ("ScriptCommands_HlpOnCmd_STRSTORE");     // "Perform the result storage in the device (Eeprom).";
          break;

        case "AOUT0":         // Par.: TB
        case "AOUT1": 
        case "AOUT2": 
        case "AOUT3": 
        case "AOUT4": 
        case "AOUT5": 
        case "AOUT6": 
        case "AOUT7": 
        case "AOUT8": 
        case "AOUT9": 
        case "AOUT10": 
        case "AOUT11": 
        case "AOUT12": 
        case "AOUT13": 
        case "AOUT14": 
        case "AOUT15": 
          s = r.GetString ("ScriptCommands_HlpOnCmd_AOUTx");        // "Outputs a certain current quantity to current loop [0,15]";
          break;

        case "DOUT0":
        case "DOUT1": 
        case "DOUT2": 
        case "DOUT3": 
        case "DOUT4": 
        case "DOUT5": 
        case "DOUT6": 
        case "DOUT7": 
        case "DOUT8": 
        case "DOUT9": 
        case "DOUT10": 
        case "DOUT11": 
        case "DOUT12": 
        case "DOUT13": 
        case "DOUT14": 
        case "DOUT15": 
        case "DOUT16": 
        case "DOUT17": 
        case "DOUT18": 
        case "DOUT19": 
        case "DOUT20": 
        case "DOUT21": 
        case "DOUT22": 
        case "DOUT23": 
        case "DOUT24": 
        case "DOUT25": 
        case "DOUT26": 
        case "DOUT27": 
        case "DOUT28": 
        case "DOUT29": 
        case "DOUT30": 
        case "DOUT31": 
          s = r.GetString ("ScriptCommands_HlpOnCmd_DOUTx");        // "Set (1) or reset (0) the DOUT Bit [0,31] on the analog board.";
          break;
        
        case "MPSETCHAN0":
        case "MPSETCHAN1": 
        case "MPSETCHAN2": 
        case "MPSETCHAN3": 
        case "MPSETCHAN4": 
        case "MPSETCHAN5": 
        case "MPSETCHAN6": 
        case "MPSETCHAN7": 
        case "MPSETCHAN8": 
        case "MPSETCHAN9": 
        case "MPSETCHAN10": 
        case "MPSETCHAN11": 
        case "MPSETCHAN12": 
        case "MPSETCHAN13": 
        case "MPSETCHAN14": 
        case "MPSETCHAN15": 
          s = r.GetString ("ScriptCommands_HlpOnCmd_MPSETCHANx");   // "Adjust the number of sample cycles for the corresponding MP channel (0 = channel disabled)."
          break;
        case "MPCYCCNT":
          s = r.GetString ("ScriptCommands_HlpOnCmd_MPCYCCNT");     // "Increments the cycle counter for evaluation on the Multiplexer board."
          break;
      }
      return s;
    }

    /// <summary>
    /// Gets a help text for the parameters of a given script command
    /// </summary>
    /// <param name="sCmd">The script command</param>
    /// <returns>The help text</returns>
    public static string HelpOnPar ( string sCmd )
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      string s = "";
      switch ( sCmd )
      {
        case "TEMPALL":       // Par.: TB
          s = r.GetString ("ScriptCommands_HlpOnPar_TEMPALL");      // "The heater temperatures 1-3, separated by commas\r\n" + "(e.g. '40,50,40').";
          break;
        case "TEMPQUITALL":   // Par.: TB
          s = r.GetString ("ScriptCommands_HlpOnPar_TEMPQUITALL");  // "The heater temperatures 1-3, separated by commas\r\n" + "(e.g. '40,50,40').";
          break;

        case "GETSTATOFFSET":
        case "GETOFFSET":
          s = r.GetString ("ScriptCommands_HlpOnPar_GETOFFSET");    // "The duration of the offset determination (in 1/10 sec).";        
          break;
        case "SCANREADY":        
          s = r.GetString ("ScriptCommands_HlpOnPar_SCANREADY");    // "The time at which the program should be continued.";        
          break;
        case "SUMSIGNAL":        
          s = r.GetString ("ScriptCommands_HlpOnPar_SUMSIGNAL");    // "The User value for DAC output (in ppb).";        
          break;

        case "GOTO":          // Par.: TB
          s = r.GetString ("ScriptCommands_HlpOnPar_GOTO");         // "The time at which the program should be continued.";
          break;
        case "KEY":           // Par.: TB
          s = r.GetString ("ScriptCommands_HlpOnPar_KEY");          // "The time at which the program should be continued.";
          break;
        case "CNT":           // Par.: TB
          s = r.GetString ("ScriptCommands_HlpOnPar_CNT");          // "The target count (> 0) and the time at which the program should be continued, separated by comma\r\n" + "(e.g. '10,220').";
          break;
    
        case "STRCLEAR":
          s = r.GetString ("ScriptCommands_HlpOnPar_STRCLEAR");     // "(Dummy).";        
          break;
        case "STRSTORE":
          s = r.GetString ("ScriptCommands_HlpOnPar_STRSTORE");     // "(Dummy).";        
          break;
          
        case "AOUT0": 
        case "AOUT1":
        case "AOUT2": 
        case "AOUT3": 
        case "AOUT4": 
        case "AOUT5": 
        case "AOUT6": 
        case "AOUT7": 
        case "AOUT8": 
        case "AOUT9": 
        case "AOUT10": 
        case "AOUT11": 
        case "AOUT12": 
        case "AOUT13": 
        case "AOUT14": 
        case "AOUT15": 
          s = r.GetString ("ScriptCommands_HlpOnPar_AOUTx");        // "The current quantity in [0,20000] �A";
          break;

        case "MPCYCCNT":
          s = r.GetString ("ScriptCommands_HlpOnPar_MPCYCCNT");    // "(Dummy).";        
          break;
        
        default:
          s = r.GetString ("ScriptCommands_HlpOnPar_Def");          // "(See the script command for details).";
          break;
      }
      return s;
    }

    /// <summary>
    /// Performs a special parameter check.
    /// </summary>
    /// <param name="sCmd">The script command string</param>
    /// <param name="sPar">The script parameter string</param>
    /// <param name="sParToken">Only in case of error: The token of the parameter string, which caused an error (out)</param>
    /// <returns>True, if the parameters are OK; False otherwise</returns>
    static public bool CheckParameters(string sCmd, string sPar, ref string sParToken)
    {
      bool res = true;

      switch (sCmd)
      {
        case "CNT":
          // Get the parameters as a string array
          string[] ars = sPar.Split (new char[] {','});
          // Demand: Target count > 0
          try
          {
            sParToken = ars[0];
            int targetCnt = int.Parse(ars[0]);
            if (!(targetCnt > 0))
              res = false;
          }
          catch
          {
            res = false;
          }
          break;
      }
      // Done
      return res;
    }
    
    #endregion static methods

    #region properties
    
    /// <summary>
    /// The available script commands
    /// </summary>
    public static ScriptCommand[] Commands { get { return arsc; } }
    
    #endregion properties

  }
}
