using System;
using System.IO;
using System.Windows.Forms;

using ICSharpCode.SharpZipLib;

namespace App
{
	/// <summary>
	/// Class Diagnosis:
	/// Device Diagnosis implementation
	/// </summary>
	public class Diagnosis
	{

    #region constructors
    
    /// <summary>
		/// Default Constructor
		/// </summary>
    public Diagnosis()
		{
      // Disable zipping of the diagnosis file
      _bZip = false;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="bZip">Indicates, whether zipping of the diagnosis file is enabled (true) 
    /// or disabled (false, Default)</param>
    public Diagnosis(Boolean bZip)
    {
      // Overgive: Zipping of the diagnosis file
      _bZip = bZip;
    }

    #endregion constructors
    
    #region members
    
    /// <summary>
    /// Indicates, whether the Diagnosis is enabled (True) or disabled (False)
    /// </summary>
    public Boolean Enabled = false;

    /// <summary>
    /// The name of the temporary Diagnosis file
    /// </summary>
    string _sTempFileName = "";

    /// <summary>
    /// The StreamWriter object for the temporary Diagnosis file
    /// </summary>
    StreamWriter _sw = null;

    /// <summary>
    /// The Diagnosis begin time
    /// </summary>
    DateTime _tim = DateTime.MinValue;

    /// <summary>
    /// Indicates, whether the logging of measurement information should be started (true) or not (false)
    /// </summary>
    Boolean _bStartLog = false;

    /// <summary>
    /// Indicates, whether zipping of the diagnosis file is enabled (true) or disabled (false, Default).
    /// I.e. defines, whether a diagnosis file should be deposed as a text file (false) or 
    /// as a zip file (true).
    /// </summary>
    Boolean _bZip = false;

    #endregion members

    #region constants & enums
    
    /// <summary>
    /// The measurement information type enumeration
    /// </summary>
    public enum Typ
    {
      Info = 1,   // Meas. information: Device information ( Message: Info )
      Result,     // Meas. information: Results ( Message: ScanResult )
      Scan        // Meas. information: Scan ( Message: ScanData )
    };
  
    #endregion constants & enums
  
    #region methods

    /// <summary>
    /// Initiates the Diagnosis
    /// </summary>
    public void Initiate ()
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      Ressources r = app.Ressources;      
      
      // Get a unique temporary filename
      _sTempFileName = Path.GetTempFileName ();
      
      // Initiate temporary Diagnosis file
      try 
      {
        // Get the current DateTime as the Diagnosis begin time
        _tim = DateTime.Now;
        // Open the temporary file for writing
        _sw = File.CreateText ( _sTempFileName );
        // Write: Scriptname, Begin time, NL
        string sx = string.Format ( "Script:\t{0}", doc.sCurrentScript );
        _sw.WriteLine ( sx );
        sx = string.Format ( "Diagnose-Beginn:\t{0}", _tim.ToString ( "dd.MM.yyyy HH.mm.ss" ) );
        _sw.WriteLine ( sx );
        _sw.WriteLine ();
      }
      catch 
      {
        // Reset the StreamWriter object
        _sw = null;
        // Disable diagnosis
        Enabled = false;
        // Message: Unable to perform diagnosis
        string sMsg = r.GetString ("cDiagnosis_Error_PerformUnable"); // "Beim Initialisieren der Diagnose ist ein Fehler aufgetreten.\r\nDie Diagnose wurde abgebrochen."
        string sCap = r.GetString ("Form_Common_TextError");          // "Fehler"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);
        // Exit
        return;
      }
      
      // Indicate, that the logging of measurement information should not yet be started
      _bStartLog = false;
      
      // Enable Diagnosis
      Enabled = true;
    }

    /// <summary>
    /// Terminates the Diagnosis
    /// </summary>
    /// <remarks>
    /// The diagnosis files are stored with the following path:
    ///   (GSM data folder)/DIAG/DIAG yy-MM-dd-HH-mm-ss.txt
    /// </remarks>
    public void Terminate ()
    {
      // Check: Diagnosis already terminated ?
      if ( !Enabled )
        return; // Yes.
      
      // Security check: Temporary file opened for writing ?
      if ( null == _sw )
        return; // No
      
      // No:
      App app = App.Instance;
      Doc doc = app.Doc;
      Ressources r = app.Ressources;      
      string sMsg, sCap, sFmt;
      
      // Disable Diagnosis
      Enabled = false;

      // Close the temporary file
      _sw.Flush ();
      _sw.Close ();

      // Create the data folder, selected per Properties dialog, if req.
      string sDiagFolder = doc.sDataFolder;
      Directory.CreateDirectory ( sDiagFolder );

      // Create the diagnosis folder as a subfolder of the data folder, if req.
      sDiagFolder = Path.Combine ( sDiagFolder, "DIAG" );
      Directory.CreateDirectory ( sDiagFolder );

      // Design the full path of the diagnosis file
      string sFilename = Path.Combine ( sDiagFolder, "DIAG " + _tim.ToString ( "yy-MM-dd-HH-mm-ss" ) + ".txt" );
        
      // Copy the temporary file into the true diagnosis file & Delete the temporary file
      File.Copy ( _sTempFileName, sFilename, true );
      File.Delete ( _sTempFileName );
       
      // Reset the StreamWriter object
      _sw = null;
      
      // CD: Should the diagnosis file be deposed as a text file or as a zip file?
      if (_bZip)
      {
        // The diagnosis file should be deposed as a zip file:

        // Zip the text diagnosis file
        SharpZipInterface szi = new SharpZipInterface ();
        //    The array of files to be zipped 
        string[] filenames = new string[] { sFilename };         
        //    The zip file path: Same dir & filename as the text diagnosis file, file extension '.zip'
        int idx = sFilename.LastIndexOf ('.'); 
        string sZipFilePath = sFilename.Substring (0, idx) + ".zip"; 
        //    Zip
        SharpZipInterface.Notification n = szi.Zip (filenames, sZipFilePath);

        // Check, whether the zip file was created successfully or whether an error ocurred during zipping.
        if (n == SharpZipInterface.Notification.ZipSuccess)
        {
          // The zip file was created successfully: The diagnosis file is the zip file 
          
          // Delete the text diagnosis file
          File.Delete (sFilename);
          
          // Message: Diagnosis performed (Result: zip file). Send this file?
          sFmt = r.GetString ("cDiagnosis_Info_PerformedAndSend");  // "Die Diagnose wurde durchgef�hrt.\r\nDie Diagnoseresultate wurden in Datei\r\n   {0}\r\nhinterlegt.\r\nSoll diese Datei an die IUT Medical GmbH gesendet werden?"
          sMsg = string.Format ( sFmt, sZipFilePath );
          sCap = r.GetString ("Form_Common_TextInfo");              // "Information"
          DialogResult dr = MessageBox.Show (sMsg, sCap, MessageBoxButtons.YesNo, MessageBoxIcon.Information);
          
          // Check: Should the zip diagnosis file be sent?
          if (dr == DialogResult.Yes)
          {
            // Yes:

            // Get the required zip diagnosis file info
            string filePath = sZipFilePath;
            DateTime dt = File.GetLastAccessTime (filePath);
        
            // Build the mailto string
            string sRecipient = "info@iut-medical.com";
            sFmt = r.GetString ("cDiagnosis_Term_Subject");   // "PID-Diagnosedatei%20vom%20{0}"
            string sSubject = string.Format (sFmt, dt.ToString ("dd.MM.yy"));
            sFmt = r.GetString ("cDiagnosis_Term_Body");      // "(Hinweis:%0D%0A" +
                                                              // "Wenn%20Sie%20die%20PID-Diagnosedatei%20'{0}'%20an%20die%20IUTMedical%20GmbH%20senden%20m�chten," +
                                                              // "%20so%20f�gen%20Sie%20diese%20bitte%20als%20Anhang%20bei%20und%20senden%20Sie%20die%20Nachricht." +
                                                              // "%20Danach%20schlie�en%20Sie%20bitte%20den%20Mailclienten.%0D%0A" +  
                                                              // "Wenn%20nicht,%20schlie�en%20Sie%20den%20Mailclienten%20bitte%20ohne%20weitere%20Aktion.)";
            string sBody = string.Format (sFmt, filePath);
            string smailtoDiag = string.Format("mailto:{0}?subject={1}&body={2}",
              sRecipient, sSubject, sBody);          

            // Launch the mail client
            System.Diagnostics.Process.Start(smailtoDiag);
          }
        }
        else
        {
          // An error ocurred during zipping: The diagnosis file is the text file

          // Delete the zip diagnosis file (just in case ...)
          File.Delete (sZipFilePath);
          
          // Message: Diagnosis performed (Result: text file).
          sFmt = r.GetString ("cDiagnosis_Info_Performed"); // "Die Diagnose wurde durchgef�hrt.\r\nDie Diagnoseresultate wurden in Datei\r\n{0}\r\nhinterlegt."
          sMsg = string.Format ( sFmt, sFilename ); 
          sCap = r.GetString ("Form_Common_TextInfo");      // "Information"
          MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
      }
      else
      {
        // The diagnosis file should be deposed as a text file:
        // We are already done.

        // Message: Diagnosis performed (Result: text file).
        sFmt = r.GetString ("cDiagnosis_Info_Performed");   // "Die Diagnose wurde durchgef�hrt.\r\nDie Diagnoseresultate wurden in Datei\r\n{0}\r\nhinterlegt."
        sMsg = string.Format ( sFmt, sFilename ); 
        sCap = r.GetString ("Form_Common_TextInfo");        // "Information"
        MessageBox.Show (sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
    }
   
    /// <summary>
    /// Appends measurement information to the Diagnosis file
    /// </summary>
    /// <param name="type">The measurement information type</param>
    /// <remarks>
    /// Structure of a row:
    /// (item name):TAB(item value)[SPACE(item value)...]
    /// </remarks>
    public void Append ( Typ type)
    {
      // Check: Diagnosis enabled?
      if ( !Enabled )
        return; // No
       
      // Security check: Temporary file opened for writing ?
      if ( null == _sw )
        return; // No

      // Yes
      App app = App.Instance;
      Doc doc = app.Doc;
      Ressources r = app.Ressources;

      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;  
      System.Globalization.NumberFormatInfo lfi = System.Globalization.NumberFormatInfo.CurrentInfo;
      string sx;
      try 
      {
        switch ( type )
        {
          case Typ.Info:      // Meas. information: Device information
            // Indicate, that the logging of measurement information should be started now
            // (i.e. with an Timestamp at the beginning )
            if ( !_bStartLog ) _bStartLog = true;
            // NL
            _sw.WriteLine ();
            // DateTime
            sx = string.Format ( "Zeit:\t{0}", doc.DeviceInfo.dtDevice.ToString ( "dd.MM.yyyy HH.mm.ss" ) );
            _sw.WriteLine ( sx );
            // p
            if (doc.DeviceInfo.nPresDispOnOff == 1)   // Pressure display on
              sx = string.Format ( "p(kPa):\t{0}", doc.DeviceInfo.dPressure.ToString ( "F1", nfi ) );
            else                                      // Pressure display off
              sx = string.Format ( "p(kPa):\t{0}", r.GetString( "Div_NotConnected" ) );
            _sw.WriteLine ( sx );
            // T
            sx = string.Format ( "T(�C):\t{0}", doc.DeviceInfo.dTemperature.ToString ( "F1", nfi ) );
            _sw.WriteLine ( sx );
            // F1-3
            sx = string.Format ("F1-3(ml/min):\t{0}, {1}, {2}", doc.DeviceInfo.nFlow1, doc.DeviceInfo.dFlow2.ToString ("F1", nfi), doc.DeviceInfo.nFlow3);
            _sw.WriteLine ( sx );
            // Offset
            sx = string.Format ( "Offset(adc):\t{0}", doc.DeviceInfo.nOffset.ToString () );
            _sw.WriteLine ( sx );
            break;

          case Typ.Result:    // Meas. information: Results
            if ( _bStartLog )
            {
              // NL
              _sw.WriteLine ();
              // Substance names
              _sw.Write ( "Subst.:\t" );
              for ( int i=0; i <  doc.arScanResult.Count ;i++ )
              {
                ScanResult sr = doc.arScanResult[i];
                if ( i > 0 ) _sw.Write ( " " );
                sx = string.Format ( "{0}", sr.sSubstance );
                _sw.Write ( sx );
              }
              _sw.WriteLine ();
              // Conc. units
              _sw.Write ( "Einh.:\t" );
              for ( int i=0; i <  doc.arScanResult.Count ;i++ )
              {
                ScanResult sr = doc.arScanResult[i];
                if ( i > 0 ) _sw.Write ( " " );
                sx = string.Format ( "{0}", sr.sConcUnit );
                _sw.Write ( sx );
              }
              _sw.WriteLine ();
              // Substance results
              _sw.Write ( "Konz.:\t" );
              for ( int i=0; i < doc.arScanResult.Count ;i++ )
              {
                ScanResult sr = doc.arScanResult[i];
                if ( i > 0 ) _sw.Write ( " " );
                sx = string.Format ( "{0}", sr.dResult.ToString ( nfi ) );
                sx = sx.Replace ( ".", lfi.NumberDecimalSeparator );
                _sw.Write ( sx );
              }
              _sw.WriteLine ();
            }
            break;

          case Typ.Scan:      // Meas. information: Scan
            if ( _bStartLog )
            {
              // NL
              _sw.WriteLine ();
              // Scan        
              _sw.Write ( "Scan:\t" );
              ScanData sd = doc.arScanData[0];
              for ( int i = 0 ; i < sd.Count ; i++ )
              {
                if ( i > 0 ) _sw.Write ( " " );
                sx = string.Format ( "{0}", sd[i].ToString ( "F3", nfi ) );
                sx = sx.Replace ( ".", lfi.NumberDecimalSeparator );
                _sw.Write ( sx );
              }
              _sw.WriteLine ();
            }
            break;

        }
      }
      catch {}
    }
    
    #endregion methods

  }
}
