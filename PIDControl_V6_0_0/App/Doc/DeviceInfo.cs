using System;

namespace App
{
	/// <summary>
	/// Class DeviceInfo:
  /// Sensor information: Temperature, Pressure, Pressure display on/off, Flow1, Flow2, Flow3, Battery level,
  ///                     RTC DateTime, Scan offset, Scan offset STD
  /// </summary>
	public class DeviceInfo
	{
    #region constructors
    
    /// <summary>
    /// Constructor
    /// </summary>
    public DeviceInfo()
		{
      dTemperature = 0.0;
      dPressure    = 0.0;
      nPresDispOnOff = 0;
      nFlow1       = 0;
      dFlow2       = 0.0; 
      nFlow3       = 0; 
      dBattery     = 0.0;
      dtDevice     = DateTime.MinValue;
      nOffset      = 0; 
      dSTD         = 0.0; 
    }
	
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="di">The DeviceInfo object to be initialized with</param>
    public DeviceInfo( DeviceInfo di)
    {
      dTemperature = di.dTemperature;
      dPressure    = di.dPressure;
      nPresDispOnOff = di.nPresDispOnOff;
      nFlow1       = di.nFlow1;
      dFlow2       = di.dFlow2; 
      nFlow3       = di.nFlow3; 
      dBattery     = di.dBattery;
      dtDevice     = di.dtDevice; 
      nOffset      = di.nOffset; 
      dSTD         = di.dSTD;
    }
  
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="sDI">A string containing DeviceInfo information</param>
    public DeviceInfo ( string sDI ) 
    {
      string[] ars = sDI.Split ( ',' );
      if ( ars.Length != _NO_DEVICEINFO_MEMBERS )
        throw new System.ApplicationException ();
      
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
      dTemperature = double.Parse ( ars[0], nfi );
      dPressure    = double.Parse ( ars[1], nfi );
      nPresDispOnOff = int.Parse (ars[2]);
      nFlow1       = int.Parse (ars[3]); 
      dFlow2       = double.Parse (ars[4], nfi);
      nFlow3       = int.Parse (ars[5]); 
      dBattery     = double.Parse ( ars[6], nfi );
      _StringToDateTime ( ars[7], out dtDevice );
      nOffset      = int.Parse (ars[8]);
      dSTD         = double.Parse ( ars[9], nfi );
    }
  
    #endregion constructors

    #region constants
   
    // The # of DeviceInfo members: T, p, Pressure display on/off, F1, F2, F3, Battery level, 
    //                              DT, scan offset, scan offset STD
    const int _NO_DEVICEINFO_MEMBERS = 10;   

    #endregion constants
    
    #region members
    
    /// <summary>
    /// Temperature in �C
    /// </summary>
    public double dTemperature;  

    /// <summary>
    /// Pressure in kPa
    /// </summary>
    public double dPressure;      
    
    /// <summary>
    /// Pressure display on/off:  0 - off, 1 - on
    /// </summary>
    public int nPresDispOnOff;

    /// <summary>
    /// Flow1 in ml/min
    /// </summary>
    public int nFlow1;

    /// <summary>
    /// Flow2 (MFC controlled) in ml/min
    /// </summary>
    public double dFlow2;   

    /// <summary>
    /// Flow3 in ml/min
    /// </summary>
    public int nFlow3;   
    
    /// <summary>
    /// Battery level in %
    /// </summary>
    public double dBattery;     
    
    /// <summary>
    /// Device RTC DateTime in a given format
    /// </summary>
    public DateTime dtDevice;     

    /// <summary>
    /// Scan offset in adc units
    /// </summary>
    public int nOffset; 

    /// <summary>
    /// Scan offset STD in adc units
    /// </summary>
    public double dSTD;

    #endregion members

    #region methods

    /// <summary>
    /// Initiates the members year, month, day, hour, minute, second of a DateTime structure by means of 
    /// a DateTime string.
    /// </summary>
    /// <param name="sDT">The Datetime string</param>
    /// <param name="dt">The DateTime object (output)</param>
    /// <remarks>
    /// The DateTime-String has the following format: 
    ///   "(Y:2B)(M:2B)(D:2B)(H:2B)(M:2B)(S:2B)".
    /// </remarks>
    void _StringToDateTime (string sDT, out DateTime dt)
    {
      dt = new DateTime (
        int.Parse ( sDT.Substring( 0, 2 ) ) + 2000,
        int.Parse ( sDT.Substring( 2, 2 ) ),
        int.Parse ( sDT.Substring( 4, 2 ) ),
        int.Parse ( sDT.Substring( 6, 2 ) ),
        int.Parse ( sDT.Substring( 8, 2 ) ),
        int.Parse ( sDT.Substring( 10, 2 ) )
        );
    }

    /// <summary>
    /// Converts the value of this instance to its equivalent string representation.
    /// </summary>
    /// <returns>The string representation</returns>
    public override string ToString()
    {
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;  
      string s = string.Format ( "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", 
        dTemperature.ToString ( "F1", nfi ), 
        dPressure.ToString ( "F1", nfi ), 
        nPresDispOnOff.ToString (),
        nFlow1.ToString (),
        dFlow2.ToString ("F1", nfi),
        nFlow3.ToString (),
        dBattery.ToString ( "F0", nfi ),
        dtDevice.ToString ( "dd.MM.yyyy HH.mm.ss" ),
        nOffset.ToString (),
        dSTD.ToString ( "F2", nfi )
        );
      return s;
    }
  
    #endregion methods

    #region properties

    /// <summary>
    /// The battery level
    /// </summary>
    public string BatteryState
    {
      get 
      { 
        string sBat = (this.dBattery > 100) ? "Charging" : Common.ConvertToScience(this.dBattery, 0);
        return sBat;         
      }
    }

    #endregion properties

  }
}
