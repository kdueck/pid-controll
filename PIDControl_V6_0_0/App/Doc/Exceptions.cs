using System;

namespace App
{

  #region Script collection level

  /// <summary>
  /// Class ScrCollectionAmbiguousIDException:
  /// Exception thrown when equal script IDs (both > 0) are encountered.
  /// - Script collection level -
  /// </summary>
  public class ScrCollectionAmbiguousIDException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrCollectionAmbiguousIDException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrCollectionAmbiguousIDException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrCollectionAmbiguousIDException_Msg"); // "Die Datei enth�lt Scripts mit gleichen Script-IDs"
      return sMsg;
    }

    #endregion methods

  }

  /// <summary>
  /// Class ScrCollectionEqualScrNamesException:
  /// Exception thrown when equal script names are encountered.
  /// - Script collection level -
  /// </summary>
  public class ScrCollectionEqualScrNamesException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrCollectionEqualScrNamesException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrCollectionEqualScrNamesException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrCollectionEqualScrNamesException_Msg"); // "Die Datei enth�lt Scripts gleichen Namens"
      return sMsg;
    }

    #endregion methods

  }

  /// <summary>
  /// Class ScrCollectionEmptyException:
  /// Exception thrown when an empty script collection is encountered.
  /// - Script collection level -
  /// </summary>
  public class ScrCollectionEmptyException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrCollectionEmptyException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrCollectionEmptyException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrCollectionEmptyException_Msg"); // "Die Datei enth�lt keine Scripts"
      return sMsg;
    }

    #endregion methods

  }

  #endregion Script collection level

  #region Script level

  /// <summary>
  /// Class ScrCmdSectionNotPresentException:
  /// Exception thrown when the command section for a given script is not present.
  /// - Script level -
  /// </summary>
  public class ScrCmdSectionNotPresentException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrCmdSectionNotPresentException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrCmdSectionNotPresentException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrCmdSectionNotPresentException_Msg"); // "Scriptkommando-Sektion nicht vorhanden oder leer"
      return sMsg;
    }

    #endregion methods

  }

  /// <summary>
  /// Class ScrCmdTimeNotAscendingException:
  /// Exception thrown when the execution times of the script commands are not in ascending order.
  /// - Script level -
  /// </summary>
  public class ScrCmdTimeNotAscendingException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrCmdTimeNotAscendingException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrCmdTimeNotAscendingException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrCmdTimeNotAscendingException_Msg"); // "Die Scriptkommandos sind nicht zeitlich aufsteigend angeordnet";
      return sMsg;
    }

    #endregion methods
  
  }

  /// <summary>
  /// Class ScrCmdEqualityException:
  /// Exception thrown when identical script commands are present.
  /// - Script level -
  /// </summary>
  public class ScrCmdEqualityException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrCmdEqualityException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrCmdEqualityException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrCmdEqualityException_Msg"); // "Identische Scriptkommandos vorhanden";
      return sMsg;
    }

    #endregion methods
  
  }

  /// <summary>
  /// Class ScrWndNameAmbiguousException:
  /// Exception thrown when the window names of a script are non-unique (i.e. ambiguous)
  /// - Script level -
  /// </summary>
  public class ScrWndNameAmbiguousException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrWndNameAmbiguousException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrWndNameAmbiguousException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrWndNameAmbiguousException_Msg"); // "Substanznamen nicht eindeutig"
      return sMsg;
    }

    #endregion methods

  }

  /// <summary>
  /// Class ScrSpanConcException:
  /// Exception thrown when SPAN(x) rows of the Parameter section of a script are NOT associated 
  /// to substance definition rows of the scripts Windows section.
  /// - Script level -
  /// </summary>
  public class ScrSpanConcException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrSpanConcException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrSpanConcException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrSpanConcException_Msg"); // "Parameter-Sektion: Inkonsistente Span-Konzentrationen vorhanden"
      return sMsg;
    }

    #endregion methods

  }

  /// <summary>
  /// Class ScrAutoSpanMonException:
  /// Exception thrown when CTRLWND(x) rows of the Parameter section of a script are NOT associated 
  /// to substance definition rows of the scripts Windows section.
  /// - Script level -
  /// </summary>
  public class ScrAutoSpanMonException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrAutoSpanMonException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrAutoSpanMonException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrAutoSpanMonException_Msg");  // "Parameter-Sektion: Inkonsistente 'Automatisiertes Spanmonitoring'-Elemente vorhanden"
      return sMsg;
    }

    #endregion methods

  }

  /// <summary>
  /// Class ScrSumSignalNumWindowsException:
  /// Exception thrown when 
  /// a script with summary signal detection contains the max. # of substance windows.
  /// - Script level -
  /// </summary>
  public class ScrSumSignalNumWindowsException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrSumSignalNumWindowsException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrSumSignalNumWindowsException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      string sFmt = r.GetString ("ScrSumSignalNumWindowsException_Msg");  // "Scripte mit Summensignal-Ermittlung d�rfen max. {0} Substanzfenster beinhalten"
      string sMsg = string.Format (sFmt, Script.MAX_SCRIPT_WINDOWS - 1);
      return sMsg;
    }

    #endregion methods

  }
  
  /// <summary>
  /// Class ScrSumSignalTotalIdentifierException:
  /// Exception thrown when a script with summary signal detection doesn't contain a summary signal identifier
  /// paramter row.
  /// - Script level -
  /// </summary>
  public class ScrSumSignalTotalIdentifierException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrSumSignalTotalIdentifierException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrSumSignalTotalIdentifierException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      string sMsg = r.GetString ("ScrSumSignalTotalIdentifierException_Msg");  // "Scripte mit Summensignal-Ermittlung m�ssen einen 'Total'-Parameter beinhalten"
      return sMsg;
    }

    #endregion methods

  }
  
  /// <summary>
  /// Class ScrMeasScriptASMScriptException:
  /// Exception thrown when the script is simultaneously both a meas. script and an 
  /// 'Automated span monitoring' script -> this is not allowed!
  /// - Script level -
  /// </summary>
  public class ScrMeasScriptASMScriptException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrMeasScriptASMScriptException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrMeasScriptASMScriptException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrMeasScriptASMScriptException_Msg");  // "A script is not allowed to be simultaneously both a meas. script and an automated span monitoring script."
      return sMsg;
    }

    #endregion methods

  }

  /// <summary>
  /// Class ScrMeasScriptASMScriptException:
  /// Exception thrown when a script with a TotalVOC window has the static scan offset correction enabled -> this is not recommended!
  /// - Script level -
  /// </summary>
  public class ScrTotalVOCOffsetCorrException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrTotalVOCOffsetCorrException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrTotalVOCOffsetCorrException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrTotalVOCOffsetCorrException_Msg");  // "Scripte mit TotalVOC-Fenster sollten die statische Scanoffset-Korrektur nicht aktivieren"
      return sMsg;
    }

    #endregion methods

  }

  #endregion Script level

  #region script section level

  /// <summary>
  /// Class ScrCmdNameInvalidException:
  /// Exception thrown when a given script command doesn't match one of the available script commands.
  /// - script section level -
  /// </summary>
  public class ScrCmdNameInvalidException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrCmdNameInvalidException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrCmdNameInvalidException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrCmdNameInvalidException_Msg"); // "Ung�ltiges Scriptkommando"
      return sMsg;
    }

    #endregion methods

  }

  /// <summary>
  /// Class ScrCmdParameterInvalidException:
  /// Exception thrown when the parameters for a given script command are invalid.
  /// - script section level -
  /// </summary>
  public class ScrCmdParameterInvalidException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrCmdParameterInvalidException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrCmdParameterInvalidException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrCmdParameterInvalidException_Msg"); // "Ung�ltige Scriptkommando-Parameter"
      return sMsg;
    }

    #endregion methods

  }

  /// <summary>
  /// Class ScrParameterInvalidException:
  /// Exception thrown when the parameters of a given script are invalid.
  /// - script section level -
  /// </summary>
  public class ScrParameterInvalidException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrParameterInvalidException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrParameterInvalidException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrParameterInvalidException_Msg"); // "Ung�ltiger Scriptparameter"
      return sMsg;
    }

    #endregion methods

  }

  /// <summary>
  /// Class ScrSectionNameInvalidException:
  /// Exception thrown when an invalid script section name is encountered.
  /// - script section level -
  /// </summary>
  public class ScrSectionNameInvalidException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrSectionNameInvalidException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrSectionNameInvalidException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrSectionNameInvalidException_Msg"); // "Ung�ltiger Sektionsbezeichner"
      return sMsg;
    }

    #endregion methods

  }

  /// <summary>
  /// Class ScrWndNameInvalidException:
  /// Exception thrown when the name of a script window is either empty or exceeds the max. permitted length.
  /// - script section level -
  /// </summary>
  public class ScrWndNameInvalidException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrWndNameInvalidException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrWndNameInvalidException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrWndNameInvalidException_Msg"); // "Substanzname entweder leer oder zu lang"
      return sMsg;
    }

    #endregion methods

  }

  /// <summary>
  /// Class ScrWndInconsistencyException:
  /// Exception thrown when the calibration arrays of a script window contain inconsistent data
  /// - script section level -
  /// </summary>
  public class ScrWndInconsistencyException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrWndInconsistencyException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrWndInconsistencyException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrWndInconsistencyException_Msg"); // "The calibration data are inconsistent."
      return sMsg;
    }

    #endregion methods

  }

  /// <summary>
  /// Class ScrMsgStringlengthException:
  /// Exception thrown when the length of a communication message exceeds the max. permitted length.
  /// - script section level -
  /// </summary>
  public class ScrMsgStringlengthException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrMsgStringlengthException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrMsgStringlengthException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sMsg = r.GetString ("ScrMsgStringlengthException_Msg"); // "L�nge des entsprechenden �bertragungsstrings fehlerhaft"
      return sMsg;
    }

    #endregion methods

  }

  /// <summary>
  /// Class ScrWndMinNoCalibPointsException:
  /// Exception thrown when the min. number of allocated substance calibration points is not reached
  /// - script section level -
  /// </summary>
  public class ScrWndMinNoCalibPointsException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrWndMinNoCalibPointsException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrWndMinNoCalibPointsException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sFmt = r.GetString ("ScrWndMinNoCalibPointsException_Msg"); // "The min. number of allocated substance calibration points ({0}) is not reached."
      string sMsg = string.Format (sFmt, ScriptWindow.MIN_CALIB_POINTS);
      return sMsg;
    }

    #endregion methods

  }
  
  /// <summary>
  /// Class ScrWndTotalVOCException:
  /// Exception thrown a TotalVOC window is not of calibration type 'area'
  /// - script section level -
  /// </summary>
  public class ScrWndTotalVOCException : System.ApplicationException
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScrWndTotalVOCException () : base (BaseMessage())
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="msg">The message that describes the error</param>
    public ScrWndTotalVOCException (string msg) : base (msg)
    {
    }

    #endregion constructors

    #region methods

    /// <summary>
    /// Gets the message that describes the error, localized for the selected language.
    /// </summary>
    /// <returns></returns>
    static string BaseMessage ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;
      string sFmt = r.GetString ("ScrWndTotalVOCException_Msg");          // "A TotalVOC window must be of calibration type 'area'."
      string sMsg = string.Format (sFmt, ScriptWindow.MIN_CALIB_POINTS);
      return sMsg;
    }

    #endregion methods

  }
  
  #endregion script section level

}





