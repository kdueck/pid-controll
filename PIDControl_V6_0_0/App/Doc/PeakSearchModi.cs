using System;
using System.Collections;

namespace App
{
  /// <summary>
  /// Class PeakSearch:
  /// Peak search modi
  /// </summary>
  public class PeakSearch
  {
    public class Modi
    {
      /// <summary>
      /// Peak search mode: Unidirectional
      /// </summary>
      public const string Unidirectional = "Unidirectional";
      /// <summary>
      /// Peak search mode: Bidirectional
      /// </summary>
      public const string Bidirectional = "Bidirectional";
    }
  }

}