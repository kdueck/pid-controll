using System;
using System.IO;
using System.Diagnostics;
using System.Collections;

using CommRS232;

namespace App
{
  /// <summary>
  /// Class StringArray:
  /// String list
  /// </summary>
  public class StringArray : IEnumerable, ICollection
  {
    #region constructors
    
    /// <summary>
    /// Constructor
    /// </summary>
    public StringArray ()
    {
    }
  
    #endregion constructors

    #region members
    
    /// <summary>
    /// The list
    /// </summary>
    private ArrayList _Items = new ArrayList();

    #endregion members

    #region methods

    /// <summary>
    /// Removes all objects from the list
    /// </summary>
    public void Clear () 
    {
      _Items.Clear ();
    }

    /// <summary>
    /// Inserts the elements of an StringArray object into the list at the specified index
    /// </summary>
    /// <param name="index">The zero-based index at which the new elements should be inserted</param>
    /// <param name="ar">The StringArray whose elements should be inserted</param>
    public void InsertAt ( int index, StringArray ar ) 
    {
      _Items.InsertRange ( index, ar );
    }

    /// <summary>
    /// Adds a string to the list
    /// </summary>
    /// <param name="s">The string to be added</param>
    public void Add ( string s ) 
    {
      _Items.Add ( s );
    }

    /// <summary>
    /// Copies the instance to a compatible destination object. 
    /// </summary>
    /// <param name="ar">The destination object</param>
    public void CopyTo (StringArray ar)
    {
      ar.Clear ();
      ar.InsertAt (0, this);
    }

    #endregion methods

    #region indexer
    
    /// <summary>
    /// The indexer
    /// </summary>
    public string this[int index]
    {
      get { return (string)_Items[index]; }
    }

    #endregion indexer

    #region interface IEnumerable
                  
    /// <summary>
    /// The enumerator
    /// </summary>
    /// <returns>The enumerator</returns>
    public IEnumerator GetEnumerator ()
    {
      return _Items.GetEnumerator ();
    }

    #endregion interface IEnumerable

    #region ICollection Members

    /// <summary>
    /// Gets a value indicating whether access to the StringArray is synchronized (thread-safe).
    /// </summary>
    public bool IsSynchronized
    {
      get
      {
        // Add StringArray.IsSynchronized getter implementation
        return _Items.IsSynchronized;
      }
    }

    /// <summary>
    /// Gets the number of elements actually contained in the StringArray.
    /// </summary>
    public int Count
    {
      get
      {
        // Add StringArray.Count getter implementation
        return _Items.Count;
      }
    }

    /// <summary>
    /// Copies the entire StringArray to a compatible one-dimensional Array, 
    /// starting at the specified index of the target array.
    /// </summary>
    /// <param name="array">
    /// The one-dimensional Array that is the destination of the elements copied from StringArray.
    /// The Array must have zero-based indexing.
    /// </param>
    /// <param name="index">The zero-based index in array at which copying begins.</param>
    public void CopyTo(Array array, int index)
    {
      // Add StringArray.CopyTo implementation
      _Items.CopyTo ( array, index );
    }

    /// <summary>
    /// Gets an object that can be used to synchronize access to the StringArray.
    /// </summary>
    public object SyncRoot
    {
      get
      {
        // Add StringArray.SyncRoot getter implementation
        return _Items.SyncRoot;
      }
    }

    #endregion ICollection Members
  }
  

  /// <summary>
  /// Class ScriptParameter:
  /// Script parameter handling
  /// </summary>
  /// <remarks>
  /// This class involves the following script parameters:
  /// 1. Scanning parameters:         CycleTime, SigProcInt, PCCommInt
  /// 2. Zero calibration parameters: OffsetCorr
  /// 3. Scripting parameters:        ScriptID, SpanConcS(), AutoSpanMon()
  /// and others
  /// </remarks>
  public class ScriptParameter
  {
    #region constants

    /// <summary>
    /// The max. length of the 'sTotal' member
    /// (should be equal logically to the max. length of a substance name 'MAX_SCRIPT_WINDOWSUBSTANCENAME')
    /// </summary>
    public const int MAXLENGTH_TOTAL = 15;
    
    /// <summary>
    /// Meas. cycle time: Minimal value, Default value
    /// </summary>
    public const int MeasCycle_Min = 100; 
    public const int MeasCycle_Def = 600; 

    /// <summary>
    /// Signal processing interval: Minimal value, Maximal value, Factor between MinMax, Default value
    /// </summary>
    public const int SigProcInt_Min = 128; 
    public const int SigProcInt_Max = 2048; 
    public const int SigProcInt_Fac = 2; 
    public const int SigProcInt_Def = 512; 

    /// <summary>
    /// PC communication interval: Minimal value, Maximal value, Factor between MinMax, Default value
    /// </summary>
    public const int PCCommInt_Min = 128; 
    public const int PCCommInt_Max = 2048; 
    public const int PCCommInt_Fac = 2; 
    public const int PCCommInt_Def = 128; 
    
    #endregion constants
    
    #region constructors
    
    /// <summary>
    /// Constructor
    /// </summary>
    public ScriptParameter () 
    {
      Default ();
    }
    
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="par">The ScriptParameter object to be initialized with</param>
    public ScriptParameter ( ScriptParameter par ) 
    {
      dwCycleTime = par.dwCycleTime;    
      dwSigProcInt = par.dwSigProcInt;
      dwPCCommInt = par.dwPCCommInt;

      byMeasScript    = par.byMeasScript;
      
      byOffsetCorr    = par.byOffsetCorr;
      
      dwScriptID      = par.dwScriptID;
      for (int i=0; i < Script.MAX_SCRIPT_WINDOWS; i++)
        arfSpanConc[i] = par.arfSpanConc[i];
      for (int i=0; i < Script.MAX_SCRIPT_WINDOWS; i++)
        arAutoSpanMon[i] = new AutoSpanMon(par.arAutoSpanMon[i]);
 
      sTotal          = par.sTotal;
    }
  
    #endregion constructors

    #region members
    
    /// <summary>
    /// Duration of a meas. cycle (in 1/10 sec)
    /// </summary>
    public UInt32 dwCycleTime;      
    /// <summary>
    /// Interval between successive triggerings of signal processing actions (in scan array points), in (128, 256, 512, 1024, 2048)
    /// </summary>
    public UInt32 dwSigProcInt;      
    /// <summary>
    /// Interval between successive 'TX data to PC' actions (in scan array points), in (128, 256, 512, 1024, 2048)
    /// </summary>
    public UInt32 dwPCCommInt;      

    /// <summary>
    /// Indicates, Indicates, whether the script is a meas. script (1) or not (0)
    /// </summary>
    public byte byMeasScript;
    
    /// <summary>
    /// Indicates, whether a static scan offset correction should be performed (1) or not (0)
    /// </summary>
    public byte byOffsetCorr;
    
    /// <summary>
    /// The script ID (in [0, 2^32-1])
    /// </summary>
    public UInt32 dwScriptID;
    /// <summary>
    /// Soll concentrations for Span calibration (units as in Windows section) 
    /// </summary>
    public float[] arfSpanConc = new float[Script.MAX_SCRIPT_WINDOWS];
    /// <summary>
    /// 'Automated span monitoring' elements (units as in Windows section)
    /// </summary>
    public AutoSpanMon[] arAutoSpanMon = new AutoSpanMon[Script.MAX_SCRIPT_WINDOWS];
 
    /// <summary>
    /// Identifier for summary signal
    /// </summary>
    public string sTotal;
    
    #endregion members
    
    #region internal classes
  
    /// <summary>
    /// Class AutoSpanMon:
    /// 'Automated span monitoring' script parameters
    /// </summary>
    public class AutoSpanMon
    {

      #region constructors
      
      /// <summary>
      /// Constructor
      /// </summary>
      public AutoSpanMon()
      {
        Default ();
      }

      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="asm">The AutoSpanMon object to be initialized with</param>
      public AutoSpanMon(AutoSpanMon asm)
      {
        Enabled = asm.Enabled;
        LowLimit = asm.LowLimit;
        UppLimit = asm.UppLimit;
        NoErrMeas = asm.NoErrMeas;
      }

      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="sPar">The AutoSpanMon string representation to be initialized with</param>
      public AutoSpanMon(bool bEnabled, string sPar)
      {
        System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
        Enabled = bEnabled;
        string[] ars = sPar.Split ( ',' );
        LowLimit = float.Parse (ars[0], nfi);
        UppLimit = float.Parse (ars[1], nfi);
        NoErrMeas = int.Parse (ars[2]);
      }
      
      #endregion constructors

      #region members

      /// <summary>
      /// Indicates, whether the 'Automated span monitoring' object is enabled (true) or not (false)
      /// </summary>
      public bool Enabled;
      /// <summary>
      /// Concentration tolerance range: lower limit
      /// </summary>
      public float LowLimit;
      /// <summary>
      /// Concentration tolerance range: upper limit
      /// </summary>
      public float UppLimit;
      /// <summary>
      /// # of erroneous measurements
      /// </summary>
      public int NoErrMeas;

      #endregion members

      #region methods

      /// <summary>
      /// Sets default values.
      /// </summary>
      void Default ()
      {
        Enabled = false;
        LowLimit = 0;
        UppLimit = 0;
        NoErrMeas = 0;
      }

      /// <summary>
      /// Copies the instance to a compatible destination object. 
      /// </summary>
      /// <param name="asm">The destination object</param>
      public void CopyTo (AutoSpanMon asm)
      {
        asm.Enabled = this.Enabled;
        asm.LowLimit = this.LowLimit;
        asm.UppLimit = this.UppLimit;
        asm.NoErrMeas = this.NoErrMeas;
      }

      /// <summary>
      /// Converts the value of this instance to its equivalent string representation.
      /// </summary>
      /// <returns>The string representation</returns>
      public override string ToString()
      {
        System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
        string s;
        if (!Enabled)  
        {
          s = string.Format ("0,{0},{1},{2}", 
            LowLimit.ToString ("F0", nfi),
            UppLimit.ToString ("F0", nfi),
            NoErrMeas.ToString ()
            );
        }
        else
        {
          s = string.Format ("1,{0},{1},{2}", 
            LowLimit.ToString ("F3", nfi),
            UppLimit.ToString ("F3", nfi),
            NoErrMeas.ToString ()
            );
        }
        return s;
      }

      #endregion methods
    
    }
    
    #endregion internal classes
    
    #region methods

    /// <summary>
    /// Sets default values
    /// </summary>
    public void Default ()
    {
      dwCycleTime  = MeasCycle_Def;
      dwSigProcInt = SigProcInt_Def;
      dwPCCommInt = PCCommInt_Def;

      byMeasScript    = 0;
      
      byOffsetCorr    = 0;
      
      dwScriptID      = 0;
      for (int i=0; i < Script.MAX_SCRIPT_WINDOWS; i++)
        arfSpanConc[i] = 0;
      for (int i=0; i < Script.MAX_SCRIPT_WINDOWS; i++)
        arAutoSpanMon[i] = new AutoSpanMon ();
 
      sTotal          = "";
    }
    
    /// <summary>
    /// Validates the CycleTime parameter value:
    /// Must be in [10, +INF) sec
    /// </summary>
    /// <returns>True if valid, False otherwise</returns>
    bool IsValidCycleTime ()
    {
      return (dwCycleTime < MeasCycle_Min) ? false : true;
    }
    
    /// <summary>
    /// Validates the SigProcInt parameter value:
    /// Must be in (128,256,512,1024,2048)
    /// </summary>
    /// <returns>True if valid, False otherwise</returns>
    bool IsValidSigProcInt ()
    {
      for (int nSigProcInt=SigProcInt_Min; nSigProcInt <= SigProcInt_Max; nSigProcInt *= 2)
        if (nSigProcInt == dwSigProcInt) return true;
      return false;
    }
    
    /// <summary>
    /// Validates the PCCommInt parameter value:
    /// Must be in (128,256,512,1024,2048)
    /// </summary>
    /// <returns>True if valid, False otherwise</returns>
    bool IsValidPCCommInt ()
    {
      for (int nPCCommInt=PCCommInt_Min; nPCCommInt <= PCCommInt_Max; nPCCommInt *= 2)
        if (nPCCommInt == dwPCCommInt) return true;
      return false;
    }
    
    /// <summary>
    /// Validates the MeasScript parameter value
    /// </summary>
    bool IsValidMeasScript ()
    {
      if ((this.byMeasScript == 0) || (this.byMeasScript == 1))  return true;
      else                                                       return false;
    }
    
    /// <summary>
    /// Validates the OffsetCorr parameter value
    /// </summary>
    bool IsValidOffsetCorr ()
    {
      if ((this.byOffsetCorr == 0) || (this.byOffsetCorr == 1))  return true;
      else                                                       return false;
    }
    
    /// <summary>
    /// Validates the Total parameter value
    /// </summary>
    /// <returns></returns>
    bool IsValidTotal ()
    {
      return (this.sTotal.Length > MAXLENGTH_TOTAL) ? false : true;
    }
    
    /// <summary>
    /// Validates the parameter values
    /// </summary>
    /// <returns>True if valid, False otherwise</returns>
    public bool IsValid ()
    {
      return IsValidCycleTime () && IsValidSigProcInt () && IsValidPCCommInt () && 
        IsValidMeasScript ()&& IsValidOffsetCorr () && IsValidTotal ();
    }

    /// <summary>
    /// Converts the value of this instance to its equivalent string representation.
    /// </summary>
    /// <returns>The string representation</returns>
    public override string ToString()
    {
      string sx = string.Format ( "{0},{1},{2},{3},{4},{5}", 
        dwCycleTime, 
        dwSigProcInt, 
        dwPCCommInt,
        byMeasScript,
        byOffsetCorr, 
        dwScriptID
        );
 
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
      string s;
      for (int i=0; i < Script.MAX_SCRIPT_WINDOWS; i++)
      {
        if (arfSpanConc[i] == 0)  s = "0";
        else                      s = arfSpanConc[i].ToString ( nfi );
        sx += string.Format (",{0}", s);
      }
      for (int i=0; i < Script.MAX_SCRIPT_WINDOWS; i++)
      {
        s = arAutoSpanMon[i].ToString (); 
        sx += string.Format (",{0}", s);
      }
   
      sx += string.Format (",{0}", this.sTotal);
      
      return sx;
    }
    
    /// <summary>
    /// Converts parts of the value of this instance to equivalent string representations.
    /// </summary>
    /// <param name="idx">The indexed part</param>
    /// <returns>The string representation</returns>
    public string ToSubString(int idx)
    {
      string sx = "";
      int i;
      switch (idx)
      {
        case 0:     // Part 1 
          sx = string.Format ( "{0},{1},{2},{3},{4},{5},{6}", 
            dwCycleTime, 
            dwSigProcInt, 
            dwPCCommInt, 
            byMeasScript,
            byOffsetCorr,
            dwScriptID,
            sTotal
            );
          break;
        case 1:     // Part 2 
          System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
          string s;
          for (i=0; i < Script.MAX_SCRIPT_WINDOWS; i++)
          {
            if (arfSpanConc[i] == 0)  s = "0";
            else                      s = arfSpanConc[i].ToString ( nfi );
            sx += string.Format ("{0}", s);
            if (i < Script.MAX_SCRIPT_WINDOWS-1) sx += ",";
          }
          break;
        default:    // Part 3ff. 
          i = idx-2;                  // The idx of the required 'Automated span monitoring' element
          sx = this.arAutoSpanMon[i].ToString ();
          break;
      }
      return sx;
    }

    /// <summary>
    /// Loads the script parameters from a Key-Value string
    /// </summary>
    /// <param name="sKeyVal">The Key-Value string</param>
    public void Load (string sKeyVal)
    {
      string sKey = string.Empty, sVal = string.Empty;
      ScriptCollection.ExtractItemString (sKeyVal, ref sKey, ref sVal);

      switch (sKey)
      {
        case ScriptCollection.MeasCycle:                  // Meas. cycle
          dwCycleTime = UInt32.Parse (sVal); 
          if (!IsValidCycleTime ())
            throw new ScrParameterInvalidException ();
          break;
        
        case ScriptCollection.SigProcInt:                 // Signal processing interval
          dwSigProcInt = UInt32.Parse (sVal); 
          if (!IsValidSigProcInt ())
            throw new ScrParameterInvalidException ();
          break;
        
        case ScriptCollection.PCCommInt:                  // PC communication interval
          dwPCCommInt = UInt32.Parse (sVal); 
          if (!IsValidPCCommInt ())
            throw new ScrParameterInvalidException ();
          break;
 
        case ScriptCollection.MeasScript:                 // Meas. script ident.
          this.byMeasScript = byte.Parse (sVal);
          if (!IsValidMeasScript ())
            throw new ScrParameterInvalidException ();
          break;
        
        case ScriptCollection.OffsetCorr:                 // Static scan offset correction
          this.byOffsetCorr = byte.Parse (sVal);
          if (!IsValidOffsetCorr ())
            throw new ScrParameterInvalidException ();
          break;
        
        case ScriptCollection.ScriptID:                   // Script ID
          dwScriptID = UInt32.Parse (sVal); 
          break;
        
        default:                                          // all other
          System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
          string s;
          // Check: Span concentration row?
          int nIdx = sKey.IndexOf (ScriptCollection.SpanConc);
          if ( nIdx != -1)
          {
            // Yes:
            s = sKey.Substring(ScriptCollection.SpanConc.Length);
            try
            {
              nIdx = int.Parse(s);
              float fSpanConc = float.Parse (sVal, nfi);
              // Ensure: Span conc. > 0
              if (fSpanConc == 0.0)
                throw new ScrParameterInvalidException ();
              this.arfSpanConc[nIdx] = fSpanConc;
            }
            catch
            {
              throw new ScrParameterInvalidException ();             
            }
          }
          else
          {
            // No:
            // Check: 'Automated span monitoring' row?
            nIdx = sKey.IndexOf (ScriptCollection.AutoSpanMon);
            if ( nIdx != -1)
            {
              // Yes:
              s = sKey.Substring(ScriptCollection.AutoSpanMon.Length);
              try
              {
                nIdx = int.Parse(s);
                AutoSpanMon asm = new AutoSpanMon (true, sVal);                
                asm.CopyTo (this.arAutoSpanMon[nIdx]);
              }
              catch
              {
                throw new ScrParameterInvalidException ();             
              }
            }
            else
            { 
              // No:
              // erroneous key
              Debug.WriteLine ( "Failed: ScriptParameter.Load()\n" );
              throw new ScrParameterInvalidException ();
            }
          }
          break;
 
        case ScriptCollection.Total:                      // Total
          this.sTotal = sVal; 
          if (!IsValidTotal ())
            throw new ScrParameterInvalidException ();
          break;
      }
    }

    /// <summary>
    /// Copies the instance to a compatible destination object. 
    /// </summary>
    /// <param name="sp">The destination object</param>
    public void CopyTo (ScriptParameter sp)
    {
      sp.dwCycleTime  = this.dwCycleTime;   
      sp.dwSigProcInt = this.dwSigProcInt;
      sp.dwPCCommInt  = this.dwPCCommInt;
   
      sp.byMeasScript = this.byMeasScript;
      
      sp.byOffsetCorr = this.byOffsetCorr;
      
      sp.dwScriptID   = this.dwScriptID;
      for (int i=0; i < Script.MAX_SCRIPT_WINDOWS; i++)
        sp.arfSpanConc[i] = this.arfSpanConc[i];
      for (int i=0; i < Script.MAX_SCRIPT_WINDOWS; i++)
        this.arAutoSpanMon[i].CopyTo (sp.arAutoSpanMon[i]);
  
      sp.sTotal       = this.sTotal;
    }

    /// <summary>
    /// Checks, whether the script parameters are valid.
    /// In case of invalidity an exception is thrown.
    /// </summary>
    public void Check ()
    {
      // Script parameters message string length check
      App app = App.Instance;
      CommMessage msg;
      int nAnzParts = 2 + Script.MAX_SCRIPT_WINDOWS;    // # of script parameter (string) parts
      for (int j=0; j < nAnzParts; j++)
      {
        //    Build the partial 'ScriptParam' message
        msg = new CommMessage (app.Comm.msgScriptParameter);
        msg.Parameter = string.Format ("{0},{1}", j, this.ToSubString (j));
        //    Check the message:
        //      If the message doesn't contain a command (Length = 0), or if the message string is too long 
        //      in order to be received well by the device UART (Length > TXBUFSIZE-6, '6': 1 char string 
        //      end recognition, 1 char End recognition for serial comm., 4 chars CRC16), 
        //      then throw the corr'ing exception.
        string sMsg = msg.ToString ();
        if ((sMsg.Length == 0) || (sMsg.Length > AppComm.TXBUFSIZE-6))
          throw new ScrMsgStringlengthException ();
      }
    }
   
    #endregion methods

  }
  

  /// <summary>
  /// Class ConcUnits:
  /// Provides the different substance concentration units
  /// </summary>
  /// <remarks>
  /// NOTE: 
  ///  The following concentration units MUST coincide with those used by the device SW
  ///  (array 'g_arsConcUnit[]').
  /// </remarks>
  public sealed class ConcUnits
  {
    /// <summary>
    /// Constructor
    /// </summary>
    private ConcUnits ()
    {
    }

    #region constants
    
    /// <summary>
    /// Conc. unit: None
    /// </summary>
    public const string None    = "";
    /// <summary>
    /// Conc. unit: ppt
    /// </summary>
    public const string ppt     = "ppt";
    /// <summary>
    /// Conc. unit: ppb
    /// </summary>
    public const string ppb     = "ppb";
    /// <summary>
    /// Conc. unit: ppm
    /// </summary>
    public const string ppm     = "ppm";
    /// <summary>
    /// Conc. unit: �g/m^3
    /// </summary>
    public const string ug_cbm  = "ug/cbm";
    /// <summary>
    /// Conc. unit: mg/m^3
    /// </summary>
    public const string mg_cbm  = "mg/cbm";
    /// <summary>
    /// Conc. unit: Percent
    /// </summary>
    public const string Percent = "%";

    // Concentration units handling
    //  No of decimal digits
    //  NOTE:
    //    The # of decimal digits assigned here should coincide with those used by the device SW (see corr'ing #def's).
    const int _CONCUNIT_NODECDIG_PPT = 0;         // ppt
    const int _CONCUNIT_NODECDIG_PPB = 3;         // ppb
    const int _CONCUNIT_NODECDIG_PPM = 3;         // ppm
    const int _CONCUNIT_NODECDIG_UG_CBM = 3;      // �g/m^3 
    const int _CONCUNIT_NODECDIG_MG_CBM = 3;      // mg/m^3
    const int _CONCUNIT_NODECDIG_PERCENT = 1;     // %

    #endregion constants

    #region methods

    /// <summary>
    /// Specifies the number of postdecimal digits corr'ing to a given concentration unit.
    /// </summary>
    /// <param name="sConcUnit">The concentration unit</param>
    /// <returns>The number of postdecimal digits</returns>
    public static int GetDecimals (string sConcUnit)
    {
      int nPostDigits = 0;
      switch (sConcUnit)
      {
        case ppt: nPostDigits = _CONCUNIT_NODECDIG_PPT; break;
        case ppb: nPostDigits = _CONCUNIT_NODECDIG_PPB; break;
        case ppm: nPostDigits = _CONCUNIT_NODECDIG_PPM; break;
        case ug_cbm: nPostDigits = _CONCUNIT_NODECDIG_UG_CBM; break;
        case mg_cbm: nPostDigits = _CONCUNIT_NODECDIG_MG_CBM; break;
        case Percent: nPostDigits = _CONCUNIT_NODECDIG_PERCENT; break;
      }
      return nPostDigits;
    }

    #endregion methods

  }


  /// <summary>
  /// Class ScriptWindow:
  /// Script window handling
  /// </summary>
  public class ScriptWindow 
  {
    #region constants

    /// <summary>
    /// Min. number of allocated substance calibration points
    /// </summary>
    public const int MIN_CALIB_POINTS = 2;
    /// <summary>
    /// Max. number substance calibration points
    /// NOTE:
    ///   The value of this constant MUST conincide with the corr'ing value in the device SW. 
    /// </summary>
    public const int MAX_CALIB_POINTS = 8;
    /// <summary>
    /// Max. length of substance names
    /// </summary>
    public const int MAX_SCRIPT_WINDOWSUBSTANCENAME = 15;

    #endregion constants

    #region constructors
    
    /// <summary>
    /// Constructor
    /// </summary>
    public ScriptWindow () 
    {
      Default ();
    }
    
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="wnd">The ScriptWindow object to be initialized with</param>
    public ScriptWindow ( ScriptWindow wnd ) 
    {
      if (wnd.szSubstance.Length > MAX_SCRIPT_WINDOWSUBSTANCENAME) 
        szSubstance = wnd.szSubstance.Substring (0, MAX_SCRIPT_WINDOWSUBSTANCENAME);
      else
        szSubstance = wnd.szSubstance;
      
      dwBegin     = wnd.dwBegin;
      dwWidth     = wnd.dwWidth;
      dA1Limit    = wnd.dA1Limit;
      dA2Limit    = wnd.dA2Limit;
      dZeroVal    = wnd.dZeroVal;
      dDispLimit  = wnd.dDispLimit;
      dSpanfac    = wnd.dSpanfac;  
      dUserVal    = wnd.dUserVal;
      
      nCalType    = wnd.nCalType;
      for ( int i=0; i < MAX_CALIB_POINTS; i++ )
      {
        ardXcal[i] = wnd.ardXcal[i];
        ardYcal[i] = wnd.ardYcal[i];
      }
      
      sConcUnit = wnd.sConcUnit;
    }
  
    #endregion constructors

    #region members
    
    /// <summary>
    /// Substance name (max. 'MAX_SCRIPT_WINDOWSUBSTANCENAME' chars)
    /// </summary>
    public string szSubstance;  
    
    /// <summary>
    /// Begin of the window (in pts)
    /// </summary>
    public UInt32 dwBegin;       
    /// <summary>
    /// Width of the window (in pts)) 
    /// </summary>
    public UInt32 dwWidth; 
    /// <summary>
    /// Level for LO alarm setting 
    /// </summary>
    public double dA1Limit;              
    /// <summary>
    /// Level for HI alarm setting 
    /// </summary>
    public double dA2Limit;              
    /// <summary>
    /// Baseline correction for result value
    /// </summary>
    public double dZeroVal;     
    /// <summary>
    /// All results below this value are set to 0.0
    /// </summary>
    public double dDispLimit;   
    /// <summary>
    /// Span factor for calibration table
    /// </summary>
    public double dSpanfac; 
    /// <summary>
    /// User value for DAC output: if 'concentration' >= 'Userval', then 20mA = 4096 digits are output via current loop
    /// </summary>
    public double dUserVal;

    /// <summary>
    /// Calibration type: 0 - height, 1 - area
    /// </summary>
    public int nCalType;
    /// <summary>
    /// Calibration array: Abscissa (Peak areas)
    /// </summary>
    public double[] ardXcal = new double[MAX_CALIB_POINTS];       
    /// <summary>
    /// Calibration array: Ordinate (Concentrations)
    /// </summary>
    public double[] ardYcal = new double[MAX_CALIB_POINTS];       

    /// <summary>
    /// Concentration unit
    /// </summary>
    public string sConcUnit;

    #endregion members

    #region methods

    /// <summary>
    /// Sets default values
    /// </summary>
    public void Default ()
    {
      szSubstance = "XXXX";
      
      dwBegin     = 100;
      dwWidth     = 10;
      dA1Limit    = 90.0;
      dA2Limit    = 100.0;
      dZeroVal    = 0.0;
      dDispLimit  = 0.0;
      dSpanfac    = 1.0;
      dUserVal    = 0.0;

      nCalType    = 0;
      for ( int i=0; i < MAX_CALIB_POINTS; i++ )
      {
        ardXcal[i] = i * 10000;
        ardYcal[i] = i * 10.0;
      }
      
      sConcUnit   = ConcUnits.ppb;
    }
    
    /// <summary>
    /// Converts the value of this instance to its equivalent string representation.
    /// </summary>
    /// <returns>The string representation</returns>
    public override string ToString()
    {
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;  
      string sx = string.Format ( 
        "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", 
        szSubstance, 
        dwBegin, 
        dwWidth, 
        dA1Limit.ToString ( nfi ), 
        dA2Limit.ToString ( nfi ), 
        dZeroVal.ToString ( nfi ), 
        dDispLimit.ToString ( nfi ),
        dSpanfac.ToString ( nfi ), 
        dUserVal.ToString ( nfi ),
        nCalType
        );
      
      int i;
      for ( i=0; i < MAX_CALIB_POINTS; i++ )
        if ((ardXcal[i] == -1) && (ardYcal[i] == -1)) break;
      int nCal = i;
      for ( i=0; i < nCal; i++ )
        sx += string.Format ( ",{0},{1}", ardXcal[i].ToString ( nfi ), ardYcal[i].ToString ( nfi ) );
      
      sx += string.Format ( ",{0}", sConcUnit );
      return sx;
    }

    /// <summary>
    /// Converts parts of the value of this instance to equivalent string representations.
    /// </summary>
    /// <param name="idx">The indexed part</param>
    /// <returns>The string representation</returns>
    public string ToSubString(int idx)
    {
      int i;
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;  
      string sx = "";
      switch (idx)
      {
        case 0:
          sx = string.Format ( 
            "{0},{1},{2},{3},{4},{5},{6},{7},{8}", 
            szSubstance, 
            dwBegin, 
            dwWidth, 
            dA1Limit.ToString ( nfi ), 
            dA2Limit.ToString ( nfi ), 
            dZeroVal.ToString ( nfi ), 
            dDispLimit.ToString ( nfi ),
            dSpanfac.ToString ( nfi ), 
            dUserVal.ToString ( nfi )
            );
          break;
        case 1:
          sx += string.Format ( "{0}", nCalType );
          for ( i=0; i < MAX_CALIB_POINTS; i++ )
            if ((ardXcal[i] == -1) && (ardYcal[i] == -1)) break;
          int nCal = i;
          for ( i=0; i < nCal; i++ )
            sx += string.Format ( ",{0},{1}", ardXcal[i].ToString ( nfi ), ardYcal[i].ToString ( nfi ) );
          sx += string.Format ( ",{0}", sConcUnit );
          break;
      }
      return sx;
    }
    
    /// <summary>
    /// Loads a script window from a Key-Value string
    /// </summary>
    /// <param name="sKeyVal">The Key-Value string</param>
    public void Load (string sKeyVal)
    {
      string sKey = string.Empty, sVal = string.Empty;
      ScriptCollection.ExtractItemString (sKeyVal, ref sKey, ref sVal);
    
      // Notes:
      //  The # of Value string tokens must be one of the following values:
      //    9 + 0 + 1
      //    9 + 2 + 1
      //    ...
      //    9 + 2*'MAX_CALIB_POINTS' + 1
      // i.e., it must be an even #, >= 10, and <= 10 + 2x'MAX_CALIB_POINTS'.
      string[] ars = sVal.Split ( ',' );
      int n = ars.Length;
      if ( ((n%2) == 1) || (n < 10) || (n > 10+MAX_CALIB_POINTS*2) )
      {
        // Error (the above demand isn't met) 
        Debug.WriteLine ( "Failed: ScriptWindow.Load()\n" );
        throw new ApplicationException ();
      }

      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
      szSubstance = sKey;
  
      if ((szSubstance.Length == 0) || (szSubstance.Length > MAX_SCRIPT_WINDOWSUBSTANCENAME))
      {
        Debug.WriteLine ( "Failed: ScriptWindow.Load()\n" );
        throw new ScrWndNameInvalidException ();
      }
      
      dwBegin     = UInt32.Parse ( ars[0] );
      dwWidth     = UInt32.Parse ( ars[1] );
      dA1Limit    = double.Parse ( ars[2], nfi );
      dA2Limit    = double.Parse ( ars[3], nfi );
      dZeroVal    = double.Parse ( ars[4], nfi );
      dDispLimit  = double.Parse ( ars[5], nfi );
      dSpanfac    = double.Parse ( ars[6], nfi );  
      dUserVal    = double.Parse ( ars[7], nfi );
      
      nCalType    = int.Parse ( ars[8] );
      int nCal = (n-10)/2;                            // The # of calib. points
      int i, j;
      for ( i=0, j=9; i < nCal; i++ )                 // Fill in the required calib. points
      {
        ardXcal[i] = double.Parse ( ars[j++], nfi );
        ardYcal[i] = double.Parse ( ars[j++], nfi );
      }
      for ( ; i < MAX_CALIB_POINTS; i++)              // Zero out the non-required calib. points  
      {
        ardXcal[i] = -1;
        ardYcal[i] = -1;
      }
      
      sConcUnit   = ars[j];
    }

    /// <summary>
    /// Copies the instance to a compatible destination object. 
    /// </summary>
    /// <param name="sw">The destination object</param>
    public void CopyTo (ScriptWindow sw)
    {
      sw.szSubstance  = this.szSubstance;
      
      sw.dwBegin      = this.dwBegin;
      sw.dwWidth      = this.dwWidth;
      sw.dA1Limit     = this.dA1Limit;
      sw.dA2Limit     = this.dA2Limit;
      sw.dZeroVal     = this.dZeroVal;
      sw.dDispLimit   = this.dDispLimit;
      sw.dSpanfac     = this.dSpanfac;
      sw.dUserVal     = this.dUserVal;
      
      sw.nCalType     = this.nCalType;
      for ( int i=0; i < MAX_CALIB_POINTS; i++ )
      {
        sw.ardXcal[i] = this.ardXcal[i];
        sw.ardYcal[i] = this.ardYcal[i];
      }
      
      sw.sConcUnit    = this.sConcUnit;
    }
    
    /// <summary>
    /// Checks, whether a script window is valid.
    /// In case of invalidity an exception is thrown.
    /// </summary>
    public void Check ()
    {
      bool bEr = false;

      // 1. calibration array consistency check:
      // Demand: Related data must be either pairwise valid or pairwise empty.  
      if (!bEr)
      {
        for (int i=0; i < ardXcal.Length; i++)
          if ((ardXcal[i] != -1) != (ardYcal[i] != -1))   { bEr = true; break; }  // Demand not fulfilled
      }
      // 2. calibration array consistency check:
      // Demand: Valid data MUST NOT follow empty data.
      if (!bEr)
      {
        for (int i=0; i < ardXcal.Length-1; i++)
          if ((ardXcal[i] == -1) && (ardXcal[i+1] != -1)) { bEr = true; break; }  // Demand not fulfilled
      }
      // 3. calibration array consistency check:
      // Demand: The abscissas of valid data MUST be in ascending order.
      if (!bEr)
      {
        for (int i=0; i < ardXcal.Length-1; i++)
        {
          if ((ardXcal[i] != -1) && (ardXcal[i+1] != -1))
          {
            if (!(ardXcal[i] < ardXcal[i+1]))             { bEr = true; break; }  // Demand not fulfilled
          }
        }
      }
      // Check: Did one of the consistency checks fail?
      if (bEr)
      {
        // Yes: Throw the corr'ing exception
        throw new ScrWndInconsistencyException ();
      }
      
      // Check: Min. number of allocated substance calibration points reached?
      int count = 0;
      for (int i=0; i < ardXcal.Length; i++)
        if ((ardXcal[i] != -1) && (ardYcal[i] != -1))   count++;
      if (count < MIN_CALIB_POINTS)
      {
        // No: Throw the corr'ing exception
        throw new ScrWndMinNoCalibPointsException ();
      }

      // Script window message string length check
      App app = App.Instance;
      CommMessage msg;
      for (int j=0; j < 2; j++)
      {
        //    Build the partial 'ScriptWindow' message
        msg = new CommMessage (app.Comm.msgScriptWindow);
        msg.Parameter = string.Format ("{0},{1}", j, this.ToSubString (j));
        //    Check the message:
        //      If the message doesn't contain a command (Length = 0), or if the message string is too long 
        //      in order to be received well by the device UART (Length > TXBUFSIZE-6, '6': 1 char string 
        //      end recognition, 1 char End recognition for serial comm., 4 chars CRC16), 
        //      then throw the corr'ing exception.
        string sMsg = msg.ToString ();
        if ((sMsg.Length == 0) || (sMsg.Length > AppComm.TXBUFSIZE-6))
          throw new ScrMsgStringlengthException ();
      }

      // Check: TotalVOC windows are allowed only with calibration type 'area'
      if (this.dwBegin == 0 && this.dwWidth == 0)
      {
        if (this.nCalType != 1)
          throw new ScrWndTotalVOCException ();
      }

    }

    #endregion methods

    #region methods required for proper script editor handling

    /// <summary>
    /// Converts the value of this instance to a special (full) string representation.
    /// </summary>
    /// <returns>The string representation</returns>
    public string ToStringFull()
    {
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;  
      string sx = string.Format ( 
        "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", 
        szSubstance, 
        dwBegin, 
        dwWidth, 
        dA1Limit.ToString ( nfi ), 
        dA2Limit.ToString ( nfi ), 
        dZeroVal.ToString ( nfi ), 
        dDispLimit.ToString ( nfi ),
        dSpanfac.ToString ( nfi ), 
        dUserVal.ToString ( nfi ),
        nCalType
        );
      
      for (int i=0; i < MAX_CALIB_POINTS; i++ )
        sx += string.Format ( ",{0},{1}", 
          (ardXcal[i] == -1) ? "" : ardXcal[i].ToString ( nfi ), 
          (ardYcal[i] == -1) ? "" : ardYcal[i].ToString ( nfi )
          );

      sx += string.Format ( ",{0}", sConcUnit );
      return sx;
    }

    /// <summary>
    /// Loads a script window from a special (full) Key-Value string
    /// </summary>
    /// <param name="sKeyVal">The Key-Value string</param>
    public void LoadFull (string sKeyVal)
    {
      string sKey = string.Empty, sVal = string.Empty;
      ScriptCollection.ExtractItemString (sKeyVal, ref sKey, ref sVal);
    
      // Notes:
      //  The # of Value string tokens must equal to the following values:
      //    9 + 2*'MAX_CALIB_POINTS' + 1
      string[] ars = sVal.Split ( ',' );
      if (ars.Length != 10+MAX_CALIB_POINTS*2)
      {
        // Error (the above demand isn't met) 
        Debug.WriteLine ( "Failed: ScriptWindow.Load()\n" );
        throw new ApplicationException ();
      }
      
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
      szSubstance = sKey;

      if ((szSubstance.Length == 0) || (szSubstance.Length > MAX_SCRIPT_WINDOWSUBSTANCENAME))
      {
        Debug.WriteLine ( "Failed: ScriptWindow.Load()\n" );
        throw new ScrWndNameInvalidException ();
      }

      dwBegin     = UInt32.Parse ( ars[0] );
      dwWidth     = UInt32.Parse ( ars[1] );
      dA1Limit    = double.Parse ( ars[2], nfi );
      dA2Limit    = double.Parse ( ars[3], nfi );
      dZeroVal    = double.Parse ( ars[4], nfi );
      dDispLimit  = double.Parse ( ars[5], nfi );
      dSpanfac    = double.Parse ( ars[6], nfi );  
      dUserVal    = double.Parse ( ars[7], nfi );
      
      nCalType    = int.Parse ( ars[8] );
      int i, j;
      for (i=0, j=9; i < MAX_CALIB_POINTS; i++ )            
      {
        ardXcal[i] = (ars[j].Length == 0) ? -1 : double.Parse ( ars[j], nfi ); j++;
        ardYcal[i] = (ars[j].Length == 0) ? -1 : double.Parse ( ars[j], nfi ); j++;
      }

      sConcUnit   = ars[j];
    }

    #endregion methods required for proper script editor handling

  }
  
  
  /// <summary>
  /// Class ScriptEvent:
  /// Script event handling
  /// </summary>
  public class ScriptEvent
  {
    #region constants

    /// <summary>
    /// Max. length of a script event command
    /// </summary>
    public const int MAX_SCRIPT_EVENTCMDSIZE = 15;
    /// <summary>
    /// Max. # of script event parameters
    /// </summary>
    public const UInt16 MAX_SCREVT_PARAMS = 5;

    #endregion constants

    #region constructors
    
    /// <summary>
    /// Constructor
    /// </summary>
    public ScriptEvent () 
    {
      Empty ();
    }
    
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="evt">The ScriptEvent object to be initialized with</param>
    public ScriptEvent ( ScriptEvent evt ) 
    {
      dwTime = evt.dwTime;
      
      if (evt.szCommand.Length > MAX_SCRIPT_EVENTCMDSIZE) 
        szCommand = evt.szCommand.Substring (0, MAX_SCRIPT_EVENTCMDSIZE);
      else
        szCommand = evt.szCommand;
      
      Array.Copy ( evt.ardwParameter, 0, ardwParameter, 0, Math.Min ( evt.wParNumber, MAX_SCREVT_PARAMS ) );
      wParNumber = evt.wParNumber;
    }
  
    #endregion constructors

    #region members
    
    /// <summary>
    /// Execution time in 1/10 sec
    /// </summary>
    public UInt32 dwTime;   
    /// <summary>
    /// Command
    /// </summary>
    public string szCommand;    
    /// <summary>
    /// Parameters
    /// </summary>
    public UInt32[] ardwParameter = new UInt32[MAX_SCREVT_PARAMS]; 
    /// <summary>
    /// Number of parameters
    /// </summary>
    public UInt16 wParNumber;                                         

    #endregion members

    #region methods

    /// <summary>
    /// Initiates the instance members
    /// </summary>
    public void Empty () 
    {
      dwTime = 0;
      
      szCommand = string.Empty;
      
      for ( int i=0; i < MAX_SCREVT_PARAMS ;i++ ) ardwParameter[i] = 0; 
      wParNumber = 0;
    }

    /// <summary>
    /// Converts the value of this instance to its equivalent string representation.
    /// </summary>
    /// <returns>The string representation</returns>
    public override string ToString()
    {
      string sx = string.Format ( "{0},{1}", dwTime, szCommand );
      for ( int i=0; i < wParNumber ;i++ ) sx += string.Format ( ",{0}", ardwParameter[i] );
      return sx;
    }

    /// <summary>
    /// Loads a script command from a Key-Value string
    /// </summary>
    /// <param name="sKeyVal">The Key-Value string</param>
    public void Load (string sKeyVal)
    {
      string sKey = string.Empty, sVal = string.Empty;
      ScriptCollection.ExtractItemString (sKeyVal, ref sKey, ref sVal);
    
      string[] ars = sVal.Split ( ',' );
      if ( ars.Length < 2 || ars.Length > MAX_SCREVT_PARAMS+1) 
      {
        Debug.WriteLine ( "Failed: ScriptEvent.Load()\n" );
        throw new ApplicationException ();
      }

      dwTime = UInt32.Parse ( sKey );
      
      if (ars[0].Length > MAX_SCRIPT_EVENTCMDSIZE) 
        szCommand = ars[0].Substring (0, MAX_SCRIPT_EVENTCMDSIZE);
      else
        szCommand = ars[0];
      
      wParNumber = (UInt16)(ars.Length-1);
      for ( int i=0; i < wParNumber ;i++ ) ardwParameter[i] = UInt32.Parse ( ars[i+1] );
    }

    /// <summary>
    /// Copies the instance to a compatible destination object. 
    /// </summary>
    /// <param name="se">The destination object</param>
    public void CopyTo (ScriptEvent se)
    {
      se.dwTime = this.dwTime;
      
      se.szCommand = this.szCommand;
      
      for ( int i=0; i < MAX_SCREVT_PARAMS ;i++ ) 
        se.ardwParameter[i] = this.ardwParameter[i]; 
      se.wParNumber = this.wParNumber;
    }

    /// <summary>
    /// Checks, whether a script command is valid.
    /// In case of invalidity an exception is thrown.
    /// </summary>
    public void Check ()
    {
      // Check, whether the script command matches one of the available script commands or not
      if (!ScriptCommands.Check (this.szCommand))
        throw new ScrCmdNameInvalidException ();
      // Check, whether the script command parameters are valid or not
      // (parameter # check)
      if (this.wParNumber != ScriptCommands.ParamNumberOnCmd(this.szCommand))
        throw new ScrCmdParameterInvalidException ();
      // Check, whether the script command parameters are valid or not
      // (parameter itself check)
      if (ScriptCommands.IsCBCmd (this.szCommand))
      {
        // CB commands:
        // Get the parameter ranges
        int min1=0, max1=-1, min2=0, max2=-1; // 1. and 2. ranges
        ScriptCommands.ParamRangeOnCmd (this.szCommand, ref min1, ref max1, ref min2, ref max2);
        // Examine 1st range
        bool b1 = false;
        if (max1 >= min1)
          if ((this.ardwParameter[0] >=  min1) && (this.ardwParameter[0] <=  max1)) b1 = true; 
        // Examine 2nd range
        bool b2 = false;
        if (max2 >= min2)
          if ((this.ardwParameter[0] >=  min2) && (this.ardwParameter[0] <=  max2)) b2 = true; 
        // Overall result
        if (!(b1 || b2))
          throw new ScrCmdParameterInvalidException ();
      }
      else
      {
        // TB commands:
        // Build the parameter string
        string sPar = "", sParToken;
        for (int i=0; i < wParNumber; i++)
        {
          sPar += string.Format ("{0}", ardwParameter[i]);
          if (i < wParNumber-1) sPar += ",";
        }
        // Perform a parameter validity check
        int res = CheckParameters (this.szCommand, sPar, out sParToken);
        if (res > 0)
          // Error
          throw new ScrCmdParameterInvalidException ();
      }
    }

    /// <summary>
    /// Performs a parameter validity check.
    /// </summary>
    /// <param name="sCmd">The script command string</param>
    /// <param name="sPar">The script parameter string</param>
    /// <param name="sParToken">Only in case of error: The token of the parameter string, which caused an error (out)</param>
    /// <returns>
    /// 0 - OK
    /// 1 - Error: parameter number outside the defined margins
    /// 2 - Error: parameter number doesn't correspond with the defined one
    /// 3 - Error: parameter is NOT a non-negative integer
    /// 4 - Error: parameter themselves outside the defined margins
    /// </returns>
    static public int CheckParameters(string sCmd, string sPar, out string sParToken)
    {
      int n;
      sParToken = "";

      // Get the parameters as a string array
      string[] ars = sPar.Split (new char[] {','});
      // Check: Parameter # margins
      if (ars.Length < 1 || ars.Length > MAX_SCREVT_PARAMS)
        return 1;
      // Check: Parameter # with regard to the command
      int number = ScriptCommands.ParamNumberOnCmd (sCmd); 
      if ( ars.Length != number )
        return 2;
      // Check: Parameter themselves
      int min1=0, max1=-1, min2=0, max2=-1;     // 1. and 2. parameter ranges
      ScriptCommands.ParamRangeOnCmd (sCmd, ref min1, ref max1, ref min2, ref max2);
      //    Loop over all parameters
      foreach (string s1 in ars)
      {
        // Overgive
        sParToken = s1;
        // Check: Each parameter MUST be a non-negative integer
        try 
        {
          n = int.Parse (s1);
          if (n < 0)
            throw new ArgumentOutOfRangeException ();
        }
        catch 
        {
          return 3;
        }
        // Examine 1st parameter range
        bool b1 = false;
        if (max1 >= min1)
          if ((n >=  min1) && (n <=  max1)) b1 = true; 
        // Examine 2nd parameter range
        bool b2 = false;
        if (max2 >= min2)
          if ((n >=  min2) && (n <=  max2)) b2 = true; 
        // Overall result over both ranges
        if (!(b1 || b2))
          return 4;
      }

      // Perform a special parameter check
      if (!ScriptCommands.CheckParameters (sCmd, sPar, ref sParToken))
        return 4;

      // Done: OK
      return 0;
    }
    
    #endregion methods

  }
  

  /// <summary>
  /// Class Script:
  /// Script handling
  /// </summary>
  public class Script 
  {
    #region constants
    
    /// <summary>
    /// Max. number of substance windows per script
    /// </summary>
    public const int MAX_SCRIPT_WINDOWS = 16;
    /// <summary>
    /// Max. number of commands (events) per script
    /// </summary>
    public const int MAX_SCRIPT_EVENTS = 64;
    /// <summary>
    /// Max. length of a script name
    /// </summary>
    public const int MAX_SCRIPT_NAMELENGTH = 29;

    #endregion constants
    
    #region constructors
    
    /// <summary>
    /// Constructor
    /// </summary>
    public Script()
    {
      // Create the script windows
      for (int i=0; i < MAX_SCRIPT_WINDOWS; i++)
        Windows[i] = new ScriptWindow ();
      // Create the script commands
      for (int i=0; i < MAX_SCRIPT_EVENTS; i++)
        Cmds[i] = new ScriptEvent ();
      // Reset the script
      Reset ();
    }
    
    #endregion constructors
   
    #region members
    
    /// <summary>
    /// The script name
    /// </summary>
    string _Name = "Default";

    /// <summary>
    /// The script parameters
    /// </summary>
    public ScriptParameter Parameter = new ScriptParameter ();
    
    /// <summary>
    /// The script window array
    /// </summary>
    public ScriptWindow[] Windows = new ScriptWindow[MAX_SCRIPT_WINDOWS]; 

    /// <summary>
    /// The # of script windows
    /// </summary>
    public int NumWindows = 0;

    /// <summary>
    /// The script command array
    /// </summary>
    public ScriptEvent[] Cmds = new ScriptEvent[MAX_SCRIPT_EVENTS];  
  
    /// <summary>
    /// The # of script commands
    /// </summary>
    public int NumCommands = 0;

    /// <summary>
    /// The script comments
    /// </summary>
    public StringArray arsComment = new StringArray ();
    /// <summary>
    /// The Parameter section comments
    /// </summary>
    public StringArray arsParComment = new StringArray ();
    /// <summary>
    /// The Windows section comments
    /// </summary>
    public StringArray arsWndComment = new StringArray ();
    /// <summary>
    /// The Commands section comments
    /// </summary>
    public StringArray arsCmdComment = new StringArray ();
    
    /// <summary>
    /// Indicates, that the number of script windows exceeds the permitted max. number
    /// </summary>
    public bool MaxWindowNoExceeded = false;
    /// <summary>
    /// Indicates, that the number of script commands exceeds the permitted max. number
    /// </summary>
    public bool MaxCmdNoExceeded = false;

    #endregion members
  
    #region properties

    /// <summary>
    /// The script name
    /// </summary>
    public string Name
    {
      get { return _Name; }
      set 
      {
        if (value.Length > MAX_SCRIPT_NAMELENGTH) 
          _Name = value.Substring (0, MAX_SCRIPT_NAMELENGTH);
        else
          _Name = value;
      }
    }

    #endregion properties

    #region methods

    /// <summary>
    /// Resets the instance
    /// </summary>
    public void Reset ()
    {
      _Name = "Default";
      Parameter.Default ();
      NumWindows = 0;
      NumCommands = 0;
      // Comments
      arsComment.Clear ();
      arsParComment.Clear ();
      arsWndComment.Clear ();
      arsCmdComment.Clear ();
      // Indicators: Max. item number exceeded
      MaxWindowNoExceeded = false;
      MaxCmdNoExceeded = false;
    }
    
    /// <summary>
    /// Copies the instance to a compatible destination object. 
    /// </summary>
    /// <param name="scr">The destination object</param>
    public void CopyTo (Script scr)
    {
      // Name
      scr.Name = this.Name;
      // Parameter
      this.Parameter.CopyTo ( scr.Parameter );
      // Windows
      for (int i=0; i < this.NumWindows; i++)
        this.Windows[i].CopyTo (scr.Windows[i]);
      scr.NumWindows = this.NumWindows;
      // Cmds
      for (int i=0; i < this.NumCommands; i++)
        this.Cmds[i].CopyTo (scr.Cmds[i]);
      scr.NumCommands = this.NumCommands;

      // Comments
      this.arsComment.CopyTo (scr.arsComment);
      this.arsParComment.CopyTo (scr.arsParComment);
      this.arsWndComment.CopyTo (scr.arsWndComment);
      this.arsCmdComment.CopyTo (scr.arsCmdComment);
    
      // Indicators: Max. item number exceeded
      scr.MaxWindowNoExceeded = this.MaxWindowNoExceeded;
      scr.MaxCmdNoExceeded = this.MaxCmdNoExceeded;
    }

    /// <summary>
    /// Checks, whether a script is valid.
    /// In case of invalidity an exception is thrown.
    /// </summary>
    public void Check ()
    {
      int i, j, k;
      bool b;

      App app = App.Instance;
      Ressources r = app.Ressources;
      
      // ----------------------------------------------------
      // Parameter section
      
      // Check, whether the Span conc's (Parameter section) and the substance windows (Windows section)
      // are consistent or not
      // (Means, that the SPAN<x> rows of the Parameter section must be associated to substance definition
      //  rows of the Windows section)
      int iMaxIdx = this.NumWindows;
      for (i=iMaxIdx; i < MAX_SCRIPT_WINDOWS; i++)
      {
        if (this.Parameter.arfSpanConc[i] > 0)
          throw new ScrSpanConcException ();
      }
      
      // Check, whether the 'Automated span monitoring' elements (Parameter section) and the substance windows
      // (Windows section) are consistent or not
      // (Means, that the CTRLWND<x> rows of the Parameter section must be associated to substance definition
      //  rows of the Windows section)
      iMaxIdx = this.NumWindows;
      for (i=iMaxIdx; i < MAX_SCRIPT_WINDOWS; i++)
      {
        if (this.Parameter.arAutoSpanMon[i].Enabled)
          throw new ScrAutoSpanMonException ();
      }
      
      // Check, whether the script is not simultaneously both a meas. script and an 'Automated span monitoring' script
      // -> this is not allowed!
      for (i=0; i < this.NumWindows; i++)
      {
        if (this.Parameter.arAutoSpanMon[i].Enabled)  
          break;
      }
      if (i < this.NumWindows)
      {
        if (this.Parameter.byMeasScript != 0)
        {
          // Yes, it is -> error!
          throw new ScrMeasScriptASMScriptException ();
        }
      }
      
      // ----------------------------------------------------
      // Command section
    
      // Checks, whether the script command section is empty or not
      if (this.NumCommands == 0)
      {
        string sMsg = string.Format ("'{0}': {1}", 
          this.Name, 
          r.GetString ("ScrCmdSectionNotPresentException_Msg")    // "Scriptkommando-Sektion nicht vorhanden oder leer"
          );
        throw new ScrCmdSectionNotPresentException (sMsg);
      }
      
      // Checks, whether the execution times of the script commands are in ascending order or not
      for (i=0; i < this.NumCommands-1; i++)
      {
        if (this.Cmds[i].dwTime > this.Cmds[i+1].dwTime)
        {
          string sMsg = string.Format ("'{0}': {1}", 
            this.Name, 
            r.GetString ("ScrCmdTimeNotAscendingException_Msg")   // "Die Scriptkommandos sind nicht zeitlich aufsteigend angeordnet"
            );
          throw new ScrCmdTimeNotAscendingException (sMsg);
        }
      }
      
      // Checks, whether two script commands are identical (not allowed!) or not
      for (i=0; i < this.NumCommands-1; i++)
      {
        for (j=i+1; j < this.NumCommands; j++)
        {
          b = this.Cmds[i].dwTime == this.Cmds[j].dwTime;
          if (b)
          {
            b = this.Cmds[i].szCommand.Trim().ToUpper() == this.Cmds[j].szCommand.Trim().ToUpper();
            if (b)
            {
              b = this.Cmds[i].wParNumber == this.Cmds[j].wParNumber;
              if (b)
              {
                for (k=0; k < this.Cmds[i].wParNumber; k++)
                {
                  b &= this.Cmds[i].ardwParameter[k] == this.Cmds[j].ardwParameter[k];
                }
                if (b)
                {
                  string sCmdLine = ScriptCollection.FormatItemString (this.Cmds[i].ToString ());
                  string sMsg = string.Format ("'{0}': {1} ({2})", 
                    this.Name, 
                    r.GetString ("ScrCmdEqualityException_Msg"),   // "Identische Scriptkommandos vorhanden"
                    sCmdLine
                    );
                  throw new ScrCmdEqualityException (sMsg);
                }
              }
            }
          }
        }
      }

      // ----------------------------------------------------
      // Windows section
      
      // Checks, whether a substance name is unique or not
      for (i=0; i < this.NumWindows-1; i++)
      {
        string sName1 = this.Windows[i].szSubstance;
        for (j=i+1; j < this.NumWindows; j++)
        {
          string sName2 = this.Windows[j].szSubstance;
          if (sName1.ToUpper() == sName2.ToUpper())
          {
            string sMsg = string.Format ("'{0}': {1}\r\n({2})", 
              this.Name, 
              r.GetString ("ScrWndNameAmbiguousException_Msg"),  // "Substanznamen nicht eindeutig"
              sName2
              );
            throw new ScrWndNameAmbiguousException (sMsg);
          }
        }
      }

      // ----------------------------------------------------
      // Miscellaneous
    
      // Check, whether a script with summary signal detection contains 'MAX_SCRIPT_WINDOWS' 
      // substance windows or not
      if (ConsiderSumSignal_NumWindows ())
      {
        // Yes, it does:
        // Error message
        string sFmt = r.GetString ("ScrSumSignalNumWindowsException_Msg");  // "Scripte mit Summensignal-Ermittlung d�rfen max. {0} Substanzfenster beinhalten"
        string sSubMsg = string.Format (sFmt, Script.MAX_SCRIPT_WINDOWS - 1);
        string sMsg = string.Format ("'{0}': {1}", this.Name, sSubMsg );
        throw new ScrSumSignalNumWindowsException (sMsg);
      }
      // Check, whether a script with summary signal detection doesn't contain a summary signal identifier
      // paramter row or not
      if (ConsiderSumSignal_TotalIdentifier ())
      {
        // Yes, a script with summary signal detection doesn't contain a summary signal identifier:
        // Error message
        string sSubMsg = r.GetString ("ScrSumSignalTotalIdentifierException_Msg");  // "Scripte mit Summensignal-Ermittlung m�ssen einen 'Total'-Parameter beinhalten"
        string sMsg = string.Format ("'{0}': {1}", this.Name, sSubMsg );
        throw new ScrSumSignalTotalIdentifierException (sMsg);
      }
 
      // Check, whether a script with a TotalVOC window has the static scan offset correction enabled (this is not recommended) or not
      if (ConsiderTotalVOC_OffsetCorr ())
      {
        // Yes, a script with a TotalVOC window has the static scan offset correction enabled:
        // Error message
        string sSubMsg = r.GetString ("ScrTotalVOCOffsetCorrException_Msg");  // "Scripte mit TotalVOC-Fenster sollten die statische Scanoffset-Korrektur nicht aktivieren"
        string sMsg = string.Format ("'{0}': {1}", this.Name, sSubMsg);
        throw new ScrTotalVOCOffsetCorrException (sMsg);
      }
    }

    /// <summary>
    /// Checks, whether a script with summary signal detection contains 'MAX_SCRIPT_WINDOWS'
    /// substance windows or not.
    /// </summary>
    /// <returns>True, if a script with summary signal detection contains 'MAX_SCRIPT_WINDOWS'
    /// substance windows; False otherwise.
    /// </returns>
    bool ConsiderSumSignal_NumWindows ()
    {
      int i;
      // Init. ret. value
      bool bRet=false;
      // Check: Does the script contain a summary signal detection?
      for (i=0; i < this.NumCommands ;i++)
      {
        ScriptEvent ev = this.Cmds[i];
        if (ev.szCommand == "SUMSIGNAL")
          break;
      }
      if (i < this.NumCommands)
      {
        // Yes:
        // Check: Does the script contain 'MAX_SCRIPT_WINDOWS' substance windows?
        if (this.NumWindows == Script.MAX_SCRIPT_WINDOWS)
          // Yes
          bRet = true;
      }
      // ret.
      return bRet;
    }
    
    /// <summary>
    /// Checks, whether a script with summary signal detection doesn't contain a summary signal identifier
    /// paramter row or not.
    /// </summary>
    /// <returns>True, if a script with summary signal detection doesn't contain a summary signal identifier
    /// paramter row; False otherwise.
    /// </returns>
    /// <remarks>
    /// The check must be performed acc'ing to the following scheme:
    /// 
    /// SUMSIGNAL          |        vorhanden           |     nicht vorhanden
    /// -------------------|                            |
    /// 'Total' parameter  |                            |
    /// -------------------|----------------------------|----------------------
    /// vorhanden          |        OK                  |     (redundant)
    /// -------------------|----------------------------|--------------------
    /// nicht              |        Error: Bezeichner   |     OK
    /// vorhanden          |               erford.      |
    /// -------------------|----------------------------|----------------------
    /// 
    /// </remarks>
    bool ConsiderSumSignal_TotalIdentifier ()
    {
      int i;
      // Init. ret. value
      bool bRet=false;
      // Check: Does the script contain a summary signal detection?
      for (i=0; i < this.NumCommands ;i++)
      {
        ScriptEvent ev = this.Cmds[i];
        if (ev.szCommand == "SUMSIGNAL")
          break;
      }
      if (i < this.NumCommands)
      {
        // Yes:
        // Check: Does the script contain a summary signal identifier?
        if (this.Parameter.sTotal.Length == 0)
          // No
          bRet = true;
      }
      // ret.
      return bRet;
    }

    /// <summary>
    /// Checks, whether a script with a TotalVOC window has the static scan offset correction enabled (this is erroneous) or not.
    /// </summary>
    /// <returns>True, if a script with a TotalVOC window has the static scan offset correction enabled; False otherwise.</returns>
    bool ConsiderTotalVOC_OffsetCorr ()
    {
      int i;
      // Init. ret. value
      bool bRet = false;
      // Check: Does the script contain a TotalVOC window?
      for (i = 0; i < this.NumWindows; i++)
      {
        ScriptWindow wnd = this.Windows[i];
        if (wnd.dwBegin == 0 && wnd.dwWidth == 0)
          break;
      }
      if (i < this.NumWindows)
      {
        // Yes:
        // Check: Should a static scan offset correction be performed?
        if (this.Parameter.byOffsetCorr != 0)
          // Yes -> Error!
          bRet = true;
      }
      // ret.
      return bRet;
    }

    #endregion methods

  }  
  

  /// <summary>
  /// Class ScriptCollection:
  /// Script collection / file handling
  /// </summary>
  public class ScriptCollection 
  {
    #region constants & enums

    /// <summary>
    /// Max. number of scripts per configuration file
    /// </summary>
    public const int MAX_SCRIPT_NUMBER = 10;
 
    /// <summary>
    /// Top line: The device no. key
    /// </summary>
    public const string DevNo = "DevNo";
    /// <summary>
    /// Top line: The User ID
    /// </summary>
    public const string UserID = "UserID";

    /// <summary>
    /// The script parameter key
    /// </summary>
    public const string Parameter = "Parameter";
    /// <summary>
    /// The script windows key
    /// </summary>
    public const string Windows = "Windows";
    /// <summary>
    /// The script commands key
    /// </summary>
    public const string Cmds = "Script";

    /// <summary>
    /// The meas. cycle key (concerns: cycle time in ms)
    /// </summary>
    public const string MeasCycle = "MeasCycle";
    /// <summary>
    /// The signal processing interval key (concerns: signal processing interval in pts)
    /// </summary>
    public const string SigProcInt = "SigProcInt";
    /// <summary>
    /// The PC communication interval key (concerns: PC communication interval in pts)
    /// </summary>
    public const string PCCommInt = "PCCommInt";

    /// <summary>
    /// The meas. script ident. key (concerns: meas. script ident.)
    /// </summary>
    public const string MeasScript = "MeasScript";
    
    /// <summary>
    /// The static scan offset correction key (concerns: static scan offset correction)
    /// </summary>
    public const string OffsetCorr = "OffsetCorr";
    
    /// <summary>
    /// The script ID key (concerns: script ID)
    /// </summary>
    public const string ScriptID = "ScriptID";
    /// <summary>
    /// The Span concentrations key (concerns: Span concentrations)
    /// </summary>
    public const string SpanConc = "Span";
    /// <summary>
    /// The 'Automated span monitoring' key (concerns: Automated span monitoring)
    /// </summary>
    public const string AutoSpanMon = "CtrlWnd";

    /// <summary>
    /// The SumSignal Identifier key
    /// </summary>
    public const string Total = "Total";
    
    // Constants used ind script file writing
    const string NEWLINE = "\r\n";                  // NL
    const string szNEWGROUP = 
      "****************************************" + 
      "****************************************" +
      NEWLINE;                                      // group separator
    const char chTopBegin     = '{';                // Begin of a top line
    const char chTopEnd       = '}';                // End of a top line
    const char chGroupBegin   = '<';                // Begin of a group name
    const char chGroupEnd     = '>';                // End of a group name
    const char chSectionBegin = '[';                // Begin of a section name
    const char chSectionEnd   = ']';                // End of a section name
    const char chComment      = ';';                // Begin of a comment line

    // Line modi specifier used ind script file writing
    enum typeModi { 
      typeTop,          // top spec. (f.i. "{DevNo = PID0815}")
      typeGroup,        // group spec. (f.i. "<myScript>") 
      typeSection,      // section spec. (f.i. "[Parameter]")
      typeItem,         // item spec. (f.i. "MeasCycle = 600")
      typeComment,      // comment spec. (f.i. ";this is a comment")
      typeEmptyRow      // spec. for empty (non-usable) lines
    };

    #endregion constants & enums

    #region constructors
    
    /// <summary>
    /// Constructor
    /// </summary>
    public ScriptCollection()
    {
      // Create the scripts
      for (int i=0; i < MAX_SCRIPT_NUMBER; i++)
        Scripts[i] = new Script ();
      // Reset the script collection
      Reset ();
    }
    
    #endregion constructors
  
    #region members
    
    /// <summary>
    /// The script file name
    /// </summary>
    string _Name = string.Empty;

    /// <summary>
    /// The script array
    /// </summary>
    public Script[] Scripts = new Script[MAX_SCRIPT_NUMBER];
  
    /// <summary>
    /// The # of scripts
    /// </summary>
    public int NumScripts = 0;

    /// <summary>
    /// The comments
    /// </summary>
    public StringArray arsComment = new StringArray ();

    /// <summary>
    /// Top line: The device no. value
    /// </summary>
    public string sDevNo = string.Empty;

    /// <summary>
    /// Top line: The User ID
    /// </summary>
    public string sUserID = "";
    
    /// <summary>
    /// Indicates, that the number of scripts exceeds the permitted max. number
    /// </summary>
    public bool MaxScriptNoExceeded = false;
    
    #endregion members
  
    #region methods
    
    /// <summary>
    /// Resets the instance
    /// </summary>
    public void Reset ()
    {
      _Name = string.Empty;
      NumScripts = 0;
      arsComment.Clear ();
      sDevNo = string.Empty;
      MaxScriptNoExceeded = false;
    }
    
    /// <summary>
    /// Finds a script in the script collection.
    /// </summary>
    /// <param name="sScrName">The name of the script</param>
    /// <returns>The script index (-1, if the script could not be found)</returns>
    public int FindScript (string sScrName)
    {
      // Find the script with the given name
      for (int j=0; j < this.NumScripts; j++)
      {
        Script scr = this.Scripts[j];
        if (scr.Name.ToUpper() == sScrName.ToUpper())
        {
          // We found it
          return j;
        }
      }
      // The script with the given name could not be found
      return -1;
    }
    
    /// <summary>
    /// Removes a script from the script collection.
    /// </summary>
    /// <param name="sScrName">The name of the script</param>
    public void RemoveScript (string sScrName)
    {
      int j;

      // Find the script with the given name
      for (j=0; j < this.NumScripts; j++)
      {
        Script scr = this.Scripts[j];
        if (scr.Name == sScrName)
        {
          // We found it
          break;
        }
      }
      // If we found it ...
      if (j < this.NumScripts)
      {
        // ... remove it
        int js = j;
        for (j=js+1; j < this.NumScripts; j++)
        {
          Script scrSource = this.Scripts[j];
          Script scrDest = this.Scripts[j-1];
          scrSource.CopyTo (scrDest);
        }
        // Update # of scripts
        this.NumScripts--;
      }
    }
    
    /// <summary>
    /// Adds a script to the script collection.
    /// </summary>
    /// <param name="scr">The script</param>
    public void AddScript (Script scr)
    {
      // Adding permitted?
      if (this.NumScripts >= MAX_SCRIPT_NUMBER)
        return; // No
      // Yes:
      // Add it
      Script scrDest =  this.Scripts[this.NumScripts];
      scr.CopyTo (scrDest);
      // Update # of scripts
      this.NumScripts++;
    }
    
    /// <summary>
    /// Updates a script within the script collection.
    /// </summary>
    /// <param name="idx">The update position within the script collection</param>
    /// <param name="scr">The script</param>
    public void UpdateScript (int idx, Script scr)
    {
      // Updating permitted?
      if (idx < 0 || idx > this.NumScripts-1)
        return; // No
      // Yes:
      // Update it
      Script scrDest =  this.Scripts[idx];
      scr.CopyTo (scrDest);
    }

    /// <summary>
    /// Checks, whether a script collection is valid.
    /// In case of invalidity an exception is thrown.
    /// </summary>
    public void Check ()
    {
      // Check, whether the script collection is not empty
      if (this.NumScripts == 0)
        throw new ScrCollectionEmptyException ();

      // Check, whether the script ID's are non-ambiguous 
      // (Means: Script IDs, that are > 0, must be non-ambiguous )
      for (int i=0; i < this.NumScripts-1; i++)
      {
        Script scr = this.Scripts[i];
        UInt32 id = scr.Parameter.dwScriptID;
        if (id == 0) continue;                          // script IDs = 0 are out of interest
        for (int j=i+1; j < this.NumScripts; j++)
        {
          Script scr1 = this.Scripts[j];
          UInt32 id1 = scr1.Parameter.dwScriptID;
          if (id1 == 0) continue;                       // script IDs = 0 are out of interest
          // Check: Ambiguous IDs?          
          if (id == id1)
          {
            // Yes:
            // Error: The 2 IDs are equal -> ambiguity
            throw new ScrCollectionAmbiguousIDException ();
          }
        }
      }

      // Check, whether the script names are different from each other 
      for (int i=0; i < this.NumScripts-1; i++)
      {
        Script scr = this.Scripts[i];
        for (int j=i+1; j < this.NumScripts; j++)
        {
          Script scr1 = this.Scripts[j];
          // Check: Equal names?          
          if (scr.Name.ToUpper () == scr1.Name.ToUpper ())
          {
            // Yes:
            // Error: The 2 names are equal
            throw new ScrCollectionEqualScrNamesException ();
          }
        }
      }

    }
    
    /// <summary>
    /// Loads a script file. 
    /// </summary>
    /// <param name="sFilename">The full script file path</param>
    /// <param name="sInfo">The info string (out)</param>
    public void Load (string sFilename, out string sInfo)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // ===============================
      // Part 1: Loading
      
      StreamReader reader = null;
      string sLine = string.Empty, sGrpName, sSecName;
      int nErrorLine = 0;
    
      Script scr = null;
      ScriptWindow wnd;
      ScriptEvent cmd;

      // Init.
      this.Reset ();            // Reset the script collection
      sGrpName = string.Empty;  // Empty script name string
      sSecName = string.Empty;  // Empty section name string
      // Load
      try
      {
        reader = File.OpenText ( sFilename );
        while ( (sLine = reader.ReadLine ()) != null )
        {
          sLine=sLine.Trim ();
          nErrorLine++;
          int nType = IdentifyString ( sLine );
          // CD: Line mode
          switch ( nType )
          {
            case (int) typeModi.typeTop :                   // Top: 
              // Get the top line
              string sTopLine = ExtractString ( nType, sLine ); 
              // Extract it
              string sTopLineKey = string.Empty, sTopLineVal = string.Empty;
              ExtractItemString (sTopLine, ref sTopLineKey, ref sTopLineVal);
              // Assign   
              switch (sTopLineKey)
              {
                case DevNo:                                 // Device No.
                  sDevNo = sTopLineVal; 
                  break;
                case UserID:                                // User ID
                  sUserID = sTopLineVal;
                  break;
              }
              break;
            
            case (int) typeModi.typeGroup :                 // Group:
              sGrpName = ExtractString ( nType, sLine );    // Get the group (= script) name
              sSecName = string.Empty;                      // Empty section name
              // CD: Script collection full?
              if (NumScripts < MAX_SCRIPT_NUMBER)           
              {
                // No:
                // Check, whether the (previously filled) script is valid
                if (null != scr) scr.Check (); 
                // Initiate the script to be filled currently
                scr = Scripts[NumScripts++];                // Assign the current script
                scr.Reset ();                               // Reset it
                scr.Name = sGrpName;                        // Assign the script name
              }
              else
              {
                // Yes
                this.MaxScriptNoExceeded = true;            // Indicate, that the number of scripts exceeds the permitted max. number
              }
              break;
                
            case (int) typeModi.typeSection :               // Section:
              sSecName = ExtractString ( nType, sLine );    // Get the section name
              // Check: Valid section name?
              if (!((sSecName == Parameter) || (sSecName == Windows) || (sSecName == Cmds)))
                throw new ScrSectionNameInvalidException ();// No
              break;
    
            case (int) typeModi.typeComment :               // Comment:
              string sCmnt = ExtractString ( nType, sLine );// Get the comment string 
              // CD: Where we are on reading the script file? 
              StringArray sa = null;
              if      (sGrpName.Length == 0)  sa = this.arsComment;   // At the beginning of the file: Add comment at script collection level
              else if (sSecName.Length == 0)  sa = scr.arsComment;    // At the beginning of a group (script): Add comment at script level
              else if (sSecName == Parameter) sa = scr.arsParComment; // At the beginning of a script section: Add comment at script section level: Parameter section
              else if (sSecName == Windows)   sa = scr.arsWndComment; // At the beginning of a script section: Add comment at script section level: Windows section
              else if (sSecName == Cmds)      sa = scr.arsCmdComment; // At the beginning of a script section: Add comment at script section level: Command section
              if (null != sa)
                sa.Add (sCmnt); // add comment
              break;
    
            case (int) typeModi.typeItem :                  // Item:
            switch (sSecName)
            {
              case Parameter:                               // Parameter item
                scr.Parameter.Load (sLine);
                // Check, whether the script parameters are valid
                scr.Parameter.Check ();
                break;

              case Windows:                                 // Windows item
                if (scr.NumWindows < Script.MAX_SCRIPT_WINDOWS)
                {
                  wnd = scr.Windows[scr.NumWindows++];
                  wnd.Load (sLine);
                  // Check, whether the script window is valid
                  wnd.Check ();
                }
                else
                {
                  scr.MaxWindowNoExceeded = true;           // Indicate, that the number of script windows exceeds the permitted max. number
                }
                break;

              case Cmds:                                    // Command item
                if (scr.NumCommands < Script.MAX_SCRIPT_EVENTS)
                {
                  cmd = scr.Cmds[scr.NumCommands++];
                  cmd.Load (sLine);
                  // Check, whether the script command is valid
                  cmd.Check ();
                }
                else
                {
                  scr.MaxCmdNoExceeded = true;              // Indicate, that the number of scripts commands exceeds the permitted max. number
                }
                break;

            }
              break;
    
          } // E - switch ( nType )
          // Finish loading, if the permitted max. number of scripts is reached
          if (this.MaxScriptNoExceeded) break;
        } // E - while  
        
        // Check, whether the (lastly filled) script is valid
        if (null != scr) scr.Check (); 
        // Checks at ScriptCollection level
        this.Check ();
        
        // Memorize the script file path
        _Name = sFilename;                                    
      }
      catch (System.Exception exc)
      {
        // Exception subgroups:
        //    user exceptions at script or script collection level:
        //    If these exceptions are thrown, the file name & a note are output 
        // Script level
        bool b1 = exc is ScrCmdSectionNotPresentException
          || exc is ScrCmdTimeNotAscendingException
          || exc is ScrCmdEqualityException
          || exc is ScrWndNameAmbiguousException
          || exc is ScrSpanConcException
          || exc is ScrAutoSpanMonException
          || exc is ScrSumSignalNumWindowsException
          || exc is ScrSumSignalTotalIdentifierException
          || exc is ScrMeasScriptASMScriptException
          || exc is ScrTotalVOCOffsetCorrException
          // Script collection level
          || exc is ScrCollectionAmbiguousIDException
          || exc is ScrCollectionEqualScrNamesException
          || exc is ScrCollectionEmptyException;
        //    user exceptions at script section level:      
        //    If these exceptions are thrown, the file name, a line info & a note are output 
        bool b2 = exc is ScrCmdNameInvalidException
          || exc is ScrCmdParameterInvalidException
          || exc is ScrParameterInvalidException
          || exc is ScrSectionNameInvalidException
          || exc is ScrWndNameInvalidException
          || exc is ScrWndInconsistencyException
          || exc is ScrWndMinNoCalibPointsException
          || exc is ScrMsgStringlengthException
          || exc is ScrWndTotalVOCException;
        //    all other exceptions:
        //    If these exceptions are thrown, the file name & a line info are output 
        bool b3 = !(b1 || b2);        
        
        // Error spec.: File name
        string fmt = r.GetString ("cScriptCollection_Load_Err_Basic");  // "Error on reading a script file: \r\n{0}";
        string s = string.Format (fmt, sFilename);
        // Error spec.: Line #, Line contents
        if (b2 || b3)
        {
          s += "\r\n";                                                  // empty line
          fmt = r.GetString ("cScriptCollection_Load_Err_Line");        // "\r\nLineno: {0}\r\nLine: {1}";
          s += string.Format (fmt, nErrorLine, sLine);
        }
        // Error spec.: Note
        if (b1 || b2)
        {
          s += "\r\n";                                                  // empty line
          fmt = r.GetString ("cScriptCollection_Load_Err_Note");        // "\r\nNote: {0}";
          s += string.Format (fmt, exc.Message);
        }
        // throw
        throw new ApplicationException (s);
      }
      finally 
      {
        if ( null != reader ) 
          reader.Close ();
      }
    
      // ===============================
      // Part 2: Info string building
      //
      // Builds a string with the information, whether the number of scripts in the script file exceeds the permitted max. number
      // OR the number of windows in a script of the script file exceeds the permitted max. number
      // OR the number of commands in a script of the script file exceeds the permitted max. number.
    
      int i, j;

      // Init.
      sInfo = "";
      // 1. ScriptCollection: # of scripts
      if (this.MaxScriptNoExceeded)
      {
        sInfo += "\r\n";
        string sFmt = r.GetString ("cScriptCollection_Load_Inf_ScrNo");     //"Die Scriptanzahl �bersteigt das zul�ssige Maximum ({0}). Nur die ersten {0} Scripte werden geladen.";
        sInfo += string.Format (sFmt, ScriptCollection.MAX_SCRIPT_NUMBER);
      }
      // 2. Scripte: # of windows 
      for (i=0; i < this.NumScripts; i++)
      {
        Script sc = this.Scripts[i];
        if (sc.MaxWindowNoExceeded) break;
      }
      if (i < this.NumScripts)
      {
        sInfo += "\r\n";    // NL f�r vorhergehende Zeile
        sInfo += "\r\n";    // Empty line
        sInfo += r.GetString ("cScriptCollection_Load_Inf_Script");         // "Scripte"
        sInfo += " (";
        for (j=0, i=0; i < this.NumScripts; i++)
        {
          Script sc = this.Scripts[i];
          if (sc.MaxWindowNoExceeded) 
          {
            if (j > 0) sInfo += ", ";
            sInfo += string.Format ("'{0}'", sc.Name);
            j++;
          }
        }
        sInfo += "):";
        sInfo += "\r\n";
        string sFmt = r.GetString ("cScriptCollection_Load_Inf_ScrWndNo");  //"Die Fensteranzahl �bersteigt das zul�ssige Maximum ({0}). Nur die ersten {0} Fenster werden geladen.";
        sInfo += string.Format (sFmt, Script.MAX_SCRIPT_WINDOWS);
      }
      // 3. Scripte: # of commands 
      for (i=0; i < this.NumScripts; i++)
      {
        Script sc = this.Scripts[i];
        if (sc.MaxCmdNoExceeded) break;
      }
      if (i < this.NumScripts)
      {
        sInfo += "\r\n";    // NL f�r vorhergehende Zeile
        sInfo += "\r\n";    // Empty line
        sInfo += r.GetString ("cScriptCollection_Load_Inf_Script");         // "Scripte"
        sInfo += " (";
        for (j=0, i=0; i < this.NumScripts; i++)
        {
          Script sc = this.Scripts[i];
          if (sc.MaxCmdNoExceeded) 
          {
            if (j > 0) sInfo += ", ";
            sInfo += string.Format ("'{0}'", sc.Name);
            j++;
          }
        }
        sInfo += "):";
        sInfo += "\r\n";
        string sFmt = r.GetString ("cScriptCollection_Load_Inf_ScrCmdNo");  //"Die Befehlsanzahl �bersteigt das zul�ssige Maximum ({0}). Nur die ersten {0} Befehle werden geladen.";
        sInfo += string.Format (sFmt, Script.MAX_SCRIPT_EVENTS);
      }
    }

    /// <summary>
    /// Writes a script file 
    /// </summary>
    /// <param name="sFilename">The full script file path</param>
    public void Write (string sFilename)
    {
      StreamWriter writer = null;
      string sLine;
      
      // Write
      try
      {
        writer = File.CreateText (sFilename);
        // Write: Comment (at script collection level)
        for (int i=0; i < this.arsComment.Count; i++)
        {
          sLine = FormatString ((int)typeModi.typeComment, this.arsComment[i]);
          writer.WriteLine (sLine);
        }
        // Top
        // Notes:
        //  Store Top line(s) only in case that value was not empty
        if (this.sDevNo.Length > 0)
        {
          sLine = string.Format ("{0} = {1}", DevNo, this.sDevNo);
          sLine = FormatString ((int)typeModi.typeTop, sLine);
          writer.WriteLine (sLine);
        }
        if (this.sUserID.Length > 0)
        {
          sLine = string.Format("{0} = {1}", UserID, this.sUserID);
          sLine = FormatString((int)typeModi.typeTop, sLine);
          writer.WriteLine(sLine);
        }
        
        // Loop over all scripts in the collection
        for (int i=0; i < this.NumScripts; i++)
        {
          // The current script
          Script scr = Scripts[i];
          scr.Check ();                                     // Check script for validity
        
          // Empty line
          writer.WriteLine ();          

          // Group (= script)
          writer.Write (szNEWGROUP);                        // Group (script) separator
          sLine = FormatString ((int)typeModi.typeGroup, scr.Name);
          writer.WriteLine (sLine);                         // Script name
          for (int j=0; j < scr.arsComment.Count; j++)      // Comment (at script level)
          {
            sLine = FormatString ((int)typeModi.typeComment, scr.arsComment[j]);
            writer.WriteLine (sLine);
          }
          
          // Empty line
          writer.WriteLine ();          
        
          // Section: Parameter
          sLine = FormatString ((int)typeModi.typeSection, Parameter);
          writer.WriteLine (sLine);                         // Section name
          for (int j=0; j < scr.arsParComment.Count; j++)   // Comment (at parameter section level)
          {
            sLine = FormatString ((int)typeModi.typeComment, scr.arsParComment[j]);
            writer.WriteLine (sLine);
          }
          // Parameter
          // Notes:
          //  0. Store meas. script ident. only in case > 0.
          //  1. Store Static scan offset correction only in case > 0.
          //  2. Store Script ID only in case > 0.
          //  3. Store Span conc's only in case > 0.
          //  4. Store 'Automated span monitoring' members only in case > 0.
          //    Meas. cycle time
          sLine = string.Format ("{0} = {1}", MeasCycle, scr.Parameter.dwCycleTime);
          writer.WriteLine (sLine);
          //    Signal processing interval
          sLine = string.Format ("{0} = {1}", SigProcInt, scr.Parameter.dwSigProcInt);
          writer.WriteLine (sLine);
          //    PC comm. interval
          sLine = string.Format ("{0} = {1}", PCCommInt, scr.Parameter.dwPCCommInt);
          writer.WriteLine (sLine);
          //    Meas. script ident.
          if (scr.Parameter.byMeasScript != 0)
          {
            sLine = string.Format ("{0} = {1}", MeasScript, scr.Parameter.byMeasScript);
            writer.WriteLine (sLine);
          }
          //    Static scan offset correction
          if (scr.Parameter.byOffsetCorr != 0)
          {
            sLine = string.Format ("{0} = {1}", OffsetCorr, scr.Parameter.byOffsetCorr);
            writer.WriteLine (sLine);
          }
          //    Script ID
          if (scr.Parameter.dwScriptID != 0)
          {
            sLine = string.Format ("{0} = {1}", ScriptID, scr.Parameter.dwScriptID);
            writer.WriteLine (sLine);
          }
          //    Span concentrations
          System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
          for (int j=0; j < Script.MAX_SCRIPT_WINDOWS; j++)
          {
            if (scr.Parameter.arfSpanConc[j] > 0)
            {
              sLine = string.Format ("{0}{1} = {2}", SpanConc, j, scr.Parameter.arfSpanConc[j].ToString (nfi));
              writer.WriteLine (sLine);
            }
          }
          //    Automated span monitoring
          for (int j=0; j < Script.MAX_SCRIPT_WINDOWS; j++)
          {
            if (scr.Parameter.arAutoSpanMon[j].Enabled)
            {
              sLine = string.Format ("{0}{1} = {2},{3},{4}", 
                AutoSpanMon, j, 
                scr.Parameter.arAutoSpanMon[j].LowLimit.ToString (nfi),
                scr.Parameter.arAutoSpanMon[j].UppLimit.ToString (nfi),
                scr.Parameter.arAutoSpanMon[j].NoErrMeas.ToString ()
                );
              writer.WriteLine (sLine);
            }
          }
          //    Summary signal identifier
          if (scr.Parameter.sTotal.Length > 0)
          {
            sLine = string.Format ("{0} = {1}", Total, scr.Parameter.sTotal);
            writer.WriteLine (sLine);
          }
          
          // Empty line
          writer.WriteLine ();          

          // Section: Windows
          sLine = FormatString ((int)typeModi.typeSection, Windows);
          writer.WriteLine (sLine);                         // Section name
          for (int j=0; j < scr.arsWndComment.Count; j++)   // Comment (at Windows section level)
          {
            sLine = FormatString ((int)typeModi.typeComment, scr.arsWndComment[j]);
            writer.WriteLine (sLine);
          }
          // Windows
          for (int j=0; j < scr.NumWindows ;j++)
          {
            sLine = FormatItemString (scr.Windows[j].ToString ());
            if (sLine.Length > 0) writer.WriteLine (sLine);            
          }

          // Empty line
          writer.WriteLine ();          

          // Section: Commands
          sLine = FormatString ((int)typeModi.typeSection, Cmds);
          writer.WriteLine (sLine);                         // Section name
          for (int j=0; j < scr.arsCmdComment.Count; j++)   // Comment (at Cmds section level)
          {
            sLine = FormatString ((int)typeModi.typeComment, scr.arsCmdComment[j]);
            writer.WriteLine (sLine);
          }
          // Cmds
          for (int j=0; j < scr.NumCommands ;j++)
          {
            sLine = FormatItemString (scr.Cmds[j].ToString ());
            if (sLine.Length > 0) writer.WriteLine (sLine);            
          }
        }
        // Memorize the script file path
        _Name = sFilename;                                    
      }
      catch ( System.Exception exc )
      {
        App app = App.Instance;
        Ressources r = app.Ressources;
        string fmt = r.GetString ("cScriptCollection_Save_Err");  // "Error on writing a script file: \r\nFile: {0},\r\nError: {1}.";
        string s = string.Format ( fmt, sFilename, exc.Message);
        throw new ApplicationException (s);
      }
      finally
      {
        if ( null != writer ) 
          writer.Close ();
      }
    }

    /// <summary>
    /// Identifies the type of a line ( string ) of a configuration file
    /// </summary>
    /// <param name="szString">The line to be identified</param>
    /// <returns>The type of the line</returns>
    int IdentifyString ( string szString )
    {
      string s = szString;
  
      if ( s.Length == 0 ) 
        return (int) typeModi.typeEmptyRow;
  
      switch ( s[0] )
      {
        case chTopBegin       :  return (int) typeModi.typeTop;    
        case chGroupBegin     :  return (int) typeModi.typeGroup;    
        case chSectionBegin   :  return (int) typeModi.typeSection;
        case chComment        :  return (int) typeModi.typeComment;
      }
  
      if ( -1 != s.IndexOf ( '=' ) ) 
        return (int) typeModi.typeItem;
  
      return (int) typeModi.typeEmptyRow;
    }
   
    /// <summary>
    /// Extracts the contents from a string according to the type of the string
    /// </summary>
    /// <param name="nType">The type</param>
    /// <param name="sInput">The string</param>
    /// <returns>The contents string</returns>
    string ExtractString ( int nType, string sInput )
    {
      int nPosBeg, nPosEnd;
  
      switch ( nType )
      {
        case (int) typeModi.typeTop:  
          nPosBeg = sInput.IndexOf ( chTopBegin ); 
          nPosEnd = sInput.IndexOf ( chTopEnd ); 
          break;
        case (int) typeModi.typeGroup:  
          nPosBeg = sInput.IndexOf ( chGroupBegin ); 
          nPosEnd = sInput.IndexOf ( chGroupEnd ); 
          break;
        case (int) typeModi.typeSection :
          nPosBeg = sInput.IndexOf ( chSectionBegin ); 
          nPosEnd = sInput.IndexOf ( chSectionEnd ); 
          break;                                       
        case (int) typeModi.typeComment :
          nPosBeg = sInput.IndexOf ( chComment ); 
          nPosEnd = sInput.Length;
          break;
        default:
          Debug.Assert ( false );
          return string.Empty;
      }

      if ( 0 > nPosBeg ) 
      {
        Debug.WriteLine ( "Failed: ExtractString()\n" );
        throw new ApplicationException ();
      }

      nPosBeg++;

      if ( nPosEnd <= nPosBeg  )
        return string.Empty;

      return sInput.Substring ( nPosBeg, nPosEnd-nPosBeg ).Trim ();
    }

    /// <summary>
    /// Formats a string according to a given type and a given contents string
    /// </summary>
    /// <param name="nType">The type</param>
    /// <param name="sInput">The contents string</param>
    /// <returns>The formatted string</returns>
    string FormatString ( int nType, string sInput )
    {
      string sOutput = string.Empty;
  
      switch ( nType )
      {
        case (int) typeModi.typeTop :
          sOutput = chTopBegin + sInput + chTopEnd;
          break;
        case (int) typeModi.typeGroup :
          sOutput = chGroupBegin + sInput + chGroupEnd;
          break;
        case (int) typeModi.typeSection :
          sOutput = chSectionBegin + sInput + chSectionEnd;
          break;
        case (int) typeModi.typeComment :
          sOutput = chComment + sInput;
          break;
        default:
          Debug.Assert ( false );
          break;
      }

      return sOutput;
    }

    /// <summary>
    /// Extracts the Key and Value parts from a Key-Value string
    /// </summary>
    /// <param name="sInput">The Key-Value string</param>
    /// <param name="sKey">The extracted Key part</param>
    /// <param name="sVal">The extracted Value part</param>
    public static void ExtractItemString ( string sInput, ref string sKey, ref string sVal )
    {
      int nPos=sInput.IndexOf ('=');
      if (nPos < 0)
      {
        Debug.WriteLine ( "Failed: ExtractItemString()\n" );
        throw new ApplicationException ();
      }
      sKey=sInput.Substring(0, nPos).Trim();
      sVal=sInput.Substring(nPos+1).Trim();
    }
    
    /// <summary>
    /// Formats an input string to a Key-Value string
    /// </summary>
    /// <param name="sInput">The input string</param>
    /// <returns>The Key-Value string</returns>
    /// <remarks>The input string elements are separated by commas, 
    /// the 1st element is considered as the key element</remarks>
    public static string FormatItemString ( string sInput )
    {
      string sLine = string.Empty;
    
      int nPos = sInput.IndexOf(',');
      if (nPos != -1)
      {
        sLine = sInput.Substring (0, nPos);
        sLine += " = ";
        sLine += sInput.Substring (nPos+1);
      }
      return sLine;
    }

    #endregion methods

  }

}
