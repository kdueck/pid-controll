using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;

namespace App
{
	/// <summary>
	/// Class Doc:
	/// The document
  /// </summary>
  public class Doc
  {
    #region constants

    /// <summary>
    /// The max. number of Scan buffers available
    /// </summary>
    public const int MAX_SCANBUFFERS = 2;
    
    /// <summary>
    /// The max. number of Scan results permitted
    /// </summary>
    public const int MAX_SCAN_RESULT = 16;

    /// <summary>
    /// The number of scan elements
    /// </summary>
    public const int NSCANDATA = 2048;

    /// <summary>
    /// The resolution of the decvice ADC (2^15)
    /// </summary>
    public const int ADC_RESOLUTION = 32768;

    /// <summary>
    /// Max. number of clock-timed scripts
    /// (MUST coincide with the the corr'ing number defined in the device SW)
    /// </summary>
    public const int MAX_CLOCKTIMEDSCRIPTS = 6;
    
     /// <summary>
    /// Loading / Saving files encryption management: CryptMode enumeration
    /// </summary>
    public enum CryptMode
    {
      /// <summary>
      /// </summary>
      Encrypted,
      /// <summary>
      /// </summary>
      Decrypted,
    }
    
    #endregion // constants
   
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public Doc()
    {
      bRunning = false;                             // Indicator: No script in execution
      RecordingReset ();                            // Reset data recording members

      dwScanCount = 0;                              // Number of incoming DATA responses (scans)
      dwNoDataCount = 0;                            // Number of incoming NODATA responses
      for (int i=0; i < MAX_SCAN_RESULT; i++)
        nMarkedWindow[i] = false;                   // Indicate: No marked windows

      sDataFolder = System.IO.Path.GetTempPath ();  // The data folder (to be selected per Properties dialog)
      
      bResultAlarm = false;                         // Indicator: No substance alarm present

      sScriptFilename = "PIDTest.gsm";              // Full path of the script configuration file
    
      nPoints = 0;                                  // The # of scan points curr'ly transferred

      // Create: The scan array ( data )
      for ( int i=0; i < MAX_SCANBUFFERS ;i++) 
        arScanData[i] = new ScanData ();
    }

    #endregion // constructors

    #region members

    /// <summary>
    /// The Device program version
    /// </summary>
    /// <remarks>
    /// NOTE: 
    ///   This version string must coincide with the version string of the device SW currently installed.
    ///   If not, no connection via RS232 is made.
    /// </remarks>
    internal string[] VERSIONSTRING = new string[] { "05.00.00" }; // Permitted versions of device SW
    
    /// <summary>
    /// Sensor information
    /// </summary>
    public DeviceInfo DeviceInfo = new DeviceInfo ();

    /// <summary>
    /// Scan array ( data ):
    ///   Idx. 0: Raw scan (as received from the device)
    ///   Idx. 1: Smoothed scan (calculated after receipt of the raw scan)
    /// </summary>
    public ScanData[] arScanData = new ScanData[MAX_SCANBUFFERS];
    
    /// <summary>
    /// Scan information
    /// </summary>
    public ScanInfo ScanInfo = new ScanInfo ();

    /// <summary>
    /// Marked windows ( in [0, '(View.)m_nMaxScanResult'-1] )
    /// </summary>
    public bool[] nMarkedWindow = new bool[MAX_SCAN_RESULT];
    
    /// <summary>
    /// Scan result array ( max. '(View.)m_nMaxScanResult' elements )
    /// </summary>
    public ScanResultArray arScanResult = new ScanResultArray ();
    
    /// <summary>
    /// Parameter of the script currently running
    /// </summary>
    public ScanParams ScanParams = new ScanParams ();
    
    /// <summary>
    /// Number of incoming DATA responses (scans)
    /// </summary>
    public UInt32 dwScanCount = 0;
    /// <summary>
    /// Number of incoming NODATA responses
    /// </summary>
    public UInt32 dwNoDataCount = 0;

    /// <summary>
    /// Script collection
    /// </summary>
    public ScriptCollection arScript = new ScriptCollection ();

    /// <summary>
    /// Indicator: Is a script in execution (true) or not (false) 
    /// </summary>
    public bool bRunning = false;
    
    /// <summary>
    /// Indicator for 'Spectrum recording - Single scan': 
    /// Should a single scan (current spectrum) be recorded (true) or not (false)
    /// </summary>
    public bool bRecordSingleScan = false;
    /// <summary>
    /// Indicator for 'Spectrum recording - Scans continuously':
    /// Should the scans (spectra continuously) be recorded (true) or not (false)
    /// </summary>
    public bool bRecordScans = false;
    /// <summary>
    /// Indicator for 'Spectrum recording - Results continuously':
    /// Should the results (results continuously) be recorded (true) or not (false)
    /// </summary>
    public bool bRecordResults = false;
    
    /// <summary>
    /// Indicator: Is a substance alarm present (true) or not (false)
    /// </summary>
    public bool bResultAlarm = false;

    /// <summary>
    /// 'Current spectrum recording': Full path of the file where the storage takes place
    /// </summary>
    public string sRecordSingleScanFilename;
    /// <summary>
    /// Data folder, where the storage in case of 'Current spectrum recording' and
    /// 'Continuous spectra recording' takes place
    /// (Subfolder of the folder, specified by 'sDataFolder')
    /// </summary>
    public string sRecordScanFolder;
    /// <summary>
    /// 'Continuous measuring result recording': Full path of the file where the storage takes place
    /// </summary>
    public string sRecordResultFilename;

    /// <summary>
    /// Full path of the script configuration file
    /// </summary>
    public string sScriptFilename;

    /// <summary>
    /// String array, containing the names of the scripts installed currently on the PID device
    /// </summary>
    public StringArray arsDeviceScripts = new StringArray ();
    /// <summary>
    /// The name of the script currently selected
    /// </summary>
    public string sCurrentScript = string.Empty;
   
    /// <summary>
    /// The device version string
    /// </summary>
    public string sVersion = string.Empty;
    /// <summary>
    /// The device no. string
    /// </summary>
    public string sDevNo = string.Empty;

    /// <summary>
    /// The data folder, selected per Properties dialog
    /// </summary>
    public string sDataFolder = string.Empty;
    
    /// <summary>
    /// The Diagnosis object
    /// </summary>
    public Diagnosis Diag = new Diagnosis ( true );

    /// <summary>
    /// Peak information
    /// </summary>
    public PeakInfo PeakInfo = new PeakInfo ();

    /// <summary>
    /// The # of scan points curr'ly transferred 
    /// </summary>
    public int nPoints = 0;                                  
    /// <summary>
    /// The Gain of the scan curr'ly transferred
    /// ( Goal: 
    ///   1. Kombi-NG-SW:
    ///     Hier kann beim direkten RX'n & Parsen der SCANALLDATA data der compressed PID-scan sauber 
    ///     gebildet werden: f�r die Skalierung ist das data-Max. erford., und das h�ngt vom Gain ab.
    ///   2. PC-SW:
    ///     Not required in principle.  
    /// )
    /// </summary>
    public int Gain = 0;

    /// <summary>
    /// Continuous spectra recording: Indicates, whether a scan was already recorded (True) or not yet (False)
    /// </summary>
    bool m_bScansRecorded = false;
    /// <summary>
    /// Continuous result recordingg: Indicates, whether a result was already recorded (True) or not yet (False)
    /// </summary>
    bool m_bResultsRecorded = false;

    /// <summary>
    /// The "Scan(s) to be drawn" array
    /// </summary>
    public bool[] arScanToDraw = new bool[MAX_SCANBUFFERS];

    /// <summary>
    /// The comment string for printer output
    /// </summary>
    public string sComment;

    /// <summary>
    /// Path of the scan file to be loaded from HD
    /// </summary>
    public string sScanFilePath = "";

    /// <summary>
    /// The # of MP channels (0, if MP not connected)
    /// </summary>
    public int nMPChan = 0;
    
    /// <summary>
    /// Indicates, whether the Clock-timed script (CTS) handling on the device is enabled (True) or not (False)
    /// </summary>
    public bool bCTSRunning = false; 

    /// <summary>
    /// Indicates, whether WarmUp is running on device (true) or not (false)
    /// </summary>
    public bool bWarmUpRunning = false;

    /// <summary>
    /// Contents of a service data file
    /// </summary>
    public string sServiceData = "";
    
    #endregion // members
  
    #region properties
    #endregion  // properties
  
    #region methods

    /// <summary>
    /// Updates all views.
    /// (Conform to the VC++ framework)
    /// </summary>
    public void UpdateAllViews () 
    {
      App app = App.Instance;
      Vw vw = app.View;

      // Update view
      vw.OnUpdate ();
    
      // Update print preview
      // TODO: How to suppress status window in dynamical case?
      if ( app.Data.MainForm.bPrintPreview ) 
      {
        // The print preview is activated.
        // Proof: Should it be updated dynamically?
        if ( !app.Data.MainForm.bStaticalPrintPreview ) 
        {  
          // Yes: Update it.
          MainForm f = app.MainForm;
          f.PrintPreview.InvalidatePreview ();
        }
      }
    }

    
    ////////////////////////////////////////////////////////////////
    // Script name related stuff

    /// <summary>
    /// Gets the index of a certain script in the device scripts array
    /// </summary>
    /// <param name="sScriptName">The name of the script, whose index should be found</param>
    /// <returns>The index of the script (-1, if the script could not be found)</returns>
    /// <remarks>
    /// This routine is particularly provided for the SCRSELECT script message: 
    /// Instead of the script name its index in the device scripts array is transferred to the device.
    /// Reason:
    /// The UART0 Rx FIFO from the LPC2148-device has a 16 Byte depth. If a SCRSELECT cmd is transferred
    /// and assembled by the device whilst the device program is in the script execution loop, it could
    /// happen, that the Pulser timer ISR (witch has HIGHER priority as the UART0 ISR) intercepts/masks
    /// the UART0 ISR. In this case the FIFO contents will not be transferred into the static Rx buffer,
    /// but remain in the FIFO. If the overall cmd-par-string length is less than 16 Bytes, then the
    /// complete cmd is preserved & fetched at next time into the Rx buffer. If on the other side the overall 
    /// cmd-par-string length is greater than 16 Bytes, then the resting Bytes are lost and an incomplete
    /// cmd is fetched at next time into the Rx buffer. THIS BEHAVIOUR MUST BE AVOIDED!
    /// A solution is to NOT transfer cmd-par-strings with a length exceeding 16 Bytes (including terminating
    /// 0x0 and 0x0D) whilst the device program is in the script execution loop. Transferring the index
    /// instead of the script name in case of SCRSELECT cmd leads to a string of the length
    ///   9 (SCRSELECT) + 1 (SPACE) + 2 (Index) + 2 (0x0 and 0x0D) = 14 Bytes,
    /// wherewith the above demand is fulfilled.   
    /// </remarks>
    public int ScriptNameToIdx ( string sScriptName )
    {
      for ( int i=0; i < arsDeviceScripts.Count ;i++ )
      {
        if ( sScriptName == arsDeviceScripts[i] )
        {
          return i;
        }
      }
      return -1;
    }
    
    /// <summary>
    /// Gets the name of a script at a certain index position in the device scripts array (JM).
    /// </summary>
    /// <param name="idx">The index position in the device scripts array</param>
    /// <returns>The name of the script (an empty string, if the index position is invalid)</returns>
    public string ScriptNameFromIdx ( int idx )
    {
      string s;
      try
      {
        s = arsDeviceScripts[idx];
      }
      catch
      {
        s = "";
      }
      return s;
    }
    
    ////////////////////////////////////////////////////////////////
    // Update Script list

    /// <summary>
    /// Updates the Script list
    /// </summary>
    /// <param name="sPar">The RX'd from device Script list string</param>
    public void UpdateScriptList (string sPar)
    {
      // Update the documents 'arsDeviceScripts' member
      string[] ars = sPar.Split ( ',' );
      int nCount = ars.Length;

      this.arsDeviceScripts.Clear ();
      for ( int i=0; i < nCount; i++ )
        if (ars[i].Length > 0) this.arsDeviceScripts.Add ( ars[i] );
    }
   
    ////////////////////////////////////////////////////////////////
    // Loading & saving script data

    /// <summary>
    /// Loads a script configuration file
    /// </summary>
    /// <param name="cm">The cryptografic mode of the file</param>
    /// <returns>True, if OK; otherwise false</returns>
    public bool LoadScript(CryptMode cm)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      Ressources r = app.Ressources;

      // The name of the script file
      string sFileName = sScriptFilename;
      string sTempFilename = string.Empty;
      string sInfo = string.Empty;
      try
      {
        // Open file dialog: Select script file
        if (GetFileName(ref sFileName, true, cm))
        {
          Cursor.Current = Cursors.WaitCursor;
          // CD: Cryptografic mode of the script file
          if (cm == CryptMode.Encrypted)
          {
            // Encrypted:
            // Read encrypted script file into byte array
            FileStream fs = File.OpenRead(sFileName);
            int nBytes = (int)fs.Length;
            byte[] arby = new byte[nBytes];
            int nBytesRead = fs.Read(arby, 0, nBytes);
            fs.Close();
            // Decrypt byte array
            string sDecrypted = Crypt.AESDecrypt(arby, AppData.CommonData.AES_KEY, AppData.CommonData.AES_IV);
            // Store decrypted data into a temporary script file
            sTempFilename = doc.FxGetTempFilename();
            StreamWriter fout = File.CreateText(sTempFilename);
            fout.Write(sDecrypted);
            fout.Close();
            // Load temporary script file
            arScript.Load(sTempFilename, out sInfo);
          }
          else
          {
            // Decrypted:
            // Load the script file
            arScript.Load(sFileName, out sInfo);
          }
          if (sInfo.Length > 0)
          {
            // Info: Zul�ssige max. Scriptanzahl OR Fensteranzahl OR Befehlsanzahl �berschritten
            string sMsg = string.Format("{0} '{1}':\r\n", r.GetString("cDoc_LoadScript_File"), sFileName); // "Datei '<filename>':\r\n"
            sMsg += sInfo;
            string sCap = r.GetString("Form_Common_TextInfo");           // "Info"
            MessageBox.Show(sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
          }
        }
        else
        {
          string sMsg = r.GetString("cDoc_LoadScript_NoFileSel");  // "Es wurde keine Datei ausgew�hlt."
          throw new ApplicationException(sMsg);
        }

        // Write app data: Name of the script file
        app.Data.Configuration.sLastFile = sFileName;
        app.Data.Write();

        // Memorize the filename on success
        sScriptFilename = sFileName;
        return true;
      }
      catch (System.Exception exc)
      {
        Debug.WriteLine("Doc.LoadScript failed\n");

        string sMsgHeader = r.GetString("cDoc_Error_LoadScript");  // "The configuration file with the script data wasn't loaded.\r\nReason:"
        string sMsg = string.Format("{0}\r\n\r\n{1}", sMsgHeader, exc.Message);
        string sCap = r.GetString("Form_Common_TextError");  // "Fehler"
        MessageBox.Show(sMsg, sCap);

        return false;
      }
      finally
      {
        Cursor.Current = Cursors.Default;
        // Delete temporary script file, if req.
        if (sTempFilename.Length > 0)
          File.Delete(sTempFilename);
      }
    }

    /// <summary>
    /// Saves a script configuration file
    /// </summary>
    /// <param name="cm">The cryptografic mode of the file</param>
    /// <returns>True, if OK; otherwise false</returns>
    /// <remarks>Not used; Needs reworking in case of usage (invocation of crypt. mode)!</remarks>
    public bool SaveScript(CryptMode cm)
    {
      App app = App.Instance;

      // The name of the script file
      string sFileName = sScriptFilename;
      try
      {
        // Save the script file
        if (sFileName.Length == 0)
          GetFileName(ref sFileName, false, cm);
        arScript.Write(sFileName);

        // Write app data: Name of the script file
        app.Data.Configuration.sLastFile = sFileName;
        app.Data.Write();

        // Memorize the filename on success
        sScriptFilename = sFileName;
        return true;
      }
      catch
      {
        Debug.WriteLine("Doc.SaveScript failed\n");

        Ressources r = app.Ressources;
        string sMsg = r.GetString("cDoc_Error_SaveScript");  // "Die Scriptkonfigurationsdatei wurde nicht gespeichert."
        MessageBox.Show(sMsg);

        return false;
      }
    }

    /// <summary>
    /// Gets the full path of a script file by means of a FileOpen-/FileSave-Dialog
    /// </summary>
    /// <param name="sFileName">The file path</param>
    /// <param name="bOpen">True for FileOpen-Dialog, False for FileSave-Dialog</param>
    /// <param name="cm">The cryptografic mode of the file</param>
    /// <returns>True, if the dialog was closed by means of OK; otherwise false</returns>
    public bool GetFileName(ref string sFileName, bool bOpen, CryptMode cm)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Initial dir.
      string sInitialDir = @"C:\";
      int nPos = sFileName.LastIndexOf('\\');
      if (-1 != nPos)
      {
        string sDir = sFileName.Substring(0, nPos);
        if (Directory.Exists(sDir))
          sInitialDir = sDir;
      }
      Directory.SetCurrentDirectory(sInitialDir);
      // Filter, Def. ext.
      string sFilter, sDefaultExt;
      //  CD: Cryptografic mode of the script file
      if (cm == CryptMode.Encrypted)
      {
        // Encrypted:
        sFilter = "PID configuration file (*.pnc)|*.pnc";   // Filter
        sDefaultExt = "pnc";                                // Def. ext.
      }
      else
      {
        // Decrypted:
        sFilter = "PID configuration file (*.gsm)|*.gsm";   // Filter
        sDefaultExt = "gsm";                                // Def. ext.
      }
      // Title
      string sTitle;
      if (bOpen)
        sTitle = r.GetString("cDoc_GetFileName_Open");  // "Open configuration file"; 
      else
        sTitle = r.GetString("cDoc_GetFileName_Save");  // "Save configuration file";

      // Dialog
      bool bRet;
      if (bOpen)
      {
        OpenFileDialog ofd = new OpenFileDialog();

        ofd.Filter = sFilter;

        // Filename is displayed only if crypt. mode coincides!
        string sExt = Path.GetExtension(sFileName);
        sExt = sExt.TrimStart('.');
        if (sExt == sDefaultExt) ofd.FileName = sFileName;

        ofd.InitialDirectory = sInitialDir;
        ofd.Title = sTitle;
        ofd.DefaultExt = sDefaultExt;
        ofd.AddExtension = true;

        bRet = (DialogResult.OK == ofd.ShowDialog());
        if (bRet)
        {
          // Overtake the file path
          sFileName = ofd.FileName;

          // Write app data: Name ( full path ) of the script file
          app.Data.Common.sScriptFileName = ofd.FileName;
          app.Data.Write();
        }
        ofd.Dispose();
      }
      else
      {
        SaveFileDialog sfd = new SaveFileDialog();

        sfd.Filter = sFilter;

        // Filename is displayed only if crypt. mode coincides!
        string sExt = Path.GetExtension(sFileName);
        sExt = sExt.TrimStart('.');
        if (sExt == sDefaultExt) sfd.FileName = sFileName;

        sfd.InitialDirectory = sInitialDir;
        sfd.Title = sTitle;
        sfd.DefaultExt = sDefaultExt;
        sfd.AddExtension = true;

        bRet = (DialogResult.OK == sfd.ShowDialog());
        if (bRet)
        {
          // Overtake the file path
          sFileName = sfd.FileName;

          // Write app data: Name ( full path ) of the configuration file
          app.Data.Common.sScriptFileName = sfd.FileName;
          app.Data.Write();
        }
        sfd.Dispose();
      }

      return bRet;
    }

    ////////////////////////////////////////////////////////////////
    // Loading & saving service data

    /// <summary>
    /// Loads a service data file
    /// </summary>
    /// <param name="cm">The cryptografic mode of the file</param>
    /// <returns>True, if OK; otherwise false</returns>
    public bool LoadServiceData(CryptMode cm)
    {
      App app = App.Instance;
      Doc doc = app.Doc;
      Ressources r = app.Ressources;

      // The name of the (lastly used) service data file
      app.Data.Read();
      string sFileName = app.Data.Common.sServiceFileName;
      try
      {
        // Open file dialog: Select service data file
        if (GetServiceFileName(ref sFileName, true, cm))
        {
          Cursor.Current = Cursors.WaitCursor;
          // CD: Cryptografic mode of the service data file
          if (cm == CryptMode.Encrypted)
          {
            // Encrypted:
            // Read encrypted service data file into byte array
            FileStream fs = File.OpenRead(sFileName);
            int nBytes = (int)fs.Length;
            byte[] arby = new byte[nBytes];
            int nBytesRead = fs.Read(arby, 0, nBytes);
            fs.Close();
            // Decrypt byte array
            string sDecrypted = Crypt.AESDecrypt(arby, AppData.CommonData.AES_KEY, AppData.CommonData.AES_IV);
            // Remember service data file contents
            sServiceData = sDecrypted;
          }
          else
          {
            // Decrypted:
            // Get service data file contents
            sServiceData = File.ReadAllText(sFileName);
          }
        }
        else
        {
          string sMsg = r.GetString("cDoc_LoadScript_NoFileSel");  // "Es wurde keine Datei ausgew�hlt."
          throw new ApplicationException(sMsg);
        }

        // Write app data: Name of the service data file
        app.Data.Common.sServiceFileName = sFileName;
        app.Data.Write();

        return true;
      }
      catch (System.Exception exc)
      {
        Debug.WriteLine("Doc.LoadServiceData failed\n");

        string sMsgHeader = r.GetString("cDoc_Error_LoadServiceData");  // "The service data file wasn't loaded.\r\nReason:"
        string sMsg = string.Format("{0}\r\n\r\n{1}", sMsgHeader, exc.Message);
        string sCap = r.GetString("Form_Common_TextError");  // "Fehler"
        MessageBox.Show(sMsg, sCap);

        return false;
      }
      finally
      {
        Cursor.Current = Cursors.Default;
      }
    }

    /// <summary>
    /// Gets the full path of a service data file by means of a FileOpen-/FileSave-Dialog
    /// </summary>
    /// <param name="sFileName">The file path</param>
    /// <param name="bOpen">True for FileOpen-Dialog, False for FileSave-Dialog</param>
    /// <param name="cm">The cryptografic mode of the file</param>
    /// <returns>True, if the dialog was closed by means of OK; otherwise false</returns>
    public bool GetServiceFileName(ref string sFileName, bool bOpen, CryptMode cm)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Initial dir.
      string sInitialDir = @"C:\";
      int nPos = sFileName.LastIndexOf('\\');
      if (-1 != nPos)
      {
        string sDir = sFileName.Substring(0, nPos);
        if (Directory.Exists(sDir))
          sInitialDir = sDir;
      }
      Directory.SetCurrentDirectory(sInitialDir);
      // Filter, Def. ext.
      string sFilter, sDefaultExt;
      //  CD: Cryptografic mode of the script file
      if (cm == CryptMode.Encrypted)
      {
        // Encrypted:
        sFilter = "PID service data file (*.psd)|*.psd";    // Filter
        sDefaultExt = "psd";                                // Def. ext.
      }
      else
      {
        // Decrypted:
        sFilter = "PID service data file (*.txt)|*.txt";    // Filter
        sDefaultExt = "txt";                                // Def. ext.
      }
      // Title
      string sTitle;
      if (bOpen)
        sTitle = r.GetString("cDoc_GetServiceFileName_Open");  // "Open service data file"; 
      else
        sTitle = r.GetString("cDoc_GetServiceFileName_Save");  // "Save service data file";

      // Dialog
      bool bRet;
      if (bOpen)
      {
        OpenFileDialog ofd = new OpenFileDialog();

        ofd.Filter = sFilter;

        // Filename is displayed only if crypt. mode coincides!
        string sExt = Path.GetExtension(sFileName);
        sExt = sExt.TrimStart('.');
        if (sExt == sDefaultExt) ofd.FileName = sFileName;

        ofd.InitialDirectory = sInitialDir;
        ofd.Title = sTitle;
        ofd.DefaultExt = sDefaultExt;
        ofd.AddExtension = true;

        bRet = (DialogResult.OK == ofd.ShowDialog());
        if (bRet)
        {
          // Overtake the file path
          sFileName = ofd.FileName;

          // Write app data: Name ( full path ) of the service data file
          app.Data.Common.sServiceFileName = ofd.FileName;
          app.Data.Write();
        }
        ofd.Dispose();
      }
      else
      {
        SaveFileDialog sfd = new SaveFileDialog();

        sfd.Filter = sFilter;

        // Filename is displayed only if crypt. mode coincides!
        string sExt = Path.GetExtension(sFileName);
        sExt = sExt.TrimStart('.');
        if (sExt == sDefaultExt) sfd.FileName = sFileName;

        sfd.InitialDirectory = sInitialDir;
        sfd.Title = sTitle;
        sfd.DefaultExt = sDefaultExt;
        sfd.AddExtension = true;

        bRet = (DialogResult.OK == sfd.ShowDialog());
        if (bRet)
        {
          // Overtake the file path
          sFileName = sfd.FileName;

          // Write app data: Name ( full path ) of the service data file
          app.Data.Common.sServiceFileName = sfd.FileName;
          app.Data.Write();
        }
        sfd.Dispose();
      }

      return bRet;
    }

    /////////////////////////////////////////////////////////////////
    // Versionning
    
    /// <summary>
    /// Checks for version coincidence.
    /// </summary>
    /// <returns>True, if the device version is correct; otherwise False</returns>
    public bool CheckVersion ()
    {
      for (int i=0; i < VERSIONSTRING.Length; i++)
      {
        string sPermVersion = VERSIONSTRING[i];
        if (sVersion.IndexOf (sPermVersion) > -1)
          return true;
      }
      return false;
    }

    /////////////////////////////////////////////////////////////////
    // Data handling
    
    /// <summary>
    /// Reset the scan data
    /// </summary>
    /// <returns>True</returns>
    public bool ResetScanData ()
    {
      // Reset number of incoming DATA responses (scans)
      dwScanCount = 0;
      // Reset number of incoming NODATA responses
      dwNoDataCount = 0;

      // Reset the scan data
      for ( int i=0; i < MAX_SCANBUFFERS; i++ )
        arScanData[i].Clear ();

      // Reset marked windows
      for (int i=0; i < MAX_SCAN_RESULT; i++)
        nMarkedWindow[i] = false;   
      // Synchronously reset the Result Checkboxes
      App app = App.Instance;
      MainForm f = app.MainForm;
      for (int i=0; i < f.archkRes.Length; i++)
        f.archkRes[i].Checked = false;

      return true;
    }
 
    /////////////////////////////////////////////////////////////////
    // Data recording

    /// <summary>
    /// Data recording - main entry:
    /// Records the scan data.
    /// </summary>
    /// <param name="scn">The scan to be (eventually) recorded</param>
    public void RecordData ( ScanData scn )
    {
      string sx, sCap, s;

      App app = App.Instance;
      Doc doc = app.Doc;
      Ressources r = app.Ressources;

      try
      {
        // Data recording: Single scan (Current scan recording)
        if ( bRecordSingleScan )
        {
          // Scan complete?
          if (doc.nPoints == Doc.NSCANDATA)
          {
            // Yes:
            bRecordSingleScan = false;
            // Record a single scan and outputs a fulfillment message
            RecordSingleScanWithMessage (scn);
            return;
          }
        };
    
        // Data recording: Scans continuously (Continuous spectra recording)
        if ( bRecordScans )
        {
          // Scan complete?
          if (doc.nPoints == Doc.NSCANDATA)
          {
            // Yes:
            // Check, whether a complete scan has already been recorded
            if (!m_bScansRecorded)
            {
              // No: 
              // Record a complete scan
              RecordScan ( scn );
              // Indicate, that a complete scan has been recorded
              m_bScansRecorded=true;
            }
          }
          else
          {
            // No: 
            // Reset the 'Scan recorded' indication
            m_bScansRecorded = false;
          }
        }
        else
        {
          // Finish recording:
          // Check: Did a storage take place?
          if (m_bScansRecorded)
          {
            // Yes:
            // Reset the 'Scan recorded' indication
            m_bScansRecorded = false;
            // Check: Did a storage take place?
            if ( sRecordScanFolder.Length > 0 )
            {
              // Yes:
              // Empty the string containing the Data folder, where the storage takes place
              s = sRecordScanFolder;
              sRecordScanFolder = string.Empty;
              // Fulfilment message
              string fmt = r.GetString ( "cDoc_Record_Scan_Success" );      // "Die Spektren wurden im Ordner\r\n   {0}\r\nabgelegt."
              sx = string.Format ( fmt, s );
              sCap = r.GetString ("Form_Common_TextInfo");                  // "Information"
              MessageBox.Show (app.MainForm, sx, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
          }
        }
    
        // Data recording: Results continuously (Continuous result recording)
        if ( bRecordResults )
        {
          // Scan complete?
          if (doc.nPoints == Doc.NSCANDATA)
          {
            // Yes:
            // Check, whether a result has already been recorded
            if (!m_bResultsRecorded)
            {
              // No: 
              // Record a result
              RecordResult ();
              // Indicate, that a result has been recorded
              m_bResultsRecorded=true;
            }
          }
          else
          {
            // No: 
            // Reset the 'Result recorded' indication
            m_bResultsRecorded = false;
          }
        }
        else
        {
          // Finish recording:
          // Check: Did a storage take place?
          if (m_bResultsRecorded)
          {
            // Reset the 'Result recorded' indication
            m_bResultsRecorded = false;
            // Check: Did a storage take place?
            if ( sRecordResultFilename.Length > 0 )
            {
              // Yes:
              // Empty the string containing the Data file path, where the storage takes place
              s = sRecordResultFilename;
              sRecordResultFilename = string.Empty;
              // Fulfilment message
              string fmt = r.GetString ( "cDoc_Record_Result_Success" );    // "Die Messergebnisse wurden in Datei\r\n   {0}\r\ngeschrieben."
              sx = string.Format ( fmt, s );
              sCap = r.GetString ("Form_Common_TextInfo");                  // "Information"
              MessageBox.Show (app.MainForm, sx, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
          }
        }

      }
      catch
      {
        // Error: Reset data recording members
        RecordingReset ();
      }
    }

    /// <summary>
    /// Data recording:
    /// Resets the data recording members.
    /// </summary>
    public void RecordingReset ()
    {
      bRecordSingleScan = false;
      bRecordScans       = false;
      bRecordResults    = false;

      sRecordSingleScanFilename = string.Empty;
      sRecordScanFolder = string.Empty;
      sRecordResultFilename = string.Empty;

      m_bScansRecorded = false;
      m_bResultsRecorded = false;
    }

    /// <summary>
    /// Data recording:
    /// Writes the common Data file header.
    /// </summary>
    /// <param name="fout">The StreamWriter object</param>
    /// <param name="sTimeStamp">The time stamp</param>
    public void RecordScanDataWriteCommonHeader ( StreamWriter fout, string sTimeStamp )
    {
      string sx;
      App app = App.Instance;
      Ressources r = app.Ressources;
      
      try
      {
        // Row: Device SW version
        FWSTR2 ( fout, "Device Version" ); FWSTR ( fout, sVersion ); FWNL ( fout );
        // Row: Device number
        FWSTR2 ( fout, "Device number" ); FWSTR ( fout, sDevNo); FWNL ( fout );
        // Row: Date
        FWSTR  ( fout, "Date"  ); FWSTR ( fout, sTimeStamp ); FWNL ( fout );
        
        // Empty row
        FWNL ( fout );
        
        // Row: Scripts in Device (subheader)
        FWSTR2 ( fout, "Scripts in Device" ); FWVAL  ( fout, arsDeviceScripts.Count ); FWNL ( fout );
        //  (Rows: Names of the Device scripts (max. 3 names per row))
        for ( int i=0; i < arsDeviceScripts.Count; i++ )
        {
          FWSTR ( fout, arsDeviceScripts[i] );
          if ( (i+1)%3 == 0 ) FWNL ( fout );
        }
        if (arsDeviceScripts.Count%3 != 0) FWNL ( fout );
        
        // Empty row
        FWNL ( fout );
        
        // Row: Running script
        FWSTR2 ( fout, "Running Script" ); FWSTR ( fout, sCurrentScript ); FWNL ( fout );
        
        // Empty row
        FWNL ( fout );
        
        // Row: User time
        // Notes: Usertime is in 1/10 sec -> Rescale for output in sec
        FWSTR2 ( fout, "User time [s]" ); FWVAL ( fout, ScanInfo.dwUserTime/10 ); FWNL ( fout );
        
        // Empty row
        FWNL ( fout );
        
        // Rows: Windows in Running Script (subheader)
        FWSTR ( fout, "Windows in Running Script" );  FWNL (fout );
        FWSTR ( fout, "Name\tBegin\tWidth\tResult\tUnit\tSpanFac" ); FWNL ( fout );
        FWSTR ( fout, "\t[pts]\t[pts]" ); FWNL ( fout );
        System.Globalization.NumberFormatInfo lfi = System.Globalization.NumberFormatInfo.CurrentInfo;
        for ( int i=0; i < arScanResult.Count; i++ )
        {
          ScanResult res = arScanResult[i];
          FWSTR ( fout, res.sSubstance  );                        // substance  
          FWVAL ( fout, res.dwBeginTime );                        // begin
          FWVAL ( fout, res.dwWidthTime );                        // width
          sx = Common.ConvertToScience ( res.dResult, 3 );        // result, including conc. incredible
          sx = sx.Replace ( ".", lfi.NumberDecimalSeparator );
          if (res.byConcIncredible == 1) sx = "> " + sx;           
          FWSTR (fout, sx);
          FWSTR (fout, res.sConcUnit);                            // conc. unit
          FWDBL (fout, res.dSpanFac, 3);                          // span factor
          FWNL ( fout );
        }
        
        // Empty row
        FWNL ( fout );
        
        // Row: Temp. & ControlIO word    
        FWSTR2 ( fout, "Temperature [�C]" ); FWDBL ( fout, DeviceInfo.dTemperature, 1 ); FWSTR ( fout, "" ); 
        FWSTR ( fout, "CTRLIO" ); FWVAL ( fout, ScanInfo.wCtrlIO ); FWNL ( fout );
        // Row: Pressure
        // NOTE:
        //  In the DeviceInfo class, the pressure is given in kPa units. This unit is our base pressure unit,
        //  as well in the device SW as in the PC SW. But there is a little blemish in the IMS SW: Other (older) SW products
        //  like IMS_1 f.i. require the pressure input in mbar units. Because these SW products get all its
        //  information from the data files recorded by means of GSMControl, we decided to store the pressure 
        //  in the IMS PC-SW in mbar units. In order to avoid different storage units in IMS and PID, we decide
        //  to store the pressure here in mbar units too.
        double dPres_mbar = DeviceInfo.dPressure * 10;            // Pres. in mbar
        FWSTR2 ( fout, "Pressure [mbar]" );  
        if (DeviceInfo.nPresDispOnOff == 1) FWDBL ( fout, dPres_mbar, 0 );                      // Pressure display on
        else                                FWSTR ( fout, r.GetString( "Div_NotConnected" ) );  // Pressure display off
        FWNL ( fout );
        // Row: Flows
        if (app.Data.Program.bDisplayFlow)
        {
          FWSTR2 (fout, "Flow [ml/min]"); FWVAL (fout, DeviceInfo.nFlow1); FWDBL (fout, DeviceInfo.dFlow2, 1); 
          FWVAL ( fout, DeviceInfo.nFlow3 ); FWNL ( fout );
        }
        // Row: Battery
        FWSTR2 ( fout, "Battery [%]" ); FWSTR (fout, DeviceInfo.BatteryState); FWNL ( fout );
        // Row: Offset
        FWSTR2 ( fout, "Offset [adc]" );  FWVAL ( fout, DeviceInfo.nOffset ); FWNL ( fout );
        // Row: Offset STD
        FWSTR2 ( fout, "STD [adc]" );  FWDBL ( fout, DeviceInfo.dSTD, 2 ); FWNL ( fout );
        // Row: Error Dword
        string sErrWar = ScanInfo.ErrorAsString ();
        if (sErrWar.Length == 0) sErrWar = "None";
        FWSTR2 ( fout, "Errors on device" ); FWSTR (fout, sErrWar); FWNL ( fout );
        // Row: Warning Word
        sErrWar = ScanInfo.WarningAsString ();
        if (sErrWar.Length == 0) sErrWar = "None";
        FWSTR2 ( fout, "Warnings on device" ); FWSTR (fout, sErrWar); FWNL ( fout );
     
        // Empty row
        FWNL ( fout );
        
        // Rows: Peak information (subheader)
        //    "Peak information" 
        FWSTR ( fout, "Peak information" );  FWNL (fout );
        FWSTR ( fout, "Position\tHeight\tArea" );  FWNL (fout );
        FWSTR ( fout, "[pts]\t[%]\t[adc*pts]" ); FWNL ( fout );
        foreach (PeakInfoItem item in this.PeakInfo.Items)
        {
          FWVAL ( fout, item.Position );                          // Pos
          FWDBL ( fout, item.Height, 2 );                         // Height (%)
          if (item.Invalid == 1)  FWSTR ( fout, "OVERRANGE" );    // Area
          else                    FWDBL ( fout, item.Area, 0 ); 
          FWNL ( fout );
        }
        
        // Empty row
        FWNL ( fout );
        
        // Smoothing: "Smoothing\t\t<width>\t<type>\t"
        string sSmoothingType = (this.ScanParams.bySmoothingFilterType == 0) ? "MA" : "SG"; 
        FWSTR2 ( fout, "Smoothing" ); FWVAL ( fout, ScanParams.bySmoothingFilterWidth ); FWSTR ( fout, sSmoothingType ); FWNL ( fout );
        // Peak search 'n' sigma factor
        FWSTR2 ( fout, "NSigma (Peaksearch)" ); FWVAL ( fout, ScanParams.byNSigPeakSearch ); FWNL ( fout );
        // Peak search mode
        string sPSmode = (this.ScanParams.byModePeakSearch == 0) ? PeakSearch.Modi.Unidirectional : PeakSearch.Modi.Bidirectional; 
        FWSTR2 ( fout, "Mode (Peaksearch)" ); FWSTR ( fout, sPSmode ); FWNL ( fout );
        // Peak area determination 'n' sigma factor
        FWSTR2 ( fout, "NSigma (Areadetection)" ); FWVAL ( fout, ScanParams.byNSigAreaDetection ); FWNL ( fout );
        // Peak area determination mode
        string sPAmode = "";
        switch (this.ScanParams.byModeAreaDetection)
        {
          case 0: sPAmode = PeakAreaDetermination.Modi.Simple; break;
          case 1: sPAmode = PeakAreaDetermination.Modi.Extended_1; break;
          case 2: sPAmode = PeakAreaDetermination.Modi.Extended_2; break;
        }
        FWSTR2 ( fout, "Mode (Areadetection)" ); FWSTR ( fout, sPAmode ); FWNL ( fout );
        
        // Empty row
        FWNL ( fout );
        
        // Row: Monitoring station
        FWSTR2 ( fout, "Meas. channel" );  FWVAL ( fout, ScanInfo.byChan ); FWNL ( fout );
        
        // Empty row
        FWNL ( fout );
      }
      catch
      {
        Debug.WriteLine ( "Doc.RecordScanDataWriteCommonHeader -> function failed" );
        RecordingReset ();
        throw;
      }
    }
    
    /// <summary>
    /// Records a single scan and outputs a fulfillment message.
    /// </summary>
    /// <param name="scn">The scan to be recorded</param>
    public void RecordSingleScanWithMessage (ScanData scn)
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Record single scan
      RecordSingleScan ( scn );
      // Empty the string containing the Data folder, where the storage takes place:
      // Must be done here in order to prevent a waste successor message caused by the 'bRecordScans' section
      sRecordScanFolder = string.Empty; 
      // Fulfilment message
      string fmt = r.GetString ( "cDoc_Record_SingleScan_Success" );  // "Das aktuelle Spektrum wurde in Datei\r\n   {0}\r\ngeschrieben."
      string sx = string.Format ( fmt, sRecordSingleScanFilename );
      string sCap = r.GetString ("Form_Common_TextInfo");               // "Information"
      MessageBox.Show (app.MainForm, sx, sCap, MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    /// <summary>
    /// Data recording:
    /// Records a single scan.
    /// </summary>
    /// <param name="scn">The scan to be recorded</param>
    public void RecordSingleScan (ScanData scn)
    {
      StreamWriter fout = null;
      string sTempFilename = string.Empty;

      App app = App.Instance;
      Ressources r = app.Ressources;

      try
      {
        // Preparation:

        //  Get the current DateTime
        DateTime tim = DateTime.Now;

        //  Create the basic directory (adjusted via the Properties dialog), if req.
        sRecordScanFolder = sDataFolder;
        Directory.CreateDirectory (sRecordScanFolder);
        //  Create the subdirectory (characterized by the Time stamp), if req.
        sRecordScanFolder = System.IO.Path.Combine (sRecordScanFolder, "SCN " + tim.ToString ("yy-MM-dd"));
        Directory.CreateDirectory (sRecordScanFolder);

        //  Get the hundredths of seconds
        int dw100sec = tim.Millisecond / 10;
        string sx = string.Format ("{0:D2}", dw100sec);

        //  Format the current Time stamp for output in the data file header, including hundredths of seconds
        string sTimeStamp = tim.ToString ("yy.MM.dd HH:mm:ss") + ":" + sx;

        //  Build the data file name, based on the current datetime including hundredths of seconds 
        string sFileTitle = tim.ToString ("HH-mm-ss") + "-" + sx;
        //  Build the full data file path
        sRecordSingleScanFilename = Path.Combine (sRecordScanFolder, "SCN " + sFileTitle + ".txt");

        //  Prepare data recording into a temp'ry file
        sTempFilename = FxGetTempFilename ();
        //  Create the data file
        fout = File.CreateText (sTempFilename);

        // Part: Header
        //  Output: Title rows
        sx = "Photo Ionisation Detector PID";
        FWSTR (fout, sx); FWNL (fout);
        sx = "Current spectrum recording";
        FWSTR (fout, sx); FWNL2 (fout);
        //  Output: Common header
        RecordScanDataWriteCommonHeader (fout, sTimeStamp);
      }
      catch
      {
        if (fout != null) fout.Close ();
        if (File.Exists (sTempFilename)) File.Delete (sTempFilename);

        RecordingReset ();

        string sMsg = r.GetString ("cDoc_Record_SingleScan_FileCreate_Error");  // "Aufzeichnung des aktuellen Spektrums: Fehler beim �ffnen der Datei."
        string sCap = r.GetString ("Form_Common_TextError");                    // "Fehler"
        MessageBox.Show (app.MainForm, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);

        throw new ApplicationException ();
      }

      // Part: Data
      try
      {
        // Output: Data
        FWSTR (fout, "Abscissa\tValue"); FWNL (fout);
        FWSTR (fout, "[pts]\t[units]"); FWNL (fout);

        for (int i = 0; i < scn.Count; i++)
        {
          FWSTR (fout, i.ToString ()); FWDBL (fout, scn[i], 3); FWNL (fout);
        }
      }
      catch
      {
        if (fout != null) fout.Close ();
        if (File.Exists (sTempFilename)) File.Delete (sTempFilename);

        RecordingReset ();

        string sMsg = r.GetString ("cDoc_Record_SingleScan_FileWrite_Error");   // "Aufzeichnung des aktuellen Spektrums: Fehler beim Schreiben der Datei."
        string sCap = r.GetString ("Form_Common_TextError");                    // "Fehler"
        MessageBox.Show (app.MainForm, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);

        throw new ApplicationException ();
      }

      // Close the file
      fout.Close ();

      // Rename the file  & Delete the temp. file
      File.Copy (sTempFilename, sRecordSingleScanFilename, true);
      File.Delete (sTempFilename);
    }

    /// <summary>
    /// Data recording:
    /// Records scans continuously.
    /// </summary>
    /// <param name="scn">The scan to be recorded</param>
    public void RecordScan (ScanData scn)
    {
      StreamWriter fout = null;
      string sTempFilename = string.Empty;
      string sFilename = string.Empty;

      App app = App.Instance;
      Ressources r = app.Ressources;

      try
      {
        // Preparation:

        //  Get the current DateTime
        DateTime tim = DateTime.Now;

        //  Create the basic directory (adjusted via the Properties dialog), if req.
        sRecordScanFolder = sDataFolder;
        Directory.CreateDirectory (sRecordScanFolder);
        //   Create the subdirectory (characterized by the Time stamp), if req.
        sRecordScanFolder = System.IO.Path.Combine (sRecordScanFolder, "SCN " + tim.ToString ("yy-MM-dd"));
        Directory.CreateDirectory (sRecordScanFolder);

        //  Get the hundredths of seconds
        int dw100sec = tim.Millisecond / 10;
        string sx = string.Format ("{0:D2}", dw100sec);

        //  Format the current Time stamp for output in the data file header, including hundredths of seconds
        string sTimeStamp = tim.ToString ("yy.MM.dd HH:mm:ss") + ":" + sx;
        //  Build the data file name, based on the current datetime including hundredths of seconds 
        string sFileTitle = tim.ToString ("HH-mm-ss") + "-" + sx;

        //  Build the full data file path
        sFilename = Path.Combine (sRecordScanFolder, "SCN " + sFileTitle + ".txt");

        //  Prepare data recording into a temp'ry file
        sTempFilename = FxGetTempFilename ();
        //  Create the data file
        fout = File.CreateText (sTempFilename);

        // Part: Header
        //  Output: Title rows
        sx = "Photo Ionisation Detector PID";
        FWSTR (fout, sx); FWNL (fout);
        sx = "Continuous spectra recording";
        FWSTR (fout, sx); FWNL2 (fout);
        //  Output: Common header
        RecordScanDataWriteCommonHeader (fout, sTimeStamp);
      }
      catch
      {
        if (fout != null) fout.Close ();
        if (File.Exists (sTempFilename)) File.Delete (sTempFilename);

        RecordingReset ();

        string sMsg = r.GetString ("cDoc_Record_Scan_FileCreate_Error");        // "Kontinuierliche Aufzeichnung der Spektren: Fehler beim �ffnen der Datei."
        string sCap = r.GetString ("Form_Common_TextError");                    // "Fehler"
        MessageBox.Show (app.MainForm, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);

        throw new ApplicationException ();
      }

      // Part: Data
      try
      {
        // Output: Data
        FWSTR (fout, "Abscissa\tValue"); FWNL (fout);
        FWSTR (fout, "[pts]\t[units]"); FWNL (fout);

        for (int i = 0; i < scn.Count; i++)
        {
          FWSTR (fout, i.ToString ()); FWDBL (fout, scn[i], 3); FWNL (fout);
        }
      }
      catch
      {
        if (fout != null) fout.Close ();
        if (File.Exists (sTempFilename)) File.Delete (sTempFilename);

        RecordingReset ();

        string sMsg = r.GetString ("cDoc_Record_Scan_FileWrite_Error");         // "Kontinuierliche Aufzeichnung der Spektren: Fehler beim Schreiben der Datei."
        string sCap = r.GetString ("Form_Common_TextError");                    // "Fehler"
        MessageBox.Show (app.MainForm, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);

        throw new ApplicationException ();
      }

      // Close the file
      fout.Close ();

      // Rename the file  & Delete the temp. file
      File.Copy (sTempFilename, sFilename, true);
      File.Delete (sTempFilename);
    }

    /// <summary>
    /// Data recording:
    /// Records results continuously.
    /// </summary>
    public void RecordResult ()
    {
      StreamWriter fout = null;
      string sx;

      App app = App.Instance;
      Ressources r = app.Ressources;

      // Open the file
      try
      {
        if (sRecordResultFilename.Length == 0)
        {
          // First access:

          // Preparation:

          //  Get the current DateTime
          DateTime tim = DateTime.Now;

          //  Get the hundredths of seconds
          int dw100sec = tim.Millisecond / 10;
          sx = string.Format ("{0:D2}", dw100sec);

          //  Format the current Time stamp for output in the data file header, including hundredths of seconds
          string sTimeStamp = tim.ToString ("yy.MM.dd HH:mm:ss") + ":" + sx;
          //  Build the data file name, based on the current datetime including hundredths of seconds 
          string sFileTitle = tim.ToString ("yy-MM-dd HH-mm-ss") + "-" + sx;

          //  Create the basic directory (adjusted via the Properties dialog), if req.
          Directory.CreateDirectory (sDataFolder);
          //  Build the full data file path
          sRecordResultFilename = Path.Combine (sDataFolder, "RES " + sFileTitle + ".txt");
          //  Create the data file
          fout = File.CreateText (sRecordResultFilename);

          // Part: Header
          //  Output: Title rows
          sx = "Photo Ionisation Detector PID";
          FWSTR (fout, sx); FWNL (fout);
          sx = "Continuous measuring result recording";
          FWSTR (fout, sx); FWNL2 (fout);
          //  Output: Common header
          RecordScanDataWriteCommonHeader (fout, sTimeStamp);
          //  Output: Data header
          //    1. row: Names ("Time\tSubA\t ...")
          FWSTR (fout, "Time");
          for (int i = 0; i < arScanResult.Count; i++)
          {
            FWSTR (fout, arScanResult[i].sSubstance);
          }
          FWNL (fout);
          //    2. row: Span factors ("SpanFac\tSubA_spanfac\t ...")
          FWSTR (fout, "Spanfac");
          for (int i = 0; i < arScanResult.Count; i++)
          {
            FWDBL (fout, arScanResult[i].dSpanFac, 3);
          }
          FWNL (fout);
          //    3. row: Units "\tSubA_concunit\t ..."
          FWSTR (fout, "");
          for (int i = 0; i < arScanResult.Count; i++)
          {
            sx = string.Format ("[{0}]", arScanResult[i].sConcUnit);
            FWSTR (fout, sx);
          }
          FWNL (fout);
        }
        else
        {
          // Subsequent access:

          // Open the data file for appending
          fout = File.AppendText (sRecordResultFilename);
        }
      }
      catch
      {
        if (fout != null) fout.Close ();
        RecordingReset ();

        string sMsg = r.GetString ("cDoc_Record_Result_FileCreate_Error");      // "Kontinuierliche Aufzeichnung der Messergebnisse: Fehler beim �ffnen der Datei."
        string sCap = r.GetString ("Form_Common_TextError");                    // "Fehler"
        MessageBox.Show (app.MainForm, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);

        throw new ApplicationException ();
      }

      // Part: Data
      try
      {
        // Output: Data

        // Time
        DateTime t = DateTime.Now;
        FWSTR (fout, t.ToString ("dd.MM.yyyy HH:mm:ss"));
        // Substance results
        System.Globalization.NumberFormatInfo lfi = System.Globalization.NumberFormatInfo.CurrentInfo;
        for (int i = 0; i < arScanResult.Count; i++)
        {
          ScanResult res = arScanResult[i];
          sx = Common.ConvertToScience (res.dResult, 3);        // result, including conc. incredible
          sx = sx.Replace (".", lfi.NumberDecimalSeparator);
          if (res.byConcIncredible == 1) sx = "> " + sx;
          FWSTR (fout, sx);
        }
        FWNL (fout);
      }
      catch
      {
        if (fout != null) fout.Close ();
        RecordingReset ();

        string sMsg = r.GetString ("cDoc_Record_Result_FileWrite_Error");       // "Kontinuierliche Aufzeichnung der Messergebnisse: Fehler beim Schreiben der Datei."
        string sCap = r.GetString ("Form_Common_TextError");                    // "Fehler"
        MessageBox.Show (app.MainForm, sMsg, sCap, MessageBoxButtons.OK, MessageBoxIcon.Error);

        throw new ApplicationException ();
      }

      // Close the file
      fout.Close ();
    }


    /////////////////////////////////////////////////////////////////
    // Data recording - helper stuff

    const string szTAB  = "\t";
    const string szTAB2 = "\t\t";
    const string szNL  = "\r\n";
    const string szNL2 = "\r\n\r\n";

    /// <summary>
    /// Outputs: "(String)(TAB)"
    /// </summary>
    void FWSTR ( StreamWriter w, string s )   { w.Write ( s );  w.Write ( szTAB ); }
    
    /// <summary>
    /// Outputs: "(String)(TAB)(TAB)"
    /// </summary>
    void FWSTR2 ( StreamWriter w, string s ) { w.Write ( s );  w.Write ( szTAB2 ); }
    
    /// <summary>
    /// Outputs: "('Val' without decimal digits)(TAB)"
    /// </summary>
    void FWVAL ( StreamWriter w, double val ) { w.Write ( val.ToString ( "F0" ) );  w.Write ( szTAB ); }
    
    /// <summary>
    /// Outputs: "('Val' with 'Prec' decimal digits)(TAB)"
    /// </summary>
    void FWDBL ( StreamWriter w, double val, int prec ) 
    {
      System.Globalization.NumberFormatInfo lfi = System.Globalization.NumberFormatInfo.CurrentInfo;
      string sx = Common.ConvertToScience ( val, prec );
      sx = sx.Replace ( ".", lfi.NumberDecimalSeparator );
      w.Write ( sx ); 
      w.Write ( szTAB );
    }

    /// <summary>
    /// Outputs: "(NL)"
    /// </summary>
    void FWNL ( StreamWriter w )    { w.Write ( szNL ); }
   
    /// <summary>
    /// Outputs: "(NL)(NL)"
    /// </summary>
    void FWNL2 ( StreamWriter w )   { w.Write ( szNL2 ); }
 
    /// <summary>
    /// Outputs: "(TAB)"
    /// </summary>
    void FWTAB ( StreamWriter w )   { w.Write ( szTAB ); }
    
    /// <summary>
    /// Outputs: "(TAB)(TAB)"
    /// </summary>
    void FWTAB2 ( StreamWriter w )  { w.Write ( szTAB2 ); }

    /// <summary>
    /// Returns a unique temporary file name and creates a zero-byte file by that name on disk.
    /// </summary>
    /// <returns>A unique temporary file name</returns>
    internal string FxGetTempFilename ()
    {
      string szTempFile = Path.GetTempFileName ();
      return szTempFile;
    }
    

    /////////////////////////////////////////////////////////////////
    // Scan processing 

    /// <summary>
    /// Builds a raw scan based on the device byte array (scan part). 
    /// </summary>
    /// <param name="arsScan">The scan element string array</param>
    /// <param name="scn">The retrieved scan (output)</param>
    /// <remarks>
    /// Here is supposed, that the scan received from the device (input) was in adc units.
    /// On output the scan is scaled to % units - thats why we name the % unit furthermore the 
    /// "basic ordinate unit".
    /// </remarks>
    public void BuildScanArray ( string[] arsScan, ref ScanData scn )
    {
      float fData;
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
      // Clear the scan
      scn.Clear ();
      // Build the scan
      foreach (string s in arsScan)
      {
        // The current scan element (in adc units)
        fData = float.Parse (s, nfi);
        // Scale it to % unit
        fData *= (float)(100.0/Doc.ADC_RESOLUTION);
        // Add it to the scan data collection
        scn.Add (fData);
      }
      // Fill up with zeros until full length is reached
      while (scn.Count < Doc.NSCANDATA)
      {
        fData = -10;        // Percent: ordinate minimum
        scn.Add (fData);
      }
    }

    /// <summary>
    /// Builds a smoothed scan.
    /// </summary>
    /// <param name="scn">The raw scan</param>
    /// <param name="scn_sm">The smoothed scan (out)</param>
    public void BuildSmoothedScanArray ( ScanData scn, ref ScanData scn_sm )
    {
      float fData;
      if  (this.nPoints == NSCANDATA)
      {
        // DAQ finished: Smooth the raw scan

        // Arrays (int) for both scans
        //    Raw scan array 
        //    Notes:
        //      The raw scan is scaled to % units. Against it the smoothing procedure on the device works
        //      over the original scan, which is scaled to adc units. Thats why we build the raw scan
        //      array based on adc units.
        int[] ar = new int[scn.Count];
        for (int i=0; i < ar.Length; i++)
        {
          ar[i] = (int)(scn[i]*Doc.ADC_RESOLUTION/100.0);     // scaling: % -> adc
        }
        //    Smoothed scan array 
        int[] ar_sm = new int[ar.Length];

        // Smoothing (based on adc units)
        Utilities.SpecEval se = new Utilities.SpecEval ();
        int width = this.ScanParams.bySmoothingFilterWidth;           
        if (this.ScanParams.bySmoothingFilterType == 0)
        {
          // MA
          se.ma_smooth (ar, ref ar_sm, ar.Length, ar.Length, width);
        }
        else
        {
          // SG: Method 4.: var. filter, wrapping
          se.sg_smooth_CalcSGCoeff (width);
          se.sg_smooth (ar, ref ar_sm, ar.Length, ar.Length, width);
        }
        
        // Create the smoothed scan, based on the smoothed scan array
        // Notes:
        //  The smoothed scan should be scaled to % units. Thats why on building the smoothed scan
        //  a scaling from adc to % units is required.
        scn_sm.Clear ();
        for (int i=0; i < ar_sm.Length; i++)
        {
          fData=(float)(ar_sm[i]*100.0/Doc.ADC_RESOLUTION);   // scaling: adc -> %
          scn_sm.Add (fData);
        }
      }
      else
      {
        // DAQ not yet finished: Fill the smoothed scan with the ordinate minimum
        
        scn_sm.Clear ();
        for (int i=0; i < scn.Count ;i++)
        {
          fData = -10;        // Percent: ordinate minimum
          scn_sm.Add (fData);
        }
      }
    }


    /////////////////////////////////////////////////////////////////
    // Loading scan from HD 


    /// <summary>
    /// Gets the path of a Scan file.
    /// </summary>
    /// <returns>True, if a scan file was selected; False otherwise</returns>
    public bool GetScanFile ()
    {
      App app = App.Instance;
      Ressources r = app.Ressources;

      // Initial dir.
      string sInitialDir = @"C:\";                        // Default dir.
      int nPos = sScanFilePath.LastIndexOf ( '\\' );      // If the file path is already known: dir. of the file
      if ( -1 != nPos )
      {
        string sDir = sScanFilePath.Substring ( 0, nPos );
        if (Directory.Exists(sDir))
          sInitialDir = sDir;
      }
      Directory.SetCurrentDirectory ( sInitialDir );
      // Filter
      string sFilter = "Scan file (*.txt)|*.txt";
      // Def. ext.
      string sDefaultExt = "txt";  
      // Title
      string sTitle = r.GetString ("cDoc_LoadScan_OFN_Open"); // "Scandatei �ffnen"

      // File open dialog
      OpenFileDialog ofd = new OpenFileDialog ();
      
      ofd.Filter = sFilter;
      ofd.FileName = sScanFilePath;
      ofd.InitialDirectory = sInitialDir;
      ofd.Title = sTitle;
      ofd.DefaultExt = sDefaultExt;
      ofd.AddExtension = true;
      
      bool bRet = ( DialogResult.OK == ofd.ShowDialog() );
      if ( bRet )
      {
        // Overtake the file path
        sScanFilePath = ofd.FileName;
      }  
      ofd.Dispose ();
      
      // Done
      return bRet;
    }

    /// <summary>
    /// Provides the Documents data structures with scan data, stored in a scan file.
    /// </summary>
    public void FillDataStructures ()
    {
      StreamReader reader = null;
      string sLine, s;
      string[] ars;
      bool bData = false;
      float y;
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
      // Init.: 'Smoothing' entry NOT available in the file
      byte bySmoothingExists = 0;
      // Indicate: nSig - peak search entry NOT available in the file
      bool bNSigPeakSearchExists = false;
      // Indicate: mode - peak search entry NOT available in the file
      bool bModePeakSearchExists = false;
      // Indicate: nSig - area det. entry NOT available in the file
      bool bNSigAreaDetectionExists = false;
      // Indicate: mode - area det. entry NOT available in the file
      bool bModeAreaDetectionExists = false;
      // Indicate: Flow entry NOT available in the file
      bool bFlowExists = false;

      try
      {
        // Init.: Parsing of section "Windows in Running Script"
        int nWinCount = 0;          
        bool bWinSection = false;
        arScanResult.Clear ();
        // Init.: Parsing of section "Peak information"
        int nPeakCount = 0;
        bool bPeakSection = false;
        PeakInfo.Items.Clear ();
        // Init.: Parsing of data section
        ScanData scn = arScanData[0];
        scn.Clear ();        
        int counter=0;
        // Open the scan file
        reader = File.OpenText ( sScanFilePath );
        // Read it
        while ( (sLine = reader.ReadLine ()) != null )
        {
          sLine=sLine.TrimEnd ();
          ars = sLine.Split ('\t');
          
          // Testing, req. for parsing of section "Windows in Running Script"
          if (bWinSection) 
          {
            nWinCount++; 
            if (sLine.Length == 0) bWinSection=false;
          }
          // Testing, req. for parsing of section "Peak info"
          if (bPeakSection) 
          {
            nPeakCount++; 
            if (sLine.Length == 0) bPeakSection=false;
          }

          // Header
          if (!bData)
          {
            if (sLine.StartsWith ("Run"))                   // Running Script
            {
              s = GetRowValue (ars, 2);
              sCurrentScript = s;
            }
            else if (sLine.StartsWith ("Win"))              // Windows in Running Script
            {
              bWinSection=true; 
            }
            else if (bWinSection && (nWinCount > 2))        // dito: SR data rows
            {
              // Parse the current SR data row (Name\tBegin\tWidth\tResult\tUnit\tSpanFac\t) 
              ScanResult sr = new ScanResult ();
              //    Name
              s = GetRowValue (ars, 1);
              sr.sSubstance = s;
              //    Begin
              s = GetRowValue (ars, 2);
              sr.dwBeginTime = UInt32.Parse (s);
              //    Width
              s = GetRowValue (ars, 3);
              sr.dwWidthTime = UInt32.Parse (s);
              //    Result (derived from this: ConcIncredible)
              s = GetRowValue (ars, 4);
              s = s.Replace ( ',', '.' ); 
              if (s.StartsWith(">"))
              {
                sr.byConcIncredible = 1;
                s=s.Substring(2);
              }
              else
              {
                sr.byConcIncredible = 0;
              }
              sr.dResult = double.Parse (s, nfi);
              //    Unit
              s = GetRowValue (ars, 5);
              sr.sConcUnit = s;
              //    SpanFac
              s = GetRowValue (ars, 6);
              s = s.Replace ( ',', '.' ); 
              sr.dSpanFac = double.Parse (s, nfi);
  
              // Depose fictive alarm values
              // Notes:
              //  A scan file doesn't contain alarm info in its 'Windows' section.
              //  Because the scan result labels are colored according to the presence of an alarm state,
              //  we have to assign at this point self-chosen alarm values to the A1 and A2 members of the 
              //  scan result. We choose them in a manner, so that optically no alarm was present.
              sr.dA1 = sr.dResult + 1;
              sr.dA2 = sr.dA1 + 1;

              // Add scan result
              arScanResult.Add (sr);
            }
            else if (sLine.StartsWith ("Temp"))             // Temp.
            {
              s = GetRowValue (ars, 2);
              s = s.Replace ( ',', '.' ); 
              DeviceInfo.dTemperature = double.Parse(s, nfi);
            }
            else if (sLine.StartsWith ("Pres"))             // Pressure ...
            {
              s = GetRowValue (ars, 2);
              s = s.Replace ( ',', '.' ); 
              // Notes: 
              //  Pressure is stored in mbar units (see comment there). Because the display units are kPa,
              //  we hwve to scale the presssure corr'ly.
              try
              {
                double pres = double.Parse(s, nfi);        
                pres /= 10;
                DeviceInfo.dPressure = pres;                  // ... in kPa uits
                DeviceInfo.nPresDispOnOff = 1;                // Pressure display on
              }
              catch
              {
                DeviceInfo.dPressure = -1;                    // ... (not connected)
                DeviceInfo.nPresDispOnOff = 0;                // Pressure display off
              }
            }
            else if (sLine.StartsWith ("Flow"))               // Flow1-3
            {
              s = GetRowValue (ars, 2);
              s = s.Replace ( ',', '.' ); 
              DeviceInfo.nFlow1 = int.Parse (s);
              s = GetRowValue (ars, 3);
              s = s.Replace ( ',', '.' );
              DeviceInfo.dFlow2 = double.Parse (s, nfi);
              try
              {
                s = GetRowValue (ars, 4);
                s = s.Replace ( ',', '.' ); 
                DeviceInfo.nFlow3 = int.Parse (s);
              }
              catch
              {
                DeviceInfo.nFlow3 = 0;
              }
              // Indicate: Flow entry found
              bFlowExists = true;
            }
            else if (sLine.StartsWith ("Off"))              // Scan offset
            {
              s = GetRowValue (ars, 2);
              s = s.Replace ( ',', '.' ); 
              DeviceInfo.nOffset = int.Parse (s);
            }
            else if (sLine.StartsWith ("STD"))              // Scan offset STD
            {
              s = GetRowValue (ars, 2);
              s = s.Replace ( ',', '.' ); 
              DeviceInfo.dSTD = double.Parse(s, nfi);
            }
            else if (sLine.StartsWith ("Error"))            // Errors on device
            {
              s = GetRowValue (ars, 2);
              if (s == "None") s="";
              ScanInfo.ErrorFromString (s); 
            }
            else if (sLine.StartsWith ("Warning"))          // Warnings on device
            {
              s = GetRowValue (ars, 2);
              if (s == "None") s="";
              ScanInfo.WarningFromString (s); 
            }
            else if (sLine.StartsWith ("Pea"))              // Peak info
            {
              bPeakSection=true; 
            }
            else if (bPeakSection && (nPeakCount > 2))      // dito: PI data rows
            {
              // Parse the current PI data row 
              PeakInfoItem item = new PeakInfoItem ();
              
              s = GetRowValue (ars, 1);                     //    pos.
              item.Position = UInt16.Parse (s);
              
              if (ars.Length == 2 ) 
              {
                // file, preceding the current version (i.e.: 2 items per PI row: pos, area)              
              
                s = GetRowValue (ars, 2);                   //    area
                s = s.Replace ( ',', '.' ); 
                if (s == "OVERRANGE") 
                {
                  item.Invalid = 1;
                  item.Area = -1;
                }
                else
                {
                  item.Invalid = 0;
                  item.Area = float.Parse(s, nfi);
                }
              }
              else
              {
                // file, corr'ing to the current version (i.e.: 3 items per PI row: pos, height, area)              
              
                s = GetRowValue (ars, 2);                   //    height
                s = s.Replace ( ',', '.' ); 
                item.Height = float.Parse(s, nfi);
              
                s = GetRowValue (ars, 3);                   //    area
                s = s.Replace ( ',', '.' ); 
                if (s == "OVERRANGE") 
                {
                  item.Invalid = 1;
                  item.Area = -1;
                }
                else
                {
                  item.Invalid = 0;
                  item.Area = float.Parse(s, nfi);
                }
              }

              PeakInfo.Items.Add (item);
            }
            else if (sLine.StartsWith ("Smo"))              // Smoothing:
            {
              s = GetRowValue (ars, 2);
              s = s.Replace ( ',', '.' ); 
              ScanParams.bySmoothingFilterWidth = byte.Parse(s);                      //  Width
              // Indicate: 'Smoothing' entry found: width
              bySmoothingExists = 1;
              try
              {
                s = GetRowValue (ars, 3);
                s = s.Replace ( ',', '.' ); 
                if (s.Length > 0)
                {
                  ScanParams.bySmoothingFilterType = (s == "MA") ? (byte)0 : (byte)1; //  Type
                  // Indicate: 'Smoothing' entry found: width & type
                  bySmoothingExists = 2;
                }
              }
              catch
              {
              }
            }
            else if (sLine.StartsWith ("NSigma (Peaksearch)"))    // nSig - peak search
            {
              s = GetRowValue (ars, 2);
              s = s.Replace ( ',', '.' ); 
              ScanParams.byNSigPeakSearch = byte.Parse(s); 
              // Indicate: nSig - peak search entry found
              bNSigPeakSearchExists = true;
            }
            else if (sLine.StartsWith ("Mode (Peaksearch)"))      // mode - peak search
            {
              s = GetRowValue (ars, 2);
              ScanParams.byModePeakSearch = (s == PeakSearch.Modi.Unidirectional) ? (byte)0 : (byte)1;
              // Indicate: mode - peak search entry found
              bModePeakSearchExists = true;
            }
            else if (sLine.StartsWith ("NSigma (Areadetection)")) // nSig - area det.
            {
              s = GetRowValue (ars, 2);
              s = s.Replace ( ',', '.' ); 
              ScanParams.byNSigAreaDetection = byte.Parse(s); 
              // Indicate: nSig - area det. entry found
              bNSigAreaDetectionExists = true;
            }
            else if (sLine.StartsWith ("Mode (Areadetection)"))   // mode - peak area det.
            {
              s = GetRowValue (ars, 2);
              if      (s == PeakAreaDetermination.Modi.Simple)     ScanParams.byModeAreaDetection = 0;
              else if (s == PeakAreaDetermination.Modi.Extended_1) ScanParams.byModeAreaDetection = 1;
              else                                                 ScanParams.byModeAreaDetection = 2;
              // Indicate: mode - area det. entry found
              bModeAreaDetectionExists = true;
            }
            else if (sLine.StartsWith ("[pts]\t[units]"))   // End of header
              bData = true;

            continue;
          }
  
          // Data
          s = GetRowValue (ars, 2);
          s = s.Replace ( ',', '.' );                   // The current y value as string
          y = float.Parse (s, nfi);                     // y in %
          if (counter < Doc.NSCANDATA)                  // Fill the raw scan (base unit: %)
            scn.Add (y);
          counter++;
        }
        
        // The scan file was completely read:
        
        // Pad the raw scan with 0's, if required
        if (counter < Doc.NSCANDATA)                    
        {
          for (int i=counter; i < Doc.NSCANDATA;i++)
            scn.Add (0);
        }
        // The # of scan points
        nPoints = scn.Count;
        // Select the scan receive buffer for the smoothed scan
        ScanData scn_sm = arScanData[1];
        
        // Check: If the 'Smoothing' entry was NOT found, then assign: type=SG, width=41
        //        (used up to version 3.6)
        //        If the 'Smoothing' entry was only partially found, then assign: type=SG
        //        (used up to version 3.7.0)
        if (0 == bySmoothingExists)
        {
          ScanParams.bySmoothingFilterType = 1;
          ScanParams.bySmoothingFilterWidth = 41;
        }
        else if (1 == bySmoothingExists)
        {
          ScanParams.bySmoothingFilterType = 1;
        }
        // Check: If the 'NSigma (Peaksearch)' entry was NOT found, then assign: 6
        if (!bNSigPeakSearchExists)
        {
          ScanParams.byNSigPeakSearch = 6;
        }
        // Check: If the 'Mode (Peaksearch)' entry was NOT found, then assign: 0 
        // (All earlier versions used unidirectional peak search mode.)
        if (!bModePeakSearchExists)
        {
          ScanParams.byModePeakSearch = 0;
        }
        // Check: If the 'NSigma (Areadetection)' entry was NOT found, then assign: 3
        if (!bNSigAreaDetectionExists)
        {
          ScanParams.byNSigAreaDetection = 3;
        }
        // Check: If the 'Mode (Areadetection)' entry was NOT found, then assign: 0
        // (All earlier versions used simple peak area determination mode.)
        if (!bModeAreaDetectionExists)
        {
          ScanParams.byModeAreaDetection = 0;
        }
        
        // Check: If the 'Flow1-3' entry was NOT found, then assign: 0
        if (!bFlowExists)
        {
          DeviceInfo.nFlow1 = 0;
          DeviceInfo.dFlow2 = 0.0;
          DeviceInfo.nFlow3 = 0;
        }

        // Build the smoothed scan
        BuildSmoothedScanArray ( scn, ref scn_sm );
      }
      catch
      {
        Debug.WriteLine ("Doc.FillDataStructures() failed.");
      }
      finally
      {
        if (null != reader)
          reader.Close ();
      }
    }

    /// <summary>
    /// Gets a given non-empty string member from a string array.
    /// </summary>
    /// <param name="ars">The string array</param>
    /// <param name="nCol">The number of the non-empty string in the array to be retrieved</param>
    /// <returns>The requested string member (Empty string, if not found)</returns>
    string GetRowValue (string[] ars, int nCol)
    {
      int i, counter;
      string s;

      // Loop over the string array
      for (i=0, counter=0; i < ars.Length; i++)
      {
        s = ars[i];                     // The current string member
        if (s.Length == 0) continue;    // Skip empty strings
        counter++;                      // The current string member is non-empty: count it
        if (counter == nCol)            // Did we found the required member?
          return s;                     // Yes.
      }
      return string.Empty;              // No.
    }
    
    #endregion // methods

  }
}
