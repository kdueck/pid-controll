using System;
using System.Collections;

namespace App
{
  /// <summary>
  /// Class PeakAreaDetermination:
  /// Peak area determination modi
  /// </summary>
  public class PeakAreaDetermination
  {
    public class Modi
    {
      /// <summary>
      /// Peak area determination mode: Simple
      /// </summary>
      public const string Simple = "Simple";
      /// <summary>
      /// Peak area determination mode: Extended_1
      /// </summary>
      public const string Extended_1 = "Extended_1";
      /// <summary>
      /// Peak area determination mode: Extended_2
      /// </summary>
      public const string Extended_2 = "Extended_2";
    }
  }

}