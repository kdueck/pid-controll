using System;
using System.Collections;

namespace App
{
	/// <summary>
	/// Class ScanResult:
	/// Scan result:  Substance, meas. result, measurement window begin, measurement window width, 
	///               LO alarm value, HI alarm value, concentration unit, concentration credible?,
	///               span factor
	/// </summary>
	public class ScanResult
	{
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScanResult()
		{
		  Empty ();
    }
    
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="res">The ScanResult object to be initialized with</param>
    public ScanResult(ScanResult res)
    {
      sSubstance  = res.sSubstance;
      dResult     = res.dResult;
      dwBeginTime = res.dwBeginTime;
      dwWidthTime = res.dwWidthTime;
      dA1         = res.dA1;
      dA2         = res.dA2;
      sConcUnit   = res.sConcUnit;
      byConcIncredible = res.byConcIncredible;
      dSpanFac    = res.dSpanFac;
    }
  
    #endregion constructors

    #region members

    /// <summary>
    /// Substance name
    /// </summary>
    public string sSubstance;    
    
    /// <summary>
    /// Measurement result
    /// </summary>
    public double dResult;        
    
    /// <summary>
    /// Measurement window begin (in pts)
    /// </summary>
    public UInt32 dwBeginTime;    
    
    /// <summary>
    /// Measurement window width (in pts)
    /// </summary>
    public UInt32 dwWidthTime; 
    
    /// <summary>
    /// LO Alarm value
    /// </summary>
    public double dA1;       

    /// <summary>
    /// HI Alarm value
    /// </summary>
    public double dA2;       
    
    /// <summary>
    /// Concentration unit
    /// </summary>
    public string sConcUnit; 

    /// <summary>
    /// Indicates, whether the calculated concentration is credibly (0) or not (OverFlow) (1)
    /// </summary>
    public byte byConcIncredible;

    /// <summary>
    ///  Span factor
    /// </summary>
    public double   dSpanFac;      

    #endregion members

    #region methods
    
    /// <summary>
    /// Initiates all members
    /// </summary>
    public void Empty ()
    {
      sSubstance  = string.Empty;
      dResult     = 0.0;
      dwBeginTime = 0;
      dwWidthTime = 0;
      dA1         = 90.0;
      dA2         = 100.0;
      sConcUnit   = string.Empty;
      byConcIncredible = 0;
      dSpanFac    = 1.0; 
    }
  
    /// <summary>
    /// Obtains the String representation of this instance.
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
      string sScanResult = string.Format 
        ("{0},{1},{2},{3},{4},{5},{6},{7},{8},", 
        this.sSubstance,
        this.dResult.ToString ("F2", nfi), 
        this.dwBeginTime,
        this.dwWidthTime,
        this.dA1.ToString ("F2", nfi),
        this.dA2.ToString ("F2", nfi),
        this.sConcUnit,
        this.byConcIncredible,
        this.dSpanFac.ToString ("F3", nfi)
        );
      return sScanResult;
    }
    
    #endregion methods

  }


  /// <summary>
  /// Class ScanResultArray:
  /// Scan result list
  /// </summary>
  public class ScanResultArray : IEnumerable
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScanResultArray ()
    {
    }

    #endregion constructors

    #region members
    
    /// <summary>
    /// The list
    /// </summary>
    private ArrayList _Items = new ArrayList();

    #endregion members
    
    #region methods

    /// <summary>
    /// Clears the list
    /// </summary>
    public void Clear () 
    {
      _Items.Clear ();
    }

    /// <summary>
    /// Adds a ScanResult object to the list
    /// </summary>
    /// <param name="res">The ScanResult object to be added</param>
    public void Add ( ScanResult res ) 
    {
      _Items.Add ( res );
    }
    
    #endregion methods

    #region properties

    /// <summary>
    /// Gets the size of the list
    /// </summary>
    public int Count 
    {
      get { return _Items.Count; }
    }
    
    #endregion properties

    #region indexer
    
    /// <summary>
    /// The indexer
    /// </summary>
    public ScanResult this[int index]
    {
      get { return (ScanResult)_Items[index]; }
    }

    #endregion indexer
    
    #region IEnumerable Members

    /// <summary>
    /// The enumerator
    /// </summary>
    /// <returns>The enumerator</returns>
    public IEnumerator GetEnumerator ()
    {
      return _Items.GetEnumerator ();
    }

    #endregion IEnumerable Members

  }

}
