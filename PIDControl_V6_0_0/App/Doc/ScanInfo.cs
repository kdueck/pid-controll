using System;

namespace App
{
	/// <summary>
	/// Class ScanInfo:
	/// Scan information: Script idx, control word, error dword, warning word, user time, play time, 
	///                   device application status, meas. channel
	/// </summary>
	/// <remarks>
	/// 1. Concerning the control word 'wCtrlIO':
	///   
	///   The bits 0 to 15 of this word serve as 'actor specifiers' and are assigned 
	///   to the device actors as follows:
  ///   #define CTRLIO_PUMP1    0
  ///   #define CTRLIO_PUMP2    1
  ///   #define CTRLIO_PUMP3    2
  ///   
  ///   #define CTRLIO_VALVE1   3
  ///   #define CTRLIO_VALVE2   4
  ///
  ///   #define CTRLIO_LAMP     5
  ///   
  ///   #define CTRLIO_TEMP1    7
  ///   #define CTRLIO_TEMP2    8
  ///   #define CTRLIO_TEMP3    9
  ///   #define CTRLIO_TEMP4    10
  ///   
  ///   If an actor is switched off, the corr'ing bit is set to 0.
  ///   If an actor is switched on, the corr'ing bit is set to 1.
  ///   
  ///   Actually only bits 0 to 13 may be used as actor state bits.
  ///   - Bit 14 is reserved for the "Signal processing triggered" recognition. 
  ///   - Bit 15 is reserved for the "Device ready for measurement" recognition. 
  ///   
  ///   ( See also the 'SetHwCtrlLine()' routine of the device SW. )
  ///   
  /// 2. Concerning the error dword 'dwError':
  /// 
  ///   This member is NOT used within the PC SW.
  ///   It is used ONLY by the CombiNG SW in order to inform about possible device errors.
  ///   
  /// </remarks>
	public class ScanInfo
	{
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScanInfo()
		{
      byScriptIdx       = 0;
      wCtrlIO           = 0;
      dwError           = 0;
      wWarning          = 0;
      dwPlayTime        = 0;		
      dwUserTime        = 0;
      wDeviceAppStatus  = 0;
      byChan            = 0;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="sPar">A string containing ScanInfo information</param>
    /// <remarks>
    /// The string has the following format:
    ///   "(Script idx),(CtrlIOWord),(ErrDWord),(WarWord),(PlayTime),(UserTime),(AppStatus),(Channel#)"
    /// Ex.:
    ///   "0,592,256,0,11,11,1,0"
    /// </remarks>
    public ScanInfo ( string sPar )
    {
      string[] ars = sPar.Split ( ',' );
      if ( ars.Length != _NO_SCANINFO_MEMBERS )
        throw new System.ApplicationException ();
      
      byScriptIdx       = byte.Parse ( ars[0] );
      wCtrlIO           = UInt16.Parse ( ars[1] );
      dwError           = UInt32.Parse ( ars[2] );
      wWarning          = UInt16.Parse ( ars[3] );
      dwPlayTime        = UInt32.Parse ( ars[4] );
      dwUserTime        = UInt32.Parse ( ars[5] );
      wDeviceAppStatus  = UInt16.Parse ( ars[6] );
      byChan            = byte.Parse ( ars[7] );
    }
  
    #endregion constructors

    #region constants
    
    // The # of ScanInfo members: Script idx, control word, error dword, warning word, user time, play time, 
    //                            device application status, meas. channel
    const int _NO_SCANINFO_MEMBERS = 8; 

    // Error messages (max. 32, corr'ing to the bit positions 0 to 31 in the error dword)
    // NOTE:
    //  This array must correspond to the array 'g_sErrorMsg' of the device SW.
    string[] sErrorMsg = new string[] 
    {
      // HR part (Bit positions 0 to 7)
      "Invalid scan offset",                                // Displayed ErrorID = 1
      "Error on Spectrum denoising",                        //          ErrorID = 2
      "Unknown script command",                             //          ErrorID = 3       
      "",                                                   //          ErrorID = 4       (n.n)
      "SD card full",                                       //          ErrorID = 5       
      "PC comm. Wait-Timeout",                              //          ErrorID = 6
      "Display comm. Wait-Timeout",                         //          ErrorID = 7
      "AB comm. Wait-Timeout",                              //          ErrorID = 8
      // AB part (Bit positions 8 to 15)
      "AB: Error temp. sensor 1",                           //          ErrorID = 9
      "AB: Error temp. sensor 2",                           //          ErrorID = 10
      "AB: Error temp. sensor 3",                           //          ErrorID = 11
      "AB: Error temp. sensor 4",                           //          ErrorID = 12
      "AB: Error pressure sensor",                          //          ErrorID = 13
      "AB: Error flow sensor 1 (sensor damaged)",           //          ErrorID = 14
      "AB: Error flow sensor 2 (sensor damaged)",           //          ErrorID = 15
      "Error comm. with AB-CPU",                            //          ErrorID = 16
      // Further errors (Bit positions 16 to 23)
      "Error in SD card file system",                       //          ErrorID = 17
      "PID lamp failure",                                   //          ErrorID = 18      
      "Automated span monitoring out-of-range error",       //          ErrorID = 19
      "No answer from Relay board",                         //          ErrorID = 20
      "Flow 1 out of range",                                //          ErrorID = 21
      "Flow 2 out of range",                                //          ErrorID = 22
      "Flow 3 out of range",                                //          ErrorID = 23
      "AB: Error flow sensor 3 (sensor damaged)",           //          ErrorID = 24
      // MP part (Bit positions 24 to 31)
      "MP: Temp 1 error",                                   //          ErrorID = 25
      "MP: Temp 2 error",                                   //          ErrorID = 26
      "Error comm. with MP-CPU",                            //          ErrorID = 27
      "",                                                   //          ErrorID = 28      (n.n.)
      "MP: Invalid meas. channel",                          //          ErrorID = 29      
      "MP: Invalid valve opening time",                     //          ErrorID = 30      
      "MP: Flow error",                                     //          ErrorID = 31
      ""                                                    //          ErrorID = 32      (n.n.)
    };  
  
    // Warning messages (max. 16, corr'ing to the bit positions 0 to 15 in the warning word)
    // NOTE:
    //  This array must correspond to the array 'g_sWarningMsg' of the device SW.
    string[] sWarningMsg = new string[] 
    {
      "SD card 90% filled",             //          WarningID = 1
      "MFC debit exceeds MFC capacity", //          WarningID = 2 
      "",                               //          WarningID = 3     (n.n.)
      "Service date exceeded",          //          WarningID = 4
      "",                               //          WarningID = 5     (n.n.)
      "",                               //          WarningID = 6     (n.n.)
      "",                               //          WarningID = 7     (n.n.)
      "",                               //          WarningID = 8     (n.n.)
      "",                               //          WarningID = 9     (n.n.)
      "",                               //          WarningID = 10    (n.n.)  
      "",                               //          WarningID = 11    (n.n.)  
      "",                               //          WarningID = 12    (n.n.)
      "",                               //          WarningID = 13    (n.n.)
      "",                               //          WarningID = 14    (n.n.)
      "",                               //          WarningID = 15    (n.n.)
      ""                                //          WarningID = 16    (n.n.)     
    };
    
    #endregion constants

    #region members
    
    /// <summary>
    /// Script idx
    /// </summary>
    public byte byScriptIdx;          
    
    /// <summary>
    /// The Control word
    /// </summary>
    public UInt16 wCtrlIO;         
    
    /// <summary>
    /// Error Dword
    /// </summary>
    public UInt32 dwError;         

    /// <summary>
    /// Warning word
    /// </summary>
    public UInt16 wWarning;          
    
    /// <summary>
    /// The Play time (in 1/10 sec)
    /// </summary>
    public UInt32 dwPlayTime;		 
    
    /// <summary>
    /// The User time (in 1/10 sec)
    /// </summary>
    public UInt32 dwUserTime;       
    
    /// <summary>
    /// Device application status ( = 0: Reset phase ( Menu ), > 0: Script execution phase ),
    /// corresponding to the global 'g_byAppStatus' of the device SW
    /// </summary>
    public UInt16 wDeviceAppStatus; 

    /// <summary>
    /// MP: the current meas. channel
    /// </summary>
    public byte byChan;             

    #endregion members

    #region methods
    
    /// <summary>
    /// Builds the device error string, based on the error Dword. 
    /// </summary>
    /// <returns>The device error string</returns>
    public string ErrorAsString ()
    {
      int nBit = System.Runtime.InteropServices.Marshal.SizeOf (typeof(UInt32)) * 8;
      string s = "";
      UInt32 j;
      int i;
      for (i=0, j=1; i < nBit; i++, j *= 2)
      {
        if ((dwError & j) != 0)
        {
          if (s.Length > 0) s += ",";
          s += string.Format ("{0}", i+1);
        }
      }
      return s;
    }

    /// <summary>
    /// Builds the error Dword, based on a device error string.
    /// </summary>
    /// <param name="sErr">The device error string</param>
    public void ErrorFromString (string sErr)
    {
      UInt32 ui = 0;
      string[] ars = sErr.Split (',');
      for (int i=0; i < ars.Length ;i++)
      {
        if (ars[i].Length == 0) continue;
        int nBitPos=int.Parse(ars[i]) - 1;
        ui += (UInt32)(1 << nBitPos);
      }
      // Overgive
      dwError = ui;
    }

    /// <summary>
    /// Builds the device error hint string, based on the error Dword. 
    /// </summary>
    /// <returns>The device error hint string</returns>
    public string ErrorAsHint ()
    {
      int nBit = System.Runtime.InteropServices.Marshal.SizeOf (typeof(UInt32)) * 8;
      string s = "";
      UInt32 j;
      int i;
      for (i=0, j=1; i < nBit; i++, j *= 2)
      {
        if ((dwError & j) != 0)
        {
          s += string.Format ("ID{0}: {1}\r\n", i+1, sErrorMsg[i]);
        }
      }
      return s;
    }

    /// <summary>
    /// Builds the device warning string, based on the warning Word. 
    /// </summary>
    /// <returns>The device warning string</returns>
    public string WarningAsString ()
    {
      int nBit = System.Runtime.InteropServices.Marshal.SizeOf (typeof(UInt16)) * 8;
      string s = "";
      UInt16 j;
      int i;
      for (i=0, j=1; i < nBit; i++, j *= 2)
      {
        if ((wWarning & j) != 0)
        {
          if (s.Length > 0) s += ",";
          s += string.Format ("{0}", i+1);
        }
      }
      return s;
    }

    /// <summary>
    /// Builds the warning Word, based on a device warning string.
    /// </summary>
    /// <param name="sWar">The device warning string</param>
    public void WarningFromString (string sWar)
    {
      UInt16 ui = 0;
      string[] ars = sWar.Split (',');
      for (int i=0; i < ars.Length ;i++)
      {
        if (ars[i].Length == 0) continue;
        int nBitPos=int.Parse(ars[i]) - 1;
        ui += (UInt16)(1 << nBitPos);
      }
      // Overgive
      wWarning = ui;
    }

    /// <summary>
    /// Builds the device warning hint string, based on the warning Word. 
    /// </summary>
    /// <returns>The device warning hint string</returns>
    public string WarningAsHint ()
    {
      int nBit = System.Runtime.InteropServices.Marshal.SizeOf (typeof(UInt16)) * 8;
      string s = "";
      UInt16 j;
      int i;
      for (i=0, j=1; i < nBit; i++, j *= 2)
      {
        if ((wWarning & j) != 0)
        {
          s += string.Format ("ID{0}: {1}\r\n", i+1, sWarningMsg[i]);
        }
      }
      return s;
    }
   
    /// <summary>
    ///  Gets the state of the "device ready for measurement" flag (Bit 15) of the devices HW control word (CTRLIO).
    /// </summary>
    /// <returns>The state of the flag: False, if the device is NOT ready for measurement; True otherwise</returns>
    public bool GetReadyForMeas()
    {
      bool bState = ( (wCtrlIO & (UInt16)(1 << 15)) == 0 ) ? false : true;
      return bState;
    }
    
    #endregion methods

  }
}
