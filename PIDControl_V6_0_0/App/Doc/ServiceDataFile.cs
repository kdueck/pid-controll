﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App
{

  /// <summary>
  /// 'ServiceDataLogo' enumeration
  /// </summary>
  /// <remarks>
  /// NOTE:
  ///  This enumeration MUST coincide with the 'Logo' enumeration of the device SW.
  /// </remarks>
  enum Logo
  {
    IUT = 0,
    ENIT = 1,
    ENVI = 2,
  };

  /// <summary>
  /// Class ServiceDataFile:
  /// 
  /// </summary>
  public class ServiceDataFile
  {

    #region constructor

    /// <summary>
    /// #tor
    /// </summary>
    public ServiceDataFile()
    {
    }

    #endregion constructror

    #region constants & enums

    // # of Service Data types (= # of different Service Data strings)
    internal const int NROFSVCDATASTRINGS = 4;

    #endregion constants & enums

    #region methods

    /// <summary>
    /// Parses a string for Key-Value pairs and replaces the Value string that corresponds to a given Key string
    /// by a new Value string.
    /// </summary>
    /// <param name="sTotal">The string</param>
    /// <param name="sKey">The Key string</param>
    /// <param name="sValNew">The new Value string</param>
    /// <returns>True, if OK; False otherwise</returns>
    internal bool SetValue(ref string sTotal, string sKey, string sValNew)
    {
      // Init.
      int idx = 0;                                        // Begin idx
      while (true)
      {
        // Get the limitations of the Key-Value row
        int idxa = sTotal.IndexOf(sKey, idx);            // Begin
        if (idxa == -1) return false;
        int idxe = sTotal.IndexOf("\r\n", idxa);         // End
        if (idxe == -1) return false;
        // Get the Key-Value row
        string sRow = sTotal.Substring(idxa, idxe - idxa);
        sRow = sRow.Trim();
        // Get the position of the Key-Value separator
        string target = "=:";
        char[] anyOf = target.ToCharArray();
        idx = sRow.IndexOfAny(anyOf, 0);
        if (-1 == idx)
        {
          // The row is NOT a Key-Value row (but a section row f.i.): Continue searching
          idx = idxe;
          continue;
        }
        // Get the Key   
        string sKeyCmp = sRow.Substring(0, idx);
        sKeyCmp = sKeyCmp.Trim();
        if (sKey != sKeyCmp)
        {
          // The Key found doesn't correspond to the given Key: Continue searching
          idx = idxe;
          continue;
        }
        // We found the row:
        // Build the new row ...
        string sSep = sRow.Substring (idx, 1);
        string sKeyVal = sKey + " " + sSep + " " + sValNew;
        // ... get its front/end limiters ...
        string sa = sTotal.Substring(0, idxa);
        string se = sTotal.Substring(idxe);
        // ... and build the resulting total string 
        string sTotalNew = sa + sKeyVal + se;
        // Overgive
        sTotal = sTotalNew;
        break;
      }
      return true;
    }

    /// <summary>
    /// Parses a string for Key-Value pairs and gets the Value string that corresponds to a given Key string.
    /// </summary>
    /// <param name="sTotal">The string</param>
    /// <param name="sKey">The Key string</param>
    /// <returns>The Value string</returns>
    internal string GetValue(string sTotal, string sKey)
    {
      // Init.
      string sVal = "";                                   // Empty value string
      int idx = 0;                                        // Begin idx
      while (true)
      {
        // Get the limitations of the Key-Value row
        int idxa = sTotal.IndexOf(sKey, idx);            // Begin
        if (idxa == -1) return sVal;
        int idxe = sTotal.IndexOf("\r\n", idxa);         // End
        if (idxe == -1) return sVal;
        // Get the Key-Value row
        string sRow = sTotal.Substring(idxa, idxe - idxa);
        sRow = sRow.Trim();
        // Get the position of the Key-Value separator
        string target = "=:";
        char[] anyOf = target.ToCharArray();
        idx = sRow.IndexOfAny(anyOf, 0);
        if (-1 == idx)
        {
          // The row is NOT a Key-Value row (but a section row f.i.): Continue searching
          idx = idxe;
          continue;
        }
        // Get the Key   
        string sKeyCmp = sRow.Substring(0, idx);
        sKeyCmp = sKeyCmp.Trim();
        if (sKey != sKeyCmp)
        {
          // The Key found doesn't correspond to the given Key: Continue searching
          idx = idxe;
          continue;
        }
        // Get the Value
        sVal = sRow.Substring(idx + 1);
        sVal = sVal.Trim();
        break;
      }
      return sVal;
    }

    /// <summary>
    /// Conversion: Service data file string -> Service Data string array
    /// </summary>
    /// <param name="sSvcFile">The service data file string</param>
    /// <returns>The service Data string array</returns>
    internal string[] ServiceDataAsStringArray(string sSvcFile)
    {
      string sVal, sVal1;
      string[] arsServiceData = new string[NROFSVCDATASTRINGS];
      StringBuilder sb = new StringBuilder ();
      
      // ---------------------------------
      // Section: Device

      sb.Clear();
      // Device No
      sVal = GetValue(sSvcFile, "DevNo");
      sb.AppendFormat("{0}\t", sVal);
      // Cell No
      sVal = GetValue(sSvcFile, "CellNo");
      sb.AppendFormat("{0}\t", sVal);
      // PID Lamptime
      //    Notes:
      //      On the device the lamptime is handled in (10 min) units, here the display is in h units.
      //      Thats why a scaling must be performed.
      sVal = GetValue(sSvcFile, "PIDLamptime");
      int idx = sVal.IndexOf("h");
      if (-1 != idx)
      {
        sVal = sVal.Substring(0, idx);
        sVal = sVal.Trim();                     // Lamptime in (h) units
        int nLampTime = int.Parse(sVal);        // dito
        nLampTime *= 6;                         // Lamptime in (10min) units
        sVal = nLampTime.ToString();            // dito
      }
      sb.AppendFormat("{0}\t", sVal);
      // Accu display: on/off         
      sVal = GetValue(sSvcFile, "AccuDisplay");
      sVal = (sVal == "On") ? "1" : "0";
      sb.AppendFormat("{0}\t", sVal);
      // Logo
      sVal = GetValue(sSvcFile, "Logo");
      int nLogo;
      switch (sVal)
      {
        case "IUT": nLogo = (int)Logo.IUT; break;
        default:
        case "ENIT": nLogo = (int)Logo.ENIT; break;
        case "ENVI": nLogo = (int)Logo.ENVI; break;
      }
      sb.AppendFormat("{0}\t", nLogo);
      // Pump On times
      int nPumpTime;
      //    P1
      sVal = GetValue(sSvcFile, "P1OnTime");
      idx = sVal.IndexOf("h");
      if (-1 != idx)
      {
        sVal = sVal.Substring(0, idx);
        sVal = sVal.Trim();                     // P1On time in (h) units
        nPumpTime = int.Parse(sVal);            // dito
        nPumpTime *= 60;                        // P1On time in (1min) units
        sVal = nPumpTime.ToString();            // dito
      }
      sb.AppendFormat("{0}\t", sVal);
      //    P2
      sVal = GetValue(sSvcFile, "P2OnTime");
      idx = sVal.IndexOf("h");
      if (-1 != idx)
      {
        sVal = sVal.Substring(0, idx);
        sVal = sVal.Trim();
        nPumpTime = int.Parse(sVal);        
        nPumpTime *= 60;                        
        sVal = nPumpTime.ToString();            
      }
      sb.AppendFormat("{0}\t", sVal);
      //    P3
      sVal = GetValue(sSvcFile, "P3OnTime");
      idx = sVal.IndexOf("h");
      if (-1 != idx)
      {
        sVal = sVal.Substring(0, idx);
        sVal = sVal.Trim();
        nPumpTime = int.Parse(sVal);
        nPumpTime *= 60;
        sVal = nPumpTime.ToString();
      }
      sb.AppendFormat("{0}\t", sVal);
      // Self-check
      sVal = GetValue(sSvcFile, "SelfCheck");
      sb.AppendFormat("{0}\t", sVal);
      // Error confirmation: on/off         
      sVal = GetValue(sSvcFile, "ErrorConfirmation");
      sVal = (sVal == "On") ? "1" : "0";
      sb.AppendFormat("{0}\t", sVal);
      // PW check: on/off         
      sVal = GetValue (sSvcFile, "PWOnOff");
      sVal = (sVal == "On") ? "1" : "0";
      sb.AppendFormat ("{0}\t", sVal);
      // PW
      sVal = GetValue (sSvcFile, "PW");
      sb.AppendFormat ("{0}\t", sVal);

      // Overtake the 1st service data string
      arsServiceData[0] = sb.ToString();

      // ---------------------------------
      // Section: DateTime

      sb.Clear();
      // DateTime
      sVal = GetValue(sSvcFile, "DateTime");
      string sDT = sVal;
      DateTime dt = new DateTime(
        int.Parse(sDT.Substring(6, 4)),
        int.Parse(sDT.Substring(3, 2)),
        int.Parse(sDT.Substring(0, 2)),
        int.Parse(sDT.Substring(11, 2)),
        int.Parse(sDT.Substring(14, 2)),
        int.Parse(sDT.Substring(17, 2))
        );
      sVal = string.Format ( "{0:D2}{1:D2}{2:D2}{3:D2}{4:D2}{5:D2}",
          dt.Year - 2000, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second
          );
      sb.AppendFormat("{0}\t", sVal);
      // Service date
      sVal = GetValue(sSvcFile, "ServiceDate");
      string sDTSvc = sVal;
      DateTime dtSvc = new DateTime(
        int.Parse(sDTSvc.Substring(6, 4)),
        int.Parse(sDTSvc.Substring(3, 2)),
        int.Parse(sDTSvc.Substring(0, 2)),
        0,
        0,
        0
        );
      sVal = string.Format("{0:D2}{1:D2}{2:D2}", dtSvc.Year - 2000, dtSvc.Month, dtSvc.Day );
      sb.AppendFormat("{0}\t", sVal);

      // Overtake the 2. service data string
      arsServiceData[1] = sb.ToString();

      // ---------------------------------
      // Section: Sensor

      sb.Clear();
      // 4 x Temp's (Soll values, 0 = heater off)
      //    T1
      sVal = GetValue(sSvcFile, "T1");
      sb.AppendFormat("{0}\t", sVal);
      //    T2
      sVal = GetValue(sSvcFile, "T2");
      sb.AppendFormat("{0}\t", sVal);
      //    T3
      sVal = GetValue(sSvcFile, "T3");
      sb.AppendFormat("{0}\t", sVal);
      //    T4
      sVal = GetValue(sSvcFile, "T4");
      sb.AppendFormat("{0}\t", sVal);
      // 3 x flow resp. pump values
      UInt16 nPump;
      //    P1/F1
      sVal = GetValue(sSvcFile, "P1");
      sVal1 = GetValue(sSvcFile, "P1/F1 handling");
      nPump = UInt16.Parse(sVal);
      if (sVal1 == "Checked") nPump |= 0x8000;    // Bit 15 set / reset if F1 is regulated / not regulated 
      sVal = nPump.ToString();
      sb.AppendFormat("{0}\t", sVal);
      //    P2/F2
      sVal = GetValue(sSvcFile, "P2");
      sVal1 = GetValue(sSvcFile, "P2/F2 handling");
      nPump = UInt16.Parse(sVal);
      if (sVal1 == "Checked") nPump |= 0x8000;
      sVal = nPump.ToString();
      sb.AppendFormat("{0}\t", sVal);
      //    P3/F3
      sVal = GetValue(sSvcFile, "P3");
      sVal1 = GetValue(sSvcFile, "P3/F3 handling");
      nPump = UInt16.Parse(sVal);
      if (sVal1 == "Checked") nPump |= 0x8000;
      sVal = nPump.ToString();
      sb.AppendFormat("{0}\t", sVal);
      // Pressure display on/off: Checked = Pressure display off = 0, Unchecked = Pressure display on = 1
      sVal = GetValue(sSvcFile, "PresDisp");
      sVal = (sVal == "Off") ? "0" : "1";
      sb.AppendFormat("{0}\t", sVal);
      // 3 x Flow Soll-Ist
      //    F1
      sVal = GetValue(sSvcFile, "F1-Soll");
      sb.AppendFormat("{0}\t", sVal);
      sVal = GetValue(sSvcFile, "F1-TolAbs");
      sb.AppendFormat("{0}\t", sVal);
      //    F2
      sVal = GetValue(sSvcFile, "F2-Soll");
      sb.AppendFormat("{0}\t", sVal);
      sVal = GetValue(sSvcFile, "F2-TolAbs");
      sb.AppendFormat("{0}\t", sVal);
      //    F3
      sVal = GetValue(sSvcFile, "F3-Soll");
      sb.AppendFormat("{0}\t", sVal);
      sVal = GetValue(sSvcFile, "F3-TolAbs");
      sb.AppendFormat("{0}\t", sVal);
      // 4 x 'P' parts for heater regulation
      //    H1
      sVal = GetValue(sSvcFile, "P-factor T1");
      sb.AppendFormat("{0}\t", sVal);
      //    H2
      sVal = GetValue(sSvcFile, "P-factor T2");
      sb.AppendFormat("{0}\t", sVal);
      //    H3
      sVal = GetValue(sSvcFile, "P-factor T3");
      sb.AppendFormat("{0}\t", sVal);
      //    H4
      sVal = GetValue(sSvcFile, "P-factor T4");
      sb.AppendFormat("{0}\t", sVal);

      // Overtake the 3. service data string
      arsServiceData[2] = sb.ToString();

      // ---------------------------------
      // Section: Spectrum analysis

      sb.Clear();
      // Scan offset min.
      sVal = GetValue(sSvcFile, "ScanOffsetMin");
      sb.AppendFormat("{0}\t", sVal);
      // Scan offset max.
      sVal = GetValue(sSvcFile, "ScanOffsetMax");
      sb.AppendFormat("{0}\t", sVal);
      // Peak search: 'n' sigma factor
      sVal = GetValue(sSvcFile, "NSigPS");
      sb.AppendFormat("{0}\t", sVal);
      // Peak search: mode
      sVal = GetValue(sSvcFile, "ModePS");
      int nPS;
      switch (sVal)
      {
        default:
        case "Unidirectional": nPS = 0; break;
        case "Bidirectional": nPS = 1; break;
      }
      sVal = nPS.ToString();
      sb.AppendFormat("{0}\t", sVal);
      // Peak area determination: 'n' sigma factor
      sVal = GetValue(sSvcFile, "NSigPA");
      sb.AppendFormat("{0}\t", sVal);
      // Peak area determination: mode
      sVal = GetValue(sSvcFile, "ModePA");
      int nPA;
      switch (sVal)
      {
        default:
        case "Simple": nPA = 0; break;
        case "Extended_1": nPA = 1; break;
        case "Extended_2": nPA = 2; break;
      }
      sVal = nPA.ToString();
      sb.AppendFormat("{0}\t", sVal);
      // Gain factor
      sVal = GetValue (sSvcFile, "GainFactor");
      sb.AppendFormat ("{0}\t", sVal);
      // Smoothing: 0 - MA, 1 - SG
      sVal = GetValue(sSvcFile, "SmoothingFilter");
      int nSmoothing;
      switch (sVal)
      {
        default:
        case "MA": nSmoothing = 0; break;
        case "SG": nSmoothing = 1; break;
      }
      sVal = nSmoothing.ToString();
      sb.AppendFormat("{0}\t", sVal);
      // MA filter width
      sVal = GetValue(sSvcFile, "MAFilterwidth");
      sb.AppendFormat("{0}\t", sVal);
      // SG filter width
      sVal = GetValue(sSvcFile, "SGFilterwidth");
      sb.AppendFormat("{0}\t", sVal);               

      // Overtake the 4. service data string
      arsServiceData[3] = sb.ToString();
    
      // Done
      return arsServiceData;
    }

    /// <summary>
    /// Conversion: Service Data string array -> Service data file string
    /// </summary>
    /// <param name="arsServiceData">The service Data string array</param>
    /// <returns>The service data file string</returns>
    internal string ServiceDataAsFileString(string[] arsServiceData)
    {
      string s, s1;
      StringBuilder sb = new StringBuilder();

      sb.Clear();

      // ---------------------------------
      // Section: Device
      string[] ars = arsServiceData[0].Split('\t');
      
      // Header
      sb.AppendLine("[Device]");
      // Device No
      s = string.Format ("DevNo = {0}", ars[0]);
      sb.AppendLine(s);
      // Cell No
      s = string.Format("CellNo = {0}", ars[1]);
      sb.AppendLine(s);
      // PID Lamptime
      // Notes:
      //  On the device the lamptime is handled in (10 min) units, here the display is in h units.
      //  Thats why a scaling must be performed.
      int nLampTime = int.Parse(ars[2]);                // Lamptime in (10min) units
      nLampTime /= 6;                                   // Lamptime in (h) units
      s = string.Format("PIDLamptime = {0} h", nLampTime);
      sb.AppendLine(s);
      // Accu display: on/off         
      s1 = (ars[3] == "1") ? "On" : "Off";
      s = string.Format("AccuDisplay = {0}", s1);
      sb.AppendLine(s);
      // Logo
      switch (ars[4])
      {
        case "0": s1 = "IUT"; break;
        default:
        case "1": s1 = "ENIT"; break;
        case "2": s1 = "ENVI"; break;
      }
      s = string.Format("Logo = {0}", s1);
      sb.AppendLine(s);
      // Pump On times
      int nPumpTime;
      //    P1
      nPumpTime = int.Parse(ars[5]);          // P1On time in (1min) units
      nPumpTime /= 60;                        // P1On time in (h) units
      s = string.Format("P1OnTime = {0} h", nPumpTime);
      sb.AppendLine(s);
      //    P2
      nPumpTime = int.Parse(ars[6]);
      nPumpTime /= 60;              
      s = string.Format("P2OnTime = {0} h", nPumpTime);
      sb.AppendLine(s);
      //    P3
      nPumpTime = int.Parse(ars[7]);
      nPumpTime /= 60;
      s = string.Format("P3OnTime = {0} h", nPumpTime);
      sb.AppendLine(s);
      // Self-check
      s = string.Format("SelfCheck = {0}", ars[8]);
      sb.AppendLine(s);
      // Error confirmation: on/off         
      s1 = (ars[9] == "1") ? "On" : "Off";
      s = string.Format("ErrorConfirmation = {0}", s1);
      sb.AppendLine(s);
      // PW check: on/off         
      s1 = (ars[10] == "1") ? "On" : "Off";
      s = string.Format ("PWOnOff = {0}", s1);
      sb.AppendLine (s);
      // PW
      s = string.Format ("PW = {0}", ars[11]);
      sb.AppendLine (s);

      // Empty line
      sb.AppendLine();

      // ---------------------------------
      // Section: DateTime
      ars = arsServiceData[1].Split('\t');

      // Header
      sb.AppendLine("[DateTime]");
      // DateTime
      string sDT = ars[0];
      DateTime dt = new DateTime(
        2000 + int.Parse(sDT.Substring(0, 2)),
        int.Parse(sDT.Substring(2, 2)),
        int.Parse(sDT.Substring(4, 2)),
        int.Parse(sDT.Substring(6, 2)),
        int.Parse(sDT.Substring(8, 2)),
        int.Parse(sDT.Substring(10, 2))
        );
      s1 = dt.ToString("dd.MM.yyyy HH:mm:ss");
      s = string.Format("DateTime = {0}", s1);
      sb.AppendLine(s);
      // Service date
      string sDTSvc = ars[1];
      DateTime dtSvc = new DateTime(
        2000 + int.Parse(sDTSvc.Substring(0, 2)),
        int.Parse(sDTSvc.Substring(2, 2)),
        int.Parse(sDTSvc.Substring(4, 2)),
        0,
        0,
        0
        );
      s1 = dtSvc.ToString("dd.MM.yyyy");
      s = string.Format("ServiceDate = {0}", s1);
      sb.AppendLine(s);

      // Empty line
      sb.AppendLine();

      // ---------------------------------
      // Section: Sensor
      ars = arsServiceData[2].Split('\t');

      // Header
      sb.AppendLine("[Sensor]");
      // 4 x T (Soll values)
      s = string.Format("T1 = {0}", ars[0]);
      sb.AppendLine(s);
      s = string.Format("T2 = {0}", ars[1]);
      sb.AppendLine(s);
      s = string.Format("T3 = {0}", ars[2]);
      sb.AppendLine(s);
      s = string.Format("T4 = {0}", ars[3]);
      sb.AppendLine(s);
      // 4 x 'P' parts for heater regulation
      s = string.Format("P-factor T1 = {0}", ars[14]);
      sb.AppendLine(s);
      s = string.Format("P-factor T2 = {0}", ars[15]);
      sb.AppendLine(s);
      s = string.Format("P-factor T3 = {0}", ars[16]);
      sb.AppendLine(s);
      s = string.Format("P-factor T4 = {0}", ars[17]);
      sb.AppendLine(s);
      // 3 x P/F handling
      // Notes/TODO:
      //  In principle 'nPump' should be declared as UInt16. Unfortunately the syntax
      //    nPump &= ~0x8000;
      //  generates a compiler error in this case. Thats why the int type is used instead.
      int nPump;
      //    P1/F1
      nPump = int.Parse(ars[4]);
      nPump &= ~0x8000; 
      s = string.Format("P1 = {0}", nPump);
      sb.AppendLine(s);
      //    P2/F2
      nPump = int.Parse(ars[5]);
      nPump &= ~0x8000;
      s = string.Format("P2 = {0}", nPump);
      sb.AppendLine(s);
      //    P3/F3
      nPump = int.Parse(ars[6]);
      nPump &= ~0x8000;
      s = string.Format("P3 = {0}", nPump);
      sb.AppendLine(s);
      // Pressure display on/off: Checked = Pressure display off = 0, Unchecked = Pressure display on = 1
      s1 = (ars[7] == "1") ? "On" : "Off";
      s = string.Format("PresDisp = {0}", s1);
      sb.AppendLine(s);
      // 3 x P/F handling
      // Notes:
      //  See above.
      //    P1/F1
      nPump = int.Parse(ars[4]);
      s1 = ((nPump & 0x8000) != 0) ? "Checked" : "Not checked";
      s = string.Format("P1/F1 handling = {0}", s1);
      sb.AppendLine(s);
      //    P2/F2
      nPump = int.Parse(ars[5]);
      s1 = ((nPump & 0x8000) != 0) ? "Checked" : "Not checked";
      s = string.Format("P2/F2 handling = {0}", s1);
      sb.AppendLine(s);
      //    P3/F3
      nPump = int.Parse(ars[6]);
      s1 = ((nPump & 0x8000) != 0) ? "Checked" : "Not checked";
      s = string.Format("P3/F3 handling = {0}", s1);
      sb.AppendLine(s);
      // 3 x Flow Soll-Ist
      //    F1
      s = string.Format("F1-Soll = {0}", ars[8]);
      sb.AppendLine(s);
      s = string.Format("F1-TolAbs = {0}", ars[9]);
      sb.AppendLine(s);
      //    F2
      s = string.Format("F2-Soll = {0}", ars[10]);
      sb.AppendLine(s);
      s = string.Format("F2-TolAbs = {0}", ars[11]);
      sb.AppendLine(s);
      //    F3
      s = string.Format("F3-Soll = {0}", ars[12]);
      sb.AppendLine(s);
      s = string.Format("F3-TolAbs = {0}", ars[13]);
      sb.AppendLine(s);

      // Empty line
      sb.AppendLine();

      // ---------------------------------
      // Section: Spectrum analysis
      ars = arsServiceData[3].Split('\t');

      // Header
      sb.AppendLine("[Analysis]");
      // Min. scan offset
      s = string.Format("ScanOffsetMin = {0}", ars[0]);
      sb.AppendLine(s);
      // Max. scan offset
      s = string.Format("ScanOffsetMax = {0}", ars[1]);
      sb.AppendLine(s);
      // Peak search: 'n' sigma factor
      s = string.Format("NSigPS = {0}", ars[2]);
      sb.AppendLine(s);
      // Peak search: mode
      switch (ars[3])
      {
        default:
        case "0": s1 = "Unidirectional"; break;
        case "1": s1 = "Bidirectional"; break;
      }
      s = string.Format("ModePS = {0}", s1);
      sb.AppendLine(s);
      // Peak area determination: 'n' sigma factor
      s = string.Format("NSigPA = {0}", ars[4]);
      sb.AppendLine(s);
      // Peak area determination: mode
      switch (ars[5])
      {
        default:
        case "0": s1 = "Simple"; break;
        case "1": s1 = "Extended_1"; break;
        case "2": s1 = "Extended_2"; break;
      }
      s = string.Format("ModePA = {0}", s1);
      sb.AppendLine(s);
      // Gain factor
      s = string.Format ("GainFactor = {0}", ars[6]);
      sb.AppendLine (s);
      // Smoothing filter: 0 - MA, 1 - SG
      switch (ars[7])
      {
        default:
        case "0": s1 = "MA"; break;
        case "1": s1 = "SG"; break;
      }
      s = string.Format("SmoothingFilter = {0}", s1);
      sb.AppendLine(s);
      // MA filter width
      s = string.Format("MAFilterwidth = {0}", ars[8]);
      sb.AppendLine(s);
      // SG filter width
      s = string.Format("SGFilterwidth = {0}", ars[9]);
      sb.AppendLine(s);

      // Done
      string sSvcFile = sb.ToString();
      return sSvcFile;
    
    }

    #endregion methods

  }
}
