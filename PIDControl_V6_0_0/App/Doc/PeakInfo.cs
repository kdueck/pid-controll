using System;
using System.Collections;

namespace App
{
  /// <summary>
  /// Class PeakInfo:
  /// Peak information
  /// </summary>
  public class PeakInfo
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public PeakInfo()
    {
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="sPeakInfo">A peak information string</param>
    /// <remarks>
    /// A peak info string has the following format:
    ///   "(pos_0),(invalid_0),(height_0),(area_0), ... ,(pos_Nm1),(invalid_Nm1),(height_Nm1),(area_Nm1)"
    /// with: N - # of peaks
    /// </remarks>
    public PeakInfo (string sPeakInfo)
    {
      // Parse the peak information string
      string[] ars = sPeakInfo.Split ( ',' );
      // Its length
      int nCount = ars.Length;
      if (nCount%4 > 0) nCount = (nCount/4)*4;
      // Fill the Peakinfo item list
      Items.Clear ();
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
      for ( int i=0; i < nCount; i+=4 )
      {
        PeakInfoItem item = new PeakInfoItem ();
        
        // Current peak: Position
        item.Position = UInt16.Parse(ars[i]);             
        // Current peak: Height
        float height  = float.Parse(ars[i+1]);         // Height in adc units ...     
        height *= (float) (100.0/Doc.ADC_RESOLUTION);  // ... and scaled to % unit as the "basic ordinate unit"
        item.Height   = height;
        // Current peak: Invalid
        item.Invalid  = byte.Parse(ars[i+2]);             
        // Current peak: Area
        item.Area     = float.Parse (ars[i+3], nfi );     
        
        Items.Add ( item );
      }   
    }

    #endregion constructors
    
    #region members

    /// <summary>
    /// The Peakinfo item list
    /// </summary>
    public PeakInfoItemList Items =  new PeakInfoItemList ();
 
    #endregion members
  
  }


  /// <summary>
  /// Class PeakInfoItem:
  /// Peak info item
  /// </summary>
  public class PeakInfoItem
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public PeakInfoItem()
    {
    }

    #endregion constructors

    #region members

    /// <summary>
    /// Abscissa (position) (in [0, 2047], unit: pts)
    /// </summary>
    public UInt16 Position = 0; 
    
    /// <summary>
    /// Height (units: [%])
    /// </summary>
    public float Height = 0;

    /// <summary>
    /// Invalidity of the current peak (0 - valid, 1 - invalid(OverFlow))
    /// </summary>
    public byte Invalid = 0;
    
    /// <summary>
    /// Area (units: [adc*pts])
    /// </summary>
    public float Area = 0;        

    #endregion members

  }

 
  /// <summary>
  /// Class PeakInfoItemList:
  /// Peakinfo item list
  /// </summary>
  public class PeakInfoItemList : IEnumerable
  {
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public PeakInfoItemList ()
    {
    }

    #endregion constructors

    #region members
    
    /// <summary>
    /// The list
    /// </summary>
    private ArrayList _Items = new ArrayList();

    #endregion members
    
    #region methods

    /// <summary>
    /// Clears the list
    /// </summary>
    public void Clear () 
    {
      _Items.Clear ();
    }

    /// <summary>
    /// Adds a PeakInfoItem object to the list
    /// </summary>
    /// <param name="res">The PeakInfoItem object to be added</param>
    public void Add ( PeakInfoItem item ) 
    {
      _Items.Add ( item );
    }
    
    #endregion methods

    #region properties

    /// <summary>
    /// Gets the size of the list
    /// </summary>
    public int Count 
    {
      get { return _Items.Count; }
    }
    
    #endregion properties

    #region indexer
    
    /// <summary>
    /// The indexer
    /// </summary>
    public PeakInfoItem this[int index]
    {
      get { return (PeakInfoItem)_Items[index]; }
    }

    #endregion indexer
    
    #region IEnumerable Members

    /// <summary>
    /// The enumerator
    /// </summary>
    /// <returns>The enumerator</returns>
    public IEnumerator GetEnumerator ()
    {
      return _Items.GetEnumerator ();
    }

    #endregion IEnumerable Members

  }  

}