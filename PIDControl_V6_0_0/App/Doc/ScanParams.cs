using System;

namespace App
{
	/// <summary>
	/// Class ScanParams:
	/// Scan parameters: Meas. cycle, signal processing interval, PC communication interval, 
	///                 accurate, amplifier scale member, smoothing filter type / width,
	///                 Peak search 'n' sigma factor & mode, 
	///                 and Peak area determination 'n' sigma factor & mode. 
	/// </summary>
	public class ScanParams
	{
    #region constructors

    /// <summary>
    /// Constructor
    /// </summary>
    public ScanParams()
		{
      wMeasCycle = 0;         
      wSigProcInt = 0;		 
      wPCCommInt = 0;       
      wAccurate = 0; 
      dGainFactor = 10.4;
      
      bySmoothingFilterType = 1;
      bySmoothingFilterWidth = 41;
      
      byNSigPeakSearch = 6;
      byModePeakSearch = 0;
      byNSigAreaDetection = 3;
      byModeAreaDetection = 0;
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="sPar">A string containing ScanParam information</param>
    public ScanParams ( string sPar )
    {
      string[] ars = sPar.Split ( ',' );
      if ( ars.Length != _NO_SCANPARAM_MEMBERS )
        throw new System.ApplicationException ();
      
      wMeasCycle              = UInt16.Parse ( ars[0] );
      wSigProcInt             = UInt16.Parse ( ars[1] );
      wPCCommInt              = UInt16.Parse ( ars[2] );
      wAccurate               = UInt16.Parse ( ars[3] );
      System.Globalization.NumberFormatInfo nfi = System.Globalization.NumberFormatInfo.InvariantInfo;
      dGainFactor             = double.Parse ( ars[4], nfi );

      bySmoothingFilterType   = byte.Parse (ars[5]);
      bySmoothingFilterWidth  = byte.Parse (ars[6]);
      
      byNSigPeakSearch        = byte.Parse (ars[7]);
      byModePeakSearch        = byte.Parse (ars[8]);
      byNSigAreaDetection     = byte.Parse (ars[9]);
      byModeAreaDetection     = byte.Parse (ars[10]);
    }
  
    #endregion constructors

    #region constants
    
    // The # of ScanParam members
    const int _NO_SCANPARAM_MEMBERS = 11; 

    #endregion constants

    #region members
    
    /// <summary>
    /// The meas. cycle (in 1/10 s)
    /// </summary>
    public UInt16 wMeasCycle;         
    
    /// <summary>
    /// The signal processing interval (in pts)
    /// </summary>
    public UInt16 wSigProcInt;		 
    
    /// <summary>
    /// The PC communication interval (in pts)
    /// </summary>
    public UInt16 wPCCommInt;       
    
    /// <summary>
    /// The accurate
    /// </summary>
    public UInt16 wAccurate; 

    /// <summary>
    /// The Gain factor
    /// </summary>
    public double dGainFactor;

    /// <summary>
    /// The smoothing filter type:
    /// 0 - MA (Moving average), 1 - SG (Savitzky/Golay) 
    /// </summary>
    public byte bySmoothingFilterType;
    /// <summary>
    /// The smoothing filter width 
    /// </summary>
    public byte bySmoothingFilterWidth;
    
    /// <summary>
    /// Peak search 'n' sigma factor
    /// </summary>
    public byte byNSigPeakSearch;
    /// <summary>
    /// The peak search mode:
    /// 0 - Unidirectional, 1 - Bidirectional 
    /// </summary>
    public byte byModePeakSearch;

    /// <summary>
    /// Peak area determination 'n' sigma factor
    /// </summary>
    public byte byNSigAreaDetection;
    /// <summary>
    /// The peak area determination mode:
    /// 0 - Simple, 1 - Extended/level 1, 2 - Extended/level 2
    /// </summary>
    public byte byModeAreaDetection;

    #endregion members

  }
}
